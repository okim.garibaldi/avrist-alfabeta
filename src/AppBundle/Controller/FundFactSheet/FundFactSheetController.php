<?php

namespace AppBundle\Controller\FundFactSheet;

use AppBundle\Controller\DefaultController;
use Pimcore\Controller\FrontendController;
use Symfony\Component\HttpFoundation\Request;
use \Pimcore\Model\DataObject;

class FundFactSheetController extends FrontendController
{
    public function lifeIndexAction()
    {
        $data = new DataObject\Product\Listing();
        $data->setCondition("categoryType = 'life'");
        $data->load();
        $this->view->data = $data;
    }

    public function generalIndexAction()
    {
        $data = new DataObject\Product\Listing();
        $data->setCondition("categoryType = 'general'");
        $data->load();
        $this->view->data = $data;
    }

    public function IndexAction(Request $request)
    {
        if($request->request->has('s'))
        {
            $s = "%".$request->get('s')."%";
        }
        else
        {
            $s = "%_%";
        }

        if(\Pimcore\Model\Site::isSiteRequest()) {
            $site = \Pimcore\Model\Site::getCurrentSite();
            $siteId = $site->getId();
        }
        else{
            $siteId = 1;
        }

        $bgBanner = DefaultController::getBgBanner($siteId);

        $this->view->bgBanner = $bgBanner;

        $objs1 = new DataObject\FundFactPage\Listing();
        $objs1->setCondition("siteId = ?", $siteId);
        $objs1->load();

        foreach($objs1 as $obj1)
        {
            $fundFactPage = $obj1;
        }

        $objs2 = new DataObject\AboutCompany\Listing();
        $objs2->setCondition("siteId = ?", $siteId);
        $objs2->load();

        foreach($objs2 as $obj2)
        {
            $aboutCompany = $obj2;
        }
        
        $product = new DataObject\Product\Listing();
        $product->setCondition("siteId = ?", $siteId);
        $product->addConditionParam("name LIKE ?", $s, "AND");
        $product->load();
        $this->view->product = $product;

        $this->view->aboutCompany = $aboutCompany;

        $this->view->fundFactPage = $fundFactPage;

        $this->view->search = True;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer($siteId);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT1;
    }

    public function detailAction(Request $request)
    {
        $key = $request->get('path');

        if(\Pimcore\Model\Site::isSiteRequest()) {
            $site = \Pimcore\Model\Site::getCurrentSite();
            $siteId = $site->getId();
        }
        else{
            $siteId = 1;
        }

        $bgBanner = DefaultController::getBgBanner($siteId);

        $this->view->bgBanner = $bgBanner;

        $objs1 = new DataObject\Product\Listing();
        $objs1->setCondition("o_key = ?", $key);
        $objs1->load();

        foreach($objs1 as $obj1)
        {
            $product = $obj1;
        }

        $objs2 = new DataObject\Listing();
        $objs2->setCondition("o_parentId = ?", $product->getId());
        $objs2->load();

        foreach($objs2 as $obj2)
        {
            $funFactFolder = $obj2;
        }

        $years = new DataObject\Listing();
        $years->setCondition("o_parentId = ?", $funFactFolder->getId());
        $years->setOrderKey("o_index");
        $years->setOrder("asc");
        $years->load();

        foreach ($years as $year)
        {   
            $fundFact = new DataObject\FundFact\Listing();
            $fundFact->setCondition("o_parentId = ?", $year->getId());
            $fundFact->load();
            $file = [];
            foreach($fundFact as $fF)
            {
                array_push($file, [
                    'name' => $fF->getKey(),
                    'file' => \Pimcore\Model\Asset::getByPath($fF->getFile())]);
            }
            $fundFacts[strval($year->getKey())] = $file;
        }

        $objs3 = new DataObject\AboutCompany\Listing();
        $objs3->setCondition("siteId = ?", $siteId);
        $objs3->load();

        foreach($objs3 as $obj3)
        {
            $aboutCompany = $obj3;
        }

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer($siteId);

        return $this->render(":Fund-fact:details.html.php", 
        [
            "navbar" => $navbar, 
            "footer" => $footer,
            "bgBanner" => $bgBanner,
            "aboutCompany" => $aboutCompany,
            "product" => $product,
            "fundFacts" => $fundFacts,
            "otherAwards" => $otherAwards,
            "script" => DefaultController::SCRIPT4
        ]);
    }
}
