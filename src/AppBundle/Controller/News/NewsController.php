<?php

namespace AppBundle\Controller\News;

use Pimcore\Controller\FrontendController;
use Pimcore\Model\DataObject;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Controller\DefaultController;

class NewsController extends FrontendController
{
    public function defaultAction(Request $request)
    {

    }

    public function indexAction(Request $request)
    {
        
        if(\Pimcore\Model\Site::isSiteRequest()) {
            $site = \Pimcore\Model\Site::getCurrentSite();
            $siteId = $site->getId();
        }
        else{
            $siteId = 1;
        }

        $this->view->siteId = $siteId;

        $sortBy = "desc";
        $category = "news"; 
        $search = "%_%";

        if($request->request->has('sortBy')) {
            $sortBy = $request->get('sortBy');
            $category = $request->get('category');
            $search = "%".$request->get('search')."%";
        }
        
        $this->view->category = $category;
        $this->view->sortBy = $sortBy;

        $bgBanner = DefaultController::getBgBanner($siteId);

        $objs1 = new DataObject\NewsPage\Listing();
        $objs1->setCondition("siteId = ?", $siteId);
        $objs1->load();

        foreach($objs1 as $obj1)
        {
            $newsPage = $obj1;
        }

        $news = new DataObject\Article\Listing();
        $news->setCondition("category = ? ", $category);
        $news->addConditionParam("siteId = ?", $siteId, "AND"); 
        $news->addConditionParam("title1 LIKE ?", $search, "AND"); 
        $news->setLimit(12);
        $news->setOrderKey(["publishDate", "o_id"]);
        $news->setOrder($sortBy);
        $news->load();

        $pageClasses = [
            "partnership" => "\Pimcore\Model\DataObject\PartnershipPage\Listing",
            "lifeGuide" => "\Pimcore\Model\DataObject\LifeGuidePage\Listing",
            "awards" => "\Pimcore\Model\DataObject\AwardsPage\Listing",
            "csr" => "\Pimcore\Model\DataObject\CsrPage\Listing"];

        if($category == "news" || $category == "event")
        {
            $this->view->page = $newsPage;
        }
        else
        {
            $objs2 = new $pageClasses[$category]();
            $objs2->setCondition("siteId = ?", $siteId);
            $objs2->load();

            foreach($objs2 as $obj2)
            {
                $page = $obj2;
            }

            $this->view->page = $page;
        }

        $articleObj = new DataObject\Article();
        $fd = $articleObj->getClass()->getFieldDefinition("category");
        $options = $fd->getOptions();

        $this->view->newsIndex = TRUE;

        $this->view->options = $options;

        $rss_feed = simplexml_load_file("http://avrist.com/lifeguide/feed/");

        $this->view->rss_feed = $rss_feed;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer($siteId);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT1;

        $this->view->newsPage = $newsPage;

        $this->view->news = $news;

        $this->view->bgBanner = $bgBanner;
    }

    //public function index2Action(Request $request)
    //{

        
        //$curDocId = $this->document->getId();
        //$curDocs = new \Pimcore\Model\Document\Listing();
        //$curDocs->setCondition('parentId = ?', $curDocId);
        //$curDocs->load();
//
        //$newsDaid = "(";
        //foreach ($curDocs  as $curDoc)
        //{
        //    $newsFoParId = $curDoc->getId();
        //    $newsPages = new \Pimcore\Model\Document\Listing();
        //    $newsPages->setCondition('parentId = ?', $newsFoParId)
        //        ->setLimit(1);
        //    $newsPages->load();
        //    foreach($newsPages as $newsPage){
        //        $newsPaId = $newsPage->getId();
        //    }
        //    $newsData = new DataObject\Article\Listing();
        //    $newsData->setCondition('page__Id = ?', $newsPaId);
        //    $newsData->load();
        //    foreach ($newsData as $nD)
        //    {
        //        $newsDaId .= strval($nD->getParentId()). ", ";
        //    }
//
        //    //$newsData = new DataObject\Article\Listing();
        //    //$newsData->setCondition('page__Id = ?', $newsPaId);
        //    //$newsData->load();  
        //    //$newsDataParId[] = $newsData->getParentId();
        //}
//
        //
        //$news = new DataObject\Article\Listing();
        //$news->setCondition('o_parentId IN (?)', $newsDaId);
        //$news->load();
//
        //foreach ($news as $n)
        //{
        //    echo $n->getId();
        //}
        //$this->view->news = $news;
        //foreach ($newsPages  as $newsPage)
        //{
        //    $newsPaParId[] = $newsPage->getId();
        //}



        //news = new DataObject\Article\Listing();
        


        //$news = new DataObject\Article\Listing();
        //$site = \Pimcore\Model\Site::getCurrentSite();
        //$siteId = $site->getId();
        //if ($siteId == 1)
        //{
        //    $news->setCondition('o_parentId IN (455, 458)');
        //    $news->load();
        //}
        //elseif ($siteId == 2){
        //    $news->setCondition('o_parentId IN (456, 459)');
        //    $news->load();
        //}
        //else
        //{
        //    $news->setCondition('o_parentId IN (457, 460)');
        //    $news->load();
        //}

    //}

    public function eventLaunchingAction(Request $request)
    {
		$news = DataObject\News::getById(144);

        $this->view->news = $news;
    }

    public function avristDukungAction(Request $request)
    {
		$news = DataObject\News::getById(145);

        $this->view->news = $news;
    }

    public function newsDetailAction(Request $request)
    {   
        //$id = $this->document->getTitle(); 
        //$news =  DataObject\News::getByTitle($id, 1);
        //echo $news->getTitle;
        if(\Pimcore\Model\Site::isSiteRequest()) {
            $site = \Pimcore\Model\Site::getCurrentSite();
            $siteId = $site->getId();
        }
        else{
            $siteId = 1;
        }

        $titlePath = $request->get('path');

        $bgBanner = DefaultController::getBgBanner($siteId);

        $objs1 = new DataObject\NewsPage\Listing();
        $objs1->setCondition("siteId = ?", $siteId);
        $objs1->load();

        foreach($objs1 as $obj1)
        {
            $newsPage = $obj1;
        }

        $news = DataObject\Article::getByPath($newsPage->getFullPath()."/".$titlePath);

        $otherNews = new DataObject\Article\Listing();
        $otherNews->setCondition("o_parentId = ?", $news->getParentId());
        $otherNews->addConditionParam("o_Id != ?", $news->getId(), "AND");
        $otherNews->setOrderKey(["publishDate", "o_id"]);
        $otherNews->setOrder("desc");
        $otherNews->load();

        $objs2 = new DataObject\AboutCompany\Listing();
        $objs2->setCondition("siteId = ?", $siteId);
        $objs2->load();

        foreach($objs2 as $obj2)
        {
            $aboutCompany = $obj2;
        }

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer($siteId);

        return $this->render(":News:details.html.php", 
        [
            "navbar" => $navbar, 
            "footer" => $footer,
            "siteId" => $siteId,
            "aboutCompany" => $aboutCompany,
            "newsPage" => $newsPage,
            "news" => $news,
            "otherNews" => $otherNews,
            "bgBanner" => $bgBanner,
            "script" => DefaultController::SCRIPT1
        ]);
    }
}
