<?php

namespace AppBundle\Controller\TataKelola;

use Carbon\Carbon;
use AppBundle\Controller\DefaultController;
use Pimcore\Controller\FrontendController;
use Symfony\Component\HttpFoundation\Request;
use \Pimcore\Model\DataObject;

class TataKelolaController extends FrontendController
{
    public function indexAction(Request $request)
    {
        if(\Pimcore\Model\Site::isSiteRequest()) {
            $site = \Pimcore\Model\Site::getCurrentSite();
            $siteId = $site->getId();
        }
        else{
            $siteId = 1;
        }

        $objs1 = new DataObject\TataKelolaPerusahaanPage\Listing();
        $objs1->setCondition("siteId = ?", $siteId);
        $objs1->load();

        foreach ($objs1 as $obj1)
        {
            $tataKelolaPerusahaanPage = $obj1;
        }

        $this->view->tataKelolaPerusahaanPage = $tataKelolaPerusahaanPage;

        foreach($tataKelolaPerusahaanPage->getBlock() as $block)
        {
            $file = [];
            foreach($block['subblock']->getData() as $subblock)
            {
                array_push($file, [
                    "name" => $subblock['name']->getData(),
                    "file" => \Pimcore\Model\Asset::getByPath($subblock['file']->getData())
                ]);
            }
            $years[$block['year']->getData()] = $file;
        }

        $this->view->years = $years;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer($siteId);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT1;   
    }

}
