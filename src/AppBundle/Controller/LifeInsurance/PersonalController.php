<?php

namespace AppBundle\Controller\LifeInsurance;

use AppBundle\Controller\DefaultController;
use Pimcore\Controller\FrontendController;
use Symfony\Component\HttpFoundation\Request;
use \Pimcore\Model\DataObject;

class PersonalController extends FrontendController
{
    public function LifePersonalCategory()
    {
        $life_personal = new DataObject\ProductCategory\Listing();
        $life_personal->setCondition('categoryType LIKE ?', ['%individu%']);
        $life_personal->setOrderKey('id');
        $life_personal->setOrder('ASC');

        return $life_personal;
    }

    /**
     * @param Request $request
     */
    public function defaultAction(Request $request)
    {

    }

    public function lifeAction()
    {
        if(\Pimcore\Model\Site::isSiteRequest()) {
            $site = \Pimcore\Model\Site::getCurrentSite();
            $siteId = $site->getId();
        }
        else{
            $siteId = 1;
        }

        $product = new DataObject\Product\Listing();
        $product->setCondition("category__id = 44");
        $product->load();

        $category = DataObject\ProductCategory::getById(44);

        $this->view->product = $product;
        $this->view->category = $category;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer(1);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT6;
    }

    public function syariahAction()
    {
        if(\Pimcore\Model\Site::isSiteRequest()) {
            $site = \Pimcore\Model\Site::getCurrentSite();
            $siteId = $site->getId();
        }
        else{
            $siteId = 1;
        }

        $product = new DataObject\Product\Listing();
        $product->setCondition("category__id = 45");
        $product->load();

        $category = DataObject\ProductCategory::getById(45);

        $this->view->product = $product;
        $this->view->category = $category;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer(1);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT6;
    }

    public function healthAction()
    {
        if(\Pimcore\Model\Site::isSiteRequest()) {
            $site = \Pimcore\Model\Site::getCurrentSite();
            $siteId = $site->getId();
        }
        else{
            $siteId = 1;
        }

        $product = new DataObject\Product\Listing();
        $product->setCondition("category__id = 60");
        $product->load();

        $category = DataObject\ProductCategory::getById(60);

        $this->view->product = $product;
        $this->view->category = $category;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer(1);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT6;
    }

    public function educationAction()
    {
        if(\Pimcore\Model\Site::isSiteRequest()) {
            $site = \Pimcore\Model\Site::getCurrentSite();
            $siteId = $site->getId();
        }
        else{
            $siteId = 1;
        }
        
        $product = new DataObject\Product\Listing();
        $product->setCondition("category__id = 116");
        $product->load();

        $category = DataObject\ProductCategory::getById(116);

        $this->view->product = $product;
        $this->view->category = $category;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer(1);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT6;
    }
}