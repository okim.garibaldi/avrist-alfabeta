<?php

namespace AppBundle\Controller\LifeInsurance\Personal;

use AppBundle\Controller\DefaultController;
use Pimcore\Controller\FrontendController;
use Pimcore\Db;
use Pimcore\Model\DataObject\Product;
use function Sabre\Event\Loop\stop;
use Symfony\Component\HttpFoundation\Request;
use Pimcore\Model\DataObject;

class DetailController extends FrontendController
{
    /**
     * @param Request $request
     */
    public function defaultAction(Request $request)
    {
//        $db = Db::get();
//        $result = $db->fetchAll("SELECT * FROM avrist_sehati WHERE id=1");
//        $this->view->result = $result;
        $product = Product::getById(4);

        $this->view->product = $product;
    }

    public function detailAction(Request $request)
    {
//        $name = str_replace('-', ' ', $request->get('slug'));
//        $dataProduct = new Product\Listing();
//        $dataProduct->setCondition("name LIKE ?", "%". $name ."%");
//        $dataProduct->load();
//        $product = [];
//       foreach ($dataProduct as $p){
//           array_push($product, $p);
//       }
//
//        $this->view->product = $product[0];
    }

    //life
    public function avristInvestmentAction()
    {
        if(\Pimcore\Model\Site::isSiteRequest()) {
            $site = \Pimcore\Model\Site::getCurrentSite();
            $siteId = $site->getId();
        }
        else{
            $siteId = 1;
        }

        $product = Product::getById(46);
        $this->view->product = $product;

        $benefits = new DataObject\ProductBenefit\Listing();
        $benefits->setCondition("product__id = ". $product->getId());
        $this->view->benefits = $benefits;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer($siteId);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT1;
    }

    public function avristInvestmentPlusAction()
    {
        if(\Pimcore\Model\Site::isSiteRequest()) {
            $site = \Pimcore\Model\Site::getCurrentSite();
            $siteId = $site->getId();
        }
        else{
            $siteId = 1;
        }

        $product = Product::getById(49);

        $this->view->product = $product;

        $benefits = new DataObject\ProductBenefit\Listing();
        $benefits->setCondition("product__id = ". $product->getId());
        $this->view->benefits = $benefits;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer($siteId);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT1;
    }

    public function avristLinkPrioritasAction()
    {
        if(\Pimcore\Model\Site::isSiteRequest()) {
            $site = \Pimcore\Model\Site::getCurrentSite();
            $siteId = $site->getId();
        }
        else{
            $siteId = 1;
        }

        $product = Product::getById(50);

        $this->view->product = $product;

        $benefits = new DataObject\ProductBenefit\Listing();
        $benefits->setCondition("product__id = ". $product->getId());
        $this->view->benefits = $benefits;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer($siteId);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT1;
    }

    public function avristPrime88Action()
    {
        if(\Pimcore\Model\Site::isSiteRequest()) {
            $site = \Pimcore\Model\Site::getCurrentSite();
            $siteId = $site->getId();
        }
        else{
            $siteId = 1;
        }

        $product = Product::getById(51);

        $this->view->product = $product;

        $benefits = new DataObject\ProductBenefit\Listing();
        $benefits->setCondition("product__id = ". $product->getId());
        $this->view->benefits = $benefits;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer($siteId);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT1;
    }

    public function avristPrimeProtectionAction()
    {
        if(\Pimcore\Model\Site::isSiteRequest()) {
            $site = \Pimcore\Model\Site::getCurrentSite();
            $siteId = $site->getId();
        }
        else{
            $siteId = 1;
        }

        $product = Product::getById(52);

        $this->view->product = $product;

        $benefits = new DataObject\ProductBenefit\Listing();
        $benefits->setCondition("product__id = ". $product->getId());
        $this->view->benefits = $benefits;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer($siteId);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT1;
    }

    public function avristPrimegenAction()
    {
        if(\Pimcore\Model\Site::isSiteRequest()) {
            $site = \Pimcore\Model\Site::getCurrentSite();
            $siteId = $site->getId();
        }
        else{
            $siteId = 1;
        }

        $product = Product::getById(53);

        $this->view->product = $product;

        $benefits = new DataObject\ProductBenefit\Listing();
        $benefits->setCondition("product__id = ". $product->getId());
        $this->view->benefits = $benefits;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer($siteId);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT1;
    }

    public function avristTerm10Action()
    {
        if(\Pimcore\Model\Site::isSiteRequest()) {
            $site = \Pimcore\Model\Site::getCurrentSite();
            $siteId = $site->getId();
        }
        else{
            $siteId = 1;
        }

        $product = Product::getById(54);

        $this->view->product = $product;

        $benefits = new DataObject\ProductBenefit\Listing();
        $benefits->setCondition("product__id = ". $product->getId());
        $this->view->benefits = $benefits;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer($siteId);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT1;
    }

    public function avristTotalCareAction()
    {
        if(\Pimcore\Model\Site::isSiteRequest()) {
            $site = \Pimcore\Model\Site::getCurrentSite();
            $siteId = $site->getId();
        }
        else{
            $siteId = 1;
        }

        $product = Product::getById(55);

        $this->view->product = $product;

        $benefits = new DataObject\ProductBenefit\Listing();
        $benefits->setCondition("product__id = ". $product->getId());
        $this->view->benefits = $benefits;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer($siteId);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT1;
    }

    public function avristXtraAction()
    {
        if(\Pimcore\Model\Site::isSiteRequest()) {
            $site = \Pimcore\Model\Site::getCurrentSite();
            $siteId = $site->getId();
        }
        else{
            $siteId = 1;
        }

        $product = Product::getById(56);

        $this->view->product = $product;

        $benefits = new DataObject\ProductBenefit\Listing();
        $benefits->setCondition("product__id = ". $product->getId());
        $this->view->benefits = $benefits;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer($siteId);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT1;
    }

    //syariah
    public function avristAsyaLinkSyariahAction()
    {
        if(\Pimcore\Model\Site::isSiteRequest()) {
            $site = \Pimcore\Model\Site::getCurrentSite();
            $siteId = $site->getId();
        }
        else{
            $siteId = 1;
        }

        $product = Product::getById(57);

        $this->view->product = $product;

        $benefits = new DataObject\ProductBenefit\Listing();
        $benefits->setCondition("product__id = ". $product->getId());
        $this->view->benefits = $benefits;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer($siteId);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT1;
    }

    public function avristILinkSyariahSyariahAction()
    {
        if(\Pimcore\Model\Site::isSiteRequest()) {
            $site = \Pimcore\Model\Site::getCurrentSite();
            $siteId = $site->getId();
        }
        else{
            $siteId = 1;
        }

        $product = Product::getById(58);

        $this->view->product = $product;

        $benefits = new DataObject\ProductBenefit\Listing();
        $benefits->setCondition("product__id = ". $product->getId());
        $this->view->benefits = $benefits;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer($siteId);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT1;
    }

    public function avristInvesmentLinkSyariahAction()
    {
        if(\Pimcore\Model\Site::isSiteRequest()) {
            $site = \Pimcore\Model\Site::getCurrentSite();
            $siteId = $site->getId();
        }
        else{
            $siteId = 1;
        }

        $product = Product::getById(59);

        $this->view->product = $product;

        $benefits = new DataObject\ProductBenefit\Listing();
        $benefits->setCondition("product__id = ". $product->getId());
        $this->view->benefits = $benefits;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer($siteId);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT1;
    }

    //health
    public function avristBasicGuardAction()
    {
        if(\Pimcore\Model\Site::isSiteRequest()) {
            $site = \Pimcore\Model\Site::getCurrentSite();
            $siteId = $site->getId();
        }
        else{
            $siteId = 1;
        }

        $product = Product::getById(61);

        $this->view->product = $product;

        $benefits = new DataObject\ProductBenefit\Listing();
        $benefits->setCondition("product__id = ". $product->getId());
        $this->view->benefits = $benefits;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer($siteId);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT1;
    }

    public function avristCriticalGuardAction()
    {
        if(\Pimcore\Model\Site::isSiteRequest()) {
            $site = \Pimcore\Model\Site::getCurrentSite();
            $siteId = $site->getId();
        }
        else{
            $siteId = 1;
        }

        $product = Product::getById(62);

        $this->view->product = $product;

        $benefits = new DataObject\ProductBenefit\Listing();
        $benefits->setCondition("product__id = ". $product->getId());
        $this->view->benefits = $benefits;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer($siteId);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT1;
    }

    public function avristIncomeGuardAction()
    {
        if(\Pimcore\Model\Site::isSiteRequest()) {
            $site = \Pimcore\Model\Site::getCurrentSite();
            $siteId = $site->getId();
        }
        else{
            $siteId = 1;
        }

        $product = Product::getById(63);

        $this->view->product = $product;

        $benefits = new DataObject\ProductBenefit\Listing();
        $benefits->setCondition("product__id = ". $product->getId());
        $this->view->benefits = $benefits;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer($siteId);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT1;
    }

    public function avristSehatiAction(Request $request)
    {
        if(\Pimcore\Model\Site::isSiteRequest()) {
            $site = \Pimcore\Model\Site::getCurrentSite();
            $siteId = $site->getId();
        }
        else{
            $siteId = 1;
        }
        
        $product = Product::getById(64);

        $this->view->product = $product;

        $benefits = new DataObject\ProductBenefit\Listing();
        $benefits->setCondition("product__id = ". $product->getId());
        $this->view->benefits = $benefits;

        $age = (int)$request->get('age');
        $gender = $request->get('gender');
        if (isset($gender)) {
            $premiumFactor = new DataObject\PremiumFactor\Listing();
            $premiumFactor->setCondition("gender LIKE ?", "%". $gender ."%");
            $premiumFactor->load();
            foreach ($premiumFactor as $pf){
                $arrayAge = explode('-', $pf->getEntryAgeBand());
                if ($age <= (int)$arrayAge[1] && $age >= (int)$arrayAge[0]){
                    $premiFac = $pf;
                    break;
                }
            }
            $this->view->premiumFactor = $premiFac;
        } else {
            $premiumFactor = new DataObject\PremiumFactor\Listing();
            $premiumFactor->setCondition("product__id = " . $product->getId());
            $premiumFactor->setLimit(1);
            $premiumFactor->load();

            foreach ($premiumFactor as $pf) {
                $premiFac = $pf;
            }

            $this->view->premiumFactor = $premiFac;
        }

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer($siteId);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT1;
    }
}
