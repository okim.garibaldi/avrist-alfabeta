<?php

namespace AppBundle\Controller\LifeInsurance;

use Pimcore\Controller\FrontendController;
use Symfony\Component\HttpFoundation\Request;
use Pimcore\Model\DataObject;
use AppBundle\Controller\DefaultController;

class HomeController extends FrontendController
{
    public function homeAction()
    {
        if(\Pimcore\Model\Site::isSiteRequest()) {
            $site = \Pimcore\Model\Site::getCurrentSite();
            $siteId = $site->getId();
        }
        else{
            $siteId = 1;
        }
        
        //$siteCategory = DataObject\SiteCategory::getById(148);

        $siteCategories = new DataObject\SiteCategory\Listing();
        $siteCategories->setCondition("siteId = ?", $siteId);
        $siteCategories->load();

        foreach($siteCategories as $sC)
        {
            $siteCategory = $sC;
        }

        $this->view->siteCategory = $siteCategory;

        //$lifeGuidePage = new DataObject\LifeGuidePage\Listing();
        //$lifeGuidePage->setCondition("siteId = ?", $siteId);
        //$lifeGuidePage->load();
//
        //foreach ($lifeGuidePage as $lGP)
        //{
        //    $lifeGuide = new DataObject\Article\Listing();
        //    $lifeGuide->setCondition("category = ?", "lifeGuide");
        //    $lifeGuide->addConditionParam("siteId = ? ", $siteId, "AND");
        //    $lifeGuide->setOrderKey(["publishDate", "o_id"]);
        //    $lifeGuide->setOrder("desc");
        //    $lifeGuide->load();
//
        //    $this->view->lifeGuidePage = $lGP;
        //    $this->view->lifeGuide = $lifeGuide;
        //}

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer($siteId);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT1;
    }
}