<?php

namespace AppBundle\Controller\LifeInsurance;

use AppBundle\Controller\DefaultController;
use Pimcore\Controller\FrontendController;
use Symfony\Component\HttpFoundation\Request;
use \Pimcore\Model\DataObject;

class BusinessController extends FrontendController
{
    /**
     * @param Request $request
     */
    public function defaultAction(Request $request)
    {

    }

    public function employeeAction()
    {
        if(\Pimcore\Model\Site::isSiteRequest()) {
            $site = \Pimcore\Model\Site::getCurrentSite();
            $siteId = $site->getId();
        }
        else{
            $siteId = 1;
        }

        $product = new DataObject\Product\Listing();
        $product->setCondition("category__id = 68");
        $product->load();

        $category = DataObject\ProductCategory::getById(68);

        $this->view->product = $product;
        $this->view->category = $category;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer(1);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT1;
    }

    public function retirementAction()
    {
        if(\Pimcore\Model\Site::isSiteRequest()) {
            $site = \Pimcore\Model\Site::getCurrentSite();
            $siteId = $site->getId();
        }
        else{
            $siteId = 1;
        }
        
        $product = new DataObject\Product\Listing();
        $product->setCondition("category__id = 69");
        $product->load();

        $category = DataObject\ProductCategory::getById(69);

        $this->view->product = $product;
        $this->view->category = $category;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer(1);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT1;
    }

}