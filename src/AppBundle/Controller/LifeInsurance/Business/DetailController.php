<?php

namespace AppBundle\Controller\LifeInsurance\Business;

use Pimcore\Controller\FrontendController;
use Pimcore\Model\DataObject\Product;
use Symfony\Component\HttpFoundation\Request;
use Pimcore\Model\DataObject;
use AppBundle\Controller\DefaultController;

class DetailController extends FrontendController
{
    /**
     * @param Request $request
     */
    public function defaultAction(Request $request)
    {

    }

    //employee
    public function avristGHCSilverAction()
    {
        if(\Pimcore\Model\Site::isSiteRequest()) {
            $site = \Pimcore\Model\Site::getCurrentSite();
            $siteId = $site->getId();
        }
        else{
            $siteId = 1;
        }

        $product = Product::getById(70);

        $this->view->product = $product;

        $benefits = new DataObject\ProductBenefit\Listing();
        $benefits->setCondition("product__id = ". $product->getId());
        $this->view->benefits = $benefits;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer($siteId);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT1;
    }
    public function avristGHCSilverPlusAction()
    {
        if(\Pimcore\Model\Site::isSiteRequest()) {
            $site = \Pimcore\Model\Site::getCurrentSite();
            $siteId = $site->getId();
        }
        else{
            $siteId = 1;
        }

        $product = Product::getById(71);

        $this->view->product = $product;

        $benefits = new DataObject\ProductBenefit\Listing();
        $benefits->setCondition("product__id = ". $product->getId());
        $this->view->benefits = $benefits;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer($siteId);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT1;
    }
    public function avristStudentGuardAction()
    {
        if(\Pimcore\Model\Site::isSiteRequest()) {
            $site = \Pimcore\Model\Site::getCurrentSite();
            $siteId = $site->getId();
        }
        else{
            $siteId = 1;
        }

        $product = Product::getById(72);

        $this->view->product = $product;

        $benefits = new DataObject\ProductBenefit\Listing();
        $benefits->setCondition("product__id = ". $product->getId());
        $this->view->benefits = $benefits;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer($siteId);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT1;
    }
    public function avristSyariahGroupHealthCareAction()
    {
        if(\Pimcore\Model\Site::isSiteRequest()) {
            $site = \Pimcore\Model\Site::getCurrentSite();
            $siteId = $site->getId();
        }
        else{
            $siteId = 1;
        }

        $product = Product::getById(73);

        $this->view->product = $product;

        $benefits = new DataObject\ProductBenefit\Listing();
        $benefits->setCondition("product__id = ". $product->getId());
        $this->view->benefits = $benefits;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer($siteId);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT1;
    }
    public function avristSyariahStudentGuardAction()
    {
        if(\Pimcore\Model\Site::isSiteRequest()) {
            $site = \Pimcore\Model\Site::getCurrentSite();
            $siteId = $site->getId();
        }
        else{
            $siteId = 1;
        }

        $product = Product::getById(74);

        $this->view->product = $product;

        $benefits = new DataObject\ProductBenefit\Listing();
        $benefits->setCondition("product__id = ". $product->getId());
        $this->view->benefits = $benefits;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer($siteId);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT1;
    }

    //retirement/pensiun
    public function avristAsuransiCarePackAction()
    {
        if(\Pimcore\Model\Site::isSiteRequest()) {
            $site = \Pimcore\Model\Site::getCurrentSite();
            $siteId = $site->getId();
        }
        else{
            $siteId = 1;
        }

        $product = Product::getById(75);

        $this->view->product = $product;

        $benefits = new DataObject\ProductBenefit\Listing();
        $benefits->setCondition("product__id = ". $product->getId());
        $this->view->benefits = $benefits;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer($siteId);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT1;
    }
    public function avristCarePackProAction()
    {
        if(\Pimcore\Model\Site::isSiteRequest()) {
            $site = \Pimcore\Model\Site::getCurrentSite();
            $siteId = $site->getId();
        }
        else{
            $siteId = 1;
        }

        $product = Product::getById(76);

        $this->view->product = $product;

        $benefits = new DataObject\ProductBenefit\Listing();
        $benefits->setCondition("product__id = ". $product->getId());
        $this->view->benefits = $benefits;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer($siteId);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT1;
    }
    public function avristDanaPensiunLembagaKeuanganAction()
    {
        if(\Pimcore\Model\Site::isSiteRequest()) {
            $site = \Pimcore\Model\Site::getCurrentSite();
            $siteId = $site->getId();
        }
        else{
            $siteId = 1;
        }

        $product = Product::getById(77);

        $this->view->product = $product;

        $benefits = new DataObject\ProductBenefit\Listing();
        $benefits->setCondition("product__id = ". $product->getId());
        $this->view->benefits = $benefits;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer($siteId);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT1;
    }
}