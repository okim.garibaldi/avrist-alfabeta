<?php

namespace AppBundle\Controller\LifeInsurance;

use Pimcore\Controller\FrontendController;
use stdClass;
use Symfony\Component\HttpFoundation\Request;
use \Pimcore\Model\DataObject;

class InformationController extends FrontendController
{
    /**
     * @param Request $request
     */
    public function defaultAction(Request $request)
    {

    }

    public function rumahSakitRekananAction()
    {
        $hospital = [];
        $temp = [];
        $data = new DataObject\HospitalProvider\Listing();
        $data->load();

        foreach ($data as $hospitalprovider)
        {
            if($hospitalprovider instanceof \Pimcore\Model\DataObject\HospitalProvider){
                $input = new StdClass();
                $input->coordinate = $hospitalprovider->getCoordinate();
                $input->category_name = $hospitalprovider->getName();
                $input->address = $hospitalprovider->getAddress();
                $input->region_name = $hospitalprovider->getRegion();
                $input->category_code = $hospitalprovider->getCategory();
                $input->phone = $hospitalprovider->getPhone();
                $input->link = $hospitalprovider->getLink();
                $temp[$hospitalprovider->getCategory()][] = $input;

            }
        }

        $hospital["list"] = $temp;
    }

}