<?php

namespace AppBundle\Controller\Location;

use Carbon\Carbon;
use AppBundle\Controller\DefaultController;
use Pimcore\Controller\FrontendController;
use Symfony\Component\HttpFoundation\Request;
use \Pimcore\Model\DataObject;

class LocationController extends FrontendController
{
    public function ourLocationIndexAction(Request $request)
    {
        LocationController::location("ourLocation", $request);

        $this->view->locationSearch = True;

        //$url = "https://www.googleapis.com/geolocation/v1/geolocate?key=AIzaSyChTxbt2T197EdUM9mdtAq_Sx7u7XujeyY";
        //$ch = curl_init( $url );
        //# Setup request to send json via POST.
        //$payload = json_encode( array() );
        //curl_setopt( $ch, CURLOPT_POSTFIELDS, $payload );
        //curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        //# Return response instead of printing.
        //curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        //# Send request.
        //$result = curl_exec($ch);
        //curl_close($ch);
        //# Print response.
        //echo $result;

        //$context  = stream_context_create($opts);
//
        //echo json_encode(file_get_contents(''), true);
    }

    public function hospitalLocationIndexAction(Request $request)
    {
        LocationController::location("RSRekanan", $request);

        $this->view->locationSearch = True;
    }

    public function bengkelLocationIndexAction(Request $request)
    {
        LocationController::location("bengkelRekanan", $request);

        $this->view->locationSearch = True;
    }

    public function location($pageCategory, $request)
    {
    	//echo $request->request->has('branchFilter')." ";
    	//echo gettype($request->get('branchFilter'))." ";
    	//echo $request->get('branchFilter')." ";
    	//echo $request->request->has('filterBengkel')." ";
    	//echo gettype($request->get('filterBengkel'))." ";
    	//echo $request->get('filterBengkel')." ";
    	//echo $request->request->has('groupFilter')." ";
    	//echo gettype($request->get('groupFilter'))." ";
    	//echo $request->get('groupFilter')." ";
    	//echo $request->request->has('hospitalCategory')." ";
    	//echo gettype($request->get('hospitalCategory'))." ";
    	//echo $request->get('hospitalCategory')." ";

        if(\Pimcore\Model\Site::isSiteRequest()) {
            $site = \Pimcore\Model\Site::getCurrentSite();
            $siteId = $site->getId();
        }
        else {
            $siteId = 1;
        }
        

    	$location = new DataObject\Location();
        $options['groups'] = $location->getClass()->getFieldDefinition("groupsFilter")->getOptions();
        $options['hospitalCategories'] = $location->getClass()->getFieldDefinition("hospitalCategory")->getOptions();
        $options['filterBengkel'] = $location->getClass()->getFieldDefinition("filterBengkel")->getOptions();
        $options['branches'] = $location->getClass()->getFieldDefinition("cabang")->getOptions();

        $this->view->options = $options;

    	if ($request->request->has('branchFilter') || $request->request->has('filterBengkel') || $request->request->has('groupFilter') || $request->request->has('hospitalCategory') || $request->request->has('s'))
    	{
    		$branchFilter = $request->get('branchFilter');
    		$filterBengkel = $request->get('filterBengkel');
    		$groupFilter = $request->get('groupFilter');
    		$hospitalCategoryFilter = $request->get('hospitalCategory');
    		$s = "%".$request->get('s')."%";
    	}
    	else
    	{
    		if($pageCategory == "ourLocation")
    		{
    			$branchFilter = $options['branches'][0]['value'];
    		}
    		else
    		{
    			$branchFilter = "NULL";
    		}
    		
    		if($pageCategory == "bengkelRekanan")
    		{
    			$filterBengkel =  $options['filterBengkel'][0]['value'];
    		}
    		else
    		{
    			$filterBengkel = "NULL";
    		}

    		if($pageCategory == "RSRekanan")
    		{
    			$groupFilter = $options['groups'][0]['value'];
				$hospitalCategoryFilter = $options['hospitalCategories'][0]['value'];
    		}
    		else
    		{
    			$groupFilter = "NULL";
				$hospitalCategoryFilter = "NULL";	
    		}

			$s = "%";
    	}

        $this->view->branchFilter = $branchFilter;
        $this->view->filterBengkel = $filterBengkel;
        $this->view->hospitalCategoryFilter = $hospitalCategoryFilter;
        $this->view->groupFilter = $groupFilter;
        

        $objs1 = new DataObject\LocationPage\Listing();
        $objs1->setCondition("siteId = ?", $siteId);
        $objs1->addConditionParam("category = ?", $pageCategory, "AND");
        $objs1->load();

        if ($pageCategory == "ourLocation") {
            $pageCategory = $pageCategory.$siteId;
        }
        $this->view->pageCategory = $pageCategory;

        foreach($objs1 as $obj1)
        {
            $locationPage = $obj1;
        }

        $this->view->locationPage = $locationPage;

        $bgBanner = DefaultController::getBgBanner($siteId);

        $this->view->bgBanner = $bgBanner;

        $locations = new DataObject\Location\Listing();
        $locations->setCondition("o_parentId = ?", $locationPage->getId());
        $locations->addConditionParam("(name LIKE ? OR address LIKE ?)", [$s, $s], "AND");
        if($branchFilter != "NULL")
        {
        	$locations->addConditionParam("cabang = ?", $branchFilter, "AND");
        }
        if($filterBengkel != "NULL")
        {
        	$locations->addConditionParam("filterBengkel = ?", $filterBengkel, "AND");
        }
        if($groupFilter != "NULL" && $hospitalCategoryFilter != "NULL")
        {
        	$locations->addConditionParam("groupsFilter = ?", $groupFilter, "AND");
        	$locations->addConditionParam("hospitalCategory = ?", $hospitalCategoryFilter, "AND");
        }
        $locations->load();

        $coordinates = [];

        foreach($locations as $location)
        {
            array_push($coordinates, [
                'id' => $location->getId(),
                'image' => strval($location->getImage()),
                'name' => $location->getName(),
                'address' => $location->getAddress(),
                'latitude' => $location->getCoordinate()->getLatitude(), 
                'longitude' => $location->getCoordinate()->getLongitude(),
                'phone' => $location->getPhone(),
                'fax' => $location->getFax(),
                'email' => $location->getEmail(),
                'distance' => ''
            ]);
        }

        $this->view->coordinates = $coordinates;

        $locationMessage = new DataObject\LocationMessage();
        $fd = $locationMessage->getClass()->getFieldDefinition("topic");
        $subjectOptions = $fd->getOptions();

        $this->view->subjectOptions = $subjectOptions;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer($siteId);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT5;

        $this->view->googleMap = TRUE;

        $this->view->googleRecaptcha = True;
    }
    public function locationMessageAction(Request $request)
    {
        $requestId = $request->get('id');

        $location = DataObject\Location::getById($requestId);

        $locationMessage = new DataObject\LocationMessage();
        $locationMessage->setParentId($location->getId());
        $locationMessage->setKey(Carbon::now() . ' - ' . $request->get('name'));
        $locationMessage->setName($request->get('name'));
        $locationMessage->setEmail($request->get('email'));
        $locationMessage->setPhoneNumber($request->get('telphone'));
        $locationMessage->setTopic($request->get('topic'));
        $locationMessage->setMessage($request->get('message'));
        $locationMessage->setPublished(true);
        $locationMessage->save();

        $mail = new \Pimcore\Mail("Pesan untuk ".$request->get('locationName'));
        $mail->addTo($request->get('locationEmail'));
        $body = 
        "Pada ".Carbon::now().", ada pesan masuk dari: <br>".
        "<ul>
            <li>Nama : ".$request->get('name')."</li>
            <li>Email : ".$request->get('email')."</li>
            <li>Telphone : ".$request->get('telphone')./*"</li>
            <li>Category : ".$request->get('categoryPage').*/"</li>
            <li>Target Lokasi : ".$request->get('locationName')."</li>
            <li>Topik : ".$request->get('topic')."</li>
            <li>Pesan : ".$request->get('message')."</li>
        </ul><br><br>"; 

        $mail->setBodyHtml($body);
        $mail->send();

        return $this->redirect($request->get('url'));
    }
}
