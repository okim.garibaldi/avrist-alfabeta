<?php

namespace AppBundle\Controller\ContactUs;

use Pimcore\Controller\FrontendController;
use Symfony\Component\HttpFoundation\Request;
use Pimcore\Model\DataObject;
use AppBundle\Controller\DefaultController;

class ContactUsController extends FrontendController
{
    public function defaultAction()
    {
        if(\Pimcore\Model\Site::isSiteRequest()) {
            $site = \Pimcore\Model\Site::getCurrentSite();
            $siteId = $site->getId();
        }
        else{
            $siteId = 1;
        }
        
        $objs1 = new DataObject\ContactPage\Listing();
        $objs1->setCondition("siteId = ?", $siteId);
        $objs1->load();

        foreach ($objs1 as $obj1)
        {
            $contactUs = $obj1;
        }

        $this->view->contactUs = $contactUs;

        $objs2 = new DataObject\Listing();
        $objs2->setCondition("o_parentId = ?", $contactUs->getId());
        $objs2->addConditionParam("o_key = ?", "Group1", "AND");
        $objs2->load();

        foreach ($objs2 as $obj2)
        {
            $obj2Id = $obj2->getId();
        }

        $group1 = new DataObject\Listing();
        $group1->setCondition("o_parentId = ?", $obj2Id);
        $group1->setOrderKey("o_index");
        $group1->setOrder("asc");
        $group1->load();

        $this->view->group1 = $group1;

        $objs3 = new DataObject\Listing();
        $objs3->setCondition("o_parentId = ?", $contactUs->getId());
        $objs3->addConditionParam("o_key = ?", "Group2", "AND");
        $objs3->load();

        foreach ($objs3 as $obj3)
        {
            $obj3Id = $obj3->getId();
        }

        $group2 = new DataObject\Listing();
        $group2->setCondition("o_parentId = ?", $obj3Id);
        $group2->setOrderKey("o_index");
        $group2->setOrder("asc");
        $group2->load();

        $this->view->group2 = $group2;

    }

    public function indexAction()
    {
        if(\Pimcore\Model\Site::isSiteRequest()) {
            $site = \Pimcore\Model\Site::getCurrentSite();
            $siteId = $site->getId();
        }
        else{
            $siteId = 1;
        }

        $bgBanner = DefaultController::getBgBanner($siteId);

        $this->view->bgBanner = $bgBanner;

        $objs1 = new DataObject\ContactPage\Listing();
        $objs1->setCondition("siteId = ?", $siteId);
        $objs1->load();

        foreach ($objs1 as $obj1)
        {
            $contactUs = $obj1;
        }

        $this->view->contactUs = $contactUs;


        foreach($contactUs->getBlock1() as $block1)
        {
            $object1 = \Pimcore\Model\DataObject::getByPath($block1['page']->getData());
            $options[$object1->getName()] = $object1;
        }

        $this->view->options = $options;

        foreach($contactUs->getBlock2() as $block2)
        {
            $object2 = DataObject::getByPath($block2['page']->getData());
            $socialMedia[$object2->getName()] = $object2;
        }

        $this->view->socialMedia = $socialMedia;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer($siteId);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT5;
    }
}
