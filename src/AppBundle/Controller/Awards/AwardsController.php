<?php

namespace AppBundle\Controller\Awards;

use AppBundle\Controller\DefaultController;
use Pimcore\Controller\FrontendController;
use Symfony\Component\HttpFoundation\Request;
use \Pimcore\Model\DataObject;

class AwardsController extends FrontendController
{
    public function indexAction()
    {

    	if(\Pimcore\Model\Site::isSiteRequest()) {
            $site = \Pimcore\Model\Site::getCurrentSite();
            $siteId = $site->getId();
        }
        else{
            $siteId = 1;
        }

        $bgBanner = DefaultController::getBgBanner($siteId);

        $objs1 = new DataObject\AwardsPage\Listing();
        $objs1->setCondition("siteId = ?", $siteId);
        $objs1->load();

        foreach($objs1 as $obj1)
        {
            $awardsPage = $obj1;
        }

        $objs2 = new DataObject\Listing();
        $objs2->setCondition("o_parentId = ?", $awardsPage->getId());
        $objs2->load();

        $yearId = "(";

        foreach($objs2 as $obj2)
        {
            $yearId .= $obj2->getId().",";
        }

        $yearId = substr($yearId, 0, -1).")";

        $awards = new DataObject\Article\Listing();
        $awards->setCondition("o_parentId IN ".$yearId);
        $awards->setOrderKey(["publishDate", "o_id"]);
        $awards->setOrder("desc");
        $awards->load();

        $objs3 = new DataObject\AboutCompany\Listing();
        $objs3->setCondition("siteId = ?", $siteId);
        $objs3->load();

        foreach($objs3 as $obj3)
        {
            $aboutCompany = $obj3;
        }

        $this->view->awardsPage = $awardsPage;

        $this->view->awards = $awards;

        $this->view->aboutCompany = $aboutCompany;

        $this->view->bgBanner = $bgBanner;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer($siteId);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT3;
    }

    public function detailAction(Request $request)
    {
    	if(\Pimcore\Model\Site::isSiteRequest()) {
            $site = \Pimcore\Model\Site::getCurrentSite();
            $siteId = $site->getId();
        }
        else{
            $siteId = 1;
        }
        $titlePath = $request->get('path');

        $bgBanner = DefaultController::getBgBanner($siteId);

        $objs1 = new DataObject\AwardsPage\Listing();
        $objs1->setCondition("siteId = ?", $siteId);
        $objs1->load();

        foreach($objs1 as $obj1)
        {
            $awardsPage = $obj1;
        }

        $awards = DataObject\Article::getByPath($awardsPage->getFullPath()."/".$titlePath);

        $otherAwards = new DataObject\Article\Listing();
        $otherAwards->setCondition("o_parentId = ?", $awards->getParentId());
        $otherAwards->addConditionParam("o_Id != ?", $awards->getId(), "AND");
        $otherAwards->setOrderKey(["publishDate", "o_id"]);
        $otherAwards->setOrder("desc");
        $otherAwards->load();

        $objs2 = new DataObject\AboutCompany\Listing();
        $objs2->setCondition("siteId = ?", $siteId);
        $objs2->load();

        foreach($objs2 as $obj2)
        {
            $aboutCompany = $obj2;
        }

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer($siteId);

        return $this->render(":Awards:details.html.php", 
        [
            "navbar" => $navbar, 
            "footer" => $footer,
            "aboutCompany" => $aboutCompany,
            "awardsPage" => $awardsPage,
            "awards" => $awards,
            "otherAwards" => $otherAwards,
            "bgBanner" => $bgBanner,
            "script" => DefaultController::SCRIPT2
        ]);
    }

}
