<?php

namespace AppBundle\Controller\Formulir;

use Carbon\Carbon;
use AppBundle\Controller\DefaultController;
use Pimcore\Controller\FrontendController;
use Symfony\Component\HttpFoundation\Request;
use \Pimcore\Model\DataObject;


class FormulirController extends FrontendController
{
    public function formulirLayananAction(Request $request)
    {
        if(\Pimcore\Model\Site::isSiteRequest()) {
            $site = \Pimcore\Model\Site::getCurrentSite();
            $siteId = $site->getId();
        }
        else{
            $siteId = 1;
        }

        $path = $request->get('path');

        $formulirLayanan = DataObject\FormulirLayanan::getByPath("/".$path);

        foreach($formulirLayanan->getBlock() as $block)
        {
            $file = [];
            foreach($block['subblock']->getData() as $subblock)
            {
                array_push($file, [
                    'name' =>  $subblock['name']->getData(),
                    'file' => \Pimcore\Model\Asset::getByPath($subblock['file']->getData())]);
            }
            $arr[$block['group']->getData()] = $file;
        }

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer($siteId);

        return $this->render(":Formulir:layanan.html.php", 
        [
            "formulirLayanan" => $formulirLayanan,
            "arr" => $arr,
            "navbar" => $navbar, 
            "footer" => $footer,
            "script" => DefaultController::SCRIPT1
        ]);
    }

    public function formulirPembukaanIndexAction(Request $request)
    {
        if(\Pimcore\Model\Site::isSiteRequest()) {
            $site = \Pimcore\Model\Site::getCurrentSite();
            $siteId = $site->getId();
        }
        else{
            $siteId = 1;
        }

        $bgBanner = DefaultController::getBgBanner($siteId);

        $this->view->bgBanner = $bgBanner;

        if($request->request->has('s'))
        {
            $s = "/".$request->get('s')."/i";
        }
        else
        {
            $s = "/.+/i";
        }

        $objs1 = new DataObject\FormulirPembukaanPage\Listing();
        $objs1->setCondition("siteId = ?", $siteId);
        $objs1->load();

        foreach($objs1 as $obj1)
        {
            $formulirPembukaanPage = $obj1;
        }

        $this->view->formulirPembukaanPage = $formulirPembukaanPage;

        $files = [];

        foreach($formulirPembukaanPage->getBlock() as $block)
        {
            if (preg_match($s, $block['name']->getData()))
            {
                array_push($files, [
                    'name' => $block['name']->getData(), 
                    'file' => \Pimcore\Model\Asset::getByPath($block['file']->getData())
                ]);
            }
        }

        $this->view->files = $files;

        $this->view->search = True;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer($siteId);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT5;
    }
}