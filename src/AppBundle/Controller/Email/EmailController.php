<?php

namespace AppBundle\Controller\Email;

use AppBundle\Controller\DefaultController;
use Pimcore\Controller\FrontendController;
use Symfony\Component\HttpFoundation\Request;
use \Pimcore\Model\DataObject;


class EmailController extends FrontendController
{
    public function sendAction()
    {
        $mail = new \Pimcore\Mail("test subject");
        //$mail->addTo('mkemalw@gmail.com');
        $mail->addTo('ryankurniawanrk27@gmail.com');
        $mail->setBodyHtml("124");
        $mail->send();
    }

    public function emptySendAction()
    {

    }

    public function sendSummaryEmailAction(Request $request)
    {
        $dataGetSolution = DataObject::getById($request->get("s"));

        
        if($dataGetSolution->getClassName() == "GetSolution")
        {
            $mail = new \Pimcore\Mail("Solution Personal");
            $mail->addTo('ryankurniawanrk27@gmail.com');
            $body = 
            "Saya yang bernama ".$dataGetSolution->getName()." ingin mencari Solusi lengkap untuk ".$dataGetSolution->  getSolutionFor()." untuk proteksi ".$dataGetSolution->getSolutionNeeds()." dengan kategori ".$dataGetSolution->getCategory().". 
            <br>"."Berikut adalah data yang telah saya isi: <br>".
            "<ul>
                <li>Name : ".$dataGetSolution->getName()."</li>
                <li>Age : ".$dataGetSolution->getAge()."</li>
                <li>Telp : ".$dataGetSolution->getTelp()."</li>
                <li>Email : ".$dataGetSolution->getEmail()."</li>
                <li>Solution Type : ".$dataGetSolution->getSolutionType()."</li>
                <li>Solution Needs : ".$dataGetSolution->getSolutionNeeds()."</li>
                <li>Submit At : ".$dataGetSolution->getSubmitAt()."</li>
                <li>Vehicle Type : ".$dataGetSolution->getVehicleType()."</li>
                <li>Vehicle Production Year : ".$dataGetSolution->getProductionYear()."</li>
                <li>Plate : ".$dataGetSolution->getPlate()."</li>
                <li>Vehicle Price : ".$dataGetSolution->getVehiclePrice()."</li>
                <li>Property City : ".$dataGetSolution->getPropertyCity()."</li>
                <li>Property Function : ".$dataGetSolution->getPropertyFunction()."</li>
            </ul><br><br>".
            "Hormat saya, <br>".
            $dataGetSolution->getName(); 
        }
        //elseif($dataGetSolution->getClassName() == "GetSolutionBisnis")
        elseif($dataGetSolution->getClassName() == "GetSolutionBisnis")
        {
            $mail = new \Pimcore\Mail("Solution Bisnis");
            $mail->addTo('ryankurniawanrk27@gmail.com');
            $body = "Saya yang bernama ".$dataGetSolution->getName()." ingin mencari Solusi lengkap untuk proteksi ".$dataGetSolution->getCategory().". 
            <br>"."Berikut adalah data yang telah saya isi: <br>".
            "<ul>
                <li>Name : ".$dataGetSolution->getName()."</li>
                <li>Category : ".$dataGetSolution->getCategory()."</li>
                <li>PIC : ".$dataGetSolution->getPic()."</li>
                <li>Telp : ".$dataGetSolution->getTelp()."</li>
                <li>Email : ".$dataGetSolution->getEmail()."</li>
                <li>Field : ".$dataGetSolution->getField()."</li>
                <li>Submit At : ".$dataGetSolution->getsubmitAt()."</li>
            </ul><br><br>".
            "Hormat saya, <br>".
            $dataGetSolution->getName();
        }
        

        $mail->setBodyHtml($body);
        $mail->send();

        $dataGetSolution->setPublished(true);
        $dataGetSolution->save();

        return $this->redirect("/");
    }
}
