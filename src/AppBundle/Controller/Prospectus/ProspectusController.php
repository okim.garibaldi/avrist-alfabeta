<?php

namespace AppBundle\Controller\Prospectus;

use Carbon\Carbon;
use AppBundle\Controller\DefaultController;
use Pimcore\Controller\FrontendController;
use Symfony\Component\HttpFoundation\Request;
use \Pimcore\Model\DataObject;

class ProspectusController extends FrontendController
{
    public function indexAction(Request $request)
    {
        if($request->request->has('s'))
        {
            $s = "/".$request->get('s')."/i";
        }
        else
        {
            $s = "/.+/i";
        }

        $site = \Pimcore\Model\Site::getCurrentSite();
        $siteId = $site->getId();

        $objs1 = new DataObject\ProspectusPage\Listing();
        $objs1->setCondition("siteId = ?", $siteId);
        $objs1->load();

        foreach($objs1 as $obj1)
        {
            $prospectus = $obj1;
        }

        $files = [];
        foreach($prospectus->getBlock1() as $block1)
        {
            if (preg_match($s, $block1['name']->getData()))
            {
                array_push($files, [
                    'name' => $block1['name']->getData(),
                    'file' => \Pimcore\Model\Asset::getByPath($block1['file']->getData())
                ]);
            }
        }

        $this->view->search = True;

        $this->view->prospectus = $prospectus;

        $this->view->files = $files;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer($siteId);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT3;
    }

}
