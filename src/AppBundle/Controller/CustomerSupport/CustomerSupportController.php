<?php

namespace AppBundle\Controller\CustomerSupport;

use AppBundle\Controller\DefaultController;
use Pimcore\Controller\FrontendController;
use Symfony\Component\HttpFoundation\Request;
use \Pimcore\Model\DataObject;
use Carbon\Carbon;


class CustomerSupportController extends FrontendController
{
    public function indexAction(Request $request)
    {
        if(\Pimcore\Model\Site::isSiteRequest()) {
            $site = \Pimcore\Model\Site::getCurrentSite();
            $siteId = $site->getId();
        }
        else{
            $siteId = 1;
        }


        $bgBanner = DefaultController::getBgBanner($siteId);

        $this->view->bgBanner = $bgBanner;

        $objs1 = new DataObject\CustomerSupport\Listing();
        $objs1->setCondition("siteId = ?", $siteId);
        $objs1->load();

        foreach($objs1 as $obj1)
        {
            $customerSupport = $obj1;
        }

        $this->view->customerSupport = $customerSupport;

        $customerSupportMessage = new DataObject\CustomerSupportMessage();
        $topikFd = $customerSupportMessage->getClass()->getFieldDefinition("topik");
        $topikOptions = $topikFd->getOptions();

        $this->view->topikOptions = $topikOptions;

        $timeFd = $customerSupportMessage->getClass()->getFieldDefinition("time");
        $timeOptions = $timeFd->getOptions();

        $this->view->timeOptions = $timeOptions;

        if($request->request->has('name')) {
            $customerSupportMessage = new DataObject\CustomerSupportMessage();
            $customerSupportMessage->setParentId($customerSupport->getId());
            $customerSupportMessage->setKey(Carbon::now() . ' - ' . $request->get('name'));
            $customerSupportMessage->setName($request->get('name'));
            $customerSupportMessage->setEmail($request->get('email'));
            $customerSupportMessage->setPhoneNumber($request->get('phoneNumber'));
            $customerSupportMessage->setTopik($request->get('topik'));
            $customerSupportMessage->setTime($request->get('time'));
            $customerSupportMessage->setMessage($request->get('message'));
            $customerSupportMessage->setPublished(true);
            $customerSupportMessage->save();
        }

        $this->view->customerSupportMessage = True;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer($siteId);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT5;
    }
}
