<?php

namespace AppBundle\Controller\LifeGuide;

use AppBundle\Controller\DefaultController;
use Pimcore\Controller\FrontendController;
use Symfony\Component\HttpFoundation\Request;
use \Pimcore\Model\DataObject;


class LifeGuideController extends FrontendController
{
    public function indexAction()
    {
        $site = \Pimcore\Model\Site::getCurrentSite();
        $siteId = $site->getId();

        $bgBanner = DefaultController::getBgBanner($siteId);

        $objs1 = new DataObject\LifeGuidePage\Listing();
        $objs1->setCondition("siteId = ?", $siteId);
        $objs1->load();

        foreach($objs1 as $obj1)
        {
            $lifeGuidePage = $obj1;
        }

        $objs2 = new DataObject\Listing();
        $objs2->setCondition("o_parentId = ?", $lifeGuidePage->getId());
        $objs2->load();

        $yearId = "(";

        foreach($objs2 as $obj2)
        {
            $yearId .= $obj2->getId().",";
        }

        $yearId = substr($yearId, 0, -1).")";

        $lifeGuide = new DataObject\Article\Listing();
        $lifeGuide->setCondition("o_parentId IN ".$yearId);
        $lifeGuide->setOrderKey(["publishDate", "o_id"]);
        $lifeGuide->setOrder("desc");
        $lifeGuide->load();

        $this->view->lifeGuidePage = $lifeGuidePage;

        $this->view->lifeGuide = $lifeGuide;

        $this->view->bgBanner = $bgBanner;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer($siteId);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT2;
    }

    public function detailAction(Request $request)
    {
        $site = \Pimcore\Model\Site::getCurrentSite();
        $siteId = $site->getId();
        $path = $request->get('path');

        $bgBanner = DefaultController::getBgBanner($siteId);

        $lifeGuide = DataObject\Article::getByPath("/Life-guide/".$path);

        $otherLifeGuide = new DataObject\Article\Listing();
        $otherLifeGuide->setCondition("o_parentId = ?", $lifeGuide->getParentId());
        $otherLifeGuide->addConditionParam("o_Id != ?", $lifeGuide->getId(), "AND");
        $otherLifeGuide->setOrderKey(["publishDate", "o_id"]);
        $otherLifeGuide->setOrder("desc");
        $otherLifeGuide->load();

        $objs2 = new DataObject\AboutCompany\Listing();
        $objs2->setCondition("siteId = ?", $siteId);
        $objs2->load();

        foreach($objs2 as $obj2)
        {
            $aboutCompany = $obj2;
        }

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer($siteId);

        return $this->render(":Life-Guide:details.html.php", 
        [
            "navbar" => $navbar, 
            "footer" => $footer,
            "aboutCompany" => $aboutCompany,
            "lifeGuide" => $lifeGuide,
            "otherLifeGuide" => $otherLifeGuide,
            "bgBanner" => $bgBanner,
            "script" => DefaultController::SCRIPT2
        ]);
    }
}
