<?php

namespace AppBundle\Controller\FinancialReport;

use AppBundle\Controller\DefaultController;
use Pimcore\Controller\FrontendController;
use Symfony\Component\HttpFoundation\Request;
use Pimcore\Model\DataObject;

class FinancialReportController extends FrontendController
{
    public function lifeInsuranceAction(Request $request)
    {

    	$db = \Pimcore\Db::get();
		$year = $db->fetchCol("SELECT DISTINCT year FROM object_query_15 ORDER BY year DESC");
		$this->view->year = $year;

		foreach ($year as $y)
		{
			$data = new \Pimcore\Model\DataObject\FinancialReport\Listing();
			$data->setCondition("year = ?", $y);
    		$data->load();
    		$yd[$y] = $data;
		}	
		$this->view->yd = $yd;
    }

    public function indexAction()
    {
    	if(\Pimcore\Model\Site::isSiteRequest()) {
            $site = \Pimcore\Model\Site::getCurrentSite();
            $siteId = $site->getId();
        }
        else{
            $siteId = 1;
        }

        $bgBanner = DefaultController::getBgBanner($siteId);

        $this->view->bgBanner = $bgBanner;

        $objs1 = new DataObject\FinancialReportPage\Listing();
        $objs1->setCondition("siteId = ?", $siteId);
        $objs1->load();

        foreach($objs1 as $obj1)
        {
            $financialReportPage = $obj1;
        }

        $objs2 = new DataObject\Listing();
        $objs2->setCondition("o_parentId = ?", $financialReportPage->getId());
        $objs2->load();

        foreach($objs2 as $obj2)
        {
        	$objs3 = new DataObject\Listing();
        	$objs3->setCondition("o_parentId = ?", $obj2->getId());
        	$objs3->load();

        	foreach($objs3 as $obj3)
        	{
        		$objs4 = new DataObject\FinancialReport\Listing();
        		$objs4->setCondition("o_parentId = ?", $obj3->getId());
        		$objs4->load();

                $files = [];
                foreach($objs4 as $obj4)
                {
                    array_push($files, [
                        "title" => $obj4->getpdfTitle(),
                        "file" => \Pimcore\Model\Asset::getByPath($obj4->getpdf())
                    ]);
                      
                }
                $yearElementArr[$obj3->getKey()] = $files;
        	}

        	$financialReport[$obj2->getKey()] = $yearElementArr;
        }
        
        $objs5 = new DataObject\AboutCompany\Listing();
        $objs5->setCondition("siteId = ?", $siteId);
        $objs5->load();

        foreach($objs5 as $obj5)
        {
            $aboutCompany = $obj5;
        }

        $this->view->financialReportPage = $financialReportPage;

        $this->view->financialReport = $financialReport;

        $this->view->aboutCompany = $aboutCompany;

		$navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer($siteId);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT1;
    }
}
