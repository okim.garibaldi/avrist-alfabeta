<?php

namespace AppBundle\Controller\About;

use Pimcore\Controller\FrontendController;
use Symfony\Component\HttpFoundation\Request;
use Pimcore\Model\DataObject;
use AppBundle\Controller\DefaultController;

class AboutController extends FrontendController
{
    public function companyAboutAction()
    {
        if(\Pimcore\Model\Site::isSiteRequest()) {
            $site = \Pimcore\Model\Site::getCurrentSite();
            $siteId = $site->getId();
        }
        else{
            $siteId = 1;
        }

        $bgBanner = DefaultController::getBgBanner($siteId);

        $this->view->bgBanner = $bgBanner;
        
        $about = new DataObject\AboutCompany\Listing();
        $about->setCondition("siteId = ?", $siteId);
        $about->load();

        if($siteId == 1)
        {
            $objs1 = new DataObject\CsrPage\Listing();
            $objs1->setCondition("siteId = ?", $siteId);
            $objs1->load();
        }
        else
        {
            $objs1 = new DataObject\NewsPage\Listing();
            $objs1->setCondition("siteId = ?", $siteId);
            $objs1->load();  
        }
        

        foreach($objs1 as $obj1)
        {
            $csrPage = $obj1;
        }

        $csr = new DataObject\Article\Listing();
        $csr->setCondition("siteId = ?", $siteId);
        if ($siteId == 1)
        {
            $csr->addConditionParam("category = ?", "csr", "AND");
        }
        else
        {
            $csr->addConditionParam("category = ?", "news", "AND");
        }
        $csr->setOrderKey(["publishDate", "o_id"]);
        $csr->setOrder("desc");
        $csr->load();

        $this->view->about = $this->getObject($about);

        $this->view->csrPage = $csrPage;

        $this->view->csr = $csr;

        $overflow = True;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer($siteId);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT9;

    }

    public function getId($dataObject)
    {
        foreach($dataObject as $dO)
        {
            return $dO->getId();
        }
    }

    public function getObjects($id, $key = NULL)
    {
        $obj = new DataObject\Listing();
        $obj->setCondition("o_parentId = ?", $id);
        if($key != NULL)
        {
            $obj->addConditionParam("o_key = ?", $key, "AND");
        }
        $obj->load();
        
        return $obj;

        
    }

    public function getObject($objects)
    {
        foreach($objects as $obj)
        {
            return $obj;
        }
    }
}
