<?php

namespace AppBundle\Controller;

use Carbon\Carbon;
use Pimcore\Controller\FrontendController;
use Symfony\Component\HttpFoundation\Request;
use Pimcore\Model\DataObject;

class AskTheExpertController extends FrontendController
{
    public function submitAction(Request $request)
    {
        $data = new DataObject\AskTheExpert();
        $data->setParentId(181);
        $data->setKey(Carbon::now() . ' - ' . $request->get('name'));
        $data->setName($request->get('name'));
        $data->setEmail($request->get('email'));
        $data->setMessage($request->get('message'));
        $data->setSubmitAt(Carbon::now());
        $data->setPublished(true);
        $data->save();

        return $this->redirect($request->get('redirect'));
    }
}