<?php

namespace AppBundle\Controller;

use Pimcore\Controller\FrontendController;
use Symfony\Component\HttpFoundation\Request;
use Pimcore\Model\DataObject;

class DefaultController extends FrontendController
{
    const SCRIPT1 = "/template_avrist/assets/js/custom-js-1.js";
    const SCRIPT2 = "/template_avrist/assets/js/custom-js-2.js";
    const SCRIPT3 = "/template_avrist/assets/js/custom-js-3.js";
    const SCRIPT4 = "/template_avrist/assets/js/custom-js-4.js";
    const SCRIPT5 = "/template_avrist/assets/js/custom-js-5.js";
    const SCRIPT6 = "/template_avrist/assets/js/custom-js-6.js";
    const SCRIPT7 = "/template_avrist/assets/js/custom-js-7.js";
    const SCRIPT8 = "/template_avrist/assets/js/custom-js-8.js";
    const SCRIPT9 = "/template_avrist/assets/js/custom-js-9.js";

    public function LifePersonalCategory()
    {
        $category = DataObject\ProductCategory::getByCategoryType('individu');

        return $category;
    }

    public function LifeBusinessCategory()
    {
        $category = DataObject\ProductCategory::getByCategoryType('bisnis');

        return $category;
    }

    public function GeneralCategory()
    {
        $category = DataObject\ProductCategory::getByCategoryType('general');

        return $category;
    }

    public function AssetCategory()
    {
        $category = DataObject\ProductCategory::getByCategoryType('asset');

        return $category;
    }

    public function siteCategoryLife()
    {
        $siteCategory = DataObject\SiteCategory::getById(148);

        return $siteCategory;
    }

    public function siteCategoryGeneral()
    {
        $siteCategory = DataObject\SiteCategory::getById(149);

        return $siteCategory;
    }

    public function siteCategoryAsset()
    {
        $siteCategory = DataObject\SiteCategory::getById(150);

        return $siteCategory;
    }

    public function testingAction()
    {
        $category_individu = DataObject\ProductCategory::getByCategoryType('individu');
        $category_bisnis = DataObject\ProductCategory::getByCategoryType('bisnis');
        $category_general = DataObject\ProductCategory::getByCategoryType('general');
        $category_asset = DataObject\ProductCategory::getByCategoryType('asset');

        $solusiFav = new DataObject\SolusiFavoritIndex\Listing();
        $this->view->solusi_fav = $solusiFav;

        $product = new DataObject\Product\Listing();

        $this->view->product = $product;

        $this->view->category_individu = $category_individu;
        $this->view->category_bisnis = $category_bisnis;
        $this->view->category_general = $category_general;
        $this->view->category_asset = $category_asset;
    }

    /**
     * @param Request $request
     */
    public function defaultAction(Request $request)
    {
        $siteId = 1;

        $url = DataObject\URL::getById(201);

        $this->view->url = $url;

        $navbar = [];

        $this->navbar($navbar, $siteId);

        $this->view->navbar = $navbar;
        $this->view->footer = $this->footer($siteId);
        $this->view->script = DefaultController::SCRIPT1;
    }

    public function defaulTestAction()
    {

    }

    public function loginPageAction()
    {
        $siteId = 1;

        $url = DataObject\URL::getById(201);

        $this->view->url = $url;

        $navbar = [];

        $this->navbar($navbar, $siteId);

        $this->view->navbar = $navbar;

        $this->view->footer = $this->footer($siteId);

        $this->view->script = False;
    }

    public function homeAction()
    {
        if($this->document->getId() == 1)
        {
            $siteId = $this->document->getId();
        }
        else
        {
            $site = \Pimcore\Model\Site::getCurrentSite();
            $siteId = $site->getId();
        }

        //$product = new DataObject\Product\Listing();
        //$product->setLimit(6);
        //$product->setOrderKey("RAND()", false);

        $objs1 = new DataObject\HomePage\Listing();
        $objs1->load();

        foreach($objs1 as $obj1)
        {
            $homePage = $obj1;
        }

        $this->view->homePage = $homePage;

        $product = [];

        foreach($homePage->getBlock3() as $block3)
        {
            array_push($product, DataObject\Product::getByPath($block3['dataObjectLink']->getData()));
        }

        $this->view->product = $product;
        
        //$lifeGuidePage = new DataObject\LifeGuidePage\Listing();
        //$lifeGuidePage->setCondition("siteId = ?", $siteId);
        //$lifeGuidePage->load();
//
        //foreach ($lifeGuidePage as $lGP)
        //{
        //    $lifeGuide = new DataObject\Article\Listing();
        //    $lifeGuide->setCondition("category = ?", "lifeGuide");
        //    $lifeGuide->addConditionParam("siteId = ? ", $siteId, "AND");
        //    $lifeGuide->setOrderKey(["publishDate", "o_id"]);
        //    $lifeGuide->setOrder("desc");
        //    $lifeGuide->load();
//
        //    $this->view->lifeGuidePage = $lGP;
        //    $this->view->lifeGuide = $lifeGuide;
        //}

        $newsMenu = new DataObject\Menu4\Listing();
        $newsMenu->setCondition("siteId = ?", $siteId);
        $newsMenu->addConditionParam("name = ?", "News & Event", "AND");
        $newsMenu->load();

        foreach($newsMenu as $nM)
        {
            $this->view->newsMenu = $nM;
        }

        $newsPage = new DataObject\NewsPage\Listing();
        $newsPage->setCondition("siteId = ?", $siteId);
        $newsPage->load();

        foreach ($newsPage as $nP)
        {
            $news = new DataObject\Article\Listing();
            $news->setCondition("category = ?", "news");
            $news->addConditionParam("siteId = ? ", $siteId, "AND");
            $news->setOrderKey(["publishDate", "o_id"]);
            $news->setOrder("desc");
            $news->load();

            $this->view->newsPage = $nP;
            $this->view->news = $news;
        }

        $life = new DataObject\Product\Listing();
        $life->setCondition("category__id = 44");
        $life->load();

        $this->view->life = $life;

        $navbar = [];

        $this->navbar($navbar, $siteId);

        $this->view->navbar = $navbar;

        $this->view->footer = $this->footer($siteId);
    }

    public function aboutAction()
    {
        $about = DataObject\About::getById(146);

        $this->view->about = $about;
    }

    public function galleryAction()
    {
        $gallery = DataObject\Gallery::getById(168);

        $this->view->gallery = $gallery;
    }

    public function tcAction()
    {
        $generic = DataObject\Generic::getById(192);

        $this->view->generic = $generic;
    }

    public function privacyPolicyAction()
    {
        $generic = DataObject\Generic::getById(193);

        $this->view->generic = $generic;
    }

    public function url()
    {
        $url = DataObject\URL::getById(201);

        return $url;
    }

    public function navbar(&$array, $siteId = 1)
    {
        $menu = new DataObject\Listing();
        $menu->setCondition("o_parentId = ?",  1);
        $menu->addConditionParam("o_key = ?", "Menu", "AND");
        $menu->load();

        foreach($menu as $m)
        {
            $name = substr($m->getKey(),0,3);
            $id = $m->getId();
            $nameId = $name.$id;
        }
        DefaultController::menuRecursive($menu, $nameId, $array);

        //$beranda = new DataObject\Menu\Listing();
        //$beranda->setCondition("o_key = ?", "Beranda");
        //$beranda->load();

        //foreach($beranda as $b)
        //{
        //    $array["beranda"] = $b;
        //}

        if(\Pimcore\Model\Site::isSiteRequest()) {
            $site = \Pimcore\Model\Site::getCurrentSite();
            $navStartNode = $site->getRootDocument();

        }
        else {
            $navStartNode = \Pimcore\Model\Document::getById(1);
        }

        $array["beranda"] = $navStartNode->getFullPath();
        $logo = new DataObject\Logo\Listing();
        $logo->setCondition("siteId = ?", $siteId);
        $logo->load();

        foreach($logo as $l)
        {
            $array["logo"] = $l;
        }

        $url = DataObject\URL::getById(201);
        $array["url"] = $url;

        $rss_feed = simplexml_load_file("http://avrist.com/lifeguide/feed/");

        $array['rss_feed'] = $rss_feed;

    }

    public function menuRecursive($daObj, $parentNameId, &$array, $limit = 4)
    {

        foreach($daObj as $dO)
        {   
            $dKey = substr($dO->getKey(),0,3);
            $dId = $dO->getId();
            $daObj = new DataObject\Listing();
            $daObj->setCondition("o_parentId = ?", $dId);
            $daObj->setOrderKey("o_index");
            $daObj->setOrder("asc");
            $daObj->load();

            $atr = $parentNameId.$dKey;
            $nameId = $dKey.$dId;

            if($daObj->getTotalCount() == 0)
            {
                $array[$atr] = $daObj;
            }
            elseif($daObj->getTotalCount() > 0)
            {   
                $array[$atr] = $daObj;
                if($limit > 0)
                { 
                    DefaultController::menuRecursive($daObj, $nameId, $array, $limit-1);
                }
                
            }

        }
    }

    public function footer($siteId)
    {
        $objs1 = new DataObject\Footer\Listing();
        $objs1->setCondition("siteId = ?", $siteId);
        $objs1->load();

        foreach($objs1 as $obj1)
        {
            $footer = $obj1;
        }

        return $footer;
    }

    public function navbarFooter($siteId, &$object, $script)
    {
        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer($siteId);

        $object->view->navbar = $navbar;

        $object->view->footer = $footer;

        $object->view->script = $script;
    }

    public function getBgBanner($siteId)
    {
        if ($siteId == 1)
        {
            $arr['bg'] = "bg-primary";
            $arr['banner'] = "/template_avrist/assets/img/pattern/banner_life_solid.png";
            $arr['bgImg'] = "/template_avrist/assets/img/common/img_banner_sample.jpg";
            $arr['color'] = "#432862";
        }
        elseif ($siteId == 2) 
        {
            $arr['bg'] = "bg-gi";
            $arr['banner'] = "/template_avrist/assets/img/pattern/banner_general_solid.png";
            $arr['bgImg'] = "/template_avrist/assets/img/common/img_banner_sample.jpg";
            $arr['color'] = "#7F8AA3";
        }
        elseif ($siteId == 3)
        {
            $arr['bg'] = "bg-avram";
            $arr['banner'] = "/template_avrist/assets/img/pattern/banner_avram_solid.png";
            $arr['bgImg'] = "/template_avrist/assets/img/tentang/img_banner_style_1_asset.jpg";
            $arr['color'] = "#579792";
        }

        return $arr;
    }

}
