<?php

namespace AppBundle\Controller\Faq;

use Pimcore\Controller\FrontendController;
use Symfony\Component\HttpFoundation\Request;
use Pimcore\Model\DataObject;
use AppBundle\Controller\DefaultController;

class FaqController extends FrontendController
{
    public function defaultAction()
    {
        if(\Pimcore\Model\Site::isSiteRequest()) {
            $site = \Pimcore\Model\Site::getCurrentSite();
            $siteId = $site->getId();
        }
        else{
            $siteId = 1;
        }

        $objs1 = new DataObject\FaqPage\Listing();
        $objs1->setCondition("siteId = ?", $siteId);
        $objs1->load();

        foreach ($objs1 as $obj1)
        {
            $faqPage = $obj1;
        }

        $this->view->faqPage = $faqPage;

        $faqCategory = new DataObject\FaqCategory\Listing();
        $faqCategory->setCondition("o_parentId = ?", $faqPage->getId());
        $faqCategory->load();

        $this->view->faqCategory = $faqCategory;

        $count = 0;
        foreach ($faqCategory as $fC)
        {
            $faq = new DataObject\Faq\Listing();
            $faq->setCondition("o_parentId = ?", $fC->getId());
            $faq->load();

            $this->view->{"faq".$fC->getId()} = $faq;
        }
    }

    public function indexAction(Request $request)
    {
        if($request->request->has('s'))
        {
            $s = "/".$request->get('s')."/i";
        }
        else
        {
            $s = "/.+/i";
        }

        if(\Pimcore\Model\Site::isSiteRequest()) {
            $site = \Pimcore\Model\Site::getCurrentSite();
            $siteId = $site->getId();
        }
        else{
            $siteId = 1;
        }

        $bgBanner = DefaultController::getBgBanner($siteId);

        $this->view->bgBanner = $bgBanner;

        $objs1 = new DataObject\FaqPage\Listing();
        $objs1->setCondition("siteId = ?", $siteId);
        $objs1->load();

        foreach ($objs1 as $obj1)
        {
            $faqPage = $obj1;
        }

        $faqCategories = [];

        foreach ($faqPage->getBlock() as $block)
        {
            $faq = [];
            foreach($block['subBlock']->getData() as $subblock)
            {
                if (preg_match($s, $subblock['title']->getData()) || preg_match($s, $block['category']->getData()))
                {
                    array_push($faq, [
                        'title' => $subblock['title']->getData(),
                        'faq' => $subblock['faq']->getData()
                    ]);
                }
            }
            if(!empty($faq))
            {
                $faqCategories[$block['category']->getData()] = $faq;
            }
        }

        $this->view->faqCategories = $faqCategories;

        $this->view->search = True;

        $this->view->faqPage = $faqPage;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer($siteId);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT1;
    }
}
