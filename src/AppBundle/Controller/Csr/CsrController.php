<?php

namespace AppBundle\Controller\Csr;

use AppBundle\Controller\DefaultController;
use Pimcore\Controller\FrontendController;
use Symfony\Component\HttpFoundation\Request;
use \Pimcore\Model\DataObject;


class CsrController extends FrontendController
{
    public function indexAction()
    {
        if(\Pimcore\Model\Site::isSiteRequest()) {
            $site = \Pimcore\Model\Site::getCurrentSite();
            $siteId = $site->getId();
        }
        else{
            $siteId = 1;
        }

        $bgBanner = DefaultController::getBgBanner($siteId);

        $objs1 = new DataObject\CsrPage\Listing();
        $objs1->setCondition("siteId = ?", $siteId);
        $objs1->load();

        foreach($objs1 as $obj1)
        {
            $csrPage = $obj1;
        }

        $objs2 = new DataObject\Listing();
        $objs2->setCondition("o_parentId = ?", $csrPage->getId());
        $objs2->load();

        $yearId = "(";

        foreach($objs2 as $obj2)
        {
            $yearId .= $obj2->getId().",";
        }

        $yearId = substr($yearId, 0, -1).")";

        $csr = new DataObject\Article\Listing();
        $csr->setCondition("o_parentId IN ".$yearId);
        $csr->setOrderKey(["publishDate", "o_id"]);
        $csr->setOrder("desc");
        $csr->load();

        $this->view->csrPage = $csrPage;

        $this->view->csr = $csr;

        $this->view->bgBanner = $bgBanner;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer($siteId);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT2;
    }

    public function detailAction(Request $request)
    {
        if(\Pimcore\Model\Site::isSiteRequest()) {
            $site = \Pimcore\Model\Site::getCurrentSite();
            $siteId = $site->getId();
        }
        else{
            $siteId = 1;
        }
        $titlePath = $request->get('path');

        $bgBanner = DefaultController::getBgBanner($siteId);

        $objs1 = new DataObject\CsrPage\Listing();
        $objs1->setCondition("siteId = ?", $siteId);
        $objs1->load();

        foreach($objs1 as $obj1)
        {
            $csrPage = $obj1;
        }

        $csr = DataObject\Article::getByPath($csrPage->getFullPath()."/".$titlePath);

        $otherCsr = new DataObject\Article\Listing();
        $otherCsr->setCondition("o_parentId = ?", $csr->getParentId());
        $otherCsr->addConditionParam("o_Id != ?", $csr->getId(), "AND");
        $otherCsr->setOrderKey(["publishDate", "o_id"]);
        $otherCsr->setOrder("desc");
        $otherCsr->load();

        $objs2 = new DataObject\AboutCompany\Listing();
        $objs2->setCondition("siteId = ?", $siteId);
        $objs2->load();

        foreach($objs2 as $obj2)
        {
            $aboutCompany = $obj2;
        }

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer($siteId);

        return $this->render(":Csr:details.html.php", 
        [
            "navbar" => $navbar, 
            "footer" => $footer,
            "siteId" => $siteId,
            "aboutCompany" => $aboutCompany,
            "csrPage" => $csrPage,
            "csr" => $csr,
            "otherCsr" => $otherCsr,
            "bgBanner" => $bgBanner,
            "script" => DefaultController::SCRIPT2
        ]);
    }
}
