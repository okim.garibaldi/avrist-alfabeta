<?php

namespace AppBundle\Controller\Search;

use AppBundle\Controller\DefaultController;
use Carbon\Carbon;
use Pimcore\Controller\FrontendController;
use Symfony\Component\HttpFoundation\Request;
use Pimcore\Model\DataObject;

class SearchController extends FrontendController
{
	public function defaultAction(Request $request)
    {

    }

    public function articleSearchAction(Request $request)
    {
    	$category = $request->get('category');
    }
	public function productSearchJsonAction(Request $request)
	{
		$s = $request->get('s');
		$products = new DataObject\Product\Listing();
		$products->setCondition("name LIKE ?", "%".$s."%");
		$products->load();
		$result = [];
		foreach ($products as $product) {
			array_push($result, $product->getName());
		}

		return $this->json($result);
	}

	public function productSearchAction(Request $request)
	{
		$s = "%".$request->get('s')."%";
		$objs1 = new DataObject\Listing();

		$url = DataObject\URL::getById(201);

		$objs1->setCondition("o_className IN ('Article', 'Product')");
		$objs1->addConditionParam("o_key LIKE ?", $s, "AND");
		$objs1->load();
		$result = [];
		foreach ($objs1 as $o1) {
			$obj1 = $o1;
			break;
		}

		if($obj1->getClassName() == "Article")
		{
			$pageClasses = [
            "partnership" => "\Pimcore\Model\DataObject\PartnershipPage\Listing",
            "lifeGuide" => "\Pimcore\Model\DataObject\LifeGuidePage\Listing",
            "awards" => "\Pimcore\Model\DataObject\AwardsPage\Listing",
            "csr" => "\Pimcore\Model\DataObject\CsrPage\Listing"];

            if($obj1->getCategory() == "news" || $obj1->getCategory()== "event")
        	{
        	    $objs2 = new \Pimcore\Model\DataObject\NewsPage\Listing();
        	    $objs2->setCondition("siteId = ?", $obj1->getSiteId());
        	    $objs2->load();
	
        	    foreach($objs2 as $obj2)
        	    {
        	        $page = $obj2;
        	    }
        	}
        	else
        	{
        	    $objs2 = new $pageClasses[$obj1->getCategory()]();
        	    $objs2->setCondition("siteId = ?", $obj1->getSiteId());
        	    $objs2->load();
	
        	    foreach($objs2 as $obj2)
        	    {
        	        $page = $obj2;
        	    }
        	}

        	$link = str_replace(strval($page->getPath()), '', strval($obj1->getFullPath()));

        	if($obj1->getSiteId() == 1)
        	{
        		$link = $url->getLife()."/".$link;
        	}
        	elseif($obj1->getSiteId() == 2)
        	{
        		$link = $url->getGeneral()."/".$link;
        	}
        	else
        	{
        		$link = $url->getAm()."/".$link;
        	}

		}
		elseif($obj1->getClassName() == "Product")
		{
			$page = strval($obj1->getPage());

			if($obj1->getSiteId() == 1)
			{
				$link = $url->getLife().str_replace("/life-insurance", '', $page);
			}
			elseif($obj1->getSiteId() == 2)
			{
				$link = $url->getGeneral().str_replace("/general", '', $page);
			}
			else
			{
				$link = $url->getAm().str_replace("/asset-management", '', $page);
			}
		}

		return $this->redirect($link);
		//$page = strval($obj->getPage());
//
		//$url = DataObject\URL::getById(201);
		//if(\Pimcore\Model\Site::isSiteRequest())
		//{
		//	return $this->redirect($page);
		//}
		//else
		//{
		//	if($obj->getSiteId() == 1)
		//	{
		//		$link = $url->getLife().str_replace("/life-insurance", '', $page);
		//	}
		//	elseif($obj->getSiteId() == 2)
		//	{
		//		$link = $url->getGeneral().str_replace("/general", '', $page);
		//	}
		//	else
		//	{
		//		$link = $url->getAm().str_replace("/asset-management", '', $page);
		//	}
		//	return $this->redirect($link);
		//}
			
	}

	public function searchAction(Request $request){
		$s = $request->get('s');
		//$products = DataObject\Product::getById(70);
		//return $this->redirect($products->getPage);
		$products = new DataObject\Product\Listing();
		$products->setCondition("name = ?", $s);
		$products->load();
		foreach ($products as $product) {
			$result = $product;
		}
		$parent = DataObject\ProductCategory::getById($result->getParentId());
		$url = DataObject\URL::getById(201);

		$page = strval($result->getPage());


		
		if (substr($page, 0, 8) == "/general"){
			return $this->redirect('http://'.$url->getGeneral().substr($page, 8));
		}
		else if (substr($page, 0, 17) == "/asset-management"){
			return $this->redirect('http://'.$url->getAm().substr($page, 17));
		}
		else if (substr($page, 0, 15) == "/life-insurance"){
			return $this->redirect('http://'.$url->getLife().substr($page, 15));
		}
		else {
			return $this->redirect($page); 
		}

		//if ($parent->getCategoryType() == "general"){
		////	if (substr($result->getPage(), 0, 8) == "/general"){
		////		return $this->redirect('http://'.$url->getGeneral().substr($result->getPage(), 8));
		////	}
		////	else{
		////		return $this->redirect($result->getPage());
		////	}
		////	return $this->redirect('http://'.$url->getGeneral().$result->getPage());
		////	return $this->redirect($result->getPage());
		//	return $this->redirect('http://'.$url->getGeneral().$page);
		//}
		//elseif ($parent->getCategoryType() == "asset") {
		////	if (substr($result->getPage(), 0, 17) == "/asset-management"){
		////		return $this->redirect('http://'.$url->getAm().substr($result->getPage(), 17));
		////	}
		////	else{
		////		return $this->redirect($result->getPage());
		////	}
		////	return $this->redirect('http://'.$url->getAm().$result->getPage());
		////	return $this->redirect($result->getPage());
		//	return $this->redirect('http://'.$url->getAm().$page);
		//}
		//else{
		////	if (substr($result->getPage(), 0, 15) == "/life-insurance"){
		////		return $this->redirect('http://'.$url->getLife().substr($result->getPage(), 15));
		////	}
		////	else {
		////		return $this->redirect($result->getPage());
		////	}
		////	return $this->redirect('http://'.$url->getLife().$result->getPage())//;
		////	return $this->redirect($result->getPage());
		//	return $this->redirect('http://'.$url->getLife().$page);
		//}
		//return $this->redirect('http://life.alfabeta.co.id/personal/life/avrist-investment-plus');
		//echo $parent->getCategoryType;
		//echo $parent->getCategoryType();	
	}
}
