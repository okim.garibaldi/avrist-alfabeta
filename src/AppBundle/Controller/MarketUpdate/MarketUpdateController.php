<?php

namespace AppBundle\Controller\MarketUpdate;

use Carbon\Carbon;
use AppBundle\Controller\DefaultController;
use Pimcore\Controller\FrontendController;
use Symfony\Component\HttpFoundation\Request;
use \Pimcore\Model\DataObject;

class MarketUpdateController extends FrontendController
{
    public function indexAction()
    {
        $site = \Pimcore\Model\Site::getCurrentSite();
        $siteId = $site->getId();

        $objs1 = new DataObject\MarketUpdate\Listing();
        $objs1->setCondition("siteId = ?", $siteId);
        $objs1->load();

        foreach($objs1 as $obj1)
        {
            $marketUpdate = $obj1;
        }

        $this->view->marketUpdate = $marketUpdate;

        foreach($marketUpdate->getBlock() as $block)
        {
            $name = [];
            foreach($block['subblock']->getData() as $subblock)
            {
                $file = [];
                foreach($subblock['subsubblock']->getData() as $subsubblock)
                {
                    array_push($file, [
                    "name" => $subsubblock['name']->getData(),
                    "file" => \Pimcore\Model\Asset::getByPath($subsubblock['file']->getData())
                    ]);
                }
                $name[$subblock['year']->getData()] = $file;
            }
            $arr[$block['name']->getData()] = $name;
        }

        $this->view->arr = $arr;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer($siteId);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT1;
    }

}
