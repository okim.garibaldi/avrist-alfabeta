<?php

namespace AppBundle\Controller\Am;

use Pimcore\Controller\FrontendController;
use Symfony\Component\HttpFoundation\Request;
use Pimcore\Model\DataObject;
use AppBundle\Controller\DefaultController;
use Carbon\Carbon;

class HomeController extends FrontendController
{
    public function homeAction(Request $request)
    {
        //setlocale(LC_ALL, "id");
        $site = \Pimcore\Model\Site::getCurrentSite();
        $siteId = $site->getId();

        $url = DataObject\URL::getById(201);

        $this->view->url = $url;
        
        $siteCategories = new DataObject\SiteCategory\Listing();
        $siteCategories->setCondition("siteId = ?", $siteId);
        $siteCategories->load();

        foreach($siteCategories as $sC)
        {
            $siteCategory = $sC;
        }

        $this->view->siteCategory = $siteCategory;

        $productCategories = new DataObject\ProductCategory\Listing();
        $productCategories->setCondition("categoryType = ?", "asset");
        $productCategories->load();

        $productCategoriesArr = [];

        foreach($productCategories as $pC)
        {
            array_push($productCategoriesArr, $pC->getName());
        }

        $this->view->productCategories = $productCategoriesArr;

        $product = new DataObject\Product\Listing();
        $product->setCondition("siteId = ?", $siteId);
        if($request->request->has('productCategory') && !(empty($request->get('productCategory'))))
        {
            $productCategory = new DataObject\ProductCategory\Listing();
            $productCategory->setCondition("categoryType = ?", "asset");
            $productCategory->addConditionParam("name = ?", $request->get('productCategory'), "AND");
            $productCategory->load();

            foreach($productCategory as $pC)
            {
                $product->addConditionParam("category__id = ?", $pC->getId(), "AND");
            }

            $this->view->selectedProductCategory = $request->get('productCategory');
        }
        else 
        {
            $this->view->selectedProductCategory = '';
        }
        if($request->request->has('s'))
        {
            $product->addConditionParam("name LIKE ?", "%".$request->get('s')."%", "AND");
        }

        $product->load();

        $priceLatestDate = new DataObject\ProductPrice\Listing();
        $priceLatestDate->setLimit(1);
        $priceLatestDate->setOrderKey("date");
        $priceLatestDate->setOrder("desc");
        $priceLatestDate->load();

        foreach ($priceLatestDate as $pLD) 
        {
            
            $latestDate = $pLD->getDate();
        }

        if($request->request->has('date') && !(empty($request->get('date'))))
        {
            $date = new Carbon($request->get('date'));
            //$date = Carbon::parse($request->get('date'));
        }
        else 
        {
            $date = $pLD->getDate();
        }


        $productPrices = [];

        foreach ($product as $p)
        {
            $objs1 = new DataObject\Listing();
            $objs1->setCondition("o_key = ?", "price");
            $objs1->addConditionParam("o_parentId = ?", $p->getId(), "AND");
            $objs1->load();
            foreach ($objs1 as $obj1)
            {
                $selectedPrices = new DataObject\ProductPrice\Listing();
                //$prices->setCondition("o_parentId = ?", $obj1->getId());
                $selectedPrices->setCondition("date = ?", $date->timestamp);
                $selectedPrices->addConditionParam("o_parentId = ?", $obj1->getId(), "AND");
                $selectedPrices->load();

                foreach($selectedPrices as $sP)
                {
                    $selectedPrice = $sP;
                }

                $previousSelectedPrices = new DataObject\ProductPrice\Listing();
                $previousSelectedPrices->setCondition("date = ?", $date->timestamp - (24 * 60 * 60));
                $previousSelectedPrices->addConditionParam("o_parentId = ?", $obj1->getId(), "AND");
                $previousSelectedPrices->load();

                foreach($previousSelectedPrices as $pSP)
                {
                    $previousSelectedPrice = $pSP;
                }

                array_push($productPrices, [
                    "product" => $p->getName(),
                    "previousSelectedPrice" => $previousSelectedPrice->getPrice(),
                    "previousSelectedPriceDate" => $previousSelectedPrice->getDate()->format('d/m/Y'),
                    "selectedPrice" => $selectedPrice->getPrice(),
                    "selectedPriceDate" => $selectedPrice->getDate()->format('d/m/Y')
                ]);
            }  
        }

        //foreach ($productPrices as $price)]
        //{
        //    echo $price['product']->getName().' ';
        //    echo $price['previousSelectedPrices'].' ';
        //    echo $price['selectedPrices'];
        //}

        $this->view->searchAm = True;

        $this->view->latestDate = $latestDate;

        $this->view->productPrices = $productPrices;

        $this->view->category = $siteCategory;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer($siteId);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT7;
    }
}