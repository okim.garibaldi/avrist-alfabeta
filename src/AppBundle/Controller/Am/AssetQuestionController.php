<?php

namespace AppBundle\Controller\Am;

use Pimcore\Model\DataObject\AssetQuestion;
use Pimcore\Controller\FrontendController;

class AssetQuestionController extends FrontendController
{
    public function indexAction()
    {
        $assetQuestion = new AssetQuestion\Listing();

        $this->view->assetQuestion = $assetQuestion;
    }
}