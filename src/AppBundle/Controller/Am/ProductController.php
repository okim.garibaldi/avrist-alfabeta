<?php

namespace AppBundle\Controller\Am;

use Pimcore\Controller\FrontendController;
use Symfony\Component\HttpFoundation\Request;
use \Pimcore\Model\DataObject\Product;
use Pimcore\Model\DataObject;
use AppBundle\Controller\DefaultController;

class ProductController extends FrontendController
{
    public function defaultAction(Request $request)
    {

    }

    //Reksadana Konvensional
    public function avristAdaKasMutiaraAction()
    {
        $site = \Pimcore\Model\Site::getCurrentSite();
        $siteId = $site->getId();

        $product = Product::getById(125);

        $this->view->product = $product;

        $benefits = new DataObject\ProductBenefit\Listing();
        $benefits->setCondition("product__id = ". $product->getId());
        $this->view->benefits = $benefits;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer($siteId);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT1;
    }
    public function avristAdaSahamBlueSafirAction()
    {
        $site = \Pimcore\Model\Site::getCurrentSite();
        $siteId = $site->getId();

        $product = Product::getById(126);

        $this->view->product = $product;

        $benefits = new DataObject\ProductBenefit\Listing();
        $benefits->setCondition("product__id = ". $product->getId());
        $this->view->benefits = $benefits;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer($siteId);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT1;
    }
    public function avristDanaObligasiSejahteraAction()
    {
        $site = \Pimcore\Model\Site::getCurrentSite();
        $siteId = $site->getId();

        $product = Product::getById(127);

        $this->view->product = $product;

        $benefits = new DataObject\ProductBenefit\Listing();
        $benefits->setCondition("product__id = ". $product->getId());
        $this->view->benefits = $benefits;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer($siteId);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT1;
    }
    public function avristIDX30Action()
    {
        $site = \Pimcore\Model\Site::getCurrentSite();
        $siteId = $site->getId();

        $product = Product::getById(128);

        $this->view->product = $product;

        $benefits = new DataObject\ProductBenefit\Listing();
        $benefits->setCondition("product__id = ". $product->getId());
        $this->view->benefits = $benefits;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer($siteId);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT1;
    }
    public function avristPrimeBondFundAction()
    {
        $site = \Pimcore\Model\Site::getCurrentSite();
        $siteId = $site->getId();

        $product = Product::getById(129);

        $this->view->product = $product;

        $benefits = new DataObject\ProductBenefit\Listing();
        $benefits->setCondition("product__id = ". $product->getId());
        $this->view->benefits = $benefits;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer($siteId);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT1;
    }
    public function avristPrimeIncomeFundAction()
    {
        $site = \Pimcore\Model\Site::getCurrentSite();
        $siteId = $site->getId();

        $product = Product::getById(130);

        $this->view->product = $product;

        $benefits = new DataObject\ProductBenefit\Listing();
        $benefits->setCondition("product__id = ". $product->getId());
        $this->view->benefits = $benefits;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer($siteId);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT1;
    }

    //Reksadana Syariah
    public function avristAdaKasSyariahAction()
    {
        $site = \Pimcore\Model\Site::getCurrentSite();
        $siteId = $site->getId();

        $product = Product::getById(131);

        $this->view->product = $product;

        $benefits = new DataObject\ProductBenefit\Listing();
        $benefits->setCondition("product__id = ". $product->getId());
        $this->view->benefits = $benefits;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer($siteId);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT1;
    }
    public function avristAdaSukukAmarSyariahAction()
    {
        $site = \Pimcore\Model\Site::getCurrentSite();
        $siteId = $site->getId();

        $product = Product::getById(135);

        $this->view->product = $product;

        $benefits = new DataObject\ProductBenefit\Listing();
        $benefits->setCondition("product__id = ". $product->getId());
        $this->view->benefits = $benefits;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer($siteId);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT1;
    }
    public function avristAdaSukukBerkahSyariahAction()
    {
        $site = \Pimcore\Model\Site::getCurrentSite();
        $siteId = $site->getId();

        $product = Product::getById(132);

        $this->view->product = $product;

        $benefits = new DataObject\ProductBenefit\Listing();
        $benefits->setCondition("product__id = ". $product->getId());
        $this->view->benefits = $benefits;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer($siteId);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT1;
    }
    public function avristAdaSukukIncomeFundAction()
    {
        $site = \Pimcore\Model\Site::getCurrentSite();
        $siteId = $site->getId();

        $product = Product::getById(136);

        $this->view->product = $product;

        $benefits = new DataObject\ProductBenefit\Listing();
        $benefits->setCondition("product__id = ". $product->getId());
        $this->view->benefits = $benefits;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer($siteId);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT1;
    }
    public function avristBalanceAmarSyariahAction()
    {
        $site = \Pimcore\Model\Site::getCurrentSite();
        $siteId = $site->getId();

        $product = Product::getById(133);

        $this->view->product = $product;

        $benefits = new DataObject\ProductBenefit\Listing();
        $benefits->setCondition("product__id = ". $product->getId());
        $this->view->benefits = $benefits;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer($siteId);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT1;
    }
    public function avristEquityAmarSyariahAction()
    {
        $site = \Pimcore\Model\Site::getCurrentSite();
        $siteId = $site->getId();

        $product = Product::getById(134);

        $this->view->product = $product;

        $benefits = new DataObject\ProductBenefit\Listing();
        $benefits->setCondition("product__id = ". $product->getId());
        $this->view->benefits = $benefits;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer($siteId);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT1;
    }

    //Reksadana Terproteksi
    public function avristDanaTerproteksiSpirit1Action()
    {
        $site = \Pimcore\Model\Site::getCurrentSite();
        $siteId = $site->getId();

        $product = Product::getById(137);

        $this->view->product = $product;

        $benefits = new DataObject\ProductBenefit\Listing();
        $benefits->setCondition("product__id = ". $product->getId());
        $this->view->benefits = $benefits;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer($siteId);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT1;
    }
    public function avristDanaTerproteksiSpirit2Action()
    {
        $site = \Pimcore\Model\Site::getCurrentSite();
        $siteId = $site->getId();

        $product = Product::getById(138);

        $this->view->product = $product;

        $benefits = new DataObject\ProductBenefit\Listing();
        $benefits->setCondition("product__id = ". $product->getId());
        $this->view->benefits = $benefits;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer($siteId);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT1;
    }
    public function avristDanaTerproteksiSpirit4Action()
    {
        $site = \Pimcore\Model\Site::getCurrentSite();
        $siteId = $site->getId();

        $product = Product::getById(139);

        $this->view->product = $product;

        $benefits = new DataObject\ProductBenefit\Listing();
        $benefits->setCondition("product__id = ". $product->getId());
        $this->view->benefits = $benefits;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer($siteId);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT1;
    }
    public function avristDanaTerproteksiSukukBerkahSyariahAction()
    {
        $site = \Pimcore\Model\Site::getCurrentSite();
        $siteId = $site->getId();

        $product = Product::getById(140);

        $this->view->product = $product;

        $benefits = new DataObject\ProductBenefit\Listing();
        $benefits->setCondition("product__id = ". $product->getId());
        $this->view->benefits = $benefits;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer($siteId);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT1;
    }
    public function avristProtectedFund1Action()
    {
        $site = \Pimcore\Model\Site::getCurrentSite();
        $siteId = $site->getId();

        $product = Product::getById(141);

        $this->view->product = $product;

        $benefits = new DataObject\ProductBenefit\Listing();
        $benefits->setCondition("product__id = ". $product->getId());
        $this->view->benefits = $benefits;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer($siteId);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT1;
    }
}
