<?php

namespace AppBundle\Controller\Am;

use Pimcore\Controller\FrontendController;
use Symfony\Component\HttpFoundation\Request;
use \Pimcore\Model\DataObject;
use AppBundle\Controller\DefaultController;

class ProductCategoryController extends FrontendController
{
    public function defaultAction(Request $request)
    {

    }

    public function reksadanaKonvensionalAction()
    {
        $site = \Pimcore\Model\Site::getCurrentSite();
        $siteId = $site->getId();

        $product = new DataObject\Product\Listing();
        $product->setCondition("category__id = 122");
        $product->load();

        $category = DataObject\ProductCategory::getById(122);

        $this->view->product = $product;
        $this->view->category = $category;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer(2);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT1;
    }

    public function reksadanaSyariahAction()
    {
        $site = \Pimcore\Model\Site::getCurrentSite();
        $siteId = $site->getId();

        $product = new DataObject\Product\Listing();
        $product->setCondition("category__id = 123");
        $product->load();

        $category = DataObject\ProductCategory::getById(123);

        $this->view->product = $product;
        $this->view->category = $category;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer(2);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT1;
    }

    public function reksadanaTerproteksiAction()
    {
        $site = \Pimcore\Model\Site::getCurrentSite();
        $siteId = $site->getId();
        
        $product = new DataObject\Product\Listing();
        $product->setCondition("category__id = 124");
        $product->load();

        $category = DataObject\ProductCategory::getById(124);

        $this->view->product = $product;
        $this->view->category = $category;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer(2);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT1;
    }
}
