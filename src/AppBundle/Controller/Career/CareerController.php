<?php

namespace AppBundle\Controller\Career;

use Carbon\Carbon;
use AppBundle\Controller\DefaultController;
use Pimcore\Controller\FrontendController;
use Symfony\Component\HttpFoundation\Request;
use \Pimcore\Model\DataObject;

class CareerController extends FrontendController
{
    public function indexAction()
    {
        if(\Pimcore\Model\Site::isSiteRequest()) {
            $site = \Pimcore\Model\Site::getCurrentSite();
            $siteId = $site->getId();
        }
        else{
            $siteId = 1;
        }

        $bgBanner = DefaultController::getBgBanner($siteId);

        $this->view->bgBanner = $bgBanner;

        $objs1 = new DataObject\CareerPage\Listing();
        $objs1->setCondition("siteId = ?", $siteId);
        $objs1->load();

        foreach($objs1 as $obj1)
        {
            $careerPage = $obj1;
        }
        
        $this->view->careerPage = $careerPage;

        $careerDAO = new DataObject\Career();
        $fd = $careerDAO->getClass()->getFieldDefinition("division");
        $options = $fd->getOptions();

        $this->view->options = $options;

        $this->view->careerSearch = True;

        $this->view->overflow = True;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer($siteId);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT3;
    }

    public function employeeIndexAction(Request $request)
    {
        if(\Pimcore\Model\Site::isSiteRequest()) {
            $site = \Pimcore\Model\Site::getCurrentSite();
            $siteId = $site->getId();
        }
        else{
            $siteId = 1;
        }

        $objs1 = new DataObject\CareerList\Listing();
        $objs1->setCondition("siteId = ?", $siteId);
        $objs1->load();

        foreach ($objs1 as $obj1)
        {
            $careerList = $obj1;
        }

        $this->view->careerList = $careerList;

        foreach($careerList->getBlock1() as $block1)
        {
            $objs2 = DataObject\CareerCategory::getByPath($block1['category']->getData());

            $objs3 = new DataObject\Career\Listing();
            $objs3->setCondition("o_parentId = ?", $objs2->getId());
            if($request->request->has('division'))
            {
                $objs3->addConditionParam("division = ?", $request->get('division'), "AND");
            }
            if($request->request->has('s'))
            {
                $objs3->addConditionParam("role LIKE ?", "%".$request->get('s')."%", "AND");
            }
            $objs3->load();

            $careers[$objs2->getName()] = $objs3;
        }

        $bgBanner = DefaultController::getBgBanner($siteId);
        
        $this->view->bgBanner = $bgBanner;

        $this->view->careers = $careers;

        $this->view->overflow = True;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer($siteId);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT3;
    }

    public function careerSearchAction(Request $request)
    {
        if(\Pimcore\Model\Site::isSiteRequest()) {
            $site = \Pimcore\Model\Site::getCurrentSite();
            $siteId = $site->getId();
        }
        else{
            $siteId = 1;
        }

        $s = "%".$request->get('s')."%";
        $division = $request->get('division');

        $objs1 = new DataObject\CareerList\Listing();
        $objs1->setCondition("siteId = ?", $siteId);
        $objs1->load();

        foreach($objs1 as $obj1)
        {
            $careerList = $obj1;
        }

        $objs2 = new DataObject\Career\Listing();
        $objs2->setCondition("role LIKE ?", $s);
        $objs2->addConditionParam("division = ?", $division, "AND");
        $objs2->addConditionParam("siteId = ?", $siteId, "AND");
        $objs2->load();

        foreach($objs2 as $obj2)
        {
            $career = $obj2;
        }
        return $this->redirect("/career/".str_replace($careerList->getPath(), '', $career->getFullPath()));

    }

    public function careerDetailAction(Request $request)
    {
        if(\Pimcore\Model\Site::isSiteRequest()) {
            $site = \Pimcore\Model\Site::getCurrentSite();
            $siteId = $site->getId();
        }
        else{
            $siteId = 1;
        }   
        $path = $request->get('path');

        $bgBanner = DefaultController::getBgBanner($siteId);

        $objs1 = new DataObject\CareerPage\Listing();
        $objs1->setCondition("siteId = ?", $siteId);
        $objs1->load();

        foreach($objs1 as $obj1)
        {
            $careerPage = $obj1;
        }

        $career = DataObject\Career::getByPath($careerPage->getFullPath()."/".$path);

        $category = strtolower(strtok($path, "/"));

        $bgBanner = DefaultController::getBgBanner($siteId);

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer($siteId);

        return $this->render(":Career:details.html.php", 
        [
            "career" => $career,
            "category" => $category,
            "bgBanner" => $bgBanner,
            "navbar" => $navbar, 
            "footer" => $footer,
            "script" => DefaultController::SCRIPT2
        ]);
    }

    public function careerFormAction()
    {
        
    }

    public function saveApplicationAction(Request $request)
    {
    	$pageId = $this->document->getId(); 
        $career = new DataObject\Career\Listing();
        $career->setCondition('page__id = ?', $pageId);
        $career->load();

        foreach($career as $ca)
        {
            $careerId = $ca->getId();
        }

    	$data = new DataObject\CareerApplicantsData();
    	$data->setParentId($careerId);
    	$data->setKey(Carbon::now() . $request->get('Name'));
    	$data->setName($request->get('name'));
    	$data->setAddress($request->get('address'));
    	$data->setCity($request->get('city'));
    	$data->setState($request->get('state'));
    	$data->setZip($request->get('phone'));
    	$data->setPhone($request->get('email'));
    	$data->setEmail($request->get('phone'));
    	$data->setGender($request->get('gender'));
    	$data->setBirthPlace($request->get('birthPlace'));
    	$data->setBirthDate($request->get('birthDate'));
    	$data->setSubmitAt(Carbon::now());
    	$data->setPublished(true);
    	$data->save();
    }

    
}
