<?php

namespace AppBundle\Controller\Partnership;

use AppBundle\Controller\DefaultController;
use Pimcore\Controller\FrontendController;
use Symfony\Component\HttpFoundation\Request;
use \Pimcore\Model\DataObject;

class PartnershipController extends FrontendController
{
    public function indexAction()
    {
    	if(\Pimcore\Model\Site::isSiteRequest()) {
            $site = \Pimcore\Model\Site::getCurrentSite();
            $siteId = $site->getId();
        }
        else{
            $siteId = 1;
        }

        $objs1 = new DataObject\PartnershipPage\Listing();
        $objs1->setCondition("siteId = ?", $siteId);
        $objs1->setLimit(12);
        $objs1->load();


        foreach($objs1 as $obj1)
        {
            $partnershipPage = $obj1;
        }

        $this->view->partnershipPage = $partnershipPage;

        foreach($partnershipPage->getBlock() as $block)
        {
            $partnership = [];
            foreach($block['subblock']->getData() as $subblock)
            {
                array_push($partnership, DataObject\Article::getByPath($subblock['page']->getData()));
            }
            $partnerships[$block['year']->getData()] = $partnership;
        }

        $this->view->partnerships = $partnerships;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer($siteId);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT1;
    }

    public function detailAction(Request $request)
    {
    	if(\Pimcore\Model\Site::isSiteRequest()) {
            $site = \Pimcore\Model\Site::getCurrentSite();
            $siteId = $site->getId();
        }
        else{
            $siteId = 1;
        }
        $path = $request->get('path');

        $objs1 = new DataObject\PartnershipPage\Listing();
        $objs1->setCondition("siteId = ?", $siteId);
        $objs1->load();

        foreach($objs1 as $obj1)
        {
            $partnershipPage = $obj1;
        }

        $partnershipDetail = DataObject\Article::getByPath($partnershipPage->getFullPath()."/".$path);

        $otherPartnershipDetail = new DataObject\Article\Listing();
        $otherPartnershipDetail->setCondition("o_parentId = ?", $partnershipDetail->getParentId());
        $otherPartnershipDetail->addConditionParam("o_Id != ?", $partnershipDetail->getId(), "AND");
        $otherPartnershipDetail->setOrderKey(["publishDate", "o_id"]);
        $otherPartnershipDetail->setOrder("desc");
        $otherPartnershipDetail->load();

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer($siteId);

        return $this->render(":Partnership:details.html.php", 
        [
            "partnershipPage" => $partnershipPage,
            "partnershipDetail" => $partnershipDetail,
            "otherPartnershipDetail" => $otherPartnershipDetail,
            "navbar" => $navbar, 
            "footer" => $footer,
            "siteId" => $siteId,
            "script" => DefaultController::SCRIPT2
        ]);
    }
}
