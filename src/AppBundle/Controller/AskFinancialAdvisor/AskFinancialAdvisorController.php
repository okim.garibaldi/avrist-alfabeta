<?php

namespace AppBundle\Controller\AskFinancialAdvisor;

use AppBundle\Controller\DefaultController;
use Pimcore\Controller\FrontendController;
use Symfony\Component\HttpFoundation\Request;
use \Pimcore\Model\DataObject;
use Carbon\Carbon;


class AskFinancialAdvisorController extends FrontendController
{
    public function indexAction(Request $request)
    {
        if(\Pimcore\Model\Site::isSiteRequest()) {
            $site = \Pimcore\Model\Site::getCurrentSite();
            $siteId = $site->getId();
        }
        else{
            $siteId = 1;
        }

        $bgBanner = DefaultController::getBgBanner($siteId);

        $this->view->bgBanner = $bgBanner;

        $objs1 = new DataObject\AskFinancialAdvisor\Listing();
        $objs1->setCondition("siteId = ?", $siteId);
        $objs1->load();

        foreach($objs1 as $obj1)
        {
            $customerSupport = $obj1;
        }

        $this->view->customerSupport = $customerSupport;

        $customerSupportMessage = new DataObject\AskFinancialAdvisorMessage();
        $topikFd = $customerSupportMessage->getClass()->getFieldDefinition("topik");
        $topikOptions = $topikFd->getOptions();

        $this->view->topikOptions = $topikOptions;

        $timeFd = $customerSupportMessage->getClass()->getFieldDefinition("time");
        $timeOptions = $timeFd->getOptions();

        $this->view->timeOptions = $timeOptions;

        if($request->request->has('name')) {
            $askFinancialAdvisorMessage = new DataObject\AskFinancialAdvisorMessage();
            $askFinancialAdvisorMessage->setParentId($customerSupport->getId());
            $askFinancialAdvisorMessage->setKey(Carbon::now() . ' - ' . $request->get('name'));
            $askFinancialAdvisorMessage->setName($request->get('name'));
            $askFinancialAdvisorMessage->setEmail($request->get('email'));
            $askFinancialAdvisorMessage->setPhoneNumber($request->get('phoneNumber'));
            $askFinancialAdvisorMessage->setTopik($request->get('topik'));
            $askFinancialAdvisorMessage->setTime($request->get('time'));
            $askFinancialAdvisorMessage->setMessage($request->get('message'));
            $askFinancialAdvisorMessage->setPublished(true);
            $askFinancialAdvisorMessage->save();
        }

        $this->view->customerSupportMessage = True;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer($siteId);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT5;
    }
}
