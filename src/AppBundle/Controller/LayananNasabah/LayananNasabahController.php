<?php

namespace AppBundle\Controller\LayananNasabah;

use Carbon\Carbon;
use AppBundle\Controller\DefaultController;
use Pimcore\Controller\FrontendController;
use Symfony\Component\HttpFoundation\Request;
use \Pimcore\Model\DataObject;

class LayananNasabahController extends FrontendController
{
    public function indexAction(Request $request)
    {
        if($request->request->has('s'))
        {
            $s = "/".$request->get('s')."/i";
        }
        else
        {
            $s = "/.+/i";
        }

        if(\Pimcore\Model\Site::isSiteRequest()) {
            $site = \Pimcore\Model\Site::getCurrentSite();
            $siteId = $site->getId();
        }
        else{
            $siteId = 1;
        }


        $objs1 = new DataObject\LayananNasabahPage\Listing();
        $objs1->setCondition("siteId = ?", $siteId);
        $objs1->load();

        foreach($objs1 as $obj1)
        {
            $layananNasabahPage = $obj1;
        }
        
        $this->view->layananNasabahPage = $layananNasabahPage;

        $layananNasabah = [];

        foreach($layananNasabahPage->getBlock1() as $block1)
        {
            $obj2 = DataObject\LayananNasabah::getByPath($block1['layanan']->getData());
            if (preg_match($s, $obj2->getName()) || preg_match($s, $obj2->getDescription()))
            {
                $layananNasabah[$obj2->getName()] = $obj2;
            }
            
        }

        $this->view->layananNasabah = $layananNasabah;

        $arrayBlock2 = [];

        foreach($layananNasabahPage->getBlock2() as $block2)
        {
            if (preg_match($s, $block2['title']->getData()) || preg_match($s, $block2['description']->getData()))
            {
                array_push($arrayBlock2, [
                    'title' =>  $block2['title']->getData(),
                    'description' => $block2['description']->getData(),
                    'file' => $block2['file']->getData()
                ]);
            }
        }

        $this->view->arrayBlock2 = $arrayBlock2;

        $arrayBlock3 = [];

        foreach($layananNasabahPage->getBlock3() as $block3)
        {
            if (preg_match($s, $block3['title']->getData()) || preg_match($s, $block3['description']->getData()))
            {
                array_push($arrayBlock3, [
                    'title' =>  $block3['title']->getData(),
                    'description' => $block3['description']->getData(),
                    'formulir' => $block3['formulir']->getData()
                ]);
            }
        }

        $this->view->arrayBlock3 = $arrayBlock3;

        $arrayBlock4 = [];

        foreach($layananNasabahPage->getBlock4() as $block4)
        {
            if (preg_match($s, $block4['name']->getData()) || preg_match($s, $block4['description']->getData()))
            {
                array_push($arrayBlock4, [
                    'name' =>  $block4['name']->getData(),
                    'description' => $block4['description']->getData(),
                    'page' => $block4['page']->getData()
                ]);
            }
        }

        $this->view->arrayBlock4 = $arrayBlock4;

        $this->view->search = True;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer($siteId);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT5;
    }

    public function layananDetailAction(Request $request)
    {
        if(\Pimcore\Model\Site::isSiteRequest()) {
            $site = \Pimcore\Model\Site::getCurrentSite();
            $siteId = $site->getId();
        }
        else{
            $siteId = 1;
        }

        $path = $request->get('path');

        $objs1 = new DataObject\LayananNasabahPage\Listing();
        $objs1->setCondition("siteId = ?", $siteId);
        $objs1->load();

        foreach($objs1 as $obj1)
        {
            $layananNasabahPage = $obj1;
        }

        $layananNasabah = DataObject\LayananNasabah::getByPath($layananNasabahPage->getFullPath()."/layanan/".$path);

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer($siteId);

        return $this->render(":Layanan-Nasabah:details.html.php", 
        [
            "layananNasabah" => $layananNasabah,
            "navbar" => $navbar, 
            "footer" => $footer,
            "script" => DefaultController::SCRIPT5
        ]);
    }
}
