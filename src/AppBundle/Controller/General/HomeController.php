<?php

namespace AppBundle\Controller\General;

use Pimcore\Controller\FrontendController;
use Symfony\Component\HttpFoundation\Request;
use Pimcore\Model\DataObject;
use AppBundle\Controller\DefaultController;

class HomeController extends FrontendController
{
    public function homeAction()
    {
        $site = \Pimcore\Model\Site::getCurrentSite();
        $siteId = $site->getId();
        
        $siteCategories = new DataObject\SiteCategory\Listing();
        $siteCategories->setCondition("siteId = ?", $siteId);
        $siteCategories->load();

        foreach($siteCategories as $sC)
        {
            $siteCategory = $sC;
        }

        $this->view->siteCategory = $siteCategory;

        $rss_feed = simplexml_load_file("http://avrist.com/lifeguide/feed/");

        $this->view->rss_feed = $rss_feed;

        //$lifeGuidePage = new DataObject\LifeGuidePage\Listing();
        //$lifeGuidePage->setCondition("siteId = ?", 1);
        //$lifeGuidePage->load();
//
        //foreach ($lifeGuidePage as $lGP)
        //{
        //    $lifeGuide = new DataObject\Article\Listing();
        //    $lifeGuide->setCondition("category = ?", "lifeGuide");
        //    $lifeGuide->addConditionParam("siteId = ? ", 1, "AND");
        //    $lifeGuide->setOrderKey(["publishDate", "o_id"]);
        //    $lifeGuide->setOrder("desc");
        //    $lifeGuide->load();
//
        //    $this->view->lifeGuidePage = $lGP;
        //    $this->view->lifeGuide = $lifeGuide;
        //}

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer($siteId);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT7;    }

    public function aboutAction()
    {
    	$data = DataObject\About::getById(453);
    	$this->view->data = $data;
    }

    public function aboutCompanyAction()
    {
    	$data = DataObject\AboutCompany::getById(452);
    	$this->view->data = $data;
    }


}
