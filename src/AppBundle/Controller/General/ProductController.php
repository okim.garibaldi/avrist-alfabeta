<?php

namespace AppBundle\Controller\General;

use Pimcore\Controller\FrontendController;
use Pimcore\Model\DataObject\Product;
use Symfony\Component\HttpFoundation\Request;
use \Pimcore\Model\DataObject;
use AppBundle\Controller\DefaultController;

class ProductController extends FrontendController
{
    public function defaultAction(Request $request)
    {

    }

    //Accident
    public function avristPersonalAccidentAction()
    {
        $site = \Pimcore\Model\Site::getCurrentSite();
        $siteId = $site->getId();

        $product = Product::getById(91);

        $this->view->product = $product;

        $benefits = new DataObject\ProductBenefit\Listing();
        $benefits->setCondition("product__id = ". $product->getId());
        $this->view->benefits = $benefits;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer($siteId);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT1;
    }
    //Automotive
    public function avristOtoAction()
    {
        $site = \Pimcore\Model\Site::getCurrentSite();
        $siteId = $site->getId();

        $product = Product::getById(92);

        $this->view->product = $product;

        $benefits = new DataObject\ProductBenefit\Listing();
        $benefits->setCondition("product__id = ". $product->getId());
        $this->view->benefits = $benefits;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer(2);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT1;
    }
    //Cargo
    public function avristMarineCargoAction()
    {
        $site = \Pimcore\Model\Site::getCurrentSite();
        $siteId = $site->getId();

        $product = Product::getById(93);

        $this->view->product = $product;

        $benefits = new DataObject\ProductBenefit\Listing();
        $benefits->setCondition("product__id = ". $product->getId());
        $this->view->benefits = $benefits;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer(2);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT1;
    }

    //Casualty
    public function avristAutomobileLiabilityAction()
    {
        $site = \Pimcore\Model\Site::getCurrentSite();
        $siteId = $site->getId();

        $product = Product::getById(95);

        $this->view->product = $product;

        $benefits = new DataObject\ProductBenefit\Listing();
        $benefits->setCondition("product__id = ". $product->getId());
        $this->view->benefits = $benefits;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer(2);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT1;
    }
    public function avristEmployeesLiabilityAction()
    {
        $site = \Pimcore\Model\Site::getCurrentSite();
        $siteId = $site->getId();

        $product = Product::getById(99);

        $this->view->product = $product;

        $benefits = new DataObject\ProductBenefit\Listing();
        $benefits->setCondition("product__id = ". $product->getId());
        $this->view->benefits = $benefits;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer(2);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT1;
    }
    public function avristPersonalLiabilityAction()
    {
        $site = \Pimcore\Model\Site::getCurrentSite();
        $siteId = $site->getId();

        $product = Product::getById(96);

        $this->view->product = $product;

        $benefits = new DataObject\ProductBenefit\Listing();
        $benefits->setCondition("product__id = ". $product->getId());
        $this->view->benefits = $benefits;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer(2);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT1;
    }
    public function avristPublicLiabilityAction()
    {
        $site = \Pimcore\Model\Site::getCurrentSite();
        $siteId = $site->getId();

        $product = Product::getById(97);

        $this->view->product = $product;

        $benefits = new DataObject\ProductBenefit\Listing();
        $benefits->setCondition("product__id = ". $product->getId());
        $this->view->benefits = $benefits;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer(2);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT1;
    }
    public function avristWorkmensCompensationAction()
    {
        $site = \Pimcore\Model\Site::getCurrentSite();
        $siteId = $site->getId();

        $product = Product::getById(98);

        $this->view->product = $product;

        $benefits = new DataObject\ProductBenefit\Listing();
        $benefits->setCondition("product__id = ". $product->getId());
        $this->view->benefits = $benefits;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer(2);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT1;
    }

    //Engineering
    public function avristAsuransiAlatBeratAction()
    {
        $site = \Pimcore\Model\Site::getCurrentSite();
        $siteId = $site->getId();

        $product = Product::getById(100);

        $this->view->product = $product;

        $benefits = new DataObject\ProductBenefit\Listing();
        $benefits->setCondition("product__id = ". $product->getId());
        $this->view->benefits = $benefits;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer(2);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT1;
    }
    public function avristAsuransiKonstruksiAction()
    {
        $site = \Pimcore\Model\Site::getCurrentSite();
        $siteId = $site->getId();

        $product = Product::getById(101);

        $this->view->product = $product;

        $benefits = new DataObject\ProductBenefit\Listing();
        $benefits->setCondition("product__id = ". $product->getId());
        $this->view->benefits = $benefits;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer(2);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT1;
    }
    public function avristAsuransiMesinIndustriAction()
    {
        $site = \Pimcore\Model\Site::getCurrentSite();
        $siteId = $site->getId();

        $product = Product::getById(102);

        $this->view->product = $product;

        $benefits = new DataObject\ProductBenefit\Listing();
        $benefits->setCondition("product__id = ". $product->getId());
        $this->view->benefits = $benefits;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer(2);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT1;
    }
    public function avristAsuransiPemasanganMesinAction()
    {
        $site = \Pimcore\Model\Site::getCurrentSite();
        $siteId = $site->getId();

        $product = Product::getById(103);

        $this->view->product = $product;

        $benefits = new DataObject\ProductBenefit\Listing();
        $benefits->setCondition("product__id = ". $product->getId());
        $this->view->benefits = $benefits;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer(2);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT1;
    }
    public function avristAsuransiPeralatanElektronikAction()
    {
        $site = \Pimcore\Model\Site::getCurrentSite();
        $siteId = $site->getId();

        $product = Product::getById(104);

        $this->view->product = $product;

        $benefits = new DataObject\ProductBenefit\Listing();
        $benefits->setCondition("product__id = ". $product->getId());
        $this->view->benefits = $benefits;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer(2);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT1;
    }

    //Glasses
    public function avristGlassesProtectionAction()
    {
        $site = \Pimcore\Model\Site::getCurrentSite();
        $siteId = $site->getId();

        $product = Product::getById(94);

        $this->view->product = $product;

        $benefits = new DataObject\ProductBenefit\Listing();
        $benefits->setCondition("product__id = ". $product->getId());
        $this->view->benefits = $benefits;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer(2);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT1;
    }

    //Mikro
    public function avristAsuransiMikroAsuransikuAction()
    {
        $site = \Pimcore\Model\Site::getCurrentSite();
        $siteId = $site->getId();

        $product = Product::getById(105);

        $this->view->product = $product;

        $benefits = new DataObject\ProductBenefit\Listing();
        $benefits->setCondition("product__id = ". $product->getId());
        $this->view->benefits = $benefits;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer(2);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT1;
    }
    public function avristAsuransiMikroRumahkuAction()
    {
        $site = \Pimcore\Model\Site::getCurrentSite();
        $siteId = $site->getId();

        $product = Product::getById(106);

        $this->view->product = $product;

        $benefits = new DataObject\ProductBenefit\Listing();
        $benefits->setCondition("product__id = ". $product->getId());
        $this->view->benefits = $benefits;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer(2);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT1;
    }
    public function avristAsuransiMikroSiabangErupsiAction()
    {
        $site = \Pimcore\Model\Site::getCurrentSite();
        $siteId = $site->getId();

        $product = Product::getById(107);

        $this->view->product = $product;

        $benefits = new DataObject\ProductBenefit\Listing();
        $benefits->setCondition("product__id = ". $product->getId());
        $this->view->benefits = $benefits;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer(2);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT1;
    }
    public function avristAsuransiMikroSiabangTsunamiAction()
    {
        $site = \Pimcore\Model\Site::getCurrentSite();
        $siteId = $site->getId();

        $product = Product::getById(108);

        $this->view->product = $product;

        $benefits = new DataObject\ProductBenefit\Listing();
        $benefits->setCondition("product__id = ". $product->getId());
        $this->view->benefits = $benefits;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer(2);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT1;
    }
    public function avristAsuransiMikroStopUsahaErupsiAction()
    {
        $site = \Pimcore\Model\Site::getCurrentSite();
        $siteId = $site->getId();

        $product = Product::getById(109);

        $this->view->product = $product;

        $benefits = new DataObject\ProductBenefit\Listing();
        $benefits->setCondition("product__id = ". $product->getId());
        $this->view->benefits = $benefits;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer(2);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT1;
    }
    public function avristAsuransiMikroStopUsahaTsunamiAction()
    {
        $site = \Pimcore\Model\Site::getCurrentSite();
        $siteId = $site->getId();

        $product = Product::getById(110);

        $this->view->product = $product;

        $benefits = new DataObject\ProductBenefit\Listing();
        $benefits->setCondition("product__id = ". $product->getId());
        $this->view->benefits = $benefits;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer(2);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT1;
    }
    public function avristAsuransiMikroWarisankuAction()
    {
        $site = \Pimcore\Model\Site::getCurrentSite();
        $siteId = $site->getId();

        $product = Product::getById(111);

        $this->view->product = $product;

        $benefits = new DataObject\ProductBenefit\Listing();
        $benefits->setCondition("product__id = ". $product->getId());
        $this->view->benefits = $benefits;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer(2);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT1;
    }

    //Property
    public function avristAsuransiGempaBumiAction()
    {
        $site = \Pimcore\Model\Site::getCurrentSite();
        $siteId = $site->getId();

        $product = Product::getById(112);

        $this->view->product = $product;

        $benefits = new DataObject\ProductBenefit\Listing();
        $benefits->setCondition("product__id = ". $product->getId());
        $this->view->benefits = $benefits;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer(2);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT1;
    }
    public function avristAsuransiKebakaranAction()
    {
        $site = \Pimcore\Model\Site::getCurrentSite();
        $siteId = $site->getId();

        $product = Product::getById(113);

        $this->view->product = $product;

        $benefits = new DataObject\ProductBenefit\Listing();
        $benefits->setCondition("product__id = ". $product->getId());
        $this->view->benefits = $benefits;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer(2);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT1;
    }
    public function avristAsuransiPropertyAllRiskAction()
    {
        $site = \Pimcore\Model\Site::getCurrentSite();
        $siteId = $site->getId();

        $product = Product::getById(114);

        $this->view->product = $product;

        $benefits = new DataObject\ProductBenefit\Listing();
        $benefits->setCondition("product__id = ". $product->getId());
        $this->view->benefits = $benefits;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer(2);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT1;
    }
    public function avristAsuransiTerorismeSabotaseAction()
    {
        $site = \Pimcore\Model\Site::getCurrentSite();
        $siteId = $site->getId();

        $product = Product::getById(115);

        $this->view->product = $product;

        $benefits = new DataObject\ProductBenefit\Listing();
        $benefits->setCondition("product__id = ". $product->getId());
        $this->view->benefits = $benefits;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer(2);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT1;
    }
}
