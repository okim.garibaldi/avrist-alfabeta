<?php

namespace AppBundle\Controller\General;

use Pimcore\Controller\FrontendController;
use Symfony\Component\HttpFoundation\Request;
use Pimcore\Model\DataObject;

class DefaultController extends FrontendController
{
    public function defaultAction(Request $request)
    {

    }
}
