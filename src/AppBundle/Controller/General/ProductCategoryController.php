<?php

namespace AppBundle\Controller\General;

use Pimcore\Controller\FrontendController;
use Symfony\Component\HttpFoundation\Request;
use \Pimcore\Model\DataObject;
use AppBundle\Controller\DefaultController;

class ProductCategoryController extends FrontendController
{
    public function defaultAction(Request $request)
    {

    }

    public function accidentAction()
    {
        $site = \Pimcore\Model\Site::getCurrentSite();
        $siteId = $site->getId();

        $product = new DataObject\Product\Listing();
        $product->setCondition("category__id = 80");
        $product->load();

        $category = DataObject\ProductCategory::getById(80);

        $this->view->product = $product;
        $this->view->category = $category;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer(2);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT1;
    }

    public function automotiveAction()
    {
        $site = \Pimcore\Model\Site::getCurrentSite();
        $siteId = $site->getId();

        $product = new DataObject\Product\Listing();
        $product->setCondition("category__id = 81");
        $product->load();

        $category = DataObject\ProductCategory::getById(81);

        $this->view->product = $product;
        $this->view->category = $category;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer(2);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT1;
    }

    public function cargoAction()
    {
        $site = \Pimcore\Model\Site::getCurrentSite();
        $siteId = $site->getId();

        $product = new DataObject\Product\Listing();
        $product->setCondition("category__id = 82");
        $product->load();

        $category = DataObject\ProductCategory::getById(82);

        $this->view->product = $product;
        $this->view->category = $category;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer(2);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT1;
    }

    public function casualtyAction()
    {
        $site = \Pimcore\Model\Site::getCurrentSite();
        $siteId = $site->getId();

        $product = new DataObject\Product\Listing();
        $product->setCondition("category__id = 83");
        $product->load();

        $category = DataObject\ProductCategory::getById(83);

        $this->view->product = $product;
        $this->view->category = $category;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer(2);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT1;
    }

    public function engineeringAction()
    {
        $site = \Pimcore\Model\Site::getCurrentSite();
        $siteId = $site->getId();

        $product = new DataObject\Product\Listing();
        $product->setCondition("category__id = 84");
        $product->load();

        $category = DataObject\ProductCategory::getById(84);

        $this->view->product = $product;
        $this->view->category = $category;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer(2);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT1;
    }

    public function glassesAction()
    {
        $site = \Pimcore\Model\Site::getCurrentSite();
        $siteId = $site->getId();

        $product = new DataObject\Product\Listing();
        $product->setCondition("category__id = 85");
        $product->load();

        $category = DataObject\ProductCategory::getById(85);

        $this->view->product = $product;
        $this->view->category = $category;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer(2);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT1;
    }

    public function microAction()
    {
        $site = \Pimcore\Model\Site::getCurrentSite();
        $siteId = $site->getId();

        $product = new DataObject\Product\Listing();
        $product->setCondition("category__id = 86");
        $product->load();

        $category = DataObject\ProductCategory::getById(86);

        $this->view->product = $product;
        $this->view->category = $category;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer(2);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT1;
    }

    public function propertyAction()
    {
        $site = \Pimcore\Model\Site::getCurrentSite();
        $siteId = $site->getId();
        
        $product = new DataObject\Product\Listing();
        $product->setCondition("category__id = 87");
        $product->load();

        $category = DataObject\ProductCategory::getById(87);

        $this->view->product = $product;
        $this->view->category = $category;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer(2);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT1;
    }
}
