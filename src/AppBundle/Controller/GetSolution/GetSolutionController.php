<?php

namespace AppBundle\Controller\GetSolution;

use AppBundle\Controller\DefaultController;
use Carbon\Carbon;
use Pimcore\Controller\FrontendController;
use Symfony\Component\HttpFoundation\Request;
use Pimcore\Model\DataObject;

class GetSolutionController extends FrontendController
{
    public function defaultAction(Request $request)
    {
        $product = new DataObject\Product\Listing();
        $product->setLimit(3);
        $product->setCondition("name LIKE ?", ["%". $request->get('slug') ."%"]);
        $product->setOrderKey("RAND()", false);

        $this->view->product = $product;
    }

    public function personalAction()
    {
        //$product_category = new DefaultController();
//
        //$this->view->life_personal = $product_category->LifePersonalCategory();
        //$this->view->life_business = $product_category->LifeBusinessCategory();
        //$this->view->general = $product_category->GeneralCategory();

        $siteId = 1;

        $url = DataObject\URL::getById(201);

        $this->view->url = $url;

        //$getSolutionObj = new DataObject\GetSolution();
        //$fd = $getSolutionObj->getClass()->getFieldDefinition("solutionFor");
        //$solutionForOptions = $fd->getOptions();

        $this->view->solutionForOptions = $solutionForOptions;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $this->view->navbar = $navbar;
        $this->view->footer = DefaultController::footer($siteId);
    }

    public function saveRecordAction(Request $request)
    {
//        $key = $request->get('name');
//        $validasi = new DataObject\GetSolution\Listing();
//        $validasi->setCondition("key LIKE :key", ["key" => "%".$key."%"]);
//
//        if ($validasi)
//            return $this->redirect('/get-solution/personal');

        if ($request->get('category') == "personal") {
            $data = new DataObject\GetSolution();
            $data->setParentId(152);
            $data->setKey(Carbon::now() . ' - ' . $request->get('name'));
            $data->setCategory($request->get('category'));
            $data->setName($request->get('name'));
            $data->setAge($request->get('age'));
            $data->setLive($request->get('live'));
            $data->setTelp($request->get('telp'));
            $data->setEmail($request->get('email'));
            $data->setSolutionType($request->get('solution-type'));
            $data->setSolutionNeeds($request->get('solution-needs'));
            $data->setSolutionFor($request->get('solution-for'));
            $data->setNotSure($request->get('notSure'));
            $data->setSubmitAt(Carbon::now());
            $data->setVehicleType($request->get('vehicle-type'));
            $data->setProductionYear($request->get('production-year'));
            $data->setPlate($request->get('plate'));
            $data->setVehiclePrice($request->get('vehicle-price'));
            $data->setPropertyCity($request->get('property-city'));
            $data->setPropertyFunction($request->get('property-function'));
            $data->setPublished(true);
            $data->save();
        } elseif ($request->get('category') == "bisnis"){
            $data = new DataObject\GetSolutionBisnis();
            $data->setParentId(159);
            $data->setKey(Carbon::now() . ' - ' . $request->get('name'));
            $data->setCategory($request->get('category'));
            $data->setPic($request->get('pic'));
            $data->setTelp($request->get('telp'));
            $data->setEmail($request->get('email'));
            $data->setField($request->get('field'));
            $data->setSubmitAt(Carbon::now());
            $data->setPublished(true);
            $data->save();
        }

        return $this->redirect('/get-solution/summary');
    }

    public function summary1Action(Request $request)
    {
        $productCategory = new DataObject\ProductCategory\Listing();

        if ($request->get('category') == "personal") {
            $data = new DataObject\GetSolution();
            $data->setParentId(152);
            $data->setKey(Carbon::now() . ' - ' . $request->get('name'));
            $data->setCategory($request->get('category'));
            $data->setName($request->get('name'));
            $data->setAge($request->get('age'));
            $data->setLive($request->get('live'));
            $data->setTelp($request->get('telp'));
            $data->setEmail($request->get('email'));
            $data->setSolutionType($request->get('solution-type'));
            $data->setSolutionNeeds($request->get('solution-needs'));
            $data->setSolutionFor($request->get('solution-for'));
            $data->setSubmitAt(Carbon::now());
            $data->setVehicleType($request->get('vehicle-type'));
            $data->setProductionYear($request->get('production-year'));
            $data->setPlate($request->get('plate'));
            $data->setVehiclePrice($request->get('vehicle-price'));
            $data->setPropertyCity($request->get('property-city'));
            $data->setPropertyFunction($request->get('property-function'));
            $data->setPublished(false);
            $data->save();

            if($request->get('solution-type') == "Conventional")
            {
                $productCategory->setCondition("name = ?", $request->get('solution-needs'));
                $productCategory->load();

                foreach($productCategory as $pC) 
                {
                    $product = new DataObject\Product\Listing();
                    $product->setCondition("o_parentId = ?", $pC->getId());
                    $product->load();

                    $products = [];
                    foreach($product as $p)
                    {
                        $benefits = new DataObject\ProductBenefit\Listing();
                        $benefits->setCondition("product__id = ". $p->getId());
                        array_push($products, [
                            'product' => $p,
                            'benefits' => $benefits
                        ]); 
                    } 
                }
            }
            elseif($request->get('solution-type') == "Syariah")
            {
                $product = new DataObject\Product\Listing();
                $product->setCondition("syariah = ?", 1);
                $product->load();

                $products = []; 
                foreach($product as $p)
                {
                    $benefits = new DataObject\ProductBenefit\Listing();
                    $benefits->setCondition("product__id = ". $p->getId());
                    array_push($products, [
                        'product' => $p,
                        'benefits' => $benefits
                    ]); 
                } 

                
            }

        } elseif ($request->get('category') == "bisnis"){
            $data = new DataObject\GetSolutionBisnis();
            $data->setParentId(159);
            $data->setKey(Carbon::now() . ' - ' . $request->get('name'));
            $data->setName($request->get('name'));
            $data->setCategory($request->get('category'));
            $data->setPic($request->get('pic'));
            $data->setTelp($request->get('telp'));
            $data->setEmail($request->get('email'));
            $data->setField($request->get('field'));
            $data->setSubmitAt(Carbon::now());
            $data->setPublished(false);
            $data->save();

            $productCategory->setCondition("o_parentId = 67");
            $productCategory->load();

            $id = "(";
            foreach($productCategory as $pC)
            {
                $id .= $pC->getId().",";
            }
            $id = substr($id, 0, -1).")";

            $product = new DataObject\Product\Listing();
            $product->setCondition("o_parentId IN ".$id);
            $product->load();

            $products = [];
            foreach($product as $p)
            {
                $benefits = new DataObject\ProductBenefit\Listing();
                $benefits->setCondition("product__id = ". $p->getId());
                array_push($products, [
                    'product' => $p,
                    'benefits' => $benefits
                ]); 
            }
            

        }        
        $record = new DataObject\GetSolution\Listing();
        $record->setLimit(1);
        $record->setOrderKey("submitAt");

        $url = DataObject\URL::getById(201);

        $this->view->solutionFor = $request->get('solution-for');

        if($request->get('solution-needs') == "Life")
        {
            $this->view->solutionNeeds = "jiwa";
        }
        elseif ($request->get('solution-needs') == "Health") {
            $this->view->solutionNeeds = "kesehatan";
        }
        elseif ($request->get('solution-needs') == "Automotive") {
            $this->view->solutionNeeds = "otomotif";
        }
        elseif ($request->get('solution-needs') == "Property") {
            $this->view->solutionNeeds = "properti";
        }
        elseif ($request->get('solution-needs') == "Pensiun") {
            $this->view->solutionNeeds = "pensiun";
        }
        elseif ($request->get('category') == "bisnis")
        {
            $this->view->solutionNeeds = "bisnis";
        }


        $this->view->sendEmailAddress = "/summary-send-email?s=".$data->getId();

        $this->view->url = $url;

        $this->view->products = $products;

        //$this->view->summaryScript = True;

        $navbar = [];

        DefaultController::navbar($navbar, 1);

        $footer = DefaultController::footer(1);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT1;
    }
}
