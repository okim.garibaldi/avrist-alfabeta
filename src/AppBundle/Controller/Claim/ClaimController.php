<?php

namespace AppBundle\Controller\Claim;

use Pimcore\Controller\FrontendController;
use Symfony\Component\HttpFoundation\Request;
use Pimcore\Model\DataObject;
use AppBundle\Controller\DefaultController;

class ClaimController extends FrontendController
{
    public function IndexAction(Request $request)
    {
        $site = \Pimcore\Model\Site::getCurrentSite();
        $siteId = $site->getId();

        $objs1 = new DataObject\ClaimPage\Listing();
        $objs1->setCondition("siteId = ?", $siteId);
        $objs1->load();

        if($request->request->has('s'))
        {
            $s = "/".$request->get('s')."/i";
        }
        else
        {
            $s = "/.+/i";
        }

        foreach($objs1 as $obj1)
        {
            $claimPage = $obj1;
        }

        $this->view->claimPage = $claimPage;

        $files = [];

        foreach($claimPage->getBlock() as $block)
        {
            if (preg_match($s, $block['name']->getData()))
            {
            array_push($files, [
                'name' => $block['name']->getData(), 
                'file' => \Pimcore\Model\Asset::getByPath($block['file']->getData())
                ]);
            }
        }

        $this->view->search = True;

        $this->view->files = $files;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer($siteId);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT5;
    }

    public function claimPageAction()
    {
        if(\Pimcore\Model\Site::isSiteRequest()) {
            $site = \Pimcore\Model\Site::getCurrentSite();
            $siteId = $site->getId();
        } else {
            $siteId = 1;
        }

        $objs1 = new DataObject\Claim\Listing();
        $objs1->setCondition("siteId = ?", $siteId);
        $objs1->load();

        foreach($objs1 as $obj1)
        {
            $claim = $obj1;
        }


        $this->view->claim = $claim;

        $navbar = [];

        DefaultController::navbar($navbar, $siteId);

        $footer = DefaultController::footer($siteId);

        $this->view->navbar = $navbar;

        $this->view->footer = $footer;

        $this->view->script = DefaultController::SCRIPT5;
    }
}
