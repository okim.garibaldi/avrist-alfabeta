/* ==========================================================================
   JS for am search
   ========================================================================== */

window.onload = function()
{
    var input = document.getElementById("searchBox");
    input.addEventListener("keyup", function(event){
        if (event.keyCode === 13) {
            event.preventDefault();
            search(input);
        }
    });
    var button = document.getElementById("cari");
    button.addEventListener("click", function(event){
        search(input);
    });
    var dateFilter = document.getElementById("dateFilter");
    dateFilter.addEventListener("keyup", function(event){
    	if (event.keyCode === 13) {
        	search(input);
        }
    });
    var productCategory = document.getElementById("productCategory");
    productCategory.addEventListener('change', function (e) {
    	search(input);
    });
}

function search(input) {
    var search = input.value;
    var productCategoryIndex = document.getElementById("productCategory").selectedIndex;
    var productCategoryOptions = document.getElementById("productCategory").options;
    var productCategoryValue = productCategoryOptions[productCategoryIndex].value;

    var date = document.getElementById("dateFilter");
    dateValue = date.value.replace(/\//g, "-");

    var form = document.createElement('form');
    document.body.appendChild(form);
    form.method = 'post';
    form.action = window.location;
    var searchInput = document.createElement('input');
    searchInput.type = 'hidden';
    searchInput.name = 's';
    searchInput.value = search;
    form.appendChild(searchInput);
    var dateInput = document.createElement('input');
    dateInput.type = 'hidden';
    dateInput.name = 'date';
    dateInput.value = dateValue;
    form.appendChild(dateInput);
    var searchInput = document.createElement('input');
    productCategory.type = 'hidden';
    productCategory.name = 'productCategory';
    productCategory.value = productCategoryValue;
    form.appendChild(productCategory);

    form.submit();   
}