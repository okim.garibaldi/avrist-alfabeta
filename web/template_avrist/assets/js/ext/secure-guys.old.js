class ReqM {
    constructor(m) {
        this.user_id = m.user_id;
        this.device_id = m.device_id;
        this.ip_address = m.ip_address;
        this.culture = m.culture;
        this.content = m.content;
    }
}

class ResM {
    constructor(m) {
        this.html_response_code = m.html_response_code;
        this.service_response_code = m.service_response_code;
        this.message_title = m.message_title;
        this.message_content = m.message_content;
        this.content = this.content;
        this.culture = this.culture;
    }
}

class TDM {
    constructor(c) {
        this.r = c;
    }
}

class HM {
    constructor(m) {
        this.rsa_public_key = m.rsa_public_key;
        this.transaction_aes_key = m.transaction_aes_key;
    }
}

class HResM {
    constructor(m) {
        this.token = m.token;
        this.transaction_aes_key = m.transaction_aes_key;
    }
}

class UM {
    constructor(m) {
        this.username = m.username;
        this.password = m.password;
    }
}

class CryptoFunc {
    enc = new JSEncrypt();
	dec = new JSEncrypt();
    AESKey = store.get('rS96YifBNi') || null;
    LoginAESKey = store.get('esGmEcQUG7') || null;
    
    constructor()
	{
		this.enc = new JSEncrypt();
		this.dec = new JSEncrypt();
    }
    
    hasKey()
    {
        var hasPublic = store.has('rS96YifBNi');
        var hasPrivate = store.has('esGmEcQUG7');
        return hasPublic && hasPrivate;
    }

    genKey()
    {
        const crypto = new JSEncrypt({default_key_size: 1024});
        crypto.getKey();
        const privateKey = crypto.getPrivateKey();
        const publicKey = crypto.getPublicKey();
        store.set('h2uVuK3hTI', publicKey);
        store.set('fVATVBGmHw', privateKey);
    }

    clnAESKey()
    {
        store.remove('rS96YifBNi');
    }

    clnLoginAESKey()
	{
		store.remove('esGmEcQUG7');
    }

    setAESKey(k)
	{
		this.AESKey = k;
		store.set('rS96YifBNi', k);
    }

	setLoginAESKey(k)
	{
		this.AESKey = k;
		store.set('esGmEcQUG7', k);
    }
    
    encStr(strRaw, strPubKey)
	{
		this.enc.setPublicKey(strPubKey);
		const strRslt = this.enc.encrypt(strRaw);
		return strRslt;
	}

	decStr(strRaw, strPrivKey)
	{
		this.dec.setPrivateKey(strPrivKey);
		const strRslt = this.dec.decrypt(strRaw);
		return strRslt;
    }
    
    encStrAES(strRaw, strKey)
	{
		const k = aesjs.utils.hex.toBytes(strKey);
		const pt = aesjs.utils.utf8.toBytes(strRaw);
		const enc = new aesjs.ModeOfOperation.ctr(k, new aesjs.Counter(k));
		let cp = enc.encrypt(pt);
		cp = aesjs.utils.hex.fromBytes(cp);
		return cp;
    }
    
    decStrAES(strHex, strKey)
	{
		const k = aesjs.utils.hex.toBytes(strKey);
		const cp = aesjs.utils.hex.toBytes(strHex);
		const dec = new aesjs.ModeOfOperation.ctr(k, new aesjs.Counter(k));
		let pt = dec.decrypt(cp);
		pt = aesjs.utils.utf8.fromBytes(pt);
		return pt;
    }
    
    getAESKey()
    {
        return this.decStr(this.AESKey, store.get('fVATVBGmHw'));
    }

    getLoginAESKey()
    {
        return this.decStr(this.LoginAESKey, store.get('fVATVBGmHw'));
    }
}

const cf = new CryptoFunc();

$(document).ready(function() {
    var redirected = store.has('fseWVxaZME');
    if (!redirected) {
        var skey = store.has('h2uVuK3hTI');
        if (!skey) {
            cf.genKey();
        }

        if(!cf.hasKey()) {
            const gURL = 'https://d-agentz.avrist.com/backend/api/v1/generate-token/';
            var hm = new HM({
                rsa_public_key: store.get('h2uVuK3hTI'),
                transaction_aes_key: ""
            });

            var gr = new ReqM({
                user_id: "",
                device_id: navigator.userAgent,
                ip_address: _engine_location,
                culture: navigator.language,
                content: hm
            });

            var gtd = new TDM(btoa(JSON.stringify(gr)));

            var g = new XMLHttpRequest();
            g.open("POST", gURL, true);
            g.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
            g.send(JSON.stringify(gtd));
            g.onreadystatechange = function() {
                if (g.readyState == 4 && g.status == 201) {
                    var resp = JSON.parse(g.responseText);
                    var r = JSON.parse(atob(resp.r));
                    var hr = new HResM(r.content);
                    cf.setAESKey(hr.transaction_aes_key)
                    store.set('mCvVeOa4DW',btoa(hr.token));
                }
            };
        }
    }
});

function submitLogin() {
    var redirected = store.has('fseWVxaZME');
    if (!redirected) {
        var u = new UM({
            username: document.getElementById("loginUsername").value,
            password: document.getElementById("loginPassword").value
        });

        var lr = new ReqM({
            user_id: "",
            device_id: navigator.userAgent,
            ip_address: _engine_location,
            culture: navigator.language,
            content: u
        });
        
        var aesKey = cf.decStr(cf.AESKey,store.get('fVATVBGmHw'));

        var ltd = new TDM(btoa(cf.encStrAES(JSON.stringify(lr),aesKey)));

        const lUrl = 'https://d-agentz.avrist.com/backend/api/v1/customer/authentication/obtain-token/';
        var l = new XMLHttpRequest();
        l.open("POST", lUrl, true);
        
        l.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        l.setRequestHeader("Authorization", atob(store.get('mCvVeOa4DW')));
        l.send(JSON.stringify(ltd));
        l.onreadystatechange = function() {
            if (l.readyState == 4) {
                var resp = l.responseText;
                var dAES = cf.decStrAES(atob((JSON.parse(resp)).r),aesKey);
                console.log("Decrypted R (AES): "+dAES);
                var r = JSON.parse(dAES);
                if (r.html_response_code == 200) {
                    var ctn = r['content']['customer']['user'];
                    console.log("R deep to User: "+r['content']['customer']['user']);
                    var r_url = ctn.redirect_url;
                    console.log("Redirect URL: "+r_url);
                    store.set('fseWVxaZME',r_url);
                    if(r_url != null || r_url != "") {
                        // For redirect
                        window.location.href = r_url;
                    }
                } else {
                    console.log("Detail Message: "+r.content.detail);
                }
            }
        }; 
    }
}

function handleSubmit(e){
    var kc = (e.keyCode ? e.keyCode : e.which);
    if (kc == '13') {
        submitLogin();
    }
}

function submitForgot() {
    window.location.href = 'https://d-custz.avrist.com/#/security/forgotpassword';
}

function submitRegister() {
    window.location.href = 'https://d-custz.avrist.com/#/registration/basicinformation';
}
