class ReqM {
    constructor(m) {
        this.user_id = m.user_id;
        this.device_id = m.device_id;
        this.ip_address = m.ip_address;
        this.culture = m.culture;
        this.content = m.content;
    }
}

class ResM {
    constructor(m) {
        this.html_response_code = m.html_response_code;
        this.service_response_code = m.service_response_code;
        this.message_title = m.message_title;
        this.message_content = m.message_content;
        this.content = this.content;
        this.culture = this.culture;
    }
}

class TDM {
    constructor(c) {
        this.r = c;
    }
}

class HM {
    constructor(m) {
        this.rsa_public_key = m.rsa_public_key;
        this.transaction_aes_key = m.transaction_aes_key;
    }
}

class HResM {
    constructor(m) {
        this.token = m.token;
        this.transaction_aes_key = m.transaction_aes_key;
    }
}

class UM {
    constructor(m) {
        this.username = m.username;
        this.password = m.password;
    }
}

const enc = new JSEncrypt();
const dec = new JSEncrypt();

const inTenMinutes = new Date(new Date().getTime() + 10 * 60 * 1000);
const inEightMinutes = new Date(new Date().getTime() + 8 * 60 * 1000);

function aesKey() {
    return Cookies.get('rS96YifBNi');
}

function loginAESKey() {
    return Cookies.get('esGmEcQUG7');
}

function hasCookies(cn)
{
    var sc = Cookies.get(cn);
    return sc !== null && sc !== undefined ? true : false;
}

function hasKey()
{
    var hasPublic = hasCookies('rS96YifBNi');
    var hasPrivate = hasCookies('esGmEcQUG7');
    return hasPublic && hasPrivate;
}

function genKey()
{
    const crypto = new JSEncrypt({default_key_size: 1024});
    crypto.getKey();
    const privateKey = crypto.getPrivateKey();
    const publicKey = crypto.getPublicKey();
    Cookies.set('h2uVuK3hTI', publicKey);
    Cookies.set('fVATVBGmHw', privateKey);
}

function clnAESKey()
{
    Cookies.remove('rS96YifBNi');
}

function clnLoginAESKey()
{
    Cookies.remove('esGmEcQUG7');
}

function setAESKey(k)
{
    Cookies.set('rS96YifBNi', k, { expires: inEightMinutes });
}

function setLoginAESKey(k)
{
    Cookies.set('esGmEcQUG7', k, { expires: inEightMinutes });
}

function encStr(strRaw, strPubKey)
{
    enc.setPublicKey(strPubKey);
    const strRslt = enc.encrypt(strRaw);
    return strRslt;
}

function decStr(strRaw, strPrivKey)
{
    dec.setPrivateKey(strPrivKey);
    const strRslt = dec.decrypt(strRaw);
    return strRslt;
}

function encStrAES(strRaw, strKey)
{
    const k = aesjs.utils.hex.toBytes(strKey);
    const pt = aesjs.utils.utf8.toBytes(strRaw);
    const en = new aesjs.ModeOfOperation.ctr(k, new aesjs.Counter(k));
    let cp = en.encrypt(pt);
    cp = aesjs.utils.hex.fromBytes(cp);
    return cp;
}

function decStrAES(strHex, strKey)
{
    const k = aesjs.utils.hex.toBytes(strKey);
    const cp = aesjs.utils.hex.toBytes(strHex);
    const de = new aesjs.ModeOfOperation.ctr(k, new aesjs.Counter(k));
    let pt = de.decrypt(cp);
    pt = aesjs.utils.utf8.fromBytes(pt);
    return pt;
}

function getAESKey()
{
    return decStr(Cookies.get('rS96YifBNi'), Cookies.get('fVATVBGmHw'));
}

function getLoginAESKey()
{
    return decStr(loginAESKey(), Cookies.get('fVATVBGmHw'));
}

$(document).ready(function() {
    var redirected = hasCookies('fseWVxaZME');
    if (!redirected) {
        var skey = hasCookies('h2uVuK3hTI');
        if (!skey) {
            genKey();
        }

        if(!hasKey()) {
            const gURL = 'https://d-agentz.avrist.com/backend/api/v1/generate-token/';
            var hm = new HM({
                rsa_public_key: Cookies.get('h2uVuK3hTI'),
                transaction_aes_key: ""
            });

            var gr = new ReqM({
                user_id: "",
                device_id: navigator.userAgent,
                ip_address: _engine_location,
                culture: navigator.language,
                content: hm
            });

            var gtd = new TDM(btoa(JSON.stringify(gr)));

            var g = new XMLHttpRequest();
            g.open("POST", gURL, true);
            g.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
            g.send(JSON.stringify(gtd));
            g.onreadystatechange = function() {
                if (g.readyState == 4 && g.status == 201) {
                    var resp = JSON.parse(g.responseText);
                    var r = JSON.parse(atob(resp.r));
                    var hr = new HResM(r.content);
                    setAESKey(hr.transaction_aes_key)
                    Cookies.set('mCvVeOa4DW', btoa(hr.token), { expires: inEightMinutes });
                }
            };
        }
    }
});

function setIdentity(user)
{
    Cookies.set('A3134identity', btoa(user), { expires: inTenMinutes });
}

function isSameIdentity(user)
{
    if (hasCookies('A3134identity')) {
        return (atob(Cookies.get('A3134identity')) === user) ? true : false;
    }
    return false;
}

function submitLogin() {
    var u = new UM({
        username: document.getElementById("loginUsername").value,
        password: document.getElementById("loginPassword").value
    });
    if (!isSameIdentity(document.getElementById("loginUsername").value)) {
        Cookies.remove('fseWVxaZME');
    }
    //var redirected = hasCookies('fseWVxaZME');
    //if (!redirected) {
    //    var lr = new ReqM({
    //        user_id: "",
    //        device_id: navigator.userAgent,
    //        ip_address: _engine_location,
    //        culture: navigator.language,
    //        content: u
    //    });
    //    
    //    var aesKey = decStr(Cookies.get('rS96YifBNi'),Cookies.get('fVATVBGmHw'));
//
    //    var ltd = new TDM(btoa(encStrAES(JSON.stringify(lr),aesKey)));
//
    //    const lUrl = 'https://d-agentz.avrist.com/backend/api/v1/customer/authentication/obtain-token/';
    //    var l = new XMLHttpRequest();
    //    l.open("POST", lUrl, true);
    //    
    //    l.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    //    l.setRequestHeader("Authorization", atob(Cookies.get('mCvVeOa4DW')));
    //    l.send(JSON.stringify(ltd));
    //    l.onreadystatechange = function() {
    //        if (l.readyState == 4) {
    //            var resp = l.responseText;
    //            var dAES = decStrAES(atob((JSON.parse(resp)).r),aesKey);
    //            console.log("Decrypted R (AES): "+dAES);
    //            var r = JSON.parse(dAES);
    //            if (r.html_response_code == 200) {
    //                var ctn = r['content']['customer']['user'];
    //                console.log("R deep to User: "+r['content']['customer']['user']);
    //                var r_url = ctn.redirect_url;
    //                console.log("Redirect URL: "+r_url);
    //                Cookies.set('fseWVxaZME', r_url, { expires: inTenMinutes });
    //                if(r_url != null || r_url != "") {
    //                    setIdentity(u.username);
    //                    // For redirect
    //                    window.location.href = r_url;
    //                }
    //            } else {
    //                var msg = document.getElementById('divMessageMain');
    //                msg.style.display = "flex";
    //                msg.getElementsByTagName('img')[0].src = "/Icon/icon_fail.svg";
    //                msg.getElementsByTagName('dt')[0].innerHTML = r.message_title;
    //                msg.getElementsByTagName('dd')[0].innerHTML = r.content.detail;
    //                console.log("Detail Message: "+r.content.detail);
    //            }
    //        }
    //    }; 
    //} else {
    //    window.location.href = Cookies.get('fseWVxaZME');
    //}
    var lr = new ReqM({
        user_id: "",
        device_id: navigator.userAgent,
        ip_address: _engine_location,
        culture: navigator.language,
        content: u
    });
    
    var aesKey = decStr(Cookies.get('rS96YifBNi'),Cookies.get('fVATVBGmHw'));
    var ltd = new TDM(btoa(encStrAES(JSON.stringify(lr),aesKey)));
    const lUrl = 'https://d-agentz.avrist.com/backend/api/v1/customer/authentication/obtain-token/';
    var l = new XMLHttpRequest();
    l.open("POST", lUrl, true);
    
    l.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    l.setRequestHeader("Authorization", atob(Cookies.get('mCvVeOa4DW')));
    l.send(JSON.stringify(ltd));
    l.onreadystatechange = function() {
        if (l.readyState == 4) {
            var resp = l.responseText;
            var dAES = decStrAES(atob((JSON.parse(resp)).r),aesKey);
            console.log("Decrypted R (AES): "+dAES);
            var r = JSON.parse(dAES);
            if (r.html_response_code == 200) {
                var ctn = r['content']['customer']['user'];
                console.log("R deep to User: "+r['content']['customer']['user']);
                var r_url = ctn.redirect_url;
                console.log("Redirect URL: "+r_url);
                Cookies.set('fseWVxaZME', r_url, { expires: inTenMinutes });
                if(r_url != null || r_url != "") {
                    setIdentity(u.username);
                    // For redirect
                    window.location.href = r_url;
                }
            } else {
                var msg = document.getElementById('divMessageMain');
                msg.style.display = "flex";
                msg.getElementsByTagName('img')[0].src = "/Icon/icon_fail.svg";
                msg.getElementsByTagName('dt')[0].innerHTML = r.message_title;
                msg.getElementsByTagName('dd')[0].innerHTML = r.content.detail;
                console.log("Detail Message: "+r.content.detail);
            }
        }
    };
}

function handleSubmit(e){
    var kc = (e.keyCode ? e.keyCode : e.which);
    if (kc == '13') {
        submitLogin();
    }
}

function submitForgot() {
    window.location.href = 'https://d-custz.avrist.com/#/security/forgotpassword';
}

function submitRegister() {
    window.location.href = 'https://d-custz.avrist.com/#/registration/basicinformation';
}
