import {} from 'jsencrypt/bin/jsencrypt';
import {} from 'secure-ls';

export var aesjs = require('aes-js');
export var seclc = new SecureLS({encodingType: 'aes'});
export var store = require('store/dist/store.legacy.min'); // require('store');
var expirePlugin = require('store/plugins/expire');
store.addPlugin(expirePlugin);

class ReqM {
    constructor(m) {
        this.user_id = m.user_id;
        this.device_id = m.device_id;
        this.ip_address = m.ip_address;
        this.culture = m.culture;
        this.content = m.content;
    }
}

class ResM {
    constructor(m) {
        this.html_response_code = m.html_response_code;
        this.service_response_code = m.service_response_code;
        this.message_title = m.message_title;
        this.message_content = m.message_content;
        this.content = m.content;
        this.culture = m.culture;
    }
}

class TDM {
    constructor(c) {
        this.r = c;
    }
}

class HM {
    constructor(m) {
        this.rsa_public_key = m.rsa_public_key;
        this.transaction_aes_key = m.transaction_aes_key;
    }
}

class HResM {
    constructor(m) {
        this.token = m.token;
        this.transaction_aes_key = m.transaction_aes_key;
    }
}

class UM {
    constructor(m) {
        this.username = m.username;
        this.password = m.password;
    }
}

class CryptoFunc {
    constructor()
	{
		this.enc = new JSEncrypt();
		this.dec = new JSEncrypt();
    }
    
    get hasKey()
    {
        var hasPublic = seclc.get('h2uVuK3hTI') !== "" || seclc.get('h2uVuK3hTI') !== null;
        var hasPrivate = seclc.get('fVATVBGmHw') !== "" || seclc.get('fVATVBGmHw') !== null;
        return hasPublic && hasPrivate;
    }

    get hasAESKey()
    {
        return store.get('rS96YifBNi') !== "" || store.get('rS96YifBNi') !== null;
    }

    get getRSAPublic()
    {
        return seclc.get('h2uVuK3hTI');
    }

    get getRSAPrivate()
    {
        return seclc.get('fVATVBGmHw');
    }

    genKey()
    {
        const crypto = new JSEncrypt({default_key_size: 1024});
        crypto.getKey();
        const privateKey = crypto.getPrivateKey();
        const publicKey = crypto.getPublicKey();
        seclc.set('h2uVuK3hTI', publicKey);
        seclc.set('fVATVBGmHw', privateKey);
    }

    clnAESKey()
    {
        store.remove('rS96YifBNi');
    }

    clnLoginAESKey()
	{
		store.remove('esGmEcQUG7');
    }

    setAESKey(k)
	{
		this.AESKey = k;
		store.set('rS96YifBNi', k, new Date().getTime() + 3600);
    }

	setLoginAESKey(k)
	{
		this.AESKey = k;
		store.set('esGmEcQUG7', k, new Date().getTime() + 3600);
    }

    get getToken()
    {
        return store.get('mCvVeOa4DW');
    }

    get hasToken()
    {
        return this.getToken() !== "" || this.getToken() !== null;
    }

    setToken(k)
    {
        store.set('mCvVeOa4DW', k, new Date().getTime() + 3600);
    }

    get getRedirectUrl()
    {
        return store.get('fseWVxaZME');
    }
    
    get hasRedirectUrl()
    {
        return this.getRedirectUrl() !== "" || this.getRedirectUrl() !== null;
    }

    setRedirectUrl(url)
    {
        store.set('fseWVxaZME', url, new Date().getTime() + 3600);
    }

    encStr(strRaw, strPubKey)
	{
		this.enc.setPublicKey(strPubKey);
		const strRslt = this.enc.encrypt(strRaw);
		return strRslt;
	}

	decStr(strRaw, strPrivKey)
	{
		this.dec.setPrivateKey(strPrivKey);
		const strRslt = this.dec.decrypt(strRaw);
		return strRslt;
    }
    
    encStrAES(strRaw, strKey)
	{
		const k = aesjs.utils.hex.toBytes(strKey);
		const pt = aesjs.utils.utf8.toBytes(strRaw);
		const enc = new aesjs.ModeOfOperation.ctr(k, new aesjs.Counter(k));
		let cp = enc.encrypt(pt);
		cp = aesjs.utils.hex.fromBytes(cp);
		return cp;
    }
    
    decStrAES(strHex, strKey)
	{
		const k = aesjs.utils.hex.toBytes(strKey);
		const cp = aesjs.utils.hex.toBytes(strHex);
		const dec = new aesjs.ModeOfOperation.ctr(k, new aesjs.Counter(k));
		let pt = dec.decrypt(cp);
		pt = aesjs.utils.utf8.fromBytes(pt);
		return pt;
    }
    
    get getAESKey()
    {
        return this.decStr(store.get('rS96YifBNi'), this.getRSAPrivate());
    }

    get getLoginAESKey()
    {
        return this.decStr(store.get('esGmEcQUG7'), this.getRSAPrivate());
    }

    get handshakeUrl()
    {
        return 'https://d-agentz.avrist.com/backend/api/v1/generate-token/';
    }

    get loginUrl()
    {
        return 'https://d-agentz.avrist.com/backend/api/v1/customer/authentication/obtain-token/';
    }
}

export const cf = new CryptoFunc();

export function secureGuysInitialize()
{
    store.removeExpiredKeys();
    if (!cf.hasKey())
        cf.genKey();
}

export function secureGuysHandshake()
{
    store.removeExpiredKeys();
    if (cf.hasAESKey())
        cf.clnAESKey();

    var handshakeRequest = new ReqM({
        user_id: "",
            device_id: navigator.userAgent,
            ip_address: _engine_location,
            culture: navigator.language,
            content: new HM({rsa_public_key: cf.getRSAPublic(), transaction_aes_key: ""})
    });

    var handshakeBody = new TDM(btoa(JSON.stringify(handshakeRequest)));

    var httpHandshake = new XMLHttpRequest();
    httpHandshake.open("POST", cf.handshakeUrl(), true);
    httpHandshake.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    httpHandshake.send(JSON.stringify(handshakeBody));
    httpHandshake.onreadystatechange = function() {
        if (httpHandshake.readyState === 4 && httpHandshake.status === 201) {
            var resp = JSON.parse(httpHandshake.responseText);
            var r = JSON.parse(atob(resp.r));
            var handshakeResponse = new HResM(r.content);
            cf.setAESKey(handshakeResponse.transaction_aes_key)
            cf.setToken(btoa(handshakeResponse.token));
        }
    };
}

export function secureGuysSafeLogin(user)
{
    secureGuysHandshake();

    var loginRequest = new ReqM({
        user_id: "",
        device_id: navigator.userAgent,
        ip_address: _engine_location,
        culture: navigator.language,
        content: user
    });

    var loginBody = new TDM(btoa(cf.encStrAES(JSON.stringify(loginRequest),cf.getAESKey())));

    var httpLogin = new XMLHttpRequest();
    httpLogin.open("POST", cf.loginUrl(), true);
    httpLogin.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    httpLogin.setRequestHeader("Authorization", atob(cf.getToken()));
    httpLogin.send(JSON.stringify(loginBody));
    httpLogin.onreadystatechange = function() {
        if (httpLogin.readyState === 4) {
            var resp = httpLogin.responseText;
            var dAES = cf.decStrAES(atob((JSON.parse(resp)).r),cf.getAESKey());
            console.log("Decrypted R (AES): "+dAES);
            var r = JSON.parse(dAES);
            if (r.html_response_code === 200) {
                var ctn = r['content']['customer']['user'];
                console.log("R deep to User: "+r['content']['customer']['user']);
                var r_url = ctn.redirect_url;
                console.log("Redirect URL: "+r_url);
                cf.setRedirectUrl(r_url);
                if(r_url != null || r_url !== "") {
                    // For redirect
                    window.location.href = r_url;
                }
            } else {
                console.log("Detail Message: "+r.content.detail);
            }
        }
    }; 
}

$(document).ready(function() {
    secureGuysInitialize();
    if (cf.hasRedirectUrl()) {

    } else {
        
    }
});



export function submitLogin() {
    if (!cf.hasRedirectUrl()) {
        var u = new UM({
            username: document.getElementById("loginUsername").value,
            password: document.getElementById("loginPassword").value
        });

        secureGuysSafeLogin(u);
    }
}

export function handleSubmit(e){
    var kc = (e.keyCode ? e.keyCode : e.which);
    if (kc === '13') {
        submitLogin();
    }
}

export function submitForgot() {
    window.location.href = 'https://d-custz.avrist.com/#/security/forgotpassword';
}

export function submitRegister() {
    window.location.href = 'https://d-custz.avrist.com/#/registration/basicinformation';
}

