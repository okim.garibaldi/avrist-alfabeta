const path = require('path');

module.exports = {
    mode: 'production',
    entry: './src/index.js',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'secure-guys.js'
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx|mjs)$/,
                use: [
                    'babel-loader'
                ]
            }
        ]
    }
};