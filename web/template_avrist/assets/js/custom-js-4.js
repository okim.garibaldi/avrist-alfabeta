/* ==========================================================================
   JS for awards fund-fact detail
   ========================================================================== */

// Navbar active state
$('#navTentang').addClass('active');

// Animation on load
document.addEventListener("DOMContentLoaded", function(event) {
    $(".loader").fadeOut('slow');
    $(".loader-wrapper").fadeOut("slow");
    //  $(".anim-1").addClass("animated fadeInLeft delayp10");
    // $(".anim-2").addClass("animated fadeInUp delayp12");
    //$(".anim-3").addClass("animated fadeInUp delayp14");  
});