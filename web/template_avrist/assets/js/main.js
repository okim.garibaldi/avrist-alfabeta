/* ==========================================================================
   General
   ========================================================================== */

/* Viewport Animations
-------------------------------------------------- */
$(document).ready(function() {
  // Attention Seekers
  $('.vp-pulse').viewportChecker({classToAdd: 'visible animated pulse', offset: 100});
  $('.vp-swing').viewportChecker({classToAdd: 'visible animated swing', offset: 100});

  // Fade In
  $('.vp-fadein').viewportChecker({classToAdd: 'visible animated fadeIn', offset: 100});
  $('.vp-fadeinup').viewportChecker({classToAdd: 'visible animated fadeInUp', offset: 100});
  $('.vp-fadeinright').viewportChecker({classToAdd: 'visible animated fadeInRight', offset: 100});
  $('.vp-fadeindown').viewportChecker({classToAdd: 'visible animated fadeInDown', offset: 100});
  $('.vp-fadeinleft').viewportChecker({classToAdd: 'visible animated fadeInLeft', offset: 100});

  // Slide In
  $('.vp-slideinup').viewportChecker({classToAdd: 'visible animated slideInUp', offset: 100});
  $('.vp-slideinright').viewportChecker({classToAdd: 'visible animated slideInRight', offset: 100});
  $('.vp-slideindown').viewportChecker({classToAdd: 'visible animated slideInDown', offset: 100});
  $('.vp-slideinleft').viewportChecker({classToAdd: 'visible animated slideInLeft', offset: 100});

  // Rotate In
  $('.vp-rotatein').viewportChecker({classToAdd: 'visible animated rotateIn', offset: 100});
  $('.vp-rotateinupright').viewportChecker({classToAdd: 'visible animated rotateInUpRight', offset: 100});
  $('.vp-rotateinupleft').viewportChecker({classToAdd: 'visible animated rotateInUpLeft', offset: 100});
  $('.vp-rotateindownright').viewportChecker({classToAdd: 'visible animated rotateInDownRight', offset: 100});
  $('.vp-rotateindownleft').viewportChecker({classToAdd: 'visible animated rotateInDownLeft', offset: 100});

  // Zoom In
  $('.vp-zoomin').viewportChecker({classToAdd: 'visible animated zoomIn', offset: 100});
  $('.vp-zoominup').viewportChecker({classToAdd: 'visible animated zoomInUp', offset: 100});
  $('.vp-zoominright').viewportChecker({classToAdd: 'visible animated zoomInRight', offset: 100});
  $('.vp-zoomindown').viewportChecker({classToAdd: 'visible animated zoomInDown', offset: 100});
  $('.vp-zoominleft').viewportChecker({classToAdd: 'visible animated zoomInLeft', offset: 100});

  // Specials
  $('.vp-jackinthebox').viewportChecker({classToAdd: 'visible animated jackInTheBox', offset: 100});
  $('.vp-rollin').viewportChecker({classToAdd: 'visible animated rollIn', offset: 100});
  $('.vp-rollout').viewportChecker({classToAdd: 'visible animated rollOut', offset: 100});
}); 


/* Smooth Scroll
-------------------------------------------------- */
$('a.smooth-scroll').click(function() {
  if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
    var target = $(this.hash);
    target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
    if (target.length) {
      $('html, body').animate({
        scrollTop: target.offset().top - 120
      }, 1200);
      return false;
    }
  }
});

/* Parallax
-------------------------------------------------- */
$(document).ready(function() {
  $('.parallax-window').parallax({parallax: 'scroll'});
}); 


/* ==========================================================================
   Navbar
   ========================================================================== */

/* Navbar Scroll
-------------------------------------------------- */
// var logo = ["assets/img/brand/logo_avrist.png", "assets/img/brand/logo_avrist.png"];

// $(window).on("scroll", function() {
//   if ($(window).scrollTop() > 10) {
//     $(".navbar-avrist").addClass("navbar-scroll");
//     $('.navbar-avrist .navbar-brand img').attr('src', logo[0]);
//   } else {
//     //remove the background property so it comes transparent again (defined in your css)
//     $(".navbar-avrist").removeClass("navbar-scroll");
//     $('.navbar-avrist .navbar-brand img').attr('src', logo[1]);
//   }
// });

// mega menu js
$('.mega-menu').hide();
$('#navLifeInsurance').click(function(){
  $('.dropdown-menu-1').addClass("show");
  $('.mega-menu').hide();
  $('#megaMenuLifeInsurance').toggle();
});

$('#navGeneralInsurance').click(function(){
  $('.dropdown-menu-1').addClass("show");
  $('.mega-menu').hide();
  $('#megaMenuGeneralInsurance').toggle();
});

$('#navAssetManagement').click(function(){
  $('.dropdown-menu-1').addClass("show");
  $('.mega-menu').hide();
  $('#megaMenuAssetManagement').toggle();
});

$(document).click(function(){
  $(".dropdown-menu-1").hide();
  $(".mega-menu").hide();
});

$(document).click(function(){
  $(".dropdown-menu-2").hide();
  $(".mega-menu").hide();
});

$(document).click(function(){
  $(".dropdown-menu-3").hide();
  $(".mega-menu").hide();
});

$(".general-insurance").hover(function() {
  $(".dropdown-menu-general").toggleClass("active-gi");
});

$(".asset-management").hover(function() {
  $(".dropdown-menu-general").toggleClass("active-am");
});

// mobile menu slide from the left
$('.navbar-slide').hide();

// mobile menu slide from the left
$('.navbar-toggler').click(function(){
  $('.navbar-slide').toggle( "slide" );
  $('.icon-bar-1').addClass('animate');
  $('.icon-bar-2').addClass('animate');
  $('.icon-bar-3').addClass('animate');
});

$('.navbar-slide-close').click(function(){
  $('.navbar-slide').toggle( "slide" );
});

//mega menu mobile Life Insurance
$('#nav-slide_product-1').click(function(){
    $('#product-slide-1').toggle( "slide" );
  });

  $('#product-back-1').click(function(){
    $('#product-slide-1').toggle( "slide" );
  });

//mega menu mobile Life Insurance
$('#nav-slide_product-tentang-1').click(function(){
  $('#product-slide-tentang-1').toggle( "slide" );
});

$('#product-back-tentang-1').click(function(){
  $('#product-slide-tentang-1').toggle( "slide" );
});

//mega menu mobile Life Insurance
$('#nav-slide_product-bantuan-1').click(function(){
  $('#product-slide-bantuan-1').toggle( "slide" );
});

$('#product-back-bantuan-1').click(function(){
  $('#product-slide-bantuan-1').toggle( "slide" );
});

//mega menu mobile General Insurance
$('#nav-slide_product-2').click(function(){
    $('#product-slide-2').toggle( "slide" );
  });

  $('#product-back-2').click(function(){
    $('#product-slide-2').toggle( "slide" );
  });

  //mega menu mobile General Insurance
$('#nav-slide_product-tentang-2').click(function(){
  $('#product-slide-tentang-2').toggle( "slide" );
});

$('#product-back-tentang-2').click(function(){
  $('#product-slide-tentang-2').toggle( "slide" );
});

  //mega menu mobile General Insurance
  $('#nav-slide_product-bantuan-2').click(function(){
    $('#product-slide-bantuan-2').toggle( "slide" );
  });
  
  $('#product-back-bantuan-2').click(function(){
    $('#product-slide-bantuan-2').toggle( "slide" );
  });

//mega menu mobile Asset Management
$('#nav-slide_product-3').click(function(){
    $('#product-slide-3').toggle( "slide" );
  });

  $('#product-back-3').click(function(){
    $('#product-slide-3').toggle( "slide" );
  });

  //mega menu mobile Asset Management
$('#nav-slide_product-tentang-3').click(function(){
  $('#product-slide-tentang-3').toggle( "slide" );
});

$('#product-back-tentang-3').click(function(){
  $('#product-slide-tentang-3').toggle( "slide" );
});

  //mega menu mobile Asset Management
  $('#nav-slide_product-bantuan-3').click(function(){
    $('#product-slide-bantuan-3').toggle( "slide" );
  });
  
  $('#product-back-bantuan-3').click(function(){
    $('#product-slide-bantuan-3').toggle( "slide" );
  });


/* ==========================================================================
   Dropdown
   ========================================================================== */

$('#megamenu-1').hide();
$('#navSolusiHover').click(function(e){
  // console.log('masuk');
  // $('#megamenu-1').toggle();
  e.preventDefault();
});
$("#navSolusiHover").hover(function(){
  $('#megamenu-1').css("display","block");
  }, function(){
  $(this).css("background-color", "pink");
});

$("#navLifeInsurance").hover(function(){
  $('#megaMenuLifeInsurance').css("display","block");
});

$("#navGeneralInsurance").hover(function(){
  $('#megaMenuGeneralInsurance').css("display","block");
});


$('.slider-mobile').owlCarousel({
    loop:false,
    margin:30,
    dots:true,
    responsive:{
        0:{
            items:1
        },
        1000:{
            items:3
        }
    }
})