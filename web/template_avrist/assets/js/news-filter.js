/* ==========================================================================
   JS for news filtering and searching
   ========================================================================== */

var input = document.getElementById("searchField");
input.addEventListener("keyup", function(event) {
    if (event.keyCode === 13) {
        event.preventDefault();
        var search = input.value;
        var sBIndex = document.getElementById("validationCustom04").selectedIndex;
        var sortBy = document.getElementById("validationCustom04").options;
        var cIndex = document.getElementById("validationCustom05").selectedIndex;
        var category = document.getElementById("validationCustom05").options;
        
        var form = document.createElement('form');
        document.body.appendChild(form);
        form.method = 'post';
        form.action = window.location;
        var sortByInput = document.createElement('input');
        sortByInput.type = 'hidden';
        sortByInput.name = 'sortBy';
        sortByInput.value = sortBy[sBIndex].value;
        form.appendChild(sortByInput);
        var categoryInput = document.createElement('input');
        categoryInput.type = 'hidden';
        categoryInput.name = 'category';
        categoryInput.value = category[cIndex].value;
        form.appendChild(categoryInput);
        var searchInput = document.createElement('input');
        searchInput.type = 'hidden';
        searchInput.name = 'search';
        searchInput.value = search;
        form.appendChild(searchInput);

        form.submit();
    }
});