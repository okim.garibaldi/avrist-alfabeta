/* ==========================================================================
   JS for googlemap
   ===================================================================== ===== */

function initialize() {
    // Tampilan peta
    var propertiPeta = {
        center: new google.maps.LatLng(-2.3937573, 116.7517431), //titik posisi maps di halaman browser
        zoom: 5,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var peta = new google.maps.Map(document.getElementById("googleMap"), propertiPeta);

    // Membuat Marker Maps

    var coordinate = <?php echo $coordinate; ?>;
    var marker = new google.maps.Marker({
        position: new google.maps.LatLng(-6.17702, 106.7267727), //titik lokasi maps
        map: peta,
    });

    var marker = new google.maps.Marker({
        position: new google.maps.LatLng(-6.952771, 110.191597), //titik lokasi maps
        map: peta,
    });

}
// event jendela di-load  
google.maps.event.addDomListener(window, 'load', initialize);