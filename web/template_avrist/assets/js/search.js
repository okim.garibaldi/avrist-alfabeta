/* ==========================================================================
   JS for search
   ========================================================================== */
window.onload = function()
{
    var input = document.getElementById("searchBox");
    input.addEventListener("keyup", function(event){
        if (event.keyCode === 13) {
            event.preventDefault();
            search(input);
        }
    });
    var button = document.getElementById("cari");
    button.addEventListener("click", function(event){
        search(input);
    });
}

function search(input) {
    var search = input.value;

    var form = document.createElement('form');
    document.body.appendChild(form);
    form.method = 'post';
    form.action = window.location;
    var searchInput = document.createElement('input');
    searchInput.type = 'hidden';
    searchInput.name = 's';
    searchInput.value = search;
    form.appendChild(searchInput);

    form.submit();   
}