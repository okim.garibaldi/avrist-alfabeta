/* ==========================================================================
   JS for global search
   ========================================================================== */

var navSearch = document.getElementById("nav-search");
navSearch.addEventListener("keyup", function(event) {
    if (event.keyCode === 13) {
        globalSearch(event);
    }
});

//var mobileNavSearch = document.getElementById("mobile-nav-search");
//mobileNavSearch.addEventListener("keyup", function(event) {
//    if (event.keyCode === 13) {
//        globalSearch(event);
//    }
//});

function globalSearch(event)
{
    event.preventDefault();
    var search = navSearch.value;
    
    var form = document.createElement('form');
    document.body.appendChild(form);
    form.method = 'post';
    form.action = '/search/product-search';
    var searchInput = document.createElement('input');
    searchInput.type = 'hidden';
    searchInput.name = 's';
    searchInput.value = search;
    form.appendChild(searchInput);

    form.submit();
}