/* ==========================================================================
   JS for Our Location
   ========================================================================== */

window.onload = function()
{

    var urlInput = document.getElementsByClassName("url-input");
    for(var i = 0; i < urlInput.length; i++)
    {
        urlInput[i].value = window.location;
    }
    var input = document.getElementById("searchBox");
    if(input){
        input.addEventListener("keyup", function(event){
            if (event.keyCode === 13) {
                event.preventDefault();
                search(input);
            }
        });
    }
    var button = document.getElementById("cari");
    if(button){
        button.addEventListener("click", function(event){
            search(input);
        });  
    }
    
}

function search(input) {
    var search = input.value;
    if (document.getElementById("branchFilter"))
    {
    	var branchFilterIndex = document.getElementById("branchFilter").selectedIndex;
        var branchFilterOptions = document.getElementById("branchFilter").options;
        var branchFilterValue = branchFilterOptions[branchFilterIndex].value;
    }
    else
    {
    	var branchFilterValue = "NULL";
    }

    if (document.getElementById("filterBengkel"))
    {
    	var filterBengkelIndex = document.getElementById("filterBengkel").selectedIndex;
        var filterBengkelOptions = document.getElementById("filterBengkel").options;
        var filterBengkelValue = filterBengkelOptions[filterBengkelIndex].value;
    }
    else
    {
    	var filterBengkelValue = "NULL";
    }

    if (document.getElementById("groupFilter"))
    {
    	var groupFilterIndex = document.getElementById("groupFilter").selectedIndex;
        var groupFilterOptions = document.getElementById("groupFilter").options;
        var groupFilterValue = groupFilterOptions[groupFilterIndex].value;
    }
    else
    {
    	var groupFilterValue = "NULL";
    }

    if (document.getElementById("hospitalCategory"))
    {
    	var hospitalCategoryIndex = document.getElementById("hospitalCategory").selectedIndex;
        var hospitalCategoryOptions = document.getElementById("hospitalCategory").options;
        var hospitalCategoryValue = hospitalCategoryOptions[hospitalCategoryIndex].value;
    }
    else
    {
    	var hospitalCategoryValue  = "NULL";
    }

    var form = document.createElement('form');
    document.body.appendChild(form);
    form.method = 'post';
    form.action = window.location;
    var branchFilter = document.createElement('input');
    branchFilter.type = 'hidden';
    branchFilter.name = 'branchFilter';
    branchFilter.value = branchFilterValue;
    form.appendChild(branchFilter);
    var filterBengkel = document.createElement('input');
    filterBengkel.type = 'hidden';
    filterBengkel.name = 'filterBengkel';
    filterBengkel.value = filterBengkelValue;
    form.appendChild(filterBengkel);
    var groupFilter = document.createElement('input');
    groupFilter.type = 'hidden';
    groupFilter.name = 'groupFilter';
    groupFilter.value = groupFilterValue;
    form.appendChild(groupFilter);
    var hospitalCategory = document.createElement('input');
    hospitalCategory.type = 'hidden';
    hospitalCategory.name = 'hospitalCategory';
    hospitalCategory.value = hospitalCategoryValue;
    form.appendChild(hospitalCategory);
    var searchInput = document.createElement('input');
    searchInput.type = 'hidden';
    searchInput.name = 's';
    searchInput.value = search;
    form.appendChild(searchInput);    

    form.submit();   
}