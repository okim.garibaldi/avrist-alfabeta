<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the public 'AppBundle\Controller\Career\CareerController' shared autowired service.

return $this->services['AppBundle\Controller\Career\CareerController'] = new \AppBundle\Controller\Career\CareerController();
