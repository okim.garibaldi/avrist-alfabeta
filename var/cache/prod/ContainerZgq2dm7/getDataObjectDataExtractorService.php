<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the private 'Pimcore\Translation\ExportDataExtractorService\DataExtractor\DataObjectDataExtractor' shared autowired service.

return $this->services['Pimcore\Translation\ExportDataExtractorService\DataExtractor\DataObjectDataExtractor'] = new \Pimcore\Translation\ExportDataExtractorService\DataExtractor\DataObjectDataExtractor();
