<?php

class MigrationManager_012d3a8 extends \Pimcore\Migrations\MigrationManager implements \ProxyManager\Proxy\VirtualProxyInterface
{
    private $valueHolder012d3a8 = null;
    private $initializer012d3a8 = null;
    private static $publicProperties012d3a8 = [
        
    ];
    public function getConfiguration(string $migrationSet) : \Pimcore\Migrations\Configuration\Configuration
    {
        $this->initializer012d3a8 && ($this->initializer012d3a8->__invoke($valueHolder012d3a8, $this, 'getConfiguration', array('migrationSet' => $migrationSet), $this->initializer012d3a8) || 1) && $this->valueHolder012d3a8 = $valueHolder012d3a8;
        return $this->valueHolder012d3a8->getConfiguration($migrationSet);
    }
    public function getVersion(string $migrationSet, string $versionId) : \Doctrine\DBAL\Migrations\Version
    {
        $this->initializer012d3a8 && ($this->initializer012d3a8->__invoke($valueHolder012d3a8, $this, 'getVersion', array('migrationSet' => $migrationSet, 'versionId' => $versionId), $this->initializer012d3a8) || 1) && $this->valueHolder012d3a8 = $valueHolder012d3a8;
        return $this->valueHolder012d3a8->getVersion($migrationSet, $versionId);
    }
    public function getBundleConfiguration(\Symfony\Component\HttpKernel\Bundle\BundleInterface $bundle) : \Pimcore\Migrations\Configuration\Configuration
    {
        $this->initializer012d3a8 && ($this->initializer012d3a8->__invoke($valueHolder012d3a8, $this, 'getBundleConfiguration', array('bundle' => $bundle), $this->initializer012d3a8) || 1) && $this->valueHolder012d3a8 = $valueHolder012d3a8;
        return $this->valueHolder012d3a8->getBundleConfiguration($bundle);
    }
    public function getBundleVersion(\Symfony\Component\HttpKernel\Bundle\BundleInterface $bundle, string $versionId) : \Doctrine\DBAL\Migrations\Version
    {
        $this->initializer012d3a8 && ($this->initializer012d3a8->__invoke($valueHolder012d3a8, $this, 'getBundleVersion', array('bundle' => $bundle, 'versionId' => $versionId), $this->initializer012d3a8) || 1) && $this->valueHolder012d3a8 = $valueHolder012d3a8;
        return $this->valueHolder012d3a8->getBundleVersion($bundle, $versionId);
    }
    public function getInstallConfiguration(\Pimcore\Migrations\Configuration\Configuration $configuration, \Pimcore\Extension\Bundle\Installer\MigrationInstallerInterface $installer) : \Pimcore\Migrations\Configuration\InstallConfiguration
    {
        $this->initializer012d3a8 && ($this->initializer012d3a8->__invoke($valueHolder012d3a8, $this, 'getInstallConfiguration', array('configuration' => $configuration, 'installer' => $installer), $this->initializer012d3a8) || 1) && $this->valueHolder012d3a8 = $valueHolder012d3a8;
        return $this->valueHolder012d3a8->getInstallConfiguration($configuration, $installer);
    }
    public function executeVersion(\Doctrine\DBAL\Migrations\Version $version, bool $up = true, bool $dryRun = false) : array
    {
        $this->initializer012d3a8 && ($this->initializer012d3a8->__invoke($valueHolder012d3a8, $this, 'executeVersion', array('version' => $version, 'up' => $up, 'dryRun' => $dryRun), $this->initializer012d3a8) || 1) && $this->valueHolder012d3a8 = $valueHolder012d3a8;
        return $this->valueHolder012d3a8->executeVersion($version, $up, $dryRun);
    }
    public function markVersionAsMigrated(\Doctrine\DBAL\Migrations\Version $version, bool $includePrevious = true)
    {
        $this->initializer012d3a8 && ($this->initializer012d3a8->__invoke($valueHolder012d3a8, $this, 'markVersionAsMigrated', array('version' => $version, 'includePrevious' => $includePrevious), $this->initializer012d3a8) || 1) && $this->valueHolder012d3a8 = $valueHolder012d3a8;
        return $this->valueHolder012d3a8->markVersionAsMigrated($version, $includePrevious);
    }
    public function markVersionAsNotMigrated(\Doctrine\DBAL\Migrations\Version $version)
    {
        $this->initializer012d3a8 && ($this->initializer012d3a8->__invoke($valueHolder012d3a8, $this, 'markVersionAsNotMigrated', array('version' => $version), $this->initializer012d3a8) || 1) && $this->valueHolder012d3a8 = $valueHolder012d3a8;
        return $this->valueHolder012d3a8->markVersionAsNotMigrated($version);
    }
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;
        $reflection = $reflection ?: $reflection = new \ReflectionClass(__CLASS__);
        $instance = (new \ReflectionClass(get_class()))->newInstanceWithoutConstructor();
        \Closure::bind(function (\Pimcore\Migrations\MigrationManager $instance) {
            unset($instance->connection, $instance->configurationFactory);
        }, $instance, 'Pimcore\\Migrations\\MigrationManager')->__invoke($instance);
        $instance->initializer012d3a8 = $initializer;
        return $instance;
    }
    public function __construct(\Pimcore\Db\Connection $connection, \Pimcore\Migrations\Configuration\ConfigurationFactory $configurationFactory)
    {
        static $reflection;
        if (! $this->valueHolder012d3a8) {
            $reflection = $reflection ?: new \ReflectionClass('Pimcore\\Migrations\\MigrationManager');
            $this->valueHolder012d3a8 = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Pimcore\Migrations\MigrationManager $instance) {
            unset($instance->connection, $instance->configurationFactory);
        }, $this, 'Pimcore\\Migrations\\MigrationManager')->__invoke($this);
        }
        $this->valueHolder012d3a8->__construct($connection, $configurationFactory);
    }
    public function & __get($name)
    {
        $this->initializer012d3a8 && ($this->initializer012d3a8->__invoke($valueHolder012d3a8, $this, '__get', ['name' => $name], $this->initializer012d3a8) || 1) && $this->valueHolder012d3a8 = $valueHolder012d3a8;
        if (isset(self::$publicProperties012d3a8[$name])) {
            return $this->valueHolder012d3a8->$name;
        }
        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder012d3a8;
            $backtrace = debug_backtrace(false);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    get_parent_class($this),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
            return;
        }
        $targetObject = $this->valueHolder012d3a8;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __set($name, $value)
    {
        $this->initializer012d3a8 && ($this->initializer012d3a8->__invoke($valueHolder012d3a8, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer012d3a8) || 1) && $this->valueHolder012d3a8 = $valueHolder012d3a8;
        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder012d3a8;
            return $targetObject->$name = $value;
            return;
        }
        $targetObject = $this->valueHolder012d3a8;
        $accessor = function & () use ($targetObject, $name, $value) {
            return $targetObject->$name = $value;
        };
        $backtrace = debug_backtrace(true);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __isset($name)
    {
        $this->initializer012d3a8 && ($this->initializer012d3a8->__invoke($valueHolder012d3a8, $this, '__isset', array('name' => $name), $this->initializer012d3a8) || 1) && $this->valueHolder012d3a8 = $valueHolder012d3a8;
        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder012d3a8;
            return isset($targetObject->$name);
            return;
        }
        $targetObject = $this->valueHolder012d3a8;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();
        return $returnValue;
    }
    public function __unset($name)
    {
        $this->initializer012d3a8 && ($this->initializer012d3a8->__invoke($valueHolder012d3a8, $this, '__unset', array('name' => $name), $this->initializer012d3a8) || 1) && $this->valueHolder012d3a8 = $valueHolder012d3a8;
        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder012d3a8;
            unset($targetObject->$name);
            return;
        }
        $targetObject = $this->valueHolder012d3a8;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();
        return $returnValue;
    }
    public function __clone()
    {
        $this->initializer012d3a8 && ($this->initializer012d3a8->__invoke($valueHolder012d3a8, $this, '__clone', array(), $this->initializer012d3a8) || 1) && $this->valueHolder012d3a8 = $valueHolder012d3a8;
        $this->valueHolder012d3a8 = clone $this->valueHolder012d3a8;
    }
    public function __sleep()
    {
        $this->initializer012d3a8 && ($this->initializer012d3a8->__invoke($valueHolder012d3a8, $this, '__sleep', array(), $this->initializer012d3a8) || 1) && $this->valueHolder012d3a8 = $valueHolder012d3a8;
        return array('valueHolder012d3a8');
    }
    public function __wakeup()
    {
        \Closure::bind(function (\Pimcore\Migrations\MigrationManager $instance) {
            unset($instance->connection, $instance->configurationFactory);
        }, $this, 'Pimcore\\Migrations\\MigrationManager')->__invoke($this);
    }
    public function setProxyInitializer(\Closure $initializer = null)
    {
        $this->initializer012d3a8 = $initializer;
    }
    public function getProxyInitializer()
    {
        return $this->initializer012d3a8;
    }
    public function initializeProxy() : bool
    {
        return $this->initializer012d3a8 && ($this->initializer012d3a8->__invoke($valueHolder012d3a8, $this, 'initializeProxy', array(), $this->initializer012d3a8) || 1) && $this->valueHolder012d3a8 = $valueHolder012d3a8;
    }
    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder012d3a8;
    }
    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder012d3a8;
    }
}
