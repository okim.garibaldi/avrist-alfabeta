<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the private 'service_locator.nunf5gg' shared service.

return $this->services['service_locator.nunf5gg'] = new \Symfony\Component\DependencyInjection\ServiceLocator(array('importDataExtractor' => function () {
    $f = function (\Pimcore\Translation\ImportDataExtractor\ImportDataExtractorInterface $v = null) { return $v; }; return $f(${($_ = isset($this->services['Pimcore\Translation\ImportDataExtractor\ImportDataExtractorInterface']) ? $this->services['Pimcore\Translation\ImportDataExtractor\ImportDataExtractorInterface'] : $this->load('getImportDataExtractorInterfaceService.php')) && false ?: '_'});
}));
