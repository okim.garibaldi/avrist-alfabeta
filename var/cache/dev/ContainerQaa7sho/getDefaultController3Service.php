<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the public 'AppBundle\Controller\General\DefaultController' shared autowired service.

return $this->services['AppBundle\Controller\General\DefaultController'] = new \AppBundle\Controller\General\DefaultController();
