<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the private 'service_locator..bueey4' shared service.

return $this->services['service_locator..bueey4'] = new \Symfony\Component\DependencyInjection\ServiceLocator(array('exportService' => function () {
    $f = function (\Pimcore\Translation\ExportService\ExportServiceInterface $v = null) { return $v; }; return $f(${($_ = isset($this->services['Pimcore\Translation\ExportService\ExportServiceInterface']) ? $this->services['Pimcore\Translation\ExportService\ExportServiceInterface'] : $this->load('getExportServiceInterfaceService.php')) && false ?: '_'});
}));
