<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the private 'Pimcore\Translation\ExportService\Exporter\ExporterInterface' shared autowired service.

return $this->services['Pimcore\Translation\ExportService\Exporter\ExporterInterface'] = new \Pimcore\Translation\ExportService\Exporter\Xliff12Exporter(${($_ = isset($this->services['autowired.Pimcore\Translation\Escaper\Xliff12Escaper']) ? $this->services['autowired.Pimcore\Translation\Escaper\Xliff12Escaper'] : ($this->services['autowired.Pimcore\Translation\Escaper\Xliff12Escaper'] = new \Pimcore\Translation\Escaper\Xliff12Escaper())) && false ?: '_'});
