<?php 

/** 
* Generated at: 2019-12-04T13:23:21+07:00
* Inheritance: no
* Variants: no
* IP: 172.17.0.1


Fields Summary: 
*/ 


return Pimcore\Model\DataObject\ClassDefinition::__set_state(array(
   'id' => '60',
   'name' => 'CareerGroup',
   'description' => NULL,
   'creationDate' => NULL,
   'modificationDate' => 1575440601,
   'userOwner' => 2,
   'userModification' => NULL,
   'parentClass' => NULL,
   'listingParentClass' => '',
   'useTraits' => '',
   'listingUseTraits' => '',
   'allowInherit' => false,
   'allowVariants' => false,
   'showVariants' => false,
   'layoutDefinitions' => NULL,
   'icon' => NULL,
   'previewUrl' => NULL,
   'group' => NULL,
   'showAppLoggerTab' => false,
   'linkGeneratorReference' => NULL,
   'propertyVisibility' => 
  array (
    'grid' => 
    array (
      'id' => true,
      'path' => true,
      'published' => true,
      'modificationDate' => true,
      'creationDate' => true,
    ),
    'search' => 
    array (
      'id' => true,
      'path' => true,
      'published' => true,
      'modificationDate' => true,
      'creationDate' => true,
    ),
  ),
   'dao' => NULL,
));
