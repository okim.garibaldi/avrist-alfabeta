<?php 

/** 
* Generated at: 2019-12-23T07:42:15+07:00
* Inheritance: no
* Variants: no
* Changed by: avristadm (2)
* IP: 172.17.0.1


Fields Summary: 
- siteId [select]
- image1 [image]
- title1 [input]
- publishDate [date]
- block1 [block]
-- imageBlock [image]
-- titleBlock [input]
-- descriptionBlock [wysiwyg]
*/ 

namespace Pimcore\Model\DataObject;



/**
* @method static \Pimcore\Model\DataObject\Awards\Listing getBySiteId ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Awards\Listing getByImage1 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Awards\Listing getByTitle1 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Awards\Listing getByPublishDate ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Awards\Listing getByBlock1 ($value, $limit = 0) 
*/

class Awards extends Concrete implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {



use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $o_classId = "39";
protected $o_className = "Awards";
protected $siteId;
protected $image1;
protected $title1;
protected $publishDate;
protected $block1;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\Awards
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get siteId - Site
* @return string
*/
public function getSiteId () {
	$preValue = $this->preGetValue("siteId"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->siteId;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set siteId - Site
* @param string $siteId
* @return \Pimcore\Model\DataObject\Awards
*/
public function setSiteId ($siteId) {
	$fd = $this->getClass()->getFieldDefinition("siteId");
	$this->siteId = $siteId;
	return $this;
}

/**
* Get image1 - Image 1
* @return \Pimcore\Model\Asset\Image
*/
public function getImage1 () {
	$preValue = $this->preGetValue("image1"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->image1;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set image1 - Image 1
* @param \Pimcore\Model\Asset\Image $image1
* @return \Pimcore\Model\DataObject\Awards
*/
public function setImage1 ($image1) {
	$fd = $this->getClass()->getFieldDefinition("image1");
	$this->image1 = $image1;
	return $this;
}

/**
* Get title1 - Title 1
* @return string
*/
public function getTitle1 () {
	$preValue = $this->preGetValue("title1"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->title1;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set title1 - Title 1
* @param string $title1
* @return \Pimcore\Model\DataObject\Awards
*/
public function setTitle1 ($title1) {
	$fd = $this->getClass()->getFieldDefinition("title1");
	$this->title1 = $title1;
	return $this;
}

/**
* Get publishDate - Publish Date
* @return \Carbon\Carbon
*/
public function getPublishDate () {
	$preValue = $this->preGetValue("publishDate"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->publishDate;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set publishDate - Publish Date
* @param \Carbon\Carbon $publishDate
* @return \Pimcore\Model\DataObject\Awards
*/
public function setPublishDate ($publishDate) {
	$fd = $this->getClass()->getFieldDefinition("publishDate");
	$this->publishDate = $publishDate;
	return $this;
}

/**
* Get block1 - Block1
* @return \Pimcore\Model\DataObject\Data\BlockElement[][]
*/
public function getBlock1 () {
	$preValue = $this->preGetValue("block1"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("block1")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set block1 - Block1
* @param \Pimcore\Model\DataObject\Data\BlockElement[][] $block1
* @return \Pimcore\Model\DataObject\Awards
*/
public function setBlock1 ($block1) {
	$fd = $this->getClass()->getFieldDefinition("block1");
	$this->block1 = $fd->preSetData($this, $block1);
	return $this;
}

protected static $_relationFields = array (
);

protected $lazyLoadedFields = array (
);

}

