<?php 

namespace Pimcore\Model\DataObject\ClaimPage;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\ClaimPage current()
 * @method DataObject\ClaimPage[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "82";
protected $className = "ClaimPage";


}
