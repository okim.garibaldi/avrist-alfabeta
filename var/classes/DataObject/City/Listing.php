<?php 

namespace Pimcore\Model\DataObject\City;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\City current()
 * @method DataObject\City[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "14";
protected $className = "City";


}
