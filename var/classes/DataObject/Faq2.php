<?php 

/** 
* Generated at: 2019-06-20T11:39:28+07:00
* Inheritance: no
* Variants: no
* Changed by: avristadm (2)
* IP: 202.87.248.23


Fields Summary: 
- question [textarea]
- email [input]
- createdDate [datetime]
*/ 

namespace Pimcore\Model\DataObject;



/**
* @method static \Pimcore\Model\DataObject\Faq2\Listing getByQuestion ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Faq2\Listing getByEmail ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Faq2\Listing getByCreatedDate ($value, $limit = 0) 
*/

class Faq2 extends Concrete implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {



use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $o_classId = "18";
protected $o_className = "Faq2";
protected $question;
protected $email;
protected $createdDate;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\Faq2
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get question - question
* @return string
*/
public function getQuestion () {
	$preValue = $this->preGetValue("question"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->question;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set question - question
* @param string $question
* @return \Pimcore\Model\DataObject\Faq2
*/
public function setQuestion ($question) {
	$fd = $this->getClass()->getFieldDefinition("question");
	$this->question = $question;
	return $this;
}

/**
* Get email - email
* @return string
*/
public function getEmail () {
	$preValue = $this->preGetValue("email"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->email;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set email - email
* @param string $email
* @return \Pimcore\Model\DataObject\Faq2
*/
public function setEmail ($email) {
	$fd = $this->getClass()->getFieldDefinition("email");
	$this->email = $email;
	return $this;
}

/**
* Get createdDate - createdDate
* @return \Carbon\Carbon
*/
public function getCreatedDate () {
	$preValue = $this->preGetValue("createdDate"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->createdDate;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set createdDate - createdDate
* @param \Carbon\Carbon $createdDate
* @return \Pimcore\Model\DataObject\Faq2
*/
public function setCreatedDate ($createdDate) {
	$fd = $this->getClass()->getFieldDefinition("createdDate");
	$this->createdDate = $createdDate;
	return $this;
}

protected static $_relationFields = array (
);

protected $lazyLoadedFields = array (
);

}

