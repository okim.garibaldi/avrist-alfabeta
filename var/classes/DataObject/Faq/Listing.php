<?php 

namespace Pimcore\Model\DataObject\Faq;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\Faq current()
 * @method DataObject\Faq[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "11";
protected $className = "Faq";


}
