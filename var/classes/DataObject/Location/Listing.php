<?php 

namespace Pimcore\Model\DataObject\Location;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\Location current()
 * @method DataObject\Location[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "83";
protected $className = "Location";


}
