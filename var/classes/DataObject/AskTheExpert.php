<?php 

/** 
* Generated at: 2019-06-20T11:39:37+07:00
* Inheritance: no
* Variants: no
* Changed by: avristadm (2)
* IP: 202.87.248.23


Fields Summary: 
- name [input]
- email [input]
- message [textarea]
- submitAt [input]
*/ 

namespace Pimcore\Model\DataObject;



/**
* @method static \Pimcore\Model\DataObject\AskTheExpert\Listing getByName ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\AskTheExpert\Listing getByEmail ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\AskTheExpert\Listing getByMessage ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\AskTheExpert\Listing getBySubmitAt ($value, $limit = 0) 
*/

class AskTheExpert extends Concrete implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {



use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $o_classId = "34";
protected $o_className = "AskTheExpert";
protected $name;
protected $email;
protected $message;
protected $submitAt;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\AskTheExpert
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get name - name
* @return string
*/
public function getName () {
	$preValue = $this->preGetValue("name"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->name;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set name - name
* @param string $name
* @return \Pimcore\Model\DataObject\AskTheExpert
*/
public function setName ($name) {
	$fd = $this->getClass()->getFieldDefinition("name");
	$this->name = $name;
	return $this;
}

/**
* Get email - email
* @return string
*/
public function getEmail () {
	$preValue = $this->preGetValue("email"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->email;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set email - email
* @param string $email
* @return \Pimcore\Model\DataObject\AskTheExpert
*/
public function setEmail ($email) {
	$fd = $this->getClass()->getFieldDefinition("email");
	$this->email = $email;
	return $this;
}

/**
* Get message - message
* @return string
*/
public function getMessage () {
	$preValue = $this->preGetValue("message"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->message;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set message - message
* @param string $message
* @return \Pimcore\Model\DataObject\AskTheExpert
*/
public function setMessage ($message) {
	$fd = $this->getClass()->getFieldDefinition("message");
	$this->message = $message;
	return $this;
}

/**
* Get submitAt - submitAt
* @return string
*/
public function getSubmitAt () {
	$preValue = $this->preGetValue("submitAt"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->submitAt;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set submitAt - submitAt
* @param string $submitAt
* @return \Pimcore\Model\DataObject\AskTheExpert
*/
public function setSubmitAt ($submitAt) {
	$fd = $this->getClass()->getFieldDefinition("submitAt");
	$this->submitAt = $submitAt;
	return $this;
}

protected static $_relationFields = array (
);

protected $lazyLoadedFields = array (
);

}

