<?php 

namespace Pimcore\Model\DataObject\AssetQuestion;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\AssetQuestion current()
 * @method DataObject\AssetQuestion[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "24";
protected $className = "AssetQuestion";


}
