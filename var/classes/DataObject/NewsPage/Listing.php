<?php 

namespace Pimcore\Model\DataObject\NewsPage;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\NewsPage current()
 * @method DataObject\NewsPage[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "63";
protected $className = "NewsPage";


}
