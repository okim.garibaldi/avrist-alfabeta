<?php 

namespace Pimcore\Model\DataObject\Menu3;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\Menu3 current()
 * @method DataObject\Menu3[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "49";
protected $className = "Menu3";


}
