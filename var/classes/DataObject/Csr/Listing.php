<?php 

namespace Pimcore\Model\DataObject\Csr;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\Csr current()
 * @method DataObject\Csr[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "26";
protected $className = "Csr";


}
