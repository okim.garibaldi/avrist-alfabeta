<?php 

namespace Pimcore\Model\DataObject\CareerPage;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\CareerPage current()
 * @method DataObject\CareerPage[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "59";
protected $className = "CareerPage";


}
