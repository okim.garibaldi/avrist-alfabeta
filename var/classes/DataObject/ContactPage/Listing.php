<?php 

namespace Pimcore\Model\DataObject\ContactPage;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\ContactPage current()
 * @method DataObject\ContactPage[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "52";
protected $className = "ContactPage";


}
