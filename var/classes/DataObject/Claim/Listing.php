<?php 

namespace Pimcore\Model\DataObject\Claim;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\Claim current()
 * @method DataObject\Claim[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "93";
protected $className = "Claim";


}
