<?php 

/** 
* Generated at: 2020-02-24T15:00:55+07:00
* Inheritance: no
* Variants: no
* Changed by: avristadm (2)
* IP: 172.17.0.1


Fields Summary: 
- title [input]
- description [textarea]
- teks [input]
- permalink [href]
- sideLink [href]
- prefix [select]
- text1 [input]
- text2 [input]
- image [image]
*/ 

namespace Pimcore\Model\DataObject;



/**
* @method static \Pimcore\Model\DataObject\Menu2\Listing getByTitle ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Menu2\Listing getByDescription ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Menu2\Listing getByTeks ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Menu2\Listing getByPermalink ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Menu2\Listing getBySideLink ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Menu2\Listing getByPrefix ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Menu2\Listing getByText1 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Menu2\Listing getByText2 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Menu2\Listing getByImage ($value, $limit = 0) 
*/

class Menu2 extends Concrete implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {



use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $o_classId = "48";
protected $o_className = "Menu2";
protected $title;
protected $description;
protected $teks;
protected $permalink;
protected $sideLink;
protected $prefix;
protected $text1;
protected $text2;
protected $image;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\Menu2
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get title - Title
* @return string
*/
public function getTitle () {
	$preValue = $this->preGetValue("title"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->title;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set title - Title
* @param string $title
* @return \Pimcore\Model\DataObject\Menu2
*/
public function setTitle ($title) {
	$fd = $this->getClass()->getFieldDefinition("title");
	$this->title = $title;
	return $this;
}

/**
* Get description - Description
* @return string
*/
public function getDescription () {
	$preValue = $this->preGetValue("description"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->description;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set description - Description
* @param string $description
* @return \Pimcore\Model\DataObject\Menu2
*/
public function setDescription ($description) {
	$fd = $this->getClass()->getFieldDefinition("description");
	$this->description = $description;
	return $this;
}

/**
* Get teks - Teks
* @return string
*/
public function getTeks () {
	$preValue = $this->preGetValue("teks"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->teks;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set teks - Teks
* @param string $teks
* @return \Pimcore\Model\DataObject\Menu2
*/
public function setTeks ($teks) {
	$fd = $this->getClass()->getFieldDefinition("teks");
	$this->teks = $teks;
	return $this;
}

/**
* Get permalink - Permalink
* @return \Pimcore\Model\Document\page
*/
public function getPermalink () {
	$preValue = $this->preGetValue("permalink"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("permalink")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set permalink - Permalink
* @param \Pimcore\Model\Document\page $permalink
* @return \Pimcore\Model\DataObject\Menu2
*/
public function setPermalink ($permalink) {
	$fd = $this->getClass()->getFieldDefinition("permalink");
	$currentData = $this->getPermalink();
	$isEqual = $fd->isEqual($currentData, $permalink);
	if (!$isEqual) {
		$this->markFieldDirty("permalink", true);
	}
	$this->permalink = $fd->preSetData($this, $permalink);
	return $this;
}

/**
* Get sideLink - Side Link
* @return \Pimcore\Model\Document\page
*/
public function getSideLink () {
	$preValue = $this->preGetValue("sideLink"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("sideLink")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set sideLink - Side Link
* @param \Pimcore\Model\Document\page $sideLink
* @return \Pimcore\Model\DataObject\Menu2
*/
public function setSideLink ($sideLink) {
	$fd = $this->getClass()->getFieldDefinition("sideLink");
	$currentData = $this->getSideLink();
	$isEqual = $fd->isEqual($currentData, $sideLink);
	if (!$isEqual) {
		$this->markFieldDirty("sideLink", true);
	}
	$this->sideLink = $fd->preSetData($this, $sideLink);
	return $this;
}

/**
* Get prefix - Prefix
* @return string
*/
public function getPrefix () {
	$preValue = $this->preGetValue("prefix"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->prefix;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set prefix - Prefix
* @param string $prefix
* @return \Pimcore\Model\DataObject\Menu2
*/
public function setPrefix ($prefix) {
	$fd = $this->getClass()->getFieldDefinition("prefix");
	$this->prefix = $prefix;
	return $this;
}

/**
* Get text1 - Text 1
* @return string
*/
public function getText1 () {
	$preValue = $this->preGetValue("text1"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->text1;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set text1 - Text 1
* @param string $text1
* @return \Pimcore\Model\DataObject\Menu2
*/
public function setText1 ($text1) {
	$fd = $this->getClass()->getFieldDefinition("text1");
	$this->text1 = $text1;
	return $this;
}

/**
* Get text2 - Text 2
* @return string
*/
public function getText2 () {
	$preValue = $this->preGetValue("text2"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->text2;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set text2 - Text 2
* @param string $text2
* @return \Pimcore\Model\DataObject\Menu2
*/
public function setText2 ($text2) {
	$fd = $this->getClass()->getFieldDefinition("text2");
	$this->text2 = $text2;
	return $this;
}

/**
* Get image - image
* @return \Pimcore\Model\Asset\Image
*/
public function getImage () {
	$preValue = $this->preGetValue("image"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->image;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set image - image
* @param \Pimcore\Model\Asset\Image $image
* @return \Pimcore\Model\DataObject\Menu2
*/
public function setImage ($image) {
	$fd = $this->getClass()->getFieldDefinition("image");
	$this->image = $image;
	return $this;
}

protected static $_relationFields = array (
  'permalink' => 
  array (
    'type' => 'href',
  ),
  'sideLink' => 
  array (
    'type' => 'href',
  ),
);

protected $lazyLoadedFields = array (
  0 => 'permalink',
  1 => 'sideLink',
);

}

