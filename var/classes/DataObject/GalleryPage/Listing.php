<?php 

namespace Pimcore\Model\DataObject\GalleryPage;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\GalleryPage current()
 * @method DataObject\GalleryPage[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "95";
protected $className = "GalleryPage";


}
