<?php 

/** 
* Generated at: 2019-06-20T11:39:35+07:00
* Inheritance: no
* Variants: no
* Changed by: avristadm (2)
* IP: 202.87.248.23


Fields Summary: 
- name [input]
- category [input]
- pic [input]
- telp [input]
- email [input]
- field [input]
- submitAt [input]
*/ 

namespace Pimcore\Model\DataObject;



/**
* @method static \Pimcore\Model\DataObject\GetSolutionBisnis\Listing getByName ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\GetSolutionBisnis\Listing getByCategory ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\GetSolutionBisnis\Listing getByPic ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\GetSolutionBisnis\Listing getByTelp ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\GetSolutionBisnis\Listing getByEmail ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\GetSolutionBisnis\Listing getByField ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\GetSolutionBisnis\Listing getBySubmitAt ($value, $limit = 0) 
*/

class GetSolutionBisnis extends Concrete implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {



use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $o_classId = "31";
protected $o_className = "GetSolutionBisnis";
protected $name;
protected $category;
protected $pic;
protected $telp;
protected $email;
protected $field;
protected $submitAt;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\GetSolutionBisnis
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get name - name
* @return string
*/
public function getName () {
	$preValue = $this->preGetValue("name"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->name;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set name - name
* @param string $name
* @return \Pimcore\Model\DataObject\GetSolutionBisnis
*/
public function setName ($name) {
	$fd = $this->getClass()->getFieldDefinition("name");
	$this->name = $name;
	return $this;
}

/**
* Get category - category
* @return string
*/
public function getCategory () {
	$preValue = $this->preGetValue("category"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->category;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set category - category
* @param string $category
* @return \Pimcore\Model\DataObject\GetSolutionBisnis
*/
public function setCategory ($category) {
	$fd = $this->getClass()->getFieldDefinition("category");
	$this->category = $category;
	return $this;
}

/**
* Get pic - pic
* @return string
*/
public function getPic () {
	$preValue = $this->preGetValue("pic"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->pic;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set pic - pic
* @param string $pic
* @return \Pimcore\Model\DataObject\GetSolutionBisnis
*/
public function setPic ($pic) {
	$fd = $this->getClass()->getFieldDefinition("pic");
	$this->pic = $pic;
	return $this;
}

/**
* Get telp - telp
* @return string
*/
public function getTelp () {
	$preValue = $this->preGetValue("telp"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->telp;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set telp - telp
* @param string $telp
* @return \Pimcore\Model\DataObject\GetSolutionBisnis
*/
public function setTelp ($telp) {
	$fd = $this->getClass()->getFieldDefinition("telp");
	$this->telp = $telp;
	return $this;
}

/**
* Get email - email
* @return string
*/
public function getEmail () {
	$preValue = $this->preGetValue("email"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->email;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set email - email
* @param string $email
* @return \Pimcore\Model\DataObject\GetSolutionBisnis
*/
public function setEmail ($email) {
	$fd = $this->getClass()->getFieldDefinition("email");
	$this->email = $email;
	return $this;
}

/**
* Get field - field
* @return string
*/
public function getField () {
	$preValue = $this->preGetValue("field"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->field;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set field - field
* @param string $field
* @return \Pimcore\Model\DataObject\GetSolutionBisnis
*/
public function setField ($field) {
	$fd = $this->getClass()->getFieldDefinition("field");
	$this->field = $field;
	return $this;
}

/**
* Get submitAt - submitAt
* @return string
*/
public function getSubmitAt () {
	$preValue = $this->preGetValue("submitAt"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->submitAt;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set submitAt - submitAt
* @param string $submitAt
* @return \Pimcore\Model\DataObject\GetSolutionBisnis
*/
public function setSubmitAt ($submitAt) {
	$fd = $this->getClass()->getFieldDefinition("submitAt");
	$this->submitAt = $submitAt;
	return $this;
}

protected static $_relationFields = array (
);

protected $lazyLoadedFields = array (
);

}

