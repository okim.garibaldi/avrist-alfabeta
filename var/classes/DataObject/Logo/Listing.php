<?php 

namespace Pimcore\Model\DataObject\Logo;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\Logo current()
 * @method DataObject\Logo[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "84";
protected $className = "Logo";


}
