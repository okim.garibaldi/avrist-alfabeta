<?php 

/** 
* Generated at: 2019-06-20T11:39:22+07:00
* IP: 202.87.248.23


Fields Summary: 
 - name [input]
 - image [image]
*/ 

namespace Pimcore\Model\DataObject\Fieldcollection\Data;

use Pimcore\Model\DataObject;

class Image extends DataObject\Fieldcollection\Data\AbstractData implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {

use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $type = "Image";
protected $name;
protected $image;


/**
* Get name - name
* @return string
*/
public function getName () {
	$data = $this->name;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	 return $data;
}

/**
* Set name - name
* @param string $name
* @return \Pimcore\Model\DataObject\Image
*/
public function setName ($name) {
	$fd = $this->getDefinition()->getFieldDefinition("name");
	$this->name = $name;
	return $this;
}

/**
* Get image - image
* @return \Pimcore\Model\Asset\Image
*/
public function getImage () {
	$data = $this->image;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	 return $data;
}

/**
* Set image - image
* @param \Pimcore\Model\Asset\Image $image
* @return \Pimcore\Model\DataObject\Image
*/
public function setImage ($image) {
	$fd = $this->getDefinition()->getFieldDefinition("image");
	$this->image = $image;
	return $this;
}

}

