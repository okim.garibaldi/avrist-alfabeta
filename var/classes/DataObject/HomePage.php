<?php 

/** 
* Generated at: 2020-02-17T02:40:46+07:00
* Inheritance: no
* Variants: no
* Changed by: avristadm (2)
* IP: 172.17.0.1


Fields Summary: 
- block1 [block]
-- title [input]
-- text1 [input]
-- text2 [input]
-- text1Page [href]
-- text2Page [href]
- title1 [input]
- description1 [textarea]
- description2 [textarea]
- text1 [input]
- text1Prefix [select]
- text1Link [input]
- block2 [block]
-- title [input]
-- prefix [select]
-- link [input]
-- image [image]
- title2 [input]
- block3 [block]
-- dataObjectLink [href]
- title3 [input]
- block4 [block]
-- title [input]
-- description [textarea]
-- image [image]
- title4 [input]
- block5 [block]
-- name [input]
-- prefix [select]
-- link [input]
-- image [image]
- title5 [input]
- subtitle5 [input]
- text2 [input]
- text2Link [input]
- title6 [input]
- text3 [input]
- text3Prefix [select]
- text3Link [input]
*/ 

namespace Pimcore\Model\DataObject;



/**
* @method static \Pimcore\Model\DataObject\HomePage\Listing getByBlock1 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\HomePage\Listing getByTitle1 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\HomePage\Listing getByDescription1 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\HomePage\Listing getByDescription2 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\HomePage\Listing getByText1 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\HomePage\Listing getByText1Prefix ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\HomePage\Listing getByText1Link ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\HomePage\Listing getByBlock2 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\HomePage\Listing getByTitle2 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\HomePage\Listing getByBlock3 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\HomePage\Listing getByTitle3 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\HomePage\Listing getByBlock4 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\HomePage\Listing getByTitle4 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\HomePage\Listing getByBlock5 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\HomePage\Listing getByTitle5 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\HomePage\Listing getBySubtitle5 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\HomePage\Listing getByText2 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\HomePage\Listing getByText2Link ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\HomePage\Listing getByTitle6 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\HomePage\Listing getByText3 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\HomePage\Listing getByText3Prefix ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\HomePage\Listing getByText3Link ($value, $limit = 0) 
*/

class HomePage extends Concrete implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {



use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $o_classId = "94";
protected $o_className = "HomePage";
protected $block1;
protected $title1;
protected $description1;
protected $description2;
protected $text1;
protected $text1Prefix;
protected $text1Link;
protected $block2;
protected $title2;
protected $block3;
protected $title3;
protected $block4;
protected $title4;
protected $block5;
protected $title5;
protected $subtitle5;
protected $text2;
protected $text2Link;
protected $title6;
protected $text3;
protected $text3Prefix;
protected $text3Link;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\HomePage
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get block1 - Block 1
* @return \Pimcore\Model\DataObject\Data\BlockElement[][]
*/
public function getBlock1 () {
	$preValue = $this->preGetValue("block1"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("block1")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set block1 - Block 1
* @param \Pimcore\Model\DataObject\Data\BlockElement[][] $block1
* @return \Pimcore\Model\DataObject\HomePage
*/
public function setBlock1 ($block1) {
	$fd = $this->getClass()->getFieldDefinition("block1");
	$this->block1 = $fd->preSetData($this, $block1);
	return $this;
}

/**
* Get title1 - Title 1
* @return string
*/
public function getTitle1 () {
	$preValue = $this->preGetValue("title1"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->title1;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set title1 - Title 1
* @param string $title1
* @return \Pimcore\Model\DataObject\HomePage
*/
public function setTitle1 ($title1) {
	$fd = $this->getClass()->getFieldDefinition("title1");
	$this->title1 = $title1;
	return $this;
}

/**
* Get description1 - Description 1
* @return string
*/
public function getDescription1 () {
	$preValue = $this->preGetValue("description1"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->description1;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set description1 - Description 1
* @param string $description1
* @return \Pimcore\Model\DataObject\HomePage
*/
public function setDescription1 ($description1) {
	$fd = $this->getClass()->getFieldDefinition("description1");
	$this->description1 = $description1;
	return $this;
}

/**
* Get description2 - Description 2
* @return string
*/
public function getDescription2 () {
	$preValue = $this->preGetValue("description2"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->description2;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set description2 - Description 2
* @param string $description2
* @return \Pimcore\Model\DataObject\HomePage
*/
public function setDescription2 ($description2) {
	$fd = $this->getClass()->getFieldDefinition("description2");
	$this->description2 = $description2;
	return $this;
}

/**
* Get text1 - Text 1
* @return string
*/
public function getText1 () {
	$preValue = $this->preGetValue("text1"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->text1;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set text1 - Text 1
* @param string $text1
* @return \Pimcore\Model\DataObject\HomePage
*/
public function setText1 ($text1) {
	$fd = $this->getClass()->getFieldDefinition("text1");
	$this->text1 = $text1;
	return $this;
}

/**
* Get text1Prefix - Text 1 Prefix
* @return string
*/
public function getText1Prefix () {
	$preValue = $this->preGetValue("text1Prefix"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->text1Prefix;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set text1Prefix - Text 1 Prefix
* @param string $text1Prefix
* @return \Pimcore\Model\DataObject\HomePage
*/
public function setText1Prefix ($text1Prefix) {
	$fd = $this->getClass()->getFieldDefinition("text1Prefix");
	$this->text1Prefix = $text1Prefix;
	return $this;
}

/**
* Get text1Link - Text 1 Link
* @return string
*/
public function getText1Link () {
	$preValue = $this->preGetValue("text1Link"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->text1Link;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set text1Link - Text 1 Link
* @param string $text1Link
* @return \Pimcore\Model\DataObject\HomePage
*/
public function setText1Link ($text1Link) {
	$fd = $this->getClass()->getFieldDefinition("text1Link");
	$this->text1Link = $text1Link;
	return $this;
}

/**
* Get block2 - Block 2
* @return \Pimcore\Model\DataObject\Data\BlockElement[][]
*/
public function getBlock2 () {
	$preValue = $this->preGetValue("block2"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("block2")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set block2 - Block 2
* @param \Pimcore\Model\DataObject\Data\BlockElement[][] $block2
* @return \Pimcore\Model\DataObject\HomePage
*/
public function setBlock2 ($block2) {
	$fd = $this->getClass()->getFieldDefinition("block2");
	$this->block2 = $fd->preSetData($this, $block2);
	return $this;
}

/**
* Get title2 - Title 2
* @return string
*/
public function getTitle2 () {
	$preValue = $this->preGetValue("title2"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->title2;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set title2 - Title 2
* @param string $title2
* @return \Pimcore\Model\DataObject\HomePage
*/
public function setTitle2 ($title2) {
	$fd = $this->getClass()->getFieldDefinition("title2");
	$this->title2 = $title2;
	return $this;
}

/**
* Get block3 - Block 3
* @return \Pimcore\Model\DataObject\Data\BlockElement[][]
*/
public function getBlock3 () {
	$preValue = $this->preGetValue("block3"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("block3")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set block3 - Block 3
* @param \Pimcore\Model\DataObject\Data\BlockElement[][] $block3
* @return \Pimcore\Model\DataObject\HomePage
*/
public function setBlock3 ($block3) {
	$fd = $this->getClass()->getFieldDefinition("block3");
	$this->block3 = $fd->preSetData($this, $block3);
	return $this;
}

/**
* Get title3 - Title 3
* @return string
*/
public function getTitle3 () {
	$preValue = $this->preGetValue("title3"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->title3;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set title3 - Title 3
* @param string $title3
* @return \Pimcore\Model\DataObject\HomePage
*/
public function setTitle3 ($title3) {
	$fd = $this->getClass()->getFieldDefinition("title3");
	$this->title3 = $title3;
	return $this;
}

/**
* Get block4 - Block 4
* @return \Pimcore\Model\DataObject\Data\BlockElement[][]
*/
public function getBlock4 () {
	$preValue = $this->preGetValue("block4"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("block4")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set block4 - Block 4
* @param \Pimcore\Model\DataObject\Data\BlockElement[][] $block4
* @return \Pimcore\Model\DataObject\HomePage
*/
public function setBlock4 ($block4) {
	$fd = $this->getClass()->getFieldDefinition("block4");
	$this->block4 = $fd->preSetData($this, $block4);
	return $this;
}

/**
* Get title4 - Title 4
* @return string
*/
public function getTitle4 () {
	$preValue = $this->preGetValue("title4"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->title4;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set title4 - Title 4
* @param string $title4
* @return \Pimcore\Model\DataObject\HomePage
*/
public function setTitle4 ($title4) {
	$fd = $this->getClass()->getFieldDefinition("title4");
	$this->title4 = $title4;
	return $this;
}

/**
* Get block5 - Block 5
* @return \Pimcore\Model\DataObject\Data\BlockElement[][]
*/
public function getBlock5 () {
	$preValue = $this->preGetValue("block5"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("block5")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set block5 - Block 5
* @param \Pimcore\Model\DataObject\Data\BlockElement[][] $block5
* @return \Pimcore\Model\DataObject\HomePage
*/
public function setBlock5 ($block5) {
	$fd = $this->getClass()->getFieldDefinition("block5");
	$this->block5 = $fd->preSetData($this, $block5);
	return $this;
}

/**
* Get title5 - Title 5
* @return string
*/
public function getTitle5 () {
	$preValue = $this->preGetValue("title5"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->title5;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set title5 - Title 5
* @param string $title5
* @return \Pimcore\Model\DataObject\HomePage
*/
public function setTitle5 ($title5) {
	$fd = $this->getClass()->getFieldDefinition("title5");
	$this->title5 = $title5;
	return $this;
}

/**
* Get subtitle5 - Subtitle 5
* @return string
*/
public function getSubtitle5 () {
	$preValue = $this->preGetValue("subtitle5"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->subtitle5;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set subtitle5 - Subtitle 5
* @param string $subtitle5
* @return \Pimcore\Model\DataObject\HomePage
*/
public function setSubtitle5 ($subtitle5) {
	$fd = $this->getClass()->getFieldDefinition("subtitle5");
	$this->subtitle5 = $subtitle5;
	return $this;
}

/**
* Get text2 - Text 2
* @return string
*/
public function getText2 () {
	$preValue = $this->preGetValue("text2"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->text2;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set text2 - Text 2
* @param string $text2
* @return \Pimcore\Model\DataObject\HomePage
*/
public function setText2 ($text2) {
	$fd = $this->getClass()->getFieldDefinition("text2");
	$this->text2 = $text2;
	return $this;
}

/**
* Get text2Link - Text 2 Link
* @return string
*/
public function getText2Link () {
	$preValue = $this->preGetValue("text2Link"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->text2Link;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set text2Link - Text 2 Link
* @param string $text2Link
* @return \Pimcore\Model\DataObject\HomePage
*/
public function setText2Link ($text2Link) {
	$fd = $this->getClass()->getFieldDefinition("text2Link");
	$this->text2Link = $text2Link;
	return $this;
}

/**
* Get title6 - Title 6
* @return string
*/
public function getTitle6 () {
	$preValue = $this->preGetValue("title6"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->title6;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set title6 - Title 6
* @param string $title6
* @return \Pimcore\Model\DataObject\HomePage
*/
public function setTitle6 ($title6) {
	$fd = $this->getClass()->getFieldDefinition("title6");
	$this->title6 = $title6;
	return $this;
}

/**
* Get text3 - Text 3
* @return string
*/
public function getText3 () {
	$preValue = $this->preGetValue("text3"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->text3;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set text3 - Text 3
* @param string $text3
* @return \Pimcore\Model\DataObject\HomePage
*/
public function setText3 ($text3) {
	$fd = $this->getClass()->getFieldDefinition("text3");
	$this->text3 = $text3;
	return $this;
}

/**
* Get text3Prefix - Text 3 Prefix
* @return string
*/
public function getText3Prefix () {
	$preValue = $this->preGetValue("text3Prefix"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->text3Prefix;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set text3Prefix - Text 3 Prefix
* @param string $text3Prefix
* @return \Pimcore\Model\DataObject\HomePage
*/
public function setText3Prefix ($text3Prefix) {
	$fd = $this->getClass()->getFieldDefinition("text3Prefix");
	$this->text3Prefix = $text3Prefix;
	return $this;
}

/**
* Get text3Link - Text 3 Link
* @return string
*/
public function getText3Link () {
	$preValue = $this->preGetValue("text3Link"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->text3Link;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set text3Link - Text 3 Link
* @param string $text3Link
* @return \Pimcore\Model\DataObject\HomePage
*/
public function setText3Link ($text3Link) {
	$fd = $this->getClass()->getFieldDefinition("text3Link");
	$this->text3Link = $text3Link;
	return $this;
}

protected static $_relationFields = array (
);

protected $lazyLoadedFields = array (
);

}

