<?php 

namespace Pimcore\Model\DataObject\Pelajari;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\Pelajari current()
 * @method DataObject\Pelajari[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "50";
protected $className = "Pelajari";


}
