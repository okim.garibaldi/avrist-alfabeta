<?php 

/** 
* Generated at: 2019-12-04T13:23:21+07:00
* Inheritance: no
* Variants: no
* IP: 172.17.0.1


Fields Summary: 
*/ 

namespace Pimcore\Model\DataObject;



/**
*/

class CareerGroup extends Concrete implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {



use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $o_classId = "60";
protected $o_className = "CareerGroup";


/**
* @param array $values
* @return \Pimcore\Model\DataObject\CareerGroup
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

}

