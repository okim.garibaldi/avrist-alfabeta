<?php 

namespace Pimcore\Model\DataObject\ProductBenefit;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\ProductBenefit current()
 * @method DataObject\ProductBenefit[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "33";
protected $className = "ProductBenefit";


}
