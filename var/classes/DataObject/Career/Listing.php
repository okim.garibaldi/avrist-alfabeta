<?php 

namespace Pimcore\Model\DataObject\Career;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\Career current()
 * @method DataObject\Career[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "8";
protected $className = "Career";


}
