<?php 

namespace Pimcore\Model\DataObject\FundFactPage;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\FundFactPage current()
 * @method DataObject\FundFactPage[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "67";
protected $className = "FundFactPage";


}
