<?php 

/** 
* Generated at: 2019-06-20T11:39:34+07:00
* Inheritance: no
* Variants: no
* Changed by: avristadm (2)
* IP: 202.87.248.23


Fields Summary: 
- contactUsTopic [href]
- contactUsTime [href]
- name [input]
- email [email]
- telephone [input]
- message [textarea]
*/ 

namespace Pimcore\Model\DataObject;



/**
* @method static \Pimcore\Model\DataObject\ContactUs\Listing getByContactUsTopic ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\ContactUs\Listing getByContactUsTime ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\ContactUs\Listing getByName ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\ContactUs\Listing getByEmail ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\ContactUs\Listing getByTelephone ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\ContactUs\Listing getByMessage ($value, $limit = 0) 
*/

class ContactUs extends Concrete implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {



use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $o_classId = "3";
protected $o_className = "ContactUs";
protected $contactUsTopic;
protected $contactUsTime;
protected $name;
protected $email;
protected $telephone;
protected $message;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\ContactUs
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get contactUsTopic - contactUsTopic
* @return \Pimcore\Model\DataObject\ContactUsTopic
*/
public function getContactUsTopic () {
	$preValue = $this->preGetValue("contactUsTopic"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("contactUsTopic")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set contactUsTopic - contactUsTopic
* @param \Pimcore\Model\DataObject\ContactUsTopic $contactUsTopic
* @return \Pimcore\Model\DataObject\ContactUs
*/
public function setContactUsTopic ($contactUsTopic) {
	$fd = $this->getClass()->getFieldDefinition("contactUsTopic");
	$currentData = $this->getContactUsTopic();
	$isEqual = $fd->isEqual($currentData, $contactUsTopic);
	if (!$isEqual) {
		$this->markFieldDirty("contactUsTopic", true);
	}
	$this->contactUsTopic = $fd->preSetData($this, $contactUsTopic);
	return $this;
}

/**
* Get contactUsTime - contactUsTime
* @return \Pimcore\Model\DataObject\ContactUsTime
*/
public function getContactUsTime () {
	$preValue = $this->preGetValue("contactUsTime"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("contactUsTime")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set contactUsTime - contactUsTime
* @param \Pimcore\Model\DataObject\ContactUsTime $contactUsTime
* @return \Pimcore\Model\DataObject\ContactUs
*/
public function setContactUsTime ($contactUsTime) {
	$fd = $this->getClass()->getFieldDefinition("contactUsTime");
	$currentData = $this->getContactUsTime();
	$isEqual = $fd->isEqual($currentData, $contactUsTime);
	if (!$isEqual) {
		$this->markFieldDirty("contactUsTime", true);
	}
	$this->contactUsTime = $fd->preSetData($this, $contactUsTime);
	return $this;
}

/**
* Get name - Name
* @return string
*/
public function getName () {
	$preValue = $this->preGetValue("name"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->name;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set name - Name
* @param string $name
* @return \Pimcore\Model\DataObject\ContactUs
*/
public function setName ($name) {
	$fd = $this->getClass()->getFieldDefinition("name");
	$this->name = $name;
	return $this;
}

/**
* Get email - Email
* @return string
*/
public function getEmail () {
	$preValue = $this->preGetValue("email"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->email;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set email - Email
* @param string $email
* @return \Pimcore\Model\DataObject\ContactUs
*/
public function setEmail ($email) {
	$fd = $this->getClass()->getFieldDefinition("email");
	$this->email = $email;
	return $this;
}

/**
* Get telephone - telephone
* @return string
*/
public function getTelephone () {
	$preValue = $this->preGetValue("telephone"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->telephone;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set telephone - telephone
* @param string $telephone
* @return \Pimcore\Model\DataObject\ContactUs
*/
public function setTelephone ($telephone) {
	$fd = $this->getClass()->getFieldDefinition("telephone");
	$this->telephone = $telephone;
	return $this;
}

/**
* Get message - Message
* @return string
*/
public function getMessage () {
	$preValue = $this->preGetValue("message"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->message;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set message - Message
* @param string $message
* @return \Pimcore\Model\DataObject\ContactUs
*/
public function setMessage ($message) {
	$fd = $this->getClass()->getFieldDefinition("message");
	$this->message = $message;
	return $this;
}

protected static $_relationFields = array (
  'contactUsTopic' => 
  array (
    'type' => 'href',
  ),
  'contactUsTime' => 
  array (
    'type' => 'href',
  ),
);

protected $lazyLoadedFields = array (
  0 => 'contactUsTopic',
  1 => 'contactUsTime',
);

}

