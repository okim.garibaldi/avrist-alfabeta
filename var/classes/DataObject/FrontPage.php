<?php 

/** 
* Generated at: 2020-01-23T18:27:09+07:00
* Inheritance: no
* Variants: no
* Changed by: avristadm (2)
* IP: 172.17.0.1


Fields Summary: 
- block1 [block]
-- title [input]
-- personal [href]
-- bisnis [href]
- title1 [input]
- description1 [textarea]
- description2 [textarea]
- text1 [input]
*/ 

namespace Pimcore\Model\DataObject;



/**
* @method static \Pimcore\Model\DataObject\FrontPage\Listing getByBlock1 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\FrontPage\Listing getByTitle1 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\FrontPage\Listing getByDescription1 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\FrontPage\Listing getByDescription2 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\FrontPage\Listing getByText1 ($value, $limit = 0) 
*/

class FrontPage extends Concrete implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {



use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $o_classId = "85";
protected $o_className = "FrontPage";
protected $block1;
protected $title1;
protected $description1;
protected $description2;
protected $text1;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\FrontPage
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get block1 - Block1
* @return \Pimcore\Model\DataObject\Data\BlockElement[][]
*/
public function getBlock1 () {
	$preValue = $this->preGetValue("block1"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("block1")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set block1 - Block1
* @param \Pimcore\Model\DataObject\Data\BlockElement[][] $block1
* @return \Pimcore\Model\DataObject\FrontPage
*/
public function setBlock1 ($block1) {
	$fd = $this->getClass()->getFieldDefinition("block1");
	$this->block1 = $fd->preSetData($this, $block1);
	return $this;
}

/**
* Get title1 - Title 1
* @return string
*/
public function getTitle1 () {
	$preValue = $this->preGetValue("title1"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->title1;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set title1 - Title 1
* @param string $title1
* @return \Pimcore\Model\DataObject\FrontPage
*/
public function setTitle1 ($title1) {
	$fd = $this->getClass()->getFieldDefinition("title1");
	$this->title1 = $title1;
	return $this;
}

/**
* Get description1 - Description 1
* @return string
*/
public function getDescription1 () {
	$preValue = $this->preGetValue("description1"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->description1;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set description1 - Description 1
* @param string $description1
* @return \Pimcore\Model\DataObject\FrontPage
*/
public function setDescription1 ($description1) {
	$fd = $this->getClass()->getFieldDefinition("description1");
	$this->description1 = $description1;
	return $this;
}

/**
* Get description2 - Description 2
* @return string
*/
public function getDescription2 () {
	$preValue = $this->preGetValue("description2"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->description2;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set description2 - Description 2
* @param string $description2
* @return \Pimcore\Model\DataObject\FrontPage
*/
public function setDescription2 ($description2) {
	$fd = $this->getClass()->getFieldDefinition("description2");
	$this->description2 = $description2;
	return $this;
}

/**
* Get text1 - Text 1
* @return string
*/
public function getText1 () {
	$preValue = $this->preGetValue("text1"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->text1;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set text1 - Text 1
* @param string $text1
* @return \Pimcore\Model\DataObject\FrontPage
*/
public function setText1 ($text1) {
	$fd = $this->getClass()->getFieldDefinition("text1");
	$this->text1 = $text1;
	return $this;
}

protected static $_relationFields = array (
);

protected $lazyLoadedFields = array (
);

}

