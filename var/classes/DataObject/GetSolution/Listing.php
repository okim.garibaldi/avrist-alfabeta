<?php 

namespace Pimcore\Model\DataObject\GetSolution;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\GetSolution current()
 * @method DataObject\GetSolution[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "30";
protected $className = "GetSolution";


}
