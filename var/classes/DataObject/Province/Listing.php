<?php 

namespace Pimcore\Model\DataObject\Province;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\Province current()
 * @method DataObject\Province[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "17";
protected $className = "Province";


}
