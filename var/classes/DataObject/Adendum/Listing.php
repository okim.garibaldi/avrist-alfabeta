<?php 

namespace Pimcore\Model\DataObject\Adendum;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\Adendum current()
 * @method DataObject\Adendum[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "80";
protected $className = "Adendum";


}
