<?php 

/** 
* Generated at: 2020-01-03T07:40:19+07:00
* Inheritance: no
* Variants: no
* Changed by: avristadm (2)
* IP: 172.17.0.1


Fields Summary: 
- name [input]
- siteId [select]
- description [textarea]
- icon [image]
- page [href]
- formPage [href]
*/ 

namespace Pimcore\Model\DataObject;



/**
* @method static \Pimcore\Model\DataObject\CareerCategory\Listing getByName ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\CareerCategory\Listing getBySiteId ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\CareerCategory\Listing getByDescription ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\CareerCategory\Listing getByIcon ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\CareerCategory\Listing getByPage ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\CareerCategory\Listing getByFormPage ($value, $limit = 0) 
*/

class CareerCategory extends Concrete implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {



use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $o_classId = "10";
protected $o_className = "CareerCategory";
protected $name;
protected $siteId;
protected $description;
protected $icon;
protected $page;
protected $formPage;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\CareerCategory
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get name - name
* @return string
*/
public function getName () {
	$preValue = $this->preGetValue("name"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->name;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set name - name
* @param string $name
* @return \Pimcore\Model\DataObject\CareerCategory
*/
public function setName ($name) {
	$fd = $this->getClass()->getFieldDefinition("name");
	$this->name = $name;
	return $this;
}

/**
* Get siteId - Site
* @return string
*/
public function getSiteId () {
	$preValue = $this->preGetValue("siteId"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->siteId;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set siteId - Site
* @param string $siteId
* @return \Pimcore\Model\DataObject\CareerCategory
*/
public function setSiteId ($siteId) {
	$fd = $this->getClass()->getFieldDefinition("siteId");
	$this->siteId = $siteId;
	return $this;
}

/**
* Get description - Description
* @return string
*/
public function getDescription () {
	$preValue = $this->preGetValue("description"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->description;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set description - Description
* @param string $description
* @return \Pimcore\Model\DataObject\CareerCategory
*/
public function setDescription ($description) {
	$fd = $this->getClass()->getFieldDefinition("description");
	$this->description = $description;
	return $this;
}

/**
* Get icon - Icon
* @return \Pimcore\Model\Asset\Image
*/
public function getIcon () {
	$preValue = $this->preGetValue("icon"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->icon;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set icon - Icon
* @param \Pimcore\Model\Asset\Image $icon
* @return \Pimcore\Model\DataObject\CareerCategory
*/
public function setIcon ($icon) {
	$fd = $this->getClass()->getFieldDefinition("icon");
	$this->icon = $icon;
	return $this;
}

/**
* Get page - page
* @return \Pimcore\Model\Document\page
*/
public function getPage () {
	$preValue = $this->preGetValue("page"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("page")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set page - page
* @param \Pimcore\Model\Document\page $page
* @return \Pimcore\Model\DataObject\CareerCategory
*/
public function setPage ($page) {
	$fd = $this->getClass()->getFieldDefinition("page");
	$currentData = $this->getPage();
	$isEqual = $fd->isEqual($currentData, $page);
	if (!$isEqual) {
		$this->markFieldDirty("page", true);
	}
	$this->page = $fd->preSetData($this, $page);
	return $this;
}

/**
* Get formPage - Form Page
* @return \Pimcore\Model\Document\page
*/
public function getFormPage () {
	$preValue = $this->preGetValue("formPage"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("formPage")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set formPage - Form Page
* @param \Pimcore\Model\Document\page $formPage
* @return \Pimcore\Model\DataObject\CareerCategory
*/
public function setFormPage ($formPage) {
	$fd = $this->getClass()->getFieldDefinition("formPage");
	$currentData = $this->getFormPage();
	$isEqual = $fd->isEqual($currentData, $formPage);
	if (!$isEqual) {
		$this->markFieldDirty("formPage", true);
	}
	$this->formPage = $fd->preSetData($this, $formPage);
	return $this;
}

protected static $_relationFields = array (
  'page' => 
  array (
    'type' => 'href',
  ),
  'formPage' => 
  array (
    'type' => 'href',
  ),
);

protected $lazyLoadedFields = array (
  0 => 'page',
  1 => 'formPage',
);

}

