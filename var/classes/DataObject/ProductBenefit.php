<?php 

/** 
* Generated at: 2019-08-13T10:21:09+07:00
* Inheritance: no
* Variants: no
* Changed by: avristadm (2)
* IP: 172.17.0.1


Fields Summary: 
- title [input]
- description [wysiwyg]
- product [href]
*/ 

namespace Pimcore\Model\DataObject;



/**
* @method static \Pimcore\Model\DataObject\ProductBenefit\Listing getByTitle ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\ProductBenefit\Listing getByDescription ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\ProductBenefit\Listing getByProduct ($value, $limit = 0) 
*/

class ProductBenefit extends Concrete implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {



use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $o_classId = "33";
protected $o_className = "ProductBenefit";
protected $title;
protected $description;
protected $product;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\ProductBenefit
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get title - title
* @return string
*/
public function getTitle () {
	$preValue = $this->preGetValue("title"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->title;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set title - title
* @param string $title
* @return \Pimcore\Model\DataObject\ProductBenefit
*/
public function setTitle ($title) {
	$fd = $this->getClass()->getFieldDefinition("title");
	$this->title = $title;
	return $this;
}

/**
* Get description - description
* @return string
*/
public function getDescription () {
	$preValue = $this->preGetValue("description"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("description")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set description - description
* @param string $description
* @return \Pimcore\Model\DataObject\ProductBenefit
*/
public function setDescription ($description) {
	$fd = $this->getClass()->getFieldDefinition("description");
	$this->description = $description;
	return $this;
}

/**
* Get product - product
* @return \Pimcore\Model\DataObject\Product
*/
public function getProduct () {
	$preValue = $this->preGetValue("product"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("product")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set product - product
* @param \Pimcore\Model\DataObject\Product $product
* @return \Pimcore\Model\DataObject\ProductBenefit
*/
public function setProduct ($product) {
	$fd = $this->getClass()->getFieldDefinition("product");
	$currentData = $this->getProduct();
	$isEqual = $fd->isEqual($currentData, $product);
	if (!$isEqual) {
		$this->markFieldDirty("product", true);
	}
	$this->product = $fd->preSetData($this, $product);
	return $this;
}

protected static $_relationFields = array (
  'product' => 
  array (
    'type' => 'href',
  ),
);

protected $lazyLoadedFields = array (
  0 => 'product',
);

}

