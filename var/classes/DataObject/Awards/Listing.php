<?php 

namespace Pimcore\Model\DataObject\Awards;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\Awards current()
 * @method DataObject\Awards[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "39";
protected $className = "Awards";


}
