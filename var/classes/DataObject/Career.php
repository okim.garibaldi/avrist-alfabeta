<?php 

/** 
* Generated at: 2020-03-16T12:15:01+07:00
* Inheritance: no
* Variants: no
* Changed by: avristadm (2)
* IP: 172.17.0.1


Fields Summary: 
- category [href]
- Division [select]
- role [input]
- location [input]
- company [input]
- siteId [select]
- validDate [date]
- url [input]
- block [block]
-- title [input]
-- content [textarea]
*/ 

namespace Pimcore\Model\DataObject;



/**
* @method static \Pimcore\Model\DataObject\Career\Listing getByCategory ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Career\Listing getByDivision ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Career\Listing getByRole ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Career\Listing getByLocation ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Career\Listing getByCompany ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Career\Listing getBySiteId ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Career\Listing getByValidDate ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Career\Listing getByUrl ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Career\Listing getByBlock ($value, $limit = 0) 
*/

class Career extends Concrete implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {



use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $o_classId = "8";
protected $o_className = "Career";
protected $category;
protected $Division;
protected $role;
protected $location;
protected $company;
protected $siteId;
protected $validDate;
protected $url;
protected $block;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\Career
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get category - category
* @return \Pimcore\Model\DataObject\CareerCategory
*/
public function getCategory () {
	$preValue = $this->preGetValue("category"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("category")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set category - category
* @param \Pimcore\Model\DataObject\CareerCategory $category
* @return \Pimcore\Model\DataObject\Career
*/
public function setCategory ($category) {
	$fd = $this->getClass()->getFieldDefinition("category");
	$currentData = $this->getCategory();
	$isEqual = $fd->isEqual($currentData, $category);
	if (!$isEqual) {
		$this->markFieldDirty("category", true);
	}
	$this->category = $fd->preSetData($this, $category);
	return $this;
}

/**
* Get Division - Division
* @return string
*/
public function getDivision () {
	$preValue = $this->preGetValue("Division"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->Division;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set Division - Division
* @param string $Division
* @return \Pimcore\Model\DataObject\Career
*/
public function setDivision ($Division) {
	$fd = $this->getClass()->getFieldDefinition("Division");
	$this->Division = $Division;
	return $this;
}

/**
* Get role - role
* @return string
*/
public function getRole () {
	$preValue = $this->preGetValue("role"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->role;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set role - role
* @param string $role
* @return \Pimcore\Model\DataObject\Career
*/
public function setRole ($role) {
	$fd = $this->getClass()->getFieldDefinition("role");
	$this->role = $role;
	return $this;
}

/**
* Get location - location
* @return string
*/
public function getLocation () {
	$preValue = $this->preGetValue("location"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->location;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set location - location
* @param string $location
* @return \Pimcore\Model\DataObject\Career
*/
public function setLocation ($location) {
	$fd = $this->getClass()->getFieldDefinition("location");
	$this->location = $location;
	return $this;
}

/**
* Get company - company
* @return string
*/
public function getCompany () {
	$preValue = $this->preGetValue("company"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->company;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set company - company
* @param string $company
* @return \Pimcore\Model\DataObject\Career
*/
public function setCompany ($company) {
	$fd = $this->getClass()->getFieldDefinition("company");
	$this->company = $company;
	return $this;
}

/**
* Get siteId - Site
* @return string
*/
public function getSiteId () {
	$preValue = $this->preGetValue("siteId"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->siteId;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set siteId - Site
* @param string $siteId
* @return \Pimcore\Model\DataObject\Career
*/
public function setSiteId ($siteId) {
	$fd = $this->getClass()->getFieldDefinition("siteId");
	$this->siteId = $siteId;
	return $this;
}

/**
* Get validDate - validDate
* @return \Carbon\Carbon
*/
public function getValidDate () {
	$preValue = $this->preGetValue("validDate"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->validDate;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set validDate - validDate
* @param \Carbon\Carbon $validDate
* @return \Pimcore\Model\DataObject\Career
*/
public function setValidDate ($validDate) {
	$fd = $this->getClass()->getFieldDefinition("validDate");
	$this->validDate = $validDate;
	return $this;
}

/**
* Get url - URL
* @return string
*/
public function getUrl () {
	$preValue = $this->preGetValue("url"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->url;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set url - URL
* @param string $url
* @return \Pimcore\Model\DataObject\Career
*/
public function setUrl ($url) {
	$fd = $this->getClass()->getFieldDefinition("url");
	$this->url = $url;
	return $this;
}

/**
* Get block - Block
* @return \Pimcore\Model\DataObject\Data\BlockElement[][]
*/
public function getBlock () {
	$preValue = $this->preGetValue("block"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("block")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set block - Block
* @param \Pimcore\Model\DataObject\Data\BlockElement[][] $block
* @return \Pimcore\Model\DataObject\Career
*/
public function setBlock ($block) {
	$fd = $this->getClass()->getFieldDefinition("block");
	$this->block = $fd->preSetData($this, $block);
	return $this;
}

protected static $_relationFields = array (
  'category' => 
  array (
    'type' => 'href',
  ),
);

protected $lazyLoadedFields = array (
  0 => 'category',
);

}

