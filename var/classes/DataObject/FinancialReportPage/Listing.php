<?php 

namespace Pimcore\Model\DataObject\FinancialReportPage;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\FinancialReportPage current()
 * @method DataObject\FinancialReportPage[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "69";
protected $className = "FinancialReportPage";


}
