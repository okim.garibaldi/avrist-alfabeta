<?php 

/** 
* Generated at: 2020-01-01T19:11:04+07:00
* Inheritance: no
* Variants: no
* Changed by: avristadm (2)
* IP: 172.17.0.1


Fields Summary: 
- siteId [select]
- name [input]
- description [textarea]
- link [input]
- title [input]
- address [textarea]
- contact1 [input]
- contact1Image [image]
- contact1Number [input]
- contact2 [input]
- contact2Image [image]
- contact2Number [input]
- title1 [input]
- description1 [textarea]
- block1 [block]
-- page [href]
- title2 [input]
- description2 [textarea]
- block2 [block]
-- page [href]
- title3 [input]
- description3 [textarea]
- text1 [input]
- text1Page [href]
- text1Image [image]
- title4 [input]
- description4 [textarea]
- title5 [input]
- block3 [block]
-- question [input]
-- answer [textarea]
*/ 

namespace Pimcore\Model\DataObject;



/**
* @method static \Pimcore\Model\DataObject\ContactPage\Listing getBySiteId ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\ContactPage\Listing getByName ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\ContactPage\Listing getByDescription ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\ContactPage\Listing getByLink ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\ContactPage\Listing getByTitle ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\ContactPage\Listing getByAddress ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\ContactPage\Listing getByContact1 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\ContactPage\Listing getByContact1Image ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\ContactPage\Listing getByContact1Number ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\ContactPage\Listing getByContact2 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\ContactPage\Listing getByContact2Image ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\ContactPage\Listing getByContact2Number ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\ContactPage\Listing getByTitle1 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\ContactPage\Listing getByDescription1 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\ContactPage\Listing getByBlock1 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\ContactPage\Listing getByTitle2 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\ContactPage\Listing getByDescription2 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\ContactPage\Listing getByBlock2 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\ContactPage\Listing getByTitle3 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\ContactPage\Listing getByDescription3 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\ContactPage\Listing getByText1 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\ContactPage\Listing getByText1Page ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\ContactPage\Listing getByText1Image ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\ContactPage\Listing getByTitle4 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\ContactPage\Listing getByDescription4 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\ContactPage\Listing getByTitle5 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\ContactPage\Listing getByBlock3 ($value, $limit = 0) 
*/

class ContactPage extends Concrete implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {



use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $o_classId = "52";
protected $o_className = "ContactPage";
protected $siteId;
protected $name;
protected $description;
protected $link;
protected $title;
protected $address;
protected $contact1;
protected $contact1Image;
protected $contact1Number;
protected $contact2;
protected $contact2Image;
protected $contact2Number;
protected $title1;
protected $description1;
protected $block1;
protected $title2;
protected $description2;
protected $block2;
protected $title3;
protected $description3;
protected $text1;
protected $text1Page;
protected $text1Image;
protected $title4;
protected $description4;
protected $title5;
protected $block3;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\ContactPage
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get siteId - Site
* @return string
*/
public function getSiteId () {
	$preValue = $this->preGetValue("siteId"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->siteId;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set siteId - Site
* @param string $siteId
* @return \Pimcore\Model\DataObject\ContactPage
*/
public function setSiteId ($siteId) {
	$fd = $this->getClass()->getFieldDefinition("siteId");
	$this->siteId = $siteId;
	return $this;
}

/**
* Get name - name
* @return string
*/
public function getName () {
	$preValue = $this->preGetValue("name"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->name;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set name - name
* @param string $name
* @return \Pimcore\Model\DataObject\ContactPage
*/
public function setName ($name) {
	$fd = $this->getClass()->getFieldDefinition("name");
	$this->name = $name;
	return $this;
}

/**
* Get description - Description
* @return string
*/
public function getDescription () {
	$preValue = $this->preGetValue("description"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->description;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set description - Description
* @param string $description
* @return \Pimcore\Model\DataObject\ContactPage
*/
public function setDescription ($description) {
	$fd = $this->getClass()->getFieldDefinition("description");
	$this->description = $description;
	return $this;
}

/**
* Get link - Link
* @return string
*/
public function getLink () {
	$preValue = $this->preGetValue("link"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->link;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set link - Link
* @param string $link
* @return \Pimcore\Model\DataObject\ContactPage
*/
public function setLink ($link) {
	$fd = $this->getClass()->getFieldDefinition("link");
	$this->link = $link;
	return $this;
}

/**
* Get title - Title 
* @return string
*/
public function getTitle () {
	$preValue = $this->preGetValue("title"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->title;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set title - Title 
* @param string $title
* @return \Pimcore\Model\DataObject\ContactPage
*/
public function setTitle ($title) {
	$fd = $this->getClass()->getFieldDefinition("title");
	$this->title = $title;
	return $this;
}

/**
* Get address - Address
* @return string
*/
public function getAddress () {
	$preValue = $this->preGetValue("address"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->address;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set address - Address
* @param string $address
* @return \Pimcore\Model\DataObject\ContactPage
*/
public function setAddress ($address) {
	$fd = $this->getClass()->getFieldDefinition("address");
	$this->address = $address;
	return $this;
}

/**
* Get contact1 - Contact1
* @return string
*/
public function getContact1 () {
	$preValue = $this->preGetValue("contact1"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->contact1;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set contact1 - Contact1
* @param string $contact1
* @return \Pimcore\Model\DataObject\ContactPage
*/
public function setContact1 ($contact1) {
	$fd = $this->getClass()->getFieldDefinition("contact1");
	$this->contact1 = $contact1;
	return $this;
}

/**
* Get contact1Image - Contact 1 Image
* @return \Pimcore\Model\Asset\Image
*/
public function getContact1Image () {
	$preValue = $this->preGetValue("contact1Image"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->contact1Image;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set contact1Image - Contact 1 Image
* @param \Pimcore\Model\Asset\Image $contact1Image
* @return \Pimcore\Model\DataObject\ContactPage
*/
public function setContact1Image ($contact1Image) {
	$fd = $this->getClass()->getFieldDefinition("contact1Image");
	$this->contact1Image = $contact1Image;
	return $this;
}

/**
* Get contact1Number - Contact 1 Number
* @return string
*/
public function getContact1Number () {
	$preValue = $this->preGetValue("contact1Number"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->contact1Number;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set contact1Number - Contact 1 Number
* @param string $contact1Number
* @return \Pimcore\Model\DataObject\ContactPage
*/
public function setContact1Number ($contact1Number) {
	$fd = $this->getClass()->getFieldDefinition("contact1Number");
	$this->contact1Number = $contact1Number;
	return $this;
}

/**
* Get contact2 - Contact 2
* @return string
*/
public function getContact2 () {
	$preValue = $this->preGetValue("contact2"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->contact2;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set contact2 - Contact 2
* @param string $contact2
* @return \Pimcore\Model\DataObject\ContactPage
*/
public function setContact2 ($contact2) {
	$fd = $this->getClass()->getFieldDefinition("contact2");
	$this->contact2 = $contact2;
	return $this;
}

/**
* Get contact2Image - Contact 2 Image
* @return \Pimcore\Model\Asset\Image
*/
public function getContact2Image () {
	$preValue = $this->preGetValue("contact2Image"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->contact2Image;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set contact2Image - Contact 2 Image
* @param \Pimcore\Model\Asset\Image $contact2Image
* @return \Pimcore\Model\DataObject\ContactPage
*/
public function setContact2Image ($contact2Image) {
	$fd = $this->getClass()->getFieldDefinition("contact2Image");
	$this->contact2Image = $contact2Image;
	return $this;
}

/**
* Get contact2Number - Contact 2
* @return string
*/
public function getContact2Number () {
	$preValue = $this->preGetValue("contact2Number"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->contact2Number;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set contact2Number - Contact 2
* @param string $contact2Number
* @return \Pimcore\Model\DataObject\ContactPage
*/
public function setContact2Number ($contact2Number) {
	$fd = $this->getClass()->getFieldDefinition("contact2Number");
	$this->contact2Number = $contact2Number;
	return $this;
}

/**
* Get title1 - Title 1
* @return string
*/
public function getTitle1 () {
	$preValue = $this->preGetValue("title1"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->title1;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set title1 - Title 1
* @param string $title1
* @return \Pimcore\Model\DataObject\ContactPage
*/
public function setTitle1 ($title1) {
	$fd = $this->getClass()->getFieldDefinition("title1");
	$this->title1 = $title1;
	return $this;
}

/**
* Get description1 - Description 1
* @return string
*/
public function getDescription1 () {
	$preValue = $this->preGetValue("description1"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->description1;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set description1 - Description 1
* @param string $description1
* @return \Pimcore\Model\DataObject\ContactPage
*/
public function setDescription1 ($description1) {
	$fd = $this->getClass()->getFieldDefinition("description1");
	$this->description1 = $description1;
	return $this;
}

/**
* Get block1 - Block 1
* @return \Pimcore\Model\DataObject\Data\BlockElement[][]
*/
public function getBlock1 () {
	$preValue = $this->preGetValue("block1"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("block1")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set block1 - Block 1
* @param \Pimcore\Model\DataObject\Data\BlockElement[][] $block1
* @return \Pimcore\Model\DataObject\ContactPage
*/
public function setBlock1 ($block1) {
	$fd = $this->getClass()->getFieldDefinition("block1");
	$this->block1 = $fd->preSetData($this, $block1);
	return $this;
}

/**
* Get title2 - Title 2
* @return string
*/
public function getTitle2 () {
	$preValue = $this->preGetValue("title2"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->title2;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set title2 - Title 2
* @param string $title2
* @return \Pimcore\Model\DataObject\ContactPage
*/
public function setTitle2 ($title2) {
	$fd = $this->getClass()->getFieldDefinition("title2");
	$this->title2 = $title2;
	return $this;
}

/**
* Get description2 - Description 2
* @return string
*/
public function getDescription2 () {
	$preValue = $this->preGetValue("description2"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->description2;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set description2 - Description 2
* @param string $description2
* @return \Pimcore\Model\DataObject\ContactPage
*/
public function setDescription2 ($description2) {
	$fd = $this->getClass()->getFieldDefinition("description2");
	$this->description2 = $description2;
	return $this;
}

/**
* Get block2 - Block 2
* @return \Pimcore\Model\DataObject\Data\BlockElement[][]
*/
public function getBlock2 () {
	$preValue = $this->preGetValue("block2"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("block2")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set block2 - Block 2
* @param \Pimcore\Model\DataObject\Data\BlockElement[][] $block2
* @return \Pimcore\Model\DataObject\ContactPage
*/
public function setBlock2 ($block2) {
	$fd = $this->getClass()->getFieldDefinition("block2");
	$this->block2 = $fd->preSetData($this, $block2);
	return $this;
}

/**
* Get title3 - Title 3
* @return string
*/
public function getTitle3 () {
	$preValue = $this->preGetValue("title3"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->title3;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set title3 - Title 3
* @param string $title3
* @return \Pimcore\Model\DataObject\ContactPage
*/
public function setTitle3 ($title3) {
	$fd = $this->getClass()->getFieldDefinition("title3");
	$this->title3 = $title3;
	return $this;
}

/**
* Get description3 - Description 3
* @return string
*/
public function getDescription3 () {
	$preValue = $this->preGetValue("description3"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->description3;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set description3 - Description 3
* @param string $description3
* @return \Pimcore\Model\DataObject\ContactPage
*/
public function setDescription3 ($description3) {
	$fd = $this->getClass()->getFieldDefinition("description3");
	$this->description3 = $description3;
	return $this;
}

/**
* Get text1 - Text 1
* @return string
*/
public function getText1 () {
	$preValue = $this->preGetValue("text1"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->text1;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set text1 - Text 1
* @param string $text1
* @return \Pimcore\Model\DataObject\ContactPage
*/
public function setText1 ($text1) {
	$fd = $this->getClass()->getFieldDefinition("text1");
	$this->text1 = $text1;
	return $this;
}

/**
* Get text1Page - Text 1 Page
* @return \Pimcore\Model\Document\page
*/
public function getText1Page () {
	$preValue = $this->preGetValue("text1Page"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("text1Page")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set text1Page - Text 1 Page
* @param \Pimcore\Model\Document\page $text1Page
* @return \Pimcore\Model\DataObject\ContactPage
*/
public function setText1Page ($text1Page) {
	$fd = $this->getClass()->getFieldDefinition("text1Page");
	$currentData = $this->getText1Page();
	$isEqual = $fd->isEqual($currentData, $text1Page);
	if (!$isEqual) {
		$this->markFieldDirty("text1Page", true);
	}
	$this->text1Page = $fd->preSetData($this, $text1Page);
	return $this;
}

/**
* Get text1Image - Text 1 Image
* @return \Pimcore\Model\Asset\Image
*/
public function getText1Image () {
	$preValue = $this->preGetValue("text1Image"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->text1Image;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set text1Image - Text 1 Image
* @param \Pimcore\Model\Asset\Image $text1Image
* @return \Pimcore\Model\DataObject\ContactPage
*/
public function setText1Image ($text1Image) {
	$fd = $this->getClass()->getFieldDefinition("text1Image");
	$this->text1Image = $text1Image;
	return $this;
}

/**
* Get title4 - Title 4
* @return string
*/
public function getTitle4 () {
	$preValue = $this->preGetValue("title4"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->title4;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set title4 - Title 4
* @param string $title4
* @return \Pimcore\Model\DataObject\ContactPage
*/
public function setTitle4 ($title4) {
	$fd = $this->getClass()->getFieldDefinition("title4");
	$this->title4 = $title4;
	return $this;
}

/**
* Get description4 - Description 4
* @return string
*/
public function getDescription4 () {
	$preValue = $this->preGetValue("description4"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->description4;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set description4 - Description 4
* @param string $description4
* @return \Pimcore\Model\DataObject\ContactPage
*/
public function setDescription4 ($description4) {
	$fd = $this->getClass()->getFieldDefinition("description4");
	$this->description4 = $description4;
	return $this;
}

/**
* Get title5 - Title 5
* @return string
*/
public function getTitle5 () {
	$preValue = $this->preGetValue("title5"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->title5;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set title5 - Title 5
* @param string $title5
* @return \Pimcore\Model\DataObject\ContactPage
*/
public function setTitle5 ($title5) {
	$fd = $this->getClass()->getFieldDefinition("title5");
	$this->title5 = $title5;
	return $this;
}

/**
* Get block3 - Block 3
* @return \Pimcore\Model\DataObject\Data\BlockElement[][]
*/
public function getBlock3 () {
	$preValue = $this->preGetValue("block3"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("block3")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set block3 - Block 3
* @param \Pimcore\Model\DataObject\Data\BlockElement[][] $block3
* @return \Pimcore\Model\DataObject\ContactPage
*/
public function setBlock3 ($block3) {
	$fd = $this->getClass()->getFieldDefinition("block3");
	$this->block3 = $fd->preSetData($this, $block3);
	return $this;
}

protected static $_relationFields = array (
  'text1Page' => 
  array (
    'type' => 'href',
  ),
);

protected $lazyLoadedFields = array (
  0 => 'text1Page',
);

}

