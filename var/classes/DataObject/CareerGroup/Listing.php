<?php 

namespace Pimcore\Model\DataObject\CareerGroup;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\CareerGroup current()
 * @method DataObject\CareerGroup[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "60";
protected $className = "CareerGroup";


}
