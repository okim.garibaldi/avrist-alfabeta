<?php 

/** 
* Generated at: 2020-01-27T09:00:04+07:00
* Inheritance: no
* Variants: no
* Changed by: avristadm (2)
* IP: 172.17.0.1


Fields Summary: 
- siteId [select]
- syariah [checkbox]
- page [href]
- LOB [href]
- category [href]
- name [input]
- smallName [input]
- abstract [wysiwyg]
- pointsContent [wysiwyg]
- summary [wysiwyg]
- imageDetail [image]
- image [image]
- imageFeatures [fieldcollections]
- compareContent [wysiwyg]
- keluarga [select]
- tabungan [select]
- assets [objects]
- asuransi [objects]
- pensiun [select]
- benefits [wysiwyg]
- termsAndProcedures [wysiwyg]
- price [input]
- pricePerMonth [input]
- pricePerYear [input]
- brosur [input]
- riskFactorLow [numeric]
- riskFactorMedium [numeric]
- riskFactorHigh [numeric]
- minPrice [input]
*/ 

namespace Pimcore\Model\DataObject;



/**
* @method static \Pimcore\Model\DataObject\Product\Listing getBySiteId ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing getBySyariah ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing getByPage ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing getByLOB ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing getByCategory ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing getByName ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing getBySmallName ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing getByAbstract ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing getByPointsContent ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing getBySummary ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing getByImageDetail ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing getByImage ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing getByImageFeatures ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing getByCompareContent ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing getByKeluarga ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing getByTabungan ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing getByAssets ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing getByAsuransi ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing getByPensiun ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing getByBenefits ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing getByTermsAndProcedures ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing getByPrice ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing getByPricePerMonth ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing getByPricePerYear ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing getByBrosur ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing getByRiskFactorLow ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing getByRiskFactorMedium ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing getByRiskFactorHigh ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Product\Listing getByMinPrice ($value, $limit = 0) 
*/

class Product extends Concrete implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {



use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $o_classId = "12";
protected $o_className = "Product";
protected $siteId;
protected $syariah;
protected $page;
protected $LOB;
protected $category;
protected $name;
protected $smallName;
protected $abstract;
protected $pointsContent;
protected $summary;
protected $imageDetail;
protected $image;
protected $imageFeatures;
protected $compareContent;
protected $keluarga;
protected $tabungan;
protected $assets;
protected $asuransi;
protected $pensiun;
protected $benefits;
protected $termsAndProcedures;
protected $price;
protected $pricePerMonth;
protected $pricePerYear;
protected $brosur;
protected $riskFactorLow;
protected $riskFactorMedium;
protected $riskFactorHigh;
protected $minPrice;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\Product
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get siteId - Site
* @return string
*/
public function getSiteId () {
	$preValue = $this->preGetValue("siteId"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->siteId;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set siteId - Site
* @param string $siteId
* @return \Pimcore\Model\DataObject\Product
*/
public function setSiteId ($siteId) {
	$fd = $this->getClass()->getFieldDefinition("siteId");
	$this->siteId = $siteId;
	return $this;
}

/**
* Get syariah - syariah
* @return boolean
*/
public function getSyariah () {
	$preValue = $this->preGetValue("syariah"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->syariah;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set syariah - syariah
* @param boolean $syariah
* @return \Pimcore\Model\DataObject\Product
*/
public function setSyariah ($syariah) {
	$fd = $this->getClass()->getFieldDefinition("syariah");
	$this->syariah = $syariah;
	return $this;
}

/**
* Get page - Page
* @return \Pimcore\Model\Document\page
*/
public function getPage () {
	$preValue = $this->preGetValue("page"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("page")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set page - Page
* @param \Pimcore\Model\Document\page $page
* @return \Pimcore\Model\DataObject\Product
*/
public function setPage ($page) {
	$fd = $this->getClass()->getFieldDefinition("page");
	$currentData = $this->getPage();
	$isEqual = $fd->isEqual($currentData, $page);
	if (!$isEqual) {
		$this->markFieldDirty("page", true);
	}
	$this->page = $fd->preSetData($this, $page);
	return $this;
}

/**
* Get LOB - LOB
* @return \Pimcore\Model\DataObject\LOB
*/
public function getLOB () {
	$preValue = $this->preGetValue("LOB"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("LOB")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set LOB - LOB
* @param \Pimcore\Model\DataObject\LOB $LOB
* @return \Pimcore\Model\DataObject\Product
*/
public function setLOB ($LOB) {
	$fd = $this->getClass()->getFieldDefinition("LOB");
	$currentData = $this->getLOB();
	$isEqual = $fd->isEqual($currentData, $LOB);
	if (!$isEqual) {
		$this->markFieldDirty("LOB", true);
	}
	$this->LOB = $fd->preSetData($this, $LOB);
	return $this;
}

/**
* Get category - Category
* @return \Pimcore\Model\DataObject\ProductCategory
*/
public function getCategory () {
	$preValue = $this->preGetValue("category"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("category")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set category - Category
* @param \Pimcore\Model\DataObject\ProductCategory $category
* @return \Pimcore\Model\DataObject\Product
*/
public function setCategory ($category) {
	$fd = $this->getClass()->getFieldDefinition("category");
	$currentData = $this->getCategory();
	$isEqual = $fd->isEqual($currentData, $category);
	if (!$isEqual) {
		$this->markFieldDirty("category", true);
	}
	$this->category = $fd->preSetData($this, $category);
	return $this;
}

/**
* Get name - name
* @return string
*/
public function getName () {
	$preValue = $this->preGetValue("name"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->name;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set name - name
* @param string $name
* @return \Pimcore\Model\DataObject\Product
*/
public function setName ($name) {
	$fd = $this->getClass()->getFieldDefinition("name");
	$this->name = $name;
	return $this;
}

/**
* Get smallName - smallName
* @return string
*/
public function getSmallName () {
	$preValue = $this->preGetValue("smallName"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->smallName;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set smallName - smallName
* @param string $smallName
* @return \Pimcore\Model\DataObject\Product
*/
public function setSmallName ($smallName) {
	$fd = $this->getClass()->getFieldDefinition("smallName");
	$this->smallName = $smallName;
	return $this;
}

/**
* Get abstract - abstract
* @return string
*/
public function getAbstract () {
	$preValue = $this->preGetValue("abstract"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("abstract")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set abstract - abstract
* @param string $abstract
* @return \Pimcore\Model\DataObject\Product
*/
public function setAbstract ($abstract) {
	$fd = $this->getClass()->getFieldDefinition("abstract");
	$this->abstract = $abstract;
	return $this;
}

/**
* Get pointsContent - pointsContent
* @return string
*/
public function getPointsContent () {
	$preValue = $this->preGetValue("pointsContent"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("pointsContent")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set pointsContent - pointsContent
* @param string $pointsContent
* @return \Pimcore\Model\DataObject\Product
*/
public function setPointsContent ($pointsContent) {
	$fd = $this->getClass()->getFieldDefinition("pointsContent");
	$this->pointsContent = $pointsContent;
	return $this;
}

/**
* Get summary - summary
* @return string
*/
public function getSummary () {
	$preValue = $this->preGetValue("summary"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("summary")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set summary - summary
* @param string $summary
* @return \Pimcore\Model\DataObject\Product
*/
public function setSummary ($summary) {
	$fd = $this->getClass()->getFieldDefinition("summary");
	$this->summary = $summary;
	return $this;
}

/**
* Get imageDetail - Image Detail
* @return \Pimcore\Model\Asset\Image
*/
public function getImageDetail () {
	$preValue = $this->preGetValue("imageDetail"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->imageDetail;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set imageDetail - Image Detail
* @param \Pimcore\Model\Asset\Image $imageDetail
* @return \Pimcore\Model\DataObject\Product
*/
public function setImageDetail ($imageDetail) {
	$fd = $this->getClass()->getFieldDefinition("imageDetail");
	$this->imageDetail = $imageDetail;
	return $this;
}

/**
* Get image - image
* @return \Pimcore\Model\Asset\Image
*/
public function getImage () {
	$preValue = $this->preGetValue("image"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->image;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set image - image
* @param \Pimcore\Model\Asset\Image $image
* @return \Pimcore\Model\DataObject\Product
*/
public function setImage ($image) {
	$fd = $this->getClass()->getFieldDefinition("image");
	$this->image = $image;
	return $this;
}

/**
* @return \Pimcore\Model\DataObject\Fieldcollection
*/
public function getImageFeatures () {
	$preValue = $this->preGetValue("imageFeatures"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { return $preValue;}
	$data = $this->getClass()->getFieldDefinition("imageFeatures")->preGetData($this);
	 return $data;
}

/**
* Set imageFeatures - imageFeatures
* @param \Pimcore\Model\DataObject\Fieldcollection $imageFeatures
* @return \Pimcore\Model\DataObject\Product
*/
public function setImageFeatures ($imageFeatures) {
	$fd = $this->getClass()->getFieldDefinition("imageFeatures");
	$this->imageFeatures = $fd->preSetData($this, $imageFeatures);
	return $this;
}

/**
* Get compareContent - Compare Content
* @return string
*/
public function getCompareContent () {
	$preValue = $this->preGetValue("compareContent"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("compareContent")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set compareContent - Compare Content
* @param string $compareContent
* @return \Pimcore\Model\DataObject\Product
*/
public function setCompareContent ($compareContent) {
	$fd = $this->getClass()->getFieldDefinition("compareContent");
	$this->compareContent = $compareContent;
	return $this;
}

/**
* Get keluarga - keluarga
* @return string
*/
public function getKeluarga () {
	$preValue = $this->preGetValue("keluarga"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->keluarga;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set keluarga - keluarga
* @param string $keluarga
* @return \Pimcore\Model\DataObject\Product
*/
public function setKeluarga ($keluarga) {
	$fd = $this->getClass()->getFieldDefinition("keluarga");
	$this->keluarga = $keluarga;
	return $this;
}

/**
* Get tabungan - tabungan
* @return string
*/
public function getTabungan () {
	$preValue = $this->preGetValue("tabungan"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->tabungan;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set tabungan - tabungan
* @param string $tabungan
* @return \Pimcore\Model\DataObject\Product
*/
public function setTabungan ($tabungan) {
	$fd = $this->getClass()->getFieldDefinition("tabungan");
	$this->tabungan = $tabungan;
	return $this;
}

/**
* Get assets - assets
* @return \Pimcore\Model\DataObject\AssetQuestion[]
*/
public function getAssets () {
	$preValue = $this->preGetValue("assets"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("assets")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set assets - assets
* @param \Pimcore\Model\DataObject\AssetQuestion[] $assets
* @return \Pimcore\Model\DataObject\Product
*/
public function setAssets ($assets) {
	$fd = $this->getClass()->getFieldDefinition("assets");
	$currentData = $this->getAssets();
	$isEqual = $fd->isEqual($currentData, $assets);
	if (!$isEqual) {
		$this->markFieldDirty("assets", true);
	}
	$this->assets = $fd->preSetData($this, $assets);
	return $this;
}

/**
* Get asuransi - asuransi
* @return \Pimcore\Model\DataObject\AsuransiQuestion[]
*/
public function getAsuransi () {
	$preValue = $this->preGetValue("asuransi"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("asuransi")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set asuransi - asuransi
* @param \Pimcore\Model\DataObject\AsuransiQuestion[] $asuransi
* @return \Pimcore\Model\DataObject\Product
*/
public function setAsuransi ($asuransi) {
	$fd = $this->getClass()->getFieldDefinition("asuransi");
	$currentData = $this->getAsuransi();
	$isEqual = $fd->isEqual($currentData, $asuransi);
	if (!$isEqual) {
		$this->markFieldDirty("asuransi", true);
	}
	$this->asuransi = $fd->preSetData($this, $asuransi);
	return $this;
}

/**
* Get pensiun - pensiun
* @return string
*/
public function getPensiun () {
	$preValue = $this->preGetValue("pensiun"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->pensiun;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set pensiun - pensiun
* @param string $pensiun
* @return \Pimcore\Model\DataObject\Product
*/
public function setPensiun ($pensiun) {
	$fd = $this->getClass()->getFieldDefinition("pensiun");
	$this->pensiun = $pensiun;
	return $this;
}

/**
* Get benefits - benefits
* @return string
*/
public function getBenefits () {
	$preValue = $this->preGetValue("benefits"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("benefits")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set benefits - benefits
* @param string $benefits
* @return \Pimcore\Model\DataObject\Product
*/
public function setBenefits ($benefits) {
	$fd = $this->getClass()->getFieldDefinition("benefits");
	$this->benefits = $benefits;
	return $this;
}

/**
* Get termsAndProcedures - termsAndProcedures
* @return string
*/
public function getTermsAndProcedures () {
	$preValue = $this->preGetValue("termsAndProcedures"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("termsAndProcedures")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set termsAndProcedures - termsAndProcedures
* @param string $termsAndProcedures
* @return \Pimcore\Model\DataObject\Product
*/
public function setTermsAndProcedures ($termsAndProcedures) {
	$fd = $this->getClass()->getFieldDefinition("termsAndProcedures");
	$this->termsAndProcedures = $termsAndProcedures;
	return $this;
}

/**
* Get price - price
* @return string
*/
public function getPrice () {
	$preValue = $this->preGetValue("price"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->price;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set price - price
* @param string $price
* @return \Pimcore\Model\DataObject\Product
*/
public function setPrice ($price) {
	$fd = $this->getClass()->getFieldDefinition("price");
	$this->price = $price;
	return $this;
}

/**
* Get pricePerMonth - pricePerMonth
* @return string
*/
public function getPricePerMonth () {
	$preValue = $this->preGetValue("pricePerMonth"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->pricePerMonth;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set pricePerMonth - pricePerMonth
* @param string $pricePerMonth
* @return \Pimcore\Model\DataObject\Product
*/
public function setPricePerMonth ($pricePerMonth) {
	$fd = $this->getClass()->getFieldDefinition("pricePerMonth");
	$this->pricePerMonth = $pricePerMonth;
	return $this;
}

/**
* Get pricePerYear - pricePerYear
* @return string
*/
public function getPricePerYear () {
	$preValue = $this->preGetValue("pricePerYear"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->pricePerYear;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set pricePerYear - pricePerYear
* @param string $pricePerYear
* @return \Pimcore\Model\DataObject\Product
*/
public function setPricePerYear ($pricePerYear) {
	$fd = $this->getClass()->getFieldDefinition("pricePerYear");
	$this->pricePerYear = $pricePerYear;
	return $this;
}

/**
* Get brosur - brosur
* @return string
*/
public function getBrosur () {
	$preValue = $this->preGetValue("brosur"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->brosur;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set brosur - brosur
* @param string $brosur
* @return \Pimcore\Model\DataObject\Product
*/
public function setBrosur ($brosur) {
	$fd = $this->getClass()->getFieldDefinition("brosur");
	$this->brosur = $brosur;
	return $this;
}

/**
* Get riskFactorLow - Risk Factor Low
* @return float
*/
public function getRiskFactorLow () {
	$preValue = $this->preGetValue("riskFactorLow"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->riskFactorLow;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set riskFactorLow - Risk Factor Low
* @param float $riskFactorLow
* @return \Pimcore\Model\DataObject\Product
*/
public function setRiskFactorLow ($riskFactorLow) {
	$fd = $this->getClass()->getFieldDefinition("riskFactorLow");
	$this->riskFactorLow = $riskFactorLow;
	return $this;
}

/**
* Get riskFactorMedium - Risk Factor Medium
* @return float
*/
public function getRiskFactorMedium () {
	$preValue = $this->preGetValue("riskFactorMedium"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->riskFactorMedium;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set riskFactorMedium - Risk Factor Medium
* @param float $riskFactorMedium
* @return \Pimcore\Model\DataObject\Product
*/
public function setRiskFactorMedium ($riskFactorMedium) {
	$fd = $this->getClass()->getFieldDefinition("riskFactorMedium");
	$this->riskFactorMedium = $riskFactorMedium;
	return $this;
}

/**
* Get riskFactorHigh - Risk Factor High
* @return float
*/
public function getRiskFactorHigh () {
	$preValue = $this->preGetValue("riskFactorHigh"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->riskFactorHigh;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set riskFactorHigh - Risk Factor High
* @param float $riskFactorHigh
* @return \Pimcore\Model\DataObject\Product
*/
public function setRiskFactorHigh ($riskFactorHigh) {
	$fd = $this->getClass()->getFieldDefinition("riskFactorHigh");
	$this->riskFactorHigh = $riskFactorHigh;
	return $this;
}

/**
* Get minPrice - minPrice
* @return string
*/
public function getMinPrice () {
	$preValue = $this->preGetValue("minPrice"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->minPrice;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set minPrice - minPrice
* @param string $minPrice
* @return \Pimcore\Model\DataObject\Product
*/
public function setMinPrice ($minPrice) {
	$fd = $this->getClass()->getFieldDefinition("minPrice");
	$this->minPrice = $minPrice;
	return $this;
}

protected static $_relationFields = array (
  'page' => 
  array (
    'type' => 'href',
  ),
  'LOB' => 
  array (
    'type' => 'href',
  ),
  'category' => 
  array (
    'type' => 'href',
  ),
  'assets' => 
  array (
    'type' => 'objects',
  ),
  'asuransi' => 
  array (
    'type' => 'objects',
  ),
);

protected $lazyLoadedFields = array (
  0 => 'page',
  1 => 'LOB',
  2 => 'category',
  3 => 'imageFeatures',
  4 => 'assets',
  5 => 'asuransi',
);

}

