<?php 

/** 
* Generated at: 2020-01-08T12:26:47+07:00
* Inheritance: no
* Variants: no
* Changed by: avristadm (2)
* IP: 172.17.0.1


Fields Summary: 
- siteId [select]
- title [input]
- description [textarea]
- link [input]
- articleAmount [numeric]
*/ 

namespace Pimcore\Model\DataObject;



/**
* @method static \Pimcore\Model\DataObject\LifeGuidePage\Listing getBySiteId ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\LifeGuidePage\Listing getByTitle ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\LifeGuidePage\Listing getByDescription ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\LifeGuidePage\Listing getByLink ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\LifeGuidePage\Listing getByArticleAmount ($value, $limit = 0) 
*/

class LifeGuidePage extends Concrete implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {



use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $o_classId = "81";
protected $o_className = "LifeGuidePage";
protected $siteId;
protected $title;
protected $description;
protected $link;
protected $articleAmount;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\LifeGuidePage
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get siteId - Site
* @return string
*/
public function getSiteId () {
	$preValue = $this->preGetValue("siteId"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->siteId;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set siteId - Site
* @param string $siteId
* @return \Pimcore\Model\DataObject\LifeGuidePage
*/
public function setSiteId ($siteId) {
	$fd = $this->getClass()->getFieldDefinition("siteId");
	$this->siteId = $siteId;
	return $this;
}

/**
* Get title - Title 
* @return string
*/
public function getTitle () {
	$preValue = $this->preGetValue("title"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->title;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set title - Title 
* @param string $title
* @return \Pimcore\Model\DataObject\LifeGuidePage
*/
public function setTitle ($title) {
	$fd = $this->getClass()->getFieldDefinition("title");
	$this->title = $title;
	return $this;
}

/**
* Get description - Description 
* @return string
*/
public function getDescription () {
	$preValue = $this->preGetValue("description"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->description;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set description - Description 
* @param string $description
* @return \Pimcore\Model\DataObject\LifeGuidePage
*/
public function setDescription ($description) {
	$fd = $this->getClass()->getFieldDefinition("description");
	$this->description = $description;
	return $this;
}

/**
* Get link - Link
* @return string
*/
public function getLink () {
	$preValue = $this->preGetValue("link"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->link;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set link - Link
* @param string $link
* @return \Pimcore\Model\DataObject\LifeGuidePage
*/
public function setLink ($link) {
	$fd = $this->getClass()->getFieldDefinition("link");
	$this->link = $link;
	return $this;
}

/**
* Get articleAmount - article Amount
* @return float
*/
public function getArticleAmount () {
	$preValue = $this->preGetValue("articleAmount"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->articleAmount;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set articleAmount - article Amount
* @param float $articleAmount
* @return \Pimcore\Model\DataObject\LifeGuidePage
*/
public function setArticleAmount ($articleAmount) {
	$fd = $this->getClass()->getFieldDefinition("articleAmount");
	$this->articleAmount = $articleAmount;
	return $this;
}

protected static $_relationFields = array (
);

protected $lazyLoadedFields = array (
);

}

