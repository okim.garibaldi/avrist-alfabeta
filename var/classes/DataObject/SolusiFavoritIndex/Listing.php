<?php 

namespace Pimcore\Model\DataObject\SolusiFavoritIndex;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\SolusiFavoritIndex current()
 * @method DataObject\SolusiFavoritIndex[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "32";
protected $className = "SolusiFavoritIndex";


}
