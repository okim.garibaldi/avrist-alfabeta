<?php 

/** 
* Generated at: 2019-12-05T11:42:58+07:00
* Inheritance: no
* Variants: no
* Changed by: avristadm (2)
* IP: 172.17.0.1


Fields Summary: 
- postDate [date]
- image1 [image]
- page [href]
- title1 [input]
- description1 [wysiwyg]
- title2 [input]
- description2 [wysiwyg]
- block1 [block]
-- imageBlock [image]
-- titleBlock [input]
-- descriptionBlock [wysiwyg]
*/ 

namespace Pimcore\Model\DataObject;



/**
* @method static \Pimcore\Model\DataObject\PartnershipDetails\Listing getByPostDate ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\PartnershipDetails\Listing getByImage1 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\PartnershipDetails\Listing getByPage ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\PartnershipDetails\Listing getByTitle1 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\PartnershipDetails\Listing getByDescription1 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\PartnershipDetails\Listing getByTitle2 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\PartnershipDetails\Listing getByDescription2 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\PartnershipDetails\Listing getByBlock1 ($value, $limit = 0) 
*/

class PartnershipDetails extends Concrete implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {



use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $o_classId = "62";
protected $o_className = "PartnershipDetails";
protected $postDate;
protected $image1;
protected $page;
protected $title1;
protected $description1;
protected $title2;
protected $description2;
protected $block1;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\PartnershipDetails
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get postDate - Post Date
* @return \Carbon\Carbon
*/
public function getPostDate () {
	$preValue = $this->preGetValue("postDate"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->postDate;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set postDate - Post Date
* @param \Carbon\Carbon $postDate
* @return \Pimcore\Model\DataObject\PartnershipDetails
*/
public function setPostDate ($postDate) {
	$fd = $this->getClass()->getFieldDefinition("postDate");
	$this->postDate = $postDate;
	return $this;
}

/**
* Get image1 - Image 1
* @return \Pimcore\Model\Asset\Image
*/
public function getImage1 () {
	$preValue = $this->preGetValue("image1"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->image1;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set image1 - Image 1
* @param \Pimcore\Model\Asset\Image $image1
* @return \Pimcore\Model\DataObject\PartnershipDetails
*/
public function setImage1 ($image1) {
	$fd = $this->getClass()->getFieldDefinition("image1");
	$this->image1 = $image1;
	return $this;
}

/**
* Get page - Page
* @return \Pimcore\Model\Document\page
*/
public function getPage () {
	$preValue = $this->preGetValue("page"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("page")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set page - Page
* @param \Pimcore\Model\Document\page $page
* @return \Pimcore\Model\DataObject\PartnershipDetails
*/
public function setPage ($page) {
	$fd = $this->getClass()->getFieldDefinition("page");
	$currentData = $this->getPage();
	$isEqual = $fd->isEqual($currentData, $page);
	if (!$isEqual) {
		$this->markFieldDirty("page", true);
	}
	$this->page = $fd->preSetData($this, $page);
	return $this;
}

/**
* Get title1 - Title 1
* @return string
*/
public function getTitle1 () {
	$preValue = $this->preGetValue("title1"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->title1;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set title1 - Title 1
* @param string $title1
* @return \Pimcore\Model\DataObject\PartnershipDetails
*/
public function setTitle1 ($title1) {
	$fd = $this->getClass()->getFieldDefinition("title1");
	$this->title1 = $title1;
	return $this;
}

/**
* Get description1 - Description 1
* @return string
*/
public function getDescription1 () {
	$preValue = $this->preGetValue("description1"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("description1")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set description1 - Description 1
* @param string $description1
* @return \Pimcore\Model\DataObject\PartnershipDetails
*/
public function setDescription1 ($description1) {
	$fd = $this->getClass()->getFieldDefinition("description1");
	$this->description1 = $description1;
	return $this;
}

/**
* Get title2 - Title 2
* @return string
*/
public function getTitle2 () {
	$preValue = $this->preGetValue("title2"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->title2;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set title2 - Title 2
* @param string $title2
* @return \Pimcore\Model\DataObject\PartnershipDetails
*/
public function setTitle2 ($title2) {
	$fd = $this->getClass()->getFieldDefinition("title2");
	$this->title2 = $title2;
	return $this;
}

/**
* Get description2 - Description 2
* @return string
*/
public function getDescription2 () {
	$preValue = $this->preGetValue("description2"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("description2")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set description2 - Description 2
* @param string $description2
* @return \Pimcore\Model\DataObject\PartnershipDetails
*/
public function setDescription2 ($description2) {
	$fd = $this->getClass()->getFieldDefinition("description2");
	$this->description2 = $description2;
	return $this;
}

/**
* Get block1 - Block1
* @return \Pimcore\Model\DataObject\Data\BlockElement[][]
*/
public function getBlock1 () {
	$preValue = $this->preGetValue("block1"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("block1")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set block1 - Block1
* @param \Pimcore\Model\DataObject\Data\BlockElement[][] $block1
* @return \Pimcore\Model\DataObject\PartnershipDetails
*/
public function setBlock1 ($block1) {
	$fd = $this->getClass()->getFieldDefinition("block1");
	$this->block1 = $fd->preSetData($this, $block1);
	return $this;
}

protected static $_relationFields = array (
  'page' => 
  array (
    'type' => 'href',
  ),
);

protected $lazyLoadedFields = array (
  0 => 'page',
);

}

