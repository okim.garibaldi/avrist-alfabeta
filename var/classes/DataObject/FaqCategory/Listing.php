<?php 

namespace Pimcore\Model\DataObject\FaqCategory;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\FaqCategory current()
 * @method DataObject\FaqCategory[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "21";
protected $className = "FaqCategory";


}
