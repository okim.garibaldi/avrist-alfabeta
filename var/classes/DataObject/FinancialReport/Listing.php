<?php 

namespace Pimcore\Model\DataObject\FinancialReport;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\FinancialReport current()
 * @method DataObject\FinancialReport[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "15";
protected $className = "FinancialReport";


}
