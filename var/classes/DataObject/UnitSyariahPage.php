<?php 

/** 
* Generated at: 2020-01-27T16:59:39+07:00
* Inheritance: no
* Variants: no
* Changed by: avristadm (2)
* IP: 172.17.0.1


Fields Summary: 
- siteId [select]
- title1 [input]
- description [textarea]
- title2 [input]
- block1 [block]
-- title [input]
-- subtitle [input]
-- description [textarea]
-- image [image]
- title3 [input]
- block2 [block]
-- image [image]
-- description [textarea]
*/ 

namespace Pimcore\Model\DataObject;



/**
* @method static \Pimcore\Model\DataObject\UnitSyariahPage\Listing getBySiteId ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\UnitSyariahPage\Listing getByTitle1 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\UnitSyariahPage\Listing getByDescription ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\UnitSyariahPage\Listing getByTitle2 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\UnitSyariahPage\Listing getByBlock1 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\UnitSyariahPage\Listing getByTitle3 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\UnitSyariahPage\Listing getByBlock2 ($value, $limit = 0) 
*/

class UnitSyariahPage extends Concrete implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {



use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $o_classId = "86";
protected $o_className = "UnitSyariahPage";
protected $siteId;
protected $title1;
protected $description;
protected $title2;
protected $block1;
protected $title3;
protected $block2;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\UnitSyariahPage
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get siteId - Site
* @return string
*/
public function getSiteId () {
	$preValue = $this->preGetValue("siteId"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->siteId;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set siteId - Site
* @param string $siteId
* @return \Pimcore\Model\DataObject\UnitSyariahPage
*/
public function setSiteId ($siteId) {
	$fd = $this->getClass()->getFieldDefinition("siteId");
	$this->siteId = $siteId;
	return $this;
}

/**
* Get title1 - Title  1
* @return string
*/
public function getTitle1 () {
	$preValue = $this->preGetValue("title1"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->title1;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set title1 - Title  1
* @param string $title1
* @return \Pimcore\Model\DataObject\UnitSyariahPage
*/
public function setTitle1 ($title1) {
	$fd = $this->getClass()->getFieldDefinition("title1");
	$this->title1 = $title1;
	return $this;
}

/**
* Get description - description 
* @return string
*/
public function getDescription () {
	$preValue = $this->preGetValue("description"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->description;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set description - description 
* @param string $description
* @return \Pimcore\Model\DataObject\UnitSyariahPage
*/
public function setDescription ($description) {
	$fd = $this->getClass()->getFieldDefinition("description");
	$this->description = $description;
	return $this;
}

/**
* Get title2 - Title 2
* @return string
*/
public function getTitle2 () {
	$preValue = $this->preGetValue("title2"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->title2;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set title2 - Title 2
* @param string $title2
* @return \Pimcore\Model\DataObject\UnitSyariahPage
*/
public function setTitle2 ($title2) {
	$fd = $this->getClass()->getFieldDefinition("title2");
	$this->title2 = $title2;
	return $this;
}

/**
* Get block1 - Block 1
* @return \Pimcore\Model\DataObject\Data\BlockElement[][]
*/
public function getBlock1 () {
	$preValue = $this->preGetValue("block1"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("block1")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set block1 - Block 1
* @param \Pimcore\Model\DataObject\Data\BlockElement[][] $block1
* @return \Pimcore\Model\DataObject\UnitSyariahPage
*/
public function setBlock1 ($block1) {
	$fd = $this->getClass()->getFieldDefinition("block1");
	$this->block1 = $fd->preSetData($this, $block1);
	return $this;
}

/**
* Get title3 - Title 3
* @return string
*/
public function getTitle3 () {
	$preValue = $this->preGetValue("title3"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->title3;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set title3 - Title 3
* @param string $title3
* @return \Pimcore\Model\DataObject\UnitSyariahPage
*/
public function setTitle3 ($title3) {
	$fd = $this->getClass()->getFieldDefinition("title3");
	$this->title3 = $title3;
	return $this;
}

/**
* Get block2 - Block 2
* @return \Pimcore\Model\DataObject\Data\BlockElement[][]
*/
public function getBlock2 () {
	$preValue = $this->preGetValue("block2"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("block2")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set block2 - Block 2
* @param \Pimcore\Model\DataObject\Data\BlockElement[][] $block2
* @return \Pimcore\Model\DataObject\UnitSyariahPage
*/
public function setBlock2 ($block2) {
	$fd = $this->getClass()->getFieldDefinition("block2");
	$this->block2 = $fd->preSetData($this, $block2);
	return $this;
}

protected static $_relationFields = array (
);

protected $lazyLoadedFields = array (
);

}

