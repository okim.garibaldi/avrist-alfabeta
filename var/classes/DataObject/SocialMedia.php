<?php 

/** 
* Generated at: 2020-01-01T06:44:49+07:00
* Inheritance: no
* Variants: no
* Changed by: avristadm (2)
* IP: 172.17.0.1


Fields Summary: 
- Image [image]
- name [input]
- link [input]
*/ 

namespace Pimcore\Model\DataObject;



/**
* @method static \Pimcore\Model\DataObject\SocialMedia\Listing getByImage ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\SocialMedia\Listing getByName ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\SocialMedia\Listing getByLink ($value, $limit = 0) 
*/

class SocialMedia extends Concrete implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {



use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $o_classId = "71";
protected $o_className = "SocialMedia";
protected $Image;
protected $name;
protected $link;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\SocialMedia
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get Image - Image 
* @return \Pimcore\Model\Asset\Image
*/
public function getImage () {
	$preValue = $this->preGetValue("Image"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->Image;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set Image - Image 
* @param \Pimcore\Model\Asset\Image $Image
* @return \Pimcore\Model\DataObject\SocialMedia
*/
public function setImage ($Image) {
	$fd = $this->getClass()->getFieldDefinition("Image");
	$this->Image = $Image;
	return $this;
}

/**
* Get name - Name
* @return string
*/
public function getName () {
	$preValue = $this->preGetValue("name"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->name;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set name - Name
* @param string $name
* @return \Pimcore\Model\DataObject\SocialMedia
*/
public function setName ($name) {
	$fd = $this->getClass()->getFieldDefinition("name");
	$this->name = $name;
	return $this;
}

/**
* Get link - Link
* @return string
*/
public function getLink () {
	$preValue = $this->preGetValue("link"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->link;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set link - Link
* @param string $link
* @return \Pimcore\Model\DataObject\SocialMedia
*/
public function setLink ($link) {
	$fd = $this->getClass()->getFieldDefinition("link");
	$this->link = $link;
	return $this;
}

protected static $_relationFields = array (
);

protected $lazyLoadedFields = array (
);

}

