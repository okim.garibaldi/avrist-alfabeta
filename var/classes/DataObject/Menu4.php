<?php 

/** 
* Generated at: 2020-01-23T10:59:42+07:00
* Inheritance: no
* Variants: no
* Changed by: avristadm (2)
* IP: 172.17.0.1


Fields Summary: 
- siteId [select]
- name [input]
- description [textarea]
- link [input]
- prefixDisabled [checkbox]
*/ 

namespace Pimcore\Model\DataObject;



/**
* @method static \Pimcore\Model\DataObject\Menu4\Listing getBySiteId ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Menu4\Listing getByName ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Menu4\Listing getByDescription ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Menu4\Listing getByLink ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Menu4\Listing getByPrefixDisabled ($value, $limit = 0) 
*/

class Menu4 extends Concrete implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {



use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $o_classId = "70";
protected $o_className = "Menu4";
protected $siteId;
protected $name;
protected $description;
protected $link;
protected $prefixDisabled;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\Menu4
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get siteId - Site
* @return string
*/
public function getSiteId () {
	$preValue = $this->preGetValue("siteId"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->siteId;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set siteId - Site
* @param string $siteId
* @return \Pimcore\Model\DataObject\Menu4
*/
public function setSiteId ($siteId) {
	$fd = $this->getClass()->getFieldDefinition("siteId");
	$this->siteId = $siteId;
	return $this;
}

/**
* Get name - name
* @return string
*/
public function getName () {
	$preValue = $this->preGetValue("name"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->name;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set name - name
* @param string $name
* @return \Pimcore\Model\DataObject\Menu4
*/
public function setName ($name) {
	$fd = $this->getClass()->getFieldDefinition("name");
	$this->name = $name;
	return $this;
}

/**
* Get description - Description
* @return string
*/
public function getDescription () {
	$preValue = $this->preGetValue("description"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->description;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set description - Description
* @param string $description
* @return \Pimcore\Model\DataObject\Menu4
*/
public function setDescription ($description) {
	$fd = $this->getClass()->getFieldDefinition("description");
	$this->description = $description;
	return $this;
}

/**
* Get link - Link
* @return string
*/
public function getLink () {
	$preValue = $this->preGetValue("link"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->link;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set link - Link
* @param string $link
* @return \Pimcore\Model\DataObject\Menu4
*/
public function setLink ($link) {
	$fd = $this->getClass()->getFieldDefinition("link");
	$this->link = $link;
	return $this;
}

/**
* Get prefixDisabled - Prefix Disabled
* @return boolean
*/
public function getPrefixDisabled () {
	$preValue = $this->preGetValue("prefixDisabled"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->prefixDisabled;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set prefixDisabled - Prefix Disabled
* @param boolean $prefixDisabled
* @return \Pimcore\Model\DataObject\Menu4
*/
public function setPrefixDisabled ($prefixDisabled) {
	$fd = $this->getClass()->getFieldDefinition("prefixDisabled");
	$this->prefixDisabled = $prefixDisabled;
	return $this;
}

protected static $_relationFields = array (
);

protected $lazyLoadedFields = array (
);

}

