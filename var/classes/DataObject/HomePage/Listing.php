<?php 

namespace Pimcore\Model\DataObject\HomePage;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\HomePage current()
 * @method DataObject\HomePage[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "94";
protected $className = "HomePage";


}
