<?php 

namespace Pimcore\Model\DataObject\Milestones;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\Milestones current()
 * @method DataObject\Milestones[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "54";
protected $className = "Milestones";


}
