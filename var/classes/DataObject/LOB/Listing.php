<?php 

namespace Pimcore\Model\DataObject\LOB;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\LOB current()
 * @method DataObject\LOB[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "1";
protected $className = "LOB";


}
