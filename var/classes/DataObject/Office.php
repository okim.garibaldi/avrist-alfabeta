<?php 

/** 
* Generated at: 2019-06-20T11:39:25+07:00
* Inheritance: no
* Variants: no
* Changed by: avristadm (2)
* IP: 202.87.248.23


Fields Summary: 
- LOB [href]
- city [href]
- name [input]
- officeType [input]
- address [textarea]
- callCenter [input]
- fax [input]
- email [input]
*/ 

namespace Pimcore\Model\DataObject;



/**
* @method static \Pimcore\Model\DataObject\Office\Listing getByLOB ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Office\Listing getByCity ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Office\Listing getByName ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Office\Listing getByOfficeType ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Office\Listing getByAddress ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Office\Listing getByCallCenter ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Office\Listing getByFax ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Office\Listing getByEmail ($value, $limit = 0) 
*/

class Office extends Concrete implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {



use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $o_classId = "16";
protected $o_className = "Office";
protected $LOB;
protected $city;
protected $name;
protected $officeType;
protected $address;
protected $callCenter;
protected $fax;
protected $email;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\Office
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get LOB - LOB
* @return \Pimcore\Model\DataObject\LOB
*/
public function getLOB () {
	$preValue = $this->preGetValue("LOB"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("LOB")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set LOB - LOB
* @param \Pimcore\Model\DataObject\LOB $LOB
* @return \Pimcore\Model\DataObject\Office
*/
public function setLOB ($LOB) {
	$fd = $this->getClass()->getFieldDefinition("LOB");
	$currentData = $this->getLOB();
	$isEqual = $fd->isEqual($currentData, $LOB);
	if (!$isEqual) {
		$this->markFieldDirty("LOB", true);
	}
	$this->LOB = $fd->preSetData($this, $LOB);
	return $this;
}

/**
* Get city - City
* @return \Pimcore\Model\DataObject\City
*/
public function getCity () {
	$preValue = $this->preGetValue("city"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("city")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set city - City
* @param \Pimcore\Model\DataObject\City $city
* @return \Pimcore\Model\DataObject\Office
*/
public function setCity ($city) {
	$fd = $this->getClass()->getFieldDefinition("city");
	$currentData = $this->getCity();
	$isEqual = $fd->isEqual($currentData, $city);
	if (!$isEqual) {
		$this->markFieldDirty("city", true);
	}
	$this->city = $fd->preSetData($this, $city);
	return $this;
}

/**
* Get name - Office Name
* @return string
*/
public function getName () {
	$preValue = $this->preGetValue("name"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->name;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set name - Office Name
* @param string $name
* @return \Pimcore\Model\DataObject\Office
*/
public function setName ($name) {
	$fd = $this->getClass()->getFieldDefinition("name");
	$this->name = $name;
	return $this;
}

/**
* Get officeType - Office Type
* @return string
*/
public function getOfficeType () {
	$preValue = $this->preGetValue("officeType"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->officeType;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set officeType - Office Type
* @param string $officeType
* @return \Pimcore\Model\DataObject\Office
*/
public function setOfficeType ($officeType) {
	$fd = $this->getClass()->getFieldDefinition("officeType");
	$this->officeType = $officeType;
	return $this;
}

/**
* Get address - Office Address
* @return string
*/
public function getAddress () {
	$preValue = $this->preGetValue("address"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->address;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set address - Office Address
* @param string $address
* @return \Pimcore\Model\DataObject\Office
*/
public function setAddress ($address) {
	$fd = $this->getClass()->getFieldDefinition("address");
	$this->address = $address;
	return $this;
}

/**
* Get callCenter - Office Call Center
* @return string
*/
public function getCallCenter () {
	$preValue = $this->preGetValue("callCenter"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->callCenter;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set callCenter - Office Call Center
* @param string $callCenter
* @return \Pimcore\Model\DataObject\Office
*/
public function setCallCenter ($callCenter) {
	$fd = $this->getClass()->getFieldDefinition("callCenter");
	$this->callCenter = $callCenter;
	return $this;
}

/**
* Get fax - Office FAX
* @return string
*/
public function getFax () {
	$preValue = $this->preGetValue("fax"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->fax;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set fax - Office FAX
* @param string $fax
* @return \Pimcore\Model\DataObject\Office
*/
public function setFax ($fax) {
	$fd = $this->getClass()->getFieldDefinition("fax");
	$this->fax = $fax;
	return $this;
}

/**
* Get email - Office Email
* @return string
*/
public function getEmail () {
	$preValue = $this->preGetValue("email"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->email;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set email - Office Email
* @param string $email
* @return \Pimcore\Model\DataObject\Office
*/
public function setEmail ($email) {
	$fd = $this->getClass()->getFieldDefinition("email");
	$this->email = $email;
	return $this;
}

protected static $_relationFields = array (
  'LOB' => 
  array (
    'type' => 'href',
  ),
  'city' => 
  array (
    'type' => 'href',
  ),
);

protected $lazyLoadedFields = array (
  0 => 'LOB',
  1 => 'city',
);

}

