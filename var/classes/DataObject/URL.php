<?php 

/** 
* Generated at: 2019-07-31T07:58:17+07:00
* Inheritance: no
* Variants: no
* Changed by: avristadm (2)
* IP: 172.17.0.1


Fields Summary: 
- home [textarea]
- life [textarea]
- general [textarea]
- am [textarea]
*/ 

namespace Pimcore\Model\DataObject;



/**
* @method static \Pimcore\Model\DataObject\URL\Listing getByHome ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\URL\Listing getByLife ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\URL\Listing getByGeneral ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\URL\Listing getByAm ($value, $limit = 0) 
*/

class URL extends Concrete implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {



use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $o_classId = "38";
protected $o_className = "URL";
protected $home;
protected $life;
protected $general;
protected $am;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\URL
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get home - Home
* @return string
*/
public function getHome () {
	$preValue = $this->preGetValue("home"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->home;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set home - Home
* @param string $home
* @return \Pimcore\Model\DataObject\URL
*/
public function setHome ($home) {
	$fd = $this->getClass()->getFieldDefinition("home");
	$this->home = $home;
	return $this;
}

/**
* Get life - Life
* @return string
*/
public function getLife () {
	$preValue = $this->preGetValue("life"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->life;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set life - Life
* @param string $life
* @return \Pimcore\Model\DataObject\URL
*/
public function setLife ($life) {
	$fd = $this->getClass()->getFieldDefinition("life");
	$this->life = $life;
	return $this;
}

/**
* Get general - General
* @return string
*/
public function getGeneral () {
	$preValue = $this->preGetValue("general"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->general;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set general - General
* @param string $general
* @return \Pimcore\Model\DataObject\URL
*/
public function setGeneral ($general) {
	$fd = $this->getClass()->getFieldDefinition("general");
	$this->general = $general;
	return $this;
}

/**
* Get am - Am
* @return string
*/
public function getAm () {
	$preValue = $this->preGetValue("am"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->am;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set am - Am
* @param string $am
* @return \Pimcore\Model\DataObject\URL
*/
public function setAm ($am) {
	$fd = $this->getClass()->getFieldDefinition("am");
	$this->am = $am;
	return $this;
}

protected static $_relationFields = array (
);

protected $lazyLoadedFields = array (
);

}

