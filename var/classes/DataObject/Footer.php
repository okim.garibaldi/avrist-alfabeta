<?php 

/** 
* Generated at: 2020-01-27T16:29:43+07:00
* Inheritance: no
* Variants: no
* Changed by: avristadm (2)
* IP: 172.17.0.1


Fields Summary: 
- siteId [select]
- image1 [image]
- text1 [input]
- image2 [image]
- image3 [image]
- block1 [block]
-- name [input]
-- page [input]
-- prefixDisabled [checkbox]
- text2 [input]
- text3 [input]
- text4 [input]
- text5 [input]
- facebook [input]
- twitter [input]
- instagram [input]
- linkedin [input]
*/ 

namespace Pimcore\Model\DataObject;



/**
* @method static \Pimcore\Model\DataObject\Footer\Listing getBySiteId ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Footer\Listing getByImage1 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Footer\Listing getByText1 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Footer\Listing getByImage2 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Footer\Listing getByImage3 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Footer\Listing getByBlock1 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Footer\Listing getByText2 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Footer\Listing getByText3 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Footer\Listing getByText4 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Footer\Listing getByText5 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Footer\Listing getByFacebook ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Footer\Listing getByTwitter ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Footer\Listing getByInstagram ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Footer\Listing getByLinkedin ($value, $limit = 0) 
*/

class Footer extends Concrete implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {



use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $o_classId = "64";
protected $o_className = "Footer";
protected $siteId;
protected $image1;
protected $text1;
protected $image2;
protected $image3;
protected $block1;
protected $text2;
protected $text3;
protected $text4;
protected $text5;
protected $facebook;
protected $twitter;
protected $instagram;
protected $linkedin;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\Footer
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get siteId - Site
* @return string
*/
public function getSiteId () {
	$preValue = $this->preGetValue("siteId"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->siteId;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set siteId - Site
* @param string $siteId
* @return \Pimcore\Model\DataObject\Footer
*/
public function setSiteId ($siteId) {
	$fd = $this->getClass()->getFieldDefinition("siteId");
	$this->siteId = $siteId;
	return $this;
}

/**
* Get image1 - Image 1
* @return \Pimcore\Model\Asset\Image
*/
public function getImage1 () {
	$preValue = $this->preGetValue("image1"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->image1;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set image1 - Image 1
* @param \Pimcore\Model\Asset\Image $image1
* @return \Pimcore\Model\DataObject\Footer
*/
public function setImage1 ($image1) {
	$fd = $this->getClass()->getFieldDefinition("image1");
	$this->image1 = $image1;
	return $this;
}

/**
* Get text1 - Text 1
* @return string
*/
public function getText1 () {
	$preValue = $this->preGetValue("text1"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->text1;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set text1 - Text 1
* @param string $text1
* @return \Pimcore\Model\DataObject\Footer
*/
public function setText1 ($text1) {
	$fd = $this->getClass()->getFieldDefinition("text1");
	$this->text1 = $text1;
	return $this;
}

/**
* Get image2 - Image 2
* @return \Pimcore\Model\Asset\Image
*/
public function getImage2 () {
	$preValue = $this->preGetValue("image2"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->image2;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set image2 - Image 2
* @param \Pimcore\Model\Asset\Image $image2
* @return \Pimcore\Model\DataObject\Footer
*/
public function setImage2 ($image2) {
	$fd = $this->getClass()->getFieldDefinition("image2");
	$this->image2 = $image2;
	return $this;
}

/**
* Get image3 - Image 3
* @return \Pimcore\Model\Asset\Image
*/
public function getImage3 () {
	$preValue = $this->preGetValue("image3"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->image3;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set image3 - Image 3
* @param \Pimcore\Model\Asset\Image $image3
* @return \Pimcore\Model\DataObject\Footer
*/
public function setImage3 ($image3) {
	$fd = $this->getClass()->getFieldDefinition("image3");
	$this->image3 = $image3;
	return $this;
}

/**
* Get block1 - Block 1
* @return \Pimcore\Model\DataObject\Data\BlockElement[][]
*/
public function getBlock1 () {
	$preValue = $this->preGetValue("block1"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("block1")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set block1 - Block 1
* @param \Pimcore\Model\DataObject\Data\BlockElement[][] $block1
* @return \Pimcore\Model\DataObject\Footer
*/
public function setBlock1 ($block1) {
	$fd = $this->getClass()->getFieldDefinition("block1");
	$this->block1 = $fd->preSetData($this, $block1);
	return $this;
}

/**
* Get text2 - Text 2
* @return string
*/
public function getText2 () {
	$preValue = $this->preGetValue("text2"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->text2;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set text2 - Text 2
* @param string $text2
* @return \Pimcore\Model\DataObject\Footer
*/
public function setText2 ($text2) {
	$fd = $this->getClass()->getFieldDefinition("text2");
	$this->text2 = $text2;
	return $this;
}

/**
* Get text3 - Text 3
* @return string
*/
public function getText3 () {
	$preValue = $this->preGetValue("text3"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->text3;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set text3 - Text 3
* @param string $text3
* @return \Pimcore\Model\DataObject\Footer
*/
public function setText3 ($text3) {
	$fd = $this->getClass()->getFieldDefinition("text3");
	$this->text3 = $text3;
	return $this;
}

/**
* Get text4 - Text 4
* @return string
*/
public function getText4 () {
	$preValue = $this->preGetValue("text4"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->text4;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set text4 - Text 4
* @param string $text4
* @return \Pimcore\Model\DataObject\Footer
*/
public function setText4 ($text4) {
	$fd = $this->getClass()->getFieldDefinition("text4");
	$this->text4 = $text4;
	return $this;
}

/**
* Get text5 - Text 5
* @return string
*/
public function getText5 () {
	$preValue = $this->preGetValue("text5"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->text5;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set text5 - Text 5
* @param string $text5
* @return \Pimcore\Model\DataObject\Footer
*/
public function setText5 ($text5) {
	$fd = $this->getClass()->getFieldDefinition("text5");
	$this->text5 = $text5;
	return $this;
}

/**
* Get facebook - Facebook
* @return string
*/
public function getFacebook () {
	$preValue = $this->preGetValue("facebook"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->facebook;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set facebook - Facebook
* @param string $facebook
* @return \Pimcore\Model\DataObject\Footer
*/
public function setFacebook ($facebook) {
	$fd = $this->getClass()->getFieldDefinition("facebook");
	$this->facebook = $facebook;
	return $this;
}

/**
* Get twitter - Twitter
* @return string
*/
public function getTwitter () {
	$preValue = $this->preGetValue("twitter"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->twitter;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set twitter - Twitter
* @param string $twitter
* @return \Pimcore\Model\DataObject\Footer
*/
public function setTwitter ($twitter) {
	$fd = $this->getClass()->getFieldDefinition("twitter");
	$this->twitter = $twitter;
	return $this;
}

/**
* Get instagram - Instagram
* @return string
*/
public function getInstagram () {
	$preValue = $this->preGetValue("instagram"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->instagram;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set instagram - Instagram
* @param string $instagram
* @return \Pimcore\Model\DataObject\Footer
*/
public function setInstagram ($instagram) {
	$fd = $this->getClass()->getFieldDefinition("instagram");
	$this->instagram = $instagram;
	return $this;
}

/**
* Get linkedin - LinkedIn
* @return string
*/
public function getLinkedin () {
	$preValue = $this->preGetValue("linkedin"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->linkedin;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set linkedin - LinkedIn
* @param string $linkedin
* @return \Pimcore\Model\DataObject\Footer
*/
public function setLinkedin ($linkedin) {
	$fd = $this->getClass()->getFieldDefinition("linkedin");
	$this->linkedin = $linkedin;
	return $this;
}

protected static $_relationFields = array (
);

protected $lazyLoadedFields = array (
);

}

