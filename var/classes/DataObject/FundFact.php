<?php 

/** 
* Generated at: 2020-01-22T13:21:17+07:00
* Inheritance: no
* Variants: no
* Changed by: avristadm (2)
* IP: 172.17.0.1


Fields Summary: 
- file [href]
*/ 

namespace Pimcore\Model\DataObject;



/**
* @method static \Pimcore\Model\DataObject\FundFact\Listing getByFile ($value, $limit = 0) 
*/

class FundFact extends Concrete implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {



use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $o_classId = "68";
protected $o_className = "FundFact";
protected $file;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\FundFact
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get file - file
* @return \Pimcore\Model\Asset\document
*/
public function getFile () {
	$preValue = $this->preGetValue("file"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("file")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set file - file
* @param \Pimcore\Model\Asset\document $file
* @return \Pimcore\Model\DataObject\FundFact
*/
public function setFile ($file) {
	$fd = $this->getClass()->getFieldDefinition("file");
	$currentData = $this->getFile();
	$isEqual = $fd->isEqual($currentData, $file);
	if (!$isEqual) {
		$this->markFieldDirty("file", true);
	}
	$this->file = $fd->preSetData($this, $file);
	return $this;
}

protected static $_relationFields = array (
  'file' => 
  array (
    'type' => 'href',
  ),
);

protected $lazyLoadedFields = array (
  0 => 'file',
);

}

