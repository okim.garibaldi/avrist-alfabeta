<?php 

/** 
* Generated at: 2020-01-31T11:45:46+07:00
* Inheritance: no
* Variants: no
* Changed by: avristadm (2)
* IP: 172.17.0.1


Fields Summary: 
- siteId [select]
- title1 [input]
- description1 [textarea]
- title2 [input]
- description2 [textarea]
- contactUs1 [input]
- contactUs1Description [textarea]
- contactUsPage [href]
- image1 [image]
- image2 [image]
- title3 [input]
- description3 [textarea]
- contactUs2 [input]
- contactUs2Description [textarea]
- block [block]
-- year [input]
-- subblock [block]
--- page [href]
*/ 

namespace Pimcore\Model\DataObject;



/**
* @method static \Pimcore\Model\DataObject\PartnershipPage\Listing getBySiteId ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\PartnershipPage\Listing getByTitle1 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\PartnershipPage\Listing getByDescription1 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\PartnershipPage\Listing getByTitle2 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\PartnershipPage\Listing getByDescription2 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\PartnershipPage\Listing getByContactUs1 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\PartnershipPage\Listing getByContactUs1Description ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\PartnershipPage\Listing getByContactUsPage ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\PartnershipPage\Listing getByImage1 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\PartnershipPage\Listing getByImage2 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\PartnershipPage\Listing getByTitle3 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\PartnershipPage\Listing getByDescription3 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\PartnershipPage\Listing getByContactUs2 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\PartnershipPage\Listing getByContactUs2Description ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\PartnershipPage\Listing getByBlock ($value, $limit = 0) 
*/

class PartnershipPage extends Concrete implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {



use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $o_classId = "45";
protected $o_className = "PartnershipPage";
protected $siteId;
protected $title1;
protected $description1;
protected $title2;
protected $description2;
protected $contactUs1;
protected $contactUs1Description;
protected $contactUsPage;
protected $image1;
protected $image2;
protected $title3;
protected $description3;
protected $contactUs2;
protected $contactUs2Description;
protected $block;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\PartnershipPage
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get siteId - Site
* @return string
*/
public function getSiteId () {
	$preValue = $this->preGetValue("siteId"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->siteId;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set siteId - Site
* @param string $siteId
* @return \Pimcore\Model\DataObject\PartnershipPage
*/
public function setSiteId ($siteId) {
	$fd = $this->getClass()->getFieldDefinition("siteId");
	$this->siteId = $siteId;
	return $this;
}

/**
* Get title1 - Title 1
* @return string
*/
public function getTitle1 () {
	$preValue = $this->preGetValue("title1"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->title1;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set title1 - Title 1
* @param string $title1
* @return \Pimcore\Model\DataObject\PartnershipPage
*/
public function setTitle1 ($title1) {
	$fd = $this->getClass()->getFieldDefinition("title1");
	$this->title1 = $title1;
	return $this;
}

/**
* Get description1 - Description 1
* @return string
*/
public function getDescription1 () {
	$preValue = $this->preGetValue("description1"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->description1;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set description1 - Description 1
* @param string $description1
* @return \Pimcore\Model\DataObject\PartnershipPage
*/
public function setDescription1 ($description1) {
	$fd = $this->getClass()->getFieldDefinition("description1");
	$this->description1 = $description1;
	return $this;
}

/**
* Get title2 - Title 2
* @return string
*/
public function getTitle2 () {
	$preValue = $this->preGetValue("title2"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->title2;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set title2 - Title 2
* @param string $title2
* @return \Pimcore\Model\DataObject\PartnershipPage
*/
public function setTitle2 ($title2) {
	$fd = $this->getClass()->getFieldDefinition("title2");
	$this->title2 = $title2;
	return $this;
}

/**
* Get description2 - Description 2
* @return string
*/
public function getDescription2 () {
	$preValue = $this->preGetValue("description2"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->description2;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set description2 - Description 2
* @param string $description2
* @return \Pimcore\Model\DataObject\PartnershipPage
*/
public function setDescription2 ($description2) {
	$fd = $this->getClass()->getFieldDefinition("description2");
	$this->description2 = $description2;
	return $this;
}

/**
* Get contactUs1 - Contact Us 1
* @return string
*/
public function getContactUs1 () {
	$preValue = $this->preGetValue("contactUs1"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->contactUs1;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set contactUs1 - Contact Us 1
* @param string $contactUs1
* @return \Pimcore\Model\DataObject\PartnershipPage
*/
public function setContactUs1 ($contactUs1) {
	$fd = $this->getClass()->getFieldDefinition("contactUs1");
	$this->contactUs1 = $contactUs1;
	return $this;
}

/**
* Get contactUs1Description - Contact Us 1 Description 
* @return string
*/
public function getContactUs1Description () {
	$preValue = $this->preGetValue("contactUs1Description"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->contactUs1Description;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set contactUs1Description - Contact Us 1 Description 
* @param string $contactUs1Description
* @return \Pimcore\Model\DataObject\PartnershipPage
*/
public function setContactUs1Description ($contactUs1Description) {
	$fd = $this->getClass()->getFieldDefinition("contactUs1Description");
	$this->contactUs1Description = $contactUs1Description;
	return $this;
}

/**
* Get contactUsPage - Contact Us Page
* @return \Pimcore\Model\Document\page
*/
public function getContactUsPage () {
	$preValue = $this->preGetValue("contactUsPage"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("contactUsPage")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set contactUsPage - Contact Us Page
* @param \Pimcore\Model\Document\page $contactUsPage
* @return \Pimcore\Model\DataObject\PartnershipPage
*/
public function setContactUsPage ($contactUsPage) {
	$fd = $this->getClass()->getFieldDefinition("contactUsPage");
	$currentData = $this->getContactUsPage();
	$isEqual = $fd->isEqual($currentData, $contactUsPage);
	if (!$isEqual) {
		$this->markFieldDirty("contactUsPage", true);
	}
	$this->contactUsPage = $fd->preSetData($this, $contactUsPage);
	return $this;
}

/**
* Get image1 - Image 1
* @return \Pimcore\Model\Asset\Image
*/
public function getImage1 () {
	$preValue = $this->preGetValue("image1"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->image1;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set image1 - Image 1
* @param \Pimcore\Model\Asset\Image $image1
* @return \Pimcore\Model\DataObject\PartnershipPage
*/
public function setImage1 ($image1) {
	$fd = $this->getClass()->getFieldDefinition("image1");
	$this->image1 = $image1;
	return $this;
}

/**
* Get image2 - Image 2
* @return \Pimcore\Model\Asset\Image
*/
public function getImage2 () {
	$preValue = $this->preGetValue("image2"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->image2;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set image2 - Image 2
* @param \Pimcore\Model\Asset\Image $image2
* @return \Pimcore\Model\DataObject\PartnershipPage
*/
public function setImage2 ($image2) {
	$fd = $this->getClass()->getFieldDefinition("image2");
	$this->image2 = $image2;
	return $this;
}

/**
* Get title3 - Title 3
* @return string
*/
public function getTitle3 () {
	$preValue = $this->preGetValue("title3"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->title3;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set title3 - Title 3
* @param string $title3
* @return \Pimcore\Model\DataObject\PartnershipPage
*/
public function setTitle3 ($title3) {
	$fd = $this->getClass()->getFieldDefinition("title3");
	$this->title3 = $title3;
	return $this;
}

/**
* Get description3 - Description 3
* @return string
*/
public function getDescription3 () {
	$preValue = $this->preGetValue("description3"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->description3;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set description3 - Description 3
* @param string $description3
* @return \Pimcore\Model\DataObject\PartnershipPage
*/
public function setDescription3 ($description3) {
	$fd = $this->getClass()->getFieldDefinition("description3");
	$this->description3 = $description3;
	return $this;
}

/**
* Get contactUs2 - Contact Us 2
* @return string
*/
public function getContactUs2 () {
	$preValue = $this->preGetValue("contactUs2"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->contactUs2;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set contactUs2 - Contact Us 2
* @param string $contactUs2
* @return \Pimcore\Model\DataObject\PartnershipPage
*/
public function setContactUs2 ($contactUs2) {
	$fd = $this->getClass()->getFieldDefinition("contactUs2");
	$this->contactUs2 = $contactUs2;
	return $this;
}

/**
* Get contactUs2Description - Contact Us 2 Description 
* @return string
*/
public function getContactUs2Description () {
	$preValue = $this->preGetValue("contactUs2Description"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->contactUs2Description;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set contactUs2Description - Contact Us 2 Description 
* @param string $contactUs2Description
* @return \Pimcore\Model\DataObject\PartnershipPage
*/
public function setContactUs2Description ($contactUs2Description) {
	$fd = $this->getClass()->getFieldDefinition("contactUs2Description");
	$this->contactUs2Description = $contactUs2Description;
	return $this;
}

/**
* Get block - Block
* @return \Pimcore\Model\DataObject\Data\BlockElement[][]
*/
public function getBlock () {
	$preValue = $this->preGetValue("block"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("block")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set block - Block
* @param \Pimcore\Model\DataObject\Data\BlockElement[][] $block
* @return \Pimcore\Model\DataObject\PartnershipPage
*/
public function setBlock ($block) {
	$fd = $this->getClass()->getFieldDefinition("block");
	$this->block = $fd->preSetData($this, $block);
	return $this;
}

protected static $_relationFields = array (
  'contactUsPage' => 
  array (
    'type' => 'href',
  ),
);

protected $lazyLoadedFields = array (
  0 => 'contactUsPage',
);

}

