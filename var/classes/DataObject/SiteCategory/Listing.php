<?php 

namespace Pimcore\Model\DataObject\SiteCategory;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\SiteCategory current()
 * @method DataObject\SiteCategory[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "29";
protected $className = "SiteCategory";


}
