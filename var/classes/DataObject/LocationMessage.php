<?php 

/** 
* Generated at: 2020-02-25T18:57:15+07:00
* Inheritance: no
* Variants: no
* Changed by: avristadm (2)
* IP: 172.17.0.1


Fields Summary: 
- name [input]
- email [input]
- phoneNumber [input]
- topic [select]
- message [textarea]
*/ 

namespace Pimcore\Model\DataObject;



/**
* @method static \Pimcore\Model\DataObject\LocationMessage\Listing getByName ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\LocationMessage\Listing getByEmail ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\LocationMessage\Listing getByPhoneNumber ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\LocationMessage\Listing getByTopic ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\LocationMessage\Listing getByMessage ($value, $limit = 0) 
*/

class LocationMessage extends Concrete implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {



use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $o_classId = "90";
protected $o_className = "LocationMessage";
protected $name;
protected $email;
protected $phoneNumber;
protected $topic;
protected $message;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\LocationMessage
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get name - name
* @return string
*/
public function getName () {
	$preValue = $this->preGetValue("name"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->name;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set name - name
* @param string $name
* @return \Pimcore\Model\DataObject\LocationMessage
*/
public function setName ($name) {
	$fd = $this->getClass()->getFieldDefinition("name");
	$this->name = $name;
	return $this;
}

/**
* Get email - Email
* @return string
*/
public function getEmail () {
	$preValue = $this->preGetValue("email"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->email;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set email - Email
* @param string $email
* @return \Pimcore\Model\DataObject\LocationMessage
*/
public function setEmail ($email) {
	$fd = $this->getClass()->getFieldDefinition("email");
	$this->email = $email;
	return $this;
}

/**
* Get phoneNumber - Phone Number
* @return string
*/
public function getPhoneNumber () {
	$preValue = $this->preGetValue("phoneNumber"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->phoneNumber;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set phoneNumber - Phone Number
* @param string $phoneNumber
* @return \Pimcore\Model\DataObject\LocationMessage
*/
public function setPhoneNumber ($phoneNumber) {
	$fd = $this->getClass()->getFieldDefinition("phoneNumber");
	$this->phoneNumber = $phoneNumber;
	return $this;
}

/**
* Get topic - Topic
* @return string
*/
public function getTopic () {
	$preValue = $this->preGetValue("topic"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->topic;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set topic - Topic
* @param string $topic
* @return \Pimcore\Model\DataObject\LocationMessage
*/
public function setTopic ($topic) {
	$fd = $this->getClass()->getFieldDefinition("topic");
	$this->topic = $topic;
	return $this;
}

/**
* Get message - Message
* @return string
*/
public function getMessage () {
	$preValue = $this->preGetValue("message"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->message;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set message - Message
* @param string $message
* @return \Pimcore\Model\DataObject\LocationMessage
*/
public function setMessage ($message) {
	$fd = $this->getClass()->getFieldDefinition("message");
	$this->message = $message;
	return $this;
}

protected static $_relationFields = array (
);

protected $lazyLoadedFields = array (
);

}

