<?php 

/** 
* Generated at: 2020-02-25T23:02:52+07:00
* Inheritance: no
* Variants: no
* Changed by: avristadm (2)
* IP: 172.17.0.1


Fields Summary: 
- image [image]
- name [input]
- address [textarea]
- filterBengkel [select]
- groupsFilter [select]
- area [select]
- hospitalCategory [select]
- cabang [select]
- coordinate [geopoint]
- phone [input]
- fax [input]
- link [input]
- region [input]
- email [input]
*/ 

namespace Pimcore\Model\DataObject;



/**
* @method static \Pimcore\Model\DataObject\Location\Listing getByImage ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Location\Listing getByName ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Location\Listing getByAddress ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Location\Listing getByFilterBengkel ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Location\Listing getByGroupsFilter ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Location\Listing getByArea ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Location\Listing getByHospitalCategory ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Location\Listing getByCabang ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Location\Listing getByCoordinate ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Location\Listing getByPhone ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Location\Listing getByFax ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Location\Listing getByLink ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Location\Listing getByRegion ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Location\Listing getByEmail ($value, $limit = 0) 
*/

class Location extends Concrete implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {



use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $o_classId = "83";
protected $o_className = "Location";
protected $image;
protected $name;
protected $address;
protected $filterBengkel;
protected $groupsFilter;
protected $area;
protected $hospitalCategory;
protected $cabang;
protected $coordinate;
protected $phone;
protected $fax;
protected $link;
protected $region;
protected $email;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\Location
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get image - Image
* @return \Pimcore\Model\Asset\Image
*/
public function getImage () {
	$preValue = $this->preGetValue("image"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->image;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set image - Image
* @param \Pimcore\Model\Asset\Image $image
* @return \Pimcore\Model\DataObject\Location
*/
public function setImage ($image) {
	$fd = $this->getClass()->getFieldDefinition("image");
	$this->image = $image;
	return $this;
}

/**
* Get name - name
* @return string
*/
public function getName () {
	$preValue = $this->preGetValue("name"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->name;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set name - name
* @param string $name
* @return \Pimcore\Model\DataObject\Location
*/
public function setName ($name) {
	$fd = $this->getClass()->getFieldDefinition("name");
	$this->name = $name;
	return $this;
}

/**
* Get address - Address
* @return string
*/
public function getAddress () {
	$preValue = $this->preGetValue("address"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->address;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set address - Address
* @param string $address
* @return \Pimcore\Model\DataObject\Location
*/
public function setAddress ($address) {
	$fd = $this->getClass()->getFieldDefinition("address");
	$this->address = $address;
	return $this;
}

/**
* Get filterBengkel - filterBengkel
* @return string
*/
public function getFilterBengkel () {
	$preValue = $this->preGetValue("filterBengkel"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->filterBengkel;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set filterBengkel - filterBengkel
* @param string $filterBengkel
* @return \Pimcore\Model\DataObject\Location
*/
public function setFilterBengkel ($filterBengkel) {
	$fd = $this->getClass()->getFieldDefinition("filterBengkel");
	$this->filterBengkel = $filterBengkel;
	return $this;
}

/**
* Get groupsFilter - Group
* @return string
*/
public function getGroupsFilter () {
	$preValue = $this->preGetValue("groupsFilter"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->groupsFilter;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set groupsFilter - Group
* @param string $groupsFilter
* @return \Pimcore\Model\DataObject\Location
*/
public function setGroupsFilter ($groupsFilter) {
	$fd = $this->getClass()->getFieldDefinition("groupsFilter");
	$this->groupsFilter = $groupsFilter;
	return $this;
}

/**
* Get area - Area
* @return string
*/
public function getArea () {
	$preValue = $this->preGetValue("area"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->area;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set area - Area
* @param string $area
* @return \Pimcore\Model\DataObject\Location
*/
public function setArea ($area) {
	$fd = $this->getClass()->getFieldDefinition("area");
	$this->area = $area;
	return $this;
}

/**
* Get hospitalCategory - Hospital Category
* @return string
*/
public function getHospitalCategory () {
	$preValue = $this->preGetValue("hospitalCategory"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->hospitalCategory;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set hospitalCategory - Hospital Category
* @param string $hospitalCategory
* @return \Pimcore\Model\DataObject\Location
*/
public function setHospitalCategory ($hospitalCategory) {
	$fd = $this->getClass()->getFieldDefinition("hospitalCategory");
	$this->hospitalCategory = $hospitalCategory;
	return $this;
}

/**
* Get cabang - Cabang
* @return string
*/
public function getCabang () {
	$preValue = $this->preGetValue("cabang"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->cabang;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set cabang - Cabang
* @param string $cabang
* @return \Pimcore\Model\DataObject\Location
*/
public function setCabang ($cabang) {
	$fd = $this->getClass()->getFieldDefinition("cabang");
	$this->cabang = $cabang;
	return $this;
}

/**
* Get coordinate - coordinate
* @return \Pimcore\Model\DataObject\Data\Geopoint
*/
public function getCoordinate () {
	$preValue = $this->preGetValue("coordinate"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->coordinate;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set coordinate - coordinate
* @param \Pimcore\Model\DataObject\Data\Geopoint $coordinate
* @return \Pimcore\Model\DataObject\Location
*/
public function setCoordinate ($coordinate) {
	$fd = $this->getClass()->getFieldDefinition("coordinate");
	$this->coordinate = $coordinate;
	return $this;
}

/**
* Get phone - Phone
* @return string
*/
public function getPhone () {
	$preValue = $this->preGetValue("phone"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->phone;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set phone - Phone
* @param string $phone
* @return \Pimcore\Model\DataObject\Location
*/
public function setPhone ($phone) {
	$fd = $this->getClass()->getFieldDefinition("phone");
	$this->phone = $phone;
	return $this;
}

/**
* Get fax - Fax
* @return string
*/
public function getFax () {
	$preValue = $this->preGetValue("fax"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->fax;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set fax - Fax
* @param string $fax
* @return \Pimcore\Model\DataObject\Location
*/
public function setFax ($fax) {
	$fd = $this->getClass()->getFieldDefinition("fax");
	$this->fax = $fax;
	return $this;
}

/**
* Get link - Link
* @return string
*/
public function getLink () {
	$preValue = $this->preGetValue("link"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->link;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set link - Link
* @param string $link
* @return \Pimcore\Model\DataObject\Location
*/
public function setLink ($link) {
	$fd = $this->getClass()->getFieldDefinition("link");
	$this->link = $link;
	return $this;
}

/**
* Get region - region
* @return string
*/
public function getRegion () {
	$preValue = $this->preGetValue("region"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->region;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set region - region
* @param string $region
* @return \Pimcore\Model\DataObject\Location
*/
public function setRegion ($region) {
	$fd = $this->getClass()->getFieldDefinition("region");
	$this->region = $region;
	return $this;
}

/**
* Get email - email
* @return string
*/
public function getEmail () {
	$preValue = $this->preGetValue("email"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->email;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set email - email
* @param string $email
* @return \Pimcore\Model\DataObject\Location
*/
public function setEmail ($email) {
	$fd = $this->getClass()->getFieldDefinition("email");
	$this->email = $email;
	return $this;
}

protected static $_relationFields = array (
);

protected $lazyLoadedFields = array (
);

}

