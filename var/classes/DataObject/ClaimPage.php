<?php 

/** 
* Generated at: 2020-02-14T07:49:35+07:00
* Inheritance: no
* Variants: no
* Changed by: avristadm (2)
* IP: 172.17.0.1


Fields Summary: 
- siteId [select]
- name [input]
- block [block]
-- name [input]
-- file [href]
- contactUs [input]
- contactUsDescription [textarea]
- contactUsPage [href]
*/ 

namespace Pimcore\Model\DataObject;



/**
* @method static \Pimcore\Model\DataObject\ClaimPage\Listing getBySiteId ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\ClaimPage\Listing getByName ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\ClaimPage\Listing getByBlock ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\ClaimPage\Listing getByContactUs ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\ClaimPage\Listing getByContactUsDescription ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\ClaimPage\Listing getByContactUsPage ($value, $limit = 0) 
*/

class ClaimPage extends Concrete implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {



use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $o_classId = "82";
protected $o_className = "ClaimPage";
protected $siteId;
protected $name;
protected $block;
protected $contactUs;
protected $contactUsDescription;
protected $contactUsPage;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\ClaimPage
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get siteId - Site
* @return string
*/
public function getSiteId () {
	$preValue = $this->preGetValue("siteId"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->siteId;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set siteId - Site
* @param string $siteId
* @return \Pimcore\Model\DataObject\ClaimPage
*/
public function setSiteId ($siteId) {
	$fd = $this->getClass()->getFieldDefinition("siteId");
	$this->siteId = $siteId;
	return $this;
}

/**
* Get name - name
* @return string
*/
public function getName () {
	$preValue = $this->preGetValue("name"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->name;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set name - name
* @param string $name
* @return \Pimcore\Model\DataObject\ClaimPage
*/
public function setName ($name) {
	$fd = $this->getClass()->getFieldDefinition("name");
	$this->name = $name;
	return $this;
}

/**
* Get block - Block
* @return \Pimcore\Model\DataObject\Data\BlockElement[][]
*/
public function getBlock () {
	$preValue = $this->preGetValue("block"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("block")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set block - Block
* @param \Pimcore\Model\DataObject\Data\BlockElement[][] $block
* @return \Pimcore\Model\DataObject\ClaimPage
*/
public function setBlock ($block) {
	$fd = $this->getClass()->getFieldDefinition("block");
	$this->block = $fd->preSetData($this, $block);
	return $this;
}

/**
* Get contactUs - Contact Us
* @return string
*/
public function getContactUs () {
	$preValue = $this->preGetValue("contactUs"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->contactUs;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set contactUs - Contact Us
* @param string $contactUs
* @return \Pimcore\Model\DataObject\ClaimPage
*/
public function setContactUs ($contactUs) {
	$fd = $this->getClass()->getFieldDefinition("contactUs");
	$this->contactUs = $contactUs;
	return $this;
}

/**
* Get contactUsDescription - contact Us Description
* @return string
*/
public function getContactUsDescription () {
	$preValue = $this->preGetValue("contactUsDescription"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->contactUsDescription;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set contactUsDescription - contact Us Description
* @param string $contactUsDescription
* @return \Pimcore\Model\DataObject\ClaimPage
*/
public function setContactUsDescription ($contactUsDescription) {
	$fd = $this->getClass()->getFieldDefinition("contactUsDescription");
	$this->contactUsDescription = $contactUsDescription;
	return $this;
}

/**
* Get contactUsPage - Contact Us Page
* @return \Pimcore\Model\Document\page
*/
public function getContactUsPage () {
	$preValue = $this->preGetValue("contactUsPage"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("contactUsPage")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set contactUsPage - Contact Us Page
* @param \Pimcore\Model\Document\page $contactUsPage
* @return \Pimcore\Model\DataObject\ClaimPage
*/
public function setContactUsPage ($contactUsPage) {
	$fd = $this->getClass()->getFieldDefinition("contactUsPage");
	$currentData = $this->getContactUsPage();
	$isEqual = $fd->isEqual($currentData, $contactUsPage);
	if (!$isEqual) {
		$this->markFieldDirty("contactUsPage", true);
	}
	$this->contactUsPage = $fd->preSetData($this, $contactUsPage);
	return $this;
}

protected static $_relationFields = array (
  'contactUsPage' => 
  array (
    'type' => 'href',
  ),
);

protected $lazyLoadedFields = array (
  0 => 'contactUsPage',
);

}

