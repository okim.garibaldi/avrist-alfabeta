<?php 

namespace Pimcore\Model\DataObject\Faq2;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\Faq2 current()
 * @method DataObject\Faq2[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "18";
protected $className = "Faq2";


}
