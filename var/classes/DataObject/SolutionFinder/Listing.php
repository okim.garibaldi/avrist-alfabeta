<?php 

namespace Pimcore\Model\DataObject\SolutionFinder;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\SolutionFinder current()
 * @method DataObject\SolutionFinder[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "23";
protected $className = "SolutionFinder";


}
