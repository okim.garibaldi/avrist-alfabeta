<?php 

namespace Pimcore\Model\DataObject\ProspectusPage;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\ProspectusPage current()
 * @method DataObject\ProspectusPage[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "77";
protected $className = "ProspectusPage";


}
