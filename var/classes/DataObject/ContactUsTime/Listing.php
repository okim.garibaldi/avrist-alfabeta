<?php 

namespace Pimcore\Model\DataObject\ContactUsTime;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\ContactUsTime current()
 * @method DataObject\ContactUsTime[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "5";
protected $className = "ContactUsTime";


}
