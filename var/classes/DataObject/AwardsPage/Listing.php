<?php 

namespace Pimcore\Model\DataObject\AwardsPage;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\AwardsPage current()
 * @method DataObject\AwardsPage[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "40";
protected $className = "AwardsPage";


}
