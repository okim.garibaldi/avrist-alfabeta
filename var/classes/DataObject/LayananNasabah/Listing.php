<?php 

namespace Pimcore\Model\DataObject\LayananNasabah;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\LayananNasabah current()
 * @method DataObject\LayananNasabah[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "75";
protected $className = "LayananNasabah";


}
