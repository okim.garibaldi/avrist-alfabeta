<?php 

namespace Pimcore\Model\DataObject\CsrType;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\CsrType current()
 * @method DataObject\CsrType[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "27";
protected $className = "CsrType";


}
