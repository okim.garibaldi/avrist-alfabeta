<?php 

namespace Pimcore\Model\DataObject\PartnershipDetails;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\PartnershipDetails current()
 * @method DataObject\PartnershipDetails[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "62";
protected $className = "PartnershipDetails";


}
