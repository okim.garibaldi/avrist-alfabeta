<?php 

namespace Pimcore\Model\DataObject\AsuransiQuestion;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\AsuransiQuestion current()
 * @method DataObject\AsuransiQuestion[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "25";
protected $className = "AsuransiQuestion";


}
