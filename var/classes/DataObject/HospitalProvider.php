<?php 

/** 
* Generated at: 2019-06-20T11:39:40+07:00
* Inheritance: no
* Variants: no
* Changed by: avristadm (2)
* IP: 202.87.248.23


Fields Summary: 
- name [input]
- address [textarea]
- category [select]
- coordinate [geopoint]
- phone [input]
- link [input]
- region [input]
*/ 

namespace Pimcore\Model\DataObject;



/**
* @method static \Pimcore\Model\DataObject\HospitalProvider\Listing getByName ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\HospitalProvider\Listing getByAddress ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\HospitalProvider\Listing getByCategory ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\HospitalProvider\Listing getByCoordinate ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\HospitalProvider\Listing getByPhone ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\HospitalProvider\Listing getByLink ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\HospitalProvider\Listing getByRegion ($value, $limit = 0) 
*/

class HospitalProvider extends Concrete implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {



use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $o_classId = "9";
protected $o_className = "HospitalProvider";
protected $name;
protected $address;
protected $category;
protected $coordinate;
protected $phone;
protected $link;
protected $region;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\HospitalProvider
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get name - name
* @return string
*/
public function getName () {
	$preValue = $this->preGetValue("name"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->name;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set name - name
* @param string $name
* @return \Pimcore\Model\DataObject\HospitalProvider
*/
public function setName ($name) {
	$fd = $this->getClass()->getFieldDefinition("name");
	$this->name = $name;
	return $this;
}

/**
* Get address - Address
* @return string
*/
public function getAddress () {
	$preValue = $this->preGetValue("address"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->address;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set address - Address
* @param string $address
* @return \Pimcore\Model\DataObject\HospitalProvider
*/
public function setAddress ($address) {
	$fd = $this->getClass()->getFieldDefinition("address");
	$this->address = $address;
	return $this;
}

/**
* Get category - category
* @return string
*/
public function getCategory () {
	$preValue = $this->preGetValue("category"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->category;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set category - category
* @param string $category
* @return \Pimcore\Model\DataObject\HospitalProvider
*/
public function setCategory ($category) {
	$fd = $this->getClass()->getFieldDefinition("category");
	$this->category = $category;
	return $this;
}

/**
* Get coordinate - coordinate
* @return \Pimcore\Model\DataObject\Data\Geopoint
*/
public function getCoordinate () {
	$preValue = $this->preGetValue("coordinate"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->coordinate;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set coordinate - coordinate
* @param \Pimcore\Model\DataObject\Data\Geopoint $coordinate
* @return \Pimcore\Model\DataObject\HospitalProvider
*/
public function setCoordinate ($coordinate) {
	$fd = $this->getClass()->getFieldDefinition("coordinate");
	$this->coordinate = $coordinate;
	return $this;
}

/**
* Get phone - Phone
* @return string
*/
public function getPhone () {
	$preValue = $this->preGetValue("phone"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->phone;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set phone - Phone
* @param string $phone
* @return \Pimcore\Model\DataObject\HospitalProvider
*/
public function setPhone ($phone) {
	$fd = $this->getClass()->getFieldDefinition("phone");
	$this->phone = $phone;
	return $this;
}

/**
* Get link - Link
* @return string
*/
public function getLink () {
	$preValue = $this->preGetValue("link"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->link;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set link - Link
* @param string $link
* @return \Pimcore\Model\DataObject\HospitalProvider
*/
public function setLink ($link) {
	$fd = $this->getClass()->getFieldDefinition("link");
	$this->link = $link;
	return $this;
}

/**
* Get region - region
* @return string
*/
public function getRegion () {
	$preValue = $this->preGetValue("region"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->region;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set region - region
* @param string $region
* @return \Pimcore\Model\DataObject\HospitalProvider
*/
public function setRegion ($region) {
	$fd = $this->getClass()->getFieldDefinition("region");
	$this->region = $region;
	return $this;
}

protected static $_relationFields = array (
);

protected $lazyLoadedFields = array (
);

}

