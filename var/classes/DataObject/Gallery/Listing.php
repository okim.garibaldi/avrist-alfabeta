<?php 

namespace Pimcore\Model\DataObject\Gallery;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\Gallery current()
 * @method DataObject\Gallery[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "36";
protected $className = "Gallery";


}
