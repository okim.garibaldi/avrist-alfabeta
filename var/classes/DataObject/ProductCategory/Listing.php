<?php 

namespace Pimcore\Model\DataObject\ProductCategory;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\ProductCategory current()
 * @method DataObject\ProductCategory[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "22";
protected $className = "ProductCategory";


}
