<?php 

namespace Pimcore\Model\DataObject\UnitSyariahPage;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\UnitSyariahPage current()
 * @method DataObject\UnitSyariahPage[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "86";
protected $className = "UnitSyariahPage";


}
