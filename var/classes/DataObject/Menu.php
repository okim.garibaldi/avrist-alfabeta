<?php 

/** 
* Generated at: 2019-11-27T10:55:27+07:00
* Inheritance: no
* Variants: no
* Changed by: avristadm (2)
* IP: 172.17.0.1


Fields Summary: 
- title [input]
- description [wysiwyg]
- page [input]
- permalink [href]
*/ 

namespace Pimcore\Model\DataObject;



/**
* @method static \Pimcore\Model\DataObject\Menu\Listing getByTitle ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Menu\Listing getByDescription ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Menu\Listing getByPage ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Menu\Listing getByPermalink ($value, $limit = 0) 
*/

class Menu extends Concrete implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {



use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $o_classId = "47";
protected $o_className = "Menu";
protected $title;
protected $description;
protected $page;
protected $permalink;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\Menu
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get title - Title
* @return string
*/
public function getTitle () {
	$preValue = $this->preGetValue("title"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->title;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set title - Title
* @param string $title
* @return \Pimcore\Model\DataObject\Menu
*/
public function setTitle ($title) {
	$fd = $this->getClass()->getFieldDefinition("title");
	$this->title = $title;
	return $this;
}

/**
* Get description - Description
* @return string
*/
public function getDescription () {
	$preValue = $this->preGetValue("description"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("description")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set description - Description
* @param string $description
* @return \Pimcore\Model\DataObject\Menu
*/
public function setDescription ($description) {
	$fd = $this->getClass()->getFieldDefinition("description");
	$this->description = $description;
	return $this;
}

/**
* Get page - Page
* @return string
*/
public function getPage () {
	$preValue = $this->preGetValue("page"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->page;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set page - Page
* @param string $page
* @return \Pimcore\Model\DataObject\Menu
*/
public function setPage ($page) {
	$fd = $this->getClass()->getFieldDefinition("page");
	$this->page = $page;
	return $this;
}

/**
* Get permalink - Permalink
* @return \Pimcore\Model\Document\page
*/
public function getPermalink () {
	$preValue = $this->preGetValue("permalink"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("permalink")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set permalink - Permalink
* @param \Pimcore\Model\Document\page $permalink
* @return \Pimcore\Model\DataObject\Menu
*/
public function setPermalink ($permalink) {
	$fd = $this->getClass()->getFieldDefinition("permalink");
	$currentData = $this->getPermalink();
	$isEqual = $fd->isEqual($currentData, $permalink);
	if (!$isEqual) {
		$this->markFieldDirty("permalink", true);
	}
	$this->permalink = $fd->preSetData($this, $permalink);
	return $this;
}

protected static $_relationFields = array (
  'permalink' => 
  array (
    'type' => 'href',
  ),
);

protected $lazyLoadedFields = array (
  0 => 'permalink',
);

}

