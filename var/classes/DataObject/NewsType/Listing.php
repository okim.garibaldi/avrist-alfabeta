<?php 

namespace Pimcore\Model\DataObject\NewsType;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\NewsType current()
 * @method DataObject\NewsType[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "6";
protected $className = "NewsType";


}
