<?php 

namespace Pimcore\Model\DataObject\AboutCompany;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\AboutCompany current()
 * @method DataObject\AboutCompany[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "53";
protected $className = "AboutCompany";


}
