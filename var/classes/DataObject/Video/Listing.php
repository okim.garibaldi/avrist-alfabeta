<?php 

namespace Pimcore\Model\DataObject\Video;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\Video current()
 * @method DataObject\Video[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "96";
protected $className = "Video";


}
