<?php 

namespace Pimcore\Model\DataObject\MarketUpdate;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\MarketUpdate current()
 * @method DataObject\MarketUpdate[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "79";
protected $className = "MarketUpdate";


}
