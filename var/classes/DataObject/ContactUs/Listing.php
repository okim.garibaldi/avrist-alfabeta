<?php 

namespace Pimcore\Model\DataObject\ContactUs;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\ContactUs current()
 * @method DataObject\ContactUs[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "3";
protected $className = "ContactUs";


}
