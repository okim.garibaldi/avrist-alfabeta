<?php 

/** 
* Generated at: 2019-06-20T11:39:24+07:00
* Inheritance: no
* Variants: no
* Changed by: avristadm (2)
* IP: 202.87.248.23


Fields Summary: 
- LOB [href]
- year [numeric]
- title [input]
- pdf [href]
- pdfTitle [input]
- order [numeric]
- full [checkbox]
*/ 

namespace Pimcore\Model\DataObject;



/**
* @method static \Pimcore\Model\DataObject\AnnualReport\Listing getByLOB ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\AnnualReport\Listing getByYear ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\AnnualReport\Listing getByTitle ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\AnnualReport\Listing getByPdf ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\AnnualReport\Listing getByPdfTitle ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\AnnualReport\Listing getByOrder ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\AnnualReport\Listing getByFull ($value, $limit = 0) 
*/

class AnnualReport extends Concrete implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {



use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $o_classId = "13";
protected $o_className = "AnnualReport";
protected $LOB;
protected $year;
protected $title;
protected $pdf;
protected $pdfTitle;
protected $order;
protected $full;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\AnnualReport
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get LOB - LOB
* @return \Pimcore\Model\DataObject\LOB
*/
public function getLOB () {
	$preValue = $this->preGetValue("LOB"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("LOB")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set LOB - LOB
* @param \Pimcore\Model\DataObject\LOB $LOB
* @return \Pimcore\Model\DataObject\AnnualReport
*/
public function setLOB ($LOB) {
	$fd = $this->getClass()->getFieldDefinition("LOB");
	$currentData = $this->getLOB();
	$isEqual = $fd->isEqual($currentData, $LOB);
	if (!$isEqual) {
		$this->markFieldDirty("LOB", true);
	}
	$this->LOB = $fd->preSetData($this, $LOB);
	return $this;
}

/**
* Get year - Year
* @return int
*/
public function getYear () {
	$preValue = $this->preGetValue("year"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->year;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set year - Year
* @param int $year
* @return \Pimcore\Model\DataObject\AnnualReport
*/
public function setYear ($year) {
	$fd = $this->getClass()->getFieldDefinition("year");
	$this->year = $year;
	return $this;
}

/**
* Get title - Financial Report Title
* @return string
*/
public function getTitle () {
	$preValue = $this->preGetValue("title"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->title;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set title - Financial Report Title
* @param string $title
* @return \Pimcore\Model\DataObject\AnnualReport
*/
public function setTitle ($title) {
	$fd = $this->getClass()->getFieldDefinition("title");
	$this->title = $title;
	return $this;
}

/**
* Get pdf - PDF File
* @return \Pimcore\Model\Asset\document
*/
public function getPdf () {
	$preValue = $this->preGetValue("pdf"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("pdf")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set pdf - PDF File
* @param \Pimcore\Model\Asset\document $pdf
* @return \Pimcore\Model\DataObject\AnnualReport
*/
public function setPdf ($pdf) {
	$fd = $this->getClass()->getFieldDefinition("pdf");
	$currentData = $this->getPdf();
	$isEqual = $fd->isEqual($currentData, $pdf);
	if (!$isEqual) {
		$this->markFieldDirty("pdf", true);
	}
	$this->pdf = $fd->preSetData($this, $pdf);
	return $this;
}

/**
* Get pdfTitle - PDF Title
* @return string
*/
public function getPdfTitle () {
	$preValue = $this->preGetValue("pdfTitle"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->pdfTitle;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set pdfTitle - PDF Title
* @param string $pdfTitle
* @return \Pimcore\Model\DataObject\AnnualReport
*/
public function setPdfTitle ($pdfTitle) {
	$fd = $this->getClass()->getFieldDefinition("pdfTitle");
	$this->pdfTitle = $pdfTitle;
	return $this;
}

/**
* Get order - Order
* @return float
*/
public function getOrder () {
	$preValue = $this->preGetValue("order"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->order;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set order - Order
* @param float $order
* @return \Pimcore\Model\DataObject\AnnualReport
*/
public function setOrder ($order) {
	$fd = $this->getClass()->getFieldDefinition("order");
	$this->order = $order;
	return $this;
}

/**
* Get full - Full Column
* @return boolean
*/
public function getFull () {
	$preValue = $this->preGetValue("full"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->full;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set full - Full Column
* @param boolean $full
* @return \Pimcore\Model\DataObject\AnnualReport
*/
public function setFull ($full) {
	$fd = $this->getClass()->getFieldDefinition("full");
	$this->full = $full;
	return $this;
}

protected static $_relationFields = array (
  'LOB' => 
  array (
    'type' => 'href',
  ),
  'pdf' => 
  array (
    'type' => 'href',
  ),
);

protected $lazyLoadedFields = array (
  0 => 'LOB',
  1 => 'pdf',
);

}

