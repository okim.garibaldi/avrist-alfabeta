<?php 

namespace Pimcore\Model\DataObject\PremiumFactor;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\PremiumFactor current()
 * @method DataObject\PremiumFactor[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "35";
protected $className = "PremiumFactor";


}
