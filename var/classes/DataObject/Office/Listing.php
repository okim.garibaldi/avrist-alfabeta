<?php 

namespace Pimcore\Model\DataObject\Office;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\Office current()
 * @method DataObject\Office[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "16";
protected $className = "Office";


}
