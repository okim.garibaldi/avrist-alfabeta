<?php 

namespace Pimcore\Model\DataObject\LayananNasabahPage;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\LayananNasabahPage current()
 * @method DataObject\LayananNasabahPage[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "74";
protected $className = "LayananNasabahPage";


}
