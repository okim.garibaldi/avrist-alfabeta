<?php 

namespace Pimcore\Model\DataObject\Page;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\Page current()
 * @method DataObject\Page[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "56";
protected $className = "Page";


}
