<?php 

/** 
* Generated at: 2020-01-24T05:02:36+07:00
* Inheritance: no
* Variants: no
* Changed by: avristadm (2)
* IP: 172.17.0.1


Fields Summary: 
- siteId [select]
- title [input]
- description [textarea]
- block [block]
-- name [input]
-- subblock [block]
--- year [input]
--- subsubblock [block]
---- name [input]
---- file [href]
*/ 

namespace Pimcore\Model\DataObject;



/**
* @method static \Pimcore\Model\DataObject\MarketUpdate\Listing getBySiteId ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\MarketUpdate\Listing getByTitle ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\MarketUpdate\Listing getByDescription ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\MarketUpdate\Listing getByBlock ($value, $limit = 0) 
*/

class MarketUpdate extends Concrete implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {



use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $o_classId = "79";
protected $o_className = "MarketUpdate";
protected $siteId;
protected $title;
protected $description;
protected $block;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\MarketUpdate
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get siteId - Site
* @return string
*/
public function getSiteId () {
	$preValue = $this->preGetValue("siteId"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->siteId;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set siteId - Site
* @param string $siteId
* @return \Pimcore\Model\DataObject\MarketUpdate
*/
public function setSiteId ($siteId) {
	$fd = $this->getClass()->getFieldDefinition("siteId");
	$this->siteId = $siteId;
	return $this;
}

/**
* Get title - Title 
* @return string
*/
public function getTitle () {
	$preValue = $this->preGetValue("title"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->title;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set title - Title 
* @param string $title
* @return \Pimcore\Model\DataObject\MarketUpdate
*/
public function setTitle ($title) {
	$fd = $this->getClass()->getFieldDefinition("title");
	$this->title = $title;
	return $this;
}

/**
* Get description - description 
* @return string
*/
public function getDescription () {
	$preValue = $this->preGetValue("description"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->description;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set description - description 
* @param string $description
* @return \Pimcore\Model\DataObject\MarketUpdate
*/
public function setDescription ($description) {
	$fd = $this->getClass()->getFieldDefinition("description");
	$this->description = $description;
	return $this;
}

/**
* Get block - Block 
* @return \Pimcore\Model\DataObject\Data\BlockElement[][]
*/
public function getBlock () {
	$preValue = $this->preGetValue("block"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("block")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set block - Block 
* @param \Pimcore\Model\DataObject\Data\BlockElement[][] $block
* @return \Pimcore\Model\DataObject\MarketUpdate
*/
public function setBlock ($block) {
	$fd = $this->getClass()->getFieldDefinition("block");
	$this->block = $fd->preSetData($this, $block);
	return $this;
}

protected static $_relationFields = array (
);

protected $lazyLoadedFields = array (
);

}

