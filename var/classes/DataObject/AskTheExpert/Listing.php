<?php 

namespace Pimcore\Model\DataObject\AskTheExpert;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\AskTheExpert current()
 * @method DataObject\AskTheExpert[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "34";
protected $className = "AskTheExpert";


}
