<?php 

/** 
* Generated at: 2019-06-27T13:56:48+07:00
* Inheritance: no
* Variants: no
* Changed by: avristadm (2)
* IP: 202.87.248.23


Fields Summary: 
- Description [wysiwyg]
- Image [image]
- Gallery [imageGallery]
*/ 

namespace Pimcore\Model\DataObject;



/**
* @method static \Pimcore\Model\DataObject\Gallery\Listing getByDescription ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Gallery\Listing getByImage ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Gallery\Listing getByGallery ($value, $limit = 0) 
*/

class Gallery extends Concrete implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {



use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $o_classId = "36";
protected $o_className = "Gallery";
protected $Description;
protected $Image;
protected $Gallery;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\Gallery
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get Description - Description
* @return string
*/
public function getDescription () {
	$preValue = $this->preGetValue("Description"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("Description")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set Description - Description
* @param string $Description
* @return \Pimcore\Model\DataObject\Gallery
*/
public function setDescription ($Description) {
	$fd = $this->getClass()->getFieldDefinition("Description");
	$this->Description = $Description;
	return $this;
}

/**
* Get Image - Image
* @return \Pimcore\Model\Asset\Image
*/
public function getImage () {
	$preValue = $this->preGetValue("Image"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->Image;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set Image - Image
* @param \Pimcore\Model\Asset\Image $Image
* @return \Pimcore\Model\DataObject\Gallery
*/
public function setImage ($Image) {
	$fd = $this->getClass()->getFieldDefinition("Image");
	$this->Image = $Image;
	return $this;
}

/**
* Get Gallery - Gallery
* @return \Pimcore\Model\DataObject\Data\ImageGallery
*/
public function getGallery () {
	$preValue = $this->preGetValue("Gallery"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->Gallery;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set Gallery - Gallery
* @param \Pimcore\Model\DataObject\Data\ImageGallery $Gallery
* @return \Pimcore\Model\DataObject\Gallery
*/
public function setGallery ($Gallery) {
	$fd = $this->getClass()->getFieldDefinition("Gallery");
	$this->Gallery = $Gallery;
	return $this;
}

protected static $_relationFields = array (
);

protected $lazyLoadedFields = array (
);

}

