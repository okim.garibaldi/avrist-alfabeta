<?php 

namespace Pimcore\Model\DataObject\Menu2;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\Menu2 current()
 * @method DataObject\Menu2[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "48";
protected $className = "Menu2";


}
