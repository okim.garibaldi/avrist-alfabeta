<?php 

/** 
* Generated at: 2020-02-05T10:41:37+07:00
* Inheritance: no
* Variants: no
* Changed by: avristadm (2)
* IP: 172.17.0.1


Fields Summary: 
- siteId [select]
- name [input]
- description [textarea]
- link [input]
- title1 [input]
- description1 [textarea]
- title1Page [href]
- image1 [image]
- block1 [block]
-- blockText [input]
-- blockPage [input]
-- blockImage [image]
- block2 [block]
-- blockTitle [input]
-- blockSubtitle [input]
-- blockDescription [textarea]
-- blockImage [image]
- title2 [input]
- block3 [block]
-- blockTitle [input]
-- blockDescription [textarea]
-- blockImage [image]
*/ 

namespace Pimcore\Model\DataObject;



/**
* @method static \Pimcore\Model\DataObject\CareerPage\Listing getBySiteId ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\CareerPage\Listing getByName ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\CareerPage\Listing getByDescription ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\CareerPage\Listing getByLink ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\CareerPage\Listing getByTitle1 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\CareerPage\Listing getByDescription1 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\CareerPage\Listing getByTitle1Page ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\CareerPage\Listing getByImage1 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\CareerPage\Listing getByBlock1 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\CareerPage\Listing getByBlock2 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\CareerPage\Listing getByTitle2 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\CareerPage\Listing getByBlock3 ($value, $limit = 0) 
*/

class CareerPage extends Concrete implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {



use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $o_classId = "59";
protected $o_className = "CareerPage";
protected $siteId;
protected $name;
protected $description;
protected $link;
protected $title1;
protected $description1;
protected $title1Page;
protected $image1;
protected $block1;
protected $block2;
protected $title2;
protected $block3;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\CareerPage
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get siteId - Site
* @return string
*/
public function getSiteId () {
	$preValue = $this->preGetValue("siteId"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->siteId;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set siteId - Site
* @param string $siteId
* @return \Pimcore\Model\DataObject\CareerPage
*/
public function setSiteId ($siteId) {
	$fd = $this->getClass()->getFieldDefinition("siteId");
	$this->siteId = $siteId;
	return $this;
}

/**
* Get name - name
* @return string
*/
public function getName () {
	$preValue = $this->preGetValue("name"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->name;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set name - name
* @param string $name
* @return \Pimcore\Model\DataObject\CareerPage
*/
public function setName ($name) {
	$fd = $this->getClass()->getFieldDefinition("name");
	$this->name = $name;
	return $this;
}

/**
* Get description - Description
* @return string
*/
public function getDescription () {
	$preValue = $this->preGetValue("description"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->description;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set description - Description
* @param string $description
* @return \Pimcore\Model\DataObject\CareerPage
*/
public function setDescription ($description) {
	$fd = $this->getClass()->getFieldDefinition("description");
	$this->description = $description;
	return $this;
}

/**
* Get link - Link
* @return string
*/
public function getLink () {
	$preValue = $this->preGetValue("link"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->link;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set link - Link
* @param string $link
* @return \Pimcore\Model\DataObject\CareerPage
*/
public function setLink ($link) {
	$fd = $this->getClass()->getFieldDefinition("link");
	$this->link = $link;
	return $this;
}

/**
* Get title1 - Title 1
* @return string
*/
public function getTitle1 () {
	$preValue = $this->preGetValue("title1"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->title1;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set title1 - Title 1
* @param string $title1
* @return \Pimcore\Model\DataObject\CareerPage
*/
public function setTitle1 ($title1) {
	$fd = $this->getClass()->getFieldDefinition("title1");
	$this->title1 = $title1;
	return $this;
}

/**
* Get description1 - description 1
* @return string
*/
public function getDescription1 () {
	$preValue = $this->preGetValue("description1"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->description1;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set description1 - description 1
* @param string $description1
* @return \Pimcore\Model\DataObject\CareerPage
*/
public function setDescription1 ($description1) {
	$fd = $this->getClass()->getFieldDefinition("description1");
	$this->description1 = $description1;
	return $this;
}

/**
* Get title1Page - Title 1 Page
* @return \Pimcore\Model\Document\page
*/
public function getTitle1Page () {
	$preValue = $this->preGetValue("title1Page"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("title1Page")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set title1Page - Title 1 Page
* @param \Pimcore\Model\Document\page $title1Page
* @return \Pimcore\Model\DataObject\CareerPage
*/
public function setTitle1Page ($title1Page) {
	$fd = $this->getClass()->getFieldDefinition("title1Page");
	$currentData = $this->getTitle1Page();
	$isEqual = $fd->isEqual($currentData, $title1Page);
	if (!$isEqual) {
		$this->markFieldDirty("title1Page", true);
	}
	$this->title1Page = $fd->preSetData($this, $title1Page);
	return $this;
}

/**
* Get image1 - Image 1
* @return \Pimcore\Model\Asset\Image
*/
public function getImage1 () {
	$preValue = $this->preGetValue("image1"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->image1;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set image1 - Image 1
* @param \Pimcore\Model\Asset\Image $image1
* @return \Pimcore\Model\DataObject\CareerPage
*/
public function setImage1 ($image1) {
	$fd = $this->getClass()->getFieldDefinition("image1");
	$this->image1 = $image1;
	return $this;
}

/**
* Get block1 - Block 1
* @return \Pimcore\Model\DataObject\Data\BlockElement[][]
*/
public function getBlock1 () {
	$preValue = $this->preGetValue("block1"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("block1")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set block1 - Block 1
* @param \Pimcore\Model\DataObject\Data\BlockElement[][] $block1
* @return \Pimcore\Model\DataObject\CareerPage
*/
public function setBlock1 ($block1) {
	$fd = $this->getClass()->getFieldDefinition("block1");
	$this->block1 = $fd->preSetData($this, $block1);
	return $this;
}

/**
* Get block2 - Block 2
* @return \Pimcore\Model\DataObject\Data\BlockElement[][]
*/
public function getBlock2 () {
	$preValue = $this->preGetValue("block2"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("block2")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set block2 - Block 2
* @param \Pimcore\Model\DataObject\Data\BlockElement[][] $block2
* @return \Pimcore\Model\DataObject\CareerPage
*/
public function setBlock2 ($block2) {
	$fd = $this->getClass()->getFieldDefinition("block2");
	$this->block2 = $fd->preSetData($this, $block2);
	return $this;
}

/**
* Get title2 - Title 2
* @return string
*/
public function getTitle2 () {
	$preValue = $this->preGetValue("title2"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->title2;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set title2 - Title 2
* @param string $title2
* @return \Pimcore\Model\DataObject\CareerPage
*/
public function setTitle2 ($title2) {
	$fd = $this->getClass()->getFieldDefinition("title2");
	$this->title2 = $title2;
	return $this;
}

/**
* Get block3 - Block 3
* @return \Pimcore\Model\DataObject\Data\BlockElement[][]
*/
public function getBlock3 () {
	$preValue = $this->preGetValue("block3"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("block3")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set block3 - Block 3
* @param \Pimcore\Model\DataObject\Data\BlockElement[][] $block3
* @return \Pimcore\Model\DataObject\CareerPage
*/
public function setBlock3 ($block3) {
	$fd = $this->getClass()->getFieldDefinition("block3");
	$this->block3 = $fd->preSetData($this, $block3);
	return $this;
}

protected static $_relationFields = array (
  'title1Page' => 
  array (
    'type' => 'href',
  ),
);

protected $lazyLoadedFields = array (
  0 => 'title1Page',
);

}

