<?php 

namespace Pimcore\Model\DataObject\AskFinancialAdvisorMessage;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\AskFinancialAdvisorMessage current()
 * @method DataObject\AskFinancialAdvisorMessage[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "92";
protected $className = "AskFinancialAdvisorMessage";


}
