<?php 

/** 
* Generated at: 2019-11-28T09:25:21+07:00
* Inheritance: no
* Variants: no
* Changed by: avristadm (2)
* IP: 172.17.0.1


Fields Summary: 
- siteId [select]
- name [input]
- description [wysiwyg]
- link [input]
- permalink [href]
- pageTitle [input]
- pageDescription [wysiwyg]
- contentTitle1 [input]
- text1l [wysiwyg]
- text1r [wysiwyg]
- contentTitle2 [input]
- text1 [wysiwyg]
- contentTitle3 [input]
- contentTitle4 [input]
*/ 

namespace Pimcore\Model\DataObject;



/**
* @method static \Pimcore\Model\DataObject\Pelajari\Listing getBySiteId ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Pelajari\Listing getByName ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Pelajari\Listing getByDescription ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Pelajari\Listing getByLink ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Pelajari\Listing getByPermalink ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Pelajari\Listing getByPageTitle ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Pelajari\Listing getByPageDescription ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Pelajari\Listing getByContentTitle1 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Pelajari\Listing getByText1l ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Pelajari\Listing getByText1r ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Pelajari\Listing getByContentTitle2 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Pelajari\Listing getByText1 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Pelajari\Listing getByContentTitle3 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Pelajari\Listing getByContentTitle4 ($value, $limit = 0) 
*/

class Pelajari extends Concrete implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {



use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $o_classId = "50";
protected $o_className = "Pelajari";
protected $siteId;
protected $name;
protected $description;
protected $link;
protected $permalink;
protected $pageTitle;
protected $pageDescription;
protected $contentTitle1;
protected $text1l;
protected $text1r;
protected $contentTitle2;
protected $text1;
protected $contentTitle3;
protected $contentTitle4;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\Pelajari
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get siteId - Site
* @return string
*/
public function getSiteId () {
	$preValue = $this->preGetValue("siteId"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->siteId;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set siteId - Site
* @param string $siteId
* @return \Pimcore\Model\DataObject\Pelajari
*/
public function setSiteId ($siteId) {
	$fd = $this->getClass()->getFieldDefinition("siteId");
	$this->siteId = $siteId;
	return $this;
}

/**
* Get name - Name
* @return string
*/
public function getName () {
	$preValue = $this->preGetValue("name"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->name;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set name - Name
* @param string $name
* @return \Pimcore\Model\DataObject\Pelajari
*/
public function setName ($name) {
	$fd = $this->getClass()->getFieldDefinition("name");
	$this->name = $name;
	return $this;
}

/**
* Get description - Description
* @return string
*/
public function getDescription () {
	$preValue = $this->preGetValue("description"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("description")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set description - Description
* @param string $description
* @return \Pimcore\Model\DataObject\Pelajari
*/
public function setDescription ($description) {
	$fd = $this->getClass()->getFieldDefinition("description");
	$this->description = $description;
	return $this;
}

/**
* Get link - Link
* @return string
*/
public function getLink () {
	$preValue = $this->preGetValue("link"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->link;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set link - Link
* @param string $link
* @return \Pimcore\Model\DataObject\Pelajari
*/
public function setLink ($link) {
	$fd = $this->getClass()->getFieldDefinition("link");
	$this->link = $link;
	return $this;
}

/**
* Get permalink - Permalink
* @return \Pimcore\Model\Document\page
*/
public function getPermalink () {
	$preValue = $this->preGetValue("permalink"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("permalink")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set permalink - Permalink
* @param \Pimcore\Model\Document\page $permalink
* @return \Pimcore\Model\DataObject\Pelajari
*/
public function setPermalink ($permalink) {
	$fd = $this->getClass()->getFieldDefinition("permalink");
	$currentData = $this->getPermalink();
	$isEqual = $fd->isEqual($currentData, $permalink);
	if (!$isEqual) {
		$this->markFieldDirty("permalink", true);
	}
	$this->permalink = $fd->preSetData($this, $permalink);
	return $this;
}

/**
* Get pageTitle - Page Title
* @return string
*/
public function getPageTitle () {
	$preValue = $this->preGetValue("pageTitle"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->pageTitle;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set pageTitle - Page Title
* @param string $pageTitle
* @return \Pimcore\Model\DataObject\Pelajari
*/
public function setPageTitle ($pageTitle) {
	$fd = $this->getClass()->getFieldDefinition("pageTitle");
	$this->pageTitle = $pageTitle;
	return $this;
}

/**
* Get pageDescription - Page Description
* @return string
*/
public function getPageDescription () {
	$preValue = $this->preGetValue("pageDescription"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("pageDescription")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set pageDescription - Page Description
* @param string $pageDescription
* @return \Pimcore\Model\DataObject\Pelajari
*/
public function setPageDescription ($pageDescription) {
	$fd = $this->getClass()->getFieldDefinition("pageDescription");
	$this->pageDescription = $pageDescription;
	return $this;
}

/**
* Get contentTitle1 - Content Title1
* @return string
*/
public function getContentTitle1 () {
	$preValue = $this->preGetValue("contentTitle1"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->contentTitle1;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set contentTitle1 - Content Title1
* @param string $contentTitle1
* @return \Pimcore\Model\DataObject\Pelajari
*/
public function setContentTitle1 ($contentTitle1) {
	$fd = $this->getClass()->getFieldDefinition("contentTitle1");
	$this->contentTitle1 = $contentTitle1;
	return $this;
}

/**
* Get text1l - Text 1 (Left)
* @return string
*/
public function getText1l () {
	$preValue = $this->preGetValue("text1l"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("text1l")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set text1l - Text 1 (Left)
* @param string $text1l
* @return \Pimcore\Model\DataObject\Pelajari
*/
public function setText1l ($text1l) {
	$fd = $this->getClass()->getFieldDefinition("text1l");
	$this->text1l = $text1l;
	return $this;
}

/**
* Get text1r - Text 1 (Right)
* @return string
*/
public function getText1r () {
	$preValue = $this->preGetValue("text1r"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("text1r")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set text1r - Text 1 (Right)
* @param string $text1r
* @return \Pimcore\Model\DataObject\Pelajari
*/
public function setText1r ($text1r) {
	$fd = $this->getClass()->getFieldDefinition("text1r");
	$this->text1r = $text1r;
	return $this;
}

/**
* Get contentTitle2 - Content Title 2
* @return string
*/
public function getContentTitle2 () {
	$preValue = $this->preGetValue("contentTitle2"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->contentTitle2;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set contentTitle2 - Content Title 2
* @param string $contentTitle2
* @return \Pimcore\Model\DataObject\Pelajari
*/
public function setContentTitle2 ($contentTitle2) {
	$fd = $this->getClass()->getFieldDefinition("contentTitle2");
	$this->contentTitle2 = $contentTitle2;
	return $this;
}

/**
* Get text1 - text1
* @return string
*/
public function getText1 () {
	$preValue = $this->preGetValue("text1"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("text1")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set text1 - text1
* @param string $text1
* @return \Pimcore\Model\DataObject\Pelajari
*/
public function setText1 ($text1) {
	$fd = $this->getClass()->getFieldDefinition("text1");
	$this->text1 = $text1;
	return $this;
}

/**
* Get contentTitle3 - Content Title 3
* @return string
*/
public function getContentTitle3 () {
	$preValue = $this->preGetValue("contentTitle3"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->contentTitle3;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set contentTitle3 - Content Title 3
* @param string $contentTitle3
* @return \Pimcore\Model\DataObject\Pelajari
*/
public function setContentTitle3 ($contentTitle3) {
	$fd = $this->getClass()->getFieldDefinition("contentTitle3");
	$this->contentTitle3 = $contentTitle3;
	return $this;
}

/**
* Get contentTitle4 - Content Title 4
* @return string
*/
public function getContentTitle4 () {
	$preValue = $this->preGetValue("contentTitle4"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->contentTitle4;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set contentTitle4 - Content Title 4
* @param string $contentTitle4
* @return \Pimcore\Model\DataObject\Pelajari
*/
public function setContentTitle4 ($contentTitle4) {
	$fd = $this->getClass()->getFieldDefinition("contentTitle4");
	$this->contentTitle4 = $contentTitle4;
	return $this;
}

protected static $_relationFields = array (
  'permalink' => 
  array (
    'type' => 'href',
  ),
);

protected $lazyLoadedFields = array (
  0 => 'permalink',
);

}

