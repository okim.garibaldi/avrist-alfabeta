<?php 

/** 
* Generated at: 2019-11-14T10:53:10+07:00
* Inheritance: no
* Variants: no
* Changed by: avristadm (2)
* IP: 172.17.0.1


Fields Summary: 
- shortDescription [wysiwyg]
- description [wysiwyg]
- page [href]
*/ 

namespace Pimcore\Model\DataObject;



/**
* @method static \Pimcore\Model\DataObject\Generic\Listing getByShortDescription ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Generic\Listing getByDescription ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Generic\Listing getByPage ($value, $limit = 0) 
*/

class Generic extends Concrete implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {



use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $o_classId = "37";
protected $o_className = "Generic";
protected $shortDescription;
protected $description;
protected $page;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\Generic
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get shortDescription - shortDescription
* @return string
*/
public function getShortDescription () {
	$preValue = $this->preGetValue("shortDescription"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("shortDescription")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set shortDescription - shortDescription
* @param string $shortDescription
* @return \Pimcore\Model\DataObject\Generic
*/
public function setShortDescription ($shortDescription) {
	$fd = $this->getClass()->getFieldDefinition("shortDescription");
	$this->shortDescription = $shortDescription;
	return $this;
}

/**
* Get description - description
* @return string
*/
public function getDescription () {
	$preValue = $this->preGetValue("description"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("description")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set description - description
* @param string $description
* @return \Pimcore\Model\DataObject\Generic
*/
public function setDescription ($description) {
	$fd = $this->getClass()->getFieldDefinition("description");
	$this->description = $description;
	return $this;
}

/**
* Get page - Page
* @return \Pimcore\Model\Document\page
*/
public function getPage () {
	$preValue = $this->preGetValue("page"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("page")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set page - Page
* @param \Pimcore\Model\Document\page $page
* @return \Pimcore\Model\DataObject\Generic
*/
public function setPage ($page) {
	$fd = $this->getClass()->getFieldDefinition("page");
	$currentData = $this->getPage();
	$isEqual = $fd->isEqual($currentData, $page);
	if (!$isEqual) {
		$this->markFieldDirty("page", true);
	}
	$this->page = $fd->preSetData($this, $page);
	return $this;
}

protected static $_relationFields = array (
  'page' => 
  array (
    'type' => 'href',
  ),
);

protected $lazyLoadedFields = array (
  0 => 'page',
);

}

