<?php 

/** 
* Generated at: 2020-02-24T18:09:32+07:00
* Inheritance: no
* Variants: no
* Changed by: avristadm (2)
* IP: 172.17.0.1


Fields Summary: 
- siteId [select]
- name [input]
- description [textarea]
- link [input]
- permalink [href]
- pageTitle [input]
- pageDescription [textarea]
- contactUs [input]
- contactUsPage [href]
- contentTitle1 [input]
- text1l [textarea]
- image1l [image]
- text1r [textarea]
- image1r [image]
- contentTitle2 [input]
- text2 [textarea]
- image2 [image]
- contentTitle3 [input]
- block1 [block]
-- number [input]
-- description [textarea]
- block2 [block]
-- contentTitle [input]
-- block [block]
--- year [input]
--- description [textarea]
- contentTitle4 [input]
- card1DisableSwipe [checkbox]
- card1Name [input]
- card1Icon [image]
- block3 [block]
-- card1Image [image]
-- card1Title [input]
-- card1Subtitle [input]
-- card1Description [textarea]
-- card1Block [block]
--- card1DetailedDescription [textarea]
- card2DisableSwipe [checkbox]
- card2Name [input]
- card2Icon [image]
- block4 [block]
-- card2Image [image]
-- card2Title [input]
-- card2Subtitle [input]
-- card2Description [textarea]
-- card2Block [block]
--- card2DetailedDescription [textarea]
- card3DisableSwipe [checkbox]
- card3Name [input]
- card3Icon [image]
- block5 [block]
-- card3Image [image]
-- card3Title [input]
-- card3Subtitle [input]
-- card3Description [textarea]
-- card3Block [block]
--- card3DetailedDescription [textarea]
- card4DisableSwipe [checkbox]
- card4Name [input]
- card4Icon [image]
- block6 [block]
-- card4Image [image]
-- card4Title [input]
-- card4Subtitle [input]
-- card4Description [textarea]
-- card4Block [block]
--- card4DetailedDescription [textarea]
- contentTitle5 [input]
- block7 [block]
-- name [input]
-- image [image]
-- link [input]
- contentTitle6 [input]
*/ 

namespace Pimcore\Model\DataObject;



/**
* @method static \Pimcore\Model\DataObject\AboutCompany\Listing getBySiteId ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\AboutCompany\Listing getByName ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\AboutCompany\Listing getByDescription ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\AboutCompany\Listing getByLink ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\AboutCompany\Listing getByPermalink ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\AboutCompany\Listing getByPageTitle ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\AboutCompany\Listing getByPageDescription ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\AboutCompany\Listing getByContactUs ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\AboutCompany\Listing getByContactUsPage ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\AboutCompany\Listing getByContentTitle1 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\AboutCompany\Listing getByText1l ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\AboutCompany\Listing getByImage1l ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\AboutCompany\Listing getByText1r ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\AboutCompany\Listing getByImage1r ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\AboutCompany\Listing getByContentTitle2 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\AboutCompany\Listing getByText2 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\AboutCompany\Listing getByImage2 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\AboutCompany\Listing getByContentTitle3 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\AboutCompany\Listing getByBlock1 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\AboutCompany\Listing getByBlock2 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\AboutCompany\Listing getByContentTitle4 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\AboutCompany\Listing getByCard1DisableSwipe ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\AboutCompany\Listing getByCard1Name ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\AboutCompany\Listing getByCard1Icon ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\AboutCompany\Listing getByBlock3 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\AboutCompany\Listing getByCard2DisableSwipe ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\AboutCompany\Listing getByCard2Name ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\AboutCompany\Listing getByCard2Icon ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\AboutCompany\Listing getByBlock4 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\AboutCompany\Listing getByCard3DisableSwipe ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\AboutCompany\Listing getByCard3Name ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\AboutCompany\Listing getByCard3Icon ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\AboutCompany\Listing getByBlock5 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\AboutCompany\Listing getByCard4DisableSwipe ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\AboutCompany\Listing getByCard4Name ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\AboutCompany\Listing getByCard4Icon ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\AboutCompany\Listing getByBlock6 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\AboutCompany\Listing getByContentTitle5 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\AboutCompany\Listing getByBlock7 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\AboutCompany\Listing getByContentTitle6 ($value, $limit = 0) 
*/

class AboutCompany extends Concrete implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {



use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $o_classId = "53";
protected $o_className = "AboutCompany";
protected $siteId;
protected $name;
protected $description;
protected $link;
protected $permalink;
protected $pageTitle;
protected $pageDescription;
protected $contactUs;
protected $contactUsPage;
protected $contentTitle1;
protected $text1l;
protected $image1l;
protected $text1r;
protected $image1r;
protected $contentTitle2;
protected $text2;
protected $image2;
protected $contentTitle3;
protected $block1;
protected $block2;
protected $contentTitle4;
protected $card1DisableSwipe;
protected $card1Name;
protected $card1Icon;
protected $block3;
protected $card2DisableSwipe;
protected $card2Name;
protected $card2Icon;
protected $block4;
protected $card3DisableSwipe;
protected $card3Name;
protected $card3Icon;
protected $block5;
protected $card4DisableSwipe;
protected $card4Name;
protected $card4Icon;
protected $block6;
protected $contentTitle5;
protected $block7;
protected $contentTitle6;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\AboutCompany
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get siteId - Site
* @return string
*/
public function getSiteId () {
	$preValue = $this->preGetValue("siteId"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->siteId;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set siteId - Site
* @param string $siteId
* @return \Pimcore\Model\DataObject\AboutCompany
*/
public function setSiteId ($siteId) {
	$fd = $this->getClass()->getFieldDefinition("siteId");
	$this->siteId = $siteId;
	return $this;
}

/**
* Get name - Name
* @return string
*/
public function getName () {
	$preValue = $this->preGetValue("name"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->name;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set name - Name
* @param string $name
* @return \Pimcore\Model\DataObject\AboutCompany
*/
public function setName ($name) {
	$fd = $this->getClass()->getFieldDefinition("name");
	$this->name = $name;
	return $this;
}

/**
* Get description - Description
* @return string
*/
public function getDescription () {
	$preValue = $this->preGetValue("description"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->description;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set description - Description
* @param string $description
* @return \Pimcore\Model\DataObject\AboutCompany
*/
public function setDescription ($description) {
	$fd = $this->getClass()->getFieldDefinition("description");
	$this->description = $description;
	return $this;
}

/**
* Get link - Link
* @return string
*/
public function getLink () {
	$preValue = $this->preGetValue("link"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->link;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set link - Link
* @param string $link
* @return \Pimcore\Model\DataObject\AboutCompany
*/
public function setLink ($link) {
	$fd = $this->getClass()->getFieldDefinition("link");
	$this->link = $link;
	return $this;
}

/**
* Get permalink - Permalink
* @return \Pimcore\Model\Document\page
*/
public function getPermalink () {
	$preValue = $this->preGetValue("permalink"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("permalink")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set permalink - Permalink
* @param \Pimcore\Model\Document\page $permalink
* @return \Pimcore\Model\DataObject\AboutCompany
*/
public function setPermalink ($permalink) {
	$fd = $this->getClass()->getFieldDefinition("permalink");
	$currentData = $this->getPermalink();
	$isEqual = $fd->isEqual($currentData, $permalink);
	if (!$isEqual) {
		$this->markFieldDirty("permalink", true);
	}
	$this->permalink = $fd->preSetData($this, $permalink);
	return $this;
}

/**
* Get pageTitle - Page Title
* @return string
*/
public function getPageTitle () {
	$preValue = $this->preGetValue("pageTitle"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->pageTitle;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set pageTitle - Page Title
* @param string $pageTitle
* @return \Pimcore\Model\DataObject\AboutCompany
*/
public function setPageTitle ($pageTitle) {
	$fd = $this->getClass()->getFieldDefinition("pageTitle");
	$this->pageTitle = $pageTitle;
	return $this;
}

/**
* Get pageDescription - Page Description
* @return string
*/
public function getPageDescription () {
	$preValue = $this->preGetValue("pageDescription"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->pageDescription;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set pageDescription - Page Description
* @param string $pageDescription
* @return \Pimcore\Model\DataObject\AboutCompany
*/
public function setPageDescription ($pageDescription) {
	$fd = $this->getClass()->getFieldDefinition("pageDescription");
	$this->pageDescription = $pageDescription;
	return $this;
}

/**
* Get contactUs - Contact Us
* @return string
*/
public function getContactUs () {
	$preValue = $this->preGetValue("contactUs"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->contactUs;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set contactUs - Contact Us
* @param string $contactUs
* @return \Pimcore\Model\DataObject\AboutCompany
*/
public function setContactUs ($contactUs) {
	$fd = $this->getClass()->getFieldDefinition("contactUs");
	$this->contactUs = $contactUs;
	return $this;
}

/**
* Get contactUsPage - contactUsPage
* @return \Pimcore\Model\Document\page
*/
public function getContactUsPage () {
	$preValue = $this->preGetValue("contactUsPage"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("contactUsPage")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set contactUsPage - contactUsPage
* @param \Pimcore\Model\Document\page $contactUsPage
* @return \Pimcore\Model\DataObject\AboutCompany
*/
public function setContactUsPage ($contactUsPage) {
	$fd = $this->getClass()->getFieldDefinition("contactUsPage");
	$currentData = $this->getContactUsPage();
	$isEqual = $fd->isEqual($currentData, $contactUsPage);
	if (!$isEqual) {
		$this->markFieldDirty("contactUsPage", true);
	}
	$this->contactUsPage = $fd->preSetData($this, $contactUsPage);
	return $this;
}

/**
* Get contentTitle1 - Content Title1
* @return string
*/
public function getContentTitle1 () {
	$preValue = $this->preGetValue("contentTitle1"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->contentTitle1;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set contentTitle1 - Content Title1
* @param string $contentTitle1
* @return \Pimcore\Model\DataObject\AboutCompany
*/
public function setContentTitle1 ($contentTitle1) {
	$fd = $this->getClass()->getFieldDefinition("contentTitle1");
	$this->contentTitle1 = $contentTitle1;
	return $this;
}

/**
* Get text1l - Text 1 (Left)
* @return string
*/
public function getText1l () {
	$preValue = $this->preGetValue("text1l"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->text1l;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set text1l - Text 1 (Left)
* @param string $text1l
* @return \Pimcore\Model\DataObject\AboutCompany
*/
public function setText1l ($text1l) {
	$fd = $this->getClass()->getFieldDefinition("text1l");
	$this->text1l = $text1l;
	return $this;
}

/**
* Get image1l - Image 1 (Left)
* @return \Pimcore\Model\Asset\Image
*/
public function getImage1l () {
	$preValue = $this->preGetValue("image1l"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->image1l;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set image1l - Image 1 (Left)
* @param \Pimcore\Model\Asset\Image $image1l
* @return \Pimcore\Model\DataObject\AboutCompany
*/
public function setImage1l ($image1l) {
	$fd = $this->getClass()->getFieldDefinition("image1l");
	$this->image1l = $image1l;
	return $this;
}

/**
* Get text1r - Text 1 (Right)
* @return string
*/
public function getText1r () {
	$preValue = $this->preGetValue("text1r"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->text1r;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set text1r - Text 1 (Right)
* @param string $text1r
* @return \Pimcore\Model\DataObject\AboutCompany
*/
public function setText1r ($text1r) {
	$fd = $this->getClass()->getFieldDefinition("text1r");
	$this->text1r = $text1r;
	return $this;
}

/**
* Get image1r - Image 1 (Right)
* @return \Pimcore\Model\Asset\Image
*/
public function getImage1r () {
	$preValue = $this->preGetValue("image1r"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->image1r;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set image1r - Image 1 (Right)
* @param \Pimcore\Model\Asset\Image $image1r
* @return \Pimcore\Model\DataObject\AboutCompany
*/
public function setImage1r ($image1r) {
	$fd = $this->getClass()->getFieldDefinition("image1r");
	$this->image1r = $image1r;
	return $this;
}

/**
* Get contentTitle2 - Content Title 2
* @return string
*/
public function getContentTitle2 () {
	$preValue = $this->preGetValue("contentTitle2"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->contentTitle2;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set contentTitle2 - Content Title 2
* @param string $contentTitle2
* @return \Pimcore\Model\DataObject\AboutCompany
*/
public function setContentTitle2 ($contentTitle2) {
	$fd = $this->getClass()->getFieldDefinition("contentTitle2");
	$this->contentTitle2 = $contentTitle2;
	return $this;
}

/**
* Get text2 - text2
* @return string
*/
public function getText2 () {
	$preValue = $this->preGetValue("text2"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->text2;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set text2 - text2
* @param string $text2
* @return \Pimcore\Model\DataObject\AboutCompany
*/
public function setText2 ($text2) {
	$fd = $this->getClass()->getFieldDefinition("text2");
	$this->text2 = $text2;
	return $this;
}

/**
* Get image2 - Image 2
* @return \Pimcore\Model\Asset\Image
*/
public function getImage2 () {
	$preValue = $this->preGetValue("image2"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->image2;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set image2 - Image 2
* @param \Pimcore\Model\Asset\Image $image2
* @return \Pimcore\Model\DataObject\AboutCompany
*/
public function setImage2 ($image2) {
	$fd = $this->getClass()->getFieldDefinition("image2");
	$this->image2 = $image2;
	return $this;
}

/**
* Get contentTitle3 - Content Title 3
* @return string
*/
public function getContentTitle3 () {
	$preValue = $this->preGetValue("contentTitle3"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->contentTitle3;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set contentTitle3 - Content Title 3
* @param string $contentTitle3
* @return \Pimcore\Model\DataObject\AboutCompany
*/
public function setContentTitle3 ($contentTitle3) {
	$fd = $this->getClass()->getFieldDefinition("contentTitle3");
	$this->contentTitle3 = $contentTitle3;
	return $this;
}

/**
* Get block1 - Block 1
* @return \Pimcore\Model\DataObject\Data\BlockElement[][]
*/
public function getBlock1 () {
	$preValue = $this->preGetValue("block1"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("block1")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set block1 - Block 1
* @param \Pimcore\Model\DataObject\Data\BlockElement[][] $block1
* @return \Pimcore\Model\DataObject\AboutCompany
*/
public function setBlock1 ($block1) {
	$fd = $this->getClass()->getFieldDefinition("block1");
	$this->block1 = $fd->preSetData($this, $block1);
	return $this;
}

/**
* Get block2 - Block 2
* @return \Pimcore\Model\DataObject\Data\BlockElement[][]
*/
public function getBlock2 () {
	$preValue = $this->preGetValue("block2"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("block2")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set block2 - Block 2
* @param \Pimcore\Model\DataObject\Data\BlockElement[][] $block2
* @return \Pimcore\Model\DataObject\AboutCompany
*/
public function setBlock2 ($block2) {
	$fd = $this->getClass()->getFieldDefinition("block2");
	$this->block2 = $fd->preSetData($this, $block2);
	return $this;
}

/**
* Get contentTitle4 - Content Title 4
* @return string
*/
public function getContentTitle4 () {
	$preValue = $this->preGetValue("contentTitle4"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->contentTitle4;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set contentTitle4 - Content Title 4
* @param string $contentTitle4
* @return \Pimcore\Model\DataObject\AboutCompany
*/
public function setContentTitle4 ($contentTitle4) {
	$fd = $this->getClass()->getFieldDefinition("contentTitle4");
	$this->contentTitle4 = $contentTitle4;
	return $this;
}

/**
* Get card1DisableSwipe - Card 1 Disable Swipe
* @return boolean
*/
public function getCard1DisableSwipe () {
	$preValue = $this->preGetValue("card1DisableSwipe"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->card1DisableSwipe;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set card1DisableSwipe - Card 1 Disable Swipe
* @param boolean $card1DisableSwipe
* @return \Pimcore\Model\DataObject\AboutCompany
*/
public function setCard1DisableSwipe ($card1DisableSwipe) {
	$fd = $this->getClass()->getFieldDefinition("card1DisableSwipe");
	$this->card1DisableSwipe = $card1DisableSwipe;
	return $this;
}

/**
* Get card1Name - Card 1 Name
* @return string
*/
public function getCard1Name () {
	$preValue = $this->preGetValue("card1Name"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->card1Name;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set card1Name - Card 1 Name
* @param string $card1Name
* @return \Pimcore\Model\DataObject\AboutCompany
*/
public function setCard1Name ($card1Name) {
	$fd = $this->getClass()->getFieldDefinition("card1Name");
	$this->card1Name = $card1Name;
	return $this;
}

/**
* Get card1Icon - Card 1 Icon
* @return \Pimcore\Model\Asset\Image
*/
public function getCard1Icon () {
	$preValue = $this->preGetValue("card1Icon"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->card1Icon;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set card1Icon - Card 1 Icon
* @param \Pimcore\Model\Asset\Image $card1Icon
* @return \Pimcore\Model\DataObject\AboutCompany
*/
public function setCard1Icon ($card1Icon) {
	$fd = $this->getClass()->getFieldDefinition("card1Icon");
	$this->card1Icon = $card1Icon;
	return $this;
}

/**
* Get block3 - Block 3
* @return \Pimcore\Model\DataObject\Data\BlockElement[][]
*/
public function getBlock3 () {
	$preValue = $this->preGetValue("block3"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("block3")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set block3 - Block 3
* @param \Pimcore\Model\DataObject\Data\BlockElement[][] $block3
* @return \Pimcore\Model\DataObject\AboutCompany
*/
public function setBlock3 ($block3) {
	$fd = $this->getClass()->getFieldDefinition("block3");
	$this->block3 = $fd->preSetData($this, $block3);
	return $this;
}

/**
* Get card2DisableSwipe - Card 2 Disable Swipe
* @return boolean
*/
public function getCard2DisableSwipe () {
	$preValue = $this->preGetValue("card2DisableSwipe"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->card2DisableSwipe;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set card2DisableSwipe - Card 2 Disable Swipe
* @param boolean $card2DisableSwipe
* @return \Pimcore\Model\DataObject\AboutCompany
*/
public function setCard2DisableSwipe ($card2DisableSwipe) {
	$fd = $this->getClass()->getFieldDefinition("card2DisableSwipe");
	$this->card2DisableSwipe = $card2DisableSwipe;
	return $this;
}

/**
* Get card2Name - Card 2 Name
* @return string
*/
public function getCard2Name () {
	$preValue = $this->preGetValue("card2Name"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->card2Name;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set card2Name - Card 2 Name
* @param string $card2Name
* @return \Pimcore\Model\DataObject\AboutCompany
*/
public function setCard2Name ($card2Name) {
	$fd = $this->getClass()->getFieldDefinition("card2Name");
	$this->card2Name = $card2Name;
	return $this;
}

/**
* Get card2Icon - Card 2 Icon
* @return \Pimcore\Model\Asset\Image
*/
public function getCard2Icon () {
	$preValue = $this->preGetValue("card2Icon"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->card2Icon;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set card2Icon - Card 2 Icon
* @param \Pimcore\Model\Asset\Image $card2Icon
* @return \Pimcore\Model\DataObject\AboutCompany
*/
public function setCard2Icon ($card2Icon) {
	$fd = $this->getClass()->getFieldDefinition("card2Icon");
	$this->card2Icon = $card2Icon;
	return $this;
}

/**
* Get block4 - Block4
* @return \Pimcore\Model\DataObject\Data\BlockElement[][]
*/
public function getBlock4 () {
	$preValue = $this->preGetValue("block4"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("block4")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set block4 - Block4
* @param \Pimcore\Model\DataObject\Data\BlockElement[][] $block4
* @return \Pimcore\Model\DataObject\AboutCompany
*/
public function setBlock4 ($block4) {
	$fd = $this->getClass()->getFieldDefinition("block4");
	$this->block4 = $fd->preSetData($this, $block4);
	return $this;
}

/**
* Get card3DisableSwipe - Card 3 Disable Swipe
* @return boolean
*/
public function getCard3DisableSwipe () {
	$preValue = $this->preGetValue("card3DisableSwipe"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->card3DisableSwipe;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set card3DisableSwipe - Card 3 Disable Swipe
* @param boolean $card3DisableSwipe
* @return \Pimcore\Model\DataObject\AboutCompany
*/
public function setCard3DisableSwipe ($card3DisableSwipe) {
	$fd = $this->getClass()->getFieldDefinition("card3DisableSwipe");
	$this->card3DisableSwipe = $card3DisableSwipe;
	return $this;
}

/**
* Get card3Name - Card 3 Name
* @return string
*/
public function getCard3Name () {
	$preValue = $this->preGetValue("card3Name"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->card3Name;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set card3Name - Card 3 Name
* @param string $card3Name
* @return \Pimcore\Model\DataObject\AboutCompany
*/
public function setCard3Name ($card3Name) {
	$fd = $this->getClass()->getFieldDefinition("card3Name");
	$this->card3Name = $card3Name;
	return $this;
}

/**
* Get card3Icon - Card 3 Icon
* @return \Pimcore\Model\Asset\Image
*/
public function getCard3Icon () {
	$preValue = $this->preGetValue("card3Icon"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->card3Icon;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set card3Icon - Card 3 Icon
* @param \Pimcore\Model\Asset\Image $card3Icon
* @return \Pimcore\Model\DataObject\AboutCompany
*/
public function setCard3Icon ($card3Icon) {
	$fd = $this->getClass()->getFieldDefinition("card3Icon");
	$this->card3Icon = $card3Icon;
	return $this;
}

/**
* Get block5 - Block5
* @return \Pimcore\Model\DataObject\Data\BlockElement[][]
*/
public function getBlock5 () {
	$preValue = $this->preGetValue("block5"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("block5")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set block5 - Block5
* @param \Pimcore\Model\DataObject\Data\BlockElement[][] $block5
* @return \Pimcore\Model\DataObject\AboutCompany
*/
public function setBlock5 ($block5) {
	$fd = $this->getClass()->getFieldDefinition("block5");
	$this->block5 = $fd->preSetData($this, $block5);
	return $this;
}

/**
* Get card4DisableSwipe - Card 4 Disable Swipe
* @return boolean
*/
public function getCard4DisableSwipe () {
	$preValue = $this->preGetValue("card4DisableSwipe"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->card4DisableSwipe;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set card4DisableSwipe - Card 4 Disable Swipe
* @param boolean $card4DisableSwipe
* @return \Pimcore\Model\DataObject\AboutCompany
*/
public function setCard4DisableSwipe ($card4DisableSwipe) {
	$fd = $this->getClass()->getFieldDefinition("card4DisableSwipe");
	$this->card4DisableSwipe = $card4DisableSwipe;
	return $this;
}

/**
* Get card4Name - Card 4 Name
* @return string
*/
public function getCard4Name () {
	$preValue = $this->preGetValue("card4Name"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->card4Name;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set card4Name - Card 4 Name
* @param string $card4Name
* @return \Pimcore\Model\DataObject\AboutCompany
*/
public function setCard4Name ($card4Name) {
	$fd = $this->getClass()->getFieldDefinition("card4Name");
	$this->card4Name = $card4Name;
	return $this;
}

/**
* Get card4Icon - Card 4 Icon
* @return \Pimcore\Model\Asset\Image
*/
public function getCard4Icon () {
	$preValue = $this->preGetValue("card4Icon"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->card4Icon;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set card4Icon - Card 4 Icon
* @param \Pimcore\Model\Asset\Image $card4Icon
* @return \Pimcore\Model\DataObject\AboutCompany
*/
public function setCard4Icon ($card4Icon) {
	$fd = $this->getClass()->getFieldDefinition("card4Icon");
	$this->card4Icon = $card4Icon;
	return $this;
}

/**
* Get block6 - Block 6
* @return \Pimcore\Model\DataObject\Data\BlockElement[][]
*/
public function getBlock6 () {
	$preValue = $this->preGetValue("block6"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("block6")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set block6 - Block 6
* @param \Pimcore\Model\DataObject\Data\BlockElement[][] $block6
* @return \Pimcore\Model\DataObject\AboutCompany
*/
public function setBlock6 ($block6) {
	$fd = $this->getClass()->getFieldDefinition("block6");
	$this->block6 = $fd->preSetData($this, $block6);
	return $this;
}

/**
* Get contentTitle5 - Content Title 5
* @return string
*/
public function getContentTitle5 () {
	$preValue = $this->preGetValue("contentTitle5"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->contentTitle5;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set contentTitle5 - Content Title 5
* @param string $contentTitle5
* @return \Pimcore\Model\DataObject\AboutCompany
*/
public function setContentTitle5 ($contentTitle5) {
	$fd = $this->getClass()->getFieldDefinition("contentTitle5");
	$this->contentTitle5 = $contentTitle5;
	return $this;
}

/**
* Get block7 - Block 7
* @return \Pimcore\Model\DataObject\Data\BlockElement[][]
*/
public function getBlock7 () {
	$preValue = $this->preGetValue("block7"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("block7")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set block7 - Block 7
* @param \Pimcore\Model\DataObject\Data\BlockElement[][] $block7
* @return \Pimcore\Model\DataObject\AboutCompany
*/
public function setBlock7 ($block7) {
	$fd = $this->getClass()->getFieldDefinition("block7");
	$this->block7 = $fd->preSetData($this, $block7);
	return $this;
}

/**
* Get contentTitle6 - Content Title 6
* @return string
*/
public function getContentTitle6 () {
	$preValue = $this->preGetValue("contentTitle6"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->contentTitle6;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set contentTitle6 - Content Title 6
* @param string $contentTitle6
* @return \Pimcore\Model\DataObject\AboutCompany
*/
public function setContentTitle6 ($contentTitle6) {
	$fd = $this->getClass()->getFieldDefinition("contentTitle6");
	$this->contentTitle6 = $contentTitle6;
	return $this;
}

protected static $_relationFields = array (
  'permalink' => 
  array (
    'type' => 'href',
  ),
  'contactUsPage' => 
  array (
    'type' => 'href',
  ),
);

protected $lazyLoadedFields = array (
  0 => 'permalink',
  1 => 'contactUsPage',
);

}

