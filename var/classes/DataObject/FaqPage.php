<?php 

/** 
* Generated at: 2020-01-25T19:41:17+07:00
* Inheritance: no
* Variants: no
* Changed by: avristadm (2)
* IP: 172.17.0.1


Fields Summary: 
- siteId [select]
- page [href]
- image [image]
- name [input]
- title1 [input]
- title2 [input]
- title3 [input]
- description1 [textarea]
- text1 [input]
- text1Page [href]
- block [block]
-- category [input]
-- subBlock [block]
--- title [input]
--- faq [wysiwyg]
- contactUs [input]
- contactUsDescription [textarea]
- contactUsPage [href]
*/ 

namespace Pimcore\Model\DataObject;



/**
* @method static \Pimcore\Model\DataObject\FaqPage\Listing getBySiteId ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\FaqPage\Listing getByPage ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\FaqPage\Listing getByImage ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\FaqPage\Listing getByName ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\FaqPage\Listing getByTitle1 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\FaqPage\Listing getByTitle2 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\FaqPage\Listing getByTitle3 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\FaqPage\Listing getByDescription1 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\FaqPage\Listing getByText1 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\FaqPage\Listing getByText1Page ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\FaqPage\Listing getByBlock ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\FaqPage\Listing getByContactUs ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\FaqPage\Listing getByContactUsDescription ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\FaqPage\Listing getByContactUsPage ($value, $limit = 0) 
*/

class FaqPage extends Concrete implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {



use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $o_classId = "58";
protected $o_className = "FaqPage";
protected $siteId;
protected $page;
protected $image;
protected $name;
protected $title1;
protected $title2;
protected $title3;
protected $description1;
protected $text1;
protected $text1Page;
protected $block;
protected $contactUs;
protected $contactUsDescription;
protected $contactUsPage;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\FaqPage
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get siteId - Site
* @return string
*/
public function getSiteId () {
	$preValue = $this->preGetValue("siteId"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->siteId;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set siteId - Site
* @param string $siteId
* @return \Pimcore\Model\DataObject\FaqPage
*/
public function setSiteId ($siteId) {
	$fd = $this->getClass()->getFieldDefinition("siteId");
	$this->siteId = $siteId;
	return $this;
}

/**
* Get page - Page
* @return \Pimcore\Model\Document\Page | \Pimcore\Model\Document\Snippet | \Pimcore\Model\Document
*/
public function getPage () {
	$preValue = $this->preGetValue("page"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("page")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set page - Page
* @param \Pimcore\Model\Document\Page | \Pimcore\Model\Document\Snippet | \Pimcore\Model\Document $page
* @return \Pimcore\Model\DataObject\FaqPage
*/
public function setPage ($page) {
	$fd = $this->getClass()->getFieldDefinition("page");
	$currentData = $this->getPage();
	$isEqual = $fd->isEqual($currentData, $page);
	if (!$isEqual) {
		$this->markFieldDirty("page", true);
	}
	$this->page = $fd->preSetData($this, $page);
	return $this;
}

/**
* Get image - Image
* @return \Pimcore\Model\Asset\Image
*/
public function getImage () {
	$preValue = $this->preGetValue("image"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->image;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set image - Image
* @param \Pimcore\Model\Asset\Image $image
* @return \Pimcore\Model\DataObject\FaqPage
*/
public function setImage ($image) {
	$fd = $this->getClass()->getFieldDefinition("image");
	$this->image = $image;
	return $this;
}

/**
* Get name - Name
* @return string
*/
public function getName () {
	$preValue = $this->preGetValue("name"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->name;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set name - Name
* @param string $name
* @return \Pimcore\Model\DataObject\FaqPage
*/
public function setName ($name) {
	$fd = $this->getClass()->getFieldDefinition("name");
	$this->name = $name;
	return $this;
}

/**
* Get title1 - Title1
* @return string
*/
public function getTitle1 () {
	$preValue = $this->preGetValue("title1"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->title1;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set title1 - Title1
* @param string $title1
* @return \Pimcore\Model\DataObject\FaqPage
*/
public function setTitle1 ($title1) {
	$fd = $this->getClass()->getFieldDefinition("title1");
	$this->title1 = $title1;
	return $this;
}

/**
* Get title2 - Title2
* @return string
*/
public function getTitle2 () {
	$preValue = $this->preGetValue("title2"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->title2;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set title2 - Title2
* @param string $title2
* @return \Pimcore\Model\DataObject\FaqPage
*/
public function setTitle2 ($title2) {
	$fd = $this->getClass()->getFieldDefinition("title2");
	$this->title2 = $title2;
	return $this;
}

/**
* Get title3 - Title3
* @return string
*/
public function getTitle3 () {
	$preValue = $this->preGetValue("title3"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->title3;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set title3 - Title3
* @param string $title3
* @return \Pimcore\Model\DataObject\FaqPage
*/
public function setTitle3 ($title3) {
	$fd = $this->getClass()->getFieldDefinition("title3");
	$this->title3 = $title3;
	return $this;
}

/**
* Get description1 - Description1
* @return string
*/
public function getDescription1 () {
	$preValue = $this->preGetValue("description1"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->description1;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set description1 - Description1
* @param string $description1
* @return \Pimcore\Model\DataObject\FaqPage
*/
public function setDescription1 ($description1) {
	$fd = $this->getClass()->getFieldDefinition("description1");
	$this->description1 = $description1;
	return $this;
}

/**
* Get text1 - text1
* @return string
*/
public function getText1 () {
	$preValue = $this->preGetValue("text1"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->text1;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set text1 - text1
* @param string $text1
* @return \Pimcore\Model\DataObject\FaqPage
*/
public function setText1 ($text1) {
	$fd = $this->getClass()->getFieldDefinition("text1");
	$this->text1 = $text1;
	return $this;
}

/**
* Get text1Page - Text 1 Page
* @return \Pimcore\Model\Document\page
*/
public function getText1Page () {
	$preValue = $this->preGetValue("text1Page"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("text1Page")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set text1Page - Text 1 Page
* @param \Pimcore\Model\Document\page $text1Page
* @return \Pimcore\Model\DataObject\FaqPage
*/
public function setText1Page ($text1Page) {
	$fd = $this->getClass()->getFieldDefinition("text1Page");
	$currentData = $this->getText1Page();
	$isEqual = $fd->isEqual($currentData, $text1Page);
	if (!$isEqual) {
		$this->markFieldDirty("text1Page", true);
	}
	$this->text1Page = $fd->preSetData($this, $text1Page);
	return $this;
}

/**
* Get block - Block
* @return \Pimcore\Model\DataObject\Data\BlockElement[][]
*/
public function getBlock () {
	$preValue = $this->preGetValue("block"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("block")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set block - Block
* @param \Pimcore\Model\DataObject\Data\BlockElement[][] $block
* @return \Pimcore\Model\DataObject\FaqPage
*/
public function setBlock ($block) {
	$fd = $this->getClass()->getFieldDefinition("block");
	$this->block = $fd->preSetData($this, $block);
	return $this;
}

/**
* Get contactUs - Contact Us
* @return string
*/
public function getContactUs () {
	$preValue = $this->preGetValue("contactUs"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->contactUs;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set contactUs - Contact Us
* @param string $contactUs
* @return \Pimcore\Model\DataObject\FaqPage
*/
public function setContactUs ($contactUs) {
	$fd = $this->getClass()->getFieldDefinition("contactUs");
	$this->contactUs = $contactUs;
	return $this;
}

/**
* Get contactUsDescription - contact Us Description
* @return string
*/
public function getContactUsDescription () {
	$preValue = $this->preGetValue("contactUsDescription"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->contactUsDescription;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set contactUsDescription - contact Us Description
* @param string $contactUsDescription
* @return \Pimcore\Model\DataObject\FaqPage
*/
public function setContactUsDescription ($contactUsDescription) {
	$fd = $this->getClass()->getFieldDefinition("contactUsDescription");
	$this->contactUsDescription = $contactUsDescription;
	return $this;
}

/**
* Get contactUsPage - Contact Us Page
* @return \Pimcore\Model\Document\Page | \Pimcore\Model\Document\Snippet | \Pimcore\Model\Document
*/
public function getContactUsPage () {
	$preValue = $this->preGetValue("contactUsPage"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("contactUsPage")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set contactUsPage - Contact Us Page
* @param \Pimcore\Model\Document\Page | \Pimcore\Model\Document\Snippet | \Pimcore\Model\Document $contactUsPage
* @return \Pimcore\Model\DataObject\FaqPage
*/
public function setContactUsPage ($contactUsPage) {
	$fd = $this->getClass()->getFieldDefinition("contactUsPage");
	$currentData = $this->getContactUsPage();
	$isEqual = $fd->isEqual($currentData, $contactUsPage);
	if (!$isEqual) {
		$this->markFieldDirty("contactUsPage", true);
	}
	$this->contactUsPage = $fd->preSetData($this, $contactUsPage);
	return $this;
}

protected static $_relationFields = array (
  'page' => 
  array (
    'type' => 'href',
  ),
  'text1Page' => 
  array (
    'type' => 'href',
  ),
  'contactUsPage' => 
  array (
    'type' => 'href',
  ),
);

protected $lazyLoadedFields = array (
  0 => 'page',
  1 => 'text1Page',
  2 => 'contactUsPage',
);

}

