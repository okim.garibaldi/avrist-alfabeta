<?php 

namespace Pimcore\Model\DataObject\FrontPage;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\FrontPage current()
 * @method DataObject\FrontPage[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "85";
protected $className = "FrontPage";


}
