<?php 

/** 
* Generated at: 2019-06-20T11:39:28+07:00
* Inheritance: no
* Variants: no
* Changed by: avristadm (2)
* IP: 202.87.248.23


Fields Summary: 
- product [href]
- date [date]
- danaValue [numeric]
*/ 

namespace Pimcore\Model\DataObject;



/**
* @method static \Pimcore\Model\DataObject\DanaNavUpdate\Listing getByProduct ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\DanaNavUpdate\Listing getByDate ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\DanaNavUpdate\Listing getByDanaValue ($value, $limit = 0) 
*/

class DanaNavUpdate extends Concrete implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {



use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $o_classId = "20";
protected $o_className = "DanaNavUpdate";
protected $product;
protected $date;
protected $danaValue;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\DanaNavUpdate
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get product - Product
* @return \Pimcore\Model\DataObject\Product
*/
public function getProduct () {
	$preValue = $this->preGetValue("product"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("product")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set product - Product
* @param \Pimcore\Model\DataObject\Product $product
* @return \Pimcore\Model\DataObject\DanaNavUpdate
*/
public function setProduct ($product) {
	$fd = $this->getClass()->getFieldDefinition("product");
	$currentData = $this->getProduct();
	$isEqual = $fd->isEqual($currentData, $product);
	if (!$isEqual) {
		$this->markFieldDirty("product", true);
	}
	$this->product = $fd->preSetData($this, $product);
	return $this;
}

/**
* Get date - Date
* @return \Carbon\Carbon
*/
public function getDate () {
	$preValue = $this->preGetValue("date"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->date;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set date - Date
* @param \Carbon\Carbon $date
* @return \Pimcore\Model\DataObject\DanaNavUpdate
*/
public function setDate ($date) {
	$fd = $this->getClass()->getFieldDefinition("date");
	$this->date = $date;
	return $this;
}

/**
* Get danaValue - Dana Value
* @return string
*/
public function getDanaValue () {
	$preValue = $this->preGetValue("danaValue"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->danaValue;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set danaValue - Dana Value
* @param string $danaValue
* @return \Pimcore\Model\DataObject\DanaNavUpdate
*/
public function setDanaValue ($danaValue) {
	$fd = $this->getClass()->getFieldDefinition("danaValue");
	$this->danaValue = $danaValue;
	return $this;
}

protected static $_relationFields = array (
  'product' => 
  array (
    'type' => 'href',
  ),
);

protected $lazyLoadedFields = array (
);

}

