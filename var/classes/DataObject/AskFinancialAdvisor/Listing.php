<?php 

namespace Pimcore\Model\DataObject\AskFinancialAdvisor;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\AskFinancialAdvisor current()
 * @method DataObject\AskFinancialAdvisor[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "91";
protected $className = "AskFinancialAdvisor";


}
