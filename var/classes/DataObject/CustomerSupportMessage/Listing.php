<?php 

namespace Pimcore\Model\DataObject\CustomerSupportMessage;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\CustomerSupportMessage current()
 * @method DataObject\CustomerSupportMessage[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "89";
protected $className = "CustomerSupportMessage";


}
