<?php 

namespace Pimcore\Model\DataObject\LokasiRumahSakit;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\LokasiRumahSakit current()
 * @method DataObject\LokasiRumahSakit[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "76";
protected $className = "LokasiRumahSakit";


}
