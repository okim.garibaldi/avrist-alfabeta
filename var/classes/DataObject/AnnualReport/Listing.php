<?php 

namespace Pimcore\Model\DataObject\AnnualReport;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\AnnualReport current()
 * @method DataObject\AnnualReport[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "13";
protected $className = "AnnualReport";


}
