<?php 

namespace Pimcore\Model\DataObject\LifeGuidePage;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\LifeGuidePage current()
 * @method DataObject\LifeGuidePage[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "81";
protected $className = "LifeGuidePage";


}
