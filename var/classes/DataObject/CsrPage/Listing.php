<?php 

namespace Pimcore\Model\DataObject\CsrPage;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\CsrPage current()
 * @method DataObject\CsrPage[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "65";
protected $className = "CsrPage";


}
