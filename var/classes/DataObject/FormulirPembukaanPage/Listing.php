<?php 

namespace Pimcore\Model\DataObject\FormulirPembukaanPage;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\FormulirPembukaanPage current()
 * @method DataObject\FormulirPembukaanPage[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "61";
protected $className = "FormulirPembukaanPage";


}
