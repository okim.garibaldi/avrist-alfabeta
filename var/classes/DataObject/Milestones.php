<?php 

/** 
* Generated at: 2019-11-28T14:37:33+07:00
* Inheritance: no
* Variants: no
* Changed by: avristadm (2)
* IP: 172.17.0.1


Fields Summary: 
- year [input]
- description [wysiwyg]
*/ 

namespace Pimcore\Model\DataObject;



/**
* @method static \Pimcore\Model\DataObject\Milestones\Listing getByYear ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Milestones\Listing getByDescription ($value, $limit = 0) 
*/

class Milestones extends Concrete implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {



use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $o_classId = "54";
protected $o_className = "Milestones";
protected $year;
protected $description;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\Milestones
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get year - Year
* @return string
*/
public function getYear () {
	$preValue = $this->preGetValue("year"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->year;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set year - Year
* @param string $year
* @return \Pimcore\Model\DataObject\Milestones
*/
public function setYear ($year) {
	$fd = $this->getClass()->getFieldDefinition("year");
	$this->year = $year;
	return $this;
}

/**
* Get description - Description
* @return string
*/
public function getDescription () {
	$preValue = $this->preGetValue("description"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("description")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set description - Description
* @param string $description
* @return \Pimcore\Model\DataObject\Milestones
*/
public function setDescription ($description) {
	$fd = $this->getClass()->getFieldDefinition("description");
	$this->description = $description;
	return $this;
}

protected static $_relationFields = array (
);

protected $lazyLoadedFields = array (
);

}

