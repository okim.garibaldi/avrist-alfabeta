<?php 

namespace Pimcore\Model\DataObject\FormulirLayanan;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\FormulirLayanan current()
 * @method DataObject\FormulirLayanan[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "78";
protected $className = "FormulirLayanan";


}
