<?php 

namespace Pimcore\Model\DataObject\FaqPage;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\FaqPage current()
 * @method DataObject\FaqPage[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "58";
protected $className = "FaqPage";


}
