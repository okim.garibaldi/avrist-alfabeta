<?php 

/** 
* Generated at: 2020-01-06T09:51:56+07:00
* Inheritance: no
* Variants: no
* IP: 172.17.0.1


Fields Summary: 
*/ 

namespace Pimcore\Model\DataObject;



/**
*/

class LokasiRumahSakit extends Concrete implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {



use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $o_classId = "76";
protected $o_className = "LokasiRumahSakit";


/**
* @param array $values
* @return \Pimcore\Model\DataObject\LokasiRumahSakit
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

}

