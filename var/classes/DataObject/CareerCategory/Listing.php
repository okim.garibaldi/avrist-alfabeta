<?php 

namespace Pimcore\Model\DataObject\CareerCategory;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\CareerCategory current()
 * @method DataObject\CareerCategory[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "10";
protected $className = "CareerCategory";


}
