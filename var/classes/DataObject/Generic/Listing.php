<?php 

namespace Pimcore\Model\DataObject\Generic;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\Generic current()
 * @method DataObject\Generic[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "37";
protected $className = "Generic";


}
