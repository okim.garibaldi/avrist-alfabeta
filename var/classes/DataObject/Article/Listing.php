<?php 

namespace Pimcore\Model\DataObject\Article;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\Article current()
 * @method DataObject\Article[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "7";
protected $className = "Article";


}
