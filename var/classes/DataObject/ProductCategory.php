<?php 

/** 
* Generated at: 2019-11-27T13:39:00+07:00
* Inheritance: no
* Variants: no
* Changed by: avristadm (2)
* IP: 172.17.0.1


Fields Summary: 
- page [href]
- LOB [href]
- name [input]
- link [input]
- categoryType [select]
- description [wysiwyg]
*/ 

namespace Pimcore\Model\DataObject;



/**
* @method static \Pimcore\Model\DataObject\ProductCategory\Listing getByPage ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\ProductCategory\Listing getByLOB ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\ProductCategory\Listing getByName ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\ProductCategory\Listing getByLink ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\ProductCategory\Listing getByCategoryType ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\ProductCategory\Listing getByDescription ($value, $limit = 0) 
*/

class ProductCategory extends Concrete implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {



use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $o_classId = "22";
protected $o_className = "ProductCategory";
protected $page;
protected $LOB;
protected $name;
protected $link;
protected $categoryType;
protected $description;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\ProductCategory
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get page - Page
* @return \Pimcore\Model\Document\Page | \Pimcore\Model\Document\Snippet | \Pimcore\Model\Document
*/
public function getPage () {
	$preValue = $this->preGetValue("page"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("page")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set page - Page
* @param \Pimcore\Model\Document\Page | \Pimcore\Model\Document\Snippet | \Pimcore\Model\Document $page
* @return \Pimcore\Model\DataObject\ProductCategory
*/
public function setPage ($page) {
	$fd = $this->getClass()->getFieldDefinition("page");
	$currentData = $this->getPage();
	$isEqual = $fd->isEqual($currentData, $page);
	if (!$isEqual) {
		$this->markFieldDirty("page", true);
	}
	$this->page = $fd->preSetData($this, $page);
	return $this;
}

/**
* Get LOB - LOB
* @return \Pimcore\Model\DataObject\LOB
*/
public function getLOB () {
	$preValue = $this->preGetValue("LOB"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("LOB")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set LOB - LOB
* @param \Pimcore\Model\DataObject\LOB $LOB
* @return \Pimcore\Model\DataObject\ProductCategory
*/
public function setLOB ($LOB) {
	$fd = $this->getClass()->getFieldDefinition("LOB");
	$currentData = $this->getLOB();
	$isEqual = $fd->isEqual($currentData, $LOB);
	if (!$isEqual) {
		$this->markFieldDirty("LOB", true);
	}
	$this->LOB = $fd->preSetData($this, $LOB);
	return $this;
}

/**
* Get name - Name
* @return string
*/
public function getName () {
	$preValue = $this->preGetValue("name"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->name;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set name - Name
* @param string $name
* @return \Pimcore\Model\DataObject\ProductCategory
*/
public function setName ($name) {
	$fd = $this->getClass()->getFieldDefinition("name");
	$this->name = $name;
	return $this;
}

/**
* Get link - Link
* @return string
*/
public function getLink () {
	$preValue = $this->preGetValue("link"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->link;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set link - Link
* @param string $link
* @return \Pimcore\Model\DataObject\ProductCategory
*/
public function setLink ($link) {
	$fd = $this->getClass()->getFieldDefinition("link");
	$this->link = $link;
	return $this;
}

/**
* Get categoryType - Category Type
* @return string
*/
public function getCategoryType () {
	$preValue = $this->preGetValue("categoryType"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->categoryType;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set categoryType - Category Type
* @param string $categoryType
* @return \Pimcore\Model\DataObject\ProductCategory
*/
public function setCategoryType ($categoryType) {
	$fd = $this->getClass()->getFieldDefinition("categoryType");
	$this->categoryType = $categoryType;
	return $this;
}

/**
* Get description - Description
* @return string
*/
public function getDescription () {
	$preValue = $this->preGetValue("description"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("description")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set description - Description
* @param string $description
* @return \Pimcore\Model\DataObject\ProductCategory
*/
public function setDescription ($description) {
	$fd = $this->getClass()->getFieldDefinition("description");
	$this->description = $description;
	return $this;
}

protected static $_relationFields = array (
  'page' => 
  array (
    'type' => 'href',
  ),
  'LOB' => 
  array (
    'type' => 'href',
  ),
);

protected $lazyLoadedFields = array (
  0 => 'page',
);

}

