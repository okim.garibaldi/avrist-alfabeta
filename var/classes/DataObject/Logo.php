<?php 

/** 
* Generated at: 2020-01-23T16:24:38+07:00
* Inheritance: no
* Variants: no
* Changed by: avristadm (2)
* IP: 172.17.0.1


Fields Summary: 
- siteId [select]
- logo [image]
*/ 

namespace Pimcore\Model\DataObject;



/**
* @method static \Pimcore\Model\DataObject\Logo\Listing getBySiteId ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Logo\Listing getByLogo ($value, $limit = 0) 
*/

class Logo extends Concrete implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {



use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $o_classId = "84";
protected $o_className = "Logo";
protected $siteId;
protected $logo;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\Logo
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get siteId - Site
* @return string
*/
public function getSiteId () {
	$preValue = $this->preGetValue("siteId"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->siteId;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set siteId - Site
* @param string $siteId
* @return \Pimcore\Model\DataObject\Logo
*/
public function setSiteId ($siteId) {
	$fd = $this->getClass()->getFieldDefinition("siteId");
	$this->siteId = $siteId;
	return $this;
}

/**
* Get logo - Logo
* @return \Pimcore\Model\Asset\Image
*/
public function getLogo () {
	$preValue = $this->preGetValue("logo"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->logo;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set logo - Logo
* @param \Pimcore\Model\Asset\Image $logo
* @return \Pimcore\Model\DataObject\Logo
*/
public function setLogo ($logo) {
	$fd = $this->getClass()->getFieldDefinition("logo");
	$this->logo = $logo;
	return $this;
}

protected static $_relationFields = array (
);

protected $lazyLoadedFields = array (
);

}

