<?php 

/** 
* Generated at: 2019-06-20T11:39:32+07:00
* Inheritance: no
* Variants: no
* Changed by: avristadm (2)
* IP: 202.87.248.23


Fields Summary: 
- name [input]
*/ 

namespace Pimcore\Model\DataObject;



/**
* @method static \Pimcore\Model\DataObject\CsrType\Listing getByName ($value, $limit = 0) 
*/

class CsrType extends Concrete implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {



use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $o_classId = "27";
protected $o_className = "CsrType";
protected $name;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\CsrType
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get name - name
* @return string
*/
public function getName () {
	$preValue = $this->preGetValue("name"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->name;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set name - name
* @param string $name
* @return \Pimcore\Model\DataObject\CsrType
*/
public function setName ($name) {
	$fd = $this->getClass()->getFieldDefinition("name");
	$this->name = $name;
	return $this;
}

protected static $_relationFields = array (
);

protected $lazyLoadedFields = array (
);

}

