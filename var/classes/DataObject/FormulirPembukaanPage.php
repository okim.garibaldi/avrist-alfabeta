<?php 

/** 
* Generated at: 2020-01-13T09:55:00+07:00
* Inheritance: no
* Variants: no
* Changed by: avristadm (2)
* IP: 172.17.0.1


Fields Summary: 
- siteId [select]
- name [input]
- block [block]
-- name [input]
-- file [href]
- contactUs [input]
- contactUsDescription [textarea]
- contactUsPage [href]
*/ 

namespace Pimcore\Model\DataObject;



/**
* @method static \Pimcore\Model\DataObject\FormulirPembukaanPage\Listing getBySiteId ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\FormulirPembukaanPage\Listing getByName ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\FormulirPembukaanPage\Listing getByBlock ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\FormulirPembukaanPage\Listing getByContactUs ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\FormulirPembukaanPage\Listing getByContactUsDescription ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\FormulirPembukaanPage\Listing getByContactUsPage ($value, $limit = 0) 
*/

class FormulirPembukaanPage extends Concrete implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {



use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $o_classId = "61";
protected $o_className = "FormulirPembukaanPage";
protected $siteId;
protected $name;
protected $block;
protected $contactUs;
protected $contactUsDescription;
protected $contactUsPage;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\FormulirPembukaanPage
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get siteId - Site
* @return string
*/
public function getSiteId () {
	$preValue = $this->preGetValue("siteId"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->siteId;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set siteId - Site
* @param string $siteId
* @return \Pimcore\Model\DataObject\FormulirPembukaanPage
*/
public function setSiteId ($siteId) {
	$fd = $this->getClass()->getFieldDefinition("siteId");
	$this->siteId = $siteId;
	return $this;
}

/**
* Get name - name
* @return string
*/
public function getName () {
	$preValue = $this->preGetValue("name"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->name;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set name - name
* @param string $name
* @return \Pimcore\Model\DataObject\FormulirPembukaanPage
*/
public function setName ($name) {
	$fd = $this->getClass()->getFieldDefinition("name");
	$this->name = $name;
	return $this;
}

/**
* Get block - Block
* @return \Pimcore\Model\DataObject\Data\BlockElement[][]
*/
public function getBlock () {
	$preValue = $this->preGetValue("block"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("block")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set block - Block
* @param \Pimcore\Model\DataObject\Data\BlockElement[][] $block
* @return \Pimcore\Model\DataObject\FormulirPembukaanPage
*/
public function setBlock ($block) {
	$fd = $this->getClass()->getFieldDefinition("block");
	$this->block = $fd->preSetData($this, $block);
	return $this;
}

/**
* Get contactUs - Contact Us
* @return string
*/
public function getContactUs () {
	$preValue = $this->preGetValue("contactUs"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->contactUs;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set contactUs - Contact Us
* @param string $contactUs
* @return \Pimcore\Model\DataObject\FormulirPembukaanPage
*/
public function setContactUs ($contactUs) {
	$fd = $this->getClass()->getFieldDefinition("contactUs");
	$this->contactUs = $contactUs;
	return $this;
}

/**
* Get contactUsDescription - contact Us Description
* @return string
*/
public function getContactUsDescription () {
	$preValue = $this->preGetValue("contactUsDescription"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->contactUsDescription;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set contactUsDescription - contact Us Description
* @param string $contactUsDescription
* @return \Pimcore\Model\DataObject\FormulirPembukaanPage
*/
public function setContactUsDescription ($contactUsDescription) {
	$fd = $this->getClass()->getFieldDefinition("contactUsDescription");
	$this->contactUsDescription = $contactUsDescription;
	return $this;
}

/**
* Get contactUsPage - Contact Us Page
* @return \Pimcore\Model\Document\page
*/
public function getContactUsPage () {
	$preValue = $this->preGetValue("contactUsPage"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("contactUsPage")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set contactUsPage - Contact Us Page
* @param \Pimcore\Model\Document\page $contactUsPage
* @return \Pimcore\Model\DataObject\FormulirPembukaanPage
*/
public function setContactUsPage ($contactUsPage) {
	$fd = $this->getClass()->getFieldDefinition("contactUsPage");
	$currentData = $this->getContactUsPage();
	$isEqual = $fd->isEqual($currentData, $contactUsPage);
	if (!$isEqual) {
		$this->markFieldDirty("contactUsPage", true);
	}
	$this->contactUsPage = $fd->preSetData($this, $contactUsPage);
	return $this;
}

protected static $_relationFields = array (
  'contactUsPage' => 
  array (
    'type' => 'href',
  ),
);

protected $lazyLoadedFields = array (
  0 => 'contactUsPage',
);

}

