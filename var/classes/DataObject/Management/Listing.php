<?php 

namespace Pimcore\Model\DataObject\Management;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\Management current()
 * @method DataObject\Management[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "55";
protected $className = "Management";


}
