<?php 

/** 
* Generated at: 2019-06-20T11:39:24+07:00
* Inheritance: no
* Variants: no
* Changed by: avristadm (2)
* IP: 202.87.248.23


Fields Summary: 
- Name [input]
*/ 

namespace Pimcore\Model\DataObject;



/**
* @method static \Pimcore\Model\DataObject\City\Listing getByName ($value, $limit = 0) 
*/

class City extends Concrete implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {



use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $o_classId = "14";
protected $o_className = "City";
protected $Name;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\City
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get Name - Name
* @return string
*/
public function getName () {
	$preValue = $this->preGetValue("Name"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->Name;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set Name - Name
* @param string $Name
* @return \Pimcore\Model\DataObject\City
*/
public function setName ($Name) {
	$fd = $this->getClass()->getFieldDefinition("Name");
	$this->Name = $Name;
	return $this;
}

protected static $_relationFields = array (
);

protected $lazyLoadedFields = array (
);

}

