<?php 

namespace Pimcore\Model\DataObject\Footer;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\Footer current()
 * @method DataObject\Footer[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "64";
protected $className = "Footer";


}
