<?php 

/** 
* Generated at: 2020-01-23T11:54:38+07:00
* Inheritance: no
* Variants: no
* Changed by: avristadm (2)
* IP: 172.17.0.1


Fields Summary: 
- siteId [select]
- title1 [input]
- description1 [textarea]
- title2 [input]
- description2 [textarea]
- block1 [block]
-- layanan [href]
- title3 [input]
- description3 [textarea]
- block2 [block]
-- title [input]
-- description [textarea]
-- file [href]
- title4 [input]
- description4 [textarea]
- block3 [block]
-- title [input]
-- description [textarea]
-- formulir [href]
- title5 [input]
- description5 [textarea]
- block4 [block]
-- name [input]
-- description [textarea]
-- page [input]
- contactUs [input]
- contactUsDescription [textarea]
- contactUsPage [href]
*/ 

namespace Pimcore\Model\DataObject;



/**
* @method static \Pimcore\Model\DataObject\LayananNasabahPage\Listing getBySiteId ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\LayananNasabahPage\Listing getByTitle1 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\LayananNasabahPage\Listing getByDescription1 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\LayananNasabahPage\Listing getByTitle2 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\LayananNasabahPage\Listing getByDescription2 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\LayananNasabahPage\Listing getByBlock1 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\LayananNasabahPage\Listing getByTitle3 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\LayananNasabahPage\Listing getByDescription3 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\LayananNasabahPage\Listing getByBlock2 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\LayananNasabahPage\Listing getByTitle4 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\LayananNasabahPage\Listing getByDescription4 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\LayananNasabahPage\Listing getByBlock3 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\LayananNasabahPage\Listing getByTitle5 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\LayananNasabahPage\Listing getByDescription5 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\LayananNasabahPage\Listing getByBlock4 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\LayananNasabahPage\Listing getByContactUs ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\LayananNasabahPage\Listing getByContactUsDescription ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\LayananNasabahPage\Listing getByContactUsPage ($value, $limit = 0) 
*/

class LayananNasabahPage extends Concrete implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {



use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $o_classId = "74";
protected $o_className = "LayananNasabahPage";
protected $siteId;
protected $title1;
protected $description1;
protected $title2;
protected $description2;
protected $block1;
protected $title3;
protected $description3;
protected $block2;
protected $title4;
protected $description4;
protected $block3;
protected $title5;
protected $description5;
protected $block4;
protected $contactUs;
protected $contactUsDescription;
protected $contactUsPage;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\LayananNasabahPage
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get siteId - Site
* @return string
*/
public function getSiteId () {
	$preValue = $this->preGetValue("siteId"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->siteId;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set siteId - Site
* @param string $siteId
* @return \Pimcore\Model\DataObject\LayananNasabahPage
*/
public function setSiteId ($siteId) {
	$fd = $this->getClass()->getFieldDefinition("siteId");
	$this->siteId = $siteId;
	return $this;
}

/**
* Get title1 - Title 1
* @return string
*/
public function getTitle1 () {
	$preValue = $this->preGetValue("title1"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->title1;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set title1 - Title 1
* @param string $title1
* @return \Pimcore\Model\DataObject\LayananNasabahPage
*/
public function setTitle1 ($title1) {
	$fd = $this->getClass()->getFieldDefinition("title1");
	$this->title1 = $title1;
	return $this;
}

/**
* Get description1 - Description 1
* @return string
*/
public function getDescription1 () {
	$preValue = $this->preGetValue("description1"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->description1;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set description1 - Description 1
* @param string $description1
* @return \Pimcore\Model\DataObject\LayananNasabahPage
*/
public function setDescription1 ($description1) {
	$fd = $this->getClass()->getFieldDefinition("description1");
	$this->description1 = $description1;
	return $this;
}

/**
* Get title2 - Title 2
* @return string
*/
public function getTitle2 () {
	$preValue = $this->preGetValue("title2"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->title2;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set title2 - Title 2
* @param string $title2
* @return \Pimcore\Model\DataObject\LayananNasabahPage
*/
public function setTitle2 ($title2) {
	$fd = $this->getClass()->getFieldDefinition("title2");
	$this->title2 = $title2;
	return $this;
}

/**
* Get description2 - Description 2
* @return string
*/
public function getDescription2 () {
	$preValue = $this->preGetValue("description2"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->description2;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set description2 - Description 2
* @param string $description2
* @return \Pimcore\Model\DataObject\LayananNasabahPage
*/
public function setDescription2 ($description2) {
	$fd = $this->getClass()->getFieldDefinition("description2");
	$this->description2 = $description2;
	return $this;
}

/**
* Get block1 - Block 1
* @return \Pimcore\Model\DataObject\Data\BlockElement[][]
*/
public function getBlock1 () {
	$preValue = $this->preGetValue("block1"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("block1")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set block1 - Block 1
* @param \Pimcore\Model\DataObject\Data\BlockElement[][] $block1
* @return \Pimcore\Model\DataObject\LayananNasabahPage
*/
public function setBlock1 ($block1) {
	$fd = $this->getClass()->getFieldDefinition("block1");
	$this->block1 = $fd->preSetData($this, $block1);
	return $this;
}

/**
* Get title3 - Title 3
* @return string
*/
public function getTitle3 () {
	$preValue = $this->preGetValue("title3"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->title3;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set title3 - Title 3
* @param string $title3
* @return \Pimcore\Model\DataObject\LayananNasabahPage
*/
public function setTitle3 ($title3) {
	$fd = $this->getClass()->getFieldDefinition("title3");
	$this->title3 = $title3;
	return $this;
}

/**
* Get description3 - Description 3
* @return string
*/
public function getDescription3 () {
	$preValue = $this->preGetValue("description3"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->description3;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set description3 - Description 3
* @param string $description3
* @return \Pimcore\Model\DataObject\LayananNasabahPage
*/
public function setDescription3 ($description3) {
	$fd = $this->getClass()->getFieldDefinition("description3");
	$this->description3 = $description3;
	return $this;
}

/**
* Get block2 - Block 2
* @return \Pimcore\Model\DataObject\Data\BlockElement[][]
*/
public function getBlock2 () {
	$preValue = $this->preGetValue("block2"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("block2")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set block2 - Block 2
* @param \Pimcore\Model\DataObject\Data\BlockElement[][] $block2
* @return \Pimcore\Model\DataObject\LayananNasabahPage
*/
public function setBlock2 ($block2) {
	$fd = $this->getClass()->getFieldDefinition("block2");
	$this->block2 = $fd->preSetData($this, $block2);
	return $this;
}

/**
* Get title4 - Title 4
* @return string
*/
public function getTitle4 () {
	$preValue = $this->preGetValue("title4"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->title4;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set title4 - Title 4
* @param string $title4
* @return \Pimcore\Model\DataObject\LayananNasabahPage
*/
public function setTitle4 ($title4) {
	$fd = $this->getClass()->getFieldDefinition("title4");
	$this->title4 = $title4;
	return $this;
}

/**
* Get description4 - Description 4
* @return string
*/
public function getDescription4 () {
	$preValue = $this->preGetValue("description4"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->description4;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set description4 - Description 4
* @param string $description4
* @return \Pimcore\Model\DataObject\LayananNasabahPage
*/
public function setDescription4 ($description4) {
	$fd = $this->getClass()->getFieldDefinition("description4");
	$this->description4 = $description4;
	return $this;
}

/**
* Get block3 - Block 3
* @return \Pimcore\Model\DataObject\Data\BlockElement[][]
*/
public function getBlock3 () {
	$preValue = $this->preGetValue("block3"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("block3")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set block3 - Block 3
* @param \Pimcore\Model\DataObject\Data\BlockElement[][] $block3
* @return \Pimcore\Model\DataObject\LayananNasabahPage
*/
public function setBlock3 ($block3) {
	$fd = $this->getClass()->getFieldDefinition("block3");
	$this->block3 = $fd->preSetData($this, $block3);
	return $this;
}

/**
* Get title5 - Title 5
* @return string
*/
public function getTitle5 () {
	$preValue = $this->preGetValue("title5"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->title5;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set title5 - Title 5
* @param string $title5
* @return \Pimcore\Model\DataObject\LayananNasabahPage
*/
public function setTitle5 ($title5) {
	$fd = $this->getClass()->getFieldDefinition("title5");
	$this->title5 = $title5;
	return $this;
}

/**
* Get description5 - Description 5
* @return string
*/
public function getDescription5 () {
	$preValue = $this->preGetValue("description5"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->description5;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set description5 - Description 5
* @param string $description5
* @return \Pimcore\Model\DataObject\LayananNasabahPage
*/
public function setDescription5 ($description5) {
	$fd = $this->getClass()->getFieldDefinition("description5");
	$this->description5 = $description5;
	return $this;
}

/**
* Get block4 - Block 4
* @return \Pimcore\Model\DataObject\Data\BlockElement[][]
*/
public function getBlock4 () {
	$preValue = $this->preGetValue("block4"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("block4")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set block4 - Block 4
* @param \Pimcore\Model\DataObject\Data\BlockElement[][] $block4
* @return \Pimcore\Model\DataObject\LayananNasabahPage
*/
public function setBlock4 ($block4) {
	$fd = $this->getClass()->getFieldDefinition("block4");
	$this->block4 = $fd->preSetData($this, $block4);
	return $this;
}

/**
* Get contactUs - Contact Us
* @return string
*/
public function getContactUs () {
	$preValue = $this->preGetValue("contactUs"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->contactUs;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set contactUs - Contact Us
* @param string $contactUs
* @return \Pimcore\Model\DataObject\LayananNasabahPage
*/
public function setContactUs ($contactUs) {
	$fd = $this->getClass()->getFieldDefinition("contactUs");
	$this->contactUs = $contactUs;
	return $this;
}

/**
* Get contactUsDescription - contact Us Description
* @return string
*/
public function getContactUsDescription () {
	$preValue = $this->preGetValue("contactUsDescription"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->contactUsDescription;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set contactUsDescription - contact Us Description
* @param string $contactUsDescription
* @return \Pimcore\Model\DataObject\LayananNasabahPage
*/
public function setContactUsDescription ($contactUsDescription) {
	$fd = $this->getClass()->getFieldDefinition("contactUsDescription");
	$this->contactUsDescription = $contactUsDescription;
	return $this;
}

/**
* Get contactUsPage - Contact Us Page
* @return \Pimcore\Model\Document\Page | \Pimcore\Model\Document\Snippet | \Pimcore\Model\Document
*/
public function getContactUsPage () {
	$preValue = $this->preGetValue("contactUsPage"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("contactUsPage")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set contactUsPage - Contact Us Page
* @param \Pimcore\Model\Document\Page | \Pimcore\Model\Document\Snippet | \Pimcore\Model\Document $contactUsPage
* @return \Pimcore\Model\DataObject\LayananNasabahPage
*/
public function setContactUsPage ($contactUsPage) {
	$fd = $this->getClass()->getFieldDefinition("contactUsPage");
	$currentData = $this->getContactUsPage();
	$isEqual = $fd->isEqual($currentData, $contactUsPage);
	if (!$isEqual) {
		$this->markFieldDirty("contactUsPage", true);
	}
	$this->contactUsPage = $fd->preSetData($this, $contactUsPage);
	return $this;
}

protected static $_relationFields = array (
  'contactUsPage' => 
  array (
    'type' => 'href',
  ),
);

protected $lazyLoadedFields = array (
  0 => 'contactUsPage',
);

}

