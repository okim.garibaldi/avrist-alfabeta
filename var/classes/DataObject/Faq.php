<?php 

/** 
* Generated at: 2019-06-20T11:39:23+07:00
* Inheritance: no
* Variants: no
* Changed by: avristadm (2)
* IP: 202.87.248.23


Fields Summary: 
- question [input]
- answer [textarea]
- LOB [href]
- faqCategory [href]
*/ 

namespace Pimcore\Model\DataObject;



/**
* @method static \Pimcore\Model\DataObject\Faq\Listing getByQuestion ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Faq\Listing getByAnswer ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Faq\Listing getByLOB ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Faq\Listing getByFaqCategory ($value, $limit = 0) 
*/

class Faq extends Concrete implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {



use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $o_classId = "11";
protected $o_className = "Faq";
protected $question;
protected $answer;
protected $LOB;
protected $faqCategory;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\Faq
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get question - question
* @return string
*/
public function getQuestion () {
	$preValue = $this->preGetValue("question"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->question;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set question - question
* @param string $question
* @return \Pimcore\Model\DataObject\Faq
*/
public function setQuestion ($question) {
	$fd = $this->getClass()->getFieldDefinition("question");
	$this->question = $question;
	return $this;
}

/**
* Get answer - answer
* @return string
*/
public function getAnswer () {
	$preValue = $this->preGetValue("answer"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->answer;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set answer - answer
* @param string $answer
* @return \Pimcore\Model\DataObject\Faq
*/
public function setAnswer ($answer) {
	$fd = $this->getClass()->getFieldDefinition("answer");
	$this->answer = $answer;
	return $this;
}

/**
* Get LOB - LOB
* @return \Pimcore\Model\DataObject\LOB
*/
public function getLOB () {
	$preValue = $this->preGetValue("LOB"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("LOB")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set LOB - LOB
* @param \Pimcore\Model\DataObject\LOB $LOB
* @return \Pimcore\Model\DataObject\Faq
*/
public function setLOB ($LOB) {
	$fd = $this->getClass()->getFieldDefinition("LOB");
	$currentData = $this->getLOB();
	$isEqual = $fd->isEqual($currentData, $LOB);
	if (!$isEqual) {
		$this->markFieldDirty("LOB", true);
	}
	$this->LOB = $fd->preSetData($this, $LOB);
	return $this;
}

/**
* Get faqCategory - FAQ Category
* @return \Pimcore\Model\DataObject\FaqCategory
*/
public function getFaqCategory () {
	$preValue = $this->preGetValue("faqCategory"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("faqCategory")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set faqCategory - FAQ Category
* @param \Pimcore\Model\DataObject\FaqCategory $faqCategory
* @return \Pimcore\Model\DataObject\Faq
*/
public function setFaqCategory ($faqCategory) {
	$fd = $this->getClass()->getFieldDefinition("faqCategory");
	$currentData = $this->getFaqCategory();
	$isEqual = $fd->isEqual($currentData, $faqCategory);
	if (!$isEqual) {
		$this->markFieldDirty("faqCategory", true);
	}
	$this->faqCategory = $fd->preSetData($this, $faqCategory);
	return $this;
}

protected static $_relationFields = array (
  'LOB' => 
  array (
    'type' => 'href',
  ),
  'faqCategory' => 
  array (
    'type' => 'href',
  ),
);

protected $lazyLoadedFields = array (
  0 => 'LOB',
  1 => 'faqCategory',
);

}

