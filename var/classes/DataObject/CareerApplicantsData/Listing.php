<?php 

namespace Pimcore\Model\DataObject\CareerApplicantsData;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\CareerApplicantsData current()
 * @method DataObject\CareerApplicantsData[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "41";
protected $className = "CareerApplicantsData";


}
