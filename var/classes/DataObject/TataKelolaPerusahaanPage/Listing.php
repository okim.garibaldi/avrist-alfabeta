<?php 

namespace Pimcore\Model\DataObject\TataKelolaPerusahaanPage;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\TataKelolaPerusahaanPage current()
 * @method DataObject\TataKelolaPerusahaanPage[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "87";
protected $className = "TataKelolaPerusahaanPage";


}
