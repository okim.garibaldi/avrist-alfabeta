<?php 

namespace Pimcore\Model\DataObject\DanaNavUpdate;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\DanaNavUpdate current()
 * @method DataObject\DanaNavUpdate[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "20";
protected $className = "DanaNavUpdate";


}
