<?php 

/** 
* Generated at: 2019-06-20T11:39:37+07:00
* Inheritance: no
* Variants: no
* Changed by: avristadm (2)
* IP: 202.87.248.23


Fields Summary: 
- product [href]
- entryAgeBand [input]
- gender [input]
- planA [input]
- planB [input]
- planC [input]
- planD [input]
- planE [input]
- planF [input]
*/ 

namespace Pimcore\Model\DataObject;



/**
* @method static \Pimcore\Model\DataObject\PremiumFactor\Listing getByProduct ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\PremiumFactor\Listing getByEntryAgeBand ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\PremiumFactor\Listing getByGender ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\PremiumFactor\Listing getByPlanA ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\PremiumFactor\Listing getByPlanB ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\PremiumFactor\Listing getByPlanC ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\PremiumFactor\Listing getByPlanD ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\PremiumFactor\Listing getByPlanE ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\PremiumFactor\Listing getByPlanF ($value, $limit = 0) 
*/

class PremiumFactor extends Concrete implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {



use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $o_classId = "35";
protected $o_className = "PremiumFactor";
protected $product;
protected $entryAgeBand;
protected $gender;
protected $planA;
protected $planB;
protected $planC;
protected $planD;
protected $planE;
protected $planF;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\PremiumFactor
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get product - product
* @return \Pimcore\Model\DataObject\Product
*/
public function getProduct () {
	$preValue = $this->preGetValue("product"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("product")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set product - product
* @param \Pimcore\Model\DataObject\Product $product
* @return \Pimcore\Model\DataObject\PremiumFactor
*/
public function setProduct ($product) {
	$fd = $this->getClass()->getFieldDefinition("product");
	$currentData = $this->getProduct();
	$isEqual = $fd->isEqual($currentData, $product);
	if (!$isEqual) {
		$this->markFieldDirty("product", true);
	}
	$this->product = $fd->preSetData($this, $product);
	return $this;
}

/**
* Get entryAgeBand - Entry Age Band
* @return string
*/
public function getEntryAgeBand () {
	$preValue = $this->preGetValue("entryAgeBand"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->entryAgeBand;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set entryAgeBand - Entry Age Band
* @param string $entryAgeBand
* @return \Pimcore\Model\DataObject\PremiumFactor
*/
public function setEntryAgeBand ($entryAgeBand) {
	$fd = $this->getClass()->getFieldDefinition("entryAgeBand");
	$this->entryAgeBand = $entryAgeBand;
	return $this;
}

/**
* Get gender - gender
* @return string
*/
public function getGender () {
	$preValue = $this->preGetValue("gender"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->gender;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set gender - gender
* @param string $gender
* @return \Pimcore\Model\DataObject\PremiumFactor
*/
public function setGender ($gender) {
	$fd = $this->getClass()->getFieldDefinition("gender");
	$this->gender = $gender;
	return $this;
}

/**
* Get planA - Plan A
* @return string
*/
public function getPlanA () {
	$preValue = $this->preGetValue("planA"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->planA;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set planA - Plan A
* @param string $planA
* @return \Pimcore\Model\DataObject\PremiumFactor
*/
public function setPlanA ($planA) {
	$fd = $this->getClass()->getFieldDefinition("planA");
	$this->planA = $planA;
	return $this;
}

/**
* Get planB - Plan B
* @return string
*/
public function getPlanB () {
	$preValue = $this->preGetValue("planB"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->planB;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set planB - Plan B
* @param string $planB
* @return \Pimcore\Model\DataObject\PremiumFactor
*/
public function setPlanB ($planB) {
	$fd = $this->getClass()->getFieldDefinition("planB");
	$this->planB = $planB;
	return $this;
}

/**
* Get planC - Plan C
* @return string
*/
public function getPlanC () {
	$preValue = $this->preGetValue("planC"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->planC;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set planC - Plan C
* @param string $planC
* @return \Pimcore\Model\DataObject\PremiumFactor
*/
public function setPlanC ($planC) {
	$fd = $this->getClass()->getFieldDefinition("planC");
	$this->planC = $planC;
	return $this;
}

/**
* Get planD - Plan D
* @return string
*/
public function getPlanD () {
	$preValue = $this->preGetValue("planD"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->planD;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set planD - Plan D
* @param string $planD
* @return \Pimcore\Model\DataObject\PremiumFactor
*/
public function setPlanD ($planD) {
	$fd = $this->getClass()->getFieldDefinition("planD");
	$this->planD = $planD;
	return $this;
}

/**
* Get planE - Plan E
* @return string
*/
public function getPlanE () {
	$preValue = $this->preGetValue("planE"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->planE;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set planE - Plan E
* @param string $planE
* @return \Pimcore\Model\DataObject\PremiumFactor
*/
public function setPlanE ($planE) {
	$fd = $this->getClass()->getFieldDefinition("planE");
	$this->planE = $planE;
	return $this;
}

/**
* Get planF - Plan F
* @return string
*/
public function getPlanF () {
	$preValue = $this->preGetValue("planF"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->planF;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set planF - Plan F
* @param string $planF
* @return \Pimcore\Model\DataObject\PremiumFactor
*/
public function setPlanF ($planF) {
	$fd = $this->getClass()->getFieldDefinition("planF");
	$this->planF = $planF;
	return $this;
}

protected static $_relationFields = array (
  'product' => 
  array (
    'type' => 'href',
  ),
);

protected $lazyLoadedFields = array (
  0 => 'product',
);

}

