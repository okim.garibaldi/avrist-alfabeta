<?php 

namespace Pimcore\Model\DataObject\SocialMedia;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\SocialMedia current()
 * @method DataObject\SocialMedia[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "71";
protected $className = "SocialMedia";


}
