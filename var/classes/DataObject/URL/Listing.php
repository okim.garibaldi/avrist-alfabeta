<?php 

namespace Pimcore\Model\DataObject\URL;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\URL current()
 * @method DataObject\URL[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "38";
protected $className = "URL";


}
