<?php 

namespace Pimcore\Model\DataObject\Misi;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\Misi current()
 * @method DataObject\Misi[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "51";
protected $className = "Misi";


}
