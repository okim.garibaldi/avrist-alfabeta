<?php 

/** 
* Generated at: 2019-11-20T08:40:22+07:00
* Inheritance: no
* Variants: no
* Changed by: avristadm (2)
* IP: 172.17.0.1


Fields Summary: 
- title [input]
- disabled [checkbox]
*/ 

namespace Pimcore\Model\DataObject;



/**
* @method static \Pimcore\Model\DataObject\Menu3\Listing getByTitle ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Menu3\Listing getByDisabled ($value, $limit = 0) 
*/

class Menu3 extends Concrete implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {



use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $o_classId = "49";
protected $o_className = "Menu3";
protected $title;
protected $disabled;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\Menu3
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get title - Title
* @return string
*/
public function getTitle () {
	$preValue = $this->preGetValue("title"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->title;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set title - Title
* @param string $title
* @return \Pimcore\Model\DataObject\Menu3
*/
public function setTitle ($title) {
	$fd = $this->getClass()->getFieldDefinition("title");
	$this->title = $title;
	return $this;
}

/**
* Get disabled - Disabled
* @return boolean
*/
public function getDisabled () {
	$preValue = $this->preGetValue("disabled"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->disabled;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set disabled - Disabled
* @param boolean $disabled
* @return \Pimcore\Model\DataObject\Menu3
*/
public function setDisabled ($disabled) {
	$fd = $this->getClass()->getFieldDefinition("disabled");
	$this->disabled = $disabled;
	return $this;
}

protected static $_relationFields = array (
);

protected $lazyLoadedFields = array (
);

}

