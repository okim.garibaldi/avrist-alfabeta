<?php 

/** 
* Generated at: 2019-06-20T11:39:30+07:00
* Inheritance: no
* Variants: no
* Changed by: avristadm (2)
* IP: 202.87.248.23


Fields Summary: 
- email [input]
- productid [objects]
*/ 

namespace Pimcore\Model\DataObject;



/**
* @method static \Pimcore\Model\DataObject\SolutionFinder\Listing getByEmail ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\SolutionFinder\Listing getByProductid ($value, $limit = 0) 
*/

class SolutionFinder extends Concrete implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {



use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $o_classId = "23";
protected $o_className = "SolutionFinder";
protected $email;
protected $productid;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\SolutionFinder
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get email - email
* @return string
*/
public function getEmail () {
	$preValue = $this->preGetValue("email"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->email;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set email - email
* @param string $email
* @return \Pimcore\Model\DataObject\SolutionFinder
*/
public function setEmail ($email) {
	$fd = $this->getClass()->getFieldDefinition("email");
	$this->email = $email;
	return $this;
}

/**
* Get productid - productid
* @return \Pimcore\Model\DataObject\AbstractObject[]
*/
public function getProductid () {
	$preValue = $this->preGetValue("productid"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("productid")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set productid - productid
* @param \Pimcore\Model\DataObject\AbstractObject[] $productid
* @return \Pimcore\Model\DataObject\SolutionFinder
*/
public function setProductid ($productid) {
	$fd = $this->getClass()->getFieldDefinition("productid");
	$currentData = $this->getProductid();
	$isEqual = $fd->isEqual($currentData, $productid);
	if (!$isEqual) {
		$this->markFieldDirty("productid", true);
	}
	$this->productid = $fd->preSetData($this, $productid);
	return $this;
}

protected static $_relationFields = array (
  'productid' => 
  array (
    'type' => 'objects',
  ),
);

protected $lazyLoadedFields = array (
  0 => 'productid',
);

}

