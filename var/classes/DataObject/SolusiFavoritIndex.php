<?php 

/** 
* Generated at: 2019-06-20T11:39:35+07:00
* Inheritance: no
* Variants: no
* Changed by: avristadm (2)
* IP: 202.87.248.23


Fields Summary: 
- number [objects]
*/ 

namespace Pimcore\Model\DataObject;



/**
* @method static \Pimcore\Model\DataObject\SolusiFavoritIndex\Listing getByNumber ($value, $limit = 0) 
*/

class SolusiFavoritIndex extends Concrete implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {



use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $o_classId = "32";
protected $o_className = "SolusiFavoritIndex";
protected $number;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\SolusiFavoritIndex
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get number - number
* @return \Pimcore\Model\DataObject\Product[]
*/
public function getNumber () {
	$preValue = $this->preGetValue("number"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("number")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set number - number
* @param \Pimcore\Model\DataObject\Product[] $number
* @return \Pimcore\Model\DataObject\SolusiFavoritIndex
*/
public function setNumber ($number) {
	$fd = $this->getClass()->getFieldDefinition("number");
	$currentData = $this->getNumber();
	$isEqual = $fd->isEqual($currentData, $number);
	if (!$isEqual) {
		$this->markFieldDirty("number", true);
	}
	$this->number = $fd->preSetData($this, $number);
	return $this;
}

protected static $_relationFields = array (
  'number' => 
  array (
    'type' => 'objects',
  ),
);

protected $lazyLoadedFields = array (
);

}

