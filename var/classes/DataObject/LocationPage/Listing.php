<?php 

namespace Pimcore\Model\DataObject\LocationPage;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\LocationPage current()
 * @method DataObject\LocationPage[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "72";
protected $className = "LocationPage";


}
