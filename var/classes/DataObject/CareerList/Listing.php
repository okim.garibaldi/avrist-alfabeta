<?php 

namespace Pimcore\Model\DataObject\CareerList;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\CareerList current()
 * @method DataObject\CareerList[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "73";
protected $className = "CareerList";


}
