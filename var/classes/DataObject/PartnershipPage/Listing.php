<?php 

namespace Pimcore\Model\DataObject\PartnershipPage;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\PartnershipPage current()
 * @method DataObject\PartnershipPage[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "45";
protected $className = "PartnershipPage";


}
