<?php 

namespace Pimcore\Model\DataObject\Menu;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\Menu current()
 * @method DataObject\Menu[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "47";
protected $className = "Menu";


}
