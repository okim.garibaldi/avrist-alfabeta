<?php 

/** 
* Generated at: 2019-12-23T08:08:52+07:00
* Inheritance: no
* Variants: no
* Changed by: avristadm (2)
* IP: 172.17.0.1


Fields Summary: 
- siteId [select]
- name [input]
- description [textarea]
- link [input]
- title1 [input]
- description1 [textarea]
*/ 

namespace Pimcore\Model\DataObject;



/**
* @method static \Pimcore\Model\DataObject\FinancialReportPage\Listing getBySiteId ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\FinancialReportPage\Listing getByName ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\FinancialReportPage\Listing getByDescription ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\FinancialReportPage\Listing getByLink ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\FinancialReportPage\Listing getByTitle1 ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\FinancialReportPage\Listing getByDescription1 ($value, $limit = 0) 
*/

class FinancialReportPage extends Concrete implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {



use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $o_classId = "69";
protected $o_className = "FinancialReportPage";
protected $siteId;
protected $name;
protected $description;
protected $link;
protected $title1;
protected $description1;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\FinancialReportPage
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get siteId - Site
* @return string
*/
public function getSiteId () {
	$preValue = $this->preGetValue("siteId"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->siteId;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set siteId - Site
* @param string $siteId
* @return \Pimcore\Model\DataObject\FinancialReportPage
*/
public function setSiteId ($siteId) {
	$fd = $this->getClass()->getFieldDefinition("siteId");
	$this->siteId = $siteId;
	return $this;
}

/**
* Get name - name
* @return string
*/
public function getName () {
	$preValue = $this->preGetValue("name"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->name;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set name - name
* @param string $name
* @return \Pimcore\Model\DataObject\FinancialReportPage
*/
public function setName ($name) {
	$fd = $this->getClass()->getFieldDefinition("name");
	$this->name = $name;
	return $this;
}

/**
* Get description - Description
* @return string
*/
public function getDescription () {
	$preValue = $this->preGetValue("description"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->description;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set description - Description
* @param string $description
* @return \Pimcore\Model\DataObject\FinancialReportPage
*/
public function setDescription ($description) {
	$fd = $this->getClass()->getFieldDefinition("description");
	$this->description = $description;
	return $this;
}

/**
* Get link - Link
* @return string
*/
public function getLink () {
	$preValue = $this->preGetValue("link"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->link;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set link - Link
* @param string $link
* @return \Pimcore\Model\DataObject\FinancialReportPage
*/
public function setLink ($link) {
	$fd = $this->getClass()->getFieldDefinition("link");
	$this->link = $link;
	return $this;
}

/**
* Get title1 - Title 1
* @return string
*/
public function getTitle1 () {
	$preValue = $this->preGetValue("title1"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->title1;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set title1 - Title 1
* @param string $title1
* @return \Pimcore\Model\DataObject\FinancialReportPage
*/
public function setTitle1 ($title1) {
	$fd = $this->getClass()->getFieldDefinition("title1");
	$this->title1 = $title1;
	return $this;
}

/**
* Get description1 - description 1
* @return string
*/
public function getDescription1 () {
	$preValue = $this->preGetValue("description1"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->description1;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set description1 - description 1
* @param string $description1
* @return \Pimcore\Model\DataObject\FinancialReportPage
*/
public function setDescription1 ($description1) {
	$fd = $this->getClass()->getFieldDefinition("description1");
	$this->description1 = $description1;
	return $this;
}

protected static $_relationFields = array (
);

protected $lazyLoadedFields = array (
);

}

