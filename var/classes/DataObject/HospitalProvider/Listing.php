<?php 

namespace Pimcore\Model\DataObject\HospitalProvider;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\HospitalProvider current()
 * @method DataObject\HospitalProvider[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "9";
protected $className = "HospitalProvider";


}
