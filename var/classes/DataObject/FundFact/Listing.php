<?php 

namespace Pimcore\Model\DataObject\FundFact;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\FundFact current()
 * @method DataObject\FundFact[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "68";
protected $className = "FundFact";


}
