<?php 

/** 
* Generated at: 2019-11-08T09:29:30+07:00
* Inheritance: no
* Variants: no
* Changed by: avristadm (2)
* IP: 172.17.0.1


Fields Summary: 
- name [input]
- address [input]
- city [input]
- state [input]
- zip [input]
- phone [input]
- email [input]
- gender [input]
- birthPlace [input]
- birthDate [input]
- submitAt [input]
- position [input]
*/ 

namespace Pimcore\Model\DataObject;



/**
* @method static \Pimcore\Model\DataObject\CareerApplicantsData\Listing getByName ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\CareerApplicantsData\Listing getByAddress ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\CareerApplicantsData\Listing getByCity ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\CareerApplicantsData\Listing getByState ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\CareerApplicantsData\Listing getByZip ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\CareerApplicantsData\Listing getByPhone ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\CareerApplicantsData\Listing getByEmail ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\CareerApplicantsData\Listing getByGender ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\CareerApplicantsData\Listing getByBirthPlace ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\CareerApplicantsData\Listing getByBirthDate ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\CareerApplicantsData\Listing getBySubmitAt ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\CareerApplicantsData\Listing getByPosition ($value, $limit = 0) 
*/

class CareerApplicantsData extends Concrete implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {



use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $o_classId = "41";
protected $o_className = "CareerApplicantsData";
protected $name;
protected $address;
protected $city;
protected $state;
protected $zip;
protected $phone;
protected $email;
protected $gender;
protected $birthPlace;
protected $birthDate;
protected $submitAt;
protected $position;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\CareerApplicantsData
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get name - Name
* @return string
*/
public function getName () {
	$preValue = $this->preGetValue("name"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->name;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set name - Name
* @param string $name
* @return \Pimcore\Model\DataObject\CareerApplicantsData
*/
public function setName ($name) {
	$fd = $this->getClass()->getFieldDefinition("name");
	$this->name = $name;
	return $this;
}

/**
* Get address - Address
* @return string
*/
public function getAddress () {
	$preValue = $this->preGetValue("address"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->address;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set address - Address
* @param string $address
* @return \Pimcore\Model\DataObject\CareerApplicantsData
*/
public function setAddress ($address) {
	$fd = $this->getClass()->getFieldDefinition("address");
	$this->address = $address;
	return $this;
}

/**
* Get city - City
* @return string
*/
public function getCity () {
	$preValue = $this->preGetValue("city"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->city;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set city - City
* @param string $city
* @return \Pimcore\Model\DataObject\CareerApplicantsData
*/
public function setCity ($city) {
	$fd = $this->getClass()->getFieldDefinition("city");
	$this->city = $city;
	return $this;
}

/**
* Get state - State
* @return string
*/
public function getState () {
	$preValue = $this->preGetValue("state"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->state;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set state - State
* @param string $state
* @return \Pimcore\Model\DataObject\CareerApplicantsData
*/
public function setState ($state) {
	$fd = $this->getClass()->getFieldDefinition("state");
	$this->state = $state;
	return $this;
}

/**
* Get zip - Zip
* @return string
*/
public function getZip () {
	$preValue = $this->preGetValue("zip"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->zip;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set zip - Zip
* @param string $zip
* @return \Pimcore\Model\DataObject\CareerApplicantsData
*/
public function setZip ($zip) {
	$fd = $this->getClass()->getFieldDefinition("zip");
	$this->zip = $zip;
	return $this;
}

/**
* Get phone - Phone
* @return string
*/
public function getPhone () {
	$preValue = $this->preGetValue("phone"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->phone;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set phone - Phone
* @param string $phone
* @return \Pimcore\Model\DataObject\CareerApplicantsData
*/
public function setPhone ($phone) {
	$fd = $this->getClass()->getFieldDefinition("phone");
	$this->phone = $phone;
	return $this;
}

/**
* Get email - Email
* @return string
*/
public function getEmail () {
	$preValue = $this->preGetValue("email"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->email;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set email - Email
* @param string $email
* @return \Pimcore\Model\DataObject\CareerApplicantsData
*/
public function setEmail ($email) {
	$fd = $this->getClass()->getFieldDefinition("email");
	$this->email = $email;
	return $this;
}

/**
* Get gender - Gender
* @return string
*/
public function getGender () {
	$preValue = $this->preGetValue("gender"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->gender;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set gender - Gender
* @param string $gender
* @return \Pimcore\Model\DataObject\CareerApplicantsData
*/
public function setGender ($gender) {
	$fd = $this->getClass()->getFieldDefinition("gender");
	$this->gender = $gender;
	return $this;
}

/**
* Get birthPlace - Birth Place
* @return string
*/
public function getBirthPlace () {
	$preValue = $this->preGetValue("birthPlace"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->birthPlace;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set birthPlace - Birth Place
* @param string $birthPlace
* @return \Pimcore\Model\DataObject\CareerApplicantsData
*/
public function setBirthPlace ($birthPlace) {
	$fd = $this->getClass()->getFieldDefinition("birthPlace");
	$this->birthPlace = $birthPlace;
	return $this;
}

/**
* Get birthDate - Birth Date
* @return string
*/
public function getBirthDate () {
	$preValue = $this->preGetValue("birthDate"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->birthDate;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set birthDate - Birth Date
* @param string $birthDate
* @return \Pimcore\Model\DataObject\CareerApplicantsData
*/
public function setBirthDate ($birthDate) {
	$fd = $this->getClass()->getFieldDefinition("birthDate");
	$this->birthDate = $birthDate;
	return $this;
}

/**
* Get submitAt - submit_at
* @return string
*/
public function getSubmitAt () {
	$preValue = $this->preGetValue("submitAt"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->submitAt;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set submitAt - submit_at
* @param string $submitAt
* @return \Pimcore\Model\DataObject\CareerApplicantsData
*/
public function setSubmitAt ($submitAt) {
	$fd = $this->getClass()->getFieldDefinition("submitAt");
	$this->submitAt = $submitAt;
	return $this;
}

/**
* Get position - Position
* @return string
*/
public function getPosition () {
	$preValue = $this->preGetValue("position"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->position;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set position - Position
* @param string $position
* @return \Pimcore\Model\DataObject\CareerApplicantsData
*/
public function setPosition ($position) {
	$fd = $this->getClass()->getFieldDefinition("position");
	$this->position = $position;
	return $this;
}

protected static $_relationFields = array (
);

protected $lazyLoadedFields = array (
);

}

