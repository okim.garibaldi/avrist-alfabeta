<?php 

namespace Pimcore\Model\DataObject\GetSolutionBisnis;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\GetSolutionBisnis current()
 * @method DataObject\GetSolutionBisnis[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "31";
protected $className = "GetSolutionBisnis";


}
