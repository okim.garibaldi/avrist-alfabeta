<?php 

namespace Pimcore\Model\DataObject\ContactUsTopic;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\ContactUsTopic current()
 * @method DataObject\ContactUsTopic[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "4";
protected $className = "ContactUsTopic";


}
