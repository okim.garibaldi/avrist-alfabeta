<?php 

namespace Pimcore\Model\DataObject\CustomerSupport;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\CustomerSupport current()
 * @method DataObject\CustomerSupport[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "57";
protected $className = "CustomerSupport";


}
