<?php 

/** 
* Generated at: 2019-11-28T14:48:34+07:00
* Inheritance: no
* Variants: no
* Changed by: avristadm (2)
* IP: 172.17.0.1


Fields Summary: 
- managementName [input]
- name [input]
- position [input]
- description [wysiwyg]
*/ 

namespace Pimcore\Model\DataObject;



/**
* @method static \Pimcore\Model\DataObject\Management\Listing getByManagementName ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Management\Listing getByName ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Management\Listing getByPosition ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Management\Listing getByDescription ($value, $limit = 0) 
*/

class Management extends Concrete implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {



use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $o_classId = "55";
protected $o_className = "Management";
protected $managementName;
protected $name;
protected $position;
protected $description;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\Management
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get managementName - Management Name
* @return string
*/
public function getManagementName () {
	$preValue = $this->preGetValue("managementName"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->managementName;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set managementName - Management Name
* @param string $managementName
* @return \Pimcore\Model\DataObject\Management
*/
public function setManagementName ($managementName) {
	$fd = $this->getClass()->getFieldDefinition("managementName");
	$this->managementName = $managementName;
	return $this;
}

/**
* Get name - Name
* @return string
*/
public function getName () {
	$preValue = $this->preGetValue("name"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->name;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set name - Name
* @param string $name
* @return \Pimcore\Model\DataObject\Management
*/
public function setName ($name) {
	$fd = $this->getClass()->getFieldDefinition("name");
	$this->name = $name;
	return $this;
}

/**
* Get position - Position
* @return string
*/
public function getPosition () {
	$preValue = $this->preGetValue("position"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->position;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set position - Position
* @param string $position
* @return \Pimcore\Model\DataObject\Management
*/
public function setPosition ($position) {
	$fd = $this->getClass()->getFieldDefinition("position");
	$this->position = $position;
	return $this;
}

/**
* Get description - Description
* @return string
*/
public function getDescription () {
	$preValue = $this->preGetValue("description"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->getClass()->getFieldDefinition("description")->preGetData($this);
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set description - Description
* @param string $description
* @return \Pimcore\Model\DataObject\Management
*/
public function setDescription ($description) {
	$fd = $this->getClass()->getFieldDefinition("description");
	$this->description = $description;
	return $this;
}

protected static $_relationFields = array (
);

protected $lazyLoadedFields = array (
);

}

