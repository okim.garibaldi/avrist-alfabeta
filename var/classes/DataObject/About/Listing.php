<?php 

namespace Pimcore\Model\DataObject\About;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\About current()
 * @method DataObject\About[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "28";
protected $className = "About";


}
