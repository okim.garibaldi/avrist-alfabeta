<?php 

namespace Pimcore\Model\DataObject\ProductPrice;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\ProductPrice current()
 * @method DataObject\ProductPrice[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "88";
protected $className = "ProductPrice";


}
