<?php 

/** 
* Generated at: 2020-02-17T06:15:50+07:00
* Inheritance: no
* Variants: no
* Changed by: avristadm (2)
* IP: 172.17.0.1


Fields Summary: 
- category [input]
- name [input]
- age [input]
- live [input]
- telp [input]
- email [input]
- solutionType [input]
- solutionNeeds [input]
- solutionFor [input]
- notSure [checkbox]
- submitAt [input]
- vehicleType [input]
- productionYear [input]
- plate [input]
- vehiclePrice [input]
- propertyCity [input]
- propertyFunction [input]
*/ 

namespace Pimcore\Model\DataObject;



/**
* @method static \Pimcore\Model\DataObject\GetSolution\Listing getByCategory ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\GetSolution\Listing getByName ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\GetSolution\Listing getByAge ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\GetSolution\Listing getByLive ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\GetSolution\Listing getByTelp ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\GetSolution\Listing getByEmail ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\GetSolution\Listing getBySolutionType ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\GetSolution\Listing getBySolutionNeeds ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\GetSolution\Listing getBySolutionFor ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\GetSolution\Listing getByNotSure ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\GetSolution\Listing getBySubmitAt ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\GetSolution\Listing getByVehicleType ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\GetSolution\Listing getByProductionYear ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\GetSolution\Listing getByPlate ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\GetSolution\Listing getByVehiclePrice ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\GetSolution\Listing getByPropertyCity ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\GetSolution\Listing getByPropertyFunction ($value, $limit = 0) 
*/

class GetSolution extends Concrete implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {



use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $o_classId = "30";
protected $o_className = "GetSolution";
protected $category;
protected $name;
protected $age;
protected $live;
protected $telp;
protected $email;
protected $solutionType;
protected $solutionNeeds;
protected $solutionFor;
protected $notSure;
protected $submitAt;
protected $vehicleType;
protected $productionYear;
protected $plate;
protected $vehiclePrice;
protected $propertyCity;
protected $propertyFunction;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\GetSolution
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get category - category
* @return string
*/
public function getCategory () {
	$preValue = $this->preGetValue("category"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->category;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set category - category
* @param string $category
* @return \Pimcore\Model\DataObject\GetSolution
*/
public function setCategory ($category) {
	$fd = $this->getClass()->getFieldDefinition("category");
	$this->category = $category;
	return $this;
}

/**
* Get name - name
* @return string
*/
public function getName () {
	$preValue = $this->preGetValue("name"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->name;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set name - name
* @param string $name
* @return \Pimcore\Model\DataObject\GetSolution
*/
public function setName ($name) {
	$fd = $this->getClass()->getFieldDefinition("name");
	$this->name = $name;
	return $this;
}

/**
* Get age - age
* @return string
*/
public function getAge () {
	$preValue = $this->preGetValue("age"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->age;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set age - age
* @param string $age
* @return \Pimcore\Model\DataObject\GetSolution
*/
public function setAge ($age) {
	$fd = $this->getClass()->getFieldDefinition("age");
	$this->age = $age;
	return $this;
}

/**
* Get live - live
* @return string
*/
public function getLive () {
	$preValue = $this->preGetValue("live"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->live;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set live - live
* @param string $live
* @return \Pimcore\Model\DataObject\GetSolution
*/
public function setLive ($live) {
	$fd = $this->getClass()->getFieldDefinition("live");
	$this->live = $live;
	return $this;
}

/**
* Get telp - telp
* @return string
*/
public function getTelp () {
	$preValue = $this->preGetValue("telp"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->telp;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set telp - telp
* @param string $telp
* @return \Pimcore\Model\DataObject\GetSolution
*/
public function setTelp ($telp) {
	$fd = $this->getClass()->getFieldDefinition("telp");
	$this->telp = $telp;
	return $this;
}

/**
* Get email - email
* @return string
*/
public function getEmail () {
	$preValue = $this->preGetValue("email"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->email;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set email - email
* @param string $email
* @return \Pimcore\Model\DataObject\GetSolution
*/
public function setEmail ($email) {
	$fd = $this->getClass()->getFieldDefinition("email");
	$this->email = $email;
	return $this;
}

/**
* Get solutionType - solutionType
* @return string
*/
public function getSolutionType () {
	$preValue = $this->preGetValue("solutionType"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->solutionType;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set solutionType - solutionType
* @param string $solutionType
* @return \Pimcore\Model\DataObject\GetSolution
*/
public function setSolutionType ($solutionType) {
	$fd = $this->getClass()->getFieldDefinition("solutionType");
	$this->solutionType = $solutionType;
	return $this;
}

/**
* Get solutionNeeds - solutionNeeds
* @return string
*/
public function getSolutionNeeds () {
	$preValue = $this->preGetValue("solutionNeeds"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->solutionNeeds;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set solutionNeeds - solutionNeeds
* @param string $solutionNeeds
* @return \Pimcore\Model\DataObject\GetSolution
*/
public function setSolutionNeeds ($solutionNeeds) {
	$fd = $this->getClass()->getFieldDefinition("solutionNeeds");
	$this->solutionNeeds = $solutionNeeds;
	return $this;
}

/**
* Get solutionFor - solutionFor
* @return string
*/
public function getSolutionFor () {
	$preValue = $this->preGetValue("solutionFor"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->solutionFor;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set solutionFor - solutionFor
* @param string $solutionFor
* @return \Pimcore\Model\DataObject\GetSolution
*/
public function setSolutionFor ($solutionFor) {
	$fd = $this->getClass()->getFieldDefinition("solutionFor");
	$this->solutionFor = $solutionFor;
	return $this;
}

/**
* Get notSure - Not Sure
* @return boolean
*/
public function getNotSure () {
	$preValue = $this->preGetValue("notSure"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->notSure;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set notSure - Not Sure
* @param boolean $notSure
* @return \Pimcore\Model\DataObject\GetSolution
*/
public function setNotSure ($notSure) {
	$fd = $this->getClass()->getFieldDefinition("notSure");
	$this->notSure = $notSure;
	return $this;
}

/**
* Get submitAt - submit_at
* @return string
*/
public function getSubmitAt () {
	$preValue = $this->preGetValue("submitAt"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->submitAt;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set submitAt - submit_at
* @param string $submitAt
* @return \Pimcore\Model\DataObject\GetSolution
*/
public function setSubmitAt ($submitAt) {
	$fd = $this->getClass()->getFieldDefinition("submitAt");
	$this->submitAt = $submitAt;
	return $this;
}

/**
* Get vehicleType - vehicleType
* @return string
*/
public function getVehicleType () {
	$preValue = $this->preGetValue("vehicleType"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->vehicleType;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set vehicleType - vehicleType
* @param string $vehicleType
* @return \Pimcore\Model\DataObject\GetSolution
*/
public function setVehicleType ($vehicleType) {
	$fd = $this->getClass()->getFieldDefinition("vehicleType");
	$this->vehicleType = $vehicleType;
	return $this;
}

/**
* Get productionYear - productionYear
* @return string
*/
public function getProductionYear () {
	$preValue = $this->preGetValue("productionYear"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->productionYear;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set productionYear - productionYear
* @param string $productionYear
* @return \Pimcore\Model\DataObject\GetSolution
*/
public function setProductionYear ($productionYear) {
	$fd = $this->getClass()->getFieldDefinition("productionYear");
	$this->productionYear = $productionYear;
	return $this;
}

/**
* Get plate - plate
* @return string
*/
public function getPlate () {
	$preValue = $this->preGetValue("plate"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->plate;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set plate - plate
* @param string $plate
* @return \Pimcore\Model\DataObject\GetSolution
*/
public function setPlate ($plate) {
	$fd = $this->getClass()->getFieldDefinition("plate");
	$this->plate = $plate;
	return $this;
}

/**
* Get vehiclePrice - vehiclePrice
* @return string
*/
public function getVehiclePrice () {
	$preValue = $this->preGetValue("vehiclePrice"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->vehiclePrice;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set vehiclePrice - vehiclePrice
* @param string $vehiclePrice
* @return \Pimcore\Model\DataObject\GetSolution
*/
public function setVehiclePrice ($vehiclePrice) {
	$fd = $this->getClass()->getFieldDefinition("vehiclePrice");
	$this->vehiclePrice = $vehiclePrice;
	return $this;
}

/**
* Get propertyCity - propertyCity
* @return string
*/
public function getPropertyCity () {
	$preValue = $this->preGetValue("propertyCity"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->propertyCity;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set propertyCity - propertyCity
* @param string $propertyCity
* @return \Pimcore\Model\DataObject\GetSolution
*/
public function setPropertyCity ($propertyCity) {
	$fd = $this->getClass()->getFieldDefinition("propertyCity");
	$this->propertyCity = $propertyCity;
	return $this;
}

/**
* Get propertyFunction - propertyFunction
* @return string
*/
public function getPropertyFunction () {
	$preValue = $this->preGetValue("propertyFunction"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->propertyFunction;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set propertyFunction - propertyFunction
* @param string $propertyFunction
* @return \Pimcore\Model\DataObject\GetSolution
*/
public function setPropertyFunction ($propertyFunction) {
	$fd = $this->getClass()->getFieldDefinition("propertyFunction");
	$this->propertyFunction = $propertyFunction;
	return $this;
}

protected static $_relationFields = array (
);

protected $lazyLoadedFields = array (
);

}

