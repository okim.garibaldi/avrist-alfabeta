<?php 

namespace Pimcore\Model\DataObject\LocationMessage;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\LocationMessage current()
 * @method DataObject\LocationMessage[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "90";
protected $className = "LocationMessage";


}
