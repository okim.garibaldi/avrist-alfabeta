<?php 

/** 
* Generated at: 2020-02-05T09:54:22+07:00
* Inheritance: no
* Variants: no
* Changed by: avristadm (2)
* IP: 172.17.0.1


Fields Summary: 
- name [input]
- email [email]
- phoneNumber [input]
- topik [select]
- time [select]
- message [textarea]
*/ 

namespace Pimcore\Model\DataObject;



/**
* @method static \Pimcore\Model\DataObject\CustomerSupportMessage\Listing getByName ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\CustomerSupportMessage\Listing getByEmail ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\CustomerSupportMessage\Listing getByPhoneNumber ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\CustomerSupportMessage\Listing getByTopik ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\CustomerSupportMessage\Listing getByTime ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\CustomerSupportMessage\Listing getByMessage ($value, $limit = 0) 
*/

class CustomerSupportMessage extends Concrete implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {



use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $o_classId = "89";
protected $o_className = "CustomerSupportMessage";
protected $name;
protected $email;
protected $phoneNumber;
protected $topik;
protected $time;
protected $message;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\CustomerSupportMessage
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get name - Name
* @return string
*/
public function getName () {
	$preValue = $this->preGetValue("name"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->name;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set name - Name
* @param string $name
* @return \Pimcore\Model\DataObject\CustomerSupportMessage
*/
public function setName ($name) {
	$fd = $this->getClass()->getFieldDefinition("name");
	$this->name = $name;
	return $this;
}

/**
* Get email - Email
* @return string
*/
public function getEmail () {
	$preValue = $this->preGetValue("email"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->email;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set email - Email
* @param string $email
* @return \Pimcore\Model\DataObject\CustomerSupportMessage
*/
public function setEmail ($email) {
	$fd = $this->getClass()->getFieldDefinition("email");
	$this->email = $email;
	return $this;
}

/**
* Get phoneNumber - Phone Number
* @return string
*/
public function getPhoneNumber () {
	$preValue = $this->preGetValue("phoneNumber"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->phoneNumber;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set phoneNumber - Phone Number
* @param string $phoneNumber
* @return \Pimcore\Model\DataObject\CustomerSupportMessage
*/
public function setPhoneNumber ($phoneNumber) {
	$fd = $this->getClass()->getFieldDefinition("phoneNumber");
	$this->phoneNumber = $phoneNumber;
	return $this;
}

/**
* Get topik - Topik
* @return string
*/
public function getTopik () {
	$preValue = $this->preGetValue("topik"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->topik;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set topik - Topik
* @param string $topik
* @return \Pimcore\Model\DataObject\CustomerSupportMessage
*/
public function setTopik ($topik) {
	$fd = $this->getClass()->getFieldDefinition("topik");
	$this->topik = $topik;
	return $this;
}

/**
* Get time - Waktu Yyang bisa dihubungi
* @return string
*/
public function getTime () {
	$preValue = $this->preGetValue("time"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->time;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set time - Waktu Yyang bisa dihubungi
* @param string $time
* @return \Pimcore\Model\DataObject\CustomerSupportMessage
*/
public function setTime ($time) {
	$fd = $this->getClass()->getFieldDefinition("time");
	$this->time = $time;
	return $this;
}

/**
* Get message - Message
* @return string
*/
public function getMessage () {
	$preValue = $this->preGetValue("message"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->message;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set message - Message
* @param string $message
* @return \Pimcore\Model\DataObject\CustomerSupportMessage
*/
public function setMessage ($message) {
	$fd = $this->getClass()->getFieldDefinition("message");
	$this->message = $message;
	return $this;
}

protected static $_relationFields = array (
);

protected $lazyLoadedFields = array (
);

}

