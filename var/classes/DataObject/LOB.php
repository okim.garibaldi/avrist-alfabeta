<?php 

/** 
* Generated at: 2019-06-20T11:39:22+07:00
* Inheritance: no
* Variants: no
* Changed by: avristadm (2)
* IP: 202.87.248.23


Fields Summary: 
- name [input]
- theme [input]
*/ 

namespace Pimcore\Model\DataObject;



/**
* @method static \Pimcore\Model\DataObject\LOB\Listing getByName ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\LOB\Listing getByTheme ($value, $limit = 0) 
*/

class LOB extends Concrete implements \Pimcore\Model\DataObject\DirtyIndicatorInterface {



use \Pimcore\Model\DataObject\Traits\DirtyIndicatorTrait;

protected $o_classId = "1";
protected $o_className = "LOB";
protected $name;
protected $theme;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\LOB
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get name - Name
* @return string
*/
public function getName () {
	$preValue = $this->preGetValue("name"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->name;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set name - Name
* @param string $name
* @return \Pimcore\Model\DataObject\LOB
*/
public function setName ($name) {
	$fd = $this->getClass()->getFieldDefinition("name");
	$this->name = $name;
	return $this;
}

/**
* Get theme - theme
* @return string
*/
public function getTheme () {
	$preValue = $this->preGetValue("theme"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->theme;
	if ($data instanceof \Pimcore\Model\DataObject\Data\EncryptedField) {
		    return $data->getPlain();
	}
	return $data;
}

/**
* Set theme - theme
* @param string $theme
* @return \Pimcore\Model\DataObject\LOB
*/
public function setTheme ($theme) {
	$fd = $this->getClass()->getFieldDefinition("theme");
	$this->theme = $theme;
	return $this;
}

protected static $_relationFields = array (
);

protected $lazyLoadedFields = array (
);

}

