<?php 

namespace Pimcore\Model\DataObject\Menu4;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\Menu4 current()
 * @method DataObject\Menu4[] load()
 */

class Listing extends DataObject\Listing\Concrete {

protected $classId = "70";
protected $className = "Menu4";


}
