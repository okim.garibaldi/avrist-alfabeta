<?php 

return [
    "thumbnail1" => [
        "items" => [
            [
                "method" => "resize",
                "arguments" => [
                    "width" => 1500,
                    "height" => 1000
                ]
            ]
        ],
        "medias" => [

        ],
        "name" => "thumbnail1",
        "description" => "",
        "group" => "",
        "format" => "SOURCE",
        "quality" => 85,
        "highResolution" => 0.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1562140527,
        "creationDate" => 1561624683,
        "id" => "thumbnail1"
    ]
];
