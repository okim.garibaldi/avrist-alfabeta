<?php 

return [
    "general" => [
        "timezone" => "Asia/Jakarta",
        "path_variable" => "",
        "domain" => "avrist-home.joranconsulting.com",
        "redirect_to_maindomain" => TRUE,
        "language" => "en",
        "validLanguages" => "en",
        "fallbackLanguages" => [
            "en" => ""
        ],
        "defaultLanguage" => "",
        "loginscreencustomimage" => "",
        "disableusagestatistics" => FALSE,
        "debug_admin_translations" => FALSE,
        "devmode" => FALSE,
        "instanceIdentifier" => "",
        "show_cookie_notice" => FALSE
    ],
    "database" => [
        "params" => [
            "username" => "pimcore_user",
            "password" => "Alfabeta43B@",
            "dbname" => "avrist",
            "host" => "localhost",
            "port" => "3306"
        ]
    ],
    "documents" => [
        "versions" => [
            "days" => NULL,
            "steps" => 2
        ],
        "default_controller" => "default",
        "default_action" => "default",
        "error_pages" => [
            "default" => "/page-not-found"
        ],
        "createredirectwhenmoved" => FALSE,
        "allowtrailingslash" => "no",
        "generatepreview" => TRUE
    ],
    "objects" => [
        "versions" => [
            "days" => NULL,
            "steps" => 4
        ]
    ],
    "assets" => [
        "versions" => [
            "days" => NULL,
            "steps" => 2
        ],
        "icc_rgb_profile" => "",
        "icc_cmyk_profile" => "",
        "hide_edit_image" => FALSE,
        "disable_tree_preview" => FALSE
    ],
    "services" => [
        "google" => [
            "client_id" => "",
            "email" => "",
            "simpleapikey" => ""
        ]
    ],
    "cache" => [
        "enabled" => FALSE,
        "lifetime" => NULL,
        "excludePatterns" => "",
        "excludeCookie" => ""
    ],
    "httpclient" => [
        "adapter" => "Socket",
        "proxy_host" => "",
        "proxy_port" => "",
        "proxy_user" => "",
        "proxy_pass" => ""
    ],
    "email" => [
        "sender" => [
            "name" => "Avrist",
            "email" => "noreply@avrist-home.alfabeta.co.id"
        ],
        "return" => [
            "name" => "",
            "email" => ""
        ],
        "method" => "smtp",
        "smtp" => [
            "host" => "smtp.sendgrid.net",
            "port" => "465",
            "ssl" => "ssl",
            "name" => "noreply@avrist-home.alfabeta.co.id",
            "auth" => [
                "method" => "login",
                "username" => "apikey",
                "password" => "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"
            ]
        ],
        "debug" => [
            "emailaddresses" => "mkemalw@gmail.com"
        ]
    ],
    "newsletter" => [
        "sender" => [
            "name" => "",
            "email" => ""
        ],
        "return" => [
            "name" => "",
            "email" => ""
        ],
        "method" => NULL,
        "smtp" => [
            "host" => "",
            "port" => "",
            "ssl" => NULL,
            "name" => "",
            "auth" => [
                "method" => NULL,
                "username" => "",
                "password" => NULL
            ]
        ],
        "debug" => NULL,
        "usespecific" => FALSE
    ],
    "branding" => [
        "color_login_screen" => "",
        "color_admin_interface" => ""
    ],
    "webservice" => [
        "enabled" => FALSE
    ],
    "applicationlog" => [
        "mail_notification" => [
            "send_log_summary" => FALSE,
            "filter_priority" => NULL,
            "mail_receiver" => ""
        ],
        "archive_treshold" => "30",
        "archive_alternative_database" => "avristadm"
    ]
];
