<?php 

return [
    1 => [
        "id" => 1,
        "name" => "get solution save record",
        "pattern" => "@/save\\-record@",
        "reverse" => "/get-solution/save-record",
        "module" => NULL,
        "controller" => "@AppBundle\\Controller\\GetSolution\\GetSolutionController",
        "action" => "saveRecord",
        "variables" => NULL,
        "defaults" => NULL,
        "siteId" => [

        ],
        "priority" => 1,
        "legacy" => FALSE,
        "creationDate" => 1549468357,
        "modificationDate" => 1549469717
    ],
    2 => [
        "id" => 2,
        "name" => "ask the expert submit",
        "pattern" => "@/submit@",
        "reverse" => "/ask-the-expert/submit",
        "module" => NULL,
        "controller" => "@AppBundle\\Controller\\AskTheExpertController",
        "action" => "submit",
        "variables" => NULL,
        "defaults" => NULL,
        "siteId" => [

        ],
        "priority" => 1,
        "legacy" => FALSE,
        "creationDate" => 1552727746,
        "modificationDate" => 1552728009
    ],
    3 => [
        "id" => 3,
        "name" => "product search",
        "pattern" => "@/product\\-search@",
        "reverse" => "/search/product-search",
        "module" => NULL,
        "controller" => "@AppBundle\\Controller\\Search\\SearchController",
        "action" => "productSearch",
        "variables" => "",
        "defaults" => NULL,
        "siteId" => [

        ],
        "priority" => 1,
        "legacy" => FALSE,
        "creationDate" => 1566263002,
        "modificationDate" => 1567137959
    ],
    5 => [
        "id" => 5,
        "name" => "save application",
        "pattern" => "@/save\\-application@",
        "reverse" => "/save-application",
        "module" => NULL,
        "controller" => "@AppBundle\\Controller\\Career\\CareerController",
        "action" => "saveApplication",
        "variables" => NULL,
        "defaults" => NULL,
        "siteId" => [

        ],
        "priority" => 1,
        "legacy" => FALSE,
        "creationDate" => 1573105342,
        "modificationDate" => 1573105786
    ],
    6 => [
        "id" => 6,
        "name" => "news detail",
        "pattern" => "/news-and-event\\/(.+)/",
        "reverse" => "/news-and-event/%path",
        "module" => NULL,
        "controller" => "@AppBundle\\Controller\\News\\NewsController",
        "action" => "newsDetail",
        "variables" => "path",
        "defaults" => NULL,
        "siteId" => [

        ],
        "priority" => 1,
        "legacy" => FALSE,
        "creationDate" => 1576043890,
        "modificationDate" => 1576116536
    ],
    7 => [
        "id" => 7,
        "name" => "csr detail",
        "pattern" => "/csr\\/(.+)/",
        "reverse" => "/csr/%path",
        "module" => NULL,
        "controller" => "@AppBundle\\Controller\\Csr\\CsrController",
        "action" => "detail",
        "variables" => "path",
        "defaults" => NULL,
        "siteId" => [

        ],
        "priority" => 1,
        "legacy" => FALSE,
        "creationDate" => 1576463046,
        "modificationDate" => 1576831110
    ],
    8 => [
        "id" => 8,
        "name" => "awards detail",
        "pattern" => "/awards\\/(.+)/",
        "reverse" => "/awards/%path",
        "module" => NULL,
        "controller" => "@AppBundle\\Controller\\Awards\\AwardsController",
        "action" => "detail",
        "variables" => "path",
        "defaults" => NULL,
        "siteId" => [

        ],
        "priority" => 1,
        "legacy" => FALSE,
        "creationDate" => 1576728346,
        "modificationDate" => 1576728527
    ],
    9 => [
        "id" => 9,
        "name" => "fund-fact detail",
        "pattern" => "/fund-fact\\/(.+)/",
        "reverse" => "/fund-fact/%path",
        "module" => NULL,
        "controller" => "@AppBundle\\Controller\\FundFactSheet\\FundFactSheetController",
        "action" => "detail",
        "variables" => "path",
        "defaults" => NULL,
        "siteId" => [

        ],
        "priority" => 1,
        "legacy" => FALSE,
        "creationDate" => 1576824332,
        "modificationDate" => 1576832747
    ],
    10 => [
        "id" => 10,
        "name" => "employee",
        "pattern" => "/\\/career\\/(.+)/",
        "reverse" => "/career/%path",
        "module" => NULL,
        "controller" => "@AppBundle\\Controller\\Career\\CareerController",
        "action" => "careerDetail",
        "variables" => "path",
        "defaults" => NULL,
        "siteId" => [

        ],
        "priority" => 1,
        "legacy" => FALSE,
        "creationDate" => 1578016557,
        "modificationDate" => 1578037169
    ],
    11 => [
        "id" => 11,
        "name" => "layanan",
        "pattern" => "/\\/layanan\\/(.+)/",
        "reverse" => "/layanan/%path",
        "module" => NULL,
        "controller" => "@AppBundle\\Controller\\LayananNasabah\\LayananNasabahController",
        "action" => "layananDetail",
        "variables" => "path",
        "defaults" => NULL,
        "siteId" => [

        ],
        "priority" => 1,
        "legacy" => FALSE,
        "creationDate" => 1578277219,
        "modificationDate" => 1578278483
    ],
    12 => [
        "id" => 12,
        "name" => "formulir layanan",
        "pattern" => "/\\/(formulir-layanan.+)/",
        "reverse" => "%path",
        "module" => NULL,
        "controller" => "@AppBundle\\Controller\\Formulir\\FormulirController",
        "action" => "formulirLayanan",
        "variables" => "path",
        "defaults" => NULL,
        "siteId" => [

        ],
        "priority" => 1,
        "legacy" => FALSE,
        "creationDate" => 1578376218,
        "modificationDate" => 1578378354
    ],
    13 => [
        "id" => 13,
        "name" => "life-guide",
        "pattern" => "/\\/(life-guide.+)/",
        "reverse" => "%path",
        "module" => NULL,
        "controller" => "@AppBundle\\Controller\\LifeGuide\\LifeGuideController",
        "action" => "detail",
        "variables" => "path",
        "defaults" => NULL,
        "siteId" => [

        ],
        "priority" => 1,
        "legacy" => FALSE,
        "creationDate" => 1578456012,
        "modificationDate" => 1578456242
    ],
    14 => [
        "id" => 14,
        "name" => "partnership",
        "pattern" => "/\\/partnership\\/(.+)/",
        "reverse" => "%path",
        "module" => NULL,
        "controller" => "@AppBundle\\Controller\\Partnership\\PartnershipController",
        "action" => "detail",
        "variables" => "path",
        "defaults" => NULL,
        "siteId" => [

        ],
        "priority" => 1,
        "legacy" => FALSE,
        "creationDate" => 1578550696,
        "modificationDate" => 1578550743
    ],
    16 => [
        "id" => 16,
        "name" => "email test",
        "pattern" => "@/email\\-test@",
        "reverse" => "/email-test/",
        "module" => NULL,
        "controller" => "Email\\Email",
        "action" => "send",
        "variables" => NULL,
        "defaults" => NULL,
        "siteId" => [

        ],
        "priority" => 1,
        "legacy" => FALSE,
        "creationDate" => 1579584986,
        "modificationDate" => 1579588005
    ],
    17 => [
        "id" => 17,
        "name" => "email test",
        "pattern" => "@/summary\\-send\\-email@",
        "reverse" => "/summary-send-email",
        "module" => NULL,
        "controller" => "@AppBundle\\Controller\\Email\\EmailController",
        "action" => "sendSummaryEmail",
        "variables" => NULL,
        "defaults" => NULL,
        "siteId" => [

        ],
        "priority" => 1,
        "legacy" => FALSE,
        "creationDate" => 1580208979,
        "modificationDate" => 1580211675
    ],
    18 => [
        "id" => 18,
        "name" => "career-search",
        "pattern" => "@/career-search@",
        "reverse" => "/career-search/",
        "module" => NULL,
        "controller" => "@AppBundle\\Controller\\Career\\CareerController",
        "action" => "careerSearch",
        "variables" => "",
        "defaults" => NULL,
        "siteId" => [

        ],
        "priority" => 1,
        "legacy" => FALSE,
        "creationDate" => 1580611183,
        "modificationDate" => 1580618196
    ],
    19 => [
        "id" => 19,
        "name" => "location-message",
        "pattern" => "@/location-message@",
        "reverse" => "/location-message/",
        "module" => NULL,
        "controller" => "@AppBundle\\Controller\\Location\\LocationController",
        "action" => "locationMessage",
        "variables" => "",
        "defaults" => NULL,
        "siteId" => [

        ],
        "priority" => 1,
        "legacy" => FALSE,
        "creationDate" => 1581392690,
        "modificationDate" => 1581392788
    ],
    20 => [
        "id" => 20,
        "name" => "gallery",
        "pattern" => "/gallery\\/(.+)/",
        "reverse" => "/gallery/%path",
        "module" => NULL,
        "controller" => "@AppBundle\\Controller\\Gallery\\GalleryController",
        "action" => "detail",
        "variables" => "path",
        "defaults" => NULL,
        "siteId" => [

        ],
        "priority" => 1,
        "legacy" => FALSE,
        "creationDate" => 1583559220,
        "modificationDate" => 1583563318
    ],
    21 => [
        "id" => 21,
        "name" => "",
        "pattern" => NULL,
        "reverse" => NULL,
        "module" => NULL,
        "controller" => NULL,
        "action" => NULL,
        "variables" => NULL,
        "defaults" => NULL,
        "siteId" => NULL,
        "priority" => 0,
        "legacy" => FALSE,
        "creationDate" => 1584610510,
        "modificationDate" => 1584610510
    ],
    22 => [
        "id" => 22,
        "name" => "",
        "pattern" => NULL,
        "reverse" => NULL,
        "module" => NULL,
        "controller" => NULL,
        "action" => NULL,
        "variables" => NULL,
        "defaults" => NULL,
        "siteId" => NULL,
        "priority" => 0,
        "legacy" => FALSE,
        "creationDate" => 1584610519,
        "modificationDate" => 1584610519
    ],
    23 => [
        "id" => 23,
        "name" => "",
        "pattern" => NULL,
        "reverse" => NULL,
        "module" => NULL,
        "controller" => NULL,
        "action" => NULL,
        "variables" => NULL,
        "defaults" => NULL,
        "siteId" => NULL,
        "priority" => 0,
        "legacy" => FALSE,
        "creationDate" => 1584610540,
        "modificationDate" => 1584610540
    ]
];
