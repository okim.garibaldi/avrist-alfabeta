<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

use Pimcore\Model\Document;
use Pimcore\Model\Document\Page;


$this->extend('layout_avrist.html.php');
$prefix = "http://avrist.dnatechnology.co.id";?>

<section class="cover cover-landing-page">
    <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/pattern/ic_ornamen_yellow.png')?>" class="pattern-yellow" alt="Pattern Yellow">
    <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/pattern/ic_ornamen_yellow.png')?>" class="pattern-yellow-1" alt="Pattern Yellow">
    <div class="container content-center">
        <div class="row w-100">
            <div class="col-md-6">
                <h2><?= $this->product->getName() ?></h2>
                <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/icon/ic_age_white.png')?>" class="img-fluid icon-small"><p>17 - 24 Tahun</p>
                <!--                <p></p>-->
            </div>
            <div class="col-md-6 text-md-right">
                <a href="#tanyaAhli" class="smooth-scroll btn btn-transparent mr-2">Tanya Ahlinya</a>
                <a href="#getQuote" class="smooth-scroll btn btn-transparent mr-2">Dapatkan Produk Ini</a>
            </div>
        </div>
    </div>
</section>

<section class="py-main section-insurance-tab">
    <div class="container">
        <ul class="nav nav-tabs insurance-tab" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link content-center active" id="tentang-tab" data-toggle="tab" href="#tentang" role="tab" aria-controls="tentang" aria-selected="true">Tentang</a>
            </li>
            <li class="nav-item">
                <a class="nav-link content-center" id="manfaat-tab" data-toggle="tab" href="#manfaat" role="tab" aria-controls="manfaat" aria-selected="false">Manfaat</a>
            </li>
            <li class="nav-item">
                <a class="nav-link content-center" id="syarat-tab" data-toggle="tab" href="#syarat" role="tab" aria-controls="syarat" aria-selected="false">Syarat & Prosedur</a>
            </li>
        </ul>
    </div>
</section>

<!-- <section class="py-main section-insurance-content">
  <div class="content" id="testingContent"></div>
</section> -->

<div class="tab-content" id="myTabContent">
    <div class="tab-pane fade show active" id="tentang" role="tabpanel" aria-labelledby="tentang-tab">
        <section class="py-main section-investment-tentang">
            <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/pattern/ornamen_waves_layer3_grey.png')?>" class="pattern-wave-grey" alt="Pattern Gray">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 content-center">
                        <div class="heading">
                            <h2>Introduksi</h2>
                            <p><?= $this->product->getSummary() ?></p>
                        </div>
                    </div>
                    <div class="col-md-6 text-center">
                        <img src="<?= $this->product->getImageDetail() ?>" class="img-fluid vp-fadeinleft" alt="Why Us">
                    </div>
                </div>
            </div>
        </section>

        <section class="py-main section-investment-tentang-2">
            <div class="container">
                <div class="heading">
                    <h2>Keunggulan</h2>
                </div>
                <div class="content mt-5">
                    <div class="row">

                        <?= $this->product->getBenefits() ?>

                        <div class="col-md-12 mt-4">
                            <small>* Perhitungan harga unit dilakukan setiap hari kerja berdasarkan nilai dana investasi dibagi jumlah semua unit yang dibentuk dari suatu dana investasi</small>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <section class="py-main section-investment-tentang-3">
            <div class="container">
                <div class="heading-w-link">
                    <h2>Ketahuilah Nilai Investasimu & Kurangi Resikonya</h2>
                    <div class="row">
                        <div class="col-md-6">
                            <p class="subtitle">Quisque felis orci, pharetra a ullamcorper et, feugiat vitae arcu. Vivamus sit amet placerat dolor. Ut dignissim mauris id elit accumsan,</p>
                        </div>
                        <div class="col-md-6 text-md-right">
                            <a href="#" class="btn btn-primary">Ilustrasi manfaat investasi</a>
                        </div>
                    </div>
                </div>

                <div class="content mt-5">
                    <div class="row row-0">
                        <div class="col-md-4">
                            <div class="nav flex-column nav-pills nav-customize" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                <a class="nav-link active" id="v-pills-low-risk-tab" data-toggle="pill" href="#v-pills-low-risk" role="tab" aria-controls="v-pills-low-risk" aria-selected="true">low-risk</a>
                                <a class="nav-link" id="v-pills-medium-risk-tab" data-toggle="pill" href="#v-pills-medium-risk" role="tab" aria-controls="v-pills-medium-risk" aria-selected="false">medium-risk</a>
                                <a class="nav-link" id="v-pills-high-risk-tab" data-toggle="pill" href="#v-pills-high-risk" role="tab" aria-controls="v-pills-high-risk" aria-selected="false">high-risk</a>
                            </div>
                        </div>
                        <div class="col-md-8 content-center">
                            <div class="tab-content tab-content-nav" id="v-pills-tabContent">
                                <div class="tab-pane fade show active" id="v-pills-low-risk" role="tabpanel" aria-labelledby="v-pills-low-risk-tab">
                                    <h5>Avrist Link Assured (IDR)</h5>
                                    <p>Dikelola untuk menghasilkan pendapatan yang stabil yang diterbitkan oleh para pihak di Indonesia (biaya per tahun 1,50% atas dana yang dikelola).</p>

                                    <h5>Avrist Link Advised (IDR)</h5>
                                    <p>Dana investasi ini dikelola untuk menghasilkan pendapatan dalam jangka panjang terutama dalam mata uang Rupiah dan saham perusahaan-perusahaan Indonesia yang tercatat di Bursa Efek Indonesia (biaya per tahun 2% atas dana yang dikelola).</p>

                                    <h5>Avrist Link Assured (USD)</h5>
                                    <p>Dana investasi ini dikelola untuk menghasilkan pendapatan yang stabil dalam mata uang USD yang diterbitkan oleh para pihak di Indonesia dan luar negeri (biaya per tahun 1,5% atas dana yang dikelola).</p>
                                </div>
                                <div class="tab-pane fade" id="v-pills-medium-risk" role="tabpanel" aria-labelledby="v-pills-medium-risk-tab">
                                    <h5>2.Avrist Link Assured (IDR)</h5>
                                    <p>Dikelola untuk menghasilkan pendapatan yang stabil yang diterbitkan oleh para pihak di Indonesia (biaya per tahun 1,50% atas dana yang dikelola).</p>

                                    <h5>Avrist Link Advised (IDR)</h5>
                                    <p>Dana investasi ini dikelola untuk menghasilkan pendapatan dalam jangka panjang terutama dalam mata uang Rupiah dan saham perusahaan-perusahaan Indonesia yang tercatat di Bursa Efek Indonesia (biaya per tahun 2% atas dana yang dikelola).</p>

                                    <h5>Avrist Link Assured (USD)</h5>
                                    <p>Dana investasi ini dikelola untuk menghasilkan pendapatan yang stabil dalam mata uang USD yang diterbitkan oleh para pihak di Indonesia dan luar negeri (biaya per tahun 1,5% atas dana yang dikelola).</p>
                                </div>
                                <div class="tab-pane fade" id="v-pills-high-risk" role="tabpanel" aria-labelledby="v-pills-high-risk-tab">
                                    <h5>3. Avrist Link Assured (IDR)</h5>
                                    <p>Dikelola untuk menghasilkan pendapatan yang stabil yang diterbitkan oleh para pihak di Indonesia (biaya per tahun 1,50% atas dana yang dikelola).</p>

                                    <h5>Avrist Link Advised (IDR)</h5>
                                    <p>Dana investasi ini dikelola untuk menghasilkan pendapatan dalam jangka panjang terutama dalam mata uang Rupiah dan saham perusahaan-perusahaan Indonesia yang tercatat di Bursa Efek Indonesia (biaya per tahun 2% atas dana yang dikelola).</p>

                                    <h5>Avrist Link Assured (USD)</h5>
                                    <p>Dana investasi ini dikelola untuk menghasilkan pendapatan yang stabil dalam mata uang USD yang diterbitkan oleh para pihak di Indonesia dan luar negeri (biaya per tahun 1,5% atas dana yang dikelola).</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix mt-4">
                        <small>Setiap investasi memiliki risiko yang berbeda berdasarkan profil risiko produk</small>
                    </div>
                </div>
            </div>
        </section>

        <section class="py-main section-investment-tentang-4" id="getQuote">
            <div class="container">
                <div class="heading">
                    <h2>Detail Premi</h2>
                    <p>Cras quis nulla commodo, aliquam lectus sed, blandit augue. Cras ullamcorper bibendum bibendum duis tincidunt</p>
                    <a href="#" class="link-arrow">
                        LIHAT DETAIL MANFAAT <i class="fas fa-chevron-right"></i>
                    </a>
                </div>

                <div class="content mt-5">
                    <div class="card card-premi p-box">
                        <div class="row">
                            <div class="col-md-6">
                                USIA MASUK
                                <div class="slider-wrapper">
                                    <div id='volume' class='slider'>
                                        <output class='slider-output'>25thn</output>
                                        <div class='slider-track'>
                                            <div class="slider-thumb"></div>
                                            <div class='slider-level'></div>
                                        </div>
                                        <input class='slider-input' type='range' value='25' min='0' max='100' />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                JENIS KELAMIN
                                <div class="row row-1 mt-4">
                                    <div class="col-6 col-sm-6 col-md-4">
                                        <a href="#" class="btn btn-gender btn-block active" id="genderLaki">Laki Laki</a>
                                    </div>
                                    <div class="col-6 col-sm-6 col-md-4">
                                        <a href="#" class="btn btn-gender btn-block" id="genderPerempuan">Perempuan</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 text-center mt-4">
                                <a href="#" class="btn btn-secondary" id="hitungBtn">Hitung</a>
                            </div>
                        </div>
                    </div>
                    <div class="card card-premi card-premi-hitung p-box" id="boxCalcPremi">
                        <div class="col-md-12 text-center mb-4 mt-4">
                            <p>PREMI PER TAHUN</p>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="card card-premi card-premi-hasil">
                                    <p class="mb-1">Plan A</p>
                                    <h5>Rp<?= $this->premiumFactor->getPlanA() ?></h5>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="card card-premi card-premi-hasil">
                                    <p class="mb-1">Plan B</p>
                                    <h5>Rp<?= $this->premiumFactor->getPlanB() ?></h5>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="card card-premi card-premi-hasil">
                                    <p class="mb-1">Plan C</p>
                                    <h5>Rp<?= $this->premiumFactor->getPlanC() ?></h5>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="card card-premi card-premi-hasil">
                                    <p class="mb-1">Plan D</p>
                                    <h5>Rp<?= $this->premiumFactor->getPlanD() ?></h5>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="card card-premi card-premi-hasil">
                                    <p class="mb-1">Plan E</p>
                                    <h5>Rp<?= $this->premiumFactor->getPlanE() ?></h5>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="card card-premi card-premi-hasil">
                                    <p class="mb-1">Plan F</p>
                                    <h5>Rp<?= $this->premiumFactor->getPlanF() ?></h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="py-main section-investment-tentang-5">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 position-relative">
                        <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/pattern/ic_ornamen_yellow.png')?>" class="pattern-yellow" alt="Ornamen Yellow">
                        <div class="heading">
                            <h2>Selalu bersama Anda</h2>
                            <p class="subtitle">Kami selalu melindungi Anda. Belum menemukan jawaban? Cari berbagai topik yang sering ditanyakan di  Frequently Asked Questions</p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div id="accordionFAQ" class="accordion accordion-faq vp-fadeinup delayp2">
                            <div class="accordion-item">
                                <div class="accordion-header" id="headingOne" data-toggle="collapse" data-target="#collapseFAQ1" aria-expanded="true" aria-controls="collapseFAQ1">
                                    <h5>Apa beda asuransi jiwa dan kesehatan?
                                        <span class="close-panel"></span>
                                    </h5>
                                </div>
                                <div id="collapseFAQ1" class="accordion-body collapse show" aria-labelledby="headingOne" data-parent="#accordionFAQ">
                                    <p class="pl-0">Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris a nisi sed velit pulvinar gravida. Vivamus blandit cursus enim, a pulvinar dui hendrerit at. Class aptent taciti sociosqu ad litora torquent per conubia nostra. </p>
                                </div>
                            </div>
                            <div class="accordion-item">
                                <div class="accordion-header" id="headingFAQ2" data-toggle="collapse" data-target="#collapseFAQ2" aria-expanded="false" aria-controls="collapseFAQ2">
                                    <h5>Bagaimana cara mendapatkan rekomendasi produk yang paling cocok bagi saya?
                                        <span class="close-panel"></span>
                                    </h5>
                                </div>
                                <div id="collapseFAQ2" class="accordion-body collapse" aria-labelledby="headingFAQ2" data-parent="#accordionFAQ">
                                    <p class="pl-0">Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris a nisi sed velit pulvinar gravida. Vivamus blandit cursus enim, a pulvinar dui hendrerit at. Class aptent taciti sociosqu ad litora torquent per conubia nostra.  </p>
                                </div>
                            </div>
                            <div class="accordion-item">
                                <div class="accordion-header" id="headingFAQ3" data-toggle="collapse" data-target="#collapseFAQ3" aria-expanded="false" aria-controls="collapseFAQ3">
                                    <h5>Bagaimana menanggulangi resiko investasi?
                                        <span class="close-panel"></span>
                                    </h5>
                                </div>
                                <div id="collapseFAQ3" class="accordion-body collapse" aria-labelledby="headingFAQ3" data-parent="#accordionFAQ">
                                    <p class="pl-0">Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris a nisi sed velit pulvinar gravida. Vivamus blandit cursus enim, a pulvinar dui hendrerit at. Class aptent taciti sociosqu ad litora torquent per conubia nostra.  </p>
                                </div>
                            </div>
                            <div class="accordion-item">
                                <div class="accordion-header" id="headingFAQ4" data-toggle="collapse" data-target="#collapseFAQ4" aria-expanded="false" aria-controls="collapseFAQ4">
                                    <h5>Apa bedanya asuransi jiwa dan kesehatan?
                                        <span class="close-panel"></span>
                                    </h5>
                                </div>
                                <div id="collapseFAQ4" class="accordion-body collapse" aria-labelledby="headingFAQ4" data-parent="#accordionFAQ">
                                    <p class="pl-0">Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris a nisi sed velit pulvinar gravida. Vivamus blandit cursus enim, a pulvinar dui hendrerit at. Class aptent taciti sociosqu ad litora torquent per conubia nostra.  </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="py-main section-investment-tentang-6" id="tanyaAhli">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="heading">
                            <h2>Tanya pada ahlinya</h2>
                            <p class="subtitle">Bingung menentukan pilihan? Atau ingin mengetahui detail solusi yang kamu inginkan? Dapatkan saran terbaik via e-mail, atau Kunjungi Cabang Terdekat</p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <form class="needs-validation" novalidate>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="">NAMA LENGKAP</label>
                                        <input type="text" class="form-control" id="name-ask" placeholder="" value="" required>
                                        <div class="invalid-feedback">
                                            Please enter first name
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="">EMAIL ADDRESS</label>
                                        <input type="text" class="form-control" id="email-ask" placeholder="" value="" required>
                                        <div class="invalid-feedback">
                                            Please enter last name
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="">MESSAGE</label>
                                        <textarea class="form-control" id="message-ask" rows="5" style="resize: none;"></textarea>
                                        <div class="invalid-feedback">
                                            Please enter last name
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <button class="btn btn-primary float-right btn-submit-ask" href="javascript:;">Submit form</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="tab-pane fade" id="manfaat" role="tabpanel" aria-labelledby="manfaat-tab">
        <section class="py-main section-investment-tentang-2">
            <div class="container">
                <div class="heading">
                    <h2>Manfaat Yang Didapat</h2>
                    <p>Duis aute irure dolor in reprehenderit in voluptate velit esse
                        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                </div>
                <div class="content mt-5">
                    <div class="row">
                        <?php foreach ($this->benefits as $benefit){?>
                            <div class="col-md-4">
                                <div class="content">
                                    <h4><?= $benefit->getTitle() ?></h4>
                                    <p><?= $benefit->getDescription() ?></p>
                                </div>
                            </div>
                        <?php } ?>

                        <div class="col-md-12 mt-4">
                            <a href="#" class="link-arrow">
                                LIHAT ESTIMASI PREMI
                                <i class="fa fa-chevron-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="bg-light py-main section-life-insurance-5" style="background: url(<?php echo $prefix;?>assets/img/common/img_banner_solution.jpg) no-repeat center; background-size: cover;">
            <div class="container">
                <div class="row">
                    <div class="col-md-7">
                        <h2>Ingin tau lebih lanjut?<a href="#">Download Brosur</a></h2>
                    </div>
                </div>
            </div>
        </section>

        <section class="py-main section-investment-tentang-2">
            <div class="container">
                <div class="heading">
                    <h2>Asuransi Rider</h2>
                    <p>Duis aute irure dolor in reprehenderit in voluptate velit esse
                        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                </div>
                <div class="content mt-5">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="content">
                                <h4>Accidental Death and Dismenberment (ADD)</h4>
                                <p>Ut maximus sollicitudin odio vel ullamcorper. Maecenas congue est eget magna blandit, vitae fringilla odio pulvinar. Ut maximus sollicitudin odio vel ullamcorper. Maecenas congue est eget magna blandit, vitae fringilla odio pulvinar.</p>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="content">
                                <h4>WP (Waiver of Premium)</h4>
                                <p>Ut maximus sollicitudin odio vel ullamcorper. Maecenas congue est eget magna blandit, vitae fringilla odio pulvinar. Ut maximus sollicitudin odio vel ullamcorper. Maecenas congue est eget magna blandit, vitae fringilla odio pulvinar.</p>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="content">
                                <h4>Payor of Benefit (PB)</h4>
                                <p>Ut maximus sollicitudin odio vel ullamcorper. Maecenas congue est eget magna blandit, vitae fringilla odio pulvinar. Ut maximus sollicitudin odio vel ullamcorper. Maecenas congue est eget magna blandit, vitae fringilla odio pulvinar.</p>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="content">
                                <h4>Hospital & Surgical (H&S)</h4>
                                <p>Ut maximus sollicitudin odio vel ullamcorper. Maecenas congue est eget magna blandit, vitae fringilla odio pulvinar. Ut maximus sollicitudin odio vel ullamcorper. Maecenas congue est eget magna blandit, vitae fringilla odio pulvinar.</p>
                            </div>
                        </div>

                        <div class="col-md-12 mt-4">
                            <a href="#" class="link-arrow">
                                Tabel Perhitungan Pertanggungjawaban
                                <i class="fa fa-chevron-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="tab-pane fade" id="syarat" role="tabpanel" aria-labelledby="syarat-tab">
        <section class="py-main section-investment-tentang-2">
            <div class="container">
                <div class="heading">
                    <h2>Syarat & Ketentuan</h2>
                    <p><?= $this->product->getTermsAndProcedures() ?></p>
                </div>
                <div class="content mt-5">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="content">
                                <h4>Age Range</h4>
                                <p>Apply before 50 years old</p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="content">
                                <h4>Indonesia/Foreigner</h4>
                                <p>For Indonesian, you must have a valid KTP or if you are a foreigner, you need too have KITAS as an identity</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

<script>

    var volumeSlider = document.getElementById('volume');
    var sliders = [volumeSlider];


    //******************************START SLIDER LOGIC******************************
    function Slider(slider) {
        this.slider = slider;
        slider.addEventListener('input', function() {
            this.updateSliderOutput();
            this.updateSliderLevel();
        }.bind(this), false);

        this.level = function() {
            var level = this.slider.querySelector('.slider-input');
            return level.value;
        }

        this.levelString = function() {
            return parseInt(this.level());
        }

        this.remaining = function() {
            return 99.5 - this.level();
        }

        this.remainingString = function() {
            return parseInt(this.remaining());
        }

        this.updateSliderOutput = function() {
            var output = this.slider.querySelector('.slider-output');
            var remaining = this.slider.querySelector('.slider-remaining');
            var thumb = this.slider.querySelector('.slider-thumb');
            output.value = this.levelString() + 'thn';
            output.style.left = this.levelString() + '%';
            thumb.style.left = this.levelString() + '%';
            if (remaining) {
                remaining.style.width = this.remainingString() + '%';
            }
        }

        this.updateSlider = function(num) {
            var input = this.slider.querySelector('.slider-input');
            input.value = num;
        }

        this.updateSliderLevel =function() {
            var level = this.slider.querySelector('.slider-level');
            level.style.width = this.levelString() + '%';
        }
    }

    sliders.forEach(function(slider) {
        new Slider(slider);
    });
    //********************************END SLIDER LOGIC*****************************************

</script>
