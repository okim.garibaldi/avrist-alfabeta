<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

use Pimcore\Model\Document;
use Pimcore\Model\Document\Page;

$this->extend('layout_avrist.html.php');
$prefix = "http://avrist.dnatechnology.co.id";?>

<section class="py-main cover cover-landing-page">
    <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/pattern/ic_ornamen_yellow.png')?>" class="pattern-yellow" alt="Pattern Yellow">
    <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/pattern/ic_ornamen_yellow.png')?>" class="pattern-yellow-1" alt="Pattern Yellow">
    <div class="container content-center text-center">
        <form class="form-cover">
            <h2>Saya butuh asuransi</h2>
            <div class="dropdown-custom form-group">
                <div class="dropdown-toggle form-control" data-toggle="dropdown" id="select-value">
                    Employee
                </div>
                <div class="dropdown-menu">
                    <div class="selection-value dropdown-item" data-select="0"><a href="/life-insurance/personal/life">Life</div>
                    <div class="selection-value dropdown-item" data-select="1"><a href="/life-insurance/personal/syariah">Syariah</div>
                    <div class="selection-value dropdown-item" data-select="2"><a href="/life-insurance/personal/health">Health</div>
                    <div class="selection-value dropdown-item" data-select="3"><a href="/life-insurance/personal/education">Education</div>
                    <div class="selection-value dropdown-item" data-select="4"><a href="/life-insurance/business/employee">Employee</div>
                    <div class="selection-value dropdown-item" data-select="5"><a href="/life-insurance/business/retirement">Retirement</div>
                </div>
            </div>
        </form>
        <!-- <h2>Saya butuh asuransi </h2> -->
        <div><p><?= $this->category->getDescription() ?></p></div>

    </div>
</section>

<section class="py-main section-insurance-life">
    <div class="container">
        <div class="row">

            <?php foreach ($this->product as $p){?>
                <div class="col-md-4">
                    <a href="<?= $p->getPage()?>" class="card card-news-insurance">
                        <div class="card-img-top" style="background: url(<?php echo $p->getImage(); ?>) no-repeat center; background-size: cover;"></div>
                        <div class="card-body">
                            <p class="card-label">Life insurance</p>
                            <h5 class="card-title text-truncate"><?= $p->getName()?></h5>
                            <p class="card-text text-truncate-multiline">
                                <?= str_replace(['<p>', '</p>'], ['<br>', '</br>'], $p->getSummary())?>
                            </p>
                            <!-- <a href="#" class="card-link">Lihat detail</a> -->
                        </div>
                    </a>
                </div>
            <?php } ?>

        </div>
    </div>
<script>
    //dropdown
    let url = window.location.href.split('?')[0];
    let dp = url.substr(url.lastIndexOf('/') + 1);
    document.getElementById('select-value').innerText = dp.charAt(0).toUpperCase() + dp.substr(1);

    //dropdown custom
    $(".selection-value").click(function(e) {
        const selected = e.target.dataset.select;
        switch(selected) {
            case '0':
                $('#select-value').html('Karyawan');
                break;
            case '1':
                $('#select-value').html('Pensiun');
                break;
        }
    });
</script>