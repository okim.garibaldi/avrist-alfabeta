<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

use Pimcore\Model\Document;
use Pimcore\Model\Document\Page;

$this->extend('layout_avrist.html.php');
$prefix = "http://avrist.dnatechnology.co.id";?>

<section class="py-main section-news">
    <div class="container">
        <div class="row">

            <?php foreach ($this->product as $p){?>
                <div class="col-md-4">
                    <a href="<?= $p->getPage()?>" class="card card-news-insurance">
                        <div class="card-img-top" style="background: url(<?php echo $p->getImage(); ?>) no-repeat center; background-size: cover;"></div>
                        <div class="card-body" style="max-height: 145px">
                            <p class="card-label">Avrist Life - Individu</p>
                            <h5 class="card-title"><?= $p->getName()?></h5>
                            <div class="card-text text-truncate-twoline">
                                <?= $p->getSummary()?>
                            </div>
                            <!-- <a href="#" class="card-link">Lihat detail</a> -->
                        </div>
                    </a>
                </div>
            <?php } ?>

        </div>
    </div>
</section>
