<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

use Pimcore\Model\Document;
use Pimcore\Model\Document\Page;
?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
    <?php $prefix="178.128.115.24/";?>
    <?php //$subPrefix="http://am.avrist.dnatechnology.co.id";?>
    <?php $subPrefix="http://178.128.115.24/asset-management";?>
    <?= $this->template("include/head.html.php"); ?>
    <?= $this->template("include/tracking-script/head.html.php"); ?>
    <style>
        .navbar.fixed-top + * {
            margin-top: 0;
        }
    </style>
</head>
<body>
<?= $this->template("include/tracking-script/body.html.php"); ?>

<div class="loader-wrapper loader-light">
    <div class="loader"></div>
</div>

<?= $this->template("include/navbar2html.php"); ?>

<!--Start: Cover Responsive-->
<div class="cover cover-responsive cover-life-insurance">
    <div class="cover-responsive-img">
        <div class="cover-responsive-overlay"></div>
    </div>
    <div class="container position-relative justify-content-center">
        <div class="cover-content text-center">
            <h3 class="cover-title animated fadeInUp delayp1"><?= $this->category->getName() ?></h3>
            <p><?= $this->category->getDescription() ?></p>
        </div>
    </div>
    <a href="#" class="scroll-hint bottom-0 animated fadeInDown delayp4">
        <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/common/arrow_down.png')?>" alt="Arrow down hint"/>
    </a>
</div>
<!--End: Cover Responsive-->

<section class="py-main section-life-insurance-1">
    <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/pattern/ornamen_waves_layer2_grey.png')?>" class="pattern-grey" alt="Gray Waves">
    <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/pattern/ic_ornamen_yellow.png')?>" class="pattern-yellow" alt="Gray Waves">
    <div class="container">
        <div class="heading vp-fadeinup delayp2">
            <h2>Tentang</h2>
            <p class="col-md-8 p-0 subtitle"><?= $this->category->getDescription() ?></p>
        </div>

        <div class="content mt-5">
            <div class="row">
                <div class="col-6 col-md-6 vp-fadeinup delayp1">
                    <a href="" class="card p-box card-insurance">
                        <div class="card-img">
                            <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/icon/ic_illustrasi_personal.png')?>" class="img-fluid">
                            <h5 class="card-title">Fund Fact Sheet</h5>
                        </div>
                    </a>
                </div>
                <div class="col-6 col-md-6 vp-fadeinup delayp1">
                    <a href="" class="card p-box card-insurance">
                        <div class="card-img">
                            <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/icon/ic_illustrasi_bisnis.png')?>" class="img-fluid">
                            <h5 class="card-title">Prospectrus</h5>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="py-main section-life-insurance-2">
    <div class="container">
        <div class="heading vp-fadeinup delayp1">
            <h2>Harga unit</h2>
            <p class="subtitle col-md-8 p-0">Update tanggal 2 Oktober 2018</p>
        </div>
        <div class="content">
            <div class="filter">
                <div class="row">
                    <div class="col-md-5">
                        <div class="form-group">
                            <label for="" style="color: transparent;">aasdasdas</label>
                            <input type="text" class="form-control" id="" placeholder="Cari dengan kata kunci" value="" required>
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="">Bandingkan ditanggal</label>
                            <input type="text" class="form-control form-control-datepicker" placeholder="(MM/DD/YYYY)" id="dateFilter" required="">
                            <div class="invalid-feedback">
                                Please input date
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="">Filter</label>
                            <div class="form-group">
                                <select class="custom-select" id="validationCustom04" required>
                                    <option value="">Lihat Semua</option>
                                    <option value="1">Pilihan 1</option>
                                    <option value="2">Pilihan 2</option>
                                    <option value="3">Pilihan 3</option>
                                </select>
                                <div class="invalid-feedback">
                                    Please choose correctly
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">Product Name</th>
                        <th scope="col">Price on 31/07/2018</th>
                        <th scope="col">Price on 01/08/2018</th>
                        <th scope="col">Status</th>
                        <th scope="col"></th>
                    </tr>
                    </thead>
                    <tbody class="text-center">
                    <tr>
                        <th scope="row" class="text-left">Avrist Protected Fund 1</th>
                        <td>750.00</td>
                        <td>1,000.00</td>
                        <td><span class="text-green"><i class="fas fa-arrow-up mr-1"></i>0.25</span></td>
                        <td>Investasi Sekarang</td>
                    </tr>
                    <tr>
                        <th scope="row" class="text-left">Avrist DanaTerproteksi Spirit 1</th>
                        <td>1,250.00</td>
                        <td>1,000.00</td>
                        <td><span class="text-red"><i class="fas fa-arrow-down mr-1"></i>0.25</span></td>
                        <td>Investasi Sekarang</td>
                    </tr>
                    <tr>
                        <th scope="row" class="text-left">Avrist Ada Saham Blue Safir</th>
                        <td>750.00</td>
                        <td>1,000.00</td>
                        <td><span class="text-green"><i class="fas fa-arrow-up mr-1"></i>0.25</span></td>
                        <td>Investasi Sekarang</td>
                    </tr>
                    <tr>
                        <th scope="row" class="text-left">Avrist Protected Fund 2</th>
                        <td>750.00</td>
                        <td>1,000.00</td>
                        <td><span class="text-green"><i class="fas fa-arrow-up mr-1"></i>0.25</span></td>
                        <td>Investasi Sekarang</td>
                    </tr>
                    <tr>
                        <th scope="row" class="text-left">Avrist DanaTerproteksi Spirit 2</th>
                        <td>1,250.00</td>
                        <td>1,000.00</td>
                        <td><span class="text-red"><i class="fas fa-arrow-down mr-1"></i>0.25</span></td>
                        <td>Investasi Sekarang</td>
                    </tr>
                    <tr>
                        <th scope="row" class="text-left">Avrist Ada Saham Blue Safir 2</th>
                        <td>750.00</td>
                        <td>1,000.00</td>
                        <td><span class="text-green"><i class="fas fa-arrow-up mr-1"></i>0.25</span></td>
                        <td>Investasi Sekarang</td>
                    </tr>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>

<section class="bg-light py-main section-life-insurance-5" style="background: url(/template_avrist/assets/img/common/img_banner_solution.jpg) no-repeat center; background-size: cover;">
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <h2>Butuh perhitungan dan proteksi ideal untukmu atau bisnismu? <a href="#">Dapatkan Solusi</a></h2>
            </div>
        </div>
    </div>
</section>


<?= $this->template("include/footer.html.php"); ?>
<?= $this->template("include/script.html.php"); ?>

<script>
    // Navbar active state
    // $('#navHome').addClass('active');

    // Animation on load
    document.addEventListener("DOMContentLoaded", function(event) {
        $(".loader").fadeOut('slow');
        $(".loader-wrapper").fadeOut("slow");
        //  $(".anim-1").addClass("animated fadeInLeft delayp10");
        // $(".anim-2").addClass("animated fadeInUp delayp12");
        //$(".anim-3").addClass("animated fadeInUp delayp14");
    });

    $('#dateFilter').datepicker();

    $(".navbar").addClass('navbar-transparent');
    var logo = ["/template_avrist/assets/img/brand/logo_avrist.png", "/template_avrist/assets/img/brand/logo_avrist_white.png"];
    $('.navbar-avrist .navbar-brand img').attr('src', logo[1]);

    $(window).on("scroll", function() {
        if ($(window).scrollTop() > 10) {
            $(".navbar-avrist").removeClass("navbar-transparent");
            $('.navbar-avrist .navbar-brand img').attr('src', logo[0]);
        } else {
            //remove the background property so it comes transparent again (defined in your css)
            $(".navbar-avrist").addClass("navbar-transparent");
            $('.navbar-avrist .navbar-brand img').attr('src', logo[1]);
        }
    });

    $('.owl-insurance').owlCarousel({
        loop: false,
        margin: 7.5,
        dots: false,
        responsive:{
            0:{
                items: 1,
                margin: 20,
                dots: true
            },
            768:{
                margin: 15,
                items: 4
            }
        }
    });

    $('.owl-kebutuhan').owlCarousel({
        loop: false,
        margin: 7.5,
        dots: false,
        responsive:{
            0:{
                items: 1
            },
            768:{
                margin: 30,
                items: 3
            }
        }
    });

    $('.navbar-toggler').click(function(){
        $('.navbar').removeClass("navbar-transparent");
    });
    $('.navbar-slide-close').click(function(){
        $('.navbar').addClass("navbar-transparent");
    });

</script>

</body>
</html>
