<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

use Pimcore\Model\Document;
use Pimcore\Model\Document\Page;
?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
    <?php $prefix="../";?>
    <?= $this->template("include/head.html.php"); ?>
    <?= $this->template("include/tracking-script/head.html.php"); ?>
</head>
<body class="body-primary">

<!--<div class="loader-wrapper loader-light">-->
<!--    <div class="loader"></div>-->
<!--</div>-->

<?= $this->template("include/navbar.html.php"); ?>

<section class="py-main section-get-solution">
    <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/pattern/ic_ornamen_yellow.png')?>" class="pattern-yellow" alt="Ornamen yellow">
    <div class="container container-sm">
        <form class="needs-validation form-get-solution" novalidate>
            <div id="form-1">
                <p>Nama saya <input type="text" class="form-control w-300" name="" required> saat ini berusia <input type="text" class="form-control w-15" name="" id="ageForm" required> tahun.</p>
            </div>

            <div id="form-2">
                <p>Saya dapat dihubungi di nomor<input type="text" class="form-control w-300" name="" id="phoneNumber" required> dan email <input type="text" class="form-control w-350" id="email" name="" required>.</p>
                <a href="#" class="btn btn-transparent" id="btn-next">
                    <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/icon/ic_next_white.png')?>" class="img-fluid icon-next" alt="Next Arrow">
                </a>
            </div>

            <div id="form-3">
                <p>Saya memilih solusi untuk</p>
                <div class="form-btn-group">
                    <button class="btn btn-select" id="btn1">
                        <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/icon/ic_quote_conventional.png')?>" class="img-fluid">
                        <span>Conventional</span>
                    </button>
                    <button class="btn btn-select" id="btn2">
                        <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/icon/ic_quote_syariah.png')?>" class="img-fluid">
                        <span>Syariah</span>
                    </button>
                </div>
            </div>

            <div id="form-4">
                <p>Saya butuh solusi untuk</p>
                <div class="form-btn-group">
                    <div class="row">
                        <div class="col-6 col-md-2">
                            <button class="btn btn-select btn-select-2" id="btn3">
                                <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/icon/ic_quote_jiwa.png')?>" class="img-fluid">
                                <span>Jiwa</span>
                            </button>
                        </div>
                        <div class="col-6 col-md-2">
                            <button class="btn btn-select btn-select-2" id="btn4">
                                <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/icon/ic_quote_kesehatan.png')?>" class="img-fluid">
                                <span>Kesehatan</span>
                            </button>
                        </div>
                        <div class="col-6 col-md-2">
                            <button class="btn btn-select btn-select-2" id="btn5">
                                <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/icon/ic_quote_otomotif.png')?>" class="img-fluid">
                                <span>Otomotif</span>
                            </button>
                        </div>
                        <div class="col-6 col-md-2">
                            <button class="btn btn-select btn-select-2" id="btn6">
                                <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/icon/ic_quote_properti.png')?>" class="img-fluid">
                                <span>Properti</span>
                            </button>
                        </div>
                        <div class="col-6 col-md-2">
                            <button class="btn btn-select btn-select-2" id="btn7">
                                <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/icon/ic_quote_pensiun.png')?>" class="img-fluid">
                                <span>Pensiun</span>
                            </button>
                        </div>
                    </div>
                </div>

                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="customControlValidation1" required>
                    <label class="custom-control-label" for="customControlValidation1">Saya tidak yakin dengan kebutuhan saya</label>
                    <div class="invalid-feedback">Example invalid feedback text</div>
                </div>
            </div>


            <div id="form-5">
                <p class="d-inline-block mb-0">Saya butuh solusi untuk
                <div class="dropdown d-inline">
                    <div class="dropdown-toggle " data-toggle="dropdown" id="select-value" required>
                        Pilih Solusi
                    </div>
                    <div class="dropdown-menu-form" id="dropdown-menu-form">
                        <div class="selection-value dropdown-item" data-select="0">Saya, pasangan dan anak</div>
                        <div class="selection-value dropdown-item" data-select="1">Fusce vehicula dolor arcu</div>
                        <div class="selection-value dropdown-item" data-select="2">Fusce vehicula dolor arcu</div>
                    </div>
                </div>
                </p>
            </div>

            <div class="clearfix" id="form-6">
                <a href="<?php echo $prefix;?>quote-summary" class="btn btn-secondary">Lanjut</a>
            </div>



        </form>
    </div>
</section>

<?= $this->template("include/script.html.php"); ?>

<script>
    // Navbar active state
    // $('#navHome').addClass('active');

    var logo = ["../../template_avrist/assets/img/brand/logo_avrist.png", "../../template_avrist/assets/img/brand/logo_avrist_white.png"];

    $(".navbar").addClass('navbar-transparent');
    $('.navbar-avrist .navbar-brand img').attr('src', logo[1]);

    $(window).on("scroll", function() {
        if ($(window).scrollTop() > 10) {
            $(".navbar-avrist").removeClass("navbar-transparent");
            $('.navbar-avrist .navbar-brand img').attr('src', logo[0]);
        } else {
            //remove the background property so it comes transparent again (defined in your css)
            $(".navbar-avrist").addClass("navbar-transparent");
            $('.navbar-avrist .navbar-brand img').attr('src', logo[1]);
        }
    });

    // Animation on load
    document.addEventListener("DOMContentLoaded", function(event) {
        $(".loader").fadeOut('slow');
        $(".loader-wrapper").fadeOut("slow");
        //  $(".anim-1").addClass("animated fadeInLeft delayp10");
        // $(".anim-2").addClass("animated fadeInUp delayp12");
        //$(".anim-3").addClass("animated fadeInUp delayp14");  
    });

    //dropdown toggle
    $('#dropdown-menu-form').hide();
    $('#select-value').click(function(){
        $('#dropdown-menu-form').toggle();
    });

    $(".selection-value").click(function(e) {
        const selected = e.target.dataset.select;
        switch(selected) {
            case '0':
                $('#select-value').html('Saya, pasangan dan anak');
                $('#dropdown-menu-form').hide();
                $('#form-6').show();
                break;
            case '1':
                $('#select-value').html('Fusce vehicula dolor arcu');
                $('#dropdown-menu-form').hide();
                $('#form-6').show();
                break;
            case '2':
                $('#select-value').html('Fusce vehicula dolor arcu');
                $('#dropdown-menu-form').hide();
                $('#form-6').show();
                break;
        }
    })

    $('#form-2').hide();
    $('#form-3').hide();
    $('#form-4').hide();
    $('#form-5').hide();
    $('#form-6').hide();
    $('#btn-next').hide();

    // Form states
    $("form .form-control").on("keyup", function(){
        if($("#ageForm").val() !== ""  && $('#ageForm').val().length >= 2  ) {
            $('#form-2').fadeIn( 2000 );
            $('#form-1').addClass('form-is-completed');
            $("#phoneNumber").trigger('focus');
            $('.form-get-solution').css("transform","translateY(calc(50vh + -220px))");
        }
        if( $("#phoneNumber").val().length >= 12 ) {
            $('#email').focus();
        }
        if($("#phoneNumber").val() !== "" && $('#email').val() !== "" ) {
            $('#btn-next').fadeIn( 1000 );
            $('.form-get-solution').css("transform","translateY(calc(50vh + -280px))");
        }
    });

    $('#btn-next').click(function(e){
        $('#form-3').fadeIn( 1000 );
        e.preventDefault();
        $('#btn-next').hide();
        $('.form-get-solution').css("transform","translateY(calc(50vh + -340px))");
    });

    $('#btn1').click(function(e){
        $('.btn-select').removeClass("active");
        $('#btn1').addClass('active');
        $('#form-4').show();
        $('.form-get-solution').css("transform","translateY(calc(50vh + -400px))");
        e.preventDefault();
    });

    $('#btn2').click(function(e){
        $('.btn-select').removeClass("active");
        $('#btn2').addClass('active');
        $('#form-4').show();
        $('.form-get-solution').css("transform","translateY(calc(50vh + -400px))");
        e.preventDefault();
    });

    $('#btn3').click(function(e){
        $('.btn-select-2').removeClass("active");
        $('#btn3').addClass('active');
        $('#form-5').show();
        $('.form-get-solution').css("transform","translateY(calc(50vh + -460px))");
        e.preventDefault();
    });

    $('#btn4').click(function(e){
        $('.btn-select-2').removeClass("active");
        $('#btn4').addClass('active');
        $('#form-5').show();
        $('.form-get-solution').css("transform","translateY(calc(50vh + -460px))");
        e.preventDefault();
    });

    $('#btn5').click(function(e){
        $('.btn-select-2').removeClass("active");
        $('#btn5').addClass('active');
        $('#form-5').show();
        $('.form-get-solution').css("transform","translateY(calc(50vh + -460px))");
        e.preventDefault();
    });

    $('#btn6').click(function(e){
        $('.btn-select-2').removeClass("active");
        $('#btn6').addClass('active');
        $('#form-5').show();
        $('.form-get-solution').css("transform","translateY(calc(50vh + -460px))");
        e.preventDefault();
    });

    $('#btn7').click(function(e){
        $('.btn-select-2').removeClass("active");
        $('#btn7').addClass('active');
        $('#form-5').show();
        $('.form-get-solution').css("transform","translateY(calc(50vh + -460px))");
        e.preventDefault();
    });

</script>

</body>
</html>
