<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

use Pimcore\Model\Document;
use Pimcore\Model\Document\Page;


$this->extend('layout_avrist.html.php');
$prefix = "http://avrist.dnatechnology.co.id";?>

<div class="confirm-alert fixed-top" style="width: 100%; z-index: 1000; margin-top: 62px; display: none;">
<div class="alert alert-success">
    <div class="container">Email has successfully send!</div>
</div>
</div>

<section class="py-main section-header-avrist">
    <div class="container">
        <div class="heading-w-link text-white">
            <h2>Solusi Lengkap Untuk</h2>
            <div class="heading-group">
                <div class="row">
                    <div class="col-md-6">
                        <p><strong>Saya, pasangan, & anak.</strong> untuk proteksi <strong>jiwa</strong></p>
                    </div>
                    <div class="col-md-6 text-md-right">
                        <a href="" class="btn btn-transparent mr-2">Kirim quote ke e-mail</a>
                        <a href="#" class="btn btn-transparent" data-toggle="modal" data-target="#modalContactMe">Hubungi saya</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="py-main section-quote-summary">
    <div class="container">
        <div class="owl-carousel owl-theme slider-mobile">

            <?php foreach ($this->product as $product){ ?>
            <div class="item">
                <div class="card card-quote-summary">
                    <div class="card-top">
                        <p><small>Untuk Anda</small></p>

                        <a href="<?= $product->getPage() ?>" class="title"><?= $product->getName() ?></a>
                        <p><small>Proteksi dasar pilihan bagi keluarga kecil</small></p>
                    </div>
                    <div class="card-middle">
                        <p class="mb-1"><small>Premi mulai dari</small></p>
                        <h5>@<?= $product->getPrice() ?></h5>
                    </div>
                    <div class="card-bottom">
                        <p class="font-size-sm">Manfaat & Fasilitas</p>
                        <ul>
                            <li>Ut maximus sollicitudin odio vel ullamcorper. Maecenas congue est eget magna blandit, vitae fringilla odio pulvinar. </li>
                            <li>Ut maximus sollicitudin odio vel ullamcorper. Maecenas congue est eget magna blandit, vitae fringilla odio pulvinar. </li>
                            <li>Ut maximus sollicitudin odio vel ullamcorper. Maecenas congue est eget magna blandit, vitae fringilla odio pulvinar. </li>
                            <li>Ut maximus sollicitudin odio vel ullamcorper. Maecenas congue est eget magna blandit, vitae fringilla odio pulvinar. </li>
                            <li>Ut maximus sollicitudin odio vel ullamcorper. Maecenas congue est eget magna blandit, vitae fringilla odio pulvinar. </li>
                            <li>Ut maximus sollicitudin odio vel ullamcorper. Maecenas congue est eget magna blandit, vitae fringilla odio pulvinar. </li>
                        </ul>
                    </div>
                </div>
            </div>
            <?php } ?>

        </div>
    </div>
</section>

<section class="py-main section-agent">
    <div class="container">
        <div class="heading">
            <h2>Ingin konsultasi langsung?</h2>
            <p>Hubungi agen terdekat, atau Kunjungi Cabang Terdekat</p>
        </div>

        <div class="content">
            <div class="owl-carousel owl-theme slider-mobile">
                <div class="item">
                    <a href="#" class="card card-agent">
                        <div class="card-body">
                            <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/people/agent-1.jpg')?>" class="img-fluid img-avatar" alt="Agent Avatar">
                            <div class="location-agent">
                                <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/icon/ic_location_agent.png')?>" class="img-fluid" width="10">
                                <small>2km from you</small>
                            </div>

                            <div class="card-info mt-5">
                                <h4>Rosa Hadyanto</h4>
                            </div>
                            <div class="card-info">
                                <div class="agent-information">
                                    <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/icon/ic_email_agent.png')?>" class="img-fluid" width="10">
                                    <span>Rosa@sample.com</span>
                                </div>
                                <div class="agent-information">
                                    <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/icon/ic_telep.png')?>" class="img-fluid" width="10">
                                    <span>+6221 654321</span>
                                </div>
                            </div>
                            <div class="location">
                                <h5 class="mb-1">KPA Tangerang</h5>
                                <p>Jl. Pajajaran No. 96 H, Bintaro, Tangerang Selatan</p>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="item">
                    <a href="#" class="card card-agent">
                        <div class="card-body">
                            <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/people/agent-2.jpg')?>" class="img-fluid img-avatar" alt="Agent Avatar">
                            <div class="location-agent">
                                <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/icon/ic_location_agent.png')?>" class="img-fluid" width="10">
                                <small>1.5km from you</small>
                            </div>

                            <div class="card-info mt-5">
                                <h4>Karenina Wijaya</h4>
                            </div>
                            <div class="card-info">
                                <div class="agent-information">
                                    <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/icon/ic_email_agent.png')?>" class="img-fluid" width="10">
                                    <span>erick@sample.com</span>
                                </div>
                                <div class="agent-information">
                                    <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/icon/ic_telep.png')?>" class="img-fluid" width="10">
                                    <span>+6221 654321</span>
                                </div>
                            </div>
                            <div class="location">
                                <h5 class="mb-1">KPA Tangerang</h5>
                                <p>Jl. Pajajaran No. 96 H, Bintaro, Tangerang Selatan</p>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="item">
                    <a href="#" class="card card-agent">
                        <div class="card-body">
                            <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/people/agent-3.jpg')?>" class="img-fluid img-avatar" alt="Agent Avatar">
                            <div class="location-agent">
                                <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/icon/ic_location_agent.png')?>" class="img-fluid" width="10">
                                <small>1.8km from you</small>
                            </div>

                            <div class="card-info mt-5">
                                <h4>Erick Gunawan</h4>
                            </div>
                            <div class="card-info">
                                <div class="agent-information">
                                    <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/icon/ic_email_agent.png')?>" class="img-fluid" width="10">
                                    <span>karen@sample.com</span>
                                </div>
                                <div class="agent-information">
                                    <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/icon/ic_telep.png')?>" class="img-fluid" width="10">
                                    <span>+6221 654321</span>
                                </div>
                            </div>
                            <div class="location">
                                <h5 class="mb-1">KPA Tangerang</h5>
                                <p>Jl. Pajajaran No. 96 H, Bintaro, Tangerang Selatan</p>
                            </div>
                        </div>
                    </a>
                </div>

            </div>
        </div>
    </div>
</section>

<!--START: Modal Contact Me-->
<div class="modal" tabindex="-1" role="dialog" id="modalContactMe">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body p-box">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><i class="fal fa-times"></i></span>
                </button>
                <h4 class="modal-title mb-3">Hubungi Saya Di</h4>
                <form class="needs-validation" novalidate>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="">Tanggal</label>
                                <input type="text" class="form-control form-control-datepicker" autocomplete="off" placeholder="(MM/DD/YYYY)" id="dateContact" required="">
                                <div class="invalid-feedback">
                                    Please input date
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="validationCustom04">Jam</label>
                                <div class="form-group">
                                    <select class="custom-select" id="validationCustom04" required>
                                        <option value="">Choose</option>
                                        <option value="1">Pagi (09:00 - 11:00)</option>
                                        <option value="2">Siang (11:00 - 14:00)</option>
                                        <option value="3">Sore (14:00 - 18:00)</option>
                                    </select>
                                    <div class="invalid-feedback">
                                        Please choose correctly
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <button class="btn btn-primary" type="submit" id="btnSubmitContact">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!--END: Modal Contact Me-->

<script>
    $('#btnSubmitContact').click(function(e){
        $('#modalContactMe').modal('hide');
        $('.confirm-alert').fadeIn();
        e.preventDefault();

        setTimeout(function(){
            $('.confirm-alert').fadeOut();
        },2000)

    });

    $('#dateContact').datepicker();
</script>