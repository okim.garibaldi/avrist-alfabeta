<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

use Pimcore\Model\Document;
use Pimcore\Model\Document\Page;
?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
    <?php $prefix="avrist.dnatechnology.co.id";?>
    <?= $this->template("include/head.html.php"); ?>
    <?= $this->template("include/tracking-script/head.html.php"); ?>
</head>
<body class="body-primary">

    <?= $this->template("include/navbar.html.php"); ?>

    <section class="py-main section-get-solution">
        <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/pattern/ic_ornamen_yellow.png')?>" class="pattern-yellow" alt="Ornamen yellow">
        <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/pattern/ic_ornamen_yellow.png')?>" class="pattern-yellow-2" alt="Ornamen yellow">
        <div class="container container-sm">
            <div class="btn-wrapper text-center">
                <h5 class="mb-3">Pilih Solusi untuk</h5>
                <a href="/get-solution/personal" class="btn btn-gender active mr-2">Personal</a>
                <a href="/get-solution/bisnis" class="btn btn-gender">Bisnis</a>
            </div>
            <div class="form-wrapper">
                <form class="needs-validation form-get-solution" novalidate>
                    <div id="form-1">
                        <p>Nama saya <input id="input-name" type="text" class="form-control w-300" name="" required> saat ini berusia <input id="input-age" type="number" class="form-control w-15" name="" required> tahun.</p>
                    </div>

                    <div id="form-2">
                        <p>Saya dapat dihubungi di nomor<input id="input-telp" type="text" class="form-control w-300" name="" required> dan email <input id="input-email" type="email" class="form-control w-350" name="" required>.</p>
                        <a href="#" class="btn btn-transparent" id="btn-next">
                            <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/icon/ic_next_white.png')?>" class="img-fluid icon-next" alt="Next Arrow">
                        </a>
                    </div>


                    <div id="form-3">
                        <p>Saya memilih solusi untuk</p>
                        <div class="form-btn-group">
                            <button class="btn btn-select" id="btn1">
                                <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/icon/ic_quote_conventional.png')?>" class="img-fluid">
                                <span>Conventional</span>
                            </button>
                            <button class="btn btn-select" id="btn2">
                                <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/icon/ic_quote_syariah.png')?>" class="img-fluid">
                                <span>Syariah</span>
                            </button>
                        </div>
                    </div>

                    <div id="form-4">
                        <p>Saya butuh solusi untuk</p>
                        <div class="form-btn-group">
                            <div class="row row-1">
                                <div class="col-6 col-md-2">
                                    <button class="btn btn-select btn-select-2" id="btn3">
                                        <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/icon/ic_quote_jiwa.png')?>" class="img-fluid">
                                        <span>Jiwa</span>
                                    </button>
                                </div>
                                <div class="col-6 col-md-2">
                                    <button class="btn btn-select btn-select-2" id="btn4">
                                        <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/icon/ic_quote_kesehatan.png')?>" class="img-fluid">
                                        <span>Kesehatan</span>
                                    </button>
                                </div>
                                <div class="col-6 col-md-2">
                                    <button class="btn btn-select btn-select-2" id="btn5">
                                        <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/icon/ic_quote_otomotif.png')?>" class="img-fluid">
                                        <span>Otomotif</span>
                                    </button>
                                </div>
                                <div class="col-6 col-md-2">
                                    <button class="btn btn-select btn-select-2" id="btn6">
                                        <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/icon/ic_quote_properti.png')?>" class="img-fluid">
                                        <span>Properti</span>
                                    </button>
                                </div>
                                <div class="col-6 col-md-2">
                                    <button class="btn btn-select btn-select-2" id="btn7">
                                        <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/icon/ic_quote_pensiun.png')?>" class="img-fluid">
                                        <span>Pensiun</span>
                                    </button>
                                </div>
                            </div>
                        </div>

                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="customControlValidation1" required>
                            <label class="custom-control-label" for="customControlValidation1">Saya tidak yakin dengan kebutuhan saya</label>
                            <div class="invalid-feedback">Example invalid feedback text</div>
                        </div>
                    </div>


                    <div id="form-5" class="form-tambahan mt-4">
                        <p class="d-inline-block mb-0">Saya butuh solusi untuk
                        <div class="dropdown d-inline">
                            <div class="dropdown-toggle " data-toggle="dropdown" id="select-value" required>
                                Pilih Solusi
                            </div>
                            <div class="dropdown-menu-form" id="dropdown-menu-form">
                                <div class="selection-value dropdown-item" data-select="0">Saya Sendiri</div>
                                <div class="selection-value dropdown-item" data-select="1">Saya dan pasangan</div>
                                <div class="selection-value dropdown-item" data-select="2">Saya, pasangan dan anak</div>
                                <div class="selection-value dropdown-item" data-select="3">Saya dan anak</div>
                            </div>
                        </div>
                        </p>
                    </div>

                    <div id="form-7" class="mt-4 form-tambahan">
                        <p class="d-inline-block mb-0">Jenis mobil yang saya gunakan
                        <div class="dropdown d-inline">
                            <div class="dropdown-toggle " data-toggle="dropdown" id="select-value-2" required>
                                Pilih Kendaraan
                            </div>
                            <div class="dropdown-menu-form" id="dropdown-menu-form-2">
                                <div class="selection-value-2 dropdown-item" data-select="0">Truck/Pick Up</div>
                                <div class="selection-value-2 dropdown-item" data-select="1">Mobil Pribadi</div>
                            </div>
                        </div>
                        </p>
                        <p>Mobil diproduksi tahun <input type="text" class="form-control w-150" name="" id="produksiMobil" required> dengan plat <input type="text" class="form-control w-150" placeholder="Misal: B" name="" id="platMobil" required></p>
                        <p>Saya membeli mobil seharga <input type="text" class="form-control w-300" name="" id="hargaMobil" required>.</p>
                    </div>

                    <div id="form-8" class="mt-4 form-tambahan">
                        <p>Properti saya berada di kota/kabupaten <input type="text" class="form-control w-150" name="" id="kotaProperti" required></p>
                        <p class="d-inline-block mb-0">dan berfungsi sebagai
                        <div class="dropdown d-inline">
                            <div class="dropdown-toggle " data-toggle="dropdown" id="select-value-3" required>
                                Pilih fungsi properti
                            </div>
                            <div class="dropdown-menu-form" id="dropdown-menu-form-3">
                                <div class="selection-value-3 dropdown-item" data-select="0">Rumah Tinggal</div>
                                <div class="selection-value-3 dropdown-item" data-select="1">Gedung</div>
                                <div class="selection-value-3 dropdown-item" data-select="2">Kantor</div>
                                <div class="selection-value-3 dropdown-item" data-select="3">Rumah Toko</div>
                                <div class="selection-value-3 dropdown-item" data-select="4">Rumah Kantor</div>
                                <div class="selection-value-3 dropdown-item" data-select="5">Pabrik</div>
                            </div>
                        </div>
                        </p>
                    </div>

                    <div class="clearfix mt-5" id="form-6">
                        <a href="javascript:;" class="btn btn-secondary btn-next-summary">Lanjut</a>
                    </div>
                </form>
            </div>
        </div>
    </section>
    
    <?= $this->template("include/script.html.php"); ?>

    <script>

        // Animation on load
        document.addEventListener("DOMContentLoaded", function(event) {
            $(".loader").fadeOut('slow');
            $(".loader-wrapper").fadeOut("slow");
            //  $(".anim-1").addClass("animated fadeInLeft delayp10");
            // $(".anim-2").addClass("animated fadeInUp delayp12");
            //$(".anim-3").addClass("animated fadeInUp delayp14");
        });

//        var logo = ["../../../template_avrist/assets/img/brand/logo_avrist.png", "../../../template_avrist/assets/img/brand/logo_avrist_white.png"];
//
//        $(".navbar").addClass('navbar-transparent');
//        $('.navbar-avrist .navbar-brand img').attr('src', logo[1]);
//
//        $(window).on("scroll", function() {
//            if ($(window).scrollTop() > 10) {
//                $(".navbar-avrist").removeClass("navbar-transparent");
//                $('.navbar-avrist .navbar-brand img').attr('src', logo[0]);
//            } else {
//                //remove the background property so it comes transparent again (defined in your css)
//                $(".navbar-avrist").addClass("navbar-transparent");
//                $('.navbar-avrist .navbar-brand img').attr('src', logo[1]);
//            }
//        });

        function allLetter(inputtxt) {
            var letters = /^[a-zA-Z ]*$/i;
            return !inputtxt.match(letters) ? false : true
        }

        var link = "";
        var solutionType = "";
        var solutionNeeds = "";
        var solutionFor = "";
        var selectedProperty = "";
        var selectedSolusi = "";
        var name = "";
        var age = "";
        var telp = "";
        var email = "";
        var vehicleType = "";
        var productionYear = "";
        var plate = "";
        var vehiclePrice = "";
        var propertyCity = "";
        var propertyFunction = "";

        $('.btn-next-summary').on('click', function(e) {
            e.preventDefault();

            console.log('LS')
            console.log(localStorage.getItem('name'))
            console.log(localStorage.getItem('age'))

            productionYear = document.getElementById("produksiMobil").value;
            plate = document.getElementById("platMobil").value;
            vehiclePrice = document.getElementById("hargaMobil").value;
            propertyCity = document.getElementById("kotaProperti").value;

            link = "http://<?php echo $prefix;?>/get-solution/save-record?category=personal&name=" + name.toLowerCase().split(' ').join('-') +
                "&age="+ age +
                "&telp="+ telp +
                "&email="+ email +
                "&solution-type="+ solutionType +
                "&solution-needs="+ solutionNeeds;
            if (solutionNeeds == "jiwa" || solutionNeeds == "kesehatan"){
                link = link + "&solution-for="+ solutionFor;
            }
            else if (solutionNeeds == "otomotif"){
                link = link + "&vehicle-type="+ vehicleType + "&production-year="+ productionYear + "&plate="+ plate + "vehicle-price="+ vehiclePrice;
            } else if (solutionNeeds == "property"){
                link = link + "&property-city="+ propertyCity + "&property-function="+ propertyFunction;
            }
            window.location.href = link;
        });



        //dropdown toggle
        $('#dropdown-menu-form').hide();
        $('#select-value').click(function(){
            $('#dropdown-menu-form').toggle();
        });

        $('#dropdown-menu-form-2').hide();
        $('#select-value-2').click(function(){
            $('#dropdown-menu-form-2').toggle();
        });

        $('#dropdown-menu-form-3').hide();
        $('#select-value-3').click(function(){
            $('#dropdown-menu-form-3').toggle();
        });

        $(".selection-value").click(function(e) {
            const selected = e.target.dataset.select;
            switch(selected) {
                case '0':
                    solutionFor = "saya-sendiri";
                    $('#select-value').html('Saya Sendiri');
                    $('#dropdown-menu-form').hide();
                    $('#form-6').show();
                    break;
                case '1':
                    solutionFor = "saya-pasangan";
                    $('#select-value').html('Saya dan pasangan');
                    $('#dropdown-menu-form').hide();
                    $('#form-6').show();
                    break;
                case '2':
                    solutionFor = "saya-pasangan-anak";
                    $('#select-value').html('Saya, pasangan dan anak');
                    $('#dropdown-menu-form').hide();
                    $('#form-6').show();
                    break;
                case '3':
                    solutionFor = "saya-anak";
                    $('#select-value').html('Saya dan anak');
                    $('#dropdown-menu-form').hide();
                    $('#form-6').show();
                    break;
            }
        })

        $(".selection-value-2").click(function(e) {
            const selected = e.target.dataset.select;
            switch(selected) {
                case '0':
                    vehicleType = "truck-pickup";
                    $('#select-value-2').html('Truck/Pick Up');
                    $('#dropdown-menu-form-2').hide();
                    $('#form-6').show();
                    break;
                case '1':
                    vehicleType = "mobil-pribadi";
                    $('#select-value-2').html('Mobil Pribadi');
                    $('#dropdown-menu-form-2').hide();
                    $('#form-6').show();
                    break;
            }
        })

        $(".selection-value-3").click(function(e) {
            const selected = e.target.dataset.select;
            switch(selected) {
                case '0':
                    propertyFunction = "rumah-tinggal";
                    $('#select-value-3').html('Rumah Tinggal');
                    $('#dropdown-menu-form-3').hide();
                    $('#form-6').show();
                    break;
                case '1':
                    propertyFunction = "gedung";
                    $('#select-value-3').html('Gedung');
                    $('#dropdown-menu-form-3').hide();
                    $('#form-6').show();
                    break;
                case '2':
                    propertyFunction = "kantor";
                    $('#select-value-3').html('Kantor');
                    $('#dropdown-menu-form-3').hide();
                    $('#form-6').show();
                    break;
                case '3':
                    propertyFunction = "rumah-toko";
                    $('#select-value-3').html('Rumah Toko');
                    $('#dropdown-menu-form-3').hide();
                    $('#form-6').show();
                    break;
                case '4':
                    propertyFunction = "rumah-kantor";
                    $('#select-value-3').html('Rumah Kantor');
                    $('#dropdown-menu-form-3').hide();
                    $('#form-6').show();
                    break;
                case '5':
                    propertyFunction = "pabrik";
                    $('#select-value-3').html('Pabrik');
                    $('#dropdown-menu-form-3').hide();
                    $('#form-6').show();
                    break;
            }
        })

        $('#form-2').hide();
        $('#form-3').hide();
        $('#form-4').hide();
        $('#form-5').hide();
        $('#form-6').hide();
        $('#form-7').hide();
        $('#form-8').hide();
        $('#btn-next').hide();

        // Form states
        $("form .form-control").on("keyup", function(){

            name = document.getElementById("input-name").value;
            age = document.getElementById("input-age").value;
            let isValidName = allLetter(name)
            if(!isValidName) alert('Nama harus dengan huruf');

            if(age !== ""  && age.length >= 2  && isValidName == true) {

                $('#form-2').fadeIn( 2000 );
                $('#form-1').addClass('form-is-completed');
                $('#form-1').css("margin-top","calc(50vh + -34rem)");
                $('#form-2').css("margin-top","calc(50vh + -20rem)");

                localStorage.setItem('name', name)
                localStorage.setItem('age', age)

            }
            if($("#input-telp").val() !== "" && $('#input-email').val() !== "" ) {

                telp = document.getElementById("input-telp").value;
                email = document.getElementById("input-email").value;

                localStorage.setItem('telp', telp)
                localStorage.setItem('email', email)

                $('#btn-next').fadeIn( 1000 );
            }
        });

        $('#btn-next').click(function(e){
            $('#form-3').fadeIn( 1000 );
            $('#form-1').css("margin-top","calc(50vh + -36rem)");
            $('#form-2').css("margin-top","calc(50vh + -36rem)");
            $('#form-3').css("margin-top","calc(50vh + -20rem)");
            e.preventDefault();
            $('#btn-next').hide();
        });

        $('#btn1').click(function(e){
            solutionType = "conventional";
            $('.btn-select').removeClass("active");
            $('#btn1').addClass('active');
            $('#form-4').show();
            $('#form-1').css("transform","translateY(calc(50vh + -36rem))");
            $('#form-2').css("transform","translateY(calc(50vh + -36rem))");
            $('#form-3').css("transform","translateY(calc(50vh + -36rem))");
            $('#form-4').css("transform","translateY(calc(50vh + -34rem))");
            e.preventDefault();
        });

        $('#btn2').click(function(e){
            solutionType = "syariah"
            $('.btn-select').removeClass("active");
            $('#btn2').addClass('active');
            $('#form-4').show();
            $('#form-1').css("transform","translateY(calc(50vh + -80rem))");
            $('#form-2').css("transform","translateY(calc(50vh + -80rem))");
            $('#form-3').css("transform","translateY(calc(50vh + -80rem))");
            $('#form-4').css("transform","translateY(calc(50vh + -34rem))");
            e.preventDefault();
        });

        //jiwa
        $('#btn3').click(function(e){
            solutionNeeds = "jiwa";
            $('.btn-select-2').removeClass("active");
            $('#btn3').addClass('active');
            $('#form-3').css("transform","translateY(calc(50vh + -80rem))");
            $('#form-4').css("transform","translateY(calc(50vh + -52rem))");
            $('#form-5').css("transform","translateY(calc(50vh + -50rem))");
            $('.form-tambahan').hide();
            $('#form-5').show();
            $('#form-6').show();
            $('#form-6').css("transform","translateY(calc(50vh + -50rem))");
            e.preventDefault();
        });

        //kesehatan
        $('#btn4').click(function(e){
            solutionNeeds = "kesehatan";
            $('.btn-select-2').removeClass("active");
            $('#btn4').addClass('active');
            $('#form-3').css("transform","translateY(calc(50vh + -80rem))");
            $('#form-4').css("transform","translateY(calc(50vh + -52rem))");
            $('#form-5').css("transform","translateY(calc(50vh + -50rem))");
            $('.form-tambahan').hide();
            $('#form-5').show();
            $('#form-6').show();
            $('#form-6').css("transform","translateY(calc(50vh + -50rem))");
            e.preventDefault();
        });

        //otomotif
        $('#btn5').click(function(e){
            solutionNeeds = "otomotif";
            $('.btn-select-2').removeClass("active");
            $('#btn5').addClass('active');
            $('#form-3').css("transform","translateY(calc(50vh + -80rem))");
            $('#form-4').css("transform","translateY(calc(50vh + -52rem))");
            $('#form-5').css("transform","translateY(calc(50vh + -50rem))");
            $('.form-tambahan').hide();
            $('#form-7').show();
            $('#form-7').css("transform","translateY(calc(50vh + -50rem))");
            $('#form-6').show();
            $('#form-6').css("transform","translateY(calc(50vh + -50rem))");
            e.preventDefault();
        });

        //properti
        $('#btn6').click(function(e){
            solutionNeeds = "properti";
            $('.btn-select-2').removeClass("active");
            $('#btn6').addClass('active');
            $('#form-3').css("transform","translateY(calc(50vh + -80rem))");
            $('#form-4').css("transform","translateY(calc(50vh + -52rem))");
            $('#form-5').css("transform","translateY(calc(50vh + -50rem))");
            $('.form-tambahan').hide();
            $('#form-8').show();
            $('#form-8').css("transform","translateY(calc(50vh + -50rem))");
            $('#form-6').show();
            $('#form-6').css("transform","translateY(calc(50vh + -50rem))");
            e.preventDefault();
        });

        //pensiun
        $('#btn7').click(function(e){
            solutionNeeds = "pensiun";
            $('.btn-select-2').removeClass("active");
            $('#btn7').addClass('active');
            $('#form-3').css("transform","translateY(calc(50vh + -80rem))");
            $('#form-4').css("transform","translateY(calc(50vh + -52rem))");
            $('#form-5').css("transform","translateY(calc(50vh + -50rem))");
            $('.form-tambahan').hide();
            $('#form-6').show();
            $('#form-6').css("transform","translateY(calc(50vh + -50rem))");
            e.preventDefault();
        });
    </script>
</body>
</html>