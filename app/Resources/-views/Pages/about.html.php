<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

$this->extend('layout_avrist_non_transparent.html.php');?>

<section class="py-main">
    <div class="container">
        <div class="heading">
            <?= $about->getShortDescription(); ?>
        </div>
        <div>
            <?= $about->getDescription(); ?>
        </div>
    </div>
</section>
