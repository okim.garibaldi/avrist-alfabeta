<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

use Pimcore\Model\Document;
use Pimcore\Model\Document\Page;
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
    <?php $prefix="http://avrist.dnatechnology.co.id";?>
    <?= $this->template("include/head.html.php"); ?>
    <?= $this->template("include/tracking-script/head.html.php"); ?>

    <style>
        .navbar.fixed-top + * {
            margin-top: 0;
        }

        .confirm-alert .alert-success {
            background-color: #B9D137;
            color: #fff;
        }

        .section-insurance-content.show {
            display: block !important;
        }
        .section-insurance-content {
            display: none;
        }

        .dropdown-item--purple {
            color: #1a0dab !important;
        }
    </style>

</head>

<body>

<?= $this->template("include/navbar.html.php"); ?>

<div class="cover cover-full" style="background: url(<?php echo $prefix;?>/assets/img/common/bg_rectangle-light-1.jpg) no-repeat center; background-size: cover;">
    <div class="container justify-content-center">
        <div class="cover-content text-center">
            <h1 class="cover-title animated fadeInUp delayp1" style="color: #432862">Sorry, but the page you were trying to view does not exist</h1>
<!--            <p class="cover-text animated fadeInUp delayp2">.</p>-->
            <a class="btn btn-primary animated fadeInUp delayp3" href="/" role="button" target="_top">Go Back to Home</a>
        </div>
    </div>
</div>

<?= $this->template("include/footer.html.php"); ?>
<?= $this->template("include/script.html.php"); ?>

</body>

</html>
