<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

use Pimcore\Model\Document;
use Pimcore\Model\Document\Page;


$this->extend('layout_avrist_non_transparent.html.php');
$prefix = "http://avrist.dnatechnology.co.id";?>

<section class="py-main section-login vp-fadeinup">
    <div class="container container-xs">
        <div class="box-form p-box">
            <div class="header">
                <a onclick="goBack()" class="btn btn-back mb-4">
                    <i class="fal fa-chevron-left"></i>
                </a>
                <a class="header-brand" href="<?php echo $prefix;?>">
                    <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/brand/logo_avrist.png')?>" class="img-fluid" alt="Logo">
                </a>
            </div>
            <div class="subheading">
                <h2>Login</h2>
                <p>Login to customer dashboard</p>
            </div>
            <form class="needs-validation" name="loginForm" onsubmit="submitLogin(); return false" novalidate>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="" class="sr-only">Email</label>
                        <input type="text" class="form-control" id="loginUsername" name="loginUsername" placeholder="Enter email" value="" required>
                        <div class="invalid-feedback">
                            Please enter your email address
                        </div>
                    </div>
                </div>
                <div class="col-md-12 mb-4">
                    <div class="form-group">
                        <label for="" class="sr-only">Password</label>
                        <input type="password" class="form-control" id="loginPassword" name="loginPassword" placeholder="Enter password" value="" required>
                        <div class="invalid-feedback">
                            Please enter your password
                        </div>
                        <a href="#" class="float-right forgot-pass" onclick="submitForgot();">Lupa Password</a>
                    </div>
                </div>
                <div class="col-12">
                    <button class="btn btn-primary btn-block mb-3" type="submit">Login</button>
                    <button class="btn btn-secondary btn-block mb-3" type="submit" onclick="submitRegister();">Daftar Sekarang!</button>
                </div>
            </div>
            </form>
        </div>
    </div>
</section>

<script>

    // Back to previous page
    function goBack() {
        window.history.back();
    }
</script>