<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

 use Pimcore\Model\Document;
 use Pimcore\Model\Document\Page;
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
  <head>
    <?php $prefix="http://avrist.dnatechnology.co.id";?>
    <?= $this->template("include/head.html.php"); ?>
    <?= $this->template("include/tracking-script/head.html.php"); ?>

    <style>
        .slider {
            position: relative;
            height: 3rem;
            margin: 3rem 0;
            width: 80%;
            display: flex;
            flex-direction: column;
        }

        .slider-output {
            display: block;
            position: absolute;
            bottom: -0.5rem;
            left: 25%;
            box-sizing: border-box;
            transform: translateX(-50%);
            font-weight: bold;
            font-size: 1.5rem;
        }

        .slider-input {
            -webkit-appearance: none;
            background-color: transparent;
            position: absolute;
            width: 105%;
            margin-left: -2.5%;
            height: 2rem;
            outline: none;
        }

        .slider-input::-webkit-slider-thumb {
            -webkit-appearance: none;
            width: 2rem;
            height: 8rem;
        }

        .slider-input::hover::-webkite-slider-thumb,
        .slider-input::focus::-webkite-slider-thumb {
            box-shadow: 0 0 0 10px #fff, 0 0 0 6px #F4B635;
        }


        .slider-thumb,
        .top-thumb,
        .tail-thumb {
            height: 1.5rem;
            width: 1.5rem;
            border-radius: 1rem;
            position: absolute;
            top: -.6rem;
            background-color: #F4B635;
            cursor: pointer;
        }

        .slider-thumb {
            left: 25%;
            margin-left: -1rem;
        }

        .top-thumb {
            left: 0;
            margin-left: -1rem;

        }

        .tail-thumb {
            right: 0;
            margin-right: -1rem;
        }

        .slider-track {
            height: .35rem;
            background-color: #ddd;
            border-radius: .25rem;
        }

        .slider-level {
            height: .35rem;
            background-color: #F4B635;
            width: 25%;
            border-radius: 1rem;
        }

        .navbar.fixed-top + * {
            margin-top: 0;
        }

      .confirm-alert .alert-success {
          background-color: #B9D137;
          color: #fff;
      }

      .section-insurance-content.show {
          display: block !important;
      }
      .section-insurance-content {
          display: none;
      }

      .dropdown-item--purple {
          color: #1a0dab !important;
      }
    </style>

  </head>
  <body>
    <?= $this->template("include/tracking-script/body.html.php"); ?>

    <div class="loader-wrapper loader-light">
      <div class="loader"></div>
    </div>

    <?= $this->template("include/navbar.html.php"); ?>

    <?php $this->slots()->output('_content') ?>

    <?= $this->template("include/footer.html.php"); ?>
    <?= $this->template("include/script.html.php"); ?>

    <script>
        // Navbar active state
        // $('#navHome').addClass('active');
        $('.navbar-avrist').addClass('navbar-transparent');
        var logo = ["/template_avrist/assets/img/brand/logo_avrist.png", "/template_avrist/assets/img/brand/logo_avrist_white.png"];
        $('.navbar-avrist .navbar-brand img').attr('src', logo[1]);

        $(window).on("scroll", function() {
            if ($(window).scrollTop() > 10) {
                $(".navbar-avrist").addClass("navbar-scroll");
                $(".navbar-avrist").removeClass("navbar-transparent");
                $('.navbar-avrist .navbar-brand img').attr('src', logo[0]);
            } else {
                //remove the background property so it comes transparent again (defined in your css)
                $(".navbar-avrist").removeClass("navbar-scroll");
                $(".navbar-avrist").addClass("navbar-transparent");
                $('.navbar-avrist .navbar-brand img').attr('src', logo[1]);
            }
        });

        // Animation on load
        document.addEventListener("DOMContentLoaded", function(event) {
            $(".loader").fadeOut('slow');
            $(".loader-wrapper").fadeOut("slow");
            //  $(".anim-1").addClass("animated fadeInLeft delayp10");
            // $(".anim-2").addClass("animated fadeInUp delayp12");
            //$(".anim-3").addClass("animated fadeInUp delayp14");
        });

        $('#headingFAQ1').click(function(){
            $('#faqHome').attr("src","/template_avrist/assets/img/common/mengapa_avrist.gif");
        });

        $('#headingFAQ2').click(function(){
            $('#faqHome').attr("src","/template_avrist/assets/img/common/premi_rendah2.gif");
        });

        $('#headingFAQ3').click(function(){
            $('#faqHome').attr("src","/template_avrist/assets/img/common/proteksi_lengkap.gif");
        });

        $('.navbar-toggler').click(function(){
            $('.navbar').removeClass("navbar-transparent");
        });
        $('.navbar-slide-close').click(function(){
            $('.navbar').addClass("navbar-transparent");
        });

        //Style 2

        var productStyle2 = "";
        var ageStyle2 = document.getElementsByClassName("slider-output");
        var genderStyle2 = "";
        var linkStyle2 = "";

        var pageURL = window.location.href.split('?')[0];
        productStyle2 = pageURL.substr(pageURL.lastIndexOf('/') + 1);
//        console.log(productStyle2);

        $('#genderLaki').click(function(e){
            $('.btn-gender').removeClass("active");
            $('#genderLaki').addClass("active");
            genderStyle2 = "male";
            e.preventDefault();
        });

        $('#genderPerempuan').click(function(e){
            $('.btn-gender').removeClass("active");
            $('#genderPerempuan').addClass("active");
            genderStyle2 = "female";
            e.preventDefault();
        });

        const urlParams = new URLSearchParams(window.location.search);
        const checkParam = urlParams.get('product');

        if (checkParam == null) $('#boxCalcPremi').hide();

        $('#hitungBtn').click(function(e){
            $('#hitungBtn').html("Hitung Ulang");
            $('#boxCalcPremi').show();

            linkStyle2 = pageURL +
                "?product=" + productStyle2 +
                "&gender=" + genderStyle2 +
                "&age=" + ageStyle2[0].value.replace(/thn/g, "");
            console.log(linkStyle2);
            window.location.href = linkStyle2;

            e.preventDefault();
        });
        //End

        //Style 3
//            $('#boxCalcPremiStyle3').hide();
//            $('#hitungBtnStyle3').click(function(e){
//                $('#hitungBtnStyle3').html("Hitung Ulang");
//                $('#boxCalcPremiStyle3').show();
//                e.preventDefault();
//            });
//
//            $('#genderLakiStyle3').click(function(e){
//                $('.btn-genderStyle3').removeClass("active");
//                $('#genderLakiStyle3').addClass("active");
//                e.preventDefault();
//            });
//
//            $('#genderPerempuanStyle3').click(function(e){
//                $('.btn-genderStyle3').removeClass("active");
//                $('#genderPerempuanStyle3').addClass("active");
//                e.preventDefault();
//            });
//
//            $('#boxCalcPremiStyle3').hide();
//            $('#hitungBtnStyle3').click(function(e){
//                $('#hitungBtnStyle3').html("Hitung Ulang");
//                $('#boxCalcPremiStyle3').show();
//                e.preventDefault();
//            });

            //question job
            $('.job-question').hide();

            $('#jobSelect').on('change', function() {
                console.log($('#jobSelect').val());
                switch($('#jobSelect').val()){
                    case '1':
                        $('.job-question').hide();
                        $('#job1').show();
                        break;
                    case '3':
                        $('.job-question').hide();
                        $('#job3').show()
                        break;
                    case '5':
                        $('.job-question').hide();
                        $('#job5').show()
                        break;
                    case '7':
                        $('.job-question').hide();
                        $('#job7').show()
                        break;
                    case '27':
                        $('.job-question').hide();
                        $('#job27').show()
                        break;
                    case '41':
                        $('.job-question').hide();
                        $('#job41').show()
                        break;
                    case '42':
                        $('.job-question').hide();
                        $('#job42').show()
                        break;
                    case '43':
                        $('.job-question').hide();
                        $('#job43').show()
                        break;
                    case '46':
                        $('.job-question').hide();
                        $('#job46').show()
                        break;
                    case '47':
                        $('.job-question').hide();
                        $('#job47').show()
                        break;
                    case '50':
                        $('.job-question').hide();
                        $('#job50').show()
                        break;
                    case '52':
                        $('.job-question').hide();
                        $('#job52').show()
                        break;
                    case '54':
                        $('.job-question').hide();
                        $('#job54').show()
                        break;
                    case '55':
                        $('.job-question').hide();
                        $('#job55').show()
                        break;
                    case '56':
                        $('.job-question').hide();
                        $('#job56').show()
                        break;
                    case '57':
                        $('.job-question').hide();
                        $('#job57').show()
                        break;
                    case '58':
                        $('.job-question').hide();
                        $('#job58').show()
                        break;
                    case '60':
                        $('.job-question').hide();
                        $('#job60').show()
                        break;
                    case '61':
                        $('.job-question').hide();
                        $('#job61').show()
                        break;
                    case '63':
                        $('.job-question').hide();
                        $('#job63').show()
                        break;
                    case '65':
                        $('.job-question').hide();
                        $('#job65').show()
                        break;
                    case '66':
                        $('.job-question').hide();
                        $('#job66').show()
                        break;
                    case '67':
                        $('.job-question').hide();
                        $('#job67').show()
                        break;

                    default:
                        $('.job-question').hide();
                        break
                }
            });

            $('#job54Select').on('change', function() {
                switch($('#job54Select').val()){
                    case '2':
                        $('.job-question').hide();
                        $('#job54').show();
                        $('#job54-quest2').show();
                        break;
                }
            });

            $('#job66Select').on('change', function() {
                switch($('#job66Select').val()){
                    case '1':
                        $('.job-question').hide();
                        $('#job66').show();
                        $('#job66-quest2').show();
                        break;
                    default:
                        $('#job66-quest2').hide();
                        break
                }
            });

            $('#job66-quest2-select').on('change', function() {
                console.log($('#job66-quest2-select').val());
                switch($('#job66-quest2-select').val()){
                    case '1':
                        $('.job-question').hide();
                        $('#job66').show();
                        $('#job66-quest2').show();
                        $('#job66-quest3').show();
                        break;
                    default:
                        $('#job66-quest3').hide();
                        break
                }
            });
        //End

        // Style 4
        var sel = document.getElementById('validationCustom04');
        $('#premiHasilBulanStyle4').hide();
        $('#premiHasilTahunStyle4').hide();
        $('#hitungBtnStyle4').click(function(e){
            var selected = sel.value;
            if (selected == "1") {
                $('#hitungBtnStyle4').html("Hitung Ulang");
                $('#premiCardStyle4').hide();
                $('#premiHasilBulanStyle4').show();
            } else if (selected == "2"){
                $('#hitungBtnStyle4').html("Hitung Ulang");
                $('#premiCardStyle4').hide();
                $('#premiHasilTahunStyle4').show();
            }
//            e.preventDefault();
        });
        // End Style 4

        // Submit Tanya Pada Ahlinya
        var name = "";
        var email = "";
        var message = "";
        var redirect = "";
        var link = "";
        $('.btn-submit-ask').on('click', function (e) {
            e.preventDefault();

            name = document.getElementById("name-ask").value;
            email = document.getElementById("email-ask").value;
            message = document.getElementById("message-ask").value;
            redirect = window.location.href;

            link = "http://" + window.location.hostname + "/ask-the-expert/submit?name=" + name + "&email=" + email + "&message=" + message + "&redirect=" + redirect;
            window.location.href = link;
        });
        //end

//        //dropdown
//        let urlDp = window.location.href.split('?')[0];
//        let dp = urlDp.substr(urlDp.lastIndexOf('/') + 1);
//        document.getElementById('select-value').innerText = dp;

    </script>

  </body>
</html>
