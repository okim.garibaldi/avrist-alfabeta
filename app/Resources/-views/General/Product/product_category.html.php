<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

use Pimcore\Model\Document;
use Pimcore\Model\Document\Page;

$this->extend('layout_avrist.html.php');
$prefix = "http://avrist.dnatechnology.co.id";?>

<section class="py-main cover cover-landing-page">
    <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/pattern/ic_ornamen_yellow.png')?>" class="pattern-yellow" alt="Pattern Yellow">
    <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/pattern/ic_ornamen_yellow.png')?>" class="pattern-yellow-1" alt="Pattern Yellow">
    <div class="container content-center text-center">
        <form class="form-cover">
            <h2>Saya butuh asuransi</h2>
            <div class="dropdown-custom form-group">
                <div class="dropdown-toggle form-control" data-toggle="dropdown" id="select-value">
                    Accident
                </div>
                <div class="dropdown-menu">
                    <div class="selection-value dropdown-item" data-select="0"><a href="/general/solusi/accident">Accident</a></div>
                    <div class="selection-value dropdown-item" data-select="1"><a href="/general/solusi/automotive">Automotive</a></div>
                    <div class="selection-value dropdown-item" data-select="2"><a href="/general/solusi/cargo">Cargo</a></div>
                    <div class="selection-value dropdown-item" data-select="3"><a href="/general/solusi/casualty">Casualty</a></div>
                    <div class="selection-value dropdown-item" data-select="4"><a href="/general/solusi/engineering">Engineering</a></div>
                    <div class="selection-value dropdown-item" data-select="5"><a href="/general/solusi/glasses">Glasses</a></div>
                    <div class="selection-value dropdown-item" data-select="6"><a href="/general/solusi/micro">Micro</a></div>
                    <div class="selection-value dropdown-item" data-select="7"><a href="/general/solusi/property">Property</a></div>
                </div>
            </div>
        </form>
        <!-- <h2>Saya butuh asuransi </h2> -->

        <div><p><?= $this->category->getDescription() ?></p></div>

    </div>
</section>

<section class="py-main section-insurance-life">
    <div class="container">
        <div class="row">

            <?php foreach ($this->product as $p){?>
                <div class="col-md-4">
                    <a href="<?= $p->getPage()?>" class="card card-news-insurance">
                        <div class="card-img-top" style="background: url(<?php echo $p->getImage(); ?>) no-repeat center; background-size: cover;"></div>
                        <div class="card-body">
                            <p class="card-label">Life insurance</p>
                            <h5 class="card-title text-truncate"><?= $p->getName()?></h5>
                            <p class="card-text text-truncate-multiline">
                                <?= str_replace(['<p>', '</p>'], ['<br>', '</br>'], $p->getSummary())?>
                            </p>
                            <!-- <a href="#" class="card-link">Lihat detail</a> -->
                        </div>
                    </a>
                </div>
            <?php } ?>

        </div>
    </div>
</section>

<script>
    //dropdown
    let url = window.location.href.split('?')[0];
    let dp = url.substr(url.lastIndexOf('/') + 1);
    document.getElementById('select-value').innerText = dp.charAt(0).toUpperCase() + dp.substr(1);

    //dropdown custom
    $(".selection-value").click(function(e) {
        const selected = e.target.dataset.select;
        switch(selected) {
            case '0':
                $('#select-value').html('Accident');
                break;
            case '1':
                $('#select-value').html('Automotive');
                break;
            case '2':
                $('#select-value').html('Cargo');
                break;
            case '3':
                $('#select-value').html('Casualty');
                break;
            case '4':
                $('#select-value').html('Engineering');
                break;
            case '5':
                $('#select-value').html('Glasses');
                break;
            case '6':
                $('#select-value').html('Micro');
                break;
            case '7':
                $('#select-value').html('Property');
                break;
        }
    });
</script>