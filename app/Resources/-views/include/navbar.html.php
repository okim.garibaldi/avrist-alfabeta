<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

$category = new \AppBundle\Controller\DefaultController();

$individu = $category->LifePersonalCategory();
$bisnis = $category->LifeBusinessCategory();
$general = $category->GeneralCategory();
$asset = $category->AssetCategory();
$siteCatLife = $category->siteCategoryLife();
$siteCatGeneral = $category->siteCategoryGeneral();
$siteCatAsset = $category->siteCategoryAsset();
?>

<?php
    $prefix = "avrist.dnatechnology.co.id";
?>

<nav class="navbar navbar-avrist navbar-absolute fixed-top navbar-expand-lg">
    <div class="container position-relative">
        <a class="navbar-brand" href="http://<?= $prefix ?>">
            <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/brand/logo_avrist.png')?>" class="logo" alt="Logo">
        </a>
        <button class="navbar-toggler navbar-toggler-left" type="button" data-toggle="collapse" data-target="#navbarContent" aria-controls="navbarContent" aria-expanded="false" aria-label="Toggle navigation">
            <i class="fal fa-bars"></i>
        </button>
        <a class="search-mobile" href="#modalSearch" data-toggle="modal">
            <i class="fal fa-search"></i>
        </a>

        <div class="collapse navbar-collapse" id="navbarContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <div class="position-relative">
                        <input class="form-control nav-search form-control-sm" type="search" placeholder="Search" id="nav-search"/>
                        <i class="fal fa-search search-icon font-size-sm"></i>
                    </div>
                </li>
            </ul>
            <ul class="navbar-nav ml-auto">
                <li class="nav-item" id="navSolusi">
                    <div class="dropdown clearfix">
                        <a href="#" class="nav-link animation-underline float-left">
                            Solusi
                        </a>
                        <a href="#" class="float-right" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fal fa-angle-down text-primary" id="angle-dropdown"></i>
                        </a>
                        <div class="dropdown-menu animated fadeIn" id="dropdown-menu-1" >
                            <!-- <a class="dropdown-item" id="navLifeInsurance">Life Insurance</a>
                            <a class="dropdown-item" id="navGeneralInsurance">General Insurance</a>
                            <a class="dropdown-item" id="navAssetManagement">Asset Management</a> -->
                            <div class="blocks-menus">
                                <div class="container">
                                    <ul class="primary-menus">
                                        <li class="menus-megamenus">
                                            <a href="http://<?= $prefix ?>/life-insurance">Life Insurance <i class="fa fa-angle-down"></i></a>
                                            <ul class="mega-menus">
                                                <div class="row">
                                                    <div class="col-md-8 scroll-bar">
                                                        <p class="megamenu-information"><?= $siteCatLife->getDescription() ?></p>
                                                        <a href="http://<?php echo $prefix;?>/life-insurance" class="d-block text-link">PELAJARI TENTANG LIFE INSURANCE</a>
                                                        <div class="content-megamenu mt-4">
                                                            <div class="heading">
                                                                <h2>Personal</h2>
                                                            </div>
                                                            <div class="row">

                                                                <?php foreach ($individu as $personal) {?>
                                                                <div class="col-md-4">
                                                                    <div class="card card-boxless card-megamenu">
                                                                        <a class="link-arrow font-weight-bold" href="<?= $personal->getPage() ?>"><?= $personal->getName() ?> <i class="fa fa-chevron-right d-inline-block p-0"></i></a>
                                                                        <p><?= $personal->getDescription() ?></p>
                                                                    </div>
                                                                </div>
                                                                <?php } ?>

                                                            </div>
                                                        </div>
                                                        <div class="content-megamenu">
                                                            <div class="heading">
                                                                <h2>Business</h2>
                                                            </div>
                                                            <div class="row">

                                                                <?php foreach ($bisnis as $business) {?>
                                                                    <div class="col-md-4">
                                                                        <div class="card card-boxless card-megamenu">
                                                                            <a class="link-arrow font-weight-bold" href="<?= $business->getPage() ?>"><?= $business->getName() ?> <i class="fa fa-chevron-right d-inline-block p-0"></i></a>
                                                                            <p><?= $business->getDescription() ?></p>
                                                                        </div>
                                                                    </div>
                                                                <?php } ?>

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <a class="card card-megamenu-right" style="background: url(http://<?php echo $prefix;?>/template_avrist/assets/img/common/Mask.png) no-repeat center; background-size: cover; min-height: 330px;">
                                                            <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/common/ic_ilustrasi_home_menus.png')?>" class="img-fluid">
                                                            <div class="content">
                                                                <h2 class="text-white">Butuh solusi ideal untuk bisnis/personal?</h2>
                                                                <p href="http://<?php echo $prefix;?>/get-solution/personal" class="link-arrow mb-0">
                                                                    Get Solution <i class="fas fa-chevron-right d-inline-block p-0"></i>
                                                                </p>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            </ul>
                                        </li>
                                        <li class="menus-megamenus">
                                            <a href="http://general.<?php echo $prefix;?>/">General Insurance <i class="fa fa-angle-down"></i></a>
                                            <ul class="mega-menus">
                                                <div class="row">
                                                    <div class="col-md-8">
                                                        <p class="megamenu-information"><?= $siteCatGeneral->getDescription() ?></p>
                                                        <a href="http://general.<?php echo $prefix;?>/" class="d-block text-link">PELAJARI TENTANG GENERAL INSURANCE</a>
                                                        <div class="content-megamenu mt-4">
                                                            <div class="row">

                                                                <?php foreach ($general as $gen){ ?>
                                                                <div class="col-md-4">
                                                                    <div class="card card-boxless card-megamenu">
                                                                        <a href="<?= $gen->getPage() ?>" class="link-arrow font-weight-bold"><?= $gen->getName() ?> <i class="fa fa-chevron-right d-inline-block p-0"></i></a>
                                                                        <p><?= $gen->getDescription() ?></p>
                                                                    </div>
                                                                </div>
                                                                <?php } ?>

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <a class="card card-megamenu-right" style="background: url(http://<?php echo $prefix;?>/template_avrist/assets/img/common/Mask.png) no-repeat center; background-size: cover; min-height: 330px;">
                                                            <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/common/ic_ilustrasi_home_menus.png')?>" class="img-fluid">
                                                            <div class="content">
                                                                <h2 class="text-white">Butuh solusi ideal untuk bisnis/personal?</h2>
                                                                <p href="http://<?php echo $prefix;?>/get-solution/personal" class="link-arrow mb-0">
                                                                    Get Solution <i class="fas fa-chevron-right d-inline-block p-0"></i>
                                                                </p>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            </ul>
                                        </li>
                                        <li class="menus-megamenus">
                                            <a href="http://am.<?php echo $prefix;?>/">Asset Management<i class="fa fa-angle-down"></i></a>
                                            <ul class="mega-menus">
                                                <div class="row">
                                                    <div class="col-md-8">
                                                        <p class="megamenu-information"><?= $siteCatAsset->getDescription() ?></p>
                                                        <a href="http://am.<?php echo $prefix;?>/" class="d-block text-link">PELAJARI TENTANG ASSET MANAGEMENT</a>
                                                        <div class="content-megamenu mt-4">
                                                            <div class="row">

                                                                <?php foreach ($asset as $ast){ ?>
                                                                <div class="col-md-4">
                                                                    <div class="card card-boxless card-megamenu">
                                                                        <a href="<?= $ast->getPage() ?>" class="link-arrow font-weight-bold"><?= $ast->getName() ?> <i class="fa fa-chevron-right d-inline-block p-0"></i></a>
                                                                        <p><?= $ast->getDescription() ?></p>
                                                                    </div>
                                                                </div>
                                                                <?php } ?>

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <a class="card card-megamenu-right" style="background: url(http://<?php echo $prefix;?>/template_avrist/assets/img/common/Mask.png) no-repeat center; background-size: cover; min-height: 330px;">
                                                            <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/common/ic_ilustrasi_home_menus.png')?>" class="img-fluid">
                                                            <div class="content">
                                                                <h2 class="text-white">Butuh solusi ideal untuk bisnis/personal?</h2>
                                                                <p href="http://<?php echo $prefix;?>/get-solution/personal" class="link-arrow mb-0">
                                                                    Get Solution <i class="fas fa-chevron-right d-inline-block p-0"></i>
                                                                </p>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="nav-item" id="navAbout">
                    <a class="nav-link animation-underline" href="http://<?php echo $prefix;?>/about">Tentang</a>
                </li>
                <li class="nav-item" id="navFaq">
                    <a class="nav-link animation-underline" href="http://<?php echo $prefix;?>/faq">FAQ</a>
                </li>

                <li class="nav-item" id="navContact">
                    <a class="nav-link animation-underline" href="http://<?php echo $prefix;?>/contact">Kontak</a>
                </li>
            </ul>
        </div>

        <div class="navbar-right">
            <ul class="navbar-nav">
                <li class="nav-item dropdown">
                    <a class="nav-link btn btn-primary text-white" href="/login" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Login
                    </a>
                    <div class="dropdown-menu dropdown-login" aria-labelledby="navbarDropdown">
                        <div class="login-top">
                            <form class="needs-validation" onsubmit="submitLogin(); return false;" novalidate>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="heading">
                                            <h4>Login</h4>
                                            <p>Login to customer dashboard</p>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="">E-mail</label>
                                            <input type="email" class="form-control" id="loginUsername" name="loginUsername" placeholder="E-mail" value="" required>
                                            <div class="invalid-feedback">
                                                Please enter email
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="">Last name</label>
                                            <input type="password" class="form-control" id="loginPassword" name="loginPassword" placeholder="Password" value="" required>
                                            <div class="invalid-feedback">
                                                Please enter password
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 mb-4">
                                        <a href="#" class="float-right forgot-pass" onclick="submitForgot();">Lupa Password</a>
                                    </div>
                                    <div class="col-12">
                                        <button class="btn btn-primary btn-block" type="submit" style="background-color: #432862 !important;">Login</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <a href="#" target="_blank">
                            <div class="login-bottom">
                                <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/icon/ic_login.png')?>" class="login-icon" alt="Login Icon">
                                <div class="text-group">
                                    <a href="#" onclick="submitRegister();">Belum punya Akun? <br> Daftar Sekarang</a>
                                </div>
                            </div>
                        </a>
                    </div>
                </li>
            </ul>
        </div>

    </div>

    <div class="navbar-slide">
        <div class="navbar-slide-close">
            <span class="icon-bar icon-bar-1"></span>
            <span class="icon-bar icon-bar-2"></span>
            <span class="icon-bar icon-bar-3"></span>
        </div>
        <div class="content">
            <ul class="nav-slide-list">
                <li class="nav-slide-item dropdown" id="navSolusi">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Solusi
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item dropdown-item--purple" style="color:#999 !important;" id="nav-slide_product-1">Life Insurance</a>
                        <a class="dropdown-item dropdown-item--purple" style="color:#999 !important;" id="nav-slide_product-2">General Insurance</a>
                        <a class="dropdown-item dropdown-item--purple" style="color:#999 !important;" id="nav-slide_product-3">Asset Management</a>
                    </div>
                </li>
                <li class="nav-slide-item" id="navAbout">
                    <a class="nav-link" href="http://<?php echo $prefix;?>/about">
                        <span>Tentang</span>
                    </a>
                </li>
                <li class="nav-slide-item" id="navFaq">
                    <a class="nav-link" href="http://<?php echo $prefix;?>/faq">
                        <span>FAQ</span>
                    </a>
                </li>
                <li class="nav-slide-item" id="navContact">
                    <a class="nav-link" href="http://<?php echo $prefix;?>/contact">
                        <span>Kontak</span>
                    </a>
                </li>
            </ul>
            <a href="/login" class="btn btn-primary">Login</a>
        </div>
    </div>

    <div class="product-slide" id="product-slide-1">
        <label class="product-title-slide" id="product-back-1"><i class="fa fa-angle-left"></i> General Insurance</label>
        <p class="navbar-slide-information">Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. </p>
        <a href="http://<?php echo $prefix;?>/life-insurance" class="btn-link">Pelajari tentang life insurance <i class="fa fa-angle-right"></i></a>
        <ul class="mega-menu-down">

            <?php foreach ($individu as $personal) {?>
            <li><a href="<?= $personal->getPage() ?>"><?= $personal->getName() ?> <i class="fa fa-angle-right"></i></a></li>
            <?php } ?>

            <?php foreach ($bisnis as $business) {?>
            <li><a href="<?= $business->getPage() ?>"><?= $business->getName() ?> <i class="fa fa-angle-right"></i></a></li>
            <?php } ?>

        </ul>
    </div>
    <div class="product-slide" id="product-slide-2">
        <label class="product-title-slide" id="product-back-2"><i class="fa fa-angle-left"></i> Life Insurance</label>
        <p class="navbar-slide-information">Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. </p>
        <a href="http://general.<?php echo $prefix;?>/" class="btn-link">Pelajari tentang life insurance <i class="fa fa-angle-right"></i></a>
        <ul class="mega-menu-down">

            <?php foreach ($general as $gen){ ?>
            <li><a href="<?= $gen->getPage() ?>"><?= $gen->getName() ?> <i class="fa fa-angle-right"></i></a></li>
            <?php } ?>

        </ul>
    </div>

    <div class="product-slide" id="product-slide-3">
        <label class="product-title-slide" id="product-back-3"><i class="fa fa-angle-left"></i> Asset Management</label>
        <p class="navbar-slide-information">Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. </p>
        <a href="#" class="btn-link">Pelajari tentang life insurance <i class="fa fa-angle-right"></i></a>
        <ul class="mega-menu-down">

            <?php foreach ($asset as $ast){ ?>
            <li><a href="<?= $ast->getPage() ?>"><?= $ast->getName() ?> <i class="fa fa-angle-right"></i></a></li>
            <?php } ?>

        </ul>
    </div>

    <div class="dropdown-menu mega-menu" id="megaMenuLifeInsurance">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <div class="megamenu-section">
                        <p class="megamenu-information">Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. </p>
                        <a href="http://<?php echo $prefix;?>/life-insurance" class="">PELAJARI TENTANG LIFE INSURANCE</a>
                    </div>

                    <div class="megamenu-section-bottom">
                        <div class="heading">
                            <h2>Personal</h2>
                        </div>
                        <div class="row">

                            <?php foreach ($individu as $personal) {?>
                            <div class="col-md-4">
                                <div class="card card-boxless card-megamenu">
                                    <div class="link-w-arrow">
                                        <a href="<?= $personal->getPage() ?>"><?= $personal->getName() ?></a>
                                        <i class="fa fa-chevron-right"></i>
                                    </div>
                                    <p><?= $personal->getDescription() ?></p>
                                </div>
                            </div>
                            <?php } ?>

                        </div>
                    </div>

                    <div class="megamenu-section-bottom">
                        <div class="heading">
                            <h2>Business</h2>
                        </div>
                        <div class="row">

                            <?php foreach ($bisnis as $business) {?>
                            <div class="col-md-4">
                                <div class="card card-boxless card-megamenu">
                                    <div class="link-w-arrow">
                                        <a href="<?= $business->getPage() ?>"><?= $business->getName() ?></a>
                                        <i class="fa fa-chevron-right"></i>
                                    </div>
                                    <p><?= $business->getDescription() ?></p>
                                </div>
                            </div>
                            <?php } ?>

                        </div>
                    </div>

                </div>
                <div class="col-md-3">
                    <div class="card card-megamenu-right" style="background: url(http://<?php echo $prefix;?>/template_avrist/assets/img/common/Mask.png) no-repeat center; background-size: cover; min-height: 330px;">
                        <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/common/ic_ilustrasi_home_menus.png')?>" class="img-fluid">
                        <p class="text-white">Butuh solusi ideal untuk bisnis/personal?</p>
                        <a href="http://<?php echo $prefix;?>/get-solution/personal" class="btn btn-secondary btn-block">Get Solution</a>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="dropdown-menu mega-menu" id="megaMenuGeneralInsurance">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <div class="megamenu-section">
                        <p class="megamenu-information">Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. </p>
                        <a href="http://general.<?php echo $prefix;?>/" class="">PELAJARI TENTANG GENERAL INSURANCE</a>
                    </div>

                    <div class="megamenu-section-bottom">
                        <div class="row">

                            <?php foreach ($general as $gen){ ?>
                            <div class="col-md-4">
                                <div class="card card-boxless card-megamenu">
                                    <div class="link-w-arrow">
                                        <a href="<?= $gen->getPage() ?>"><?= $gen->getName() ?></a>
                                        <i class="fa fa-chevron-right"></i>
                                    </div>
                                    <p><?= $gen->getDescription() ?></p>
                                </div>
                            </div>
                            <?php } ?>

                        </div>
                    </div>

                </div>
                <div class="col-md-3">
                    <div class="card card-megamenu-right" style="background: url(http://<?php echo $prefix;?>/template_avrist/assets/img/common/Mask.png) no-repeat center; background-size: cover; min-height: 330px;">
                        <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/common/ic_ilustrasi_home_menus.png')?>" class="img-fluid">
                        <p class="text-white">Butuh solusi ideal untuk bisnis/personal?</p>
                        <a href="<?php echo $prefix;?>/get-solution/personal" class="btn btn-secondary btn-block">Get Solution</a>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="dropdown-menu mega-menu" id="megaMenuAssetManagement">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <div class="megamenu-section">
                        <p class="megamenu-information">Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. </p>
                        <a href="http://am.<?php echo $prefix;?>/" class="">PELAJARI TENTANG ASSET MANAGEMENT</a>
                    </div>

                    <div class="megamenu-section-bottom">
                        <div class="row">

                            <?php foreach ($asset as $ast){ ?>
                            <div class="col-md-4">
                                <div class="card card-boxless card-megamenu">
                                    <div class="link-w-arrow">
                                        <a href="<?= $ast->getPage() ?>"><?= $ast->getName() ?></a>
                                        <i class="fa fa-chevron-right"></i>
                                    </div>
                                    <p><?= $ast->getDescription() ?></p>
                                </div>
                            </div>
                            <?php } ?>

                        </div>
                    </div>

                </div>
                <div class="col-md-3">
                    <div class="card card-megamenu-right" style="background: url(http://<?php echo $prefix;?>/template_avrist/assets/img/common/Mask.png) no-repeat center; background-size: cover; min-height: 330px;">
                        <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/common/ic_ilustrasi_home_menus.png')?>" class="img-fluid">
                        <p class="text-white">Butuh solusi ideal untuk bisnis/personal?</p>
                        <a href="<?php echo $prefix;?>/get-solution/personal" class="btn btn-secondary btn-block">Get Solution</a>
                    </div>
                </div>
            </div>

        </div>
    </div>

</nav>

<!--START: Modal Search-->
<!-- <div class="modal modal-full" tabindex="-1" role="dialog" id="modalSearch">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body p-box">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"><i class="fal fa-times"></i></span>
        </button>
        <form class="needs-validation" novalidate>
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <input type="text" class="form-control" autocomplete="off" placeholder="search" id="search" required="">
                <div class="invalid-feedback">
                  Please input date
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div> -->
<!--END: Modal Search-->



