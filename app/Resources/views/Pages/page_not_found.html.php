<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

use Pimcore\Model\Document;
use Pimcore\Model\Document\Page;

$this->extend('layout_avrist_new.html.php');?> 

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
  <head>
    <?= $this->template("include/head2.html.php"); ?>
  </head>
  <body>

    <?= $this->template("include/navbar3.html.php"); ?>

    <div class="cover cover-full" style="background: url(/template_avrist/assets/img/common/bg_rectangle-light-1.jpg) no-repeat center; background-size: cover;">
        <div class="container justify-content-center">
          <div class="cover-content text-center">
            <h1 class="cover-title animated fadeInUp delayp1">Oops.. something went wrong.</h1>
            <p class="cover-text animated fadeInUp delayp2">Sorry, but the page you were trying to view does not exist.</p>
            <a class="btn btn-primary animated fadeInUp delayp3" href="/" role="button" target="_top">Go Back to Home</a>
          </div>
        </div>
      </div>

    </body>

    <?= $this->template("include/script2.html.php"); ?>

  </body>
</html>
