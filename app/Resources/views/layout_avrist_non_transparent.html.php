<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

use Pimcore\Model\Document;
use Pimcore\Model\Document\Page;
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
    <?php $prefix="157.230.240.113:8891";?>
    <?= $this->template("include/head.html.php"); ?>
    <?= $this->template("include/tracking-script/head.html.php"); ?>

    <style>
        .slider {
            position: relative;
            height: 3rem;
            margin: 3rem 0;
            width: 80%;
            display: flex;
            flex-direction: column;
        }

        .slider-output {
            display: block;
            position: absolute;
            bottom: -0.5rem;
            left: 25%;
            box-sizing: border-box;
            transform: translateX(-50%);
            font-weight: bold;
            font-size: 1.5rem;
        }

        .slider-input {
            -webkit-appearance: none;
            background-color: transparent;
            position: absolute;
            width: 105%;
            margin-left: -2.5%;
            height: 2rem;
            outline: none;
        }

        .slider-input::-webkit-slider-thumb {
            -webkit-appearance: none;
            width: 2rem;
            height: 8rem;
        }

        .slider-input::hover::-webkite-slider-thumb,
        .slider-input::focus::-webkite-slider-thumb {
            box-shadow: 0 0 0 10px #fff, 0 0 0 6px #F4B635;
        }


        .slider-thumb,
        .top-thumb,
        .tail-thumb {
            height: 1.5rem;
            width: 1.5rem;
            border-radius: 1rem;
            position: absolute;
            top: -.6rem;
            background-color: #F4B635;
            cursor: pointer;
        }

        .slider-thumb {
            left: 25%;
            margin-left: -1rem;
        }

        .top-thumb {
            left: 0;
            margin-left: -1rem;

        }

        .tail-thumb {
            right: 0;
            margin-right: -1rem;
        }

        .slider-track {
            height: .35rem;
            background-color: #ddd;
            border-radius: .25rem;
        }

        .slider-level {
            height: .35rem;
            background-color: #F4B635;
            width: 25%;
            border-radius: 1rem;
        }

        .navbar.fixed-top + * {
            margin-top: 0;
        }


        .navbar.fixed-top + * {
            margin-top: 0;
        }

        .confirm-alert .alert-success {
            background-color: #B9D137;
            color: #fff;
        }

        .section-insurance-content.show {
            display: block !important;
        }
        .section-insurance-content {
            display: none;
        }

        .dropdown-item--purple {
            color: #1a0dab !important;
        }
    </style>

</head>
<body>
<?= $this->template("include/tracking-script/body.html.php"); ?>

<div class="loader-wrapper loader-light">
    <div class="loader"></div>
</div>

<?= $this->template("include/navbar.html.php"); ?>

<?php $this->slots()->output('_content') ?>

<?= $this->template("include/footer.html.php"); ?>
<?= $this->template("include/script.html.php"); ?>

<script>
    // Navbar active state
    $(function(){
        $('a').each(function(){
            if ($(this).prop('href') == window.location.href) {
                $(this).addClass('active'); $(this).parents('li').addClass('active');
            }
        });
    });
    //  $('#navAbout').addClass('active');

    // Animation on load
    document.addEventListener("DOMContentLoaded", function(event) {
        $(".loader").fadeOut('slow');
        $(".loader-wrapper").fadeOut("slow");
        //  $(".anim-1").addClass("animated fadeInLeft delayp10");
        // $(".anim-2").addClass("animated fadeInUp delayp12");
        //$(".anim-3").addClass("animated fadeInUp delayp14");
    });

    $('.owl-solution').owlCarousel({
        loop: false,
        margin: 7.5,
        dots: false,
        responsive:{
            0:{
                items: 1
            },
            768:{
                margin: 30,
                items: 3
            }
        }
    });

    $('.owl-kebutuhan').owlCarousel({
        loop: false,
        margin: 7.5,
        dots: false,
        responsive:{
            0:{
                items: 1
            },
            768:{
                margin: 30,
                items: 3
            }
        }
    });

    $('#headingFAQ1').click(function(){
        $('#faqHome').attr("src","/template_avrist/assets/img/common/mengapa_avrist.gif");
    });

    $('#headingFAQ2').click(function(){
        $('#faqHome').attr("src","/template_avrist/assets/img/common/premi_rendah2.gif");
    });

    $('#headingFAQ3').click(function(){
        $('#faqHome').attr("src","/template_avrist/assets/img/common/proteksi_lengkap.gif");
    });

    $('.navbar-toggler').click(function(){
        $('.navbar').removeClass("navbar-transparent");
    });
    $('.navbar-slide-close').click(function(){
        $('.navbar').addClass("navbar-transparent");
    });

</script>

</body>
</html>
