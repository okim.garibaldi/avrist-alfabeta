<script>

/* ==========================================================================
   JS for personal product
   ========================================================================== */

// Navbar active state
// $('#navHome').addClass('active');

// Animation on load
document.addEventListener("DOMContentLoaded", function(event) {
    $(".loader").fadeOut('slow');
    $(".loader-wrapper").fadeOut("slow");
    //  $(".anim-1").addClass("animated fadeInLeft delayp10");
    // $(".anim-2").addClass("animated fadeInUp delayp12");
    //$(".anim-3").addClass("animated fadeInUp delayp14");  
});

// di local ../../../../ 
// di staging ../../../
var logo = ["../../../assets/img/brand/logo_avrist.png", "../../../assets/img/brand/logo_avrist_white.png"];

$(".navbar").addClass('navbar-transparent');
$('.navbar-avrist .navbar-brand img').attr('src', logo[1]);

$(window).on("scroll", function() {
    if ($(window).scrollTop() > 10) {
        $(".navbar-avrist").removeClass("navbar-transparent");
        $('.navbar-avrist .navbar-brand img').attr('src', logo[0]);
    } else {
        //remove the background property so it comes transparent again (defined in your css)
        $(".navbar-avrist").addClass("navbar-transparent");
        $('.navbar-avrist .navbar-brand img').attr('src', logo[1]);
    }
});

$('.owl-insurance').owlCarousel({
    loop: false,
    margin: 7.5,
    dots: false,
    responsive: {
        0: {
            items: 1,
            margin: 20,
            dots: true
        },
        768: {
            margin: 15,
            items: 4
        }
    }
});

$('.owl-kebutuhan').owlCarousel({
    loop: false,
    margin: 7.5,
    dots: false,
    responsive: {
        0: {
            items: 1
        },
        768: {
            margin: 30,
            items: 3
        }
    }
});

//dropdown custom
$(".selection-value").click(function(e) {
    const selected = e.target.dataset.select;
    switch (selected) {
        case '0':
            $('#select-value').html('Jiwa');
            break;
        case '1':
            $('#select-value').html('Jiwa Syariah');
            break;
        case '2':
            $('#select-value').html('Kecelakaan & Kesehatan');
            break;
        case '3':
            $('#select-value').html('Edukasi');
            break;
        case '4':
            $('#select-value').html('Karyawan');
            break;
        case '5':
            $('#select-value').html('Pensiun');
            break;
    }
})

</script>