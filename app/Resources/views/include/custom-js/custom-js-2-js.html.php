<script>

/* ==========================================================================
   JS for csr details
   ========================================================================== */

// Navbar active state
$('#navTentang').addClass('active');

// Animation on load
document.addEventListener("DOMContentLoaded", function(event) {
    $(".loader").fadeOut('slow');
    $(".loader-wrapper").fadeOut("slow");
    //  $(".anim-1").addClass("animated fadeInLeft delayp10");
    // $(".anim-2").addClass("animated fadeInUp delayp12");
    //$(".anim-3").addClass("animated fadeInUp delayp14");  
});

$('.owl-insurance').owlCarousel({
    loop: false,
    margin: 7.5,
    dots: false,
    responsive: {
        0: {
            items: 1,
            margin: 20,
            dots: true
        },
        768: {
            margin: 15,
            items: 4
        }
    }
});

$('.owl-kebutuhan').owlCarousel({
    loop: false,
    margin: 7.5,
    dots: true,
    responsive: {
        0: {
            items: 1.3,
            loop: true
        },
        768: {
            margin: 30,
            items: 3
        }
    }
});

$('#arrowLeft').click(function() {
    $('.owl-prev').click();
});
$('#arrowRight').click(function() {
    $('.owl-next').click();
});
</script>