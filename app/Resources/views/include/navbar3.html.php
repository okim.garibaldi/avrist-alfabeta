<nav class="navbar navbar-avrist navbar-absolute fixed-top navbar-expand-lg navbar-scroll">
  <div class="container position-relative">
    <button class="navbar-toggler navbar-toggler-left" type="button" data-toggle="collapse" data-target="#navbarContent" aria-controls="navbarContent" aria-expanded="false" aria-label="Toggle navigation">
      <i class="fal fa-bars"></i>
    </button>
    <a class="search-mobile" href="#modalSearch" data-toggle="modal">
      <i class="fal fa-search"></i>
    </a>

    <div class="collapse navbar-collapse" id="navbarContent">
      <ul class="navbar-nav mr-auto">
      <?php $navItem = ["navBeranda", "navSolusi", "navTentang", "navBantuan"];  ?>
      <?php $navItemCounter = 0; ?>
      <?php foreach($this->navbar["Men906Men"] as $m):?>
        <li class="nav-item" id=<?= $navItem[$navItemCounter] ?>>
        <?php if ($this->navbar["Men906".substr($m->getKey(),0,3)]->getTotalCount() == 0): ?>
          <a class="nav-link animation-underline" href="<?= $m->getPage() ?>"><?= $m->getTitle() ?></a>
        <?php elseif ($this->navbar["Men906".substr($m->getKey(),0,3)]->getTotalCount() > 0): ?>
          <div class="dropdown clearfix">
            <a href="#" class="nav-link animation-underline float-left">
              <?= $m->getTitle() ?>
            </a>
            <a href="#" class="float-right" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <i class="fal fa-angle-down text-primary" id="angle-dropdown"></i>
            </a>
            <div class="dropdown-menu animated fadeIn dropdown-menu-general dropdown-menu-1">
              <!-- <a class="dropdown-item" id="navLifeInsurance">Life Insurance</a>
              <a class="dropdown-item" id="navGeneralInsurance">General Insurance</a>
              <a class="dropdown-item" id="navAssetManagement">Asset Management</a> -->
              <div class="blocks-menus">
                <div class="container">
                  <ul class="primary-menus">
                  <?php $count = 0; ?>
                  <?php $megaMenus = ['', ' general-insurance', ' asset-management']; ?>
                  <?php foreach($this->navbar["Men906".substr($m->getKey(),0,3)] as $h):?>
                    <li class="menus-megamenus <?= $megaMenus[$count] ?>">
                      <a class="nav-link-menus" href="
                      <?php if ($h->getPrefix()){
                        echo $this->navbar["url"]->{"get".$h->getPrefix()}();
                      } 
                      else {
                        echo $h->getPermalink();
                      } ?>"><?= $h->getTitle(); ?> <i class="fa fa-angle-down"></i></a>
                    <?php if($this->navbar[substr($m->getKey(),0,3).$m->getId().substr($h->getKey(),0,3)]->getTotalCount() > 0):?>
                      <ul class="mega-menus">
                        <div class="row">
                          <div class="col-md-8 scroll-bar">
                              <p class="megamenu-information"><?= $h->getDescription() ?> </p>
                            <a href="
                            <?php if ($h->getPrefix()){
                              echo $this->navbar["url"]->{"get".$h->getPrefix()}();
                            } 
                            else {
                              echo $h->getPermalink();
                            } ?>" class="d-block text-link link-arrow"><?= $h->getTeks()?> <i class="fa fa-chevron-right d-inline-block p-0"></i></a>
                          <?php foreach($this->navbar[substr($m->getKey(),0,3).$m->getId().substr($h->getKey(),0,3)] as $i):?>
                            <div class="content-megamenu mt-4">
                            <?php if($i->getDisabled() == FALSE): ?>
                              <div class="heading">
                                <h2><?= $i->getTitle(); ?></h2>
                              </div>
                            <?php endif; ?>  
                              <div class="row">
                              <?php foreach($this->navbar[substr($h->getKey(),0,3).$h->getId().substr($i->getKey(),0,3)] as $j):?>
                                <div class="col-md-4">
                                  <div class="card card-boxless card-megamenu">
                                      <a class="link-arrow font-weight-bold" href="
                                      <?php 
                                      if (!($j->getPrefixDisabled()))
                                      {
                                        if ($h->getPrefix()){
                                          echo $this->navbar["url"]->{"get".$h->getPrefix()}().$j->getLink();
                                        } 
                                        else {
                                          echo $h->getPermalink().$j->getLink();;
                                        }
                                      }
                                      else
                                      {
                                        echo $j->getLink();
                                      }
                                       
                                      ?>"><?= $j->getName() ?> <i class="fa fa-chevron-right d-inline-block p-0"></i></a>
                                    <p><?= $j->getDescription() ?></p>
                                  </div>
                                </div>
                              <?php endforeach; ?>
                              </div>
                            </div>
                          <?php endforeach; ?>
                          </div>
                          <div class="col-md-4">
                        <?php if($navItemCounter == 1): ?>
                          <?php if($count != 2): ?>
                            <a href="<?= $h->getSideLink() ?>" class="card card-megamenu-right" style="background: url(/template_avrist/assets/img/common/Mask.png) no-repeat center; background-size: cover; min-height: 330px;">
                              <img src="<?= $h->getImage() ?>" class="img-fluid">
                              <div class="content">
                                <h2 class="text-white"><?= $h->getText1() ?></h2>
                                <p href="#" class="link-arrow mb-0">
                                  <?= $h->getText2() ?> <i class="fas fa-chevron-right d-inline-block p-0"></i>
                                </p>
                              </div>
                            </a>
                          <?php else: ?>
                            <a href="#" class="card card-megamenu-right" style="background: url(/template_avrist/assets/img/common/Mask.png) no-repeat center; background-size: cover; min-height: 330px;">
                              <img src="<?= $h->getImage() ?>" class="img-fluid">
                              <div class="content">
                                <h2 class="text-white"><?= $h->getText1() ?></h2>
                                <p href="<?= $h->getSideLink() ?>" class="link-arrow mb-0">
                                  <?= $h->getText2() ?><i class="fas fa-chevron-right d-inline-block p-0"></i>
                                </p>
                              </div>
                            </a>
                          <?php endif; ?>
                        <?php else: ?>

                          <?php foreach ($this->navbar['rss_feed']->channel->item as $feed_item) : ?>
                              <div class="card card-solution card-megamenus">
                                <div class="card-img-top" style="background: url(http://avrist.com/lifeguide/wp-content/uploads/2020/02/Photo-by-Ben-White-on-Unsplash-534x462.jpg) no-repeat center; background-size: cover;"></div>
                                <div class="card-body">
                                  <p class="card-label"></p>
                                  <h5 class="card-title text-truncate"><?= $feed_item->title ?></h5>
                                  <p class="card-text"><?= substr($feed_item->title, 0, 97)."..." ?></p>
                                  <a href="<?= $feed_item->link ?>" class="card-link">Lihat detail</a>
                                </div>
                              </div>
                            <?php break; ?>
                          <?php endforeach; ?>

                        <?php endif; ?>
                          </div>
                        </div>
                      </ul>
                    <?php endif;?>
                    </li>
                    <?php $count++; ?>  
                  <?php endforeach;?>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        <?php endif; ?>
        </li>
        <?php $navItemCounter++; ?>
      <?php endforeach; ?>
      </ul>
      <ul class="navbar-nav ml-auto">
        <li class="nav-item">
          <div class="position-relative">
            <input class="form-control nav-search form-control-sm" type="search" placeholder="Search" id="nav-search"/>
            <i class="fal fa-search search-icon font-size-sm"></i>
          </div>
        </li>
      </ul>
    </div>

    <div class="navbar-right">
      <ul class="navbar-nav">
        <li class="nav-item dropdown">
          <a class="nav-link btn btn-primary text-white" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Login
          </a>
          <div class="dropdown-menu dropdown-login" aria-labelledby="navbarDropdown">
            <div class="login-top">
              <form class="needs-validation" method="POST" onsubmit="submitLogin(); return false;" novalidate>
                <div class="row">
                    <div class="col-md-12">
                        <div class="heading">
                            <h4>Login</h4>
                            <p>Login to customer dashboard</p>
                            <div class="ContainerLeft MessageContainer" id="divMessageMain">
                                <img class="Grow1">
                                <dl class="Grow1">
                                    <dt></dt>
                                    <dd></dd>
                                </dl>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="">E-mail</label>
                            <input type="email" class="form-control" id="loginUsername" name="loginUsername" placeholder="E-mail" value="" required>
                            <div class="invalid-feedback">
                                Please enter email
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="">Password</label>
                            <input type="password" class="form-control" id="loginPassword" name="loginPassword" placeholder="Password" value="" required>
                            <div class="invalid-feedback">
                                Please enter password
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 mb-4">
                        <a href="#" class="float-right forgot-pass" onclick="submitForgot();">Lupa Password</a>
                    </div>
                    <div class="col-12">
                        <button class="btn btn-primary btn-block" type="submit" style="background-color: #432862 !important;">Login</button>
                    </div>
                </div>
              </form>
            </div>
            <a href="#" target="_blank">
              <div class="login-bottom">
                <img src="/template_avrist/assets/img/icon/ic_login.png" class="login-icon" alt="Login Icon">
                <div class="text-group">
                  <a href="#">Belum punya Akun? <br> Daftar Sekarang</a>
                </div>
              </div>
            </a>
          </div>
        </li>
      </ul>
    </div>
    <a class="navbar-brand" href="<?= $this->navbar["beranda"] ?>">
      <img src="<?= $this->navbar["logo"]->getLogo() ?>" class="logo" alt="Logo">
    </a>

  </div>

  <div class="navbar-slide">
    <div class="navbar-slide-close">
      <span class="icon-bar icon-bar-1"></span>
      <span class="icon-bar icon-bar-2"></span>
      <span class="icon-bar icon-bar-3"></span>
    </div>
    <div class="content">
      <ul class="nav-slide-list">
      <?php $navItem = ["navBeranda", "navSolusi", "navTentang", "navBantuan"];  ?>
      <?php $navItemCounter = 0; ?>
      <?php foreach($this->navbar["Men906Men"] as $m):?>
        <li class="nav-slide-item" id="<?= $navItem[$navItemCounter] ?>">
          <?php $navItemCounter++; ?>
          <?php if ($this->navbar["Men906".substr($m->getKey(),0,3)]->getTotalCount() == 0): ?>
          <a class="nav-link" href="<?= $m->getPage() ?>">
            <span><?= $m->getTitle() ?></span>
          </a>
          <?php elseif ($this->navbar["Men906".substr($m->getKey(),0,3)]->getTotalCount() > 0): ?>
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <?= $m->getTitle() ?>
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <?php $navSlideCount = 1; ?>
          <?php foreach($this->navbar["Men906".substr($m->getKey(),0,3)] as $h):?>
            <a class="text-dark dropdown-item" id="nav-slide_product-<?php if($m->getTitle() != "Solusi") echo strtolower($m->getTitle()."-") ?><?= $navSlideCount++ ?>"><?= $h->getTitle(); ?></a>
          <?php endforeach; ?>
          </div>
          <?php endif; ?>
        </li>
      <?php endforeach; ?>
      </ul>
      <a href="/login" class="btn btn-primary">Login</a>
    </div>
  </div>

<?php foreach($this->navbar["Men906Men"] as $m):?>
  <?php $productSlideCount = 1; ?>
  <?php foreach($this->navbar["Men906".substr($m->getKey(),0,3)] as $h):?>
  <div class="product-slide" id="product-slide-<?php if($m->getTitle() != "Solusi") echo strtolower($m->getTitle()."-") ?><?= $productSlideCount ?>">
    <label class="product-title-slide" id="product-back-<?php if($m->getTitle() != "Solusi") echo strtolower($m->getTitle()."-") ?><?= $productSlideCount++ ?>"><i class="fa fa-angle-left"></i> <?= $h->getTitle() ?></label>
    <p class="navbar-slide-information"><?= $h->getDescription() ?></p>
    <a href="<?php 
    if ($h->getPrefix()){
        echo $this->navbar["url"]->{"get".$h->getPrefix()}();
      } 
      else {
        echo $h->getPermalink();
      }?>" class="btn-link"><?= $h->getTeks()?> <i class="fa fa-angle-right"></i></a></p>
    <ul class="mega-menu-down">
    <?php $count = 0; ?>
    <?php foreach($this->navbar[substr($m->getKey(),0,3).$m->getId().substr($h->getKey(),0,3)] as $i):?>
      <?php foreach($this->navbar[substr($h->getKey(),0,3).$h->getId().substr($i->getKey(),0,3)] as $j):?>
      <li><a href="
        <?php 
          if (!($j->getPrefixDisabled()))
          {
            if ($h->getPrefix()){
              echo $this->navbar["url"]->{"get".$h->getPrefix()}().$j->getLink();
            } 
            else {
              echo $h->getPermalink().$j->getLink();;
            }
          }
          else
          {
            echo $j->getLink();
          }
           
          ?>
          "><?= $j->getName() ?><i class="fa fa-angle-right"></i></a></li>
      <?php endforeach; ?>
    <?php endforeach; ?>
    </ul>
  </div>
  <?php endforeach; ?>
<?php endforeach; ?>

<!--
  <div class="dropdown-menu mega-menu" id="megaMenuLifeInsurance">
    <div class="container">
      <div class="row">
        <div class="col-md-9">
          <div class="megamenu-section">
            <p class="megamenu-information">Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. </p>
            <a href="/life-insurance" class="">PELAJARI TENTANG LIFE INSURANCE</a>
          </div>

          <div class="megamenu-section-bottom">
            <div class="row">
              <div class="col-md-4">
                <div class="card card-boxless card-megamenu">
                  <div class="link-w-arrow">
                    <a href="#">Avrist Oto</a>
                    <i class="fa fa-chevron-right"></i>
                  </div>
                  <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                </div>
              </div>
              <div class="col-md-4">
                <div class="card card-boxless card-megamenu">
                  <div class="link-w-arrow">
                    <a href="#">Property</a>
                    <i class="fa fa-chevron-right"></i>
                  </div>
                  <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                </div>
              </div>
              <div class="col-md-4">
                <div class="card card-boxless card-megamenu">
                  <div class="link-w-arrow">
                    <a href="#">Personal Accident</a>
                    <i class="fa fa-chevron-right"></i>
                  </div>
                  <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                </div>
              </div>
              <div class="col-md-4">
                <div class="card card-boxless card-megamenu">
                  <div class="link-w-arrow">
                    <a href="#">Engineering</a>
                    <i class="fa fa-chevron-right"></i>
                  </div>
                  <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                </div>
              </div>
              <div class="col-md-4">
                <div class="card card-boxless card-megamenu">
                  <div class="link-w-arrow">
                    <a href="#">Casualty Insurance</a>
                    <i class="fa fa-chevron-right"></i>
                  </div>
                  <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                </div>
              </div>
              <div class="col-md-4">
                <div class="card card-boxless card-megamenu">
                  <div class="link-w-arrow">
                    <a href="#">Micro Insurance</a>
                    <i class="fa fa-chevron-right"></i>
                  </div>
                  <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                </div>
              </div>
              <div class="col-md-4">
                <div class="card card-boxless card-megamenu">
                  <div class="link-w-arrow">
                    <a href="#">Glasses Protection</a>
                    <i class="fa fa-chevron-right"></i>
                  </div>
                  <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                </div>
              </div>
              <div class="col-md-4">
                <div class="card card-boxless card-megamenu">
                  <div class="link-w-arrow">
                    <a href="#">Marine Cargo</a>
                    <i class="fa fa-chevron-right"></i>
                  </div>
                  <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-3">
          <div class="card card-megamenu-right" style="background: url(/template_avrist/assets/img/common/Mask.png) no-repeat center; background-size: cover; min-height: 330px;">
            <img src="/template_avrist/assets/img/common/ic_ilustrasi_home_menus.png" class="img-fluid">
            <p class="text-white">Butuh solusi ideal untuk bisnis/personal?</p>
            <a href="/get-solution/personal" class="btn btn-secondary btn-block">Get Solution</a>
          </div>
        </div>
      </div>
      
    </div>
  </div>

  <div class="dropdown-menu mega-menu" id="megaMenuGeneralInsurance">
    <div class="container">
      <div class="row">
        <div class="col-md-9">
          <div class="megamenu-section">
            <p class="megamenu-information">Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. </p>
            <a href="/general-insurance" class="">PELAJARI TENTANG GENERAL INSURANCE <i class="fa fa-angle-right"></i></a>
          </div>

          <div class="megamenu-section-bottom">
            <div class="heading">
              <h2>Personal</h2>
            </div>
            <div class="row">
              <div class="col-md-4">
                <div class="card card-boxless card-megamenu">
                  <div class="link-w-arrow">
                    <a href="#">Life</a>
                    <i class="fa fa-chevron-right"></i>
                  </div>
                  <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                </div>
              </div>
              <div class="col-md-4">
                <div class="card card-boxless card-megamenu">
                  <div class="link-w-arrow">
                    <a href="#">Life Syariah</a>
                    <i class="fa fa-chevron-right"></i>
                  </div>
                  <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                </div>
              </div>
              <div class="col-md-4">
                <div class="card card-boxless card-megamenu">
                  <div class="link-w-arrow">
                    <a href="#">Accident & Health</a>
                    <i class="fa fa-chevron-right"></i>
                  </div>
                  <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                </div>
              </div>
              <div class="col-md-4">
                <div class="card card-boxless card-megamenu">
                  <div class="link-w-arrow">
                    <a href="#">Education</a>
                    <i class="fa fa-chevron-right"></i>
                  </div>
                  <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                </div>
              </div>
            </div>
          </div>

          <div class="megamenu-section-bottom">
            <div class="heading">
              <h2>Business</h2>
            </div>
            <div class="row">
              <div class="col-md-4">
                <div class="card card-boxless card-megamenu">
                  <div class="link-w-arrow">
                    <a href="#">Life</a>
                    <i class="fa fa-chevron-right"></i>
                  </div>
                  <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                </div>
              </div>
              <div class="col-md-4">
                <div class="card card-boxless card-megamenu">
                  <div class="link-w-arrow">
                    <a href="#">Life Syariah</a>
                    <i class="fa fa-chevron-right"></i>
                  </div>
                  <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                </div>
              </div>
              <div class="col-md-4">
                <div class="card card-boxless card-megamenu">
                  <div class="link-w-arrow">
                    <a href="#">Accident & Health</a>
                    <i class="fa fa-chevron-right"></i>
                  </div>
                  <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                </div>
              </div>
              <div class="col-md-4">
                <div class="card card-boxless card-megamenu">
                  <div class="link-w-arrow">
                    <a href="#">Education</a>
                    <i class="fa fa-chevron-right"></i>
                  </div>
                  <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                </div>
              </div>
            </div>
          </div>

        </div>
        <div class="col-md-3">
          <div class="card card-megamenu-right" style="background: url(/template_avrist/assets/img/common/Mask.png) no-repeat center; background-size: cover; min-height: 330px;">
            <img src="/template_avrist/assets/img/common/ic_ilustrasi_avram_menus.png" class="img-fluid">
            <p class="text-white">Ingin tahu produk investasi bagi Anda?</p>
            <a href="/get-solution/personal" class="btn btn-secondary btn-block">Get Solution</a>
          </div>
        </div>
      </div>
      
    </div>
  </div>

  <div class="dropdown-menu mega-menu" id="megaMenuAssetManagement">
    <div class="container">
      <div class="row">
        <div class="col-md-9">
          <div class="megamenu-section">
            <p class="megamenu-information">Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. </p>
            <a href="/asset-management" class="">PELAJARI TENTANG ASSET MANAGEMENT</a>
          </div>

          <div class="megamenu-section-bottom">
            <div class="heading">
              <h2>Personal</h2>
            </div>
            <div class="row">
              <div class="col-md-4">
                <div class="card card-boxless card-megamenu">
                  <div class="link-w-arrow">
                    <a href="#">Life</a>
                    <i class="fa fa-chevron-right"></i>
                  </div>
                  <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                </div>
              </div>
              <div class="col-md-4">
                <div class="card card-boxless card-megamenu">
                  <div class="link-w-arrow">
                    <a href="#">Life Syariah</a>
                    <i class="fa fa-chevron-right"></i>
                  </div>
                  <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                </div>
              </div>
              <div class="col-md-4">
                <div class="card card-boxless card-megamenu">
                  <div class="link-w-arrow">
                    <a href="#">Accident & Health</a>
                    <i class="fa fa-chevron-right"></i>
                  </div>
                  <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                </div>
              </div>
              <div class="col-md-4">
                <div class="card card-boxless card-megamenu">
                  <div class="link-w-arrow">
                    <a href="#">Education</a>
                    <i class="fa fa-chevron-right"></i>
                  </div>
                  <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                </div>
              </div>
            </div>
          </div>

          <div class="megamenu-section-bottom">
            <div class="heading">
              <h2>Business</h2>
            </div>
            <div class="row">
              <div class="col-md-4">
                <div class="card card-boxless card-megamenu">
                  <div class="link-w-arrow">
                    <a href="#">Life</a>
                    <i class="fa fa-chevron-right"></i>
                  </div>
                  <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                </div>
              </div>
              <div class="col-md-4">
                <div class="card card-boxless card-megamenu">
                  <div class="link-w-arrow">
                    <a href="#">Life Syariah</a>
                    <i class="fa fa-chevron-right"></i>
                  </div>
                  <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                </div>
              </div>
              <div class="col-md-4">
                <div class="card card-boxless card-megamenu">
                  <div class="link-w-arrow">
                    <a href="#">Accident & Health</a>
                    <i class="fa fa-chevron-right"></i>
                  </div>
                  <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                </div>
              </div>
              <div class="col-md-4">
                <div class="card card-boxless card-megamenu">
                  <div class="link-w-arrow">
                    <a href="#">Education</a>
                    <i class="fa fa-chevron-right"></i>
                  </div>
                  <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                </div>
              </div>
            </div>
          </div>

        </div>
        <div class="col-md-3">
          <div class="card card-megamenu-right" style="background: url(/template_avrist/assets/img/common/Mask.png) no-repeat center; background-size: cover; min-height: 330px;">
            <img src="/template_avrist/assets/img/common/ic_ilustrasi_avram_menus.png" class="img-fluid">
            <p class="text-white">Ingin tahu produk investasi bagi Anda?</p>
            <a href="/get-solution/personal" class="btn btn-secondary btn-block">Get Solution</a>
          </div>
        </div>
      </div>
      
    </div>
  </div>
-->

</nav>

<!--START: Modal Search-->
<!-- <div class="modal modal-full" tabindex="-1" role="dialog" id="modalSearch">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body p-box">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"><i class="fal fa-times"></i></span>
        </button>
        <form class="needs-validation" novalidate>
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <input type="text" class="form-control" autocomplete="off" placeholder="search" id="search" required="">
                <div class="invalid-feedback">
                  Please input date
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div> -->
<!--END: Modal Search-->

