<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */
 ?>

<footer>
	<div class="footer-top">
		<div class="container">
			<div class="row footer-list">
				<div class="col-md-4">
					<div class="footer-item">
						<h3 class="footer-title">PT Avrist Assurance</h3>
						<!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua.</p> -->
					</div>
				</div>
				<div class="col-md-4">
					<div class="footer-item">
						<h3 class="footer-title">Link lainnya</h3>
						<ul class="footer-links">
							<li><a href="<?php echo $prefix;?>">Berita & Event</a></li>
							<li><a href="<?php echo $prefix;?>">Karir</a></li>
							<li><a href="<?php echo $prefix;?>">Harga Unit</a></li>
							<li><a href="<?php echo $prefix;?>">Bengkel Partner</a></li>
							<li><a href="<?php echo $prefix;?>">Rumah Sakit Partner</a></li>
						</ul>
					</div>
				</div>
				<div class="col-md-4">
					<div class="footer-item">
						<h3 class="footer-title">Kontak kami di</h3>
						<p class="footer-info">021-252-1662</p>
						<p class="footer-info">Senin - jumat, 08.00-17.00 WIB</p>
						<ul class="footer-social">
							<li><a href="https://www.facebook.com/avrist/" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
							<li><a href="https://twitter.com/avrist" target="_blank"><i class="fab fa-twitter"></i></a></li>
							<li><a href="https://www.instagram.com/avristsolution" target="_blank"><i class="fab fa-instagram"></i></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="footer-bottom">
		<div class="container">
			<div class="row">
				<div class="col-md-8">
					<p class="copyright">Copyright Avrist Assurance 2018 | Registered and supervised by Otoritas Jasa Keuangan</p>
					<!-- &copy; 2017 Antikode. Site by <a href="http://www.antikode.com" target="_blank">Antikode</a> -->
				</div>
				<div class="col-md-4 text-md-right">
					<a href="<?php echo $prefix;?>terms-conditions" class="mr-3 divider-right">Terms &amp; Conditions</a>
					<a href="<?php echo $prefix;?>privacy-policy">Privacy Policy</a>
				</div>
			</div>
		</div>
	</div>
</footer>

