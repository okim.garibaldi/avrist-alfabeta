<footer>
    <div class="footer-top">
        <div class="container">
            <div class="row footer-list">
                <div class="col-md-4 positon-relative">
                    <div class="footer-item">
                        <img src="<?= $footer->getImage1() ?>" class="footer-logo" alt="Logo">
                        <h3 class="footer-title"><?= $footer->getText1() ?></h3>
                        <!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua.</p> -->
                        <div class="logo-trusted">
                            <img src="<?= $footer->getImage2() ?>" alt="Logo Reksa Dana">
                            <img src="<?= $footer->getImage3() ?>" alt="Logo OJK">
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="footer-item">
                        <h3 class="footer-title">Link lainnya</h3>
                        <ul class="footer-links">
                        <?php foreach($footer->getBlock1() as $block1): ?>
                            <li><a href="<?php 
                            if ($block1['prefixDisabled']->getData())
                            {
                                echo $block1['page']->getData();
                            }
                            elseif(isset($url))
                            {
                                echo $url->getLife().$block1['page']->getData();
                            }
                            else
                            {
                                echo $block1['page']->getData();
                            } ?>"><?= $block1['name']->getData() ?></a></li>
                        <?php endforeach; ?>
                        </ul>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="footer-item">
                        <h3 class="footer-title"><?= $footer->getText2() ?></h3>
                        <p class="mb-1"><strong><?= $footer->getText3() ?></strong></p>
                        <a href="tel:<?= $footer->getText4() ?>"><p class="footer-info mb-1"><?= $footer->getText4() ?></p></a>
                        <p class="footer-info mb-1"><?= $footer->getText5() ?></p>
                        <ul class="footer-social">
                            <li><a href="<?= $footer->getFacebook() ?>" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                            <li><a href="<?= $footer->getTwitter() ?>" target="_blank"><i class="fab fa-twitter"></i></a></li>
                            <li><a href="<?= $footer->getInstagram() ?>" target="_blank"><i class="fab fa-instagram"></i></a></li>
                            <li><a href="<?= $footer->getLinkedin() ?>" target="_blank"><i class="fab fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <p class="copyright">Copyright Avrist Assurance 2018 | Registered and supervised by Otoritas Jasa Keuangan</p>
                    <!-- &copy; 2017 Antikode. Site by <a href="http://www.antikode.com" target="_blank">Antikode</a> -->
                </div>
                <div class="col-md-4 text-md-right">
                    <a href="terms-conditions" class="mr-3 divider-right">Terms &amp; Conditions</a>
                    <a href="privacy-policy">Privacy Policy</a>
                </div>
            </div>
        </div>
    </div>
</footer>

<!-- <div class="floating-btn">
    <a href="#" class="">
        <img src="<?php echo $prefix;?>assets/img/icon/ic_FB_messanger.png" class="img-fluid">
    </a>
    <a href="#" class="">
        <img src="<?php echo $prefix;?>assets/img/icon/ic_whatsapp.png" class="img-fluid">
    </a>
</div> -->