<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */
 ?>

  <?php $prefix="/template_avrist/";?>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>Avrist</title>
  <meta name="description" content="">
  <meta name="author" content="">

  <meta property="og:title" content=""/>
  <meta property="og:description" content="" />
  <meta property="og:url" content="" />
  <meta property="og:image" content="/template_avrist/assets/ico/richlink.jpg" />

  <?php $this->headLink(['rel' => 'shortcut icon', 'href' => '/template_avrist/assets/ico/favicon.ico'], 'PREPEND'); ?>
  <?php $this->headLink(['rel' => 'apple-touch-icon', 'href' => '/template_avrist/assets/ico/apple-touch-icon.png'], 'PREPEND'); ?>

  <!-- <?php $this->headLink()->appendStylesheet('/template_avrist/assets/css/main.css');?>
  <?= $this->headLink(); ?> -->
  <link rel="stylesheet" href="/template_avrist/assets/css/main.css" />
  <link rel="stylesheet" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css" />
