
<script>
/* ==========================================================================
   JS for googlemap
   ===================================================================== ===== */

var coordinates = <?= json_encode($coordinates); ?>;
var locationType = "<?= $pageCategory ?>";
function initialize() {

    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position){
            var myOptions = {
            center: new google.maps.LatLng(res.latitude, res.longitude),
            zoom: 12,
            mapTypeId: google.maps.MapTypeId.ROADMAP

            };
            var map = new google.maps.Map(document.getElementById("googleMap"), myOptions);

            setMarkers(map, coordinates);
        }, 
        function(){
            $.ajax('http://geoip-db.com/json/')
                .then(
                    function success(response) {
                        var res = JSON.parse(response);
                        var myOptions = {
                        center: new google.maps.LatLng(res.latitude, res.longitude),
                        zoom: 12,
                        mapTypeId: google.maps.MapTypeId.ROADMAP

                        };
                        var map = new google.maps.Map(document.getElementById("googleMap"), myOptions);

                        setMarkers(map, coordinates);
                    },

                    function fail(data, status) {
                        console.log('Request failed.  Returned status of', status);
                    }
                );
        });
    }
    
    if ((locationType != "ourLocation3") && (coordinates.length >0))
    {
        calculateDistances(); 
    }
    

    //let directionsService = new google.maps.DirectionsService();
    //// Create route from existing points used for markers
    //distances = [];
    //var cardRow = document.getElementById('cardRow');
    //for (i = 0; i < coordinates.length; i++) 
    //{
    //    const route = {
    //    origin: {lat: -6.215417, lng: 106.821408},
    //    destination: {lat: coordinates[i]['latitude'], lng: coordinates[i]['longitude']},
    //    travelMode: 'DRIVING'
    //    }
    //
    //    directionsService.route(route, distanceFunc);
    //}
    //alert(distances[0])
    //alert(coordinates[0]['distance']);
}

function calculateDistances() {
    var service = new google.maps.DistanceMatrixService();
    latLong = [];
    for(var i = 0; i < coordinates.length; i++){
        latLong.push({lat: coordinates[i]['latitude'], lng: coordinates[i]['longitude']})
    }
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position){
            service.getDistanceMatrix({
            origins: [{lat: position.coords.latitude, lng: position.coords.longitude}], //array of origins
            destinations: latLong, //array of destinations
            travelMode: google.maps.TravelMode.DRIVING,
            unitSystem: google.maps.UnitSystem.METRIC,
            avoidHighways: false,
            avoidTolls: false
            }, callback);
        }, locateByIP);
    }
}

function locateByIP(error){

    $.ajax('http://geoip-db.com/json/')
    .then(
        function success(response) {
            var service = new google.maps.DistanceMatrixService();
            var res = JSON.parse(response);
            service.getDistanceMatrix({
            origins: [{lat: res.latitude, lng: res.longitude}], //array of origins
            destinations: latLong, //array of destinations
            travelMode: google.maps.TravelMode.DRIVING,
            unitSystem: google.maps.UnitSystem.METRIC,
            avoidHighways: false,
            avoidTolls: false
            }, callback);
        },

        function fail(data, status) {
            console.log('Request failed.  Returned status of', status);
        }
    );
}

function callback(response, status) {
    if (status != google.maps.DistanceMatrixStatus.OK) {
        alert('Error was: ' + status);
    } else {
        //we only have one origin so there should only be one row
        var routes = response.rows[0];
        for (var i = 0; i < routes.elements.length;  i++) {
            var rteDistance = routes.elements[i].distance.value;
            coordinates[i]['distance'] = rteDistance;
        }
        coordinates.sort((function(index) {
            return function(a, b) {
                return (a[index] === b[index] ? 0 : (a[index] < b[index] ? -1 : 1));
                };
        })('distance'));

        var cardRow = document.getElementById('cardRow');
        for(var i = 0; i < coordinates.length; i++){
            var div1 = document.createElement('div');
            div1.className = "col-md-4";
            var div2 = document.createElement('div');
            div2.className = "card card-lokasi";
            div2.setAttribute("href", "#");
            var div3 = document.createElement('div');
            div3.className = "card-body";
            if(locationType == "ourLocation1")
            {
                var div3Child1 = document.createElement("img");
                div3Child1.className = "img-fluid avatar";
                div3Child1.src = coordinates[i]['image'];
            }
            else if(locationType == "ourLocation2" || locationType == "bengkelRekanan" || locationType == "RSRekanan")
            {
                var div3Child1 = document.createElement('span');
                div3Child1.className = "direction";
                div3Child1.innerHTML = Math.round((coordinates[i]['distance'] / 1000) * 10)/10 + "km";
            }
            
            var h5 = document.createElement('h5');
            h5.innerHTML = coordinates[i]['name'];
            var p = document.createElement('p');
            p.innerHTML = coordinates[i]['address'];
            var a1 = document.createElement('a');
            a1.href = "https://www.google.com/maps/search/?api=1&query=" + coordinates[i]['latitude'] + "," + coordinates[  i]['longitude'] ;
            a1.className = "d-block";
            a1.innerHTML = '<i class="fas fa-directions"></i>Get Direction';
            if(locationType == "ourLocation2" || locationType == "RSRekanan")
            {
                var a2 = document.createElement('a');
                a2.href = "tel:" + coordinates[i]['phone'];
                a2.className = "d-block";
                a2.innerHTML = '<i class="far fa-address-book"></i>Call ' + (coordinates[i]['phone'] || "");
            }
            if(locationType == "ourLocation1")
            {
                var a2 = document.createElement('a');
                a2.href = "#";
                a2.className = "";
                a2.setAttribute("data-toggle", "modal");
                a2.setAttribute("data-target", "#modalAgen" + coordinates[i]['name'].replace(/(\W|\s)/g, ''));
                a2.innerHTML = '<i class="far fa-address-book"></i>Contact';
            }
            if(locationType == "ourLocation2")
            {
                var a3 = document.createElement('a');
                a3.href = "tel:" + coordinates[i]['fax'];
                a3.className = "d-block";
                a3.innerHTML = '<i class="fas fa-fax"></i>Fax ' + (coordinates[i]['fax']  || "");
                a3.setAttribute("data-toggle", "modal");
                a3.setAttribute("data-target", "#modalAgen" + coordinates[i]['name'].replace(/(\W|\s)/g, ''));
            }
            if(locationType == "ourLocation2" || locationType == "bengkelRekanan")
            {
                var a4 = document.createElement('a');
                a4.href = "#";
                a4.className = "d-block";
                a4.setAttribute("data-toggle", "modal");
                a4.setAttribute("data-target", "#modalAgen" + coordinates[i]['name'].replace(/(\W|\s)/g, ''));
                a4.innerHTML = '<i class="far fa-envelope"></i>Email';
            }
            div3.appendChild(div3Child1);
            div3.appendChild(h5);
            div3.appendChild(p);
            div3.appendChild(a1);
            if(locationType == "ourLocation2" || locationType == "RSRekanan" || locationType == "ourLocation1")
            {
                div3.appendChild(a2);
            }
            if(locationType == "ourLocation2")
            {
                div3.appendChild(a3);
            }
            if(locationType == "ourLocation2" || locationType == "bengkelRekanan")
            {
                div3.appendChild(a4);
            }
            div2.appendChild(div3);
            div1.appendChild(div2);
            
            cardRow.appendChild(div1);
        }
        
    }
}

function setMarkers(map, coordinates) {

    var marker, i

    for (i = 0; i < coordinates.length; i++) {

        var name = coordinates[i]['name']
        var address = coordinates[i]['address']
        var lat = coordinates[i]['latitude']
        var long = coordinates[i]['longitude']
        var phone = coordinates[i]['phone']

        latlngset = new google.maps.LatLng(lat, long);

        var marker = new google.maps.Marker({
            map: map,
            title: name,
            position: latlngset
        });
        map.setCenter(marker.getPosition())

        var content = name + '</h3>, ' + address + ', ' + phone

        var infowindow = new google.maps.InfoWindow()

        google.maps.event.addListener(marker, 'click', (function(marker, content, infowindow) {
            return function() {
                infowindow.setContent(content);
                infowindow.open(map, marker);
            };
        })(marker, content, infowindow));

    }
}

//function distanceFunc(response, status) { // anonymous function to capture directions
//    if (status !== 'OK') {
//        window.alert('Directions request failed due to ' + status);
//        return;
//    } else {
//        var directionsData = response.routes[0].legs[0]; // Get data about the mapped route
//        if (!directionsData) {
//            window.alert('Directions request failed');
//            return;
//        } else {
//            alert(coordinates[i]['latitude']);
//            //var div1 = document.createElement('div');
//            //div1.className = "col-md-4";
//            //var div2 = document.createElement('div');
//            //div2.className = "card card-lokasi";
//            //var div3 = document.createElement('div');
//            //div3.className = "card-body";
//            //var span = document.createElement('span');
//            //span.className = "direction";
//            //span.innerHTML = directionsData.value;
//            //var distanceInput = document.createElement('input');
//            //distanceInput.id = "distancesInput-" + i;
//            //distanceInput.setAttribute("type", "hidden");
//            //distanceInput.value =     .value;
//            //var h5 = document.createElement('h5');
//            //h5.innerHTML = coordinates[i]['name'];
//            //h5.id = "name-" + i;
//            //var p = document.createElement('p');
//            //p.innerHTML = coordinates[i]['address'];
//            //p.id = "address" + i;
//            //var a1 = document.createElement('a');
//            //a1.href = "https://www.google.com/maps/search/?api=1&query=" + coordinates[i]['latitude'] + "," + ////coordinates[i]['longitude'];
//            //a1.className = "d-block";
//            //a1.innerHTML = '<i class="fas fa-directions"></i>Get Direction';
//            //var a2 = document.createElement('a');
//            //a2.href = "tel:" + coordinates[i]['phone'];
//            //a2.className = "d-block";
//            //a2.innerHTML = '<i class="far fa-address-book"></i>Call ' + coordinates[i]['phone'];
//            //var a3 = document.createElement('a');
//            //a3.className = "d-block";
//            //a3.innerHTML = '<i class="fas fa-fax"></i>Fax ' + coordinates[i]['fax'];
//            //a3.setAttribute("data-toggle", "modal");
//            //a3.setAttribute("data-target", "#modalAgen");
//            //a3.href = "#";
//            //a3.innerHTML = '<i class="far fa-envelope"></i>Email';
//            //
//            //div3.appendChild(span);
//            //div3.appendChild(distanceInput);
//            //div3.appendChild(h5);
//            //div3.appendChild(p);
//            //div3.appendChild(a1);
//            //div3.appendChild(a2);
//            //div3.appendChild(a3);
//            //div2.appendChild(div3);
//            //div1.appendChild(div2);
//            //
//            //cardRow.appendChild(div1);
//            //coordinates[i]['distance'] = directionsData.distance.value;
//            //
//            //alert(" Driving distance is " + directionsData.distance.value + " (" + directionsData.duration.text + //").");
//        }
//    }
//}
// event jendela di-load  
google.maps.event.addDomListener(window, 'load', initialize);
</script>

