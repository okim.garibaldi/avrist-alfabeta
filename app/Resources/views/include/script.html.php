<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */
 ?>

 <!--Bootstrap 4.1.3 dependency-->
 <?php $this->inlineScript()->appendFile('/template_avrist/assets/plugins/jquery-3.3.1/jquery.min.js'); ?>
<?php $this->inlineScript()->appendFile('/template_avrist/assets/plugins/popper-1.14.0/popper.min.js'); ?>
<?php $this->inlineScript()->appendFile('/template_avrist/assets/plugins/bootstrap-4.1.3/js/bootstrap.min.js'); ?>

 <!--jQuery UI-->
 <!--Has dependency on main.scss-->
 <?php $this->inlineScript()->appendFile('/template_avrist/assets/plugins/jquery-ui-1.12.1/jquery-ui.min.js'); ?>

 <!--Viewport Checker-->
 <!--For viewport animation-->
 <?php $this->inlineScript()->appendFile('/template_avrist/assets/plugins/viewportchecker-1.8.8/viewportchecker.min.js'); ?>

 <!--Parallax JS-->
 <!--For parallax effect-->
 <?php $this->inlineScript()->appendFile('/template_avrist/assets/plugins/parallax-1.5.0/parallax.min.js'); ?>

 <!--Fancy Box-->
 <!--For lightbox-->
 <!--Has dependency on main.scss"-->
 <?php $this->inlineScript()->appendFile('/template_avrist/assets/plugins/fancybox-3.3.5/jquery.fancybox.min.js'); ?>

 <!--Select2-->
 <!--For custom select-->
 <!--Has dependency on main.scss"-->
 <?php $this->inlineScript()->appendFile('/template_avrist/assets/plugins/select2-4.0.6/select2.min.js'); ?>

 <!--DataTables-->
 <!--For advanced table-->
 <!--Has dependency on main.scss"-->
 <?php $this->inlineScript()->appendFile('/template_avrist/assets/plugins/datatables-1.10.16/datatables.min.js'); ?>

 <!--Slick Slider-->
 <!--For advanced slider-->
 <!--Has dependency on main.scss"-->
 <?php $this->inlineScript()->appendFile('/template_avrist/assets/plugins/slick-1.8.1/slick.min.js'); ?>

 <!--Owl Carousel-->
 <!--For custom carousel-->
 <!--Has dependency on .main.scss"-->
 <?php $this->inlineScript()->appendFile('/template_avrist/assets/plugins/owl-carousel-2.2.1/owl.carousel.js'); ?>

 <!--Antikode Custom JS-->
 <!--For component based JS customized by the author-->
 <!--Some has dependency on main.scss-->
  <?php //$this->inlineScript()->appendFile('/template_avrist/assets/plugins/antikode-custom-1.0.0/animate.js'); ?>
  <?php //$this->inlineScript()->appendFile('/template_avrist/assets/js/antikode-custom/navbar.js'); ?>

    <!-- Login -->
    <!-- Login script -->
    <script type="text/javascript">
        var _engine_location;
    </script>
    <script type="text/javascript" src="https://l2.io/ip.js?var=_engine_location"></script>

    <?php $this->inlineScript()->appendFile('/template_avrist/assets/js/ext/jsencrypt.min.js'); ?>
    <?php $this->inlineScript()->appendFile('/template_avrist/assets/js/ext/aesjs.js'); ?>
    <?php $this->inlineScript()->appendFile('/template_avrist/assets/js/ext/js.cookie.min.js'); ?>
    <?php $this->inlineScript()->appendFile('/template_avrist/assets/js/ext/secure-guys.js'); ?>

 <!--Main.js-->
 <!--Main custom JS from author-->
 <?php $this->inlineScript()->appendFile('/template_avrist/assets/js/main.js'); ?>

 
 <?= $this->inlineScript(); ?>

 

