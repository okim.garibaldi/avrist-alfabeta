<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

 use Pimcore\Model\Document;
 use Pimcore\Model\Document\Page;
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
  <head>
    <?= $this->template("include/head2.html.php"); ?>
    <?= $this->template("include/tracking-script/head.html.php"); ?>
    <style>
      .owl-stage-outer {overflow: hidden !important;}
    </style>
  </head>
  <body class="body-primary">
    <?= $this->template("include/tracking-script/body.html.php"); ?>

    <div class="loader-wrapper loader-light">
      <div class="loader"></div>
    </div> 

    <?= $this->template("include/navbar3.html.php"); ?>

<section class="py-main section-get-solution">
    <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/pattern/ic_ornamen_yellow.png')?>" class="pattern-yellow" alt="Ornamen yellow">
    <div class="container container-sm">
        <div class="btn-wrapper text-center">
            <h5 class="mb-3">Pilih Solusi untuk</h5>
            <a href="/get-solution/personal" class="btn btn-gender w-150 mr-2">Personal</a>
            <a href="/get-solution/bisnis" class="btn btn-gender w-150 active">Bisnis</a>
        </div>
        <form class="needs-validation form-get-solution" novalidate>
            <div id="form-1">
                <p>Nama saya <input type="text" class="form-control w-300" name="" id="input-name" required>, PIC untuk perusahaan <input type="text" class="form-control w-300" name="" id="namaPerusahaan" required>.</p>
            </div>

            <div id="form-2">
                <p>Saya dapat dihubungi di nomor<input type="text" class="form-control w-300" name="" id="phoneNumber" required> dan email <input type="text" class="form-control w-350" id="email" name="" required>.</p>
            </div>

            <div id="form-3">
                <p class="d-inline-block mb-0">Perusahaan saya bergerak di bidang
                <div class="dropdown d-inline">
                    <div class="dropdown-toggle " data-toggle="dropdown" id="select-value" required>
                        Pilih bidang
                    </div>
                    <div class="dropdown-menu-form" id="dropdown-menu-form">
                        <div class="selection-value dropdown-item" data-select="0">Design & Technology</div>
                        <div class="selection-value dropdown-item" data-select="1">Web Developement</div>
                        <div class="selection-value dropdown-item" data-select="2">UI & UX</div>
                    </div>
                </div>
                </p>
            </div>


            <div class="clearfix mt-4" id="form-4">
                <a href="javascript:;" class="btn btn-secondary btn-next-summary">Lanjut</a>
            </div>



        </form>
    </div>
</section>

<?= $this->template("include/script2.html.php"); ?>

<script>
    // Animation on load
    document.addEventListener("DOMContentLoaded", function(event) {
        $(".loader").fadeOut('slow');
        $(".loader-wrapper").fadeOut("slow");
        //  $(".anim-1").addClass("animated fadeInLeft delayp10");
        // $(".anim-2").addClass("animated fadeInUp delayp12");
        //$(".anim-3").addClass("animated fadeInUp delayp14");
    });

//    var logo = ["../../../template_avrist/assets/img/brand/logo_avrist.png", "../../../template_avrist/assets/img/brand/logo_avrist_white.png"];
//
//    $(".navbar").addClass('navbar-transparent');
//    $('.navbar-avrist .navbar-brand img').attr('src', logo[1]);
//
//    $(window).on("scroll", function() {
//        if ($(window).scrollTop() > 10) {
//            $(".navbar-avrist").removeClass("navbar-transparent");
//            $('.navbar-avrist .navbar-brand img').attr('src', logo[0]);
//        } else {
//            //remove the background property so it comes transparent again (defined in your css)
//            $(".navbar-avrist").addClass("navbar-transparent");
//            $('.navbar-avrist .navbar-brand img').attr('src', logo[1]);
//        }
//    });

    var link = "";
    var name = "";
    var pic = "";
    var telp = "";
    var email = "";
    var field = "";

    $('.btn-next-summary').on('click', function(e) {
        e.preventDefault();
        name = document.getElementById("input-name").value;
        pic = document.getElementById("namaPerusahaan").value;
        telp = document.getElementById("phoneNumber").value;
        email = document.getElementById("email").value;

        link = "/get-solution/summary?category=bisnis&name=" + name.toLowerCase().split(' ').join('-') +
                "&pic="+ pic +
                "&telp="+ telp +
                "&email="+ email +
                "&field="+ field;
        window.location.href = link;
    });

    //dropdown toggle
    $('#dropdown-menu-form').hide();
    $('#select-value').click(function(){
        $('#dropdown-menu-form').toggle();
    });

    $(".selection-value").click(function(e) {
        const selected = e.target.dataset.select;
        switch(selected) {
            case '0':
                field = "design-technology";
                $('#select-value').html('Design & Technology');
                $('#dropdown-menu-form').hide();
                $('#form-6').show();
                break;
            case '1':
                field = "web-development";
                $('#select-value').html('Web Developement');
                $('#dropdown-menu-form').hide();
                $('#form-6').show();
                break;
            case '2':
                field = "ui-ux";
                $('#select-value').html('UI & UX');
                $('#dropdown-menu-form').hide();
                $('#form-6').show();
                break;
        }
    })

    $('#form-2').hide();
    $('#form-3').hide();
    $('#form-4').hide();

    // Form states
    $("form .form-control").on("keyup", function(){
        if($("#ageForm").val() !== ""  && $('#namaPerusahaan').val() !== ""  ) {

            $('#form-2').fadeIn( 2000 );
            // $('#form-1').addClass('form-is-completed');
            // $("#phoneNumber").trigger('focus');
            // $('.form-get-solution').css("transform","translateY(calc(50vh + -220px))");
        }
        if($("#phoneNumber").val() !== "" && $('#email').val() !== "" ) {
            $('#form-3').fadeIn( 1000 );
            $('#form-4').show();
            // $('.form-get-solution').css("transform","translateY(calc(50vh + -280px))");
        }
    });

</script>

</body>
</html>
