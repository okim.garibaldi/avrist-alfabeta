<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

use Pimcore\Model\Document;
use Pimcore\Model\Document\Page;

$this->extend('layout_avrist_new.html.php');?>

<div class="confirm-alert fixed-top" style="width: 100%; z-index: 1000; margin-top: 62px; display: none;">
    <div class="alert alert-success">
        <div class="container">Email has successfully send!</div>
    </div>
</div>

<section class="py-main section-header-avrist">
    <div class="container">
        <div class="heading-w-link text-white">
            <h2>Solusi Lengkap Untuk</h2>
            <div class="heading-group">
                <div class="row">
                    <div class="col-md-6">
                        <p><strong><?= $solutionFor ?></strong> untuk proteksi <strong><?= $solutionNeeds ?></strong></p>
                    </div>
                    <div class="col-md-6 text-md-right">
                        <a href="<?= $sendEmailAddress ?>" class="btn btn-transparent mr-2" id="sendEmail">Kirim quote ke e-mail</a>
                        <a href="#" class="btn btn-transparent" data-toggle="modal" data-target="#modalContactMe">Hubungi saya</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="py-main section-quote-summary">
    <div class="container">
        <div class="owl-carousel owl-theme slider-mobile">

            <?php foreach ($this->products as $product){ ?>
            <div class="item">
                <div class="card card-quote-summary">
                    <div class="card-top">
                        <p><small>Untuk Anda</small></p>
                        <a href="
                        <?php
                        if($product['product']->getSiteId() == 1)
                        {
                          echo $url->getLife().str_replace("/life-insurance", "",$product['product']->getPage());
                        } 
                        elseif($product['product']->getSiteId() == 2)
                        {
                          echo $url->getGeneral().str_replace("/general", "",$product['product']->getPage());
                        }
                        else
                        {
                          echo $url->getAm().str_replace("/asset-management", "",$product['product']->getPage());;
                        }
            
                         ?>" class="title"><?= $product['product']->getName() ?></a>
                        <p><small></small></p>
                    </div>
                    <div class="card-middle">
                        <p class="mb-1"><small>Premi mulai dari</small></p>
                        <h5>@<?= $product['product']->getPrice() ?></h5>
                    </div>
                    <div class="card-bottom">
                        <p class="font-size-sm">Manfaat & Fasilitas</p>
                        <ul>
                        <?php foreach($product['benefits'] as $benefit): ?>
                            <li><?= $benefit->getTitle() ?><br><?= $benefit->getDescription() ?> </li>
                        <?php endforeach; ?>
                        </ul>
                    </div>
                </div>
            </div>
            <?php } ?>

        </div>
    </div>
</section>

<section class="py-main section-agent">
    <div class="container">
        <div class="heading">
            <h2>Ingin konsultasi langsung?</h2>
            <p>Hubungi agen terdekat, atau Kunjungi Cabang Terdekat</p>
        </div>

        <div class="content">
            <div class="owl-carousel owl-theme slider-mobile">
                <div class="item">
                    <div class="card card-agent-new">
                        <div class="card-body">
                            <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/common/ic_consultation_agent.png')?>" class="img-fluid" alt="Agent">
                            <h5>Agent</h5>
                            <p class="text-truncate-twoline">Hubungi agen terdekat, atau kunjungi cabang terdekat</p>
                            <a href="#" class="btn btn-yellow w-150">Find Agent</a>
                        </div>
                    </div>
                </div>

                <div class="item">
                    <div class="card card-agent-new">
                        <div class="card-body">
                            <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/common/ic_consultation_financial.png')?>" class="img-fluid" alt="Agent">
                            <h5>Financial Advisor</h5>
                            <p class="text-truncate-twoline">Talk to Avrist guide</p>
                            <a href="#" class="btn btn-yellow w-150">Talk to Us</a>
                        </div>
                    </div>
                </div>

                <div class="item">
                    <div class="card card-agent-new">
                        <div class="card-body">
                            <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/common/ic_consultation_message.png')?>" class="img-fluid" alt="Agent">
                            <h5>Message</h5>
                            <p class="text-truncate-twoline">Message us on feedback</p>
                            <a href="#" class="btn btn-yellow w-150">Message Us</a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>

<!--START: Modal Contact Me-->
<div class="modal" tabindex="-1" role="dialog" id="modalContactMe">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body p-box">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><i class="fal fa-times"></i></span>
                </button>
                <h4 class="modal-title mb-3">Hubungi Saya Di</h4>
                <form class="needs-validation" novalidate>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="">Tanggal</label>
                                <input type="text" class="form-control form-control-datepicker" autocomplete="off" placeholder="(MM/DD/YYYY)" id="dateContact" required="">
                                <div class="invalid-feedback">
                                    Please input date
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="validationCustom04">Jam</label>
                                <div class="form-group">
                                    <select class="custom-select" id="validationCustom04" required>
                                        <option value="">Choose</option>
                                        <option value="1">Pagi (09:00 - 11:00)</option>
                                        <option value="2">Siang (11:00 - 14:00)</option>
                                        <option value="3">Sore (14:00 - 18:00)</option>
                                    </select>
                                    <div class="invalid-feedback">
                                        Please choose correctly
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <button class="btn btn-primary" type="submit" id="btnSubmitContact">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!--END: Modal Contact Me-->

<script>
    $('#btnSubmitContact').click(function(e){
        $('#modalContactMe').modal('hide');
        $('.confirm-alert').fadeIn();
        e.preventDefault();

        setTimeout(function(){
            $('.confirm-alert').fadeOut();
        },2000)

    });

    $('#dateContact').datepicker();
</script>
