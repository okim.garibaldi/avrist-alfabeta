<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

use Pimcore\Model\Document;
use Pimcore\Model\Document\Page;

$this->extend('layout_avrist_new.html.php');?>

<div class="cover cover-sm cover-responsive <?= $bgBanner['bg'] ?> cover-general">
    <div class="cover-responsive-img">
        <div class="cover-responsive-overlay"></div>
    </div>
    <div class="container position-relative">
        <img src="/template_avrist/assets/img/pattern/ic_ornamen_yellow.png" class="pattern-purple" alt="Ornamen Purple">
        <!-- <img src="/template_avrist/assets/img/pattern/ic_ornament_triangle_yellow.png" class="pattern-triangle-yellow" alt="Ornamen Triangle Yellow"> -->
        <img src="/template_avrist/assets/img/pattern/ic_ornament_circle_pink.png" class="pattern-pink" alt="Ornamen Circle Pink">
        <nav aria-label="breadcrumb" class="general-breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active"><a href="#">Contact Us</a></li>
            </ol>
        </nav>
        <div class="cover-content">
            <h3 class="cover-title text-white animated fadeInUp delayp1"><?= $contactUs->getTitle() ?></h3>
            <p class="cover-text"><?= $contactUs->getAddress() ?></p>
        </div>
        <div class="col-md-4">
            <div class="contact-us-list">
              <label><?= $contactUs->getContact1() ?></label>
              <p class="contact-box mb-3"><img src="<?= $contactUs->getContact1Image() ?>" class="icon"/><a href=""><?= $contactUs->getContact1Number() ?></a></p>
              <label><?= $contactUs->getContact2() ?></label>
              <p class="contact-box"><img src="<?= $contactUs->getContact2Image() ?>" class="icon"/><a href=""><?= $contactUs->getContact2Number() ?></a></p>
            </div>
          </div>
    </div>
</div>

<section class="py-main section-life-insurance-1">
    <div class="container">
        <div class="heading vp-fadeinup delayp2">
            <h2><?= $contactUs->getTitle1() ?></h2>
            <p class="p-0 subtitle"><?= $contactUs->getDescription1() ?></p>
        </div>

        <div class="content mt-5">
            <div class="row">
            <?php foreach($options as $o): ?>
                <div class="col-12 col-md-4 vp-fadeinup delayp1">
                    <a href="<?= $o->getPage() ?>" class="card p-box card-insurance customer-support">
                        <div class="card-img">
                            <img src="<?= $o->getImage() ?>" class="img-fluid">
                            <h5 class="card-title"><?= $o->getName() ?></h5>
                        </div>
                    </a>
                </div>
            <?php endforeach; ?>
            </div>
        </div>
    </div>
</section>

<section class="py-main section-life-insurance-1">
    <div class="container">
        <div class="heading vp-fadeinup delayp2">
            <h2><?= $contactUs->getTitle2() ?></h2>
            <p class="p-0 subtitle"><?= $contactUs->getDescription2() ?></p>
        </div>

        <div class="content mt-5">
            <div class="row">
            <?php foreach($socialMedia as $sM): ?>
                <div class="col-12 col-md-3 vp-fadeinup delayp1">
                    <div class="card p-box card-insurance">
                        <a href="<?= $sM->getLink() ?>">
                            <div class="card-img">
                                <img src="<?= $sM->getImage() ?>" class="img-fluid">
                                <h5 class="card-title"><?= $sM->getName() ?></h5>
                            </div>
                        </a>
                    </div>
                </div>
            <?php endforeach; ?>
            </div>
        </div>
    </div>
</section>

<section class="py-main ask-your-financial <?= $bgBanner['bg'] ?> cover" style="background: url(<?= $contactUs->getText1Image() ?>) no-repeat right; background-size: contain; height: 500px;">
    <div class="container">
        <div class="cover-content">
          <h1 class="cover-title text-white animated fadeInUp delayp1"><?= $contactUs->getTitle3() ?></h1>
          <p class="cover-text text-white animated fadeInUp delayp2"><?= $contactUs->getDescription3() ?></p>
          <a href="<?= $contactUs->getText1Page() ?>"><?= $contactUs->getText1() ?></a>
        </div>
    </div>
</section>

<section class="py-main section-why-us">
    <div class="container position-relative">
        <img src="/template_avrist/assets/img/pattern/ic_ornamen_yellow.png" class="pattern-yellow vp-fadeinup delayp1" alt="Ornamen Yellow">
        <div class="row">
            <div class="col-md-6 text-center">
                <!-- <img src="/template_avrist/assets/img/common/mengapa_avrist.gif" class="img-fluid vp-fadeinleft" id="faqHome" alt="Why Us"> -->
                <div class="heading text-left">
                    <h2><?= $contactUs->getTitle4() ?></h2>
                    <p><?= $contactUs->getDescription4() ?></p>
                </div>
            </div>
            <div class="col-md-6 content-center">
                <div class="heading mb-4 vp-fadeinup delayp1">
                    <h2><?= $contactUs->getTitle5(); ?></h2>
                </div>
                <div id="accordionFAQ" class="accordion accordion-faq vp-fadeinup delayp2">
                <?php $expanded = "true"; ?>
                <?php $show = " show"; ?>
                <?php foreach($contactUs->getBlock3() as $block3): ?>
                    <?php $i++; ?>
                    <div class="accordion-item">
                        <div class="accordion-header" id="headingFAQ<?= $i ?>" data-toggle="collapse" data-target="#collapseFAQ<?= $i ?>" aria-expanded="<?= $expanded ?>" aria-controls="collapseFAQ<?= $i ?>">
                            <h5><?= $block3['question']->getData() ?>
                  <span class="close-panel"></span>
                  </h5>
                        </div>
                        <div id="collapseFAQ<?= $i ?>" class="accordion-body collapse<?= $show ?>" aria-labelledby="headingFAQ<?= $i ?>" data-parent="#accordionFAQ">
                            <p><?= $block3['answer']->getData() ?> </p>
                        </div>
                    </div>
                    <?php if($expanded == "true" || $show == "show"): ?>
                        <?php $expanded = "false"; ?>
                        <?php $show = ""; ?>
                    <?php endif; ?>
                <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</section>