<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

$this->extend('layout_avrist_non_transparent.html.php');?>

<section class="py-main">
    <div class="container">
        <?php foreach ($this->data as $d){?>
        	<div class="heading">
            	<?= $d->getTitle(); ?>
        	</div>
        	<div>
            	<?= $d->getDescription(); ?>
        	</div>
        <?php } ?>
    </div>
</section>
