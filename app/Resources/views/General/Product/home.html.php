<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

use Pimcore\Model\Document;
use Pimcore\Model\Document\Page;

$this->extend('layout_avrist_new.html.php');?>

<div class="cover cover-sm cover-responsive cover-general" style="background: linear-gradient(270deg, rgba(0,0,0,0.00) 0%, #7F8AA3 70%), url(/template_avrist/assets/img/common/img_banner_sample.jpg) no-repeat center; background-size: cover;">
      <div class="cover-responsive-img">
        <div class="cover-responsive-overlay"></div>
      </div>
      <div class="container position-relative">
        <img src="/template_avrist/assets/img/pattern/ic_ornamen_yellow.png" class="pattern-purple" alt="Ornamen Purple">
        <!-- <img src="/template_avrist/assets/img/pattern/ic_ornament_triangle_yellow.png" class="pattern-triangle-yellow" alt="Ornamen Triangle Yellow"> -->
        <img src="/template_avrist/assets/img/pattern/ic_ornament_circle_pink.png" class="pattern-pink" alt="Ornamen Circle Pink">
        <nav aria-label="breadcrumb" class="general-breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active"><a href="#">Avrist General Insurance</a></li>
          </ol>
        </nav>
        <div class="cover-content">
          <h3 class="cover-title text-white animated fadeInUp delayp1">Avrist General Insurance</h3>
          <p class="cover-content text-white">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
          tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
          quis nostrud exercitation ullamco.</p>
        </div>
      </div>
      <!-- <a href="#" class="scroll-hint bottom-0 animated fadeInDown delayp4">
        <img src="/template_avrist/assets/img/common/arrow_down.png" alt="Arrow down hint"/>
      </a> -->
    </div>

    <section class="py-main section-life-insurance-1">
      <img src="/template_avrist/assets/img/pattern/ornamen_waves_layer2_grey.png" class="pattern-grey" alt="Gray Waves">
      <img src="/template_avrist/assets/img/pattern/ic_ornamen_yellow.png" class="pattern-yellow" alt="Gray Waves">
      <div class="container">
        <div class="heading vp-fadeinup delayp2">
          <h2>Tentang</h2>
          <p class="col-md-8 p-0 subtitle">Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. </p>
        </div>

        <div class="content mt-5">
          <div class="row">
            <div class="col-6 col-md-6 vp-fadeinup delayp1">
              <a href="#" class="card p-box card-insurance">
                <div class="card-img">
                  <img src="/template_avrist/assets/img/tentang/ic_general_tentang_investasi.png" class="img-fluid">
                  <h5 class="card-title">Tentang Investasi</h5>
                </div>
              </a>
            </div>
            <div class="col-6 col-md-6 vp-fadeinup delayp1">
              <a href="#" class="card p-box card-insurance">
                <div class="card-img">
                  <img src="/template_avrist/assets/img/tentang/ic_general_perencanaan_keuangan.png" class="img-fluid">
                  <h5 class="card-title">Perencanaan Keuangan</h5>
                </div>
              </a>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="py-main section-life-insurance-2">
      <div class="container">
        <div class="heading vp-fadeinup delayp1">
          <h2>Proteksi Lengkap</h2>
          <p class="subtitle col-md-8 p-0">Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. </p>
        </div>
        <div class="content">
          <div class="owl-carousel owl-theme owl-dots-solid owl-insurance mt-5 vp-fadeinup delayp2">

            <a href="#" class="card p-box card-insurance insurance-2">
              <div class="card-img">
                <img src="/template_avrist/assets/img/tentang/ic_general_layanan_kendaraan.png" class="img-fluid">
                <h5 class="card-title">Kendaraan</h5>
              </div>
            </a>

            <a href="#" class="card p-box card-insurance insurance-2">
              <div class="card-img">
                <img src="/template_avrist/assets/img/tentang/ic_general_layanan_bangunan.png" class="img-fluid">
                <h5 class="card-title">Bangunan</h5>
              </div>
            </a>

            <a href="#" class="card p-box card-insurance insurance-2">
              <div class="card-img">
                <img src="/template_avrist/assets/img/tentang/ic_general_layanan_kecelakaan.png" class="img-fluid">
                <h5 class="card-title">Kecelakaan</h5>
              </div>
            </a>

            <a href="#" class="card p-box card-insurance insurance-2">
              <div class="card-img">
                <img src="/template_avrist/assets/img/tentang/ic_general_layanan_casualy.png" class="img-fluid">
                <h5 class="card-title">Casualty</h5>
              </div>
            </a>

            <a href="#" class="card p-box card-insurance insurance-2">
              <div class="card-img">
                <img src="/template_avrist/assets/img/tentang/ic_general_layanan_glass.png" class="img-fluid">
                <h5 class="card-title">Kacamata</h5>
              </div>
            </a>

            
          </div>
        </div>
      </div>
    </section>

    <section class="py-main section-life-insurance-3">
      <div class="container">
        <div class="heading">
          <h2>Keuntungan</h2>
        </div>
        <div class="content">
          <div class="row">
            <div class="col-md-4 vp-fadeinup delayp1">
              <div class="card-information generalInsurance">
                <h4 class="number">1</h4>
                <p>Premi Mulai dari Rp5000/hari</p>
              </div>
            </div>

            <div class="col-md-4 vp-fadeinup delayp1">
              <div class="card-information generalInsurance">
                <h4 class="number">2</h4>
                <p>Pertanggungan maksimum hinggal Rp 1Milyar</p>
              </div>
            </div>

            <div class="col-md-4 vp-fadeinup delayp1">
              <div class="card-information generalInsurance">
                <h4 class="number">3</h4>
                <p>Premi Mulai dari Rp5000/hari</p>
              </div>
            </div>

            <div class="col-md-4 vp-fadeinup delayp1">
              <div class="card-information generalInsurance">
                <h4 class="number">4</h4>
                <p>Premi Mulai dari Rp5000/hari</p>
              </div>
            </div>

            <div class="col-md-4 vp-fadeinup delayp1">
              <div class="card-information generalInsurance">
                <h4 class="number">5</h4>
                <p>Premi Mulai dari Rp5000/hari</p>
              </div>
            </div>

            <div class="col-md-4 vp-fadeinup delayp1">
              <div class="card-information generalInsurance">
                <h4 class="number">6</h4>
                <p>Premi Mulai dari Rp5000/hari</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="py-main section-life-insurance-4">
      <img src="/template_avrist/assets/img/pattern/ic_ornamen_green.png" class="pattern-green vp-fadeinup delayp1" alt="Ornamen green">
      <div class="container">
        <div class="heading-w-link">
          <h2>Kenali kebutuhanmu</h2>
          <div class="heading-group">
            <div class="row">
              <div class="col-md-6">
                <p class="text-uppercase">by life group</p>
              </div>
              <div class="col-md-6 text-md-right">
                <a href="/about/life-insurance/news">Lihat semua</a>
              </div>
            </div>
          </div>
        </div>
        <div class="content text-white">
          <div class="owl-carousel owl-theme owl-dots-solid owl-kebutuhan mt-5 vp-fadeinup delayp2">
          <a href="/about/life-insurance/news/detail" class="card card-kebutuhan">
            <div class="card-img">
              <img src="/template_avrist/assets/img/sample/bg_rectangle-dark-2.jpg" class="img-fluid">
            </div>
            <div class="card-body">
              <p class="card-date">12 Agustus 2018</p>
              <h5 class="card-title">Ketahui Asuransi Jiwa yang Diperlukan Keluarga</h5>
            </div>
          </a>

          <a href="/about/life-insurance/news/detail" class="card card-kebutuhan">
            <div class="card-img">
              <img src="/template_avrist/assets/img/sample/bg_rectangle-light-1.jpg" class="img-fluid">
            </div>
            <div class="card-body">
              <p class="card-date">12 Agustus 2018</p>
              <h5 class="card-title">Ketahui Asuransi Jiwa yang Diperlukan Keluarga</h5>
            </div>
          </a>

          <a href="/about/life-insurance/news/detail" class="card card-kebutuhan">
            <div class="card-img">
              <img src="/template_avrist/assets/img/sample/bg_rectangle-light-2.jpg" class="img-fluid">
            </div>
            <div class="card-body">
              <p class="card-date">12 Agustus 2018</p>
              <h5 class="card-title">Ketahui Asuransi Jiwa yang Diperlukan Keluarga</h5>
            </div>
          </a>

          <a href="/about/life-insurance/news/detail" class="card card-kebutuhan">
            <div class="card-img">
              <img src="/template_avrist/assets/img/sample/bg_rectangle-light-3.jpeg" class="img-fluid">
            </div>
            <div class="card-body">
              <p class="card-date">12 Agustus 2018</p>
              <h5 class="card-title">Ketahui Asuransi Jiwa yang Diperlukan Keluarga</h5>
            </div>
          </a>

          <a href="/about/life-insurance/news/detail" class="card card-kebutuhan">
            <div class="card-img">
              <img src="/template_avrist/assets/img/sample/bg_rectangle-dark-2.jpg" class="img-fluid">
            </div>
            <div class="card-body">
              <p class="card-date">12 Agustus 2018</p>
              <h5 class="card-title">Ketahui Asuransi Jiwa yang Diperlukan Keluarga</h5>
            </div>
          </a>

        </div>
        </div>
      </div>
    </section>

    <section class="bg-light py-main section-life-insurance-5" style="background: url(/template_avrist/assets/img/common/img_banner_solution_general.jpg) no-repeat center; background-size: cover;">
        <div class="container">
          <div class="row">
            <div class="col-md-7">
              <h2>Butuh perhitungan dan proteksi ideal untukmu atau bisnismu? <a href="/get-solution/personal">Dapatkan Solusi</a></h2>
            </div>
          </div>
        </div>
      </section>


    <section class="py-main section-layanan-kamu">
      <div class="container position-relative">
        <img src="/template_avrist/assets/img/pattern/ic_ornamen_green.png" class="pattern-green" alt="Pattern Green">
        <div class="heading text-center mb-5 vp-fadeinup delayp1">
          <h2>Layanan Kami</h2>
        </div>
        <div class="content">
          <div class="row">
            <div class="col-md-4 offset-md-2 vp-fadeinup delayp2">
              <a href="#" class="card card-service">
                <img src="/template_avrist/assets/img/common/ic_ilustrasi_isometrik_RS.png" class="img-fluid service-img" alt="Card Img">
                <div class="content">
                  <h5 class="card-title">Rumah Sakit <br>Partner</h5>
                </div>
              </a>
            </div>
            <div class="col-md-4 vp-fadeinup delayp3">
              <a href="#" class="card card-service">
                <img src="/template_avrist/assets/img/common/ic_ilustrasi_isometrik_RS.png" class="img-fluid service-img" alt="Card Img">
                <div class="content">
                  <h5 class="card-title">Klaim Asuransi Saya</h5>
                </div>
              </a>
            </div>
            <!-- <div class="col-md-4 vp-fadeinup delayp4">
              <a href="#" class="card card-service">
                <img src="/template_avrist/assets/img/common/ic_ilustrasi_isometrik_RS.png" class="img-fluid service-img" alt="Card Img">
                <div class="content">
                  <h5 class="card-title">Harga Unit</h5>
                </div>
              </a>
            </div> -->
          </div>
        </div>
      </div>
    </section>