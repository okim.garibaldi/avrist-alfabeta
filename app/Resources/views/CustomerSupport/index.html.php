<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

use Pimcore\Model\Document;
use Pimcore\Model\Document\Page;

$this->extend('layout_avrist_new.html.php');?>

<div class="cover cover-sm cover-responsive <?= $bgBanner['bg'] ?> cover-general">
    <div class="cover-responsive-img">
        <div class="cover-responsive-overlay"></div>
    </div>
    <div class="container position-relative">
        <img src="/template_avrist/assets/img/pattern/ic_ornamen_yellow.png" class="pattern-purple" alt="Ornamen Purple">
        <!-- <img src="/template_avrist/assets/img/pattern/ic_ornament_triangle_yellow.png" class="pattern-triangle-yellow" alt="Ornamen Triangle Yellow"> -->
        <img src="/template_avrist/assets/img/pattern/ic_ornament_circle_pink.png" class="pattern-pink" alt="Ornamen Circle Pink">
        <nav aria-label="breadcrumb" class="general-breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item"><a href="#">Contact Us</a></li>
                <li class="breadcrumb-item active"><a href="#"><?= $customerSupport->getName() ?></a></li>
            </ol>
        </nav>
        <div class="cover-content">
            <h3 class="cover-title text-white animated fadeInUp delayp1"><?= $customerSupport->getName() ?></h3>
            <p class="cover-text"><?= $customerSupport->getDescription() ?></p>
        </div>

    </div>
</div>

<section class="py-main section-ask-support">
    <img src="<?= $customerSupport->getPageImage() ?>" class="customer-care img-fluid animated vp-fadeinup delayp2" alt="Customer Care">
    <div class="container">
        <div class="row">
            <div class="col-md-6 offset-md-6">
                <div class="card card-support">
                    <div class="card-body">
                        <form id = "messageForm" class="needs-validation" method="POST" novalidate>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="name" id="name" placeholder="Nama" value="" required>
                                        <div class="invalid-feedback">
                                            Please enter first name
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="email" class="form-control "name="email" id="email" placeholder="Alamat Email" value="" required>
                                        <div class="invalid-feedback">
                                            Please enter email
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="tel" class="form-control" name="phoneNumber" id="phoneNumber" placeholder="Phone Number" value="" required>
                                        <div class="invalid-feedback">
                                            Please enter phone number
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 mb-3">
                                    <div class="form-group">
                                        <div class="form-group">
                                            <select class="custom-select" name="topik" id="topik" required>
                                                <option value="">Topik</option>
                                            <?php foreach($topikOptions as $toO): ?>
                                                <option value="<?= $toO['value'] ?>"><?= $toO['key'] ?></option>
                                            <?php endforeach; ?>
                                            </select>
                                            <div class="invalid-feedback">
                                                Please choose a valid topik
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 mb-3">
                                    <div class="form-group">
                                        <div class="form-group">
                                            <select class="custom-select" name="time" id="time" required>
                                                <option value="">Waktu yang bisa dihubungi</option>
                                            <?php foreach($timeOptions as $tiO): ?>
                                                <option value="<?= $tiO['value'] ?>"><?= $tiO['key'] ?></option>
                                            <?php endforeach; ?>
                                            </select>
                                            <div class="invalid-feedback">
                                                Please choose a valid topik
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="message" id="message" placeholder="Message" value="" required>
                                        <div class="invalid-feedback">
                                            Please enter message
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 text-md-right">
                                    <button class="btn btn-primary" type="submit">Kirim</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>