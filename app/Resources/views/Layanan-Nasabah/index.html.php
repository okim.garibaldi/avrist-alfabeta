<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

$this->extend('layout_avrist_new.html.php');?>

<div class="cover cover-sm cover-responsive bg-primary cover-general">
    <div class="cover-responsive-img">
        <div class="cover-responsive-overlay"></div>
    </div>
    <div class="container position-relative">
        <img src="/template_avrist/assets/img/pattern/ic_ornamen_yellow.png" class="pattern-purple" alt="Ornamen Purple">
        <!-- <img src="/template_avrist/assets/img/pattern/ic_ornament_triangle_yellow.png" class="pattern-triangle-yellow" alt="Ornamen Triangle Yellow"> -->
        <img src="/template_avrist/assets/img/pattern/ic_ornament_circle_pink.png" class="pattern-pink" alt="Ornamen Circle Pink">
        <nav aria-label="breadcrumb" class="general-breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active"><a href="#"><?= $layananNasabahPage->getTitle1() ?></a></li>
            </ol>
        </nav>
        <div class="cover-content">
            <h3 class="cover-title text-white animated fadeInUp delayp1"><?= $layananNasabahPage->getTitle1() ?></h3>
            <div class="col-md-6 pl-0">
                <div class="form-group">
                    <input type="text" class="form-control transparent" id="searchBox" placeholder="Topik atau Pertanyaan" value="" required>
                    <div class="invalid-feedback">
                        Please
                    </div>
                </div>
            </div>
            <div class="clearfix" id="cari">
                <a href="#" class="btn btn-light w-150">Cari</a>
            </div>
        </div>
    </div>
</div>

<?php if(!empty($layananNasabah)): ?>
<section class="py-main section-life-insurance-1 pb-0">
    <img src="/template_avrist/assets/img/pattern/ornamen_waves_layer2_grey.png" class="pattern-grey" alt="Gray Waves">
    <img src="/template_avrist/assets/img/pattern/ic_ornamen_yellow.png" class="pattern-yellow" alt="Gray Waves">
    <div class="container">
        <div class="heading vp-fadeinup delayp2">
            <h2><?= $layananNasabahPage->getTitle2() ?></h2>
            <p class="p-0 subtitle"><?= $layananNasabahPage->getDescription2() ?></p>
        </div>

        <div class="content mt-5">
            <div class="row">
            <?php foreach($layananNasabah as $lN): ?>
                <div class="col-6 col-md-6 vp-fadeinup delayp1">
                    <div class="card card-general">
                        <div class="card-body">
                            <h5><?= $lN->getName() ?></h5>
                            <p class="mb-0 content-text"><?= $lN->getDescription() ?></p>
                            <div class="clearfix mt-4">
                                <a href="<?= substr(str_replace($layananNasabahPage->getFullPath(), '', $lN->getFullPath()), 1) ?>" class="text-uppercase">Pelajari lebih lanjut</a>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
            </div>
        </div>
    </div>
</section>
<?php endif; ?>


<?php if(!empty($arrayBlock2)): ?>
<section class="py-main section-life-insurance-2 pb-0">
    <div class="container">
        <div class="heading vp-fadeinup delayp1">
            <h2><?= $layananNasabahPage->getTitle3() ?></h2>
            <p class="p-0 subtitle p-0"><?= $layananNasabahPage->getDescription3() ?></p>
        </div>

        <div class="content mt-5">
            <div class="row">
            <?php foreach($arrayBlock2 as $block2): ?>
                <div class="col-6 col-md-4 vp-fadeinup delayp1">
                    <div class="card card-general layanan">
                        <div class="card-body">
                            <h5><?= $block2['title'] ?></h5>
                            <p class="mb-0 content-text"><?= $block2['description'] ?></p>
                            <div class="clearfix mt-4">
                                <a href="<?= $block2['file'] ?>" download class="text-uppercase">Download Panduan</a>
                            </div>
                        </div>
                    </div>
                </div>
           <?php endforeach; ?> 
            </div>
        </div>

    </div>
</section>
<?php endif; ?>

<?php if(!empty($arrayBlock3)): ?>
<section class="py-main section-life-insurance-2 pb-0">
    <div class="container">
        <div class="heading vp-fadeinup delayp1">
            <h2><?= $layananNasabahPage->getTitle4() ?></h2>
            <p class="p-0 subtitle p-0"><?= $layananNasabahPage->getDescription4() ?> </p>
        </div>

        <div class="content mt-5">
            <div class="row">
            <?php foreach($arrayBlock3 as $block3): ?>
                <div class="col-6 col-md-4 vp-fadeinup delayp1">
                    <div class="card card-general layanan">
                        <div class="card-body">
                            <h5><?= $block3['title'] ?></h5>
                            <p class="mb-0 content-text"><?= $block3['description'] ?>.</p>
                            <div class="clearfix mt-4">
                                <a href="<?= substr($block3['formulir'],1) ?>" class="text-uppercase">Pelajari Lebih Lanjut</a>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
            </div>
        </div>

    </div>
</section>
<?php endif; ?>

<?php if(!empty($arrayBlock4)): ?>
<section class="py-main section-life-insurance-2">
    <div class="container">
        <div class="heading vp-fadeinup delayp1">
            <h2><?= $layananNasabahPage->getTitle5() ?></h2>
            <p class="p-0 subtitle p-0"><?= $layananNasabahPage->getDescription5() ?></p>
        </div>

        <div class="content mt-5">
            <div class="row">
            <?php foreach($arrayBlock4 as $block4): ?>
                <div class="col-6 col-md-6 vp-fadeinup delayp1">
                    <div class="card card-general layanan">
                        <div class="card-body">
                            <h5><?= $block4['name'] ?></h5>
                            <p class="mb-0 content-text"><?= $block4['description'] ?></p>
                            <div class="clearfix mt-4">
                                <a href="<?= $block4['page'] ?>" class="text-uppercase">Submit</a>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
            </div>
        </div>

    </div>
</section>
<?php endif; ?>

<section class="py-main bg-light">
    <div class="container">
        <div class="row row-3">
            <div class="col-md-6 content-center">
                <div class="heading">
                    <h2 class="mb-4"><?= $layananNasabahPage->getContactUs() ?></h2>
                    <p><?= $layananNasabahPage->getContactUsDescription() ?></p>
                    <a href="<?= $layananNasabahPage->getContactUsPage() ?>" class="btn btn-primary">Contact Us</a>
                </div>
            </div>
            <div class="col-md-6">
                <img src="/template_avrist/assets/img/tentang/ic_ilustrasi_home_mengapaavrist.png" class="img-fluid animated vp-fadeinup delayp1">
            </div>
        </div>
    </div>
</section>