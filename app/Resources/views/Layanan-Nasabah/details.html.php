<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

$this->extend('layout_avrist_new.html.php');?>

<div class="cover cover-sm cover-responsive bg-primary cover-general">
    <div class="cover-responsive-img">
        <div class="cover-responsive-overlay"></div>
    </div>
    <div class="container position-relative">
        <img src="/template_avrist/assets/img/pattern/ic_ornamen_yellow.png" class="pattern-purple" alt="Ornamen Purple">
        <!-- <img src="/template_avrist/assets/img/pattern/ic_ornament_triangle_yellow.png" class="pattern-triangle-yellow" alt="Ornamen Triangle Yellow"> -->
        <img src="/template_avrist/assets/img/pattern/ic_ornament_circle_pink.png" class="pattern-pink" alt="Ornamen Circle Pink">
        <nav aria-label="breadcrumb" class="general-breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active"><a href="#"><?= $layananNasabah->getTitle() ?></a></li>
            </ol>
        </nav>
        <div class="cover-content">
            <h3 class="cover-title text-white animated fadeInUp delayp1"><?= $layananNasabah->getTitle() ?></h3>
            <p class="cover-text"><?= $layananNasabah->getDescription() ?></p>
        </div>
    </div>
</div>

<section class="tab-general">
    <div class="container">
        <ul class="nav nav-tabs general-tab-cover" id="generalTab" role="tablist">
        <?php $active = " active"; ?>
        <?php $selected = "true"; ?>
        <?php foreach($layananNasabah->getBlock() as $block): ?>
            <li class="nav-item">
                <a class="nav-link<?= $active ?>" id="<?= strtolower($block['category']->getData()) ?>-tab" data-toggle="tab" href="#<?= strtolower($block['category']->getData()) ?>" role="tab" aria-controls="<?= strtolower($block['category']->getData()) ?>" aria-selected="<?= $selected ?>"><?= $block['category']->getData() ?></a>
            </li>
            <?php if($active == " active" || $selected == "true"): ?>
                <?php $active = ""; ?>
                <?php $selected = "false"; ?>
            <?php endif; ?>
        <?php endforeach; ?>
        </ul>
        <div class="tab-content" id="generalTabContent">
        <?php $show = " show active"; ?>
        <?php foreach($layananNasabah->getBlock() as $block): ?>
            <div class="tab-pane fade<?= $show ?>" id="<?= strtolower($block['category']->getData()) ?>" role="tabpanel" aria-labelledby="<?= strtolower($block['category']->getData()) ?>-tab">
                <div class="py-main">

                    <div class="row">
                    <?php foreach($block['subblock']->getData() as $subblock): ?>
                        <div class="col-6 col-md-4 vp-fadeinup delayp1">
                            <div class="card card-general">
                                <div class="card-body">
                                    <h5><?= $subblock['title']->getData() ?></h5>
                                    <p class="mb-0 content-text"><?= $subblock['description']->getData() ?></p>
                                    <div class="clearfix mt-4">
                                        <a href="<?= $subblock['link']->getData() ?>" class="text-uppercase">Check Sekarang</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                    </div>
                </div>
            </div>
            <?php if($show == " show active"): ?>
                <?php $show = ""; ?>
            <?php endif; ?>
        <?php endforeach; ?>
</section>

<section class="py-main bg-light">
    <div class="container">
        <div class="row row-3">
            <div class="col-md-6 content-center">
                <div class="heading">
                    <h2 class="mb-4"><?= $layananNasabah->getContactUs() ?></h2>
                    <p><?= $layananNasabah->getContactUsDescription() ?></p>
                    <a href="<?= $layananNasabah->getContactUsPage() ?>" class="btn btn-primary">Contact Us</a>
                </div>
            </div>
            <div class="col-md-6">
                <img src="/template_avrist/assets/img/tentang/ic_ilustrasi_home_mengapaavrist.png" class="img-fluid animated vp-fadeinup delayp1">
            </div>
        </div>
    </div>
</section>