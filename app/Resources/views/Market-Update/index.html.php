<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

use Pimcore\Model\Document;
use Pimcore\Model\Document\Page;

$this->extend('layout_avrist_new.html.php');?>

<div class="cover cover-sm cover-responsive bg-avram cover-general">
    <div class="cover-responsive-img">
        <div class="cover-responsive-overlay"></div>
    </div>
    <div class="container position-relative">
        <img src="/template_avrist/assets/img/pattern/ic_ornamen_yellow.png" class="pattern-purple" alt="Ornamen Purple">
        <!-- <img src="/template_avrist/assets/img/pattern/ic_ornament_triangle_yellow.png" class="pattern-triangle-yellow" alt="Ornamen Triangle Yellow"> -->
        <img src="/template_avrist/assets/img/pattern/ic_ornament_circle_pink.png" class="pattern-pink" alt="Ornamen Circle Pink">
        <nav aria-label="breadcrumb" class="general-breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active"><a href="#"><?= $marketUpdate->getTitle() ?></a></li>
            </ol>
        </nav>
        <div class="cover-content">
            <h3 class="cover-title text-white animated fadeInUp delayp1"><?= $marketUpdate->getTitle() ?></h3>
        </div>
    </div>
</div>

<section class="tab-general">
    <div class="container">
        <ul class="nav nav-tabs general-tab-cover" id="generalTab" role="tablist">
        <?php $active = " active"; ?>
        <?php $selected = "true"; ?>
        <?php foreach($arr as $arrName => $name): ?>
            <li class="nav-item">
                <a class="nav-link <?= $active ?>" id="<?= strtolower($arrName) ?>-tab" data-toggle="tab" href="#<?= strtolower($arrName) ?>" role="tab" aria-controls="<?= strtolower($arrName) ?>" aria-selected="<?= $selected ?>"><?= $arrName ?></a>
            </li>
            <?php if($active == " active" || $selected = "true"): ?>
                <?php $active = ""; ?>
                <?php $selected = "false"; ?>
            <?php endif; ?>
        <?php endforeach; ?>
        </ul>
        <div class="tab-content" id="generalTabContent">
        <?php $show = " show active"; ?>
        <?php foreach($arr as $arrName => $name): ?>
            <div class="tab-pane fade<?= $show ?>" id="<?= strtolower($arrName) ?>" role="tabpanel" aria-labelledby="<?= strtolower($arrName) ?>-tab">
                <div class="py-main">
                <?php foreach($name as $yearKey => $year): ?>
                    <div class="heading mb-3">
                        <h2 class="font-weight-bold" style="color: #b2bbc3;"><?= $yearKey ?></h2>
                    </div>
                    <div class="row">
                    <?php foreach($year as $fileKey => $file): ?>
                        <div class="col-6 col-md-3">
                            <a href="<?= $file['file']->getFullPath() ?>" download class="card card-list">
                                <div class="card-body">
                                    <h3 class="text-truncate"><?= $file['name'] ?> <i class="fal fa-angle-right"></i></h3>
                                    <p><img src="/template_avrist/assets/img/tentang/pdf.png" class="extension-file">Pdf Files | <?= number_format($file['file']->getFileSize() / 1024, 2) . ' KB' ?></p>
                                    <p class="download">Download</p>
                                </div>
                            </a>
                        </div>
                    <?php endforeach; ?>
                    </div>
                <?php endforeach; ?>
                </div>
            </div>
            <?php if($show = " show active"): ?>
                <?php $show = ""; ?>
            <?php endif; ?>
        <?php endforeach; ?>
        </div>
    </div>
</section>