<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

use Pimcore\Model\Document;
use Pimcore\Model\Document\Page;

$this->extend('layout_avrist_new.html.php');?>

<div class="cover cover-sm cover-responsive <?= $bgBanner['bg'] ?> cover-general">
    <div class="cover-responsive-img">
        <div class="cover-responsive-overlay"></div>
    </div>
    <div class="container position-relative">
        <img src="/template_avrist/assets/img/pattern/ic_ornamen_yellow.png" class="pattern-purple" alt="Ornamen Purple">
        <!-- <img src="/template_avrist/assets/img/pattern/ic_ornament_triangle_yellow.png" class="pattern-triangle-yellow" alt="Ornamen Triangle Yellow"> -->
        <img src="/template_avrist/assets/img/pattern/ic_ornament_circle_pink.png" class="pattern-pink" alt="Ornamen Circle Pink">
        <nav aria-label="breadcrumb" class="general-breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item"><a href="#"><?= $aboutCompany->getName() ?></a></li>
                <li class="breadcrumb-item"><a href="#">Awards</a></li>
                <li class="breadcrumb-item active"><a href="#">Detail</a></li>
            </ol>
        </nav>
        <div class="cover-content">
            <h3 class="cover-title text-white animated fadeInUp delayp1"><?= $awards->getTitle1() ?></h3>
            <p class="cover-subtitle text-white"><?= date("j F Y", strtotime($awards->getPublishDate())) ?></p>
        </div>
    </div>
</div>

<section class="py-main section-awards-detail">
    <img src="/template_avrist/assets/img/pattern/img_ornamen_waves_grey.png" class="pattern-grey" alt="Pattern Grey">
    <div class="container">
    <?php foreach($awards->getBlock1() as $block1): ?>
        <img src="<?= $block1['imageBlock']->getData() ?>" class="img-fluid my-5 w-100">

        <div class="heading">
            <h2 class="animated vp-fadeinup delayp1"><?= $block1['titleBlock']->getData() ?></h2>
        </div>
        <p class="animated vp-fadeinup delayp1"><?= $block1['descriptionBlock']->getData() ?></p>
    <?php endforeach; ?>
    </div>
</section>

<section class="py-main section-topik-awards bg-gray">
    <div class="container">
        <div class="heading mb-3">
            <h2>Topik Terkait</h2>
        </div>
        <div class="row">
        <?php foreach($otherAwards as $oA): ?>
            <div class="col-md-4">
                <a href="<?= "/".str_replace($awardsPage->getPath(), '', $oA->getFullPath()) ?> " class="card card-kebutuhan">
                    <div class="card-img">
                        <img src="<?= $oA->getImage1() ?>" class="img-fluid">
                    </div>
                    <div class="card-body p-box">
                        <h5 class="card-title text-truncate-twoline"><?= $oA->getTitle1()?></h5>
                        <p href="#" class="link">Lihat Detail</p>
                    </div>
                </a>
            </div>
        <?php endforeach; ?>
        </div>
    </div>
</section>