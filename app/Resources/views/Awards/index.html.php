<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

use Pimcore\Model\Document;
use Pimcore\Model\Document\Page;

$this->extend('layout_avrist_new.html.php');?>

<div class="cover cover-sm cover-responsive cover-general" style="background: linear-gradient(270deg, rgba(0,0,0,0.00) 0%, <?= $bgBanner['color'] ?> 70%), url(<?= $bgBanner['bgImg'] ?>) no-repeat center; background-size: cover;">
    <div class="cover-responsive-img">
        <div class="cover-responsive-overlay"></div>
    </div>
    <div class="container position-relative">
        <img src="/template_avrist/assets/img/pattern/ic_ornamen_yellow.png" class="pattern-purple" alt="Ornamen Purple">
        <!-- <img src="/template_avrist/assets/img/pattern/ic_ornament_triangle_yellow.png" class="pattern-triangle-yellow" alt="Ornamen Triangle Yellow"> -->
        <img src="/template_avrist/assets/img/pattern/ic_ornament_circle_pink.png" class="pattern-pink" alt="Ornamen Circle Pink">
        <nav aria-label="breadcrumb" class="general-breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item"><a href="#"><?= $aboutCompany->getName() ?></a></li>
                <li class="breadcrumb-item active"><a href="#">Awards</a></li>
            </ol>
        </nav>
        <div class="cover-content">
            <h3 class="cover-title text-white animated fadeInUp delayp1"><?= $awardsPage->getTitle1() ?></h3>
            <p class="cover-subtitle text-white"><?= $awardsPage->getDescription1() ?></p>
        </div>
    </div>
</div>

<section class="py-main section-general-tentang general-page">
    <div class="container">
        <div class="row">
        <?php foreach($awards as $a): ?>
            <div class="col-md-4">
                <a href="<?= str_replace($awardsPage->getPath(), '', $a->getFullPath()) ?>" class="card card-kebutuhan news">
                    <div class="card-img">
                        <img src="<?= $a->getImage1() ?>" class="img-fluid">
                    </div>
                    <div class="card-body p-box">
                        <h5 class="card-title text-truncate-twoline"><?= $a->getTitle1() ?></h5>
                        <p href="#" class="link">Lihat Detail</p>
                    </div>
                </a>
            </div>
        <?php endforeach; ?>
        </div>
    </div>
</section>