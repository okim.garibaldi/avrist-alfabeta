<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
  <head>
    <?= $this->template("include/head2.html.php"); ?>
    <?= $this->template("include/tracking-script/head.html.php"); ?>
  </head>
  <body>
    <?= $this->template("include/tracking-script/body.html.php"); ?>

    <div class="loader-wrapper loader-light">
      <div class="loader"></div>
    </div> 

    <?php //include($prefix."include/navbar.php"); ?>

    <section class="py-main section-login vp-fadeinup">
      <div class="container container-xs">
        <div class="box-form p-box">
          <div class="header">
            <a onclick="goBack()" class="btn btn-back mb-4">
              <i class="fal fa-chevron-left"></i>
            </a>
            <a class="header-brand" href="/">
              <img src="/template_avrist/assets/img/brand/logo_avrist.png" class="img-fluid" alt="Logo">
            </a>
          </div>
          <div class="subheading">
            <h2>Login</h2>
            <p>Login to customer dashboard</p>
            <div class="ContainerLeft MessageContainer" id="divMessageMain">
                <img class="Grow1">
                <dl class="Grow1">
                    <dt></dt>
                    <dd></dd>
                </dl>
            </div>
          </div>
          <form class="needs-validation" method="POST" onsubmit="submitLogin(); return false;" novalidate>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label for="" class="sr-only">Email</label>
                  <input type="text" class="form-control" id="loginUsername" placeholder="Enter email" value="" required>
                  <div class="invalid-feedback">
                    Please enter your email address
                  </div>
                </div>
              </div>
              <div class="col-md-12 mb-4">
                <div class="form-group">
                  <label for="" class="sr-only">Password</label>
                  <input type="password" class="form-control" id="loginPassword" placeholder="Enter password" value="" required>
                  <div class="invalid-feedback">
                    Please enter your password
                  </div>
                  <a href="#" class="float-right forgot-pass">Lupa Password</a>
                </div>
              </div>
              <div class="col-12">
                <button class="btn btn-primary btn-block mb-3" type="submit">Login</button>
                <button class="btn btn-secondary btn-block mb-3" type="submit">Daftar Sekarang!</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </section>


    <?php //include($prefix."include/footer.php"); ?>
    <?= $this->template("include/script2.html.php"); ?>

    <script>
      // Navbar active state
      $('#navFaq').addClass('active');

      // Animation on load
      document.addEventListener("DOMContentLoaded", function(event) {
        $(".loader").fadeOut('slow');
        $(".loader-wrapper").fadeOut("slow");
      //  $(".anim-1").addClass("animated fadeInLeft delayp10");
       // $(".anim-2").addClass("animated fadeInUp delayp12");
        //$(".anim-3").addClass("animated fadeInUp delayp14");  
      });

      // Back to previous page
      function goBack() {
        window.history.back();
      }


    </script>

  </body>
</html>
