<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

use Pimcore\Model\Document;
use Pimcore\Model\Document\Page;

$this->extend('layout_avrist_new.html.php');?> 

<div class="cover cover-sm cover-responsive <?= $bgBanner['bg'] ?> cover-general" style="background: linear-gradient(270deg, rgba(0,0,0,0.00) 0%, <?= $bgBanner['color'] ?> 70%), url(<?= $bgBanner['bgImg'] ?>) no-repeat center; background-size: cover;">
    <div class="cover-responsive-img">
        <div class="cover-responsive-overlay"></div>
        <img src="/template_avrist/assets/img/common/img_banner_sample.jpg" class="img-fluid d-block d-md-none">
    </div>
    <div class="container position-relative">
        <img src="/template_avrist/assets/img/pattern/ic_ornamen_yellow.png" class="pattern-purple" alt="Ornamen Purple">
        <!-- <img src="/template_avrist/assets/img/pattern/ic_ornament_triangle_yellow.png" class="pattern-triangle-yellow" alt="Ornamen Triangle Yellow"> -->
        <img src="/template_avrist/assets/img/pattern/ic_ornament_circle_pink.png" class="pattern-pink" alt="Ornamen Circle Pink">
        <nav aria-label="breadcrumb" class="general-breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active"><a href="#"><?= $about->getPageTitle() ?></a></li>
            </ol>
        </nav>
        <div class="cover-content">
            <h3 class="cover-title text-white animated fadeInUp delayp1"><?= $about->getPageTitle() ?></h3>
            <p class="cover-subtitle text-white"><?= $about->getPageDescription() ?></p>
            <a href="<?= $about->getContactUsPage() ?>" class="btn btn-light mt-3"><?= $about->getContactUs() ?></a>
        </div>
    </div>
    <!-- <a href="#" class="scroll-hint bottom-0 animated fadeInDown delayp4">
        <img src="/template_avrist/assets/img/common/arrow_down.png" alt="Arrow down hint"/>
      </a> -->
</div>

<section class="py-main section-life-insurance-general">
    <div class="container">
        <div class="heading">
            <h2><?= $about->getContentTitle1() ?></h2>
        </div>
        <div class="row row-3">
            <div class="col-md-6">
                <p class="animated vp-fadeinup delayp1"><?= $about->getText1L() ?></p>
            </div>
            <div class="col-md-6 content-center">
                <img src="<?= $about->getImage1L() ?>" class="img-fluid animated vp-fadeinup delayp2">
            </div>
        </div>
        <div class="row row-3 mt-5">
            <div class="col-md-6 order-md-last">
                <p class="animated vp-fadeinup delayp1"><?= $about->getText1R() ?></p>
            </div>
            <div class="col-md-6 content-center">
                <img src="<?= $about->getImage1R() ?>" class="img-fluid animated vp-fadeinup delayp1">
            </div>
        </div>
    </div>
</section>

<section class="py-main section-tab-general avram pb-0">
    <!-- <img src="/template_avrist/assets/img/pattern/img_ornament_waves_purple.png" class="pattern-purple" alt="Pattern purple"> -->
    <div class="container position-relative">
        <ul class="nav nav-tabs general-tab avram" id="generalTab" role="tablist">
            <li class="nav-item">
            <?php $class = " active"; ?>
            <?php foreach($about->getBlock2() as $block2): ?>
                <a class="nav-link<?= $class ?>" id="<?= $block2['contentTitle']->getData() ?>-tab" data-toggle="tab" href="#<?= $block2['contentTitle']->getData() ?>" role="tab" aria-controls="<?= $block2['contentTitle']->getData() ?>" aria-selected="true">Milestone</a>
            <?php if($class != "" ): ?>
            <?php $class = ""; ?>
            <?php endif; ?>
        <?php endforeach; ?>
            </li>
        </ul>
        <div class="tab-content" id="generalTabContent">
        <?php $class = " show active"; ?>
        <?php foreach ($about->getBlock2() as $block2): ?>
            <div class="tab-pane fade show active" id="<?= $block2['contentTitle']->getData() ?>" role="tabpanel" aria-labelledby="<?= $block2['contentTitle']->getData() ?>-tab">
                <ul class="tab-list">
                <?php foreach($block2['block']->getData() as $b2Block): ?>
                    <li>
                        <div class="year"><?= $b2Block['year']->getData() ?></div>
                        <div class="detail">
                            <?= $b2Block['description']->getData() ?>
                        </div>
                    </li>
                <?php endforeach; ?>
                </ul>
            </div>
            <?php if($class != "" ): ?>
            <?php $class = ""; ?>
            <?php endif; ?>
        <?php endforeach; ?>
        </div>
    </div>
</section>

<section class="py-main section-life-insurance-1 pb-0">
    <!-- <img src="/template_avrist/assets/img/pattern/ornamen_waves_layer2_grey.png" class="pattern-grey" alt="Gray Waves"> -->
    <img src="/template_avrist/assets/img/pattern/ic_ornamen_yellow.png" class="pattern-yellow" alt="Gray Waves">
    <div class="container">
        <div class="heading vp-fadeinup delayp2">
            <h2><?= $about->getContentTitle4() ?></h2>
        </div>

        <div class="content mt-5">
            <div class="row">
                <div class="col-6 col-md-6 vp-fadeinup delayp1">
                    <a href="#" class="card p-box card-insurance" data-toggle="modal" data-target="#modalBoard<?= strtolower(str_replace(" ", "", ucwords($about->getCard1Name()))) ?>">
                        <div class="card-img">
                            <img src="<?= $about->getCard1Icon() ?>" class="img-fluid">
                            <h5 class="card-title"><?= $about->getCard1Name() ?></h5>
                        </div>
                    </a>
                </div>
                <div class="col-6 col-md-6 vp-fadeinup delayp1">
                    <a href="#" class="card p-box card-insurance" data-toggle="modal" data-target="#modalBoard<?= strtolower(str_replace(" ", "", ucwords($about->getCard2Name()))) ?>">
                        <div class="card-img">
                            <img src="<?= $about->getCard2Icon() ?>" class="img-fluid">
                            <h5 class="card-title"><?= $about->getCard2Name() ?></h5>
                        </div>
                    </a>
                </div>
            <?php if(!empty($about->getCard3Name())): ?>
                <div class="col-6 col-md-6 vp-fadeinup delayp1">
                    <a href="#" class="card p-box card-insurance" data-toggle="modal" data-target="#modalBoard<?= strtolower(str_replace(" ", "", ucwords($about->getCard3Name()))) ?>">
                        <div class="card-img">
                            <img src="<?= $about->getCard3Icon() ?>" class="img-fluid">
                            <h5 class="card-title"><?= $about->getCard3Name() ?></h5>
                        </div>
                    </a>
                </div>
            <?php endif;?>
            <?php if(!empty($about->getCard4Name())): ?>
                <div class="col-6 col-md-6 vp-fadeinup delayp1">
                    <a href="#" class="card p-box card-insurance" data-toggle="modal" data-target="#modalBoard<?= strtolower(str_replace(" ", "", ucwords($about->getCard4Name()))) ?>">
                        <div class="card-img">
                            <img src="<?= $about->getCard4Icon() ?>" class="img-fluid">
                            <h5 class="card-title"><?= $about->getCard4Name() ?></h5>
                        </div>
                    </a>
                </div>
            <?php endif;?>
            </div>
        </div>
    </div>
</section>

<section class="py-main section-life-insurance-4">
    <img src="/template_avrist/assets/img/pattern/ic_ornamen_green.png" class="pattern-green vp-fadeinup delayp1" alt="Ornamen green">
    <div class="container">
        <div class="heading-w-link">
            <h2><?= $about->getContentTitle6() ?></h2>
        </div>
        <div class="content text-white">
            <div class="owl-carousel owl-theme owl-dots-solid owl-kebutuhan mt-5 vp-fadeinup delayp2">
            <?php foreach($csr as $c): ?>
                <a href="<?= str_replace($csrPage->getPath(), '', $c->getFullPath()) ?>" class="card card-kebutuhan news">
                    <div class="card-img">
                        <img src="<?= $c->getImage1() ?>" class="img-fluid">
                    </div>
                    <div class="card-body">
                        <p class="card-date"><?= date_format($c->getPublishDate(), "j F Y") ?></p>
                        <h5 class="card-title text-truncate-twoline"><?= $c->getTitle1() ?></h5>
                        <p class="download">Lihat Semua</p>
                    </div>
                </a>
            <?php endforeach; ?>
            </div>
        </div>
    </div>
</section>

<div class="modal modal-general" tabindex="-1" role="dialog" id="modalBoard<?= strtolower(str_replace(" ", "", ucwords($about->getCard1Name()))) ?>">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="owl-carousel owl-theme owl-modal content">
            <?php foreach($about->getBlock3() as $block3): ?>
                <div class="item">
                    <div class="modal-body p-box">
                        <img src="/template_avrist/assets/img/pattern/ic_ornamen_yellow.png" class="pattern-purple" alt="Ornamen Purple">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <div class="row">
                            <div class="col-md-4 order-md-2 content-center">
                                <div class="img-wrapper">
                                    <img src="<?= $block3['card1Image']->getData() ?>" class="img-fluid">
                                </div>
                            </div>
                            <div class="col-md-8">
                                <h2><?= $block3['card1Title']->getData() ?></h2>
                                <h4><?= $block3['card1Subtitle']->getData() ?></h4>
                                <p class="mt-3"><?= $block3['card1Description']->getData() ?></p>
                            </div>
                            <div class="col-md-12 order-md-last">
                            <?php foreach($block3['card1Block']->getData() as $card1Block): ?>
                                <p><?= $card1Block['card1DetailedDescription']->getData() ?></p>
                            <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach ?>
            </div>
        </div>
    </div>
</div>
<div class="modal modal-general" tabindex="-1" role="dialog" id="modalBoard<?= strtolower(str_replace(" ", "", ucwords($about->getCard2Name()))) ?>">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="owl-carousel owl-theme owl-modal content">
            <?php foreach($about->getBlock4() as $block4): ?>
                <div class="item">
                    <div class="modal-body p-box">
                        <img src="/template_avrist/assets/img/pattern/ic_ornamen_yellow.png" class="pattern-purple" alt="Ornamen Purple">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <div class="row">
                            <div class="col-md-4 order-md-2 content-center">
                                <div class="img-wrapper">
                                    <img src="<?= $block4['card2Image']->getData() ?>" class="img-fluid">
                                </div>
                            </div>
                            <div class="col-md-8">
                                <h2><?= $block4['card2Title']->getData() ?></h2>
                                <h4><?= $block4['card2Subtitle']->getData() ?></h4>
                                <p class="mt-3"><?= $block4['card2Description']->getData() ?></p>
                            </div>
                            <div class="col-md-12 order-md-last">
                            <?php foreach($block4['card2Block']->getData() as $card2Block): ?>
                                <p><?= $card2Block['card2DetailedDescription']->getData() ?></p>
                            <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach ?>
            </div>
        </div>
    </div>
</div>
<div class="modal modal-general" tabindex="-1" role="dialog" id="modalBoard<?= strtolower(str_replace(" ", "", ucwords($about->getCard3Name()))) ?>">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="owl-carousel owl-theme owl-modal content">
            <?php foreach($about->getBlock5() as $block5): ?>
                <div class="item">
                    <div class="modal-body p-box">
                        <img src="/template_avrist/assets/img/pattern/ic_ornamen_yellow.png" class="pattern-purple" alt="Ornamen Purple">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <div class="row">
                            <div class="col-md-4 order-md-2 content-center">
                                <div class="img-wrapper">
                                    <img src="<?= $block5['card3Image']->getData() ?>" class="img-fluid">
                                </div>
                            </div>
                            <div class="col-md-8">
                                <h2><?= $block5['card3Title']->getData() ?></h2>
                                <h4><?= $block5['card3Subtitle']->getData() ?></h4>
                                <p class="mt-3"><?= $block5['card3Description']->getData() ?></p>
                            </div>
                            <div class="col-md-12 order-md-last">
                            <?php foreach($block5['card3Block']->getData() as $card3Block): ?>
                                <p><?= $card3Block['card3DetailedDescription']->getData() ?></p>
                            <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach ?>
            </div>
        </div>
    </div>
</div>
<div class="modal modal-general" tabindex="-1" role="dialog" id="modalBoard<?= strtolower(str_replace(" ", "", ucwords($about->getCard4Name()))) ?>">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="owl-carousel owl-theme owl-modal content">
            <?php foreach($about->getBlock6() as $block6): ?>
                <div class="item">
                    <div class="modal-body p-box">
                        <img src="/template_avrist/assets/img/pattern/ic_ornamen_yellow.png" class="pattern-purple" alt="Ornamen Purple">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <div class="row">
                            <div class="col-md-4 order-md-2 content-center">
                                <div class="img-wrapper">
                                    <img src="<?= $block6['card4Image']->getData() ?>" class="img-fluid">
                                </div>
                            </div>
                            <div class="col-md-8">
                                <h2><?= $block6['card4Title']->getData() ?></h2>
                                <h4><?= $block6['card4Subtitle']->getData() ?></h4>
                                <p class="mt-3"><?= $block6['card4Description']->getData() ?></p>
                            </div>
                            <div class="col-md-12 order-md-last">
                            <?php foreach($block6['card4Block']->getData() as $card4Block): ?>
                                <p><?= $card4Block['card4DetailedDescription']->getData() ?></p>
                            <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach ?>
            </div>
        </div>
    </div>
</div>