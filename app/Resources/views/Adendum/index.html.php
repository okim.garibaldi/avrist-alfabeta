<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

$this->extend('layout_avrist_new.html.php');?>

<div class="cover cover-sm cover-responsive bg-avram cover-general">
    <div class="cover-responsive-img">
        <div class="cover-responsive-overlay"></div>
    </div>
    <div class="container position-relative">
        <img src="/template_avrist/assets/img/pattern/ic_ornamen_yellow.png" class="pattern-purple" alt="Ornamen Purple">
        <!-- <img src="/template_avrist/assets/img/pattern/ic_ornament_triangle_yellow.png" class="pattern-triangle-yellow" alt="Ornamen Triangle Yellow"> -->
        <img src="/template_avrist/assets/img/pattern/ic_ornament_circle_pink.png" class="pattern-pink" alt="Ornamen Circle Pink">
        <nav aria-label="breadcrumb" class="general-breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active"><a href="#"><?= $adendum->getTitle() ?></a></li>
            </ol>
        </nav>
        <div class="cover-content">
            <h3 class="cover-title text-white animated fadeInUp delayp1"><?= $adendum->getTitle() ?></h3>
            <p class="cover-text"><?= $adendum->getDescription() ?></p>
        </div>
    </div>
</div>

<div class="py-main">
    <div class="container">
    <?php foreach($years as $year => $files): ?>
        <div class="heading mb-3">
            <h2 class="font-weight-bold" style="color: #b2bbc3;"><?= $year ?></h2>
        </div>
        <div class="row">
        <?php foreach($files as $file): ?>
            <div class="col-6 col-md-3">
                <a href="<?= $file['file']->getFullPath() ?>" download class="card card-list">
                    <div class="card-body">
                        <h3 class="text-truncate"><?= $file['name'] ?> <i class="fal fa-angle-right"></i></h3>
                        <p><img src="/template_avrist/assets/img/tentang/pdf.png" class="extension-file">Pdf Files | <?= number_format($file['file']->getFileSize() / 1024, 2) . ' KB' ?></p>
                        <p class="download">Download</p>
                    </div>
                </a>
            </div>
        <?php endforeach; ?>
        </div>
    <?php endforeach; ?>
    </div>
</div>