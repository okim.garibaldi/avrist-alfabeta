<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

use Pimcore\Model\Document;
use Pimcore\Model\Document\Page;

$this->extend('layout_avrist_new.html.php');?>

<section class="cover cover-landing-page">
    <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/pattern/ic_ornamen_yellow.png')?>" class="pattern-yellow" alt="Pattern Yellow">
    <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/pattern/ic_ornamen_yellow.png')?>" class="pattern-yellow-1" alt="Pattern Yellow">
    <div class="container content-center">
        <div class="row w-100">
            <div class="col-md-6">
                <h2><?= $this->product->getName() ?></h2>
                <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/icon/ic_age_white.png')?>" class="img-fluid icon-small"><p>17 - 24 Tahun</p>
                <!--                <p></p>-->
            </div>
            <div class="col-md-6 text-md-right">
                <a href="#tanyaAhli" class="smooth-scroll btn btn-transparent mr-2">Tanya Ahlinya</a>
                <a href="#getQuote" class="smooth-scroll btn btn-transparent mr-2">Solution Finder</a>
            </div>
        </div>
    </div>
</section>

<section class="py-main section-insurance-tab">
    <div class="container">
        <ul class="nav nav-tabs insurance-tab" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link content-center active" id="tentang-tab" data-toggle="tab" href="#tentang" role="tab" aria-controls="tentang" aria-selected="true">Tentang</a>
            </li>
            <li class="nav-item">
                <a class="nav-link content-center" id="manfaat-tab" data-toggle="tab" href="#manfaat" role="tab" aria-controls="manfaat" aria-selected="false">Manfaat</a>
            </li>
            <li class="nav-item">
                <a class="nav-link content-center" id="syarat-tab" data-toggle="tab" href="#syarat" role="tab" aria-controls="syarat" aria-selected="false">Syarat & Prosedur</a>
            </li>
        </ul>
    </div>
</section>

<!-- <section class="py-main section-insurance-content">
  <div class="content" id="testingContent"></div>
</section> -->

<div class="tab-content" id="myTabContent">
    <div class="tab-pane fade show active" id="tentang" role="tabpanel" aria-labelledby="tentang-tab">
        <section class="py-main section-investment-tentang">
            <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/pattern/ornamen_waves_layer3_grey.png')?>" class="pattern-wave-grey" alt="Pattern Gray">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 content-center">
                        <div class="heading">
                            <h2>Introduksi</h2>
                            <p class="text-truncate-paragraf">
                                <?= str_replace(['<p>', '</p>'], ['<br>', '</br>'], $this->product->getSummary())?>
                            </p>
                            <a class="text-primary" style="cursor: pointer;" id="modalBtn" data-toggle="modal" data-target="#modalReadMore">Read More</a>
                        </div>
                    </div>
                    <div class="col-md-6 text-center">
                        <img src="<?= $this->product->getImageDetail() ?>" class="img-fluid vp-fadeinleft" alt="Why Us">
                    </div>
                </div>
            </div>
        </section>

        <section class="py-main section-investment-tentang-2">
            <div class="container">
                <div class="heading">
                    <h2>Keunggulan</h2>
                </div>
                <div class="content mt-5">
                    <div class="row">
                        <?php foreach ($this->benefits as $benefit){?>
                            <div class="col-md-4">
                                <div class="content">
                                    <h4><?= $benefit->getTitle() ?></h4>
                                    <p><?= $benefit->getDescription() ?></p>
                                </div>
                            </div>
                        <?php } ?>
                        <div class="col-md-12 mt-4">
                            <small>* Perhitungan harga unit dilakukan setiap hari kerja berdasarkan nilai dana investasi dibagi jumlah semua unit yang dibentuk dari suatu dana investasi</small>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <section class="py-main section-investment-tentang-4" id="getQuote">
            <div class="container">
                <div class="heading">
                    <h2>Detail Premi</h2>
                    <p>Cras quis nulla commodo, aliquam lectus sed, blandit augue. Cras ullamcorper bibendum bibendum duis tincidunt</p>
                    <a href="#manfaat" class="link-arrow">
                        LIHAT DETAIL MANFAAT <i class="fas fa-chevron-right"></i>
                    </a>
                </div>

                <div class="content mt-5">
                    <div class="card card-premi p-box">
                        <div class="row">
                            <div class="col-md-6">
                                USIA MASUK
                                <div class="slider-wrapper">
                                    <div id='volume' class='slider'>
                                        <output class='slider-output'>25thn</output>
                                        <div class='slider-track'>
                                            <div class="slider-thumb"></div>
                                            <div class='slider-level'></div>
                                        </div>
                                        <input class='slider-input' type='range' value='25' min='0' max='100' />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                JENIS KELAMIN
                                <div class="row row-1 mt-4">
                                    <div class="col-6 col-sm-6 col-md-4">
                                        <a href="#" class="btn btn-gender btn-block active" id="genderLaki">Laki Laki</a>
                                    </div>
                                    <div class="col-6 col-sm-6 col-md-4">
                                        <a href="#" class="btn btn-gender btn-block" id="genderPerempuan">Perempuan</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="validationCustom04">PEKERJAAN</label>
                                    <div class="form-group">
                                        <select class="custom-select" id="jobSelect" required>
                                            <option value="">Choose...</option>
                                            <option value="1">Agen Asuransi</option>
                                            <option value="2">Akuntan</option>
                                            <option value="3">Dokter</option>
                                            <option value="4">Eksekutif Perusahaan</option>
                                            <option value="5">Fotografer</option>
                                            <option value="6">Ibu Rumah Tangga</option>
                                            <option value="7">Koki</option>
                                            <option value="8">Manager/Supervisor</option>
                                            <option value="9">Pelajar/Mahasiswa</option>
                                            <option value="10">Pembawa Acara TV</option>
                                            <option value="11">Pemiliik perusahaan</option>
                                            <option value="12">Pendeta</option>
                                            <option value="13">Penerjemah</option>
                                            <option value="14">Perancang Busana</option>
                                            <option value="15">Pialang Saham</option>
                                            <option value="16">Programmer Komputer</option>
                                            <option value="17">Resepsionis</option>
                                            <option value="18">Sekretaris</option>
                                            <option value="19">Underwriter</option>
                                            <option value="20">Agen Properti</option>
                                            <option value="21">Anggota Parlemen</option>
                                            <option value="22">Arsitek</option>
                                            <option value="23">Event Organizer</option>
                                            <option value="24">Guru/Dosen</option>
                                            <option value="25">Instruktur Gym</option>
                                            <option value="26">MLM</option>
                                            <option value="27">Pelayan</option>
                                            <option value="28">Penata Rambut</option>
                                            <option value="29">Pengacara</option>
                                            <option value="30">Profesor</option>
                                            <option value="31">Teller</option>
                                            <option value="32">Tukang Jahit</option>
                                            <option value="33">Karyawan Percetakan</option>
                                            <option value="34">Messenger</option>
                                            <option value="35">Office Boy</option>
                                            <option value="36">Pelatih Olahraga</option>
                                            <option value="37">Petani</option>
                                            <option value="38">Travel Guide</option>
                                            <option value="39">Tukang Kayu</option>
                                            <option value="40">Tukang Pos</option>
                                            <option value="41">Buruh</option>
                                            <option value="42">Operator Alat</option>
                                            <option value="43">Pembantu Rumah Tangga</option>
                                            <option value="44">Sipir Penjara</option>
                                            <option value="45">Yatchman</option>
                                            <option value="46">Atlet</option>
                                            <option value="47">Nelayan</option>
                                            <option value="48">Pemburu</option>
                                            <option value="49">Aktor/Aktris</option>
                                            <option value="50">Reporter/Jurnalis</option>
                                            <option value="51">Penulis</option>
                                            <option value="52">Kolektor</option>
                                            <option value="53">Pembuat Film</option>
                                            <option value="54">Insinyur/Ahli</option>
                                            <option value="55">Marketing</option>
                                            <option value="56">Polisi</option>
                                            <option value="57">Supir</option>
                                            <option value="58">Salesman</option>
                                            <option value="59">Penunggang kuda balap</option>
                                            <option value="60">Perawat</option>
                                            <option value="61">Penyelam</option>
                                            <option value="62">Pelatih hewan (bukan binatang buas)</option>
                                            <option value="63">Keamanan/Security</option>
                                            <option value="64">Penjaga/Pekerja di hutan</option>
                                            <option value="65">Pekerja Lepas Pantai</option>
                                            <option value="66">Awak Transportasi</option>
                                            <option value="67">Tentara</option>
                                            <option value="68">Pemasang iklan (di luar ruangan/lapangan)</option>
                                            <option value="69">Pekerja di lab kimia (non-nuklir)</option>
                                            <option value="70">Pekerja bangunan (kecuali pekerja pemanjat menara & pekerjan stuktural)</option>
                                            <option value="71">Karyawan di fasilitas nuklir</option>
                                            <option value="72">Pekerja dalam terowongan/gorong-gorong</option>
                                            <option value="73">Pekerja Pengeboran</option>
                                            <option value="74">Pekerja yang berhubungan dengan asbes</option>
                                            <option value="75">Pemadam kebakaran</option>
                                        </select>
                                        <div class="invalid-feedback">
                                            Please choose job
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- question agen asuransi -->
                            <div class="col-sm-6 job-question" id="job1">
                                <div class="form-group">
                                    <label for="validationCustom04">Jenis Asuransi</label>
                                    <div class="form-group">
                                        <select class="custom-select" id="validationCustom04" required>
                                            <option value="">Choose...</option>
                                            <option value="1">Avrist</option>
                                            <option value="2">Non Avrist</option>
                                        </select>
                                        <div class="invalid-feedback">
                                            Please choose correctly
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- question dokter -->
                            <div class="col-sm-6 job-question" id="job3">
                                <div class="form-group">
                                    <label for="validationCustom04">Jenis Pekerjaan</label>
                                    <div class="form-group">
                                        <select class="custom-select" id="validationCustom04" required>
                                            <option value="">Choose...</option>
                                            <option value="1">Hanya melakukan konsultasi</option>
                                            <option value="2">Dokter Bedah</option>
                                            <option value="3">Dokter Gigi</option>
                                        </select>
                                        <div class="invalid-feedback">
                                            Please choose correctly
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- question fotografer -->
                            <div class="col-sm-6 job-question" id="job5">
                                <div class="form-group">
                                    <label for="validationCustom04">Jenis Pekerjaan</label>
                                    <div class="form-group">
                                        <select class="custom-select" id="validationCustom04" required>
                                            <option value="">Choose...</option>
                                            <option value="1">Luar ruangan (tidak dari udara)</option>
                                            <option value="2">Dalam ruangan</option>
                                            <option value="3">Perang/dari udara</option>
                                        </select>
                                        <div class="invalid-feedback">
                                            Please choose correctly
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- question Koki -->
                            <div class="col-sm-6 job-question" id="job7">
                                <div class="form-group">
                                    <label for="validationCustom04">Bintang</label>
                                    <div class="form-group">
                                        <select class="custom-select" id="validationCustom04" required>
                                            <option value="">Choose...</option>
                                            <option value="1">4-5</option>
                                            <option value="2">0-3</option>
                                        </select>
                                        <div class="invalid-feedback">
                                            Please choose correctly
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- question pelayan -->
                            <div class="col-sm-6 job-question" id="job27">
                                <div class="form-group">
                                    <label for="validationCustom04">Jenis Restoran</label>
                                    <div class="form-group">
                                        <select class="custom-select" id="validationCustom04" required>
                                            <option value="">Choose...</option>
                                            <option value="1">Restoran/Cafe</option>
                                            <option value="2">Bar</option>
                                        </select>
                                        <div class="invalid-feedback">
                                            Please choose correctly
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- question buruh -->
                            <div class="col-sm-6 job-question" id="job41">
                                <div class="form-group">
                                    <label for="validationCustom04">Jenis Pekerjaan</label>
                                    <div class="form-group">
                                        <select class="custom-select" id="validationCustom04" required>
                                            <option value="">Choose...</option>
                                            <option value="1">Angkut</option>
                                            <option value="2">Kargo</option>
                                            <option value="3">Pabrik</option>
                                        </select>
                                        <div class="invalid-feedback">
                                            Please choose correctly
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- question Operator Alat -->
                            <div class="col-sm-6 job-question" id="job42">
                                <div class="form-group">
                                    <label for="validationCustom04">Jenis Alat</label>
                                    <div class="form-group">
                                        <select class="custom-select" id="validationCustom04" required>
                                            <option value="">Choose...</option>
                                            <option value="1">Forklift</option>
                                            <option value="2">Alat Berat</option>
                                        </select>
                                        <div class="invalid-feedback">
                                            Please choose correctly
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- question PRT -->
                            <div class="col-sm-6 job-question" id="job43">
                                <div class="form-group">
                                    <label for="validationCustom04">Lokasi Bekerja</label>
                                    <div class="form-group">
                                        <select class="custom-select" id="validationCustom04" required>
                                            <option value="">Choose...</option>
                                            <option value="1">Luar Negri</option>
                                            <option value="2">Dalam Negri</option>
                                        </select>
                                        <div class="invalid-feedback">
                                            Please choose correctly
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- question altet -->
                            <div class="col-sm-6 job-question" id="job46">
                                <div class="form-group">
                                    <label for="validationCustom04">Jenis Pekerjaan</label>
                                    <div class="form-group">
                                        <select class="custom-select" id="validationCustom04" required>
                                            <option value="">Choose...</option>
                                            <option value="1">Amatir, non-bela diri</option>
                                            <option value="2">Profesinal, non-bela diri</option>
                                            <option value="3">Bela diri</option>
                                        </select>
                                        <div class="invalid-feedback">
                                            Please choose correctly
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- question nelayan -->
                            <div class="col-sm-6 job-question" id="job47">
                                <div class="form-group">
                                    <label for="validationCustom04">Apakah kembali ke darat setiap hari?</label>
                                    <div class="form-group">
                                        <select class="custom-select" id="validationCustom04" required>
                                            <option value="">Choose...</option>
                                            <option value="1">Ya</option>
                                            <option value="2">Tidak</option>
                                        </select>
                                        <div class="invalid-feedback">
                                            Please choose correctly
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- question aktor -->
                            <div class="col-sm-6 job-question" id="job49">
                                <div class="form-group">
                                    <label for="validationCustom04">Jenis Pekerjaan</label>
                                    <div class="form-group">
                                        <select class="custom-select" id="validationCustom04" required>
                                            <option value="">Choose...</option>
                                            <option value="1">Laga</option>
                                            <option value="2">Non-Laga</option>
                                            <option value="2">Stuntman</option>
                                        </select>
                                        <div class="invalid-feedback">
                                            Please choose correctly
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- question reporter -->
                            <div class="col-sm-6 job-question" id="job50">
                                <div class="form-group">
                                    <label for="validationCustom04">Jenis liputan</label>
                                    <div class="form-group">
                                        <select class="custom-select" id="validationCustom04" required>
                                            <option value="">Choose...</option>
                                            <option value="1">Perang</option>
                                            <option value="2">Non-perang</option>
                                        </select>
                                        <div class="invalid-feedback">
                                            Please choose correctly
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- question kolektor -->
                            <div class="col-sm-6 job-question" id="job52">
                                <div class="form-group">
                                    <label for="validationCustom04">Jenis Pekerjaan</label>
                                    <div class="form-group">
                                        <select class="custom-select" id="validationCustom04" required>
                                            <option value="">Choose...</option>
                                            <option value="1">Bank</option>
                                            <option value="2">Non-Bank</option>
                                        </select>
                                        <div class="invalid-feedback">
                                            Please choose correctly
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- question insyinur -->
                            <div class="col-sm-6 job-question" id="job54">
                                <div class="form-group">
                                    <label for="validationCustom04">Jenis Keahlian</label>
                                    <div class="form-group">
                                        <select class="custom-select" id="job54Select" required>
                                            <option value="">Choose...</option>
                                            <option value="1">Bangunan</option>
                                            <option value="2">Listrik</option>
                                        </select>
                                        <div class="invalid-feedback">
                                            Please choose correctly
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- question insyinur 2-->
                            <div class="col-sm-6 job-question" id="job54-quest2">
                                <div class="form-group">
                                    <label for="validationCustom04">Tegangan</label>
                                    <div class="form-group">
                                        <select class="custom-select" id="job54" required>
                                            <option value="">Choose...</option>
                                            <option value="1">Rendah (&#60;1000Vms, &#60;1500V)</option>
                                            <option value="2">Tinggi (&#62;1000Vms, &#62;1500V)</option>
                                        </select>
                                        <div class="invalid-feedback">
                                            Please choose correctly
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- question marketing -->
                            <div class="col-sm-6 job-question" id="job55">
                                <div class="form-group">
                                    <label for="validationCustom04">Apakah Anda Mengendarai Mobil?</label>
                                    <div class="form-group">
                                        <select class="custom-select" id="validationCustom04" required>
                                            <option value="">Choose...</option>
                                            <option value="1">Ya</option>
                                            <option value="2">Tidak</option>
                                        </select>
                                        <div class="invalid-feedback">
                                            Please choose correctly
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- question polisi -->
                            <div class="col-sm-6 job-question" id="job56">
                                <div class="form-group">
                                    <label for="validationCustom04">Jenis Pekerjaan</label>
                                    <div class="form-group">
                                        <select class="custom-select" id="validationCustom04" required>
                                            <option value="">Choose...</option>
                                            <option value="1">Pekerjaan Admin, tidak bersenjata</option>
                                            <option value="2">Bukan Admin, bersenjata</option>
                                        </select>
                                        <div class="invalid-feedback">
                                            Please choose correctly
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- question supir -->
                            <div class="col-sm-6 job-question" id="job57">
                                <div class="form-group">
                                    <label for="validationCustom04">Jenis Kendaraan</label>
                                    <div class="form-group">
                                        <select class="custom-select" id="validationCustom04" required>
                                            <option value="">Choose...</option>
                                            <option value="1">Roda 4</option>
                                            <option value="2">Roda 6</option>
                                        </select>
                                        <div class="invalid-feedback">
                                            Please choose correctly
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- question salesman -->
                            <div class="col-sm-6 job-question" id="job58">
                                <div class="form-group">
                                    <label for="validationCustom04">Apakah Anda Mengendarai Mobil?</label>
                                    <div class="form-group">
                                        <select class="custom-select" id="validationCustom04" required>
                                            <option value="">Choose...</option>
                                            <option value="1">Ya</option>
                                            <option value="2">Tidak</option>
                                        </select>
                                        <div class="invalid-feedback">
                                            Please choose correctly
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- question perawat -->
                            <div class="col-sm-6 job-question" id="job60">
                                <div class="form-group">
                                    <label for="validationCustom04">Jenis Pekerjaan</label>
                                    <div class="form-group">
                                        <select class="custom-select" id="validationCustom04" required>
                                            <option value="">Choose...</option>
                                            <option value="1">Psikiatri</option>
                                            <option value="2">Bukan bagian Psikiatri</option>
                                        </select>
                                        <div class="invalid-feedback">
                                            Please choose correctly
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- question penyelam -->
                            <div class="col-sm-6 job-question" id="job61">
                                <div class="form-group">
                                    <label for="validationCustom04">Jenis Pekerjaan</label>
                                    <div class="form-group">
                                        <select class="custom-select" id="validationCustom04" required>
                                            <option value="">Choose...</option>
                                            <option value="1">Amatir (Maks. 45ft/14m)</option>
                                            <option value="2">Profesional</option>
                                        </select>
                                        <div class="invalid-feedback">
                                            Please choose correctly
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- question keamanan -->
                            <div class="col-sm-6 job-question" id="job63">
                                <div class="form-group">
                                    <label for="validationCustom04">Lokasi Pekerjaan</label>
                                    <div class="form-group">
                                        <select class="custom-select" id="validationCustom04" required>
                                            <option value="">Choose...</option>
                                            <option value="1">Non-bank (tidak bersenjata)</option>
                                            <option value="2">Bank (bersenjata)</option>
                                        </select>
                                        <div class="invalid-feedback">
                                            Please choose correctly
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- question pekerja lepas pantai -->
                            <div class="col-sm-6 job-question" id="job65">
                                <div class="form-group">
                                    <label for="validationCustom04">Jenis Pekerjaan</label>
                                    <div class="form-group">
                                        <select class="custom-select" id="validationCustom04" required>
                                            <option value="">Choose...</option>
                                            <option value="1">Buruh/admin/operator, tidak berhubungan dengan bahan peledak</option>
                                            <option value="2">Lainnya</option>
                                        </select>
                                        <div class="invalid-feedback">
                                            Please choose correctly
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- question awak transportasi -->
                            <div class="col-sm-6 job-question" id="job66">
                                <div class="form-group">
                                    <label for="validationCustom04">Jenis Transportasi</label>
                                    <div class="form-group">
                                        <select class="custom-select" id="job66Select" required>
                                            <option value="">Choose...</option>
                                            <option value="1">Kapal Laut</option>
                                            <option value="2">Kereta Api</option>
                                            <option value="3">Lainnya</option>
                                        </select>
                                        <div class="invalid-feedback">
                                            Please choose correctly
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- question awak transportasi -->
                            <div class="col-sm-6 job-question" id="job66-quest2">
                                <div class="form-group">
                                    <label for="validationCustom04">Jenis Kapal</label>
                                    <div class="form-group">
                                        <select class="custom-select" id="job66-quest2-select" required>
                                            <option value="">Choose...</option>
                                            <option value="1">Kapal Minyak</option>
                                            <option value="2">Kereta Non-Minyak</option>
                                        </select>
                                        <div class="invalid-feedback">
                                            Please choose correctly
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- question awak transportasi -->
                            <div class="col-sm-6 job-question" id="job66-quest3">
                                <div class="form-group">
                                    <label for="validationCustom04">Jabatan</label>
                                    <div class="form-group">
                                        <select class="custom-select" id="job66-quest3" required>
                                            <option value="">Choose...</option>
                                            <option value="1">Kapten</option>
                                            <option value="2">Kepala Karyawan</option>
                                            <option value="3">Kepala Pelayan</option>
                                            <option value="4">Insinyur</option>
                                            <option value="5">Mualim</option>
                                            <option value="6">Musisi</option>
                                            <option value="7">Dokter</option>
                                            <option value="8">Kepala Keuangan</option>
                                            <option value="9">Juru Mudi</option>
                                            <option value="10">Operator Radio</option>
                                            <option value="11">Kepala Kelasi</option>
                                            <option value="12">Tukang Kayu</option>
                                            <option value="13">Juru Masak</option>
                                            <option value="14">Lainnya</option>
                                        </select>
                                        <div class="invalid-feedback">
                                            Please choose correctly
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- question awak transportasi -->
                            <div class="col-sm-6 job-question" id="job67">
                                <div class="form-group">
                                    <label for="validationCustom04">Jenis Pekerjaan</label>
                                    <div class="form-group">
                                        <select class="custom-select" id="job67" required>
                                            <option value="">Choose...</option>
                                            <option value="1">Bagian media (bekerja di kantor, tidak bersenjata)</option>
                                            <option value="2">Lainnya</option>
                                        </select>
                                        <div class="invalid-feedback">
                                            Please choose correctly
                                        </div>
                                    </div>
                                </div>
                            </div>



                            <div class="col-md-12 text-center mt-4">
                                <a href="#" class="btn btn-secondary" id="hitungBtnStyle3">Hitung</a>
                            </div>
                        </div>
                    </div>
                    <div class="card card-premi card-premi-hitung p-box" id="boxCalcPremiStyle3">
                        <div class="col-md-12 text-center mb-4 mt-4">
                            <p>PREMI PER TAHUN</p>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="card card-premi card-premi-hasil">
                                    <p class="mb-1">Plan A</p>
                                    <h5>Rp880.000</h5>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="card card-premi card-premi-hasil">
                                    <p class="mb-1">Plan B</p>
                                    <h5>Rp2.289.100</h5>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="card card-premi card-premi-hasil">
                                    <p class="mb-1">Plan C</p>
                                    <h5>Rp2.958.200</h5>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="card card-premi card-premi-hasil">
                                    <p class="mb-1">Plan D</p>
                                    <h5>Rp5.000.000</h5>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="card card-premi card-premi-hasil">
                                    <p class="mb-1">Plan E</p>
                                    <h5>Rp6.438.000</h5>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="card card-premi card-premi-hasil">
                                    <p class="mb-1">Plan F</p>
                                    <h5>Rp8.430.000</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="py-main section-investment-tentang-5">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 position-relative">
                        <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/pattern/ic_ornamen_yellow.png')?>" class="pattern-yellow" alt="Ornamen Yellow">
                        <div class="heading">
                            <h2>Selalu bersama Anda</h2>
                            <p class="subtitle">Kami selalu melindungi Anda. Belum menemukan jawaban? Cari berbagai topik yang sering ditanyakan di  Frequently Asked Questions</p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div id="accordionFAQ" class="accordion accordion-faq vp-fadeinup delayp2">
                            <div class="accordion-item">
                                <div class="accordion-header" id="headingOne" data-toggle="collapse" data-target="#collapseFAQ1" aria-expanded="true" aria-controls="collapseFAQ1">
                                    <h5>Apa beda asuransi jiwa dan kesehatan?
                                        <span class="close-panel"></span>
                                    </h5>
                                </div>
                                <div id="collapseFAQ1" class="accordion-body collapse show" aria-labelledby="headingOne" data-parent="#accordionFAQ">
                                    <p class="pl-0">Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris a nisi sed velit pulvinar gravida. Vivamus blandit cursus enim, a pulvinar dui hendrerit at. Class aptent taciti sociosqu ad litora torquent per conubia nostra. </p>
                                </div>
                            </div>
                            <div class="accordion-item">
                                <div class="accordion-header" id="headingFAQ2" data-toggle="collapse" data-target="#collapseFAQ2" aria-expanded="false" aria-controls="collapseFAQ2">
                                    <h5>Bagaimana cara mendapatkan rekomendasi produk yang paling cocok bagi saya?
                                        <span class="close-panel"></span>
                                    </h5>
                                </div>
                                <div id="collapseFAQ2" class="accordion-body collapse" aria-labelledby="headingFAQ2" data-parent="#accordionFAQ">
                                    <p class="pl-0">Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris a nisi sed velit pulvinar gravida. Vivamus blandit cursus enim, a pulvinar dui hendrerit at. Class aptent taciti sociosqu ad litora torquent per conubia nostra.  </p>
                                </div>
                            </div>
                            <div class="accordion-item">
                                <div class="accordion-header" id="headingFAQ3" data-toggle="collapse" data-target="#collapseFAQ3" aria-expanded="false" aria-controls="collapseFAQ3">
                                    <h5>Bagaimana menanggulangi resiko investasi?
                                        <span class="close-panel"></span>
                                    </h5>
                                </div>
                                <div id="collapseFAQ3" class="accordion-body collapse" aria-labelledby="headingFAQ3" data-parent="#accordionFAQ">
                                    <p class="pl-0">Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris a nisi sed velit pulvinar gravida. Vivamus blandit cursus enim, a pulvinar dui hendrerit at. Class aptent taciti sociosqu ad litora torquent per conubia nostra.  </p>
                                </div>
                            </div>
                            <div class="accordion-item">
                                <div class="accordion-header" id="headingFAQ4" data-toggle="collapse" data-target="#collapseFAQ4" aria-expanded="false" aria-controls="collapseFAQ4">
                                    <h5>Apa bedanya asuransi jiwa dan kesehatan?
                                        <span class="close-panel"></span>
                                    </h5>
                                </div>
                                <div id="collapseFAQ4" class="accordion-body collapse" aria-labelledby="headingFAQ4" data-parent="#accordionFAQ">
                                    <p class="pl-0">Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris a nisi sed velit pulvinar gravida. Vivamus blandit cursus enim, a pulvinar dui hendrerit at. Class aptent taciti sociosqu ad litora torquent per conubia nostra.  </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="py-main section-investment-tentang-6" id="tanyaAhli">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="heading">
                            <h2>Tanya pada ahlinya</h2>
                            <p class="subtitle">Bingung menentukan pilihan? Atau ingin mengetahui detail solusi yang kamu inginkan? Dapatkan saran terbaik via e-mail, atau Kunjungi Cabang Terdekat</p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <form class="needs-validation" novalidate>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="">NAMA LENGKAP</label>
                                        <input type="text" class="form-control" id="name-ask" placeholder="" value="" required>
                                        <div class="invalid-feedback">
                                            Please enter first name
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="">EMAIL ADDRESS</label>
                                        <input type="text" class="form-control" id="email-ask" placeholder="" value="" required>
                                        <div class="invalid-feedback">
                                            Please enter last name
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="">MESSAGE</label>
                                        <textarea class="form-control" id="message-ask" rows="5" style="resize: none;"></textarea>
                                        <div class="invalid-feedback">
                                            Please enter last name
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <button class="btn btn-primary float-right btn-submit-ask" href="javascript:;">Submit form</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="tab-pane fade" id="manfaat" role="tabpanel" aria-labelledby="manfaat-tab">
        <section class="py-main section-investment-tentang-2">
            <div class="container">
                <div class="heading">
                    <h2>Manfaat Yang Didapat</h2>
                    <p>Duis aute irure dolor in reprehenderit in voluptate velit esse
                        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                </div>
                <div class="content mt-5">
                    <div class="row">
                        <?php foreach ($this->benefits as $benefit){?>
                            <div class="col-md-4">
                                <div class="content">
                                    <h4><?= $benefit->getTitle() ?></h4>
                                    <p><?= $benefit->getDescription() ?></p>
                                </div>
                            </div>
                        <?php } ?>

                        <div class="col-md-12 mt-4">
                            <a href="/#getQuote" class="link-arrow">
                                LIHAT ESTIMASI PREMI
                                <i class="fa fa-chevron-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="bg-light py-main section-life-insurance-5" style="background: url(/template_avrist/assets/img/common/img_banner_solution.jpg) no-repeat center; background-size: cover;">
            <div class="container">
                <div class="row">
                    <div class="col-md-7">
                        <h2>Ingin tau lebih lanjut?<a href="<?= $this->product->getBrosur() ?>">Download Brosur</a></h2>
                    </div>
                </div>
            </div>
        </section>

        <section class="py-main section-investment-tentang-2">
            <div class="container">
                <div class="heading">
                    <h2>Asuransi Rider</h2>
                    <p>Duis aute irure dolor in reprehenderit in voluptate velit esse
                        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                </div>
                <div class="content mt-5">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="content">
                                <h4>Accidental Death and Dismenberment (ADD)</h4>
                                <p>Ut maximus sollicitudin odio vel ullamcorper. Maecenas congue est eget magna blandit, vitae fringilla odio pulvinar. Ut maximus sollicitudin odio vel ullamcorper. Maecenas congue est eget magna blandit, vitae fringilla odio pulvinar.</p>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="content">
                                <h4>WP (Waiver of Premium)</h4>
                                <p>Ut maximus sollicitudin odio vel ullamcorper. Maecenas congue est eget magna blandit, vitae fringilla odio pulvinar. Ut maximus sollicitudin odio vel ullamcorper. Maecenas congue est eget magna blandit, vitae fringilla odio pulvinar.</p>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="content">
                                <h4>Payor of Benefit (PB)</h4>
                                <p>Ut maximus sollicitudin odio vel ullamcorper. Maecenas congue est eget magna blandit, vitae fringilla odio pulvinar. Ut maximus sollicitudin odio vel ullamcorper. Maecenas congue est eget magna blandit, vitae fringilla odio pulvinar.</p>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="content">
                                <h4>Hospital & Surgical (H&S)</h4>
                                <p>Ut maximus sollicitudin odio vel ullamcorper. Maecenas congue est eget magna blandit, vitae fringilla odio pulvinar. Ut maximus sollicitudin odio vel ullamcorper. Maecenas congue est eget magna blandit, vitae fringilla odio pulvinar.</p>
                            </div>
                        </div>

                        <div class="col-md-12 mt-4">
                            <a href="#" class="link-arrow">
                                Tabel Perhitungan Pertanggungjawaban
                                <i class="fa fa-chevron-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="tab-pane fade" id="syarat" role="tabpanel" aria-labelledby="syarat-tab">
        <section class="py-main section-investment-tentang-2">
            <div class="container">
                <div class="heading">
                    <h2>Syarat & Ketentuan</h2>
                    <p><?= $this->product->getTermsAndProcedures() ?></p>
                </div>
                <div class="content mt-5">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="content">
                                <h4>Age Range</h4>
                                <p>Apply before 50 years old</p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="content">
                                <h4>Indonesia/Foreigner</h4>
                                <p>For Indonesian, you must have a valid KTP or if you are a foreigner, you need too have KITAS as an identity</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

<div class="modal fade modal-checkout" id="modalReadMore" tabindex="-1" role="dialog" aria-labelledby="modalCheckOut" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body p-box">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="text-primary">&times;</span>
                </button>
                <div class="content">
                    <h2 class="mb-3">Introduksi</h2>
                    <p><?= $this->product->getSummary() ?></p>
                </div>
            </div>
        </div>
    </div>
</div>

<script>

    var volumeSlider = document.getElementById('volume');
    var sliders = [volumeSlider];


    //******************************START SLIDER LOGIC******************************
    function Slider(slider) {
        this.slider = slider;
        slider.addEventListener('input', function() {
            this.updateSliderOutput();
            this.updateSliderLevel();
        }.bind(this), false);

        this.level = function() {
            var level = this.slider.querySelector('.slider-input');
            return level.value;
        }

        this.levelString = function() {
            return parseInt(this.level());
        }

        this.remaining = function() {
            return 99.5 - this.level();
        }

        this.remainingString = function() {
            return parseInt(this.remaining());
        }

        this.updateSliderOutput = function() {
            var output = this.slider.querySelector('.slider-output');
            var remaining = this.slider.querySelector('.slider-remaining');
            var thumb = this.slider.querySelector('.slider-thumb');
            output.value = this.levelString() + 'thn';
            output.style.left = this.levelString() + '%';
            thumb.style.left = this.levelString() + '%';
            if (remaining) {
                remaining.style.width = this.remainingString() + '%';
            }
        }

        this.updateSlider = function(num) {
            var input = this.slider.querySelector('.slider-input');
            input.value = num;
        }

        this.updateSliderLevel =function() {
            var level = this.slider.querySelector('.slider-level');
            level.style.width = this.levelString() + '%';
        }
    }

    sliders.forEach(function(slider) {
        new Slider(slider);
    });
    //********************************END SLIDER LOGIC*****************************************


</script>
