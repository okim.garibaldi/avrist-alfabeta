<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

 use Pimcore\Model\Document;
 use Pimcore\Model\Document\Page;
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
  <head>
    <?= $this->template("include/head2.html.php"); ?>
    <?= $this->template("include/tracking-script/head.html.php"); ?>
    <?php if($overflow == True): ?>
    <style>
      .owl-stage-outer {overflow: hidden !important;}
    </style>
    <?php endif; ?>
  </head>
  <body>
    <?= $this->template("include/tracking-script/body.html.php"); ?>

    <div class="loader-wrapper loader-light">
      <div class="loader"></div>
    </div> 

    <?= $this->template("include/navbar3.html.php"); ?>

    <?php $this->slots()->output('_content') ?>

    <?= $this->template("include/footer2.html.php"); ?>
    <?= $this->template("include/script2.html.php"); ?>
    
    <?php if($googleMap == TRUE): ?>
    <?= $this->template("include/googlemap.html.php"); ?>
    <?php endif; ?>
  </body>
</html>
