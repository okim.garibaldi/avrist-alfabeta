<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

use Pimcore\Model\Document;
use Pimcore\Model\Document\Page;

$this->extend('layout_avrist_new.html.php');?>

<div class="cover cover-sm cover-responsive <?= $bgBanner['bg'] ?> cover-general">
    <div class="cover-responsive-img">
        <div class="cover-responsive-overlay"></div>
    </div>
    <div class="container position-relative">
        <img src="/template_avrist/assets/img/pattern/ic_ornamen_yellow.png" class="pattern-purple" alt="Ornamen Purple">
        <!-- <img src="/template_avrist/assets/img/pattern/ic_ornament_triangle_yellow.png" class="pattern-triangle-yellow" alt="Ornamen Triangle Yellow"> -->
        <img src="/template_avrist/assets/img/pattern/ic_ornament_circle_pink.png" class="pattern-pink" alt="Ornamen Circle Pink">
        <nav aria-label="breadcrumb" class="general-breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active"><a href="#">FAQ</a></li>
            </ol>
        </nav>
        <div class="cover-content">
            <h3 class="cover-title text-white animated fadeInUp delayp1">FAQ</h3>
            <div class="col-md-6 pl-0">
                <div class="form-group">
                    <input type="text" class="form-control transparent" id="searchBox" placeholder="Topik atau Pertanyaan" value="" required>
                    <div class="invalid-feedback">
                        Please enter first name
                    </div>
                </div>
            </div>
            <div class="clearfix" id="cari">
                <a href="#" class="btn btn-light w-150">Cari</a>
            </div>
        </div>
    </div>
</div>
<img src="<?= $bgBanner['banner'] ?>" class="img-fluid" alt="Purple Wave">

<section class="py-main section-faq">
    <div class="container">
        <div class="row">
            <div class="col-4">
                <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                <?php $active = " active"; ?>
                <?php $selected = "true"; ?>
                <?php foreach ($faqCategories as $category => $faq): ?>
                    <a class="nav-link<?= $active ?>" id="v-pills-<?= strtolower(str_replace(' ','',$category)) ?>-tab" data-toggle="pill" href="#v-pills-<?= strtolower(str_replace(' ','',$category)) ?>" role="tab" aria-controls="v-pills-<?= strtolower(str_replace(' ','',$category)) ?>" aria-selected="<?= $selected ?>"><?= $category ?></a>
                    <?php if($active == " active" || $selected == "true"): ?>
                        <?php $active = ""; ?>
                        <?php $selected = "false"; ?>
                    <?php endif; ?>
                <?php endforeach; ?>
                </div>
            </div>
            <div class="col-8">
                <div class="tab-content" id="v-pills-tabContent">
                <?php $active = " active"; ?>
                <?php $selected = "true"; ?>
                <?php foreach ($faqCategories as $category => $faq): ?>
                    <div class="tab-pane fade show<?= $active ?>" id="v-pills-<?= strtolower(str_replace(' ','',$category)) ?>" role="tabpanel" aria-labelledby="v-pills-<?= strtolower(str_replace(' ','',$category)) ?>-tab">
                        <div class="heading">
                            <h2><?= $category ?></h2>
                        </div>
                    <?php foreach($faq as $f): ?>
                        <div class="card card-faq">
                            <div class="card-body">
                                <?= $f['title'] ?>
                                <?= $f['faq'] ?>
                            </div>
                        </div>
                    <?php endforeach; ?>
                    </div>
                    <?php if($active == " active" || $selected == "true"): ?>
                        <?php $active = ""; ?>
                        <?php $selected = "false"; ?>
                    <?php endif; ?>
                <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="py-main bg-light">
    <div class="container">
        <div class="row row-3">
            <div class="col-md-6 content-center">
                <div class="heading">
                    <h2 class="mb-4"><?= $faqPage->getContactUs() ?></h2>
                    <p><?= $faqPage->getContactUsDescription() ?></p>
                    <a href="<?= $faqPage->getContactUsPage() ?>" class="btn btn-primary">Contact Us</a>
                </div>
            </div>
            <div class="col-md-6">
                <img src="/template_avrist/assets/img/tentang/ic_ilustrasi_home_mengapaavrist.png" class="img-fluid animated vp-fadeinup delayp1">
            </div>
        </div>
    </div>
</section>