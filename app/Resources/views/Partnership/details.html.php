<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

use Pimcore\Model\Document;
use Pimcore\Model\Document\Page;

$this->extend('layout_avrist_new.html.php');?>    

<div class="cover cover-sm cover-responsive bg-primary cover-general">
    <div class="cover-responsive-img">
        <div class="cover-responsive-overlay"></div>
    </div>
    <div class="container position-relative">
        <img src="/template_avrist/assets/img/pattern/ic_ornamen_yellow.png" class="pattern-purple" alt="Ornamen Purple">
        <!-- <img src="/template_avrist/assets/img/pattern/ic_ornament_triangle_yellow.png" class="pattern-triangle-yellow" alt="Ornamen Triangle Yellow"> -->
        <img src="/template_avrist/assets/img/pattern/ic_ornament_circle_pink.png" class="pattern-pink" alt="Ornamen Circle Pink">
        <nav aria-label="breadcrumb" class="general-breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item"><a href="#">Partnership</a></li>
                <li class="breadcrumb-item active"><a href="#">Detail</a></li>
            </ol>
        </nav>
        <div class="cover-content">
            <h3 class="cover-title text-white animated fadeInUp delayp1"><?= $partnershipDetail->getTitle1() ?></h3>
            <p class="cover-subtitle text-white"><?= $partnershipDetail->getDescription1() ?></p>
        </div>
    </div>
</div>
<img src="/template_avrist/assets/img/pattern/banner_life_solid.png" class="banner-wave img-fluid">

<section class="py-main section-awards-detail">
    <img src="/template_avrist/assets/img/pattern/img_ornamen_waves_grey.png" class="pattern-grey" alt="Pattern Grey">
    <div class="container">
    <?php foreach($partnershipDetail->getBlock1() as $block1): ?>
        <?php if(!(empty($block1['titleBlock']->getData()))): ?>
        <div class="heading mt-5">
            <h2 class="animated vp-fadeinup delayp1"><?= $block1['titleBlock']->getData() ?></h2>
        </div>
        <?php endif; ?>
        <?php if(!(empty($block1['descriptionBlock']->getData()))): ?>
        <p class="animated vp-fadeinup delayp1"><?= $block1['descriptionBlock']->getData() ?></p>
        <?php endif; ?>
        <?php if(!(empty($block1['imageBlock']->getData()))): ?>
        <img src="<?= $block1['imageBlock']->getData() ?>" class="img-fluid my-5 w-100">
        <?php endif; ?>
    <?php endforeach; ?>
    </div>
</section>

<section class="py-main bg-primary section-csr-detail">
    <img src="/template_avrist/assets/img/pattern/ic_ornamen_yellow.png" class="pattern-purple" alt="Ornamen Purple">
    <div class="container position-relative">
        <div class="arrow-carousel">
            <img src="/template_avrist/assets/img/tentang/ic_slider_left.png" class="arrow left" id="arrowLeft" alt="Arrow Left">
            <img src="/template_avrist/assets/img/tentang/ic_slider_right.png" class="arrow right" id="arrowRight" alt="Arrow Left">
        </div>
        <div class="owl-carousel owl-theme owl-dots-solid owl-kebutuhan vp-fadeinup delayp2">
        <?php foreach($partnershipDetail->getBlock2() as $block2): ?>
          <div class="item">
            <div class="card card-news">
              <img src="<?= $block2['image']->getData() ?>" class="img-fluid">
            </div>
          </div>
        <?php endforeach; ?>
        </div>
    </div>
</section>

<section class="py-main section-topik-awards bg-gray">
    <div class="container">
        <div class="heading mb-3">
          <h2>Topik Terkait</h2>
        </div>
        <div class="row">
        <?php foreach($otherPartnershipDetail as $oPD): ?>
            <div class="col-md-4">
                <a href="<?php
                if($siteId == 1){
                  echo "/life-insurance";
                } 
                echo "/".str_replace($partnershipPage->getPath(), '', $oPD->getFullPath()) ?>" class="card card-kebutuhan">
                    <div class="card-img">
                <img src="<?= $oPD->getImage1() ?>" class="img-fluid">
              </div>
              <div class="card-body p-box">
                <h5 class="card-title text-truncate-twoline"><?= $oPD->getTitle1()?></h5>
                <p href="#" class="link">Lihat Detail</p>
              </div>
            </a>
          </div>
        <?php endforeach; ?>
        </div>
    </div>
</section>