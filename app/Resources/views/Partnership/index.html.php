<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

use Pimcore\Model\Document;
use Pimcore\Model\Document\Page;

$this->extend('layout_avrist_new.html.php');?>    

<div class="cover cover-sm cover-responsive bg-primary cover-general">
    <div class="cover-responsive-img">
        <div class="cover-responsive-overlay"></div>
    </div>
    <div class="container position-relative">
        <img src="/template_avrist/assets/img/pattern/ic_ornamen_yellow.png" class="pattern-purple" alt="Ornamen Purple">
        <!-- <img src="/template_avrist/assets/img/pattern/ic_ornament_triangle_yellow.png" class="pattern-triangle-yellow" alt="Ornamen Triangle Yellow"> -->
        <img src="/template_avrist/assets/img/pattern/ic_ornament_circle_pink.png" class="pattern-pink" alt="Ornamen Circle Pink">
        <nav aria-label="breadcrumb" class="general-breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active"><a href="#"><?= $partnershipPage->getTitle1() ?></a></li>
            </ol>
        </nav>
        <div class="cover-content">
            <h3 class="cover-title text-white animated fadeInUp delayp1"><?= $partnershipPage->getTitle1() ?></h3>
            <p class="cover-subtitle text-white animate fadeInUp delayp2"><?= $partnershipPage->getDescription1() ?></p>
        </div>
    </div>
</div>

<section class="py-main section-life-insurance-general">
    <img src="/template_avrist/assets/img/pattern/img_ornamen_waves_grey.png" class="pattern-grey" alt="Pattern Grey">
    <div class="container">
        <div class="heading">
            <h2><?= $partnershipPage->getTitle2() ?></h2>
        </div>
        <div class="row row-3">
            <div class="col-md-6 content-center order-md-last">
                <img src="/template_avrist/assets/img/tentang/img_card_1.jpg" class="img-fluid animated vp-fadeinup delayp2">
            </div>
            <div class="col-md-6">
                <p class="animated vp-fadeinup delayp1"><?= $partnershipPage->getDescription2() ?></p>
                <h5 class="mt-5 animated vp-fadeinup delayp2"><?= $partnershipPage->getContactUs1() ?></h5>
                <p class="animated vp-fadeinup delayp2"><?= $partnershipPage->getcontactUs1Description() ?></p>
                <a href="<?= $partnershipPage->getContactUsPage() ?>" class="btn btn-primary animated vp-fadeinup delayp3 mt-2">Contact Us</a>
            </div>
        </div>
    </div>
</section>

<section class="py-main section-visi">
    <div class="container">
        <div class="row row-3">
            <div class="col-md-6 order-md-first">
                <img src="/template_avrist/assets/img/tentang/ic_ilustrasi_home_mengapaavrist.png" class="img-fluid animated vp-fadeinup delayp1">
            </div>
            <div class="col-md-6 content-center">
                <h3><?= $partnershipPage->getTitle3() ?></h3>
                <p class="animated vp-fadeinup delayp1"><?= $partnershipPage->getDescription3() ?></p>
                <h5 class="mt-5 animated vp-fadeinup delayp2"><?= $partnershipPage->getContactUs2() ?></h5>
                <p class="animated vp-fadeinup delayp2"><?= $partnershipPage->getcontactUs2Description() ?></p>
                <div class="clearfix">
                    <a href="<?= $partnershipPage->getContactUsPage() ?>" class="btn btn-primary animated vp-fadeinup delayp3 mt-2">Contact Us</a>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="py-main section-tab-general partnership">
    <div class="container position-relative">
        <ul class="nav nav-tabs general-tab avram" id="generalTab" role="tablist">
        <?php $active = " active"; ?>
        <?php $selected = "true"; ?>
        <?php foreach($partnerships as $year => $partnership): ?> 
            <li class="nav-item">
                <a class="nav-link<?= $active ?>" id="y<?= $year ?>-tab" data-toggle="tab" href="#y<?= $year ?>" role="tab" aria-controls="y<?= $year ?>" aria-selected="<?= $selected ?>"><?= $year ?></a>
            </li>
            <?php if($active == " active" || $selected = "true"): ?>
                <?php $active = ""; ?>
                <?php $selected = ""; ?>
            <?php endif; ?>
        <?php endforeach; ?>
        </ul>
        <div class="tab-content" id="generalTabContent">
        <?php $show = " show active"; ?>
        <?php foreach($partnerships as $year => $partnership): ?>
            <div class="tab-pane fade<?= $show ?>" id="y<?= $year ?>" role="tabpanel" aria-labelledby="y<?= $year ?>-tab">
                <div class="row">
                <?php foreach($partnership as $p): ?>
                    <a href="<?= str_replace($partnershipPage->getPath(), '', $p->getFullPath()) ?>" class="col-md-4">
                        <div class="card card-partnership-detail">
                            <img src="<?= $p->getImage1() ?>" class="img-fluid" alt="Ornamen Yellow">
                            <div class="content">
                                <h5><?= $p->getTitle1()?></h5>
                                <p class="link">Lihat Detail</p>
                            </div>
                        </div>
                    </a>
                <?php endforeach; ?>

                </div>
            </div>
            <?php if($show = " show active"): ?>
                <?php $show = ""; ?>
            <?php endif; ?>
        <?php endforeach; ?>
        </div>
    </div>
</section>