<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

$this->extend('layout_avrist_new.html.php');?>

<div class="cover cover-sm cover-responsive <?= $bgBanner['bg'] ?> cover-general">
    <div class="cover-responsive-img">
        <div class="cover-responsive-overlay"></div>
    </div>
    <div class="container position-relative">
        <img src="/template_avrist/assets/img/pattern/ic_ornamen_yellow.png" class="pattern-purple" alt="Ornamen Purple">
        <!-- <img src="/template_avrist/assets/img/pattern/ic_ornament_triangle_yellow.png" class="pattern-triangle-yellow" alt="Ornamen Triangle Yellow"> -->
        <img src="/template_avrist/assets/img/pattern/ic_ornament_circle_pink.png" class="pattern-pink" alt="Ornamen Circle Pink">
        <nav aria-label="breadcrumb" class="general-breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active"><a href="#">Career</a></li>
            </ol>
        </nav>
        <div class="cover-content">
            <h3 class="cover-title text-white animated fadeInUp delayp1">Career</h3>
            <div class="row pl-2">
                <div class="col-md-6 pl-0 mt-4">
                    <div class="form-group">
                        <input type="text" class="form-control transparent" id="searchBox" placeholder="Judul, nama posisi atau lokasi" value="">
                    </div>
                </div>
                <div class="col-md-6 pl-0 mt-4">
                    <div class="form-group">
                        <div class="form-group">
                            <select class="custom-select transparent" id="division" required>
                            <?php foreach($options as $option): ?>
                                <option value="<?= $option['value'] ?>"><?= $option['key'] ?></option>
                            <?php  endforeach; ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix pl-0 mt-3" id="cari">
                <a href="#" class="btn btn-light w-150">Cari</a>
            </div>
        </div>
    </div>
</div>

<section class="py-main section-life-insurance-1">
    <img src="/template_avrist/assets/img/pattern/ornamen_waves_layer2_grey.png" class="pattern-grey" alt="Gray Waves">
    <img src="/template_avrist/assets/img/pattern/ic_ornamen_yellow.png" class="pattern-yellow" alt="Gray Waves">
    <img src="/template_avrist/assets/img/tentang/img_bg_grey.png" class="bg-gray">
    <div class="container">
        <div class="row">
            <div class="col-md-6 content-center">
                <div class="heading vp-fadeinup delayp2">
                    <h2><?= $careerPage->getTitle1() ?></h2>
                    <p class="p-0 subtitle"><?= $careerPage->getDescription1() ?> </p>
                    <a href="<?= $careerPage->getTitle1Page() ?>" class="download">View Jobs Opening</a>
                </div>
            </div>
            <div class="col-md-6">
                <img src="<?= $careerPage->getImage1() ?>" class="img-fluid">
            </div>
        </div>

    </div>
</section>

<section class="py-main section-career-list <?= $bgBanner['bg'] ?>">
    <div class="container position-relative">
        <div class="owl-carousel owl-theme owl-dots-solid owl-career vp-fadeinup delayp2">
        <?php foreach($careerPage->getBlock2() as $block2): ?>
            <div class="item">
                <div class="row row-5">
                    <div class="col-md-6 content-center">
                        <div class="heading mb-2">
                            <h2><?= $block2['blockTitle']->getData() ?></h2>
                            <p><?= $block2['blockSubtitle']->getData() ?></p>
                        </div>
                        <p><?= $block2['blockDescription']->getData() ?></p>
                    </div>
                    <div class="col-md-6">
                        <img src="<?= $block2['blockImage']->getData() ?>" class="img-fluid">
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
        </div>
    </div>
</section>

<section class="py-main section-life-insurance-1 career">
    <img src="/template_avrist/assets/img/tentang/ic_ornament_circle_pink.png" class="pattern-circle-pink" alt="Circle Pink">
    <img src="/template_avrist/assets/img/pattern/ic_ornament_circle_purple.png" class="pattern-circle-blue" alt="Circle Blue">
    <div class="container">
        <div class="heading vp-fadeinup delayp2">
            <h2><?= $careerPage->getTitle2() ?></h2>
        </div>

        <div class="content mt-5">
        <?php foreach($careerPage->getBlock3() as $block3): ?>
            <?php $i++; ?>
            <?php if(($i % 2) == 1): ?>
            <div class="row">
                <div class="col-6 col-md-5 vp-fadeinup delayp1">
            <?php else: ?>
                <div class="col-6 col-md-5 offset-md-1 vp-fadeinup delayp2">
            <?php endif; ?>
                    <div class="card card-no-border text-left">
                        <div class="card-body">
                            <img src="<?= $block3['blockImage']->getData() ?>" class="img-fluid" alt="Career">
                            <h5><?= $block3['blockTitle']->getData() ?></h5>
                            <p><?= $block3['blockDescription']->getData() ?></p>
                        </div>
                    </div>
                </div>
            <?php if(($i % 2) == 0): ?>
            </div>
            <?php endif; ?>
        <?php endforeach; ?>
        </div>
    </div>
</section>