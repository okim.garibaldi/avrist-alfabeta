<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

$this->extend('layout_avrist_non_transparent.html.php');?>

<section class="py-main">
    <div class="container">
        <?php foreach ($this->data as $d){?>
        	<div class="heading">
            	<a href="<?= $d->getPage() ?>"><?= $d->getName(); ?></a>
        	</div>
        <?php } ?>
    </div>
</section>
