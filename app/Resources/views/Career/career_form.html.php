<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

$this->extend('layout_avrist_non_transparent.html.php');?>

<section class="py-main">
    <div class="container">
        <form action="/save-application" method="post">
        	<fieldset>
        		<legend>Personal Information</legend>
        			Name<br>
        			<input type="text" name="name"><br>
        			Address<br>
        			<input type="text" name="address"><br>
                    Position<br>
                    <input type="text" name="position" value="<?= $this->position ?>" disabled><br>
        			City<br>
        			<input type="text" name="city"><br>
        			State<br>
        			<input type="text" name="state"><br>
        			Zip<br>
        			<input type="text" name="zip"><br>
        			Phone<br>
        			<input type="text" name="phone"><br>
        			Email<br>
        			<input type="text" name="email"><br>
        			Gender<br>
        			<input type="text" name="gender"><br>
        			Birth Place<br>
        			<input type="text" name="birthPlace"><br>
        			Birth Date<br>
        			<input type="text" name="birthDate"><br>
        			<input type="submit" name="submit">
        	</fieldset>
        </form>
    </div>
</section>
