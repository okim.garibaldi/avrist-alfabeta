<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

$this->extend('layout_avrist_new.html.php');?>

<div class="cover cover-sm cover-responsive <?= $bgBanner['bg'] ?> cover-general">
    <div class="cover-responsive-img">
        <div class="cover-responsive-overlay"></div>
    </div>
    <div class="container position-relative">
        <img src="/template_avrist/assets/img/pattern/ic_ornamen_yellow.png" class="pattern-purple" alt="Ornamen Purple">
        <!-- <img src="/template_avrist/assets/img/pattern/ic_ornament_triangle_yellow.png" class="pattern-triangle-yellow" alt="Ornamen Triangle Yellow"> -->
        <img src="/template_avrist/assets/img/pattern/ic_ornament_circle_pink.png" class="pattern-pink" alt="Ornamen Circle Pink">
        <nav aria-label="breadcrumb" class="general-breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item"><a href="#">Career</a></li>
                <li class="breadcrumb-item"><a href="#"><?= $category ?> </a></li>
                <li class="breadcrumb-item active"><a href="#">Detail</a></li>
            </ol>
        </nav>
        <div class="cover-content">
            <h3 class="cover-title text-white animated fadeInUp delayp1"><?= $career->getRole() ?></h3>
            <a href="<?= $career->getURL() ?>" class="btn btn-light w-150 mt-3">Apply</a>
        </div>
    </div>
</div>

<section class="py-main section-awards-detail">
    <img src="/template_avrist/assets/img/pattern/img_ornamen_waves_grey.png" class="pattern-grey" alt="Pattern Grey">
    <div class="container">
    <?php foreach($career->getBlock() as $block): ?>
        <div class="heading">
            <h2 class="animated vp-fadeinup delayp1"><?= $block['title']->getData() ?></h2>
        </div>
        <p class="animated vp-fadeinup delayp1"><?= $block['content']->getData() ?></p>
    <?php endforeach; ?>
    </div>
</section>