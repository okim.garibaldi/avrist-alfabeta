<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

$this->extend('layout_avrist_new.html.php');?>

<div class="cover cover-sm cover-responsive <?= $bgBanner['bg'] ?> cover-general">
    <div class="cover-responsive-img">
        <div class="cover-responsive-overlay"></div>
    </div>
    <div class="container position-relative">
        <img src="/template_avrist/assets/img/pattern/ic_ornamen_yellow.png" class="pattern-purple" alt="Ornamen Purple">
        <!-- <img src="/template_avrist/assets/img/pattern/ic_ornament_triangle_yellow.png" class="pattern-triangle-yellow" alt="Ornamen Triangle Yellow"> -->
        <img src="/template_avrist/assets/img/pattern/ic_ornament_circle_pink.png" class="pattern-pink" alt="Ornamen Circle Pink">
        <nav aria-label="breadcrumb" class="general-breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item"><a href="#">Career</a></li>
                <li class="breadcrumb-item active"><a href="#">Employee of Avrist</a></li>
            </ol>
        </nav>
        <div class="cover-content">
            <h3 class="cover-title text-white animated fadeInUp delayp1"><?= $careerList->getTitle1() ?></h3>
            <p class="cover-text"><?= $careerList->getDescription() ?></p>
        </div>
    </div>
</div>

<section class="tab-general">
    <div class="container">
        <!-- <ul class="nav nav-tabs general-tab-cover tab-three" id="generalTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="all-tab" data-toggle="tab" href="#all" role="tab" aria-controls="all" aria-selected="true">All Position</a>
            </li>
        <?php foreach ($careers as $key => $career): ?>
            <li class="nav-item">
                <a class="nav-link" id="<?= strtolower(strtok($key, " ")) ?>-tab" data-toggle="tab" href="#<?= strtolower(strtok($key, " ")) ?>" role="tab" aria-controls="<?= strtolower(strtok($key, " ")) ?>" aria-selected="false"><?= $key ?></a>
            </li>
        <?php endforeach; ?>
        </ul>-->
        <div class="tab-content" id="generalTabContent">
            <div class="tab-pane fade show active" id="all" role="tabpanel" aria-labelledby="all-tab">
                <div class="py-main">
                    <div class="row">
                <?php foreach($careers as $key => $career): ?>
                	<?php foreach($career as $c): ?>
                        <div class="col-12 col-md-6">
                            <a href="<?= str_replace($careerList->getPath(), '', $c->getFullPath()) ?>" class="card card-list">
                                <div class="card-body">
                                    <p class="division"><?= $key ?></p>
                                    <h3><?= $c->getRole() ?></h3>
                                    <p><?= substr($c->getBlock()['0']['content']->getData(), 0, 124) ?>..</p>
                                    <p class="download">Lihat detail</p>
                                </div>
                            </a>
                        </div>
                    <?php endforeach; ?>
                <?php endforeach; ?>	
                    </div>
                </div>
            </div>
        <?php foreach ($careers as $key => $career): ?>
            <div class="tab-pane fade" id="<?= strtolower(strtok($key, " ")) ?>" role="tabpanel" aria-labelledby="<?= strtolower(strtok($key, " ")) ?>-tab">
                <div class="py-main">
                    <div class="row">
                    <?php foreach($career as $c): ?>
                        <div class="col-12 col-md-6">
                            <a href="<?= str_replace($careerList->getPath(), '', $c->getFullPath()) ?>" class="card card-list">
                                <div class="card-body">
                                    <p class="division"><?= $c->getDivision() ?></p>
                                    <h3><?= $c->getRole() ?></h3>
                                    <p><?= substr($c->getBlock()['0']['content']->getData(), 0, 124) ?>.. </p>
                                    <p class="download">Lihat detail</p>
                                </div>
                            </a>
                        </div>
                    <?php endforeach; ?>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
        </div>
    </div>
</section>

<section class="py-main section-career-list">
    <div class="container position-relative">
        <div class="owl-carousel owl-theme owl-dots-solid owl-career vp-fadeinup delayp2">
        <?php foreach($careerList->getBlock2() as $block2): ?>
            <div class="item">
                <div class="row row-5">
                    <div class="col-md-6 content-center">
                        <div class="heading mb-2">
                            <h2><?= $block2['blockTitle']->getData() ?></h2>
                            <p><?= $block2['blockSubtitle']->getData() ?></p>
                        </div>
                        <p><?= $block2['blockDescription']->getData() ?></p>
                    </div>
                    <div class="col-md-6	">
                        <img src="<?= $block2['blockImage']->getData() ?>" class="img-fluid">
                    </div>
                </div>
            </div>
        <?php endforeach; ?> 
        </div>
    </div>
</section>

<section class="py-main section-life-insurance-1">
    <img src="/template_avrist/assets/img/pattern/ic_ornamen_yellow.png" class="pattern-yellow" alt="Gray Waves">
    <div class="container">
        <div class="heading vp-fadeinup delayp2">
            <h2><?= $careerList->getTitle2() ?></h2>
        </div>

        <div class="content mt-5">
        <?php foreach($careerList->getBlock3() as $block3): ?>
            <?php $i++; ?>
            <?php if(($i % 2) == 1): ?>
            <div class="row">
                <div class="col-6 col-md-5 vp-fadeinup delayp1">
            <?php else: ?>
                <div class="col-6 col-md-5 offset-md-1 vp-fadeinup delayp2">
            <?php endif; ?>
                    <div class="card card-no-border text-left">
                        <div class="card-body">
                            <img src="<?= $block3['blockImage']->getData() ?>" class="img-fluid" alt="Career">
                            <h5><?= $block3['blockTitle']->getData() ?></h5>
                            <p><?= $block3['blockDescription']->getData() ?></p>
                        </div>
                    </div>
                </div>
            <?php if(($i % 2) == 0): ?>
            </div>
            <?php endif; ?>
        <?php endforeach; ?>
        </div>
    </div>
</section>
