<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

use Pimcore\Model\Document;
use Pimcore\Model\Document\Page;

$this->extend('layout_avrist_new.html.php');?>

<div class="cover cover-sm cover-responsive cover-general <?= $bgBanner['bg'] ?>" style="background: linear-gradient(270deg, rgba(0,0,0,0.00) 0%, #432862 70%), url(/template_avrist/assets/img/common/img_banner_sample.jpg) no-repeat center; background-size: cover;">
    <div class="cover-responsive-img">
        <div class="cover-responsive-overlay"></div>
    </div>
    <div class="container position-relative">
        <img src="/template_avrist/assets/img/pattern/ic_ornamen_yellow.png" class="pattern-purple" alt="Ornamen Purple">
        <!-- <img src="/template_avrist/assets/img/pattern/ic_ornament_triangle_yellow.png" class="pattern-triangle-yellow" alt="Ornamen Triangle Yellow"> -->
        <img src="/template_avrist/assets/img/pattern/ic_ornament_circle_pink.png" class="pattern-pink" alt="Ornamen Circle Pink">
        <nav aria-label="breadcrumb" class="general-breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active"><a href="#"><?= $siteCategory->getName() ?></a></li>
            </ol>
        </nav>
        <div class="cover-content">
            <h3 class="cover-title text-white animated fadeInUp delayp1"><?= $siteCategory->getName() ?></h3>
            <p class="cover-text text-white"><?= $siteCategory->getDescription() ?></p>
        </div>
    </div>
    <!-- <a href="#" class="scroll-hint bottom-0 animated fadeInDown delayp4">
        <img src="/template_avrist/assets/img/common/arrow_down.png" alt="Arrow down hint"/>
      </a> -->
</div>

<section class="py-main section-life-insurance-1 pb-0">
    <img src="/template_avrist/assets/img/pattern/ornamen_waves_layer2_grey.png" class="pattern-grey" alt="Gray Waves">
    <img src="/template_avrist/assets/img/pattern/ic_ornamen_yellow.png" class="pattern-yellow" alt="Gray Waves">
    <div class="container">
        <div class="heading vp-fadeinup delayp2">
            <h2><?= $siteCategory->getTitle1() ?></h2>
            <p class="p-0 subtitle"><?= $siteCategory->getDescription1() ?></p>
        </div>

        <div class="content mt-5">
            <div class="row">
            <?php foreach($siteCategory->getBlock1() as $block1): ?>
                <div class="col-6 col-md-6 vp-fadeinup delayp1">
                    <a href="<?= $block1['link']->getData() ?>" class="card p-box card-insurance">
                        <div class="card-img">
                            <img src="<?= $block1['image']->getData() ?>" class="img-fluid">
                            <h5 class="card-title"><?= $block1['name']->getData() ?></h5>
                        </div>
                    </a>
                </div>
            <?php endforeach; ?>
            </div>
        </div>
    </div>
</section>

<section class="py-main section-life-insurance-2 pb-0">
    <div class="container">
        <div class="heading vp-fadeinup delayp1">
            <h2><?= $siteCategory->getTitle2() ?></h2>
            <p class="p-0 subtitle p-0"><?= $siteCategory->getDescription2() ?></p>
        </div>
        <div class="content">
            <div class="owl-carousel owl-theme owl-dots-solid owl-insurance mt-5 vp-fadeinup delayp2">
            <?php foreach($siteCategory->getBlock2() as $block2): ?>
                <a href="<?= $block2['link']->getData() ?>" class="card p-box card-insurance insurance-2">
                    <div class="card-img">
                        <img src="<?= $block2['image']->getData() ?>" class="img-fluid">
                        <h5 class="card-title"><?= $block2['name']->getData() ?></h5>
                    </div>
                </a>
            <?php endforeach; ?>
            </div>
        </div>
    </div>
</section>

<section class="py-main section-life-insurance-3 pb-0">
    <div class="container">
        <div class="heading">
            <h2><?= $siteCategory->getTitle3() ?></h2>
            <p class="p-0 subtitle"><?= $siteCategory->getDescription3() ?></p>
        </div>
        <div class="content">
            <div class="row">
            <?php $block3Count = 1; ?>
            <?php foreach($siteCategory->getBlock3() as $block3): ?>
                <div class="col-md-4 vp-fadeinup delayp1">
                    <div class="card-information">
                        <h4 class="number"><?= $block3Count++ ?></h4>
                        <p><?= $block3['text']->getData() ?></p>
                    </div>
                </div>
            <?php endforeach; ?>
            </div>
        </div>
    </div>
</section>

<section class="py-main section-life-insurance-4">
    <img src="/template_avrist/assets/img/pattern/ic_ornamen_green.png" class="pattern-green vp-fadeinup delayp1" alt="Ornamen green">
    <div class="container">
        <div class="heading-w-link">
            <h2>Kenali Kebutuhanmu</h2>
            <div class="heading-group">
                <div class="row">
                    <div class="col-md-6">
                        <p class="text-uppercase">by life guide</p>
                    </div>
                    <div class="col-md-6 text-md-right">
                        <a href="http://avrist.com/lifeguide/">Lihat semua</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="content text-white">
            <div class="owl-carousel owl-theme owl-dots-solid owl-kebutuhan mt-5 vp-fadeinup delayp2">

                <?php foreach ($this->navbar['rss_feed']->channel->item as $feed_item) : ?>
                    <a href="<?= $feed_item->link ?>" class="card card-kebutuhan">
                    <div class="card-img">
                      <img src="http://avrist.com/lifeguide/wp-content/uploads/2020/02/Photo-by-Ben-White-on-Unsplash-534x462.jpg" class="img-fluid">
                    </div>
                    <div class="card-body">
                      <p class="card-date"><?= date("j F Y", strtotime($feed_item->pubDate)) ?></p>
                      <h5 class="card-title text-truncate-twoline"><?= $feed_item->title ?></h5>
                    </div>
                  </a>
                <?php endforeach; ?>

            </div>
        </div>
    </div>
</section>

<section class="bg-light py-main section-life-insurance-5" style="background: url(/template_avrist/assets/img/common/img_banner_solution.jpg) no-repeat center; background-size: cover;">
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <h2>Butuh perhitungan dan proteksi ideal untukmu atau bisnismu? <a href="<?= $this->navbar["url"]->getHome() ?>/get-solution/personal">Dapatkan Solusi</a></h2>
            </div>
        </div>
    </div>
</section>

<section class="py-main section-layanan-kamu">
    <div class="container position-relative">
        <img src="/template_avrist/assets/img/pattern/ic_ornamen_green.png" class="pattern-green" alt="Pattern Green">
        <div class="heading text-center mb-5 vp-fadeinup delayp1">
            <h2><?= $siteCategory->getTitle4() ?></h2>
        </div>
        <div class="content">
            <div class="row">
            <?php $layananKamiCount = 0; ?>
            <?php foreach($siteCategory->getBlock4() as $block4): ?>
                <?php if(($layananKamiCount % 2) == 0): ?>
                <div class="col-md-4 offset-md-2 vp-fadeinup delayp2 visible animated fadeInUp">
                <?php $layananKamiCount++; ?>
                <?php else: ?>  
                    <div class="col-md-4 vp-fadeinup delayp3 visible animated fadeInUp">
                <?php endif; ?>
                    <a href="<?= $block4['link']->getData() ?>" class="card card-service">
                    <img src="<?= $block4['image']->getData() ?>" class="img-fluid service-img" alt="Card Img">
                    <div class="content">
                      <h5 class="card-title"><?= $block4['name']->getData() ?></h5>
                    </div>
                    </a>
                </div>
            <?php endforeach; ?>
                <!-- <div class="col-md-4 vp-fadeinup delayp4">
              <a href="#" class="card card-service">
                <img src="/template_avrist/assets/img/common/ic_ilustrasi_isometrik_RS.png" class="img-fluid service-img" alt="Card Img">
                <div class="content">
                  <h5 class="card-title">Harga Unit</h5>
                </div>
              </a>
            </div> -->
            </div>
        </div>
    </div>
</section>
