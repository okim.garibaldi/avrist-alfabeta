<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

use Pimcore\Model\Document;
use Pimcore\Model\Document\Page;

$controller = new \AppBundle\Controller\DefaultController();
$url = $controller->url();

$this->extend('layout_avrist.html.php');
$prefix = $url->getHome();
$lifeaddr = $url->getLife();
$generaladdr = $url->getGeneral();
$amaddr = $url->getAm();?>

<section class="py-main cover cover-landing-page">
    <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/pattern/ic_ornamen_yellow.png')?>" class="pattern-yellow" alt="Pattern Yellow">
    <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/pattern/ic_ornamen_yellow.png')?>" class="pattern-yellow-1" alt="Pattern Yellow">
    <div class="container content-center text-center">
        <form class="form-cover">
            <h2>Saya butuh asuransi</h2>
            <div class="dropdown-custom form-group">
                <div class="dropdown-toggle form-control" data-toggle="dropdown" id="select-value">
                    Jiwa
                </div>
                <div class="dropdown-menu">
                    <div class="selection-value dropdown-item" data-select="0"><a href="/personal/life">Jiwa</a></div>
                    <div class="selection-value dropdown-item" data-select="1"><a href="/personal/syariah">Jiwa Syariah</a></div>
                    <div class="selection-value dropdown-item" data-select="2"><a href="/personal/health">Kecelakaan & Kesehatan</a></div>
                    <div class="selection-value dropdown-item" data-select="3"><a href="/personal/education">Edukasi</a></div>
                    <div class="selection-value dropdown-item" data-select="4"><a href="/business/employee">Karyawan</a></div>
                    <div class="selection-value dropdown-item" data-select="5"><a href="/business/retirement">Pensiun</a></div>
                </div>
            </div>
        </form>
        <!-- <h2>Saya butuh asuransi </h2> -->

    </div>
</section>

<section class="py-main section-insurance-life">
    <div class="container">
        <div class="row">

            <?php foreach ($this->product as $p){?>
            <div class="col-md-4">
                <a href="<?= $p->getPage()?>" class="card card-news-insurance">
                    <div class="card-img-top" style="background: url(<?php echo $p->getImage ?>) no-repeat center; background-size: cover;"></div>
                    <div class="card-body">
                        <p class="card-label">Avrist Life - Individu</p>
                        <h5 class="card-title"><?= $p->getName()?></h5>
                        <p class="card-text"><?= $p->getSummary()?></p>
                        <!-- <a href="#" class="card-link">Lihat detail</a> -->
                    </div>
                </a>
            </div>
            <?php } ?>

        </div>
    </div>
</section>
