<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

use Pimcore\Model\Document;
use Pimcore\Model\Document\Page;

$this->extend('layout_avrist_new.html.php');?>

<section class="py-main cover cover-landing-page">
    <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/pattern/ic_ornamen_yellow.png')?>" class="pattern-yellow" alt="Pattern Yellow">
    <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/pattern/ic_ornamen_yellow.png')?>" class="pattern-yellow-1" alt="Pattern Yellow">
    <div class="container content-center text-center">
        <form class="form-cover">
            <h2>Saya butuh asuransi</h2>
            <div class="dropdown-custom form-group">
                <div class="dropdown-toggle form-control" data-toggle="dropdown" id="select-value">
                    Life
                </div>
                <div class="dropdown-menu">
                    <a href="/life-insurance/personal/life"><div class="selection-value dropdown-item" data-select="0">Life</div></a>
                    <a href="/life-insurance/personal/syariah"><div class="selection-value dropdown-item" data-select="1">Syariah</div></a>
                    <a href="/life-insurance/personal/health"><div class="selection-value dropdown-item" data-select="2">Health</div></a>
                    <a href="/life-insurance/personal/education"><div class="selection-value dropdown-item" data-select="3">Education</div></a>
                    <a href="/life-insurance/business/employee"><div class="selection-value dropdown-item" data-select="4">Employee</div></a>
                    <a href="/life-insurance/business/retirement"><div class="selection-value dropdown-item" data-select="5">Retirement</div></a>
                </div>
            </div>
        </form>

        <div><p><?= $this->category->getDescription() ?></p></div>
    </div>
</section>

<section class="py-main section-insurance-life">
    <div class="container">
        <div class="row">

            <?php foreach ($this->product as $p){?>
            <div class="col-md-4">
                <a href="<?= $p->getPage()?>" class="card card-news-insurance">
                    <div class="card-img-top" style="background: url(<?php echo $p->getImage(); ?>) no-repeat center; background-size: cover;"></div>
                    <div class="card-body">
                        <p class="card-label">Life insurance</p>
                        <h5 class="card-title text-truncate"><?= $p->getName()?></h5>
                        <p class="card-text text-truncate-multiline">
                            <?= str_replace(['<p>', '</p>'], ['<br>', '</br>'], $p->getSummary())?>
                        </p>
                        <!-- <a href="#" class="card-link">Lihat detail</a> -->
                    </div>
                </a>
            </div>
            <?php } ?>

        </div>
    </div>
</section>

<script>
    //dropdown
    let url = window.location.href.split('?')[0];
    let dp = url.substr(url.lastIndexOf('/') + 1);
    document.getElementById('select-value').innerText = dp.charAt(0).toUpperCase() + dp.substr(1);

    $('.owl-insurance').owlCarousel({
        loop: false,
        margin: 7.5,
        dots: false,
        responsive:{
            0:{
                items: 1,
                margin: 20,
                dots: true
            },
            768:{
                margin: 15,
                items: 4
            }
        }
    });

    $('.owl-kebutuhan').owlCarousel({
        loop: false,
        margin: 7.5,
        dots: false,
        responsive:{
            0:{
                items: 1
            },
            768:{
                margin: 30,
                items: 3
            }
        }
    });

    //dropdown custom
    $(".selection-value").click(function(e) {
        const selected = e.target.dataset.select;
        switch(selected) {
            case '0':
                $('#select-value').html('Jiwa');
                break;
            case '1':
                $('#select-value').html('Jiwa Syariah');
                break;
            case '2':
                $('#select-value').html('Kecelakaan & Kesehatan');
                break;
            case '3':
                $('#select-value').html('Edukasi');
                break;
            case '4':
                $('#select-value').html('Karyawan');
                break;
            case '5':
                $('#select-value').html('Pensiun');
                break;
        }
    })
</script>
