<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

use Pimcore\Model\Document;
use Pimcore\Model\Document\Page;

$controller = new \AppBundle\Controller\DefaultController();
$url = $controller->url();
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
    <head>
        <?php $prefix = $url->getHome();?>
        <?php $lifeaddr = $url->getLife();?>
        <?php $generaladdr = $url->getGeneral();?>
        <?php $amaddr = $url->getAm();?>
        <?= $this->template("include/head.html.php"); ?>
        <?= $this->template("include/tracking-script/head.html.php"); ?>

        <style>
            .navbar.fixed-top + * {
                margin-top: 0;
            }
        </style>

    </head>
    <body>
        <?= $this->template("include/tracking-script/body.html.php"); ?>

        <div class="loader-wrapper loader-light">
            <div class="loader"></div>
        </div>

        <?= $this->template("include/navbar.html.php"); ?>

        <!--Start: Cover Responsive-->
        <div class="cover cover-responsive cover-life-insurance">
            <div class="cover-responsive-img">
                <div class="cover-responsive-overlay"></div>
            </div>
            <div class="container position-relative justify-content-center">
                <div class="cover-content text-center">
                    <h3 class="cover-title animated fadeInUp delayp1"><?= $this->category->getName() ?></h3>
                    <p><?= $this->category->getDescription() ?></p>
                    <a href="<?= $prefix ?>/get-solution/personal" class="btn btn-primary">Temukan Solusi Untuk Anda</a>
                </div>
            </div>
            <a href="#" class="scroll-hint bottom-0 animated fadeInDown delayp4">
                <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/common/arrow_down.png')?>" alt="Arrow down hint"/>
            </a>
        </div>
        <!--End: Cover Responsive-->

        <section class="py-main section-life-insurance-1">
            <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/pattern/ornamen_waves_layer2_grey.png')?>" class="pattern-grey" alt="Gray Waves">
            <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/pattern/ic_ornamen_yellow.png')?>" class="pattern-yellow" alt="Gray Waves">
            <div class="container">
                <div class="heading vp-fadeinup delayp2">
                    <h2>Tentang</h2>
                    <p class="col-md-8 p-0 subtitle"><?= $this->category->getDescription() ?></p>
                </div>

                <div class="content mt-5">
                    <div class="row">
                        <div class="col-6 col-md-6 vp-fadeinup delayp1">
                            <a href="/personal/life" class="card p-box card-insurance">
                                <div class="card-img">
                                    <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/icon/ic_illustrasi_personal.png')?>" class="img-fluid">
                                    <h5 class="card-title">Personal</h5>
                                </div>
                            </a>
                        </div>
                        <div class="col-6 col-md-6 vp-fadeinup delayp1">
                            <a href="/business/employee" class="card p-box card-insurance">
                                <div class="card-img">
                                    <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/icon/ic_illustrasi_bisnis.png')?>" class="img-fluid">
                                    <h5 class="card-title">Bisnis</h5>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="py-main section-life-insurance-2">
            <div class="container">
                <div class="heading vp-fadeinup delayp1">
                    <h2>Proteksi Lengkap</h2>
                    <p class="subtitle col-md-8 p-0">Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. </p>
                </div>
                <div class="content">
                    <div class="owl-carousel owl-theme owl-dots-solid owl-insurance mt-5 vp-fadeinup delayp2">

                        <a href="/personal/life" class="card p-box card-insurance insurance-2">
                            <div class="card-img">
                                <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/icon/ic_life_insurance_life.png')?>" class="img-fluid">
                                <h5 class="card-title">Jiwa</h5>
                            </div>
                        </a>

                        <a href="/personal/health" class="card p-box card-insurance insurance-2">
                            <div class="card-img">
                                <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/icon/ic_life_insurance_health.png')?>" class="img-fluid">
                                <h5 class="card-title">Kesehatan</h5>
                            </div>
                        </a>

                        <a href="/personal/education" class="card p-box card-insurance insurance-2">
                            <div class="card-img">
                                <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/icon/ic_life_insurance_education.png')?>" class="img-fluid">
                                <h5 class="card-title">Pendidikan</h5>
                            </div>
                        </a>

                        <a href="/business/retirement" class="card p-box card-insurance insurance-2">
                            <div class="card-img">
                                <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/icon/ic_life_insurance_retirement.png')?>" class="img-fluid">
                                <h5 class="card-title">Pensiun</h5>
                            </div>
                        </a>

                        <a href="/personal/syariah" class="card p-box card-insurance insurance-2">
                            <div class="card-img">
                                <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/icon/ic_life_insurance_syariah.png')?>" class="img-fluid">
                                <h5 class="card-title">Syariah</h5>
                            </div>
                        </a>

                        <a href="/business/employee" class="card p-box card-insurance insurance-2">
                            <div class="card-img">
                                <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/icon/ic_life_insurance_employee.png')?>" class="img-fluid">
                                <h5 class="card-title">Karyawan</h5>
                            </div>
                        </a>

                        <a href="/personal/health" class="card p-box card-insurance insurance-2">
                            <div class="card-img">
                                <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/icon/ic_proteksi_kesehatan.png')?>" class="img-fluid">
                                <h5 class="card-title">Kesehatan</h5>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </section>

        <section class="py-main section-life-insurance-3">
            <div class="container">
                <div class="heading">
                    <h2>Keuntungan</h2>
                </div>
                <div class="content">
                    <div class="row">
                        <div class="col-md-4 vp-fadeinup delayp1">
                            <div class="card-information">
                                <h4 class="number">1</h4>
                                <p>Premi Mulai dari Rp5000/hari</p>
                            </div>
                        </div>

                        <div class="col-md-4 vp-fadeinup delayp1">
                            <div class="card-information">
                                <h4 class="number">2</h4>
                                <p>Premi Mulai dari Rp5000/hari</p>
                            </div>
                        </div>

                        <div class="col-md-4 vp-fadeinup delayp1">
                            <div class="card-information">
                                <h4 class="number">3</h4>
                                <p>Premi Mulai dari Rp5000/hari</p>
                            </div>
                        </div>

                        <div class="col-md-4 vp-fadeinup delayp1">
                            <div class="card-information">
                                <h4 class="number">4</h4>
                                <p>Premi Mulai dari Rp5000/hari</p>
                            </div>
                        </div>

                        <div class="col-md-4 vp-fadeinup delayp1">
                            <div class="card-information">
                                <h4 class="number">5</h4>
                                <p>Premi Mulai dari Rp5000/hari</p>
                            </div>
                        </div>

                        <div class="col-md-4 vp-fadeinup delayp1">
                            <div class="card-information">
                                <h4 class="number">6</h4>
                                <p>Premi Mulai dari Rp5000/hari</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="py-main section-life-insurance-4">
            <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/pattern/ic_ornamen_green.png')?>" class="pattern-green vp-fadeinup delayp1" alt="Ornamen green">
            <div class="container">
                <div class="heading-w-link">
                    <h2>Kenali kebutuhanmu</h2>
                    <div class="heading-group">
                        <div class="row">
                            <div class="col-md-6">
                                <p>by life group</p>
                            </div>
                            <div class="col-md-6 text-md-right">
                                <a href="#">Lihat semua</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content text-white">
                    <div class="owl-carousel owl-theme owl-dots-solid owl-kebutuhan mt-5 vp-fadeinup delayp2">
                        <a href="#" class="card card-kebutuhan">
                            <div class="card-img">
                                <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/sample/bg_rectangle-dark-2.jpg')?>" class="img-fluid">
                            </div>
                            <div class="card-body">
                                <p class="card-date">12 Agustus 2018</p>
                                <h5 class="card-title">Ketahui Asuransi Jiwa yang Diperlukan Keluarga</h5>
                            </div>
                        </a>

                        <a href="#" class="card card-kebutuhan">
                            <div class="card-img">
                                <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/sample/bg_rectangle-light-1.jpg')?>" class="img-fluid">
                            </div>
                            <div class="card-body">
                                <p class="card-date">12 Agustus 2018</p>
                                <h5 class="card-title">Ketahui Asuransi Jiwa yang Diperlukan Keluarga</h5>
                            </div>
                        </a>

                        <a href="#" class="card card-kebutuhan">
                            <div class="card-img">
                                <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/sample/bg_rectangle-light-2.jpg')?>" class="img-fluid">
                            </div>
                            <div class="card-body">
                                <p class="card-date">12 Agustus 2018</p>
                                <h5 class="card-title">Ketahui Asuransi Jiwa yang Diperlukan Keluarga</h5>
                            </div>
                        </a>

                        <a href="#" class="card card-kebutuhan">
                            <div class="card-img">
                                <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/sample/bg_rectangle-light-3.jpeg')?>" class="img-fluid">
                            </div>
                            <div class="card-body">
                                <p class="card-date">12 Agustus 2018</p>
                                <h5 class="card-title">Ketahui Asuransi Jiwa yang Diperlukan Keluarga</h5>
                            </div>
                        </a>

                        <a href="#" class="card card-kebutuhan">
                            <div class="card-img">
                                <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/sample/bg_rectangle-dark-2.jpg')?>" class="img-fluid">
                            </div>
                            <div class="card-body">
                                <p class="card-date">12 Agustus 2018</p>
                                <h5 class="card-title">Ketahui Asuransi Jiwa yang Diperlukan Keluarga</h5>
                            </div>
                        </a>

                    </div>
                </div>
            </div>
        </section>

        <section class="bg-light py-main section-life-insurance-5" style="background: url(/template_avrist/assets/img/common/img_banner_solution.jpg) no-repeat center; background-size: cover;">
            <div class="container">
                <div class="row">
                    <div class="col-md-7">
                        <h2>Butuh perhitungan dan proteksi ideal untukmu atau bisnismu? <a href="#">Dapatkan Solusi</a></h2>
                    </div>
                </div>
            </div>
        </section>


        <section class="py-main section-layanan-kamu">
            <div class="container position-relative">
                <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/pattern/ic_ornamen_green.png')?>" class="pattern-green" alt="Pattern Green">
                <div class="heading text-center mb-5 vp-fadeinup delayp1">
                    <h2>Layanan Kami</h2>
                </div>
                <div class="content">
                    <div class="row">
                        <div class="col-md-4 vp-fadeinup delayp2">
                            <a href="#" class="card card-service">
                                <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/common/ic_ilustrasi_isometrik_RS.png')?>" class="img-fluid service-img" alt="Card Img">
                                <div class="content">
                                    <h5 class="card-title">Rumah Sakit <br>Partner</h5>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-4 vp-fadeinup delayp3">
                            <a href="#" class="card card-service">
                                <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/common/ic_ilustrasi_isometrik_RS.png')?>" class="img-fluid service-img" alt="Card Img">
                                <div class="content">
                                    <h5 class="card-title">Bengkel <br>Partner</h5>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-4 vp-fadeinup delayp4">
                            <a href="#" class="card card-service">
                                <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/common/ic_ilustrasi_isometrik_RS.png')?>" class="img-fluid service-img" alt="Card Img">
                                <div class="content">
                                    <h5 class="card-title">Harga Unit</h5>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <?= $this->template("include/footer.html.php"); ?>
        <?= $this->template("include/script.html.php"); ?>

        <script>
            // Navbar active state
            // $('#navHome').addClass('active');

            // Animation on load
            document.addEventListener("DOMContentLoaded", function(event) {
                $(".loader").fadeOut('slow');
                $(".loader-wrapper").fadeOut("slow");
                //  $(".anim-1").addClass("animated fadeInLeft delayp10");
                // $(".anim-2").addClass("animated fadeInUp delayp12");
                //$(".anim-3").addClass("animated fadeInUp delayp14");
            });

            $(".navbar").addClass('navbar-transparent');
            var logo = ["/template_avrist/assets/img/brand/logo_avrist.png", "/template_avrist/assets/img/brand/logo_avrist_white.png"];
            $('.navbar-avrist .navbar-brand img').attr('src', logo[1]);

            $(window).on("scroll", function() {
                if ($(window).scrollTop() > 10) {
                    $(".navbar-avrist").removeClass("navbar-transparent");
                    $('.navbar-avrist .navbar-brand img').attr('src', logo[0]);
                } else {
                    //remove the background property so it comes transparent again (defined in your css)
                    $(".navbar-avrist").addClass("navbar-transparent");
                    $('.navbar-avrist .navbar-brand img').attr('src', logo[1]);
                }
            });

            $('.owl-insurance').owlCarousel({
                loop: false,
                margin: 7.5,
                dots: false,
                responsive:{
                    0:{
                        items: 1,
                        margin: 20,
                        dots: true
                    },
                    768:{
                        margin: 15,
                        items: 4
                    }
                }
            });

            $('.owl-kebutuhan').owlCarousel({
                loop: false,
                margin: 7.5,
                dots: false,
                responsive:{
                    0:{
                        items: 1
                    },
                    768:{
                        margin: 30,
                        items: 3
                    }
                }
            });

        </script>

    </body>
</html>
