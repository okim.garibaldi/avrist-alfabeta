<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

$this->extend('layout_avrist_new.html.php');?>

<div class="cover cover-sm cover-responsive <?= $bgBanner['bg'] ?> cover-general">
    <div class="cover-responsive-img">
        <div class="cover-responsive-overlay"></div>
    </div>
    <div class="container position-relative">
        <img src="/template_avrist/assets/img/pattern/ic_ornamen_yellow.png" class="pattern-purple" alt="Ornamen Purple">
        <!-- <img src="/template_avrist/assets/img/pattern/ic_ornament_triangle_yellow.png" class="pattern-triangle-yellow" alt="Ornamen Triangle Yellow"> -->
        <img src="/template_avrist/assets/img/pattern/ic_ornament_circle_pink.png" class="pattern-pink" alt="Ornamen Circle Pink">
        <nav aria-label="breadcrumb" class="general-breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active"><a href="#"><?= $formulirPembukaanPage->getName() ?></a></li>
            </ol>
        </nav>
        <div class="cover-content">
            <h3 class="cover-title text-white animated fadeInUp delayp1"><?= $formulirPembukaanPage->getName() ?></h3>
            <div class="col-md-6 pl-0">
                <div class="form-group">
                    <input type="text" class="form-control transparent" id="searchBox" placeholder="Cari Formulir" value="" required>
                    <div class="invalid-feedback">
                        Please enter cari formulir
                    </div>
                </div>
            </div>
            <div class="clearfix mt-3" id="cari">
                <a href="#" class="btn btn-light w-150">Cari</a>
            </div>
        </div>
    </div>
</div>

<section class="py-main section-general-tentang-2">
    <div class="container">
        <div class="row">
        <?php foreach($files as $file): ?>
            <div class="col-6 col-md-4">
                <a href="<?= $file['file']->getFullPath() ?>" class="card card-list" download>
                    <div class="card-body">
                        <h3><?= $file['name'] ?></h3>
                        <p><img src="/template_avrist/assets/img/tentang/pdf.png" class="extension-file">Pdf Files | <?= number_format($file['file']->getFileSize() / 1024, 2) . ' KB' ?></p>
                        <p class="download">Download</p>
                    </div>
                </a>
            </div>
        <?php endforeach; ?>
            </div>
        </div>
    </div>
</section>

<section class="py-main bg-light">
    <div class="container">
        <div class="row row-3">
            <div class="col-md-6 content-center">
                <div class="heading">
                    <h2 class="mb-4"><?= $formulirPembukaanPage->getContactUs() ?></h2>
                    <<?= $formulirPembukaanPage->getContactUsDescription() ?></p>
                    <a href="<?= $formulirPembukaanPage->getContactUsPage() ?>" class="btn btn-primary">Contact Us</a>
                </div>
            </div>
            <div class="col-md-6">
                <img src="/template_avrist/assets/img/tentang/ic_ilustrasi_home_mengapaavrist.png" class="img-fluid animated vp-fadeinup delayp1">
            </div>
        </div>
    </div>
</section>