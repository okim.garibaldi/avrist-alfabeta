<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

$this->extend('layout_avrist_new.html.php');?>

<div class="cover cover-sm cover-responsive bg-primary cover-general">
    <div class="cover-responsive-img">
        <div class="cover-responsive-overlay"></div>
    </div>
    <div class="container position-relative">
        <img src="/template_avrist/assets/img/pattern/ic_ornamen_yellow.png" class="pattern-purple" alt="Ornamen Purple">
        <!-- <img src="/template_avrist/assets/img/pattern/ic_ornament_triangle_yellow.png" class="pattern-triangle-yellow" alt="Ornamen Triangle Yellow"> -->
        <img src="/template_avrist/assets/img/pattern/ic_ornament_circle_pink.png" class="pattern-pink" alt="Ornamen Circle Pink">
        <nav aria-label="breadcrumb" class="general-breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item"><a href="#">Layanan Nasabah</a></li>
                <li class="breadcrumb-item active"><a href="#"><?= $formulirLayanan->getTitle() ?></a></li>
            </ol>
        </nav>
        <div class="cover-content">
            <h3 class="cover-title text-white animated fadeInUp delayp1"><?= $formulirLayanan->getTitle() ?></h3>
            <p class="cover-text"><?= $formulirLayanan->getDescription() ?></p>
        </div>
    </div>
</div>

<?php foreach($arr as $key => $assets): ?>
<section class="py-main section-general-tentang-2">
    <div class="container">
        <div class="heading">
            <h2 class="title"><?= $key ?></h2>
        </div>
        <div class="row">
        <?php foreach($assets as $asset): ?>
            <div class="col-6 col-md-3">
                <a href="<?= $asset['file']->getFullPath() ?>" class="card card-list" download>
                    <div class="card-body">
                        <h3 class="text-truncate"><?= $asset['name'] ?> </h3>
                        <p><img src="/template_avrist/assets/img/tentang/pdf.png" class="extension-file">Pdf Files | <?= number_format($asset['file']->getFileSize() / 1024, 2) . ' KB' ?></p>
                        <p class="download">Download</p>
                    </div>
                </a>
            </div>
        <?php endforeach; ?>
        </div>
    </div>
</section>
<?php endforeach; ?>

<section class="py-main bg-light">
    <div class="container">
        <div class="row row-3">
            <div class="col-md-6 content-center">
                <div class="heading">
                    <h2 class="mb-4"><?= $formulirLayanan->getContactUs() ?></h2>
                    <p><?= $formulirLayanan->getContactUsDescription() ?></p>
                    <a href="<?= $formulirLayanan->getContactUsPage() ?>" class="btn btn-primary">Contact Us</a>
                </div>
            </div>
            <div class="col-md-6">
                <img src="/template_avrist/assets/img/tentang/ic_ilustrasi_home_mengapaavrist.png" class="img-fluid animated vp-fadeinup delayp1">
            </div>
        </div>
    </div>
</section>