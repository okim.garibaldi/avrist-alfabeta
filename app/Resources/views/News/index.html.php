<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

use Pimcore\Model\Document;
use Pimcore\Model\Document\Page;

$this->extend('layout_avrist_new.html.php');?>
    
    <div class="cover cover-sm cover-responsive <?= $bgBanner['bg'] ?> cover-general" <?php if($siteId != 1) echo 'style="background: linear-gradient(270deg, rgba(0,0,0,0.00) 0%, #7F8AA3 70%), url('.$bgBanner['bgImg'].') no-repeat center; background-size: cover;"'  ?>>
      <div class="cover-responsive-img">
        <div class="cover-responsive-overlay"></div>
      </div>
      <div class="container position-relative">
        <img src="/template_avrist/assets/img/pattern/ic_ornamen_yellow.png" class="pattern-purple" alt="Ornamen Purple">
        <!-- <img src="/template_avrist/assets/img/pattern/ic_ornament_triangle_yellow.png" class="pattern-triangle-yellow" alt="Ornamen Triangle Yellow"> -->
        <img src="/template_avrist/assets/img/pattern/ic_ornament_circle_pink.png" class="pattern-pink" alt="Ornamen Circle Pink">
        <nav aria-label="breadcrumb" class="general-breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active"><a href="#">News & Events</a></li>
          </ol>
        </nav>
        <div class="cover-content">
          <h3 class="cover-title text-white animated fadeInUp delayp1"><?= $newsPage->getTitle1() ?></h3>
          <p class="cover-subtitle text-white"><?= $newsPage->getDescription1() ?></p>
        </div>
      </div>
    </div>

    <section class="py-main section-general-news">
      <div class="container">
        <div class="filter-general">
          <div class="row">
            <div class="col-md-2 offset-md-5">
              <div class="form-group">
                <select class="custom-select" id="validationCustom04" required>
                  <option value="desc" <?php if ($sortBy == "desc") echo "selected"?> >Terbaru</option>
                  <option value="asc" <?php if ($sortBy == "asc") echo "selected"?>>Terlama</option>   
                </select>
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <select class="custom-select" id="validationCustom05" required>
                <?php foreach($options as $option): ?>
                  <option value="<?= $option['value'] ?>" <?php if($category == $option['value']) echo "selected" ?>><?= $option['key'] ?></option>
                <?php endforeach; ?>
                </select>
              </div>
            </div>
            <div class="col-md-3">
              <input type="text" class="form-control" id="searchField" placeholder="Search" value="" required>
            </div>
          </div>
        </div>
        <div class="row">
        <?php foreach($news as $n): ?>
          <div class="col-6 col-md-4">
            <a href="<?= str_replace($page->getPath(), '', $n->getFullPath()) ?>" class="card card-kebutuhan news">
              <div class="card-img">
                <img src="<?= $n->getImage1() ?>" class="img-fluid">
              </div>
              <div class="card-body p-box">
                <h5 class="card-title text-truncate-twoline"><?= $n->getTitle1() ?></h5>
                <p href="#" class="link">Lihat Detail</p>
              </div>
            </a>
          </div>
        <?php endforeach; ?>
        </div>
      </div>
    </section>
