<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

use Pimcore\Model\Document;
use Pimcore\Model\Document\Page;

$this->extend('layout_avrist_non_transparent.html.php');?>

<section class="py-main">
    <div class="container">
        <div class="heading">
        </div>
        <div>
            <div class="container position-relative">
                <img src="<?= $news->getImage()->getThumbnail('thumbnail1'); ?>" class="img-fluid vp-fadeinleft">
            </div>
            <h1><?= $news->getTitle() ?></h1>
            <?= $news->getDescription(); ?>
        </div>
    </div>
</section>
