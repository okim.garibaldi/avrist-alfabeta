
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
  <head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>Avrist</title>
  <meta name="description" content="">
  <meta name="author" content="">
  
  <meta property="og:title" content=""/>
  <meta property="og:description" content="" />
  <meta property="og:url" content="" />
  <meta property="og:image" content="/template_avrist/assets/ico/richlink.jpg" />

  <link rel="shortcut icon" href="/template_avrist/assets/ico/favicon.ico" type="image/x-icon">
  <link rel="apple-touch-icon" href="/template_avrist/assets/ico/apple-touch-icon.png">
  
  <link rel="stylesheet" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css" />
  <?php $this->headLink()->appendStylesheet('/template_avrist/assets/css/main.css');?>
  <?= $this->headLink(); ?>     <!-- Place any <head> tracking script here -->  </head>
  <body>
    <!-- Place any <body> tracking script here -->
    <div class="loader-wrapper loader-light">
      <div class="loader"></div>
    </div> 

    <nav class="navbar navbar-avrist navbar-absolute fixed-top navbar-expand-lg navbar-scroll">
  <div class="container position-relative">
    <button class="navbar-toggler navbar-toggler-left" type="button" data-toggle="collapse" data-target="#navbarContent" aria-controls="navbarContent" aria-expanded="false" aria-label="Toggle navigation">
      <i class="fal fa-bars"></i>
    </button>
    <a class="search-mobile" href="#modalSearch" data-toggle="modal">
      <i class="fal fa-search"></i>
    </a>

    <div class="collapse navbar-collapse" id="navbarContent">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item" id="navBeranda">
          <a class="nav-link animation-underline" href="../../../faq">Beranda</a>
        </li>
        <li class="nav-item" id="navSolusi">
          <div class="dropdown clearfix">
            <a href="#" class="nav-link animation-underline float-left">
              Solusi
            </a>
            <a href="#" class="float-right" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <i class="fal fa-angle-down text-primary" id="angle-dropdown"></i>
            </a>
            <div class="dropdown-menu animated fadeIn dropdown-menu-general dropdown-menu-1">
              <!-- <a class="dropdown-item" id="navLifeInsurance">Life Insurance</a>
              <a class="dropdown-item" id="navGeneralInsurance">General Insurance</a>
              <a class="dropdown-item" id="navAssetManagement">Asset Management</a> -->
              <div class="blocks-menus">
                <div class="container">
                  <ul class="primary-menus">
                    <li class="menus-megamenus">
                      <a class="nav-link-menus" href="../../../life-insurance">Life Insurance <i class="fa fa-angle-down"></i></a>
                      <ul class="mega-menus">
                        <div class="row">
                          <div class="col-md-8 scroll-bar">
                            <p class="megamenu-information">Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. </p>
                            <a href="../../../life-insurance/personal/life" class="d-block text-link link-arrow">PELAJARI TENTANG LIFE INSURANCE <i class="fa fa-chevron-right d-inline-block p-0"></i></a>
                            <div class="content-megamenu mt-4">
                              <div class="heading">
                                <h2>Personal</h2>
                              </div>
                              <div class="row">
                                <div class="col-md-4">
                                  <div class="card card-boxless card-megamenu">
                                      <a class="link-arrow font-weight-bold" href="../../../life-insurance/personal/life/avrist-sehati">Life <i class="fa fa-chevron-right d-inline-block p-0"></i></a>
                                    <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                                  </div>
                                </div>
                                <div class="col-md-4">
                                  <div class="card card-boxless card-megamenu">
                                      <a class="link-arrow font-weight-bold" href="../../../life-insurance/personal/life-syariah">Life Syariah <i class="fa fa-chevron-right d-inline-block p-0"></i></a>
                                    <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                                  </div>
                                </div>
                                <div class="col-md-4">
                                  <div class="card card-boxless card-megamenu">
                                      <a class="link-arrow font-weight-bold" href="../../../life-insurance/personal/accident-health">Accident & Health <i class="fa fa-chevron-right d-inline-block p-0"></i></a>
                                    <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                                  </div>
                                </div>
                                <div class="col-md-4">
                                  <div class="card card-boxless card-megamenu">
                                      <a class="link-arrow font-weight-bold" href="../../../life-insurance/personal/education">Education <i class="fa fa-chevron-right d-inline-block p-0"></i></a>
                                    <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="content-megamenu">
                              <div class="heading">
                                <h2>Business</h2>
                              </div>
                              <div class="row">
                                <div class="col-md-4">
                                  <div class="card card-boxless card-megamenu">
                                      <a class="link-arrow font-weight-bold" href="../../../life-insurance/business/life">Life <i class="fa fa-chevron-right d-inline-block p-0"></i></a>
                                    <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                                  </div>
                                </div>
                                <div class="col-md-4">
                                  <div class="card card-boxless card-megamenu">
                                      <a class="link-arrow font-weight-bold" href="#">Life Syariah <i class="fa fa-chevron-right d-inline-block p-0"></i></a>
                                    <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                                  </div>
                                </div>
                                <div class="col-md-4">
                                  <div class="card card-boxless card-megamenu">
                                      <a class="link-arrow font-weight-bold" href="#">Accident & Health <i class="fa fa-chevron-right d-inline-block p-0"></i></a>
                                    <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                                  </div>
                                </div>
                                <div class="col-md-4">
                                  <div class="card card-boxless card-megamenu">
                                      <a class="link-arrow font-weight-bold" href="#">Education <i class="fa fa-chevron-right d-inline-block p-0"></i></a>
                                    <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-4">
                            <a href="#" class="card card-megamenu-right" style="background: url(/template_avrist/assets/img/common/Mask.png) no-repeat center; background-size: cover; min-height: 330px;">
                              <img src="/template_avrist/assets/img/common/ic_ilustrasi_home_menus.png" class="img-fluid">
                              <div class="content">
                                <h2 class="text-white">Butuh solusi ideal untuk bisnis/personal?</h2>
                                <p href="#" class="link-arrow mb-0">
                                  Get Solution <i class="fas fa-chevron-right d-inline-block p-0"></i>
                                </p>
                              </div>
                            </a>
                          </div>
                        </div>
                      </ul>
                    </li>
                    <li class="menus-megamenus general-insurance">
                      <a class="nav-link-menus" href="../../../general-insurance">General Insurance <i class="fa fa-angle-down"></i></a>
                      <ul class="mega-menus">
                        <div class="row">
                          <div class="col-md-8">
                            <p class="megamenu-information">Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. </p>
                            <a href="../../../general-insurance" class="d-block text-link">PELAJARI TENTANG GENERAL INSURANCE <i class="fa fa-angle-right"></i></a>
                            <div class="content-megamenu mt-4">
                              <div class="row">
                                <div class="col-md-4">
                                  <div class="card card-boxless card-megamenu">
                                    <a href="../../../general-insurance/avrist-oto" class="link-arrow font-weight-bold">Avrist Oto <i class="fa fa-chevron-right d-inline-block p-0"></i></a>
                                    <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                                  </div>
                                </div>
                                <div class="col-md-4">
                                  <div class="card card-boxless card-megamenu">
                                    <a class="link-arrow font-weight-bold" href="../../../general-insurance/property">Property <i class="fa fa-chevron-right d-inline-block p-0"></i></a>
                                    <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                                  </div>
                                </div>
                                <div class="col-md-4">
                                  <div class="card card-boxless card-megamenu">
                                    <a class="link-arrow font-weight-bold" href="../../../general-insurance/personal-accident">Personal Accident <i class="fa fa-chevron-right d-inline-block p-0"></i></a>
                                    <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                                  </div>
                                </div>
                                <div class="col-md-4">
                                  <div class="card card-boxless card-megamenu">
                                    <a class="link-arrow font-weight-bold" href="../../../general-insurance/engineering">Engineering <i class="fa fa-chevron-right d-inline-block p-0"></i></a>
                                    <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                                  </div>
                                </div>
                                <div class="col-md-4">
                                  <div class="card card-boxless card-megamenu">
                                    <a class="link-arrow font-weight-bold" href="../../../general-insurance/casualty-insurance">Casualty Insurance <i class="fa fa-chevron-right d-inline-block p-0"></i></a>
                                    <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                                  </div>
                                </div>
                                <div class="col-md-4">
                                  <div class="card card-boxless card-megamenu">
                                    <a class="link-arrow font-weight-bold" href="../../../general-insurance/micro-insurance">Micro Insurance <i class="fa fa-chevron-right d-inline-block p-0"></i></a>
                                    <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                                  </div>
                                </div>
                                <div class="col-md-4">
                                  <div class="card card-boxless card-megamenu">
                                    <a class="link-arrow font-weight-bold" href="../../../general-insurance/glasses-protection">Glasses Protection <i class="fa fa-chevron-right d-inline-block p-0"></i></a>
                                    <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                                  </div>
                                </div>
                                <div class="col-md-4">
                                  <div class="card card-boxless card-megamenu">
                                    <a class="link-arrow font-weight-bold" href="../../../general-insurance/marine-cargo">Marine Cargo <i class="fa fa-chevron-right d-inline-block p-0"></i></a>
                                    <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-4">
                            <a href="#" class="card card-megamenu-right" style="background: url(/template_avrist/assets/img/common/Mask.png) no-repeat center; background-size: cover; min-height: 330px;">
                              <img src="/template_avrist/assets/img/common/ic_ilustrasi_home_menus.png" class="img-fluid">
                              <div class="content">
                                <h2 class="text-white">Butuh solusi ideal untuk bisnis/personal?</h2>
                                <p href="#" class="link-arrow mb-0">
                                  Get Solution <i class="fas fa-chevron-right d-inline-block p-0"></i>
                                </p>
                              </div>
                            </a>
                          </div>
                        </div>
                      </ul>
                    </li>
                    <li class="menus-megamenus asset-management">
                      <a class="nav-link-menus" href="../../../asset-management">Asset Management<i class="fa fa-angle-down"></i></a>
                      <ul class="mega-menus">
                        <div class="row">
                          <div class="col-md-8">
                            <p class="megamenu-information">Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. </p>
                            <a href="../../../asset-management" class="d-block text-link">PELAJARI TENTANG ASSET MANAGEMENT</a>
                            <div class="content-megamenu mt-4">
                              <div class="row">
                                <div class="col-md-4">
                                  <div class="card card-boxless card-megamenu">
                                    <a href="#" class="link-arrow font-weight-bold">Reksa Dana Konvensional <i class="fa fa-chevron-right d-inline-block p-0"></i></a>
                                    <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                                  </div>
                                </div>
                                <div class="col-md-4">
                                  <div class="card card-boxless card-megamenu">
                                    <a href="#" class="link-arrow font-weight-bold">Reksa Dana Syariah <i class="fa fa-chevron-right d-inline-block p-0"></i></a>
                                    <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                                  </div>
                                </div>
                                <div class="col-md-4">
                                  <div class="card card-boxless card-megamenu">
                                    <a href="#" class="link-arrow font-weight-bold">Reksa Dana Terproteksi <i class="fa fa-chevron-right d-inline-block p-0"></i></a>
                                    <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                                  </div>
                                </div>
                                <div class="col-md-4">
                                  <div class="card card-boxless card-megamenu">
                                    <a href="../../../asset-management/kas-mutiara" class="link-arrow font-weight-bold">Kas Mutiara <i class="fa fa-chevron-right d-inline-block p-0"></i></a>
                                    <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-4">
                            <a href="#" class="card card-megamenu-right" style="background: url(/template_avrist/assets/img/common/Mask.png) no-repeat center; background-size: cover; min-height: 330px;">
                              <img src="/template_avrist/assets/img/common/ic_ilustrasi_avram_menus.png" class="img-fluid">
                              <div class="content">
                                <h2 class="text-white">Ingin tahu produk rekomendasi bagi Anda?</h2>
                                <p href="#" class="link-arrow mb-0">
                                  Try Now <i class="fas fa-chevron-right d-inline-block p-0"></i>
                                </p>
                              </div>
                            </a>
                          </div>
                        </div>
                      </ul>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </li>
        <li class="nav-item" id="navTentang">
          <div class="dropdown clearfix">
            <a href="#" class="nav-link animation-underline float-left">
              Tentang
            </a>
            <a href="#" class="float-right" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <i class="fal fa-angle-down text-primary" id="angle-dropdown"></i>
            </a>
            <div class="dropdown-menu animated fadeIn dropdown-menu-general dropdown-menu-2">
              <!-- <a class="dropdown-item" id="navLifeInsurance">Life Insurance</a>
              <a class="dropdown-item" id="navGeneralInsurance">General Insurance</a>
              <a class="dropdown-item" id="navAssetManagement">Asset Management</a> -->
              <div class="blocks-menus">
                <div class="container">
                  <ul class="primary-menus">
                    <li class="menus-megamenus">
                      <a class="nav-link-menus" href="../../../about/life-insurance">Life Insurance <i class="fa fa-angle-down"></i></a>
                      <ul class="mega-menus">
                        <div class="row">
                          <div class="col-md-8 scroll-bar">
                            <p class="megamenu-information">Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. </p>
                            <a href="../../../about/life-insurance" class="d-block text-link link-arrow">PELAJARI TENTANG LIFE INSURANCE <i class="fa fa-chevron-right d-inline-block p-0"></i></a>
                            <div class="content-megamenu mt-4">
                              <div class="row">
                                <div class="col-md-4">
                                  <div class="card card-boxless card-megamenu">
                                      <a class="link-arrow font-weight-bold" href="../../../about/life-insurance/pt-avrist">PT. Avrist Life Assurance <i class="fa fa-chevron-right d-inline-block p-0"></i></a>
                                    <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                                  </div>
                                </div>
                                <div class="col-md-4">
                                  <div class="card card-boxless card-megamenu">
                                      <a class="link-arrow font-weight-bold" href="../../../about/life-insurance/news">News & Event <i class="fa fa-chevron-right d-inline-block p-0"></i></a>
                                    <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                                  </div>
                                </div>
                                <div class="col-md-4">
                                  <div class="card card-boxless card-megamenu">
                                      <a class="link-arrow font-weight-bold" href="../../../about/life-insurance/csr">CSR <i class="fa fa-chevron-right d-inline-block p-0"></i></a>
                                    <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                                  </div>
                                </div>
                                <div class="col-md-4">
                                  <div class="card card-boxless card-megamenu">
                                      <a class="link-arrow font-weight-bold" href="../../../about/life-insurance/career">Career <i class="fa fa-chevron-right d-inline-block p-0"></i></a>
                                    <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                                  </div>
                                </div>
                                <div class="col-md-4">
                                  <div class="card card-boxless card-megamenu">
                                      <a class="link-arrow font-weight-bold" href="../../../about/life-insurance/awards">Awards <i class="fa fa-chevron-right d-inline-block p-0"></i></a>
                                    <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                                  </div>
                                </div>
                                <div class="col-md-4">
                                  <div class="card card-boxless card-megamenu">
                                      <a class="link-arrow font-weight-bold" href="../../../about/life-insurance/fund-fact">Fund Fact Sheet <i class="fa fa-chevron-right d-inline-block p-0"></i></a>
                                    <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                                  </div>
                                </div>
                                <div class="col-md-4">
                                  <div class="card card-boxless card-megamenu">
                                      <a class="link-arrow font-weight-bold" href="../../../about/life-insurance/financial-report">Financial Report <i class="fa fa-chevron-right d-inline-block p-0"></i></a>
                                    <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                                  </div>
                                </div>
                                <div class="col-md-4">
                                  <div class="card card-boxless card-megamenu">
                                      <a class="link-arrow font-weight-bold" href="../../../">Partnership <i class="fa fa-chevron-right d-inline-block p-0"></i></a>
                                    <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-4">
                            <div class="card card-solution card-megamenus">
                              <div class="card-img-top" style="background: url(/template_avrist/assets/img/sample/bg_rectangle-dark-2.jpg) no-repeat center; background-size: cover;"></div>
                              <div class="card-body">
                                <p class="card-label">Life insurance</p>
                                <h5 class="card-title text-truncate">Avrist Prime Whole Life</h5>
                                <p class="card-text">Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec.</p>
                                <!-- max 100 character -->
                                <a href="#" class="card-link">Lihat detail</a>
                              </div>
                            </div>
                          </div>
                        </div>
                      </ul>
                    </li>
                    <li class="menus-megamenus general-insurance">
                      <a class="nav-link-menus" href="../../../about/general-insurance">General Insurance <i class="fa fa-angle-down"></i></a>
                      <ul class="mega-menus">
                        <div class="row">
                          <div class="col-md-8">
                            <p class="megamenu-information">Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. </p>
                            <a href="../../../about/general-insurance" class="d-block text-link link-arrow">PELAJARI TENTANG GENERAL INSURANCE <i class="fa fa-angle-right"></i></a>
                            <div class="content-megamenu mt-4">
                              <div class="row">
                                <div class="col-md-4">
                                  <div class="card card-boxless card-megamenu">
                                    <a href="../../../about/life-insurance/pt-avrist" class="link-arrow font-weight-bold">PT.Avrist General Insurance <i class="fa fa-chevron-right d-inline-block p-0"></i></a>
                                    <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                                  </div>
                                </div>
                                <div class="col-md-4">
                                  <div class="card card-boxless card-megamenu">
                                    <a class="link-arrow font-weight-bold" href="../../../about/life-insurance/news">News & Events <i class="fa fa-chevron-right d-inline-block p-0"></i></a>
                                    <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                                  </div>
                                </div>
                                <div class="col-md-4">
                                  <div class="card card-boxless card-megamenu">
                                    <a class="link-arrow font-weight-bold" href="../../../about/life-insurance/csr">CSR <i class="fa fa-chevron-right d-inline-block p-0"></i></a>
                                    <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                                  </div>
                                </div>
                                <div class="col-md-4">
                                  <div class="card card-boxless card-megamenu">
                                    <a class="link-arrow font-weight-bold" href="../../../about/life-insurance/career">Career <i class="fa fa-chevron-right d-inline-block p-0"></i></a>
                                    <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                                  </div>
                                </div>
                                <div class="col-md-4">
                                  <div class="card card-boxless card-megamenu">
                                    <a class="link-arrow font-weight-bold" href="../../../about/life-insurance/awards">Awards <i class="fa fa-chevron-right d-inline-block p-0"></i></a>
                                    <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                                  </div>
                                </div>
                                <div class="col-md-4">
                                  <div class="card card-boxless card-megamenu">
                                    <a class="link-arrow font-weight-bold" href="../../../about/life-insurance/financial-report">Financial Report <i class="fa fa-chevron-right d-inline-block p-0"></i></a>
                                    <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-4">
                            <div class="card card-solution card-megamenus">
                              <div class="card-img-top" style="background: url(/template_avrist/assets/img/sample/bg_rectangle-dark-2.jpg) no-repeat center; background-size: cover;"></div>
                              <div class="card-body">
                                <p class="card-label">Life insurance</p>
                                <h5 class="card-title text-truncate">Avrist Prime Whole Life</h5>
                                <p class="card-text">Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec.</p>
                                <!-- max 100 character -->
                                <a href="#" class="card-link">Lihat detail</a>
                              </div>
                            </div>
                          </div>
                        </div>
                      </ul>
                    </li>
                    <li class="menus-megamenus asset-management">
                      <a class="nav-link-menus" href="../../../about/asset-management">Asset Management<i class="fa fa-angle-down"></i></a>
                      <ul class="mega-menus">
                        <div class="row">
                          <div class="col-md-8">
                            <p class="megamenu-information">Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. </p>
                            <a href="../../../about/asset-management" class="d-block text-link link-arrow">PELAJARI TENTANG ASSET MANAGEMENT</a>
                            <div class="content-megamenu mt-4">
                              <div class="row">
                                <div class="col-md-4">
                                  <div class="card card-boxless card-megamenu">
                                    <a href="../../../about/asset-management/pt-avrist" class="link-arrow font-weight-bold">PT.Avrist Asset Management <i class="fa fa-chevron-right d-inline-block p-0"></i></a>
                                    <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                                  </div>
                                </div>
                                <div class="col-md-4">
                                  <div class="card card-boxless card-megamenu">
                                    <a href="../../../about/asset-management/news" class="link-arrow font-weight-bold">News & Events <i class="fa fa-chevron-right d-inline-block p-0"></i></a>
                                    <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                                  </div>
                                </div>
                                <div class="col-md-4">
                                  <div class="card card-boxless card-megamenu">
                                    <a href="../../../about/asset-management/csr" class="link-arrow font-weight-bold">CSR <i class="fa fa-chevron-right d-inline-block p-0"></i></a>
                                    <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                                  </div>
                                </div>
                                <div class="col-md-4">
                                  <div class="card card-boxless card-megamenu">
                                    <a href="../../../about/asset-management/career" class="link-arrow font-weight-bold">Career <i class="fa fa-chevron-right d-inline-block p-0"></i></a>
                                    <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                                  </div>
                                </div>
                                <div class="col-md-4">
                                  <div class="card card-boxless card-megamenu">
                                    <a href="../../../about/asset-management/awards" class="link-arrow font-weight-bold">Awards <i class="fa fa-chevron-right d-inline-block p-0"></i></a>
                                    <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                                  </div>
                                </div>
                                <div class="col-md-4">
                                  <div class="card card-boxless card-megamenu">
                                    <a href="../../../about/asset-management/adendum" class="link-arrow font-weight-bold">Adendum KIK <i class="fa fa-chevron-right d-inline-block p-0"></i></a>
                                    <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                                  </div>
                                </div>
                                <div class="col-md-4">
                                  <div class="card card-boxless card-megamenu">
                                    <a href="../../../about/asset-management/market-update" class="link-arrow font-weight-bold">Market Update <i class="fa fa-chevron-right d-inline-block p-0"></i></a>
                                    <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                                  </div>
                                </div>
                                <div class="col-md-4">
                                  <div class="card card-boxless card-megamenu">
                                    <a href="../../../about/asset-management/fund-fact" class="link-arrow font-weight-bold">Fund Fact Sheet <i class="fa fa-chevron-right d-inline-block p-0"></i></a>
                                    <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                                  </div>
                                </div>
                                <div class="col-md-4">
                                  <div class="card card-boxless card-megamenu">
                                    <a href="../../../about/asset-management/prospectus" class="link-arrow font-weight-bold">Prospectus <i class="fa fa-chevron-right d-inline-block p-0"></i></a>
                                    <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                                  </div>
                                </div>
                                <div class="col-md-4">
                                  <div class="card card-boxless card-megamenu">
                                    <a href="../../../about/asset-management/financial-report" class="link-arrow font-weight-bold">Financial Report <i class="fa fa-chevron-right d-inline-block p-0"></i></a>
                                    <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-4">
                            <div class="card card-solution card-megamenus">
                              <div class="card-img-top" style="background: url(/template_avrist/assets/img/sample/bg_rectangle-dark-2.jpg) no-repeat center; background-size: cover;"></div>
                              <div class="card-body">
                                <p class="card-label">Life insurance</p>
                                <h5 class="card-title text-truncate">Avrist Prime Whole Life</h5>
                                <p class="card-text">Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec.</p>
                                <!-- max 100 character -->
                                <a href="#" class="card-link">Lihat detail</a>
                              </div>
                            </div>
                          </div>
                        </div>
                      </ul>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </li>
        <!-- <li class="nav-item" id="navAbout">
          <a class="nav-link animation-underline" href="../../../about">Tentang</a>
        </li> -->
        <li class="nav-item" id="navTentang">
          <div class="dropdown clearfix">
            <a href="#" class="nav-link animation-underline float-left">
              Bantuan
            </a>
            <a href="#" class="float-right" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <i class="fal fa-angle-down text-primary" id="angle-dropdown"></i>
            </a>
            <div class="dropdown-menu animated fadeIn dropdown-menu-general dropdown-menu-3">
              <!-- <a class="dropdown-item" id="navLifeInsurance">Life Insurance</a>
              <a class="dropdown-item" id="navGeneralInsurance">General Insurance</a>
              <a class="dropdown-item" id="navAssetManagement">Asset Management</a> -->
              <div class="blocks-menus">
                <div class="container">
                  <ul class="primary-menus">
                    <li class="menus-megamenus">
                      <a class="nav-link-menus" href="../../../life-insurance">Life Insurance <i class="fa fa-angle-down"></i></a>
                      <ul class="mega-menus">
                        <div class="row">
                          <div class="col-md-8 scroll-bar">
                            <p class="megamenu-information">Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. </p>
                            <a href="../../../life-insurance/personal/life" class="d-block text-link link-arrow">PELAJARI TENTANG LIFE INSURANCE <i class="fa fa-chevron-right d-inline-block p-0"></i></a>
                            <div class="content-megamenu mt-4">
                              <div class="row">
                                <div class="col-md-4">
                                  <div class="card card-boxless card-megamenu">
                                      <a class="link-arrow font-weight-bold" href="../../../">PT. Avrist Life Assurance <i class="fa fa-chevron-right d-inline-block p-0"></i></a>
                                    <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                                  </div>
                                </div>
                                <div class="col-md-4">
                                  <div class="card card-boxless card-megamenu">
                                      <a class="link-arrow font-weight-bold" href="../../../bantuan/lokasi-kami">Lokasi Kami <i class="fa fa-chevron-right d-inline-block p-0"></i></a>
                                    <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                                  </div>
                                </div>
                                <div class="col-md-4">
                                  <div class="card card-boxless card-megamenu">
                                      <a class="link-arrow font-weight-bold" href="../../../bantuan/lokasi-rs">Lokasi RS Rekanan <i class="fa fa-chevron-right d-inline-block p-0"></i></a>
                                    <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                                  </div>
                                </div>
                                <div class="col-md-4">
                                  <div class="card card-boxless card-megamenu">
                                      <a class="link-arrow font-weight-bold" href="../../../bantuan/e-claim">e-Claim <i class="fa fa-chevron-right d-inline-block p-0"></i></a>
                                    <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                                  </div>
                                </div>
                                <div class="col-md-4">
                                  <div class="card card-boxless card-megamenu">
                                      <a class="link-arrow font-weight-bold" href="../../../bantuan/layanan-nasabah">Layanan Nasabah <i class="fa fa-chevron-right d-inline-block p-0"></i></a>
                                    <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                                  </div>
                                </div>
                                <div class="col-md-4">
                                  <div class="card card-boxless card-megamenu">
                                      <a class="link-arrow font-weight-bold" href="../../../bantuan/faq">FAQ <i class="fa fa-chevron-right d-inline-block p-0"></i></a>
                                    <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                                  </div>
                                </div>
                                <div class="col-md-4">
                                  <div class="card card-boxless card-megamenu">
                                      <a class="link-arrow font-weight-bold" href="../../../bantuan/contact-us">Contact Us <i class="fa fa-chevron-right d-inline-block p-0"></i></a>
                                    <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-4">
                            <div class="card card-solution card-megamenus">
                              <div class="card-img-top" style="background: url(/template_avrist/assets/img/sample/bg_rectangle-dark-2.jpg) no-repeat center; background-size: cover;"></div>
                              <div class="card-body">
                                <p class="card-label">Life insurance</p>
                                <h5 class="card-title text-truncate">Avrist Prime Whole Life</h5>
                                <p class="card-text">Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec.</p>
                                <!-- max 100 character -->
                                <a href="#" class="card-link">Lihat detail</a>
                              </div>
                            </div>
                          </div>
                        </div>
                      </ul>
                    </li>
                    <li class="menus-megamenus general-insurance">
                      <a class="nav-link-menus" href="../../../general-insurance">General Insurance <i class="fa fa-angle-down"></i></a>
                      <ul class="mega-menus">
                        <div class="row">
                          <div class="col-md-8">
                            <p class="megamenu-information">Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. </p>
                            <a href="../../../general-insurance" class="d-block text-link">PELAJARI TENTANG GENERAL INSURANCE <i class="fa fa-angle-right"></i></a>
                            <div class="content-megamenu mt-4">
                              <div class="row">
                                <div class="col-md-4">
                                  <div class="card card-boxless card-megamenu">
                                    <a href="../../../" class="link-arrow font-weight-bold">PT.Avrist General Insurance <i class="fa fa-chevron-right d-inline-block p-0"></i></a>
                                    <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                                  </div>
                                </div>
                                <div class="col-md-4">
                                  <div class="card card-boxless card-megamenu">
                                    <a class="link-arrow font-weight-bold" href="../../../">Lokasi Kami <i class="fa fa-chevron-right d-inline-block p-0"></i></a>
                                    <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                                  </div>
                                </div>
                                <div class="col-md-4">
                                  <div class="card card-boxless card-megamenu">
                                    <a class="link-arrow font-weight-bold" href="../../../">Lokasi Bengkel Rekanan <i class="fa fa-chevron-right d-inline-block p-0"></i></a>
                                    <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                                  </div>
                                </div>
                                <div class="col-md-4">
                                  <div class="card card-boxless card-megamenu">
                                    <a class="link-arrow font-weight-bold" href="../../../">Claim <i class="fa fa-chevron-right d-inline-block p-0"></i></a>
                                    <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                                  </div>
                                </div>
                                <div class="col-md-4">
                                  <div class="card card-boxless card-megamenu">
                                    <a class="link-arrow font-weight-bold" href="../../../">Formulir Pembukaan <i class="fa fa-chevron-right d-inline-block p-0"></i></a>
                                    <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                                  </div>
                                </div>
                                <div class="col-md-4">
                                  <div class="card card-boxless card-megamenu">
                                    <a class="link-arrow font-weight-bold" href="../../../">FAQ <i class="fa fa-chevron-right d-inline-block p-0"></i></a>
                                    <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                                  </div>
                                </div>
                                <div class="col-md-4">
                                  <div class="card card-boxless card-megamenu">
                                    <a class="link-arrow font-weight-bold" href="../../../">Contact Us <i class="fa fa-chevron-right d-inline-block p-0"></i></a>
                                    <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-4">
                            <div class="card card-solution card-megamenus">
                              <div class="card-img-top" style="background: url(/template_avrist/assets/img/sample/bg_rectangle-dark-2.jpg) no-repeat center; background-size: cover;"></div>
                              <div class="card-body">
                                <p class="card-label">Life insurance</p>
                                <h5 class="card-title text-truncate">Avrist Prime Whole Life</h5>
                                <p class="card-text">Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec.</p>
                                <!-- max 100 character -->
                                <a href="#" class="card-link">Lihat detail</a>
                              </div>
                            </div>
                          </div>
                        </div>
                      </ul>
                    </li>
                    <li class="menus-megamenus asset-management">
                      <a class="nav-link-menus" href="../../../asset-management">Asset Management<i class="fa fa-angle-down"></i></a>
                      <ul class="mega-menus">
                        <div class="row">
                          <div class="col-md-8">
                            <p class="megamenu-information">Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. </p>
                            <a href="../../../asset-management" class="d-block text-link">PELAJARI TENTANG ASSET MANAGEMENT</a>
                            <div class="content-megamenu mt-4">
                              <div class="row">
                                <div class="col-md-4">
                                  <div class="card card-boxless card-megamenu">
                                    <a href="../../../" class="link-arrow font-weight-bold">PT.Avrist Asset Management <i class="fa fa-chevron-right d-inline-block p-0"></i></a>
                                    <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                                  </div>
                                </div>
                                <div class="col-md-4">
                                  <div class="card card-boxless card-megamenu">
                                    <a href="../../../" class="link-arrow font-weight-bold">Lokasi Kami <i class="fa fa-chevron-right d-inline-block p-0"></i></a>
                                    <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                                  </div>
                                </div>
                                <div class="col-md-4">
                                  <div class="card card-boxless card-megamenu">
                                    <a href="../../../" class="link-arrow font-weight-bold">Formulir <i class="fa fa-chevron-right d-inline-block p-0"></i></a>
                                    <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                                  </div>
                                </div>
                                <div class="col-md-4">
                                  <div class="card card-boxless card-megamenu">
                                    <a href="../../../" class="link-arrow font-weight-bold">FAQ <i class="fa fa-chevron-right d-inline-block p-0"></i></a>
                                    <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                                  </div>
                                </div>
                                <div class="col-md-4">
                                  <div class="card card-boxless card-megamenu">
                                    <a href="../../../" class="link-arrow font-weight-bold">Contact Us <i class="fa fa-chevron-right d-inline-block p-0"></i></a>
                                    <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-4">
                            <div class="card card-solution card-megamenus">
                              <div class="card-img-top" style="background: url(/template_avrist/assets/img/sample/bg_rectangle-dark-2.jpg) no-repeat center; background-size: cover;"></div>
                              <div class="card-body">
                                <p class="card-label">Life insurance</p>
                                <h5 class="card-title text-truncate">Avrist Prime Whole Life</h5>
                                <p class="card-text">Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec.</p>
                                <!-- max 100 character -->
                                <a href="#" class="card-link">Lihat detail</a>
                              </div>
                            </div>
                          </div>
                        </div>
                      </ul>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </li>
      </ul>
      <ul class="navbar-nav ml-auto">
        <li class="nav-item">
          <div class="position-relative">
            <input class="form-control nav-search form-control-sm" type="search" placeholder="Search" id="nav-search"/>
            <i class="fal fa-search search-icon font-size-sm"></i>
          </div>
        </li>
      </ul>
    </div>

    <div class="navbar-right">
      <ul class="navbar-nav">
        <li class="nav-item dropdown">
          <a class="nav-link btn btn-primary text-white" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Login
          </a>
          <div class="dropdown-menu dropdown-login" aria-labelledby="navbarDropdown">
            <div class="login-top">
              <form class="needs-validation" novalidate>
                <div class="row">
                  <div class="col-md-12">
                    <div class="heading">
                      <h4>Login</h4>
                      <p>Login to customer dashboard</p>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="">E-mail</label>
                      <input type="email" class="form-control" id="email-login" placeholder="E-mail" value="" required>
                      <div class="invalid-feedback">
                        Please enter email
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="">Last name</label>
                      <input type="password" class="form-control" id="password-login" placeholder="Password" value="" required>
                      <div class="invalid-feedback">
                        Please enter password
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12 mb-4">
                    <a href="#" class="float-right forgot-pass">Lupa Password</a>
                  </div>
                  <div class="col-12">
                    <button class="btn btn-primary btn-block" type="submit" style="background-color: #432862 !important;">Login</button>
                  </div>
                </div>
              </form>
            </div>
            <a href="#" target="_blank">
              <div class="login-bottom">
                <img src="/template_avrist/assets/img/icon/ic_login.png" class="login-icon" alt="Login Icon">
                <div class="text-group">
                  <a href="#">Belum punya Akun? <br> Daftar Sekarang</a>
                </div>
              </div>
            </a>
          </div>
        </li>
      </ul>
    </div>
    <a class="navbar-brand" href="../../../">
      <img src="/template_avrist/assets/img/brand/logo_avrist.png" class="logo" alt="Logo">
    </a>

  </div>

  <div class="navbar-slide">
    <div class="navbar-slide-close">
      <span class="icon-bar icon-bar-1"></span>
      <span class="icon-bar icon-bar-2"></span>
      <span class="icon-bar icon-bar-3"></span>
    </div>
    <div class="content">
      <ul class="nav-slide-list">
      <li class="nav-slide-item" id="navAbout">
          <a class="nav-link" href="../../../">
            <span>Beranda</span>
          </a>
        </li>
        <li class="nav-slide-item dropdown" id="navSolusi">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Solusi
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="text-dark dropdown-item" id="nav-slide_product-1">Life Insurance</a>
            <a class="text-dark dropdown-item" id="nav-slide_product-2">General Insurance</a>
            <a class="text-dark dropdown-item" id="nav-slide_product-3">Asset Management</a>
          </div>
        </li>
        <li class="nav-slide-item dropdown" id="navTentang">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Tentang
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="text-dark dropdown-item" id="nav-slide_product-tentang-1">Life Insurance</a>
            <a class="text-dark dropdown-item" id="nav-slide_product-tentang-2">General Insurance</a>
            <a class="text-dark dropdown-item" id="nav-slide_product-tentang-3">Asset Management</a>
          </div>
        </li>
        <li class="nav-slide-item dropdown" id="navBantuan">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Bantuan
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="text-dark dropdown-item" id="nav-slide_product-bantuan-1">Life Insurance</a>
            <a class="text-dark dropdown-item" id="nav-slide_product-bantuan-2">General Insurance</a>
            <a class="text-dark dropdown-item" id="nav-slide_product-bantuan-3">Asset Management</a>
          </div>
        </li>
      </ul>
      <a href="../../../login" class="btn btn-primary">Login</a>
    </div>
  </div>

  <div class="product-slide" id="product-slide-1">
    <label class="product-title-slide" id="product-back-1"><i class="fa fa-angle-left"></i> Life Insurance</label>
    <p class="navbar-slide-information">Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. </p>
    <a href="#" class="btn-link">Pelajari tentang life insurance <i class="fa fa-angle-right"></i></a>
    <ul class="mega-menu-down">
      <li><a href="../../../">Life <i class="fa fa-angle-right"></i></a></li>
      <li><a href="../../../">Life Syariah <i class="fa fa-angle-right"></i></a></li>
      <li><a href="../../../">Accident & Health<i class="fa fa-angle-right"></i></a></li>
      <li><a href="../../../">Education <i class="fa fa-angle-right"></i></a></li>
      <li><a href="../../../">Employee <i class="fa fa-angle-right"></i></a></li>
      <li><a href="../../../">Retirement<i class="fa fa-angle-right"></i></a></li>
    </ul>
  </div>

  <div class="product-slide" id="product-slide-tentang-1">
    <label class="product-title-slide" id="product-back-tentang-1"><i class="fa fa-angle-left"></i> Life Insurance</label>
    <p class="navbar-slide-information">Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. </p>
    <a href="#" class="btn-link">Pelajari tentang life insurance <i class="fa fa-angle-right"></i></a>
    <ul class="mega-menu-down">
      <li><a href="../../../">PT.Avrist Life Assurance <i class="fa fa-angle-right"></i></a></li>
      <li><a href="../../../">News & Event <i class="fa fa-angle-right"></i></a></li>
      <li><a href="../../../">CSR <i class="fa fa-angle-right"></i></a></li>
      <li><a href="../../../">Career <i class="fa fa-angle-right"></i></a></li>
      <li><a href="../../../">Awards <i class="fa fa-angle-right"></i></a></li>
      <li><a href="../../../">Fund Fact Sheet <i class="fa fa-angle-right"></i></a></li>
      <li><a href="../../../">Financial Report <i class="fa fa-angle-right"></i></a></li>
      <li><a href="../../../">Partnership <i class="fa fa-angle-right"></i></a></li>
    </ul>
  </div>

  <div class="product-slide" id="product-slide-bantuan-1">
    <label class="product-title-slide" id="product-back-bantuan-1"><i class="fa fa-angle-left"></i> Life Insurance</label>
    <p class="navbar-slide-information">Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. </p>
    <a href="#" class="btn-link">Pelajari tentang life insurance <i class="fa fa-angle-right"></i></a>
    <ul class="mega-menu-down">
      <li><a href="../../../">PT.Avrist Life Assurance <i class="fa fa-angle-right"></i></a></li>
      <li><a href="../../../">Lokasi kami <i class="fa fa-angle-right"></i></a></li>
      <li><a href="../../../">Lokasi RS Rekanan <i class="fa fa-angle-right"></i></a></li>
      <li><a href="../../../">e-Claim <i class="fa fa-angle-right"></i></a></li>
      <li><a href="../../../">Layanan Nasabah <i class="fa fa-angle-right"></i></a></li>
      <li><a href="../../../">FAQ <i class="fa fa-angle-right"></i></a></li>
      <li><a href="../../../">Contact Us <i class="fa fa-angle-right"></i></a></li>
    </ul>
  </div>

  <div class="product-slide" id="product-slide-2">
    <label class="product-title-slide" id="product-back-2"><i class="fa fa-angle-left"></i> General Insurance</label>
    <p class="navbar-slide-information">Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. </p>
    <a href="#" class="btn-link">Pelajari tentang life insurance <i class="fa fa-angle-right"></i></a>
    <ul class="mega-menu-down">
      <li><a href="../../../general-insurance/avrist-oto">Avrist Oto <i class="fa fa-angle-right"></i></a></li>
      <li><a href="../../../general-insurance/property">Property <i class="fa fa-angle-right"></i></a></li>
      <li><a href="../../../general-insurance/personal-accident">Personal Accident <i class="fa fa-angle-right"></i></a></li>
      <li><a href="../../../general-insurance/engineering">Engineering <i class="fa fa-angle-right"></i></a></li>
      <li><a href="../../../general-insurance/avrist-oto">Casualty Insurance <i class="fa fa-angle-right"></i></a></li>
      <li><a href="../../../general-insurance/micro-insurance">Micro Insurance <i class="fa fa-angle-right"></i></a></li>
      <li><a href="../../../general-insurance/glasses-protection">Glasses Protection <i class="fa fa-angle-right"></i></a></li>
      <li><a href="../../../general-insurance/marine-cargo">Marine Cargo <i class="fa fa-angle-right"></i></a></li>
    </ul>
  </div>

  <div class="product-slide" id="product-slide-tentang-2">
    <label class="product-title-slide" id="product-back-tentang-2"><i class="fa fa-angle-left"></i> General Insurance</label>
    <p class="navbar-slide-information">Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. </p>
    <a href="#" class="btn-link">Pelajari tentang general insurance <i class="fa fa-angle-right"></i></a>
    <ul class="mega-menu-down">
      <li><a href="../../../">PT.Avrist General Insurance <i class="fa fa-angle-right"></i></a></li>
      <li><a href="../../../">News & Event <i class="fa fa-angle-right"></i></a></li>
      <li><a href="../../../">CSR <i class="fa fa-angle-right"></i></a></li>
      <li><a href="../../../">Career <i class="fa fa-angle-right"></i></a></li>
      <li><a href="../../../">Awards <i class="fa fa-angle-right"></i></a></li>
      <li><a href="../../../">Financial Report <i class="fa fa-angle-right"></i></a></li>
    </ul>
  </div>

  <div class="product-slide" id="product-slide-bantuan-2">
    <label class="product-title-slide" id="product-back-bantuan-2"><i class="fa fa-angle-left"></i> General Insurance</label>
    <p class="navbar-slide-information">Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. </p>
    <a href="#" class="btn-link">Pelajari tentang general insurance <i class="fa fa-angle-right"></i></a>
    <ul class="mega-menu-down">
      <li><a href="../../../">PT.Avrist General Insurance <i class="fa fa-angle-right"></i></a></li>
      <li><a href="../../../">Lokasi Kami <i class="fa fa-angle-right"></i></a></li>
      <li><a href="../../../">Lokasi Bengkel Rekanan <i class="fa fa-angle-right"></i></a></li>
      <li><a href="../../../">Claim <i class="fa fa-angle-right"></i></a></li>
      <li><a href="../../../">Formulir Pembukaan <i class="fa fa-angle-right"></i></a></li>
      <li><a href="../../../">FAQ <i class="fa fa-angle-right"></i></a></li>
      <li><a href="../../../">Contact Us <i class="fa fa-angle-right"></i></a></li>
    </ul>
  </div>

  <div class="product-slide" id="product-slide-3">
    <label class="product-title-slide" id="product-back-3"><i class="fa fa-angle-left"></i> Asset Management</label>
    <p class="navbar-slide-information">Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. </p>
    <a href="#" class="btn-link">Pelajari tentang life insurance <i class="fa fa-angle-right"></i></a>
    <ul class="mega-menu-down">
      <li><a href="../../../">Life <i class="fa fa-angle-right"></i></a></li>
      <li><a href="../../../">Life Syariah <i class="fa fa-angle-right"></i></a></li>
      <li><a href="../../../">Accident & Health<i class="fa fa-angle-right"></i></a></li>
      <li><a href="../../../">Education <i class="fa fa-angle-right"></i></a></li>
      <li><a href="../../../">Employee <i class="fa fa-angle-right"></i></a></li>
      <li><a href="../../../">Retirement<i class="fa fa-angle-right"></i></a></li>
    </ul>
  </div>

  <div class="product-slide" id="product-slide-tentang-3">
    <label class="product-title-slide" id="product-back-tentang-3"><i class="fa fa-angle-left"></i> Asset Management</label>
    <p class="navbar-slide-information">Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. </p>
    <a href="#" class="btn-link">Pelajari tentang general insurance <i class="fa fa-angle-right"></i></a>
    <ul class="mega-menu-down">
      <li><a href="../../../">PT.Avrist Asset Management <i class="fa fa-angle-right"></i></a></li>
      <li><a href="../../../">News & Event <i class="fa fa-angle-right"></i></a></li>
      <li><a href="../../../">CSR <i class="fa fa-angle-right"></i></a></li>
      <li><a href="../../../">Career <i class="fa fa-angle-right"></i></a></li>
      <li><a href="../../../">Awards <i class="fa fa-angle-right"></i></a></li>
      <li><a href="../../../">Adendum KIK <i class="fa fa-angle-right"></i></a></li>
      <li><a href="../../../">Market Update <i class="fa fa-angle-right"></i></a></li>
      <li><a href="../../../">Fund Fact Sheet <i class="fa fa-angle-right"></i></a></li>
      <li><a href="../../../">Prospectus <i class="fa fa-angle-right"></i></a></li>
      <li><a href="../../../">Financial Report <i class="fa fa-angle-right"></i></a></li>
    </ul>
  </div>

  <div class="product-slide" id="product-slide-bantuan-3">
    <label class="product-title-slide" id="product-back-bantuan-3"><i class="fa fa-angle-left"></i> Asset Management</label>
    <p class="navbar-slide-information">Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. </p>
    <a href="#" class="btn-link">Pelajari tentang general insurance <i class="fa fa-angle-right"></i></a>
    <ul class="mega-menu-down">
      <li><a href="../../../">PT.Avrist Asset Management <i class="fa fa-angle-right"></i></a></li>
      <li><a href="../../../">Lokasi Kami <i class="fa fa-angle-right"></i></a></li>
      <li><a href="../../../">Formulir <i class="fa fa-angle-right"></i></a></li>
      <li><a href="../../../">FAQ <i class="fa fa-angle-right"></i></a></li>
      <li><a href="../../../">Contact Us <i class="fa fa-angle-right"></i></a></li>
    </ul>
  </div>

  <div class="product-slide" id="product-slide-1">
    <label class="product-title-slide" id="product-back-1"><i class="fa fa-angle-left"></i> General Insurance</label>
    <p class="navbar-slide-information">Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. </p>
    <a href="#" class="btn-link">Pelajari tentang life insurance <i class="fa fa-angle-right"></i></a>
    <ul class="mega-menu-down">
      <li><a href="../../../">Life <i class="fa fa-angle-right"></i></a></li>
      <li><a href="../../../">Life Syariah <i class="fa fa-angle-right"></i></a></li>
      <li><a href="../../../">Accident & Health<i class="fa fa-angle-right"></i></a></li>
      <li><a href="../../../">Education <i class="fa fa-angle-right"></i></a></li>
      <li><a href="../../../">Employee <i class="fa fa-angle-right"></i></a></li>
      <li><a href="../../../">Retirement<i class="fa fa-angle-right"></i></a></li>
    </ul>
  </div>

  <div class="dropdown-menu mega-menu" id="megaMenuLifeInsurance">
    <div class="container">
      <div class="row">
        <div class="col-md-9">
          <div class="megamenu-section">
            <p class="megamenu-information">Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. </p>
            <a href="../../../life-insurance" class="">PELAJARI TENTANG LIFE INSURANCE</a>
          </div>

          <div class="megamenu-section-bottom">
            <div class="row">
              <div class="col-md-4">
                <div class="card card-boxless card-megamenu">
                  <div class="link-w-arrow">
                    <a href="#">Avrist Oto</a>
                    <i class="fa fa-chevron-right"></i>
                  </div>
                  <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                </div>
              </div>
              <div class="col-md-4">
                <div class="card card-boxless card-megamenu">
                  <div class="link-w-arrow">
                    <a href="#">Property</a>
                    <i class="fa fa-chevron-right"></i>
                  </div>
                  <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                </div>
              </div>
              <div class="col-md-4">
                <div class="card card-boxless card-megamenu">
                  <div class="link-w-arrow">
                    <a href="#">Personal Accident</a>
                    <i class="fa fa-chevron-right"></i>
                  </div>
                  <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                </div>
              </div>
              <div class="col-md-4">
                <div class="card card-boxless card-megamenu">
                  <div class="link-w-arrow">
                    <a href="#">Engineering</a>
                    <i class="fa fa-chevron-right"></i>
                  </div>
                  <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                </div>
              </div>
              <div class="col-md-4">
                <div class="card card-boxless card-megamenu">
                  <div class="link-w-arrow">
                    <a href="#">Casualty Insurance</a>
                    <i class="fa fa-chevron-right"></i>
                  </div>
                  <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                </div>
              </div>
              <div class="col-md-4">
                <div class="card card-boxless card-megamenu">
                  <div class="link-w-arrow">
                    <a href="#">Micro Insurance</a>
                    <i class="fa fa-chevron-right"></i>
                  </div>
                  <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                </div>
              </div>
              <div class="col-md-4">
                <div class="card card-boxless card-megamenu">
                  <div class="link-w-arrow">
                    <a href="#">Glasses Protection</a>
                    <i class="fa fa-chevron-right"></i>
                  </div>
                  <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                </div>
              </div>
              <div class="col-md-4">
                <div class="card card-boxless card-megamenu">
                  <div class="link-w-arrow">
                    <a href="#">Marine Cargo</a>
                    <i class="fa fa-chevron-right"></i>
                  </div>
                  <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-3">
          <div class="card card-megamenu-right" style="background: url(/template_avrist/assets/img/common/Mask.png) no-repeat center; background-size: cover; min-height: 330px;">
            <img src="/template_avrist/assets/img/common/ic_ilustrasi_home_menus.png" class="img-fluid">
            <p class="text-white">Butuh solusi ideal untuk bisnis/personal?</p>
            <a href="../../../get-solution/personal" class="btn btn-secondary btn-block">Get Solution</a>
          </div>
        </div>
      </div>
      
    </div>
  </div>

  <div class="dropdown-menu mega-menu" id="megaMenuGeneralInsurance">
    <div class="container">
      <div class="row">
        <div class="col-md-9">
          <div class="megamenu-section">
            <p class="megamenu-information">Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. </p>
            <a href="../../../general-insurance" class="">PELAJARI TENTANG GENERAL INSURANCE <i class="fa fa-angle-right"></i></a>
          </div>

          <div class="megamenu-section-bottom">
            <div class="heading">
              <h2>Personal</h2>
            </div>
            <div class="row">
              <div class="col-md-4">
                <div class="card card-boxless card-megamenu">
                  <div class="link-w-arrow">
                    <a href="#">Life</a>
                    <i class="fa fa-chevron-right"></i>
                  </div>
                  <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                </div>
              </div>
              <div class="col-md-4">
                <div class="card card-boxless card-megamenu">
                  <div class="link-w-arrow">
                    <a href="#">Life Syariah</a>
                    <i class="fa fa-chevron-right"></i>
                  </div>
                  <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                </div>
              </div>
              <div class="col-md-4">
                <div class="card card-boxless card-megamenu">
                  <div class="link-w-arrow">
                    <a href="#">Accident & Health</a>
                    <i class="fa fa-chevron-right"></i>
                  </div>
                  <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                </div>
              </div>
              <div class="col-md-4">
                <div class="card card-boxless card-megamenu">
                  <div class="link-w-arrow">
                    <a href="#">Education</a>
                    <i class="fa fa-chevron-right"></i>
                  </div>
                  <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                </div>
              </div>
            </div>
          </div>

          <div class="megamenu-section-bottom">
            <div class="heading">
              <h2>Business</h2>
            </div>
            <div class="row">
              <div class="col-md-4">
                <div class="card card-boxless card-megamenu">
                  <div class="link-w-arrow">
                    <a href="#">Life</a>
                    <i class="fa fa-chevron-right"></i>
                  </div>
                  <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                </div>
              </div>
              <div class="col-md-4">
                <div class="card card-boxless card-megamenu">
                  <div class="link-w-arrow">
                    <a href="#">Life Syariah</a>
                    <i class="fa fa-chevron-right"></i>
                  </div>
                  <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                </div>
              </div>
              <div class="col-md-4">
                <div class="card card-boxless card-megamenu">
                  <div class="link-w-arrow">
                    <a href="#">Accident & Health</a>
                    <i class="fa fa-chevron-right"></i>
                  </div>
                  <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                </div>
              </div>
              <div class="col-md-4">
                <div class="card card-boxless card-megamenu">
                  <div class="link-w-arrow">
                    <a href="#">Education</a>
                    <i class="fa fa-chevron-right"></i>
                  </div>
                  <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                </div>
              </div>
            </div>
          </div>

        </div>
        <div class="col-md-3">
          <div class="card card-megamenu-right" style="background: url(/template_avrist/assets/img/common/Mask.png) no-repeat center; background-size: cover; min-height: 330px;">
            <img src="/template_avrist/assets/img/common/ic_ilustrasi_avram_menus.png" class="img-fluid">
            <p class="text-white">Ingin tahu produk investasi bagi Anda?</p>
            <a href="../../../get-solution/personal" class="btn btn-secondary btn-block">Get Solution</a>
          </div>
        </div>
      </div>
      
    </div>
  </div>

  <div class="dropdown-menu mega-menu" id="megaMenuAssetManagement">
    <div class="container">
      <div class="row">
        <div class="col-md-9">
          <div class="megamenu-section">
            <p class="megamenu-information">Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. </p>
            <a href="../../../asset-management" class="">PELAJARI TENTANG ASSET MANAGEMENT</a>
          </div>

          <div class="megamenu-section-bottom">
            <div class="heading">
              <h2>Personal</h2>
            </div>
            <div class="row">
              <div class="col-md-4">
                <div class="card card-boxless card-megamenu">
                  <div class="link-w-arrow">
                    <a href="#">Life</a>
                    <i class="fa fa-chevron-right"></i>
                  </div>
                  <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                </div>
              </div>
              <div class="col-md-4">
                <div class="card card-boxless card-megamenu">
                  <div class="link-w-arrow">
                    <a href="#">Life Syariah</a>
                    <i class="fa fa-chevron-right"></i>
                  </div>
                  <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                </div>
              </div>
              <div class="col-md-4">
                <div class="card card-boxless card-megamenu">
                  <div class="link-w-arrow">
                    <a href="#">Accident & Health</a>
                    <i class="fa fa-chevron-right"></i>
                  </div>
                  <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                </div>
              </div>
              <div class="col-md-4">
                <div class="card card-boxless card-megamenu">
                  <div class="link-w-arrow">
                    <a href="#">Education</a>
                    <i class="fa fa-chevron-right"></i>
                  </div>
                  <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                </div>
              </div>
            </div>
          </div>

          <div class="megamenu-section-bottom">
            <div class="heading">
              <h2>Business</h2>
            </div>
            <div class="row">
              <div class="col-md-4">
                <div class="card card-boxless card-megamenu">
                  <div class="link-w-arrow">
                    <a href="#">Life</a>
                    <i class="fa fa-chevron-right"></i>
                  </div>
                  <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                </div>
              </div>
              <div class="col-md-4">
                <div class="card card-boxless card-megamenu">
                  <div class="link-w-arrow">
                    <a href="#">Life Syariah</a>
                    <i class="fa fa-chevron-right"></i>
                  </div>
                  <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                </div>
              </div>
              <div class="col-md-4">
                <div class="card card-boxless card-megamenu">
                  <div class="link-w-arrow">
                    <a href="#">Accident & Health</a>
                    <i class="fa fa-chevron-right"></i>
                  </div>
                  <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                </div>
              </div>
              <div class="col-md-4">
                <div class="card card-boxless card-megamenu">
                  <div class="link-w-arrow">
                    <a href="#">Education</a>
                    <i class="fa fa-chevron-right"></i>
                  </div>
                  <p>Fusce vehicula dolor arcu, sit amet blandit dolor Fusce vehicula dolor</p>
                </div>
              </div>
            </div>
          </div>

        </div>
        <div class="col-md-3">
          <div class="card card-megamenu-right" style="background: url(/template_avrist/assets/img/common/Mask.png) no-repeat center; background-size: cover; min-height: 330px;">
            <img src="/template_avrist/assets/img/common/ic_ilustrasi_avram_menus.png" class="img-fluid">
            <p class="text-white">Ingin tahu produk investasi bagi Anda?</p>
            <a href="../../../get-solution/personal" class="btn btn-secondary btn-block">Get Solution</a>
          </div>
        </div>
      </div>
      
    </div>
  </div>

</nav>

<!--START: Modal Search-->
<!-- <div class="modal modal-full" tabindex="-1" role="dialog" id="modalSearch">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body p-box">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"><i class="fal fa-times"></i></span>
        </button>
        <form class="needs-validation" novalidate>
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <input type="text" class="form-control" autocomplete="off" placeholder="search" id="search" required="">
                <div class="invalid-feedback">
                  Please input date
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div> -->
<!--END: Modal Search-->


    <div class="cover cover-sm cover-responsive bg-primary cover-general">
      <div class="cover-responsive-img">
        <div class="cover-responsive-overlay"></div>
      </div>
      <div class="container position-relative">
        <img src="/template_avrist/assets/img/pattern/ic_ornamen_yellow.png" class="pattern-purple" alt="Ornamen Purple">
        <!-- <img src="/template_avrist/assets/img/pattern/ic_ornament_triangle_yellow.png" class="pattern-triangle-yellow" alt="Ornamen Triangle Yellow"> -->
        <img src="/template_avrist/assets/img/pattern/ic_ornament_circle_pink.png" class="pattern-pink" alt="Ornamen Circle Pink">
        <nav aria-label="breadcrumb" class="general-breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active"><a href="#">News & Events</a></li>
          </ol>
        </nav>
        <div class="cover-content">
          <h3 class="cover-title text-white animated fadeInUp delayp1">News & Event</h3>
          <p class="cover-subtitle text-white">Duis aute irure dolor in reprehenderit in voluptate velit esse
          cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
          proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
        </div>
      </div>
    </div>
    <img src="/template_avrist/assets/img/pattern/banner_life_solid.png" class="img-fluid" alt="Purple Wave">

    <section class="py-main section-general-news">
      <div class="container">
        <div class="filter-general">
          <div class="row">
            <div class="col-md-2 offset-md-5">
              <div class="form-group">
                <select class="custom-select" id="validationCustom04" required>
                  <option value="">Sort By</option>
                  <option value="1">One</option>
                  <option value="2">Two</option>
                  <option value="3">Three</option>
                </select>
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <select class="custom-select" id="validationCustom05" required>
                  <option value="">Filter</option>
                  <option value="1">One</option>
                  <option value="2">Two</option>
                  <option value="3">Three</option>
                </select>
              </div>
            </div>
            <div class="col-md-3">
              <input type="text" class="form-control" id="lastName" placeholder="Search" value="" required>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-6 col-md-4">
            <a href="../../../about/life-insurance/news/detail" class="card card-kebutuhan news">
              <div class="card-img">
                <img src="/template_avrist/assets/img/tentang/img_card_1.jpg" class="img-fluid">
              </div>
              <div class="card-body p-box">
                <h5 class="card-title">Info bank Unit Link Awards 2018</h5>
                <p href="#" class="link">Lihat Detail</p>
              </div>
            </a>
          </div>
          <div class="col-6 col-md-4">
            <a href="../../../about/life-insurance/news/detail" class="card card-kebutuhan news">
              <div class="card-img">
                <img src="/template_avrist/assets/img/tentang/img_card_2.jpg" class="img-fluid">
              </div>
              <div class="card-body p-box">
                <h5 class="card-title">Info bank Unit Link Awards 2018</h5>
                <p href="#" class="link">Lihat Detail</p>
              </div>
            </a>
          </div>
          <div class="col-6 col-md-4">
            <a href="../../../about/life-insurance/news/detail" class="card card-kebutuhan news">
              <div class="card-img">
                <img src="/template_avrist/assets/img/tentang/img_card_3.jpg" class="img-fluid">
              </div>
              <div class="card-body p-box">
                <h5 class="card-title">Info bank Unit Link Awards 2018</h5>
                <p href="#" class="link">Lihat Detail</p>
              </div>
            </a>
          </div>
          <div class="col-6 col-md-4">
            <a href="../../../about/life-insurance/news/detail" class="card card-kebutuhan news">
              <div class="card-img">
                <img src="/template_avrist/assets/img/tentang/img_card_1.jpg" class="img-fluid">
              </div>
              <div class="card-body p-box">
                <h5 class="card-title">Info bank Unit Link Awards 2018</h5>
                <p href="#" class="link">Lihat Detail</p>
              </div>
            </a>
          </div>
          <div class="col-6 col-md-4">
            <a href="../../../about/life-insurance/news/detail" class="card card-kebutuhan news">
              <div class="card-img">
                <img src="/template_avrist/assets/img/tentang/img_card_2.jpg" class="img-fluid">
              </div>
              <div class="card-body p-box">
                <h5 class="card-title">Info bank Unit Link Awards 2018</h5>
                <p href="#" class="link">Lihat Detail</p>
              </div>
            </a>
          </div>
          <div class="col-6 col-md-4">
            <a href="../../../about/life-insurance/news/detail" class="card card-kebutuhan news">
              <div class="card-img">
                <img src="/template_avrist/assets/img/tentang/img_card_3.jpg" class="img-fluid">
              </div>
              <div class="card-body p-box">
                <h5 class="card-title">Info bank Unit Link Awards 2018</h5>
                <p href="#" class="link">Lihat Detail</p>
              </div>
            </a>
          </div>
        </div>
      </div>
    </section>

    <?= $this->template("include/footer2.html.php"); ?>
    <?= $this->template("include/script2.html.php"); ?>

<!-- <div class="floating-btn">
	<a href="#" class="">
		<img src="/template_avrist/assets/img/icon/ic_FB_messanger.png" class="img-fluid">
	</a>
	<a href="#" class="">
		<img src="/template_avrist/assets/img/icon/ic_whatsapp.png" class="img-fluid">
	</a>
</div> -->        <!--Bootstrap 4.1.3 dependency-->
    <script src="/template_avrist/assets/plugins/jquery-3.3.1/jquery.min.js"></script>
	<script src="/template_avrist/assets/plugins/popper-1.14.0/popper.min.js"></script>
	<script src="/template_avrist/assets/plugins/bootstrap-4.1.3/js/bootstrap.min.js"></script>
	
    <!--jQuery UI-->
    <!--Has dependency on main.scss-->
    <script src="/template_avrist/assets/plugins/jquery-ui-1.12.1/jquery-ui.min.js"></script>
    
    <!--Viewport Checker-->
    <!--For viewport animation-->
    <script src="/template_avrist/assets/plugins/viewportchecker-1.8.8/viewportchecker.min.js"></script>
    
    <!--Parallax JS-->
    <!--For parallax effect-->
    <script src="/template_avrist/assets/plugins/parallax-1.5.0/parallax.min.js"></script>
    
    <!--Fancy Box-->
    <!--For lightbox-->
    <!--Has dependency on main.scss"-->
    <script src="/template_avrist/assets/plugins/fancybox-3.3.5/jquery.fancybox.min.js"></script>
    
    <!--Select2-->
    <!--For custom select-->
    <!--Has dependency on main.scss"-->
    <script src="/template_avrist/assets/plugins/select2-4.0.6/select2.min.js"></script>

    <!--DataTables-->
    <!--For advanced table-->
    <!--Has dependency on main.scss"-->
    <script src="/template_avrist/assets/plugins/datatables-1.10.16/datatables.min.js"></script>

    <!--Slick Slider-->
    <!--For advanced slider-->
    <!--Has dependency on main.scss"-->
    <script src="/template_avrist/assets/plugins/slick-1.8.1/slick.min.js"></script>

    <!--Owl Carousel-->
    <!--For custom carousel-->
    <!--Has dependency on .main.scss"-->
    <script src="/template_avrist/assets/plugins/owl-carousel-2.2.1/owl.carousel.js"></script>
    
    <!--Antikode Custom JS-->
    <!--For component based JS customized by the author-->
    <!--Some has dependency on main.scss-->
    <!-- <script src="/template_avrist/assets/plugins/antikode-custom-1.0.0/animate.js"></script> -->
    <!-- <script src="/template_avrist/assets/js/antikode-custom/navbar.js"></script> -->

    <!--Main.js-->
    <!--Main custom JS from author-->
    <script src="/template_avrist/assets/js/main.js"></script>
   
    <script>
      // Navbar active state
      $('#navAbout').addClass('active');

      // Animation on load
      document.addEventListener("DOMContentLoaded", function(event) {
        $(".loader").fadeOut('slow');
        $(".loader-wrapper").fadeOut("slow");
      //  $(".anim-1").addClass("animated fadeInLeft delayp10");
       // $(".anim-2").addClass("animated fadeInUp delayp12");
        //$(".anim-3").addClass("animated fadeInUp delayp14");  
      });

      $('.owl-insurance').owlCarousel({
        loop: false,
        margin: 7.5,
        dots: false,
        responsive:{
          0:{
            items: 1,
            margin: 20,
            dots: true
          },
          768:{
              margin: 15,
              items: 4
          }
        }
      });

       $('.owl-kebutuhan').owlCarousel({
        loop: false,
        margin: 7.5,
        dots: false,
        responsive:{
          0:{
            items: 1
          },
          768:{
              margin: 30,
              items: 3
          }
        }
      });

    </script>

  </body>
</html>
