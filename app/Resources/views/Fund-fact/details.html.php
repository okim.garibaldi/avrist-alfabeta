<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

use Pimcore\Model\Document;
use Pimcore\Model\Document\Page;

$this->extend('layout_avrist_new.html.php');?>

<div class="cover cover-sm cover-responsive <?= $bgBanner['bg'] ?> cover-general">
    <div class="cover-responsive-img">
        <div class="cover-responsive-overlay"></div>
    </div>
    <div class="container position-relative">
        <img src="/template_avrist/assets/img/pattern/ic_ornamen_yellow.png" class="pattern-purple" alt="Ornamen Purple">
        <!-- <img src="/template_avrist/assets/img/pattern/ic_ornament_triangle_yellow.png" class="pattern-triangle-yellow" alt="Ornamen Triangle Yellow"> -->
        <img src="/template_avrist/assets/img/pattern/ic_ornament_circle_pink.png" class="pattern-pink" alt="Ornamen Circle Pink">
        <nav aria-label="breadcrumb" class="general-breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item"><a href="#"><?= $aboutCompany->getName() ?></a></li>
                <li class="breadcrumb-item"><a href="#">Fund Facts</a></li>
                <li class="breadcrumb-item active"><a href="#"><?= $product->getName() ?></a></li>
            </ol>
        </nav>
        <div class="cover-content">
            <h3 class="cover-title text-white animated fadeInUp delayp1"><?= $product->getName() ?></h3>
        </div>
    </div>
</div>

<section class="py-main section-general-tentang-2">
    <div class="container">
    <?php foreach ($fundFacts as $key => $value){ ?>
        <div class="heading mb-3">
            <h2 class="font-weight-bold" style="color: #b2bbc3;"><?= $key ?></h2>
        </div>
        <div class="row">
        <?php foreach($value as $v): ?>
            <div class="col-6 col-md-3">
                <a href="<?= $v['file']->getFullPath() ?>" download class="card card-list">
                    <div class="card-body">
                        <h3><?= $v['name'] ?> <i class="fal fa-angle-right"></i></h3>
                        <p><img src="/template_avrist/assets/img/tentang/pdf.png" class="extension-file">Pdf Files | <?= number_format($v['file']->getFileSize() / 1024, 2) . ' KB' ?></p>
                        <p class="download">Download</p>
                    </div>
                </a>
            </div>
        <?php endforeach; ?>
        </div>
    <?php } ?>
    </div>
</section>