<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

use Pimcore\Model\Document;
use Pimcore\Model\Document\Page;

$this->extend('layout_avrist_new.html.php');?>

<div class="cover cover-sm cover-responsive <?= $bgBanner['bg'] ?> cover-general">
      <div class="cover-responsive-img">
        <div class="cover-responsive-overlay"></div>
      </div>
      <div class="container position-relative">
        <img src="/template_avrist/assets/img/pattern/ic_ornamen_yellow.png" class="pattern-purple" alt="Ornamen Purple">
        <!-- <img src="/template_avrist/assets/img/pattern/ic_ornament_triangle_yellow.png" class="pattern-triangle-yellow" alt="Ornamen Triangle Yellow"> -->
        <img src="/template_avrist/assets/img/pattern/ic_ornament_circle_pink.png" class="pattern-pink" alt="Ornamen Circle Pink">
        <nav aria-label="breadcrumb" class="general-breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item"><a href="#"><?= $aboutCompany->getName() ?></a></li>
            <li class="breadcrumb-item active"><a href="#">Fund Facts</a></li>
          </ol>
        </nav>
        <div class="cover-content">
          <h3 class="cover-title text-white animated fadeInUp delayp1">Fund Fact Sheet</h3>
          <div class="col-md-6 pl-0 mt-4">
            <input type="text" class="form-control transparent" id="searchBox" placeholder="Cari Product" value="">
            <div class="clearfix pl-0 mt-3" id="cari">
              <a href="#" class="btn btn-light w-150">Cari</a>
            </div>
          </div>
        </div>
      </div>
    </div>

    <section class="py-main section-general-tentang">
      <div class="container">
        <div class="row">
        <?php foreach($product as $p): ?>
          <div class="col-6 col-md-4">
            <a href="/fund-fact/<?= $p->getKey() ?>" class="card card-list">
              <div class="card-body">
                <h3 class="text-truncate"><?= $p->getName() ?><i class="fal fa-angle-right"></i></h3>
                <p><?= $p->getName() ?></p>
              </div>
            </a>
          </div>
        <?php endforeach; ?>
        </div>
      </div>
    </section>