<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

use Pimcore\Model\Document;
use Pimcore\Model\Document\Page;

$this->extend('layout_avrist_new.html.php');?>
<div class="cover cover-sm cover-responsive bg-primary cover-general">
    <div class="cover-responsive-img">
        <div class="cover-responsive-overlay"></div>
    </div>
    <div class="container position-relative">
        <img src="/template_avrist/assets/img/pattern/ic_ornamen_yellow.png" class="pattern-purple" alt="Ornamen Purple">
        <!-- <img src="/template_avrist/assets/img/pattern/ic_ornament_triangle_yellow.png" class="pattern-triangle-yellow" alt="Ornamen Triangle Yellow"> -->
        <img src="/template_avrist/assets/img/pattern/ic_ornament_circle_pink.png" class="pattern-pink" alt="Ornamen Circle Pink">
        <nav aria-label="breadcrumb" class="general-breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active"><a href="#"><?= $locationPage->getName() ?></a></li>
            </ol>
        </nav>
        <div class="cover-content">
            <h3 class="cover-title text-white animated fadeInUp delayp1"><?= $locationPage->getName() ?></h3>
            <div class="col-md-6 pl-0">
                <div class="form-group">
                    <div class="form-group">
                        <select class="custom-select" id="groupFilter" required>
                        <?php foreach($options['groups'] as $group): ?>
                            <option value="<?= $group['value'] ?>" <?php if($groupFilter == $group['value']) echo "selected" ?>><?= $group['key'] ?></option>
                        <?php endforeach; ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-6 pl-0">
                <div class="form-group">
                    <div class="form-group">
                        <select class="custom-select" id="hospitalCategory" required>
                        <?php foreach($options['hospitalCategories'] as $hospitalCategory): ?>
                            <option value="<?= $hospitalCategory['value'] ?>" <?php if($hospitalCategoryFilter == $hospitalCategory['value']) echo "selected" ?>><?= $hospitalCategory['key'] ?></option>
                        <?php endforeach; ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-6 pl-0">
                <div class="form-group">
                    <input type="text" class="form-control" id="searchBox" placeholder="Please enter keywords" value="" required>
                    <div class="invalid-feedback">
                        Please enter first name
                    </div>
                </div>
            </div>
            <div class="clearfix mt-3" id="cari">
                <a href="#" class="btn btn-light w-150">Cari</a>
            </div>
        </div>
        <div id="googleMap" style="width:100%;height:380px;"></div>
    </div>
</div>
<img src="<?= $bgBanner['banner'] ?>" class="img-fluid" alt="Purple Wave">

<section class=""></section>

<section class="py-main section-lokasi-kami bg-gray">
    <div class="container">
        <div class="row" id="cardRow">
        </div>
    </div>
</section>
