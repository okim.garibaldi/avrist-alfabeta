<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

use Pimcore\Model\Document;
use Pimcore\Model\Document\Page;

$this->extend('layout_avrist_new.html.php');?>

<div class="cover cover-sm cover-responsive <?= $bgBanner['bg'] ?> cover-general">
    <div class="cover-responsive-img">
        <div class="cover-responsive-overlay"></div>
    </div>
    <div class="container position-relative">
        <img src="/template_avrist/assets/img/pattern/ic_ornamen_yellow.png" class="pattern-purple" alt="Ornamen Purple">
        <!-- <img src="/template_avrist/assets/img/pattern/ic_ornament_triangle_yellow.png" class="pattern-triangle-yellow" alt="Ornamen Triangle Yellow"> -->
        <img src="/template_avrist/assets/img/pattern/ic_ornament_circle_pink.png" class="pattern-pink" alt="Ornamen Circle Pink">
        <nav aria-label="breadcrumb" class="general-breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active"><a href="#">Lokasi Kami</a></li>
            </ol>
        </nav>
        <div class="cover-content">
            <h3 class="cover-title text-white animated fadeInUp delayp1">Lokasi Kami</h3>
            <p class="cover-subtitle text-white animated fadeInUp delayp2">Lorem ipsum dolor sit amet consectetur adipisicing elit. Quae consequuntur ullam soluta, rem reprehenderit, error, vero eveniet commodi magnam laborum iure provident est quibusdam incidunt in consectetur temporibus id pariatur!</p>
        </div>
    </div>
</div>

<section class="py-main section-maps avram">
    <div class="container">
        <div class="card card-maps">
            <div class="row">
            <?php foreach($coordinates as $coordinate): ?>
                <div class="col-md-4 content-center">
                    <div class="card-body">
                        <h5><?= $coordinate['name'] ?></h5>
                        <p><?= $coordinate['address'] ?></p>
                        <a href="https://www.google.com/maps/search/?api=1&query=<?= $coordinate['latitude'] ?>,<?= $coordinate['longitude'] ?>" class="d-block"><i class="fas fa-directions"></i>Get Direction</a>
                        <a href="tel:<?= $coordinate['phone'] ?>" class="d-block"><i class="far fa-address-book"></i>Call <?= $coordinate['phone'] ?></a>
                        <a href="tel:<?= $coordinate['fax'] ?>" class="d-block"><i class="fas fa-fax"></i>Fax <?= $coordinate['fax'] ?></a>
                        <a data-toggle="modal" data-target="#modalAgen<?= preg_replace("/(\W|\s)/", '', $coordinate['name']) ?>" href="#" class="d-block"><i class="far fa-envelope"></i>Email</a>
                    </div>
                </div>
            <?php endforeach; ?>  
                <div class="col-md-8">
                    <div id="googleMap" class="maps-lokasi animated fadeInUp delayp2" style="width:100%;height:380px;"></div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php foreach($coordinates as $coordinate): ?>
<div class="modal modal-lokasi" tabindex="-1" role="dialog" id="modalAgen<?= preg_replace("/(\W|\s)/", '', $coordinate['name']) ?>">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body p-box">
                <img src="/template_avrist/assets/img/pattern/ic_ornamen_yellow.png" class="pattern-purple" alt="Ornamen Purple">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="heading">
                    <h2 class="title">Hubungi <span class="font-weight-bold"><?= $coordinate['name'] ?></span></h2>
                </div>
                <form class="needs-validation" novalidate action="our-location/location-message" method="POST">
                    <div class="row">
                        <input type="hidden" name="id" value="<?= $coordinate['id'] ?>">
                        <input type="hidden" name="locationName" value="<?= $coordinate['name'] ?>">
                        <input class="url-input" type="hidden" name="url">
                        <input type="hidden" name="locationEmail" value="<?= $coordinate['email'] ?>">
                        <input type="hidden" name="categoryPage" value="<?= $locationPage->getCategory() ?>">
                        <div class="col-md-12">
                            <div class="form-group">
                                <input type="text" class="form-control" name="name" placeholder="Nama" value="" required>
                                <div class="invalid-feedback">
                                    Please enter first name
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <input type="email" class="form-control" name="email" placeholder="Email" value="" required>
                                <div class="invalid-feedback">
                                    Please enter email
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <input type="number" class="form-control" name="telphone" placeholder="Phone Number" value="" required>
                                <div class="invalid-feedback">
                                    Please enter phone number
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="form-group">
                                    <select class="custom-select" name="topic" required>
                                    <?php foreach($subjectOptions as $subjectOption): ?>
                                        <option value="<?= $subjectOption['value'] ?>"><?= $subjectOption['key'] ?></option>
                                    <?php endforeach; ?>
                                    </select>
                                    <div class="invalid-feedback">
                                        Please choose a valid Subject
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <input type="text" class="form-control" name="message" placeholder="Message" value="" required>
                                <div class="invalid-feedback">
                                    Please enter message
                                </div>
                            </div>
                        </div>
                        <div class="col-12 g-recaptcha" data-sitekey="6Lf3I9UUAAAAAPDIakKwcSC4240AtpiMp6KS9tO3"></div>
                        <div class="col-12">
                            <button class="btn btn-primary" type="submit">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php endforeach; ?>