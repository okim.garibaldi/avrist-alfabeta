<!DOCTYPE html>
<html>
<head>
    <style>
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }
    </style>
</head>
<body>

<h2>Product Table</h2>

<table>
    <tr>
        <th>Name</th>
    </tr>
    <?php foreach ($this->assetQuestion as $aQ){?>
        <tr>
            <td><?php echo $aQ->getName(); ?></td>
        </tr>
    <?php } ?>
</table>

<div>
    <!-- <form action="http://general.avrist.dnatechnology.co.id/news/index"> -->
        <form action="http://157.230.240.113:8891/general/news/index">
        <input type="submit" value="Go to News" />
    </form>
</div>
<div>
    <form action="157.230.240.113:8891/product/index">
        <input type="submit" value="Go to Product" />
    </form>
</div>
</body>
</html>
