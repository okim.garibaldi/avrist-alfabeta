<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

use Pimcore\Model\Document;
use Pimcore\Model\Document\Page;

$this->extend('layout_avrist_new.html.php');?>

<div class="cover cover-sm cover-responsive cover-general <?= $bgBanner['bg'] ?>" style="background: linear-gradient(270deg, rgba(0,0,0,0.00) 0%, #579792 70%), url(/template_avrist/assets/img/common/img_banner_sample.jpg) no-repeat center; background-size: cover;">
    <div class="cover-responsive-img">
        <div class="cover-responsive-overlay"></div>
    </div>
    <div class="container position-relative">
        <img src="/template_avrist/assets/img/pattern/ic_ornamen_yellow.png" class="pattern-purple" alt="Ornamen Purple">
        <!-- <img src="/template_avrist/assets/img/pattern/ic_ornament_triangle_yellow.png" class="pattern-triangle-yellow" alt="Ornamen Triangle Yellow"> -->
        <img src="/template_avrist/assets/img/pattern/ic_ornament_circle_pink.png" class="pattern-pink" alt="Ornamen Circle Pink">
        <nav aria-label="breadcrumb" class="general-breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active"><a href="#"><?= $siteCategory->getName() ?></a></li>
            </ol>
        </nav>
        <div class="cover-content">
            <h3 class="cover-title text-white animated fadeInUp delayp1"><?= $siteCategory->getName() ?></h3>
            <p class="cover-content text-white"><?= $siteCategory->getDescription() ?></p>
        </div>
    </div>
    <!-- <a href="#" class="scroll-hint bottom-0 animated fadeInDown delayp4">
        <img src="/template_avrist/assets/img/common/arrow_down.png" alt="Arrow down hint"/>
      </a> -->
</div>

<section class="py-main section-life-insurance-1 pb-0">
    <img src="/template_avrist/assets/img/pattern/ornamen_waves_layer2_grey.png" class="pattern-grey" alt="Gray Waves">
    <img src="/template_avrist/assets/img/pattern/ic_ornamen_yellow.png" class="pattern-yellow" alt="Gray Waves">
    <div class="container">
        <div class="heading vp-fadeinup delayp2">
            <h2><?= $siteCategory->getTitle1() ?></h2>
            <p class="p-0 subtitle"><?= $siteCategory->getDescription1() ?></p>
        </div>

        <div class="content mt-5">
            <div class="row">
            <?php foreach($siteCategory->getBlock1() as $block1): ?>
                <div class="col-6 col-md-6 vp-fadeinup delayp1">
                    <a href="<?= $block1['link']->getData() ?>" class="card p-box card-insurance">
                        <div class="card-img">
                            <img src="<?= $block1['image']->getData() ?>" class="img-fluid">
                            <h5 class="card-title"><?= $block1['name']->getData() ?></h5>
                        </div>
                    </a>
                </div>
            <?php endforeach; ?>
            </div>
        </div>
    </div>
</section>

<section class="py-main section-life-insurance pb-0">
    <!-- <img src="/template_avrist/assets/img/pattern/ornamen_waves_layer2_grey.png" class="pattern-grey" alt="Gray Waves">
      <img src="/template_avrist/assets/img/pattern/ic_ornamen_yellow.png" class="pattern-yellow" alt="Gray Waves"> -->
    <div class="container">
        <div class="heading vp-fadeinup delayp2">
            <h2><?= $siteCategory->getTitle2() ?></h2>
            <p class="p-0 subtitle"><?= $siteCategory->getDescription2() ?></p>
        </div>

        <div class="content mt-5">
            <div class="row">
            <?php foreach($siteCategory->getBlock2() as $block2): ?>
                <div class="col-6 col-md-4 vp-fadeinup delayp1">
                    <a href="<?= $block2['link']->getData() ?>" class="card p-box card-insurance">
                        <div class="card-img">
                            <img src="<?= $block2['image']->getData() ?>" class="img-fluid">
                            <h5 class="card-title"><?= $block2['name']->getData() ?></h5>
                        </div>
                    </a>
                </div>
            <?php endforeach; ?>
            </div>
        </div>
    </div>
</section>

<section class="py-main section-life-insurance-2 ">
    <div class="container">
        <div class="heading vp-fadeinup delayp1">
            <h2>Harga unit</h2>
            <p class="p-0 subtitle p-0">Update tanggal <?= $latestDate->format('d F Y') ?></p>
        </div>
        <div class="content">
            <div class="filter">
                <div class="row">
                    <div class="col-md-5">
                        <div class="form-group position-relative form-search">
                            <label for="" style="color: transparent;"></label>
                            <input type="text" class="form-control" id="searchBox" placeholder="Cari dengan kata kunci" value="" required>
                            <button type="submit" class="btn-search" id="cari">
                                <i class="fal fa-search search-icon font-size-sm"></i>
                            </button>
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="">Bandingkan ditanggal</label>
                            <input type="text" class="form-control form-control-datepicker" placeholder="(DD/MM/YYYY)" id="dateFilter" required="">
                            <div class="invalid-feedback">
                                Please input date
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="">Filter</label>
                            <div class="form-group">
                                <select class="custom-select" id="productCategory" required>
                                    <option value="">Lihat Semua</option>
                                <?php foreach($productCategories as $pC): ?>
                                    <option value="<?= $pC ?>" <?php if($selectedProductCategory == $pC) echo "selected" ?>><?= $pC ?></option>
                                <?php endforeach; ?>
                                </select>
                                <div class="invalid-feedback">
                                    Please choose correctly
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="clearfix pl-0 mt-3" id="cari">
                    <a href="#" class="btn btn-light w-150">Cari</a> -->
            </div>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                        <?php foreach($productPrices as $price): ?>
                            <th scope="col">Product Name</th>
                            <th scope="col"><?= $price['previousSelectedPriceDate'] ?></th>
                            <th scope="col"><?= $price['selectedPriceDate'] ?></th>
                            <th scope="col">Status</th>
                            <th scope="col"></th>
                            <?php break; ?>
                        <?php endforeach; ?>
                        </tr>
                    </thead>
                    <tbody class="text-center">
                    <?php foreach($productPrices as $price): ?>
                        <tr>
                            <th scope="row" class="text-left"><?= $price['product'] ?></th>
                            <td><?= number_format($price['previousSelectedPrice'],2) ?></td>
                            <td><?= number_format($price['selectedPrice'],2) ?></td>
                            <td><span class="text-green">
                            <?php $diff = $price['selectedPrice'] - $price['previousSelectedPrice']; ?>
                            <?php if($diff < 0): ?>
                                <i class="fas fa-arrow-down mr-1">
                            <?php elseif($diff >= 0): ?>
                                <i class="fas fa-arrow-up mr-1">
                            <?php endif; ?>
                                </i><?= number_format(abs($diff/1000),2) ?></span></td>
                            <td>Investasi Sekarang</td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>

<!--<section class="bg-light py-main section-life-insurance-5" style="background: url(/template_avrist/assets/img/common/img_banner_solution_avram.jpg) no-repeat center; background-size: cover;">
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <h2>Butuh perhitungan dan proteksi ideal untukmu atau bisnismu? <a href="<?= $url->getHome() ?>/get-solution/personal">Dapatkan Solusi</a></h2>
            </div>
        </div>
    </div>
</section>-->