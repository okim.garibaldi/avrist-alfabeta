<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

use Pimcore\Model\Document;
use Pimcore\Model\Document\Page;

$this->extend('layout_avrist_new.html.php');?>

<div class="cover cover-sm cover-responsive bg-primary cover-general">
    <div class="cover-responsive-img">
        <div class="cover-responsive-overlay"></div>
    </div>
    <div class="container position-relative">
        <img src="/template_avrist/assets/img/pattern/ic_ornamen_yellow.png" class="pattern-purple" alt="Ornamen Purple">
        <!-- <img src="/template_avrist/assets/img/pattern/ic_ornament_triangle_yellow.png" class="pattern-triangle-yellow" alt="Ornamen Triangle Yellow"> -->
        <img src="/template_avrist/assets/img/pattern/ic_ornament_circle_pink.png" class="pattern-pink" alt="Ornamen Circle Pink">
        <nav aria-label="breadcrumb" class="general-breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item"><a href="#">PT. Avrist Insurance</a></li>
                <li class="breadcrumb-item active"><a href="#">Unit Syariah</a></li>
            </ol>
        </nav>
        <div class="cover-content">
            <h3 class="cover-title text-white animated fadeInUp delayp1"><?= $unitSyariahPage->getTitle1() ?></h3>
            <p class="cover-subtitle text-white"><?= $unitSyariahPage->getDescription() ?></p>
        </div>
    </div>
</div>

<section class="py-main section-dewan-pengawas-syariah">
    <div class="container">
        <div class="card card-general">
            <div class="card-body">
                <div class="heading vp-fadeinup delayp2">
                    <h2><?= $unitSyariahPage->getTitle2() ?></h2>
                </div>
                <div class="row mt-3">
                <?php foreach($unitSyariahPage->getBlock1() as $block1): ?>
                    <div class="col-md-6">
                        <div class="card card-syariah">
                            <div class="card-body">
                                <div class="info-account">
                                    <div class="img-wrapper">
                                        <img src="<?= $block1['image']->getData() ?>" class="img-fluid">
                                    </div>
                                    <div class="title-wrapper">
                                        <h5 class="mb-2"><?= $block1['title']->getData() ?></h5>
                                        <p class="position"><?= $block1['subtitle']->getData() ?></p>
                                    </div>
                                </div>
                                <p class="mb-0 content-text"><?= $block1['description']->getData() ?></p>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="py-main section-unit-syariah">
    <div class="container">
        <div class="heading mb-5 vp-fadeinup delayp2 text-center">
            <h2><?= $unitSyariahPage->getTitle3() ?></h2>
        </div>
        <div class="row">
        <?php foreach($unitSyariahPage->getBlock2() as $block2): ?>
            <div class="col-md-4">
                <div class="card card-no-border">
                    <div class="card-body">
                        <img src="<?= $block2['image']->getData() ?>" class="img-fluid">
                        <p><?= $block2['description']->getData() ?></p>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
        </div>
    </div>
</section>