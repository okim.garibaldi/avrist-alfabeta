<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

use Pimcore\Model\Document;
use Pimcore\Model\Document\Page;

$this->extend('layout_avrist_new.html.php');?>

<div class="cover cover-sm cover-responsive <?= $bgBanner['bg'] ?> cover-general">
    <div class="cover-responsive-img">
        <div class="cover-responsive-overlay"></div>
    </div>
    <div class="container position-relative">
        <img src="/template_avrist/assets/img/pattern/ic_ornamen_yellow.png" class="pattern-purple" alt="Ornamen Purple">
        <!-- <img src="/template_avrist/assets/img/pattern/ic_ornament_triangle_yellow.png" class="pattern-triangle-yellow" alt="Ornamen Triangle Yellow"> -->
        <img src="/template_avrist/assets/img/pattern/ic_ornament_circle_pink.png" class="pattern-pink" alt="Ornamen Circle Pink">
        <nav aria-label="breadcrumb" class="general-breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item"><a href="#"><?= $aboutCompany->getName() ?></a></li>
                <li class="breadcrumb-item active"><a href="#"><?= $financialReportPage->getTitle1() ?></a></li>
            </ol>
        </nav>
        <div class="cover-content">
            <h3 class="cover-title text-white animated fadeInUp delayp1"><?= $financialReportPage->getTitle1() ?></h3>
        </div>
    </div>
</div>

<section class="tab-general">
    <div class="container">
        <ul class="nav nav-tabs general-tab-cover" id="generalTab" role="tablist">
        <?php $selected = "true"; ?>
        <?php $navlink = " active"; ?>
        <?php foreach ($financialReport as $fRKey => $fRYears):?>
            <li class="nav-item">
                <a class="nav-link<?= $navlink ?>" id="<?= strtolower($fRKey) ?>-tab" data-toggle="tab" href="#<?= strtolower($fRKey) ?>" role="tab" aria-controls="<?= strtolower($fRKey) ?>" aria-selected="<?= $selected ?>"><?= $fRKey ?></a>
            </li>
            <?php if ($selected == "true" || $navlink == " active"): ?>
            <?php $selected = "false";?>
            <?php $navlink = ""; ?> 
            <?php endif; ?>
        <?php endforeach; ?>
        </ul>
        <div class="tab-content" id="generalTabContent">
            <?php $class = " show active"; ?>
            <?php foreach ($financialReport as $fRKey => $fRYears):?>
            <div class="tab-pane fade<?= $class ?>" id="<?= strtolower($fRKey) ?>" role="tabpanel" aria-labelledby="<?= strtolower($fRKey) ?>-tab">
                <div class="py-main">
                <?php foreach($fRYears as $fRYearsKey => $fRs): ?>
                    <div class="heading mb-3">
                        <h2 class="font-weight-bold" style="color: #b2bbc3;"><?= $fRYearsKey ?></h2>
                    </div>
                    <div class="row">
                        <?php foreach($fRs as $fR): ?>
                        <div class="col-6 col-md-3">
                            <a href="<?= $fR['file']->getFullPath() ?>" target="_blank" class="card card-list">
                                <div class="card-body">
                                    <h3 class="text-truncate"><?= $fR['title'] ?> <i class="fal fa-angle-right"></i></h3>
                                    <p><img src="/template_avrist/assets/img/tentang/pdf.png" class="extension-file">Pdf Files | <?= number_format($fR['file']->getFileSize() / 1024, 2) . ' KB' ?></p>
                                </div>
                            </a>
                        </div>
                        <?php endforeach; ?>
                    </div>
                <?php endforeach; ?>
                </div>
            </div>
            <?php if ($class == " show active"): ?>
            <?php $class = ""; ?>
            <?php endif; ?>
            <?php endforeach; ?>
        </div>
    </div>
</section>