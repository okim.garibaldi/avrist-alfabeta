<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

 use Pimcore\Model\Document;
 use Pimcore\Model\Document\Page;
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
  <head>
  <?= $this->template("include/head2.html.php"); ?>
  <?= $this->template("include/tracking-script/head.html.php"); ?>
  <style>
    .navbar.fixed-top + * {
      margin-top: 0;
    }
  </style>
  </head>
  <body>
    <?= $this->template("include/tracking-script/body.html.php"); ?>

    <div class="loader-wrapper loader-light">
      <div class="loader"></div>
    </div> 

    <?= $this->template("include/navbar3.html.php"); ?>

    <!--Start: Cover Responsive-->
    
    <!--End: Cover Responsive-->

    <div class="cover cover-responsive">
      <div id="carouselCoverResponsive" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
        <?php $block1Count1 = 0; ?>
        <?php foreach($homePage->getBlock1() as $block1): ?>
          <li data-target="#carouselCoverResponsive" data-slide-to="<?php echo $block1Count1 ?>" <?php if($block1Count == 0) echo 'class="active"'; ?>></li>
          <?php $block1Count1++; ?>
        <?php endforeach; ?> 
        </ol>
        <div class="carousel-inner">
        <?php $block1Count2 = 0; ?>
        <?php foreach($homePage->getBlock1() as $block1): ?>
          <div class="carousel-item <?php if($block1Count2 == 0) echo "active" ?>">
            <div class="cover cover-responsive cover-home cover-avrist">
              <div class="cover-responsive-img">
                <div class="cover-responsive-overlay"></div>
              </div>
              <div class="container position-relative">
                <img src="/template_avrist/assets/img/pattern/ic_ornamen_yellow.png" class="pattern-purple" alt="Ornamen Purple">
                <img src="/template_avrist/assets/img/pattern/ic_ornament_triangle_yellow.png" class="pattern-triangle-yellow" alt="Ornamen Triangle Yellow">
                <img src="/template_avrist/assets/img/pattern/ic_ornament_circle_pink.png" class="pattern-pink" alt="Ornamen Circle Pink">
                <div class="cover-content">
                  <h3 class="cover-title animated fadeInUp delayp1"><?= $block1['title']->getData() ?></h3>
                  <div class="button-group">
                    <a href="<?= $block1['text1Page']->getData() ?>" class="btn btn-secondary w-150"><?= $block1['text1']->getData() ?></a>
                    <a href="<?= $block1['text2Page']->getData() ?>" class="btn btn-secondary w-150"><?= $block1['text2']->getData() ?></a>
                  </div>
                </div>
              </div>
              <!-- <a href="#" class="scroll-hint bottom-0 animated fadeInDown delayp4">
                <img src="/template_avrist/assets/img/common/arrow_down.png" alt="Arrow down hint"/>
              </a> -->
            </div>
          </div>
          <?php $block1Count2++; ?>
        <?php endforeach; ?>
        </div>
        <a class="carousel-control-prev hidden" href="#carouselCoverResponsive" role="button" data-slide="prev">
          <i class="fal fa-angle-left" data-fa-transform="grow-20"></i>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next hidden" href="#carouselCoverResponsive" role="button" data-slide="next">
          <i class="fal fa-angle-right" data-fa-transform="grow-20"></i>
          <span class="sr-only">Next</span>
        </a>
      </div>
      <!-- <a href="#" class="scroll-hint bottom-25 animated fadeInDown delayp4">
        <img src="/template_avrist/assets/img/common/arrow_down.png" alt="Arrow down hint"/>
      </a> -->
    </div>

    <section class="py-main section-sekilas-avrist">
      <img src="/template_avrist/assets/img/pattern/ic_ornamen_yellow.png" class="pattern-yellow vp-fadeinup delayp1" alt="Ornamen Yellow">
      <div class="container">
        <div class="heading vp-fadeinup delayp1">
          <h2><?= $homePage->getTitle1() ?></h2>
        </div>
        <div class="content">
          <p class="desc-one vp-fadeinup delayp2"><?= $homePage->getDescription1() ?></p>
          <p class="desc-two vp-fadeinup delayp2"><?= $homePage->getDescription2() ?>.</p>
          <a href="
          <?php if($homePage->getText1Prefix()) {
            echo $this->navbar['url']->{"get".$homePage->getText1Prefix()}();
          }
          echo $homePage->getText1Link(); ?>" class="link-arrow vp-fadeinup delayp2"><?= $homePage->getText1() ?> <i class="fas fa-chevron-right"></i></a>
        </div>
      </div>
    </section>

    <section class="section-home-product">
      <div class="row row-0">
      <?php foreach($homePage->getBlock2() as $block2): ?>
        <div class="col vp-fadeinup delayp1">
          <a href="
          <?php if($block2['prefix']->getData()) {
            echo $this->navbar['url']->{"get".$block2['prefix']->getData()}();
          }
          echo $block2['link']->getData(); ?>">
            <div class="product-wrapper">
              <img src="<?= $block2['image']->getData() ?>" class="img-fluid" alt="Ornamen Yellow">
              <p><?= $block2['title']->getData() ?></p>
            </div>
          </a>
        </div>
      <?php endforeach; ?>
    </section>

    <section class="py-main section-solusi-favorit">
      <div class="container position-relative">
        <img src="/template_avrist/assets/img/pattern/ic_ornament_circle_purple.png" class="pattern-circle-purple" alt="Ornamen Circle Purple">
        <img src="/template_avrist/assets/img/pattern/ic_ornament_circle_pink.png" class="pattern-circle-pink" alt="Ornamen Pink">
        <div class="heading vp-fadeinup delayp1">
          <h2>Solusi Favorit</h2>
        </div>
        <div class="owl-carousel owl-theme owl-dots-solid owl-solution mt-5">
          <?php foreach($product as $p): ?>
            <div class="card card-solution vp-fadeinup delayp1">
            <div class="card-img-top" style="background: url(<?php echo $p->getImage(); ?>) no-repeat center; background-size: cover;"></div>
            <div class="card-body">
              <p class="card-label">
              <?php if ($p->getSiteId() == 1)
              {
                echo "Life-Insurance";
              }
              elseif ($p->getSiteId() == 2)
              {
                echo "General Insurance";
              }
              else
              {
                echo "Asset Management";
              } ?></p>
              <h5 class="card-title text-truncate"><?= $p->getName()?></h5>
              <p class="card-text text-truncate-multiline"><?= str_replace(['<p>', '</p>'], ['<br>', '</br>'], $p->getSummary())?></p>
              <a href="
              <?php
              if($p->getSiteId() == 1)
              {
                echo $this->navbar['url']->getLife().str_replace("/life-insurance", "",$p->getPage());
              } 
              elseif($p->getSiteId() == 2)
              {
                echo $this->navbar['url']->getGeneral().str_replace("/general", "",$p->getPage());
              }
              else
              {
                echo $this->navbar['url']->getAm().str_replace("/asset-management", "",$p->getPage());;
              }

               ?>" class="card-link">Lihat detail</a>
            </div>
          </div>
          <?php endforeach;?>

        </div>
      </div>
    </section>

    <section class="py-main section-why-us">
      <img src="/template_avrist/assets/img/pattern/img_ornamen_waves_grey.png" class="pattern-grey" alt="Pattern Grey">
      <div class="container position-relative">
        <img src="/template_avrist/assets/img/pattern/ic_ornamen_yellow.png" class="pattern-yellow vp-fadeinup delayp1" alt="Ornamen Yellow">
        <div class="row">
          <div class="col-md-6 text-center">
            <img src="/template_avrist/assets/img/common/mengapa_avrist.gif" class="img-fluid vp-fadeinleft" id="faqHome" alt="Why Us">
          </div>
          <div class="col-md-6 content-center">
            <div class="heading mb-4 vp-fadeinup delayp1">
              <h2><?= $homePage->getTitle3() ?></h2>
            </div>
            <div id="accordionFAQ" class="accordion accordion-faq vp-fadeinup delayp2">
            <?php $faqCount = 0; ?>
            <?php foreach($homePage->getBlock4() as $block4): ?>
              <div class="accordion-item">
                <div class="accordion-header" id="headingFAQ<?= $faqCount ?>" data-toggle="collapse" data-target="#collapseFAQ<?= $faqCount ?>" aria-expanded="<?php if($faqCount == 0){
                  echo "true";
                }
                else{
                  echo "false";
                } ?>" aria-controls="collapseFAQ<?= $faqCount ?>">
                  <h5><?= $block4['title']->getData() ?>
                  <span class="close-panel"></span>
                  </h5>
                </div>
                <div id="collapseFAQ<?= $faqCount ?>" class="accordion-body collapse<?php if($faqCount == 0){
                  echo "show";
                } ?>" aria-labelledby="headingFAQ<?= $faqCount ?>" data-parent="#accordionFAQ">
                  <p><?= $block4['description']->getData() ?></p>
                </div>
              </div>
              <?php $faqCount++; ?>
            <?php endforeach; ?>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- <section class="py-main section-how-to">
      <div class="container">
        <div class="heading mb-5 text-center">
          <img src="/template_avrist/assets/img/pattern/ic_ornament_triangle_yellow.png" class="pattern-triangle-yellow" alt="Pattern Triangle Yellow">
          <h2 class="vp-fadeinup delayp1">How To</h2>
        </div>
        <div class="content">
          <div class="row">
            <div class="col-6 col-md-3 vp-fadeinup delayp2">
              <a href="faq" class="card card-how-to">
                <img src="/template_avrist/assets/img/icon/ic_howto_buy.png" class="img-card-how-to img-fluid" alt="Card Img">
                <h5 class="card-title">Buy Product</h5>
              </a>
            </div>
            <div class="col-6 col-md-3 vp-fadeinup delayp3">
              <a href="faq" class="card card-how-to">
                <img src="/template_avrist/assets/img/icon/ic_howto_pay.png" class="img-card-how-to img-fluid" alt="Card Img">
                <h5 class="card-title mt-1">Pay</h5>
              </a>
            </div>
            <div class="col-6 col-md-3 vp-fadeinup delayp4">
              <a href="faq" class="card card-how-to">
                <img src="/template_avrist/assets/img/icon/ic_howto_claim.png" class="img-card-how-to img-fluid" alt="Card Img">
                <h5 class="card-title">Claim</h5>
              </a>
            </div>
            <div class="col-6 col-md-3 vp-fadeinup delayp5">
              <a href="faq" class="card card-how-to">
                <img src="/template_avrist/assets/img/icon/ic_howto_plan.png" class="img-card-how-to img-fluid" alt="Card Img">
                <h5 class="card-title">Change Plan</h5>
              </a>
            </div>
          </div>
        </div>
      </div>
    </section> -->

    <section class="py-main section-layanan-kamu">
      <div class="container position-relative">
        <img src="/template_avrist/assets/img/pattern/ic_ornamen_green.png" class="pattern-green" alt="Pattern Green">
        <div class="heading text-center mb-5 vp-fadeinup delayp1">
          <h2><?= $homePage->getTitle4() ?></h2>
        </div>
        <div class="content">
          <div class="row">
          <?php foreach($homePage->getBlock5() as $block5): ?>
            <div class="col-md-4 vp-fadeinup delayp2">
              <a href="<?php if($block5['prefix']->getData()) {
              echo $this->navbar['url']->{"get".$block5['prefix']->getData()}();
            }
            echo $block5['link']->getData(); ?>" class="card card-service">
                <img src="<?= $block5['image']->getData() ?>" class="img-fluid service-img" alt="Card Img">
                <div class="content">
                  <h5 class="card-title"><?= $block5['name']->getData() ?></h5>
                </div>
              </a>
            </div>
          <?php endforeach; ?>
          </div>
        </div>
      </div>
    </section>

    <section class="py-main section-kenali-kebutuhanmu">
      <img src="/template_avrist/assets/img/pattern/img_ornament_waves_purple.png" class="pattern-purple" alt="Pattern Purple">
      <div class="container">
        <div class="heading-w-link">
          <h2><?= $homePage->getTitle5() ?></h2>
          <div class="heading-group">
            <div class="row">
              <div class="col-md-6">
                <p><?= $homePage->getSubtitle5() ?></p>
              </div>
              <div class="col-md-6 text-md-right">
                <a href="<?= $homePage->getText2Link() ?>" class="link-arrow"><?= $homePage->getText2() ?> <i class="fa fa-chevron-right d-inline-block p-0"></i></a>
              </div>
            </div>
          </div>
        </div>
        <div class="content text-white">
          <div class="owl-carousel owl-theme owl-dots-solid owl-kebutuhan mt-5 vp-fadeinup delayp2">

        <?php foreach ($this->navbar['rss_feed']->channel->item as $feed_item) : ?>
          <a href="<?= $feed_item->link ?>" class="card card-kebutuhan">
            <div class="card-img">
              <img src="http://avrist.com/lifeguide/wp-content/uploads/2020/02/Photo-by-Ben-White-on-Unsplash-534x462.jpg" class="img-fluid">
            </div>
            <div class="card-body">
              <p class="card-date"><?= date("j F Y", strtotime($feed_item->pubDate)) ?></p>
              <h5 class="card-title text-truncate-twoline"><?= $feed_item->title ?></h5>
            </div>
          </a>
          <?php if($i++ > 10) break; ?>
        <?php endforeach; ?>
        </div>
        </div>
      </div>
    </section>

    <section class="py-main section-news">
      <div class="container">
        <div class="heading-w-link">
          <div class="heading-group">
            <div class="row">
              <div class="col-md-6">
                <h2><?= $homePage->getTitle6() ?></h2>
              </div>
              <div class="col-md-6 text-md-right">
                <a href="<?php if($homePage->getText3Prefix()) {
                echo $this->navbar['url']->{"get".$homePage->getText3Prefix()}();
              }
              echo $homePage->getText3Link(); ?>" class="link-arrow"><?= $homePage->getText3() ?> <i class="fa fa-chevron-right d-inline-block p-0"></i></a>
              </div>
            </div>
          </div>
        </div>
        <div class="content">
          <div class="owl-carousel owl-theme owl-dots-solid owl-kebutuhan mt-5 vp-fadeinup delayp2">
          <?php foreach($news as $n): ?>
            <a href="<?= $this->navbar['url']->getLife()."/".str_replace($newsPage->getPath(), '', $n->getFullPath()) ?>" class="card card-news">
              <div class="card-img">
                <img src="<?= $n->getImage1() ?>" class="img-fluid">
              </div>
              <div class="card-body">
                <p class="card-date"><?= date("j F Y", strtotime($n->getPublishDate())) ?></p>
                <h5 class="card-title text-truncate-twoline"><?= $n->getTitle1() ?></h5>
              </div>
            </a>
          <?php endforeach; ?>
          </div>
        </div>
      </div>
    </section>


    <?= $this->template("include/footer2.html.php"); ?>
    <?= $this->template("include/script2.html.php"); ?>

    <script>
      // Navbar active state
      // $('#navHome').addClass('active');

      // Animation on load
      document.addEventListener("DOMContentLoaded", function(event) {
        $(".loader").fadeOut('slow');
        $(".loader-wrapper").fadeOut("slow");
      //  $(".anim-1").addClass("animated fadeInLeft delayp10");
       // $(".anim-2").addClass("animated fadeInUp delayp12");
        //$(".anim-3").addClass("animated fadeInUp delayp14");  
      });

      $('.owl-solution').owlCarousel({
        loop: false,
        margin: 7.5,
        dots: false,
        responsive:{
          0:{
            items: 1
          },
          768:{
              margin: 30,
              items: 3
          }
        }
      });

      $('.owl-kebutuhan').owlCarousel({
        loop: false,
        margin: 7.5,
        dots: false,
        responsive:{
          0:{
            items: 1
          },
          768:{
              margin: 30,
              items: 3
          }
        }
      });
    <?php $faqCount = 0; ?>
    <?php foreach($homePage->getBlock4() as $block4): ?>
      $('#headingFAQ<?= $faqCount ?>').click(function(){
        $('#faqHome').attr("src","<?= $block4['image']->getData() ?>");
      });
    <?php $faqCount++; ?>
    <?php endforeach; ?>

      $('.navbar-toggler').click(function(){
        $('.navbar').removeClass("navbar-transparent");
      });
      // $('.navbar-slide-close').click(function(){
      //   $('.navbar').addClass("navbar-transparent");
      // });

    </script>

  </body>
</html>
