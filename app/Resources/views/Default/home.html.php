<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

 use Pimcore\Model\Document;
 use Pimcore\Model\Document\Page;

 $controller = new \AppBundle\Controller\DefaultController();
 $url = $controller->url();
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
  <head>
    <?php $prefix = $url->getHome();?>
    <?php $lifeaddr = $url->getLife();?>
    <?php $generaladdr = $url->getGeneral();?>
    <?php $amaddr = $url->getAm();?>
    <?= $this->template("include/head.html.php"); ?>
    <?= $this->template("include/tracking-script/head.html.php"); ?>

    <style>
      .navbar.fixed-top + * {
        margin-top: 0;
      }
    </style>

   </head>
  <body>
    <?= $this->template("include/tracking-script/body.html.php"); ?>

    <div class="loader-wrapper loader-light">
        <div class="loader"></div>
    </div>

    <?= $this->template("include/navbar3.html.php"); ?>

    <!--Start: Cover Responsive-->
    <div class="cover cover-responsive">
        <div id="carouselCoverResponsive" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselCoverResponsive" data-slide-to="0" class="active"></li>
                <li data-target="#carouselCoverResponsive" data-slide-to="1"></li>
                <li data-target="#carouselCoverResponsive" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <div class="cover cover-responsive cover-home cover-avrist">
                        <div class="cover-responsive-img">
                            <div class="cover-responsive-overlay"></div>
                        </div>
                        <div class="container position-relative">
                            <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/pattern/ic_ornamen_yellow.png')?>" class="pattern-purple" alt="Ornamen Purple">
                            <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/pattern/ic_ornament_triangle_yellow.png')?>" class="pattern-triangle-yellow" alt="Ornamen Triangle Yellow">
                            <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/pattern/ic_ornament_circle_pink.png')?>" class="pattern-pink" alt="Ornamen Circle Pink">
                            <div class="cover-content">
                                <h3 class="cover-title animated fadeInUp delayp1">Lindungi Masa Depan dengan Temukan Solusi Ideal.</h3>
                                <div class="button-group">
                                    <a href="/get-solution/personal" class="btn btn-secondary w-150">Personal</a>
                                    <a href="/get-solution/bisnis" class="btn btn-secondary w-150">Bisnis</a>
                                </div>
                            </div>
                        </div>
                        <!-- <a href="#" class="scroll-hint bottom-0 animated fadeInDown delayp4">

                  </a> -->
                    </div>
                </div>
                <div class="carousel-item">
                    <div class="cover cover-responsive cover-home cover-avrist">
                        <div class="cover-responsive-img">
                            <div class="cover-responsive-overlay"></div>
                        </div>
                        <div class="container position-relative">
                            <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/pattern/ic_ornamen_yellow.png')?>" class="pattern-purple" alt="Ornamen Purple">
                            <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/pattern/ic_ornament_triangle_yellow.png')?>" class="pattern-triangle-yellow" alt="Ornamen Triangle Yellow">
                            <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/pattern/ic_ornament_circle_pink.png')?>" class="pattern-pink" alt="Ornamen Circle Pink">
                            <div class="cover-content">
                                <h3 class="cover-title animated fadeInUp delayp1">Temukan Solusi Anda</h3>
                                <div class="button-group">
                                    <a href="/get-solution/personal" class="btn btn-secondary w-150">Personal</a>
                                    <a href="/get-solution/bisnis" class="btn btn-secondary w-150">Bisnis</a>
                                </div>
                            </div>
                        </div>
                        <!-- <a href="#" class="scroll-hint bottom-0 animated fadeInDown delayp4">

                  </a> -->
                    </div>
                </div>
                <div class="carousel-item">
                    <div class="cover cover-responsive cover-home cover-avrist">
                        <div class="cover-responsive-img">
                            <div class="cover-responsive-overlay"></div>
                        </div>
                        <div class="container position-relative">
                            <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/pattern/ic_ornamen_yellow.png')?>" class="pattern-purple" alt="Ornamen Purple">
                            <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/pattern/ic_ornament_triangle_yellow.png')?>" class="pattern-triangle-yellow" alt="Ornamen Triangle Yellow">
                            <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/pattern/ic_ornament_circle_pink.png')?>" class="pattern-pink" alt="Ornamen Circle Pink">
                            <div class="cover-content">
                                <h3 class="cover-title animated fadeInUp delayp1">Lindungi Masa Depan dengan Temukan Solusi Ideal 3.</h3>
                                <div class="button-group">
                                    <a href="/get-solution/personal" class="btn btn-secondary w-150">Personal</a>
                                    <a href="/get-solution/bisnis" class="btn btn-secondary w-150">Bisnis</a>
                                </div>
                            </div>
                        </div>
                        <!-- <a href="#" class="scroll-hint bottom-0 animated fadeInDown delayp4">

                  </a> -->
                    </div>
                </div>
            </div>
            <a class="carousel-control-prev hidden" href="#carouselCoverResponsive" role="button" data-slide="prev">
                <i class="fal fa-angle-left" data-fa-transform="grow-20"></i>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next hidden" href="#carouselCoverResponsive" role="button" data-slide="next">
                <i class="fal fa-angle-right" data-fa-transform="grow-20"></i>
                <span class="sr-only">Next</span>
            </a>
        </div>
        <!-- <a href="#" class="scroll-hint bottom-25 animated fadeInDown delayp4">

          </a> -->
    </div>

    <section class="py-main section-sekilas-avrist">
        <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/pattern/ic_ornamen_yellow.png')?>" class="pattern-yellow vp-fadeinup delayp1" alt="Ornamen Yellow">
        <div class="container">
            <div class="heading vp-fadeinup delayp1">
                <h2>Sekilas Avrist</h2>
            </div>
            <div class="content">
                <p class="desc-one vp-fadeinup delayp2"><?= $this->about->getShortDescription() ?></p>
                <p class="desc-two vp-fadeinup delayp2"><?= $this->about->getDescription() ?></p>
                <a href="<?= $this->about->getPage() ?>" class="link-arrow vp-fadeinup delayp2">Learn more about avrist <i class="fas fa-chevron-right"></i></a>
            </div>
        </div>
    </section>

    <section class="section-home-product">
        <div class="row row-0">
            <div class="col vp-fadeinup delayp1">
                <a href="http://<?= $lifeaddr ?>">
                    <div class="product-wrapper">
                        <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/common/img_life_insurance.jpg')?>" class="img-fluid" alt="Ornamen Yellow">
                        <p>Life Insurance</p>
                    </div>
                </a>
            </div>
            <div class="col vp-fadeinup delayp2">
                <!-- <a href="http://general.<?= $prefix ?>"> -->
                <a href="http://<?= $generaladdr ?>">
                    <div class="product-wrapper">
                        <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/common/img_general_insurance.jpg')?>" class="img-fluid" alt="Ornamen Yellow">
                        <p>General Insurance</p>
                    </div>
                </a>
            </div>
            <div class="col vp-fadeinup delayp3">
                <!-- <a href="http://am.<?= $prefix ?>"> -->
                <a href="http://<?= $amaddr ?>">
                    <div class="product-wrapper">
                        <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/common/img_asset_management.jpg')?>" class="img-fluid" alt="Ornamen Yellow">
                        <p>Asset Management</p>
                    </div>
                </a>
            </div>
        </div>
    </section>

    <section class="py-main section-solusi-favorit">
        <div class="container position-relative">
            <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/pattern/ic_ornament_circle_purple.png')?>" class="pattern-circle-purple" alt="Ornamen Circle Purple">
            <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/pattern/ic_ornament_circle_pink.png')?>" class="pattern-circle-pink" alt="Ornamen Pink">
            <div class="heading vp-fadeinup delayp1">
                <h2>Solusi Favorit</h2>
            </div>
            <div class="owl-carousel owl-theme owl-dots-solid owl-solution mt-5">

                <?php foreach ($this->product as $product) {?>
                    <div class="card card-solution vp-fadeinup delayp1">
                        <div class="card-img-top" style="background: url(<?php echo $product->getImage();?>) no-repeat center; background-size: cover;"></div>
                        <div class="card-body">
                            <p class="card-label">
                                <?php if (strpos($product->getCategory(), 'Life-Insurance') == true) {
                                    echo 'Life Insurance';
                                } elseif (strpos($product->getCategory(), 'General') == true) {
                                    echo 'General Insurance';
                                } elseif (strpos($product->getCategory(), 'AM') == true) {
                                    echo 'Asset Management';
                                }?>
                            </p>
                            <h5 class="card-title text-truncate"><?= $product->getName() ?></h5>
                            <p class="card-text text-truncate-multiline"><?= str_replace(['<p>', '</p>'], ['<br>', '</br>'], $product->getSummary())?></p>
                            <a href="<?= $product->getPage() ?>" class="card-link">Lihat detail</a>
                        </div>
                    </div>
                <?php } ?>

            </div>
        </div>
    </section>

    <section class="py-main section-why-us">
        <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/pattern/img_ornamen_waves_grey.png')?>" class="pattern-grey" alt="Pattern Grey">
        <div class="container position-relative">
            <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/pattern/ic_ornamen_yellow.png')?>" class="pattern-yellow vp-fadeinup delayp1" alt="Ornamen Yellow">
            <div class="row">
                <div class="col-md-6 text-center">
                    <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/common/mengapa_avrist.gif')?>" class="img-fluid vp-fadeinleft" id="faqHome" alt="Why Us">
                </div>
                <div class="col-md-6 content-center">
                    <div class="heading mb-4 vp-fadeinup delayp1">
                        <h2>Mengapa Avrist?</h2>
                    </div>
                    <div id="accordionFAQ" class="accordion accordion-faq vp-fadeinup delayp2">
                        <div class="accordion-item">
                            <div class="accordion-header" id="headingFAQ1" data-toggle="collapse" data-target="#collapseFAQ1" aria-expanded="true" aria-controls="collapseFAQ1">
                                <h5>1. Aplikasi mudah
                                    <span class="close-panel"></span>
                                </h5>
                            </div>
                            <div id="collapseFAQ1" class="accordion-body collapse show" aria-labelledby="headingFAQ1" data-parent="#accordionFAQ">
                                <p>Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. </p>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <div class="accordion-header" id="headingFAQ2" data-toggle="collapse" data-target="#collapseFAQ2" aria-expanded="false" aria-controls="collapseFAQ2">
                                <h5>2. Premi rendah, manfaat terbaik
                                    <span class="close-panel"></span>
                                </h5>
                            </div>
                            <div id="collapseFAQ2" class="accordion-body collapse" aria-labelledby="headingFAQ2" data-parent="#accordionFAQ">
                                <p>Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. </p>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <div class="accordion-header" id="headingFAQ3" data-toggle="collapse" data-target="#collapseFAQ3" aria-expanded="false" aria-controls="collapseFAQ3">
                                <h5>3. Proteksi lengkap dalam 1 harga
                                    <span class="close-panel"></span>
                                </h5>
                            </div>
                            <div id="collapseFAQ3" class="accordion-body collapse" aria-labelledby="headingFAQ3" data-parent="#accordionFAQ">
                                <p>Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. Fusce vehicula dolor arcu, sit amet blandit dolor mollis nec. </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="py-main section-layanan-kamu">
        <div class="container position-relative">
            <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/pattern/ic_ornamen_green.png')?>" class="pattern-green" alt="Pattern Green">
            <div class="heading text-center mb-5 vp-fadeinup delayp1">
                <h2>Layanan Kami</h2>
            </div>
            <div class="content">
                <div class="row">
                    <div class="col-md-4 vp-fadeinup delayp2">
                        <a href="#" class="card card-service">
                            <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/common/ic_ilustrasi_isometrik_RS.png')?>" class="img-fluid service-img" alt="Card Img">
                            <div class="content">
                                <h5 class="card-title">Rumah Sakit <br>Partner</h5>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-4 vp-fadeinup delayp3">
                        <a href="#" class="card card-service">
                            <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/common/ic_ilustrasi_isometrik_RS.png')?>" class="img-fluid service-img" alt="Card Img">
                            <div class="content">
                                <h5 class="card-title">Bengkel <br>Partner</h5>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-4 vp-fadeinup delayp4">
                        <a href="#" class="card card-service">
                            <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/common/ic_ilustrasi_isometrik_RS.png')?>" class="img-fluid service-img" alt="Card Img">
                            <div class="content">
                                <h5 class="card-title">Harga Unit</h5>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="py-main section-kenali-kebutuhanmu">
        <img src="<?= $view['assets']->getUrl('/template_avrist/assets/img/pattern/img_ornament_waves_purple.png')?>" class="pattern-purple" alt="Pattern Purple">
        <div class="container">
            <div class="heading-w-link">
                <h2>Kenali kebutuhanmu</h2>
                <div class="heading-group">
                    <div class="row">
                        <div class="col-md-6">
                            <p>by life guide</p>
                        </div>
                        <div class="col-md-6 text-md-right">
                            <a href="#" class="link-arrow">Lihat semua <i class="fa fa-chevron-right d-inline-block p-0"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content text-white">
                <div class="owl-carousel owl-theme owl-dots-solid owl-kebutuhan mt-5 vp-fadeinup delayp2">
                    <?php foreach ($this->life as $l){?>
                    <a href="#" class="card card-kebutuhan">
                        <div class="card-img">
                            <img src="<?= $l->getImage() ?>" class="img-fluid">
                        </div>
                        <div class="card-body">
                            <p class="card-date">12 Agustus 2018</p>
                            <h5 class="card-title text-truncate-twoline">Ketahui Asuransi Jiwa yang Diperlukan Keluarga</h5>
                        </div>
                    </a>
                    <?php } ?>

                </div>
            </div>
        </div>
    </section>

    <section class="py-main section-news">
        <div class="container">
            <div class="heading-w-link">
                <div class="heading-group">
                    <div class="row">
                        <div class="col-md-6">
                            <h2>Berita Terkini</h2>
                        </div>
                        <div class="col-md-6 text-md-right">
                            <a href="/news" class="link-arrow">Lihat semua <i class="fa fa-chevron-right d-inline-block p-0"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content">
                <div class="owl-carousel owl-theme owl-dots-solid owl-kebutuhan mt-5 vp-fadeinup delayp2">
                    <?php foreach ($this->news as $n){?>
                        <a href="<?= $n->getPage() ?>" class="card card-news">
                        <div class="card-img">
                            <img src="<?= $n->getImage() ?>" class="img-fluid">
                        </div>
                        <div class="card-body">
                            <p class="card-date">12 Agustus 2018</p>
                            <h5 class="card-title text-truncate-twoline"><?= $n->getTitle() ?></h5>
                        </div>
                    </a>
                    <?php } ?>

                </div>
            </div>
        </div>
    </section>

    <?= $this->template("include/footer.html.php"); ?>
    <?= $this->template("include/script.html.php"); ?>

    <script>
        // Navbar active state
        // $('#navHome').addClass('active');
        $('.navbar-avrist').addClass('navbar-transparent');
        var logo = ["/template_avrist/assets/img/brand/logo_avrist.png", "/template_avrist/assets/img/brand/logo_avrist_white.png"];
        $('.navbar-avrist .navbar-brand img').attr('src', logo[1]);

        $(window).on("scroll", function() {
            if ($(window).scrollTop() > 10) {
                $(".navbar-avrist").addClass("navbar-scroll");
                $(".navbar-avrist").removeClass("navbar-transparent");
                $('.navbar-avrist .navbar-brand img').attr('src', logo[0]);
            } else {
                //remove the background property so it comes transparent again (defined in your css)
                $(".navbar-avrist").removeClass("navbar-scroll");
                $(".navbar-avrist").addClass("navbar-transparent");
                $('.navbar-avrist .navbar-brand img').attr('src', logo[1]);
            }
        });

        // Animation on load
        document.addEventListener("DOMContentLoaded", function(event) {
            $(".loader").fadeOut('slow');
            $(".loader-wrapper").fadeOut("slow");
            //  $(".anim-1").addClass("animated fadeInLeft delayp10");
            // $(".anim-2").addClass("animated fadeInUp delayp12");
            //$(".anim-3").addClass("animated fadeInUp delayp14");
        });

         $('.owl-solution').owlCarousel({
        loop: false,
        margin: 7.5,
        dots: false,
        responsive:{
          0:{
            items: 1
          },
          768:{
              margin: 30,
              items: 3
          }
        }
      });

      $('.owl-kebutuhan').owlCarousel({
        loop: false,
        margin: 7.5,
        dots: false,
        responsive:{
          0:{
            items: 1
          },
          768:{
              margin: 30,
              items: 3
          }
        }
      });

      $('#headingFAQ1').click(function(){
        $('#faqHome').attr("src","/template_avrist/assets/img/common/mengapa_avrist.gif");
      });

      $('#headingFAQ2').click(function(){
        $('#faqHome').attr("src","/template_avrist/assets/img/common/premi_rendah2.gif");
      });

      $('#headingFAQ3').click(function(){
        $('#faqHome').attr("src","/template_avrist/assets/img/common/proteksi_lengkap.gif");
      });

      $('.navbar-toggler').click(function(){
        $('.navbar').removeClass("navbar-transparent");
      });
      $('.navbar-slide-close').click(function(){
        $('.navbar').addClass("navbar-transparent");
      });

    </script>

  </body>
</html>
