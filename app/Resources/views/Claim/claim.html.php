<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

use Pimcore\Model\Document;
use Pimcore\Model\Document\Page;

$this->extend('layout_avrist_new.html.php');?>

<div class="cover cover-sm cover-responsive bg-primary cover-general">
    <div class="cover-responsive-img">
        <div class="cover-responsive-overlay"></div>
    </div>
    <div class="container position-relative">
        <img src="/template_avrist/assets/img/pattern/ic_ornamen_yellow.png" class="pattern-purple" alt="Ornamen Purple">
        <!-- <img src="/template_avrist/assets/img/pattern/ic_ornament_triangle_yellow.png" class="pattern-triangle-yellow" alt="Ornamen Triangle Yellow"> -->
        <img src="/template_avrist/assets/img/pattern/ic_ornament_circle_pink.png" class="pattern-pink" alt="Ornamen Circle Pink">
        <nav aria-label="breadcrumb" class="general-breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Home</li></a>
                <li class="breadcrumb-item active"><a href="#"><?= $claim->getName() ?></li></a>
            </ol>
        </nav>
        <div class="cover-content">
            <h3 class="cover-title text-white animated fadeInUp delayp1"><?= $claim->getName() ?></h3>
        </div>
    </div>
</div>

<section class="py-main section-life-insurance-2">
    <div class="container">
        <div class="heading vp-fadeinup delayp1">
            <h2><?= $claim->getTitle() ?></h2>
            <p class="p-0 subtitle p-0"><?= $claim->getDescription() ?></p>
        </div>

        <div class="content mt-5">
            <div class="row">
            <?php foreach($claim->getBlock() as $block): ?>
            <div class="col-6 col-md-6 vp-fadeinup delayp1">
                <div class="card card-general layanan">
                    <div class="card-body">
                        <h5><?= $block['name']->getData() ?></h5>
                        <p class="mb-0 content-text"><?= $block['description']->getData() ?></p>
                        <div class="clearfix mt-4">
                            <a href="<?= $block['link']->getData() ?>">SUBMIT</a>
                        </div>
                    </div>
                </div>
            </div>
            <?php endforeach; ?>
            </div>
        </div>

    </div>
</section>

<section class="py-main bg-light">
    <div class="container">
        <div class="row row-3">
            <div class="col-md-6 content-center">
                <div class="heading">
                    <h2 class="mb-4"><?= $claim->getContactUs() ?></h2>
                    <p><?= $claim->getContactUsDescription() ?></p>
                    <a href="<?= $claim->getContactUsPage() ?>" class="btn btn-primary">Contact Us</a>
                </div>
            </div>
            <div class="col-md-6">
                <img src="/template_avrist/assets/img/tentang/ic_ilustrasi_home_mengapaavrist.png" class="img-fluid animated vp-fadeinup delayp1">
            </div>
        </div>
    </div>
</section>